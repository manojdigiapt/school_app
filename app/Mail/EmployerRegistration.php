<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class EmployerRegistration extends Mailable
{
    use Queueable, SerializesModels;
    public $Name;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($Name)
    {
        $this->Name = $Name;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('UI.email.employer_register_confirmation');
    }
}
