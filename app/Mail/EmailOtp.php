<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class EmailOtp extends Mailable
{
    use Queueable, SerializesModels;
    public $Otp;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($Otp)
    {
        $this->Otp = $Otp;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('UI.email.email_otp');
    }
}
