<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Admin\JobTitleModel;
use App\Model\Admin\JobTypeModel;
use App\Model\Admin\IndustriesModel;
use App\Model\Admin\QualificationModel;
use App\Model\Admin\FunctionalAreasModel;
use App\Model\Admin\TrainingCompletionModel;
use App\Model\Admin\CareerLevelModel;
use App\Model\Admin\TrainingDurationModel;
use App\Model\Admin\TeamSizeModel;
use App\Model\Admin\MaritalStatusModel;
use App\Model\Admin\TrainingTitleModel;
use App\Model\Admin\LanguagesModel;

class JobsAttributesController extends Controller
{
    public function GetJobTitle(){
        $title = "Job Title Lists";

        $GetJobTitle = JobTitleModel::get();
        return view("Admin.layouts.job_title_list", compact('title', 'GetJobTitle'));
    }

    public function AddJobTitle(Request $request){
        $GetJobTitle = new JobTitleModel();

        $GetJobTitle->name = $request->name;
        $GetJobTitle->status = 1;
        $InsertJobTitle = $GetJobTitle->save();
        if($InsertJobTitle){
            return response()->json(array(
                "error"=>FALSE,
                "message"=> "Job title added successfully"
            ));
        }else{
            return response()->json(array(
                "error"=>TRUE,
                "message"=> "failed"
            ));
        }
    }

    public function GetJobTitlebyId($id){
        $GetJobTitlebyId = JobTitleModel::where('id', $id)->first();
        echo json_encode($GetJobTitlebyId);
    }

    public function UpdateJobTitle(Request $request){
        $Id = $request->id;
        $GetJobTitles = JobTitleModel::findorFail($Id);

        $GetJobTitles->name = $request->name;
        $UpdateJobTitle = $GetJobTitles->save();
        if($UpdateJobTitle){
            return response()->json(array(
                "error"=>FALSE,
                "message"=> "Job title updated successfully"
            ));
        }else{
            return response()->json(array(
                "error"=>TRUE,
                "message"=> "failed"
            ));
        }
    }

    public function DeleteJobTitle($Id){
        $DeleteJobTitle = JobTitleModel::where('id', $Id)->delete();
        if($DeleteJobTitle){
            return response()->json(array(
                "error"=>FALSE,
                "message"=>"Job title deleted successfully"
            ));
        }else{
            return response()->json(array(
                "error"=>TRUE,
                "message"=>"Failed"
            ));
        }
    }

    // Job Types
    public function GetJobType(){
        $title = "Job Types";

        $GetJobType = JobTypeModel::get();
        return view("Admin.layouts.job_types", compact('title', 'GetJobType'));
    }

    public function AddJobType(Request $request){
        $GetJobType = new JobTypeModel();

        $GetJobType->job_type = $request->name;
        $GetJobType->status = 1;
        $InsertJobType = $GetJobType->save();
        if($InsertJobType){
            return response()->json(array(
                "error"=>FALSE,
                "message"=> "JobType added successfully"
            ));
        }else{
            return response()->json(array(
                "error"=>TRUE,
                "message"=> "failed"
            ));
        }
    }

    public function GetJobTypebyId($id){
        $GetJobTypebyId = JobTypeModel::where('id', $id)->first();
        echo json_encode($GetJobTypebyId);
    }

    public function UpdateJobType(Request $request){
        $Id = $request->id;
        $GetJobType = JobTypeModel::findorFail($Id);

        $GetJobType->job_type = $request->name;
        $UpdateJobType = $GetJobType->save();
        if($UpdateJobType){
            return response()->json(array(
                "error"=>FALSE,
                "message"=> "Job Type updated successfully"
            ));
        }else{
            return response()->json(array(
                "error"=>TRUE,
                "message"=> "failed"
            ));
        }
    }

    public function DeleteJobType($Id){
        $DeleteJobType = JobTypeModel::where('id', $Id)->delete();
        if($DeleteJobType){
            return response()->json(array(
                "error"=>FALSE,
                "message"=>"JobType deleted successfully"
            ));
        }else{
            return response()->json(array(
                "error"=>TRUE,
                "message"=>"Failed"
            ));
        }
    }

    // Industries
    public function GetIndustries(){
        $title = "Skill Names";

        $GetIndustries = IndustriesModel::get();
        return view("Admin.layouts.industries", compact('title', 'GetIndustries'));
    }

    public function AddIndustries(Request $request){
        $GetIndustries = new IndustriesModel();

        $GetIndustries->name = $request->name;
        $GetIndustries->status = 1;
        $InsertIndustries = $GetIndustries->save();
        if($InsertIndustries){
            return response()->json(array(
                "error"=>FALSE,
                "message"=> "Industries added successfully"
            ));
        }else{
            return response()->json(array(
                "error"=>TRUE,
                "message"=> "failed"
            ));
        }
    }

    public function GetIndustrybyId($id){
        $GetIndustrybyId = IndustriesModel::where('id', $id)->first();
        echo json_encode($GetIndustrybyId);
    }

    public function UpdateIndustries(Request $request){
        $Id = $request->id;
        $GetIndustries = IndustriesModel::findorFail($Id);

        $GetIndustries->name = $request->name;
        $UpdateIndustries = $GetIndustries->save();
        if($UpdateIndustries){
            return response()->json(array(
                "error"=>FALSE,
                "message"=> "Industries updated successfully"
            ));
        }else{
            return response()->json(array(
                "error"=>TRUE,
                "message"=> "failed"
            ));
        }
    }

    public function DeleteIndustries($Id){
        $DeleteIndustries = IndustriesModel::where('id', $Id)->delete();
        if($DeleteIndustries){
            return response()->json(array(
                "error"=>FALSE,
                "message"=>"Industries deleted successfully"
            ));
        }else{
            return response()->json(array(
                "error"=>TRUE,
                "message"=>"Failed"
            ));
        }
    }


    // Qualification
    public function GetQualification(){
        $title = "Qualification Lists";

        $GetQualifications = QualificationModel::get();
        return view("Admin.layouts.qualification", compact('title', 'GetQualifications'));
    }

    public function AddQualification(Request $request){
        $GetQualification = new QualificationModel();

        $GetQualification->qualification = $request->name;
        $GetQualification->status = 1;
        $InsertQualification = $GetQualification->save();
        if($InsertQualification){
            return response()->json(array(
                "error"=>FALSE,
                "message"=> "Qualification added successfully"
            ));
        }else{
            return response()->json(array(
                "error"=>TRUE,
                "message"=> "failed"
            ));
        }
    }

    public function GetQualificationbyId($id){
        $GetQualificationbyId = QualificationModel::where('id', $id)->first();
        echo json_encode($GetQualificationbyId);
    }

    public function UpdateQualification(Request $request){
        $Id = $request->id;
        $GetQualification = QualificationModel::findorFail($Id);

        $GetQualification->qualification = $request->name;
        $UpdateQualification = $GetQualification->save();
        if($UpdateQualification){
            return response()->json(array(
                "error"=>FALSE,
                "message"=> "Qualification updated successfully"
            ));
        }else{
            return response()->json(array(
                "error"=>TRUE,
                "message"=> "failed"
            ));
        }
    }

    public function DeleteQualification($Id){
        $DeleteQualification = QualificationModel::where('id', $Id)->delete();
        if($DeleteQualification){
            return response()->json(array(
                "error"=>FALSE,
                "message"=>"Qualification deleted successfully"
            ));
        }else{
            return response()->json(array(
                "error"=>TRUE,
                "message"=>"Failed"
            ));
        }
    }



    // Functional Areas
    public function GetFunctionalAreas(){
        $title = "Functional Area Lists";

        $GetFunctionalAreas = FunctionalAreasModel::get();
        return view("Admin.layouts.functional_areas", compact('title', 'GetFunctionalAreas'));
    }

    public function AddFunctionalAreas(Request $request){
        $GetFunctionalAreas = new FunctionalAreasModel();

        $GetFunctionalAreas->functional_areas = $request->name;
        $GetFunctionalAreas->status = 1;
        $InsertFunctionalAreas = $GetFunctionalAreas->save();
        if($InsertFunctionalAreas){
            return response()->json(array(
                "error"=>FALSE,
                "message"=> "FunctionalAreas added successfully"
            ));
        }else{
            return response()->json(array(
                "error"=>TRUE,
                "message"=> "failed"
            ));
        }
    }

    public function GetFunctionalAreasbyId($id){
        $GetFunctionalAreasbyId = FunctionalAreasModel::where('id', $id)->first();
        echo json_encode($GetFunctionalAreasbyId);
    }

    public function UpdateFunctionalAreas(Request $request){
        $Id = $request->id;
        $GetFunctionalAreas = FunctionalAreasModel::findorFail($Id);

        $GetFunctionalAreas->functional_areas = $request->name;
        $UpdateFunctionalAreas = $GetFunctionalAreas->save();
        if($UpdateFunctionalAreas){
            return response()->json(array(
                "error"=>FALSE,
                "message"=> "Functional Areas updated successfully"
            ));
        }else{
            return response()->json(array(
                "error"=>TRUE,
                "message"=> "failed"
            ));
        }
    }

    public function DeleteFunctionalAreas($Id){
        $DeleteFunctionalAreas = FunctionalAreasModel::where('id', $Id)->delete();
        if($DeleteFunctionalAreas){
            return response()->json(array(
                "error"=>FALSE,
                "message"=>"FunctionalAreas deleted successfully"
            ));
        }else{
            return response()->json(array(
                "error"=>TRUE,
                "message"=>"Failed"
            ));
        }
    }


    // Training Completion
    public function GetTrainingCompletion(){
        $title = "Training Completion Lists";

        $GetTrainingCompletion = TrainingCompletionModel::get();
        return view("Admin.layouts.training_completion", compact('title', 'GetTrainingCompletion'));
    }

    public function AddTrainingCompletion(Request $request){
        $GetTrainingCompletion = new TrainingCompletionModel();

        $GetTrainingCompletion->training_completion = $request->name;
        $GetTrainingCompletion->status = 1;
        $InsertTrainingCompletion = $GetTrainingCompletion->save();
        if($InsertTrainingCompletion){
            return response()->json(array(
                "error"=>FALSE,
                "message"=> "Training Completion added successfully"
            ));
        }else{
            return response()->json(array(
                "error"=>TRUE,
                "message"=> "failed"
            ));
        }
    }

    public function GetTrainingCompletionbyId($id){
        $GetTrainingCompletionbyId = TrainingCompletionModel::where('id', $id)->first();
        echo json_encode($GetTrainingCompletionbyId);
    }

    public function UpdateTrainingCompletion(Request $request){
        $Id = $request->id;
        $GetTrainingCompletion = TrainingCompletionModel::findorFail($Id);

        $GetTrainingCompletion->training_completion = $request->name;
        $UpdateTrainingCompletion = $GetTrainingCompletion->save();
        if($UpdateTrainingCompletion){
            return response()->json(array(
                "error"=>FALSE,
                "message"=> "Training completion updated successfully"
            ));
        }else{
            return response()->json(array(
                "error"=>TRUE,
                "message"=> "failed"
            ));
        }
    }

    public function DeleteTrainingCompletion($Id){
        $DeleteTrainingCompletio = TrainingCompletionModel::where('id', $Id)->delete();
        if($DeleteTrainingCompletio){
            return response()->json(array(
                "error"=>FALSE,
                "message"=>"Training completion deleted successfully"
            ));
        }else{
            return response()->json(array(
                "error"=>TRUE,
                "message"=>"Failed"
            ));
        }
    }


    // Training Completion
    public function GetCareerLevel(){
        $title = "Career Level Lists";

        $GetCareerLevel = CareerLevelModel::get();
        return view("Admin.layouts.career_level", compact('title', 'GetCareerLevel'));
    }

    public function AddCareerLevel(Request $request){
        $GetCareerLevel = new CareerLevelModel();

        $GetCareerLevel->career_level = $request->name;
        $GetCareerLevel->status = 1;
        $InsertCareerLevel = $GetCareerLevel->save();
        if($InsertCareerLevel){
            return response()->json(array(
                "error"=>FALSE,
                "message"=> "Career level added successfully"
            ));
        }else{
            return response()->json(array(
                "error"=>TRUE,
                "message"=> "failed"
            ));
        }
    }

    public function GetCareerLevelbyId($id){
        $GetCareerLevelbyId = CareerLevelModel::where('id', $id)->first();
        echo json_encode($GetCareerLevelbyId);
    }

    public function UpdateCareerLevel(Request $request){
        $Id = $request->id;
        $GetCareerLevel = CareerLevelModel::findorFail($Id);

        $GetCareerLevel->career_level = $request->name;
        $UpdateCareerLevel = $GetCareerLevel->save();
        if($UpdateCareerLevel){
            return response()->json(array(
                "error"=>FALSE,
                "message"=> "Career level updated successfully"
            ));
        }else{
            return response()->json(array(
                "error"=>TRUE,
                "message"=> "failed"
            ));
        }
    }

    public function DeleteCareerLevel($Id){
        $DeleteCareerLevel = CareerLevelModel::where('id', $Id)->delete();
        if($DeleteCareerLevel){
            return response()->json(array(
                "error"=>FALSE,
                "message"=>"Career level deleted successfully"
            ));
        }else{
            return response()->json(array(
                "error"=>TRUE,
                "message"=>"Failed"
            ));
        }
    }



     // Training Duration
     public function GetTrainingDuration(){
        $title = "Training Duration";

        $GetTrainingDuration = TrainingDurationModel::get();
        return view("Admin.layouts.training_duration", compact('title', 'GetTrainingDuration'));
    }

    public function AddTrainingDuration(Request $request){
        $GetTrainingDuration = new TrainingDurationModel();

        $GetTrainingDuration->training_duration = $request->name;
        $GetTrainingDuration->status = 1;
        $InsertTrainingDuration = $GetTrainingDuration->save();
        if($InsertTrainingDuration){
            return response()->json(array(
                "error"=>FALSE,
                "message"=> "Training duration added successfully"
            ));
        }else{
            return response()->json(array(
                "error"=>TRUE,
                "message"=> "failed"
            ));
        }
    }

    public function GetTrainingDurationbyId($id){
        $GetTrainingDurationbyId = TrainingDurationModel::where('id', $id)->first();
        echo json_encode($GetTrainingDurationbyId);
    }

    public function UpdateTrainingDuration(Request $request){
        $Id = $request->id;
        $GetTrainingDuration = TrainingDurationModel::findorFail($Id);

        $GetTrainingDuration->training_duration = $request->name;
        $UpdateTrainingDuration = $GetTrainingDuration->save();
        if($UpdateTrainingDuration){
            return response()->json(array(
                "error"=>FALSE,
                "message"=> "Training duration updated successfully"
            ));
        }else{
            return response()->json(array(
                "error"=>TRUE,
                "message"=> "failed"
            ));
        }
    }

    public function DeleteTrainingDuration($Id){
        $DeleteTrainingDuration = TrainingDurationModel::where('id', $Id)->delete();
        if($DeleteTrainingDuration){
            return response()->json(array(
                "error"=>FALSE,
                "message"=>"Training duration deleted successfully"
            ));
        }else{
            return response()->json(array(
                "error"=>TRUE,
                "message"=>"Failed"
            ));
        }
    }

    // Team size
    public function GetTeamSize(){
        $title = "Team Size";

        $GetTeamSize = TeamSizeModel::get();
        return view("Admin.layouts.team_size", compact('title', 'GetTeamSize'));
    }

    public function AddTeamSize(Request $request){
        $GetTeamSize = new TeamSizeModel();

        $GetTeamSize->team_size = $request->name;
        $GetTeamSize->status = 1;
        $InsertTeamSize = $GetTeamSize->save();
        if($InsertTeamSize){
            return response()->json(array(
                "error"=>FALSE,
                "message"=> "Team size added successfully"
            ));
        }else{
            return response()->json(array(
                "error"=>TRUE,
                "message"=> "failed"
            ));
        }
    }

    public function GetTeamSizebyId($id){
        $GetTeamSizebyId = TeamSizeModel::where('id', $id)->first();
        echo json_encode($GetTeamSizebyId);
    }

    public function UpdateTeamSize(Request $request){
        $Id = $request->id;
        $GetTeamSize = TeamSizeModel::findorFail($Id);

        $GetTeamSize->team_size = $request->name;
        $UpdateTeamSize = $GetTeamSize->save();
        if($UpdateTeamSize){
            return response()->json(array(
                "error"=>FALSE,
                "message"=> "Team size updated successfully"
            ));
        }else{
            return response()->json(array(
                "error"=>TRUE,
                "message"=> "failed"
            ));
        }
    }

    public function DeleteTeamSize($Id){
        $DeleteTeamSize = TeamSizeModel::where('id', $Id)->delete();
        if($DeleteTeamSize){
            return response()->json(array(
                "error"=>FALSE,
                "message"=>"Team size deleted successfully"
            ));
        }else{
            return response()->json(array(
                "error"=>TRUE,
                "message"=>"Failed"
            ));
        }
    }


    // Marital status
    public function GetMarital(){
        $title = "Marital Status";

        $GetMarital = MaritalStatusModel::get();
        return view("Admin.layouts.martial_status", compact('title', 'GetMarital'));
    }

    public function AddMarital(Request $request){
        $GetMarital = new MaritalStatusModel();

        $GetMarital->marital_status = $request->name;
        $GetMarital->status = 1;
        $InsertMarital = $GetMarital->save();
        if($InsertMarital){
            return response()->json(array(
                "error"=>FALSE,
                "message"=> "Marital status added successfully"
            ));
        }else{
            return response()->json(array(
                "error"=>TRUE,
                "message"=> "failed"
            ));
        }
    }

    public function GetMaritalbyId($id){
        $GetMaritalbyId = MaritalStatusModel::where('id', $id)->first();
        echo json_encode($GetMaritalbyId);
    }

    public function UpdateMarital(Request $request){
        $Id = $request->id;
        $GetMarital = MaritalStatusModel::findorFail($Id);

        $GetMarital->marital_status = $request->name;
        $UpdateMarital = $GetMarital->save();
        if($UpdateMarital){
            return response()->json(array(
                "error"=>FALSE,
                "message"=> "Marital status updated successfully"
            ));
        }else{
            return response()->json(array(
                "error"=>TRUE,
                "message"=> "failed"
            ));
        }
    }

    public function DeleteMarital($Id){
        $DeleteMarital = MaritalStatusModel::where('id', $Id)->delete();
        if($DeleteMarital){
            return response()->json(array(
                "error"=>FALSE,
                "message"=>"Marital status deleted successfully"
            ));
        }else{
            return response()->json(array(
                "error"=>TRUE,
                "message"=>"Failed"
            ));
        }
    }

    // Experience Level
    public function GetTrainingTitle(){
        $title = "Training title";

        $GetTitle = TrainingTitleModel::get();
        return view("Admin.layouts.training_title", compact('title', 'GetTitle'));
    }

    public function AddTrainingTitle(Request $request){
        $GetTitle = new TrainingTitleModel();

        $GetTitle->title = $request->name;
        $GetTitle->status = 1;
        $InsertTitle = $GetTitle->save();
        if($InsertTitle){
            return response()->json(array(
                "error"=>FALSE,
                "message"=> "Title added successfully"
            ));
        }else{
            return response()->json(array(
                "error"=>TRUE,
                "message"=> "failed"
            ));
        }
    }

    public function GetTrainingTitlebyId($id){
        $GetTrainingTitlebyId = TrainingTitleModel::where('id', $id)->first();
        echo json_encode($GetTrainingTitlebyId);
    }

    public function UpdateTrainingTitle(Request $request){
        $Id = $request->id;
        $GetTitle = TrainingTitleModel::findorFail($Id);

        $GetTitle->title = $request->name;
        $UpdateTitle = $GetTitle->save();
        if($UpdateTitle){
            return response()->json(array(
                "error"=>FALSE,
                "message"=> "Training title updated successfully"
            ));
        }else{
            return response()->json(array(
                "error"=>TRUE,
                "message"=> "failed"
            ));
        }
    }

    public function DeleteTrainingTitle($Id){
        $DeleteTitle = TrainingTitleModel::where('id', $Id)->delete();
        if($DeleteTitle){
            return response()->json(array(
                "error"=>FALSE,
                "message"=>"Training title deleted successfully"
            ));
        }else{
            return response()->json(array(
                "error"=>TRUE,
                "message"=>"Failed"
            ));
        }
    }


    // Languages
    public function GetLanguages(){
        $title = "Languages";

        $GetLanguages = LanguagesModel::get();
        return view("Admin.layouts.languages", compact('title', 'GetLanguages'));
    }

    public function AddLanguages(Request $request){
        $GetLanguages = new LanguagesModel();

        $GetLanguages->languages = $request->name;
        $GetLanguages->status = 1;
        $InsertLanguages = $GetLanguages->save();
        if($InsertLanguages){
            return response()->json(array(
                "error"=>FALSE,
                "message"=> "Languages added successfully"
            ));
        }else{
            return response()->json(array(
                "error"=>TRUE,
                "message"=> "failed"
            ));
        }
    }

    public function GetLanguagesbyId($id){
        $GetLanguagesbyId = LanguagesModel::where('id', $id)->first();
        echo json_encode($GetLanguagesbyId);
    }

    public function UpdateLanguages(Request $request){
        $Id = $request->id;
        $GetLanguages = LanguagesModel::findorFail($Id);

        $GetLanguages->languages = $request->name;
        $UpdateLanguages = $GetLanguages->save();
        if($UpdateLanguages){
            return response()->json(array(
                "error"=>FALSE,
                "message"=> "Languages updated successfully"
            ));
        }else{
            return response()->json(array(
                "error"=>TRUE,
                "message"=> "failed"
            ));
        }
    }

    public function DeleteLanguages($Id){
        $DeleteLanguages = LanguagesModel::where('id', $Id)->delete();
        if($DeleteLanguages){
            return response()->json(array(
                "error"=>FALSE,
                "message"=>"Languages deleted successfully"
            ));
        }else{
            return response()->json(array(
                "error"=>TRUE,
                "message"=>"Failed"
            ));
        }
    }

}
