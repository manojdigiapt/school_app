<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Admin\AdminModel;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;


class AdminController extends Controller
{
    public function login(){
        $title = "Admin Login";

        return view("Admin.login", compact('title'));
    }

    public function Dashboard(){
        $title = "Admin Dashboard";

        return view("Admin.layouts.dashboard", compact('title'));
    }


    public function AdminLogin(Request $request){
        $Email = $request->email;
        $Password = $request->password;

        $CheckCredentials = AdminModel::where('email', $Email)->first();
        
        if($CheckCredentials){
            $CheckAdminPassword = Hash::check($Password, $CheckCredentials->password);
            
            if($CheckAdminPassword == true){
                $GetRoleId = AdminModel::where('email', $Email)->first();
                
                if($GetRoleId->role_id == 1){
                    if (Auth::guard('super_admin')->attempt(['email' => $Email, 'password' => $Password])) {
                        $request->session()->put('AdminName', Auth::guard('super_admin')->user()->name);
                        $request->session()->put('AdminEmail', Auth::guard('super_admin')->user()->email);
                        $request->session()->put('AdminId', Auth::guard('super_admin')->user()->id);
                        return response()->json(array(
                            "error"=>FALSE,
                            "message"=> "Login successfully",
                            "type" => 1
                        ));
                    }else{
                        return response()->json(array(
                            "error"=>TRUE,
                            "message"=> "Please check your credentials."
                        ));
                    }
                }else if($GetRoleId->role_id == 2){{
                    if (Auth::guard('sub_admin')->attempt(['email' => $Email, 'password' => $Password])) {
                        $request->session()->put('SubAdminName', Auth::guard('sub_admin')->user()->name);
                        $request->session()->put('SubAdminEmail', Auth::guard('sub_admin')->user()->email);
                        $request->session()->put('SubAdminId', Auth::guard('sub_admin')->user()->id);
                        return response()->json(array(
                            "error"=>FALSE,
                            "message"=> "Login successfully",
                            "type" => 2
                        ));
                    }else{
                        return response()->json(array(
                            "error"=>TRUE,
                            "message"=> "Please check your credentials."
                        ));
                    }
                }
            }else{
                return response()->json(array(
                    "error"=>TRUE,
                    "message"=> "Please check your credentials."
                ));
            }
        }else{
            return response()->json(array(
                "error"=>TRUE,
                "message"=> "Please check your credentials."
            ));
        }
        }
    }


    public function AddAdmins(Request $request){
        $AdminUsers = new AdminModel();

        $AdminUsers->name = $request->name;
        $AdminUsers->email = $request->email;
        $AdminUsers->role_id = $request->role;
        $AdminUsers->password = Hash::make($request->password);

        $CheckEmail = AdminModel::where('email', $request->email)->first();
        
        if($CheckEmail){
            return response()->json(array(
                "error"=>TRUE,
                "message"=> "Email id is already exist."
            ));
        }else{
            $InsertAdmins = $AdminUsers->save();
            if($InsertAdmins){
                return response()->json(array(
                    "error"=>FALSE,
                    "message"=> "Admin users added successfully."
                ));
            }else{
                return response()->json(array(
                    "error"=>TRUE,
                    "message"=> "Failed"
                ));
            }
        }
        
    }


    public function GetEditAdminUser($id){
        $GetAdminUsers = AdminModel::findorFail($id);
        echo json_encode($GetAdminUsers);
    }

    public function UpdateAdmins(Request $request){
        $AdminUsers = new AdminModel();
        $GetAdmins = AdminModel::where('id', $request->id)->first();

        $GetAdmins->name = $request->name;
        $GetAdmins->email = $request->email;
        $GetAdmins->role_id = $request->role;
        // $GetAdmins->password = Hash::make($request->password);

        $UpdateAdmins = $GetAdmins->save();
        if($UpdateAdmins){
            return response()->json(array(
                "error"=>FALSE,
                "message"=> "Admin users updated successfully."
            ));
        }else{
            return response()->json(array(
                "error"=>TRUE,
                "message"=> "Failed"
            ));
        }
        
    }

    public function DeleteAdminUsers($Id){
        $DeleteAdmins = AdminModel::where('id', $Id)->delete();
        if($DeleteAdmins){
            return response()->json(array(
                "error"=>FALSE,
                "message"=>"Admin deleted successfully"
            ));
        }else{
            return response()->json(array(
                "error"=>TRUE,
                "message"=>"Failed"
            ));
        }
    }

    public function Adminlists(){
        $title = "Admin Users List";

        $GetAdmins = AdminModel::get();

        return view("Admin.layouts.admin_users", compact('title', 'GetAdmins'));
    }
}
