<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\UI\JobsModel;
use App\Model\UI\EmployerModel;
use DB;

class JobsController extends Controller
{
    public function JobsList(){
        $title = "Jobs List";
        $GetJobs = DB::select("SELECT jobs.id, employer.company_name, jobs.title, jobs.email, jobs.city, jobs.job_type FROM jobs, employer WHERE jobs.employer_id = employer.id");

        return view('Admin.layouts.jobs_list', compact('title','GetJobs'));
    }

    public function add_jobs(){
        $title = "Add Jobs";
        $GetEmployer = EmployerModel::get();

        return view('Admin.layouts.add_jobs', compact('title', 'GetEmployer'));
    }

    public function PostJobs(Request $request){
        $Jobs = new JobsModel();

        $Jobs->employer_id = $request->employer_id;
        $Jobs->title = $request->title;
        $Jobs->slug = strtolower(str_replace(' ', '_', $request->title));
        $Jobs->location = $request->address;
        $Jobs->zip_code = $request->zipcode;
        $Jobs->country = $request->country;
        $Jobs->state = $request->state;
        $Jobs->city = $request->city;
        // $Jobs->email = Session::get("EmployerEmail");
        // $Jobs->mobile = Session::get("EmployerMobile");
        $Jobs->job_description = $request->description;
        $Jobs->job_type = $request->job_type;
        $Jobs->industry = $request->industry;
        $Jobs->role = $request->role;
        $Jobs->offered_salary = $request->offered_salary;
        $Jobs->salary_type = $request->salary_type;
        $Jobs->career_level = $request->career_level;
        $Jobs->experience = $request->experience;
        $Jobs->gender = $request->gender;
        $Jobs->qualification = $request->qualification;
        $Jobs->job_expires = $request->last_date;
        $Jobs->total_positions = $request->positions;

        $AddJobs = $Jobs->save();
        if($AddJobs){
            return response()->json(array(
                "error" => FALSE,
                "message" => "Jobs posted successfully"
            ));
        }else{
            return response()->json(array(
                "error" => TRUE,
                "message" => "Failed"
            ));
        }

    }
}
