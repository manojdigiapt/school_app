<?php

namespace App\Http\Controllers\UI;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Model\UI\EmployerModel;
use App\Model\UI\JobsModel;
use App\Model\UI\TrainingModel;
use App\Model\UI\CandidateProfileModel;
use App\Model\UI\CandidateModel;
use App\Model\UI\TotalViews;
use App\Model\UI\JobsApplied;
use Illuminate\Support\Facades\Auth;
use App\Model\UI\ShortlistCandidatesModel;
use App\Model\UI\SelectedTraineeModel;
use App\Model\UI\ReviewsModel;
use Illuminate\Support\Facades\Hash;
use App\Model\UI\FavouriteModel;
use App\Model\UI\CandidateResume;

use DateTime;
use Session;
use DB;

// Admin Model
use App\Model\Admin\TrainingCompletionModel;
use App\Model\Admin\IndustriesModel;
use App\Model\Admin\JobTitleModel;
use App\Model\Admin\QualificationModel;
use App\Model\Admin\TrainingTitleModel;


class TrainingController extends Controller
{
    public function __construct(){
        $this->middleware('guest:trainee')->except('logout', 'TrainingDetails', 'AppliedTraineeDetails', 'DownloadCV', 'ViewAllTrainings', 'ChangeTrainingStatus', 'ChangeAppliedTraineeTrainingStatus', 'GetStudentDataByCertificates', 'AddTraineeProfile', 'InsertOrUpdateCV', 'UpdateProfilePic', 'InsertEducation', 'UpdatePersonalDetails', 'ChangeTrainingEndDate', 'ChangeTrainingCompletionStatus', 'DeleteEducation');
    }

    public function logout(Request $request) {
        // Auth::logout();
        Auth::guard('trainee')->logout();
        return redirect('/');
    }

    

    public function TraineeChangePassword(){
        $title = "Trainee Change Password";
        $GetEmail = Session::get('TraineeEmail');
        $GetCandidateId = Session::get('TraineeId');

        $TraineeProfile = CandidateProfileModel::where("candidate_id", $GetCandidateId)->first();

        $CandidateProfile = CandidateProfileModel::where("candidate_id", $GetCandidateId)->first();
        if($CandidateProfile){
            $GetCandidateId = Session::get('TraineeId');
        }else{
            $GetCandidateId = "0";
        }
        // echo $GetCandidateId;
        // exit;
        $Profile = CandidateModel::where("email", $GetEmail)->first();
        
        $GetFavouriteJobs = DB::select("SELECT employer.profile_pic, jobs.title, employer.slug AS CompanySlug, jobs.slug, employer.company_name FROM favourite_jobs, jobs, employer WHERE favourite_jobs.candidate_id = $GetCandidateId AND favourite_jobs.jobs_id = jobs.id AND jobs.employer_id = employer.id AND favourite_jobs.status = 1 AND favourite_jobs.type = 1");

        return view("UI.layouts.trainee_change_password", compact('title', "Profile", 'CandidateProfile', 'GetFavouriteJobs', 'TraineeProfile'));
    }


    

    public function GetAllTrainings(){
        $title = "All Trainings";
        $GetTraineeId = Session::get('TraineeId');

        $GetTrainings = DB::select("SELECT employer.profile_pic, training.slug, training.title, employer.company_name, employer.country, employer.city, training.duration, employer.slug AS CompanySlug, training.id FROM training, employer WHERE training.employer_id = employer.id ORDER BY training.id ASC LIMIT 1");

        $TraineeProfile = CandidateProfileModel::where("candidate_id", $GetTraineeId)->first();

        $GetFavouriteTrainings = DB::select("SELECT employer.profile_pic, training.title, training.duration, employer.slug AS CompanySlug, training.slug, employer.company_name FROM favourite_jobs, training, employer WHERE favourite_jobs.candidate_id = $GetTraineeId AND favourite_jobs.training_id = training.id AND training.employer_id = employer.id AND favourite_jobs.status = 1 AND favourite_jobs.type = 2");

        return view("UI.layouts.trainings_list", compact('title', 'GetTrainings', 'TraineeProfile', 'GetFavouriteTrainings'));
    }





    // Trainee Dashboard

    public function TraineeProfile(){
        $title = "Trainee Profile";
        $GetEmail = Session::get('TraineeEmail');
        $GetTraineeId = Session::get('TraineeId');
        $TraineeProfile = CandidateProfileModel::where("candidate_id", $GetTraineeId)->first();
        if($TraineeProfile){
            $GetTraineeId = Session::get('TraineeId');
        }else{
            $GetTraineeId = "0";
        }
        // echo $GetCandidateId;
        // exit;

        $GetFavouriteTrainings = DB::select("SELECT employer.profile_pic, training.title, training.duration, employer.slug AS CompanySlug, training.slug, employer.company_name FROM favourite_jobs, training, employer WHERE favourite_jobs.candidate_id = $GetTraineeId AND favourite_jobs.training_id = training.id AND training.employer_id = employer.id AND favourite_jobs.status = 1 AND favourite_jobs.type = 2");

        
        $GetCV = CandidateResume::where('candidate_id', $GetTraineeId)
        ->where('type', 4)->first();

        // echo json_encode($GetTraineeId);
        // exit;

        $Profile = CandidateModel::where("id", $GetTraineeId)->first();
        $GetQualification = QualificationModel::get();


        $GetEducation = DB::table('resume')
                            ->select('resume.*', 'qualifications.qualification')
                            ->join('qualifications', 'qualifications.id', '=', 'resume.department')
                            ->where('type', 1)
                            ->where('resume.candidate_id', $GetTraineeId)
                            ->orderBy('id', 'DESC')
                            ->get();

        return view("UI.layouts.trainee_profile", compact('title', "Profile", 'TraineeProfile', 'GetFavouriteTrainings', 'GetCV', 'GetEducation', 'GetQualification'));
    }


    

    public function GetAppliedTrainings(){
        $title = "Applied Trainings";
        $GetTraineeId = Session::get('TraineeId');

        $GetEmail = Session::get('TraineeEmail');

        $Profile = CandidateModel::where("email", $GetEmail)->first();

        $GetAppliedTrainings = DB::select("SELECT DISTINCT training_title.title, employer.company_name, employer.address, employer.city, jobs_applied.created_at, training.duration, jobs_applied.recruiter_status, jobs_applied.download_cv_status, jobs_applied.application_view_date, jobs_applied.download_cv_date, training.training_completion, jobs_applied.training_status, jobs_applied.training_completion_status FROM training_title, jobs_applied, training, employer WHERE jobs_applied.training_id = training.id AND training.employer_id = employer.id AND training_title.id = training.title AND jobs_applied.candidate_id = $GetTraineeId");

        $TraineeProfile = CandidateProfileModel::where("candidate_id", $GetTraineeId)->first();

        $GetRecruiterViewed = JobsApplied::where('candidate_id', $GetTraineeId)
                                        ->where('recruiter_status', 1)
                                        ->count();

        $GetApplicationDownload = JobsApplied::where('candidate_id', $GetTraineeId)
                                        ->where('training_completion_status', 1)
                                        ->count();

        $GetFavouriteTrainings = DB::select("SELECT employer.profile_pic, training.title, training.duration, employer.slug AS CompanySlug, training.slug, employer.company_name FROM favourite_jobs, training, employer WHERE favourite_jobs.candidate_id = $GetTraineeId AND favourite_jobs.training_id = training.id AND training.employer_id = employer.id AND favourite_jobs.status = 1 AND favourite_jobs.type = 2");

        // echo json_encode($GetAppliedTrainings);
        // exit;
        return view("UI.layouts.trainee_applied_trainings", compact('title', 'GetAppliedTrainings', 'TraineeProfile', 'GetFavouriteTrainings', 'GetRecruiterViewed', 'GetApplicationDownload', 'Profile'));
    }


    public function SearchTrainingTitle(Request $request){
        $title = $request->term;

        $GetTrainings = DB::select("SELECT training.title FROM training, employer WHERE employer.id = training.employer_id AND title LIKE '%$title%' ORDER BY title");
        echo json_encode($GetTrainings);

    }

    public function SearchTrainings(Request $request){
        $Title = $request->title;
        $City = $request->city;

        $GetTrainingsBySearch = DB::select("SELECT employer.profile_pic, training.slug, training_title.title, employer.company_name, employer.country, employer.city, training.duration, training.start_date, training.stipend, training.training_completion, training.description, training.created_at, employer.slug AS CompanySlug, training.id FROM training, employer, training_title WHERE training.employer_id = employer.id AND training_title.id = training.title AND training_title.title LIKE '%$Title%' AND employer.city = '$City'");

        // echo json_encode($Title);
        // exit;

        $GetPostedOn = "";
        $GetTrainingResults = "";

        $GetTrainingDuration = "";

        $GetStipend = "";
        $GetTrainingCompletion = "";


        foreach($GetTrainingsBySearch as $Trainings){

            $now = new DateTime;
            $full = false;	
            $ago = new DateTime($Trainings->created_at);
            $diff = $now->diff($ago);

            $diff->w = floor($diff->d / 7);
            $diff->d -= $diff->w * 7;

            $string = array(
            'y' => 'year',
            'm' => 'month',
            'w' => 'week',
            'd' => 'day',
            'h' => 'hour',
            'i' => 'minute',
            's' => 'second',
            );

            foreach ($string as $k => &$v) {
            if ($diff->$k) {
            $v = $diff->$k . ' ' . $v . ($diff->$k > 1 ? 's' : '');
            } else {
            unset($string[$k]);
            }
            }

            if (!$full) $string = array_slice($string, 0, 1);
            $GetPostedOn =  $string ? implode(', ', $string) . ' ago' : 'just now';


            if($Trainings->duration == 1){
                $GetTrainingDuration = "30 Days";
            }else{
                $GetTrainingDuration = "60 Days";
            }

            $num = $Trainings->stipend;
            $units = ['', 'K', 'M'];
            for($i = 0; $num>=1000;$i++){
                $num /= 1000;
            }
            $GetStipend = round($num, 1).$units[$i];
                      
            if($Trainings->training_completion == 1){
                $GetTrainingCompletion = "Certification";
            }else{
                $GetTrainingCompletion = "Full Time Job";
            }


                $GetTrainingResults = "<div class='job-listings-sec no-border text-center'>
                <img id='AjaxLoader' class='jobs-search-loader' src='/UI/ajax_loader.gif' style='display:none;'>
                <div class='job-listing wtabs recommented-jobs'>
                    <div class='job-title-sec recommended-jobs-pad-left'>
                        <h3><a href='/JobDetails/companyslug/slug' target='_blank'>".$Trainings->title."</a></h3>
                        <p class='recommended-jobs-company-name'>".$Trainings->company_name."</p>
                        <div class='job-lctn job-attributes-recommended-jobs'><i class='fa fa-briefcase'></i>".date('d M Y', strtotime($Trainings->start_date))."</div>
                        <div class='job-lctn job-attributes-recommended-jobs'><i class='fa fa-clock-o'></i>
                            ".$GetTrainingDuration."
                        </div>
                        <div class='job-lctn job-attributes-recommended-jobs'><i class='fa fa-briefcase' ></i> 
                            ".$GetStipend."
                                / Month
                        </div>
                        <div class='job-lctn job-attributes-recommended-jobs'><i class='fa fa-industry'></i>
                            ".$GetTrainingCompletion."
                        </div>
                    </div>
                    <p class='recommended-jobs-posted-on'>Posted on: 
                        ".$GetPostedOn."
                    </p>
                </div>
                </div>";
        }

        

        if($GetTrainingResults){
            echo $GetTrainingResults;
        }else{
            echo json_encode("No Results Found...");
        }
        
    }


    public function TrainingDetails($company_name, $slug){
        $title = "Training Details";
        // $GetTrainingBySlug = DB::select("SELECT *  FROM training, employer WHERE training.employer_id = employer.id AND training.slug = '$slug'");
        $GetTraineeId = Session::get('TraineeId'); 

        $GetTrainingId = TrainingModel::where('slug', $slug)->first();

        

        $GetEmployerId = Session::get("EmployerId");
        $EmployerProfile = EmployerModel::where("id", $GetEmployerId)->first();

        $GetEmail = Session::get('TraineeEmail');

        $Profile = CandidateModel::where("email", $GetEmail)->first();

        $GetTrainingSegment = request()->segment(3);

        $GetIndustries = IndustriesModel::get();
        // $GetStipend = TrainingCompletionModel::get();
        $GetJobTitle = JobTitleModel::get();
        $GetQualification = QualificationModel::get();
        $GetTrainingTitle = TrainingTitleModel::get();

        $CheckPercentage = DB::table('candidate_profile')
                            ->where('candidate_id', $GetTraineeId)
                            ->where('candidate_profile.profile_completion', '>=', '90')
                            ->first();

        if(Auth::guard('trainee')->check()){
            $CheckTotalViews = DB::select("SELECT * FROM total_views WHERE total_views.candidate_id = $GetTraineeId AND total_views.training_id = $GetTrainingId->id");

            if($CheckTotalViews != null){
                $GetTrainingBySlug = DB::select("SELECT training.id, training_title.title, employer.website, employer.mobile, employer.email, employer.profile_pic, employer.company_name, employer.city, training.duration, training.created_at, training.description, training.start_date, training.stipend, training.last_date_of_application, training.training_completion  FROM training, employer, training_title WHERE training.employer_id = employer.id AND training_title.id = training.title AND training.slug = '$slug'");

                $GetTotalViewsCount = TotalViews::where('training_id', $GetTrainingId->id)
                                            ->get();

                $CheckTrainingApplied = JobsApplied::where('candidate_id', $GetTraineeId)
                                                ->where('training_id', $GetTrainingId->id)
                                                ->first();

                $CheckApplicantsCount = JobsApplied::where('training_id', $GetTrainingId->id)
                                                    ->get();

                $CheckFavourites = FavouriteModel::where('candidate_id', $GetTraineeId)
                                                ->where('training_id', $GetTrainingId->id)
                                                ->where('type', 2)
                                                ->first();

                $TraineeProfile = CandidateProfileModel::where("candidate_id", $GetTraineeId)->first();

                $GetFavouriteTrainings = DB::select("SELECT employer.profile_pic, training.title, training.duration, employer.slug AS CompanySlug, training.slug, employer.company_name FROM favourite_jobs, training, employer WHERE favourite_jobs.candidate_id = $GetTraineeId AND favourite_jobs.training_id = training.id AND training.employer_id = employer.id AND favourite_jobs.status = 1 AND favourite_jobs.type = 2");

                $GetSimilarTrainings = DB::select("SELECT employer.profile_pic, training.title, training.duration, employer.slug AS CompanySlug, training.slug, employer.company_name, employer.state, employer.city FROM training, employer WHERE training.slug != '$GetTrainingSegment' AND training.employer_id = employer.id ORDER BY training.id DESC LIMIT 4");

                $GetStipend = TrainingCompletionModel::get();

                return view("UI.layouts.training_details", compact('title', 'GetTrainingBySlug', 'GetTotalViewsCount', 'CheckTrainingApplied', 'CheckApplicantsCount', 'CheckFavourites', 'TraineeProfile', 'GetFavouriteTrainings', 'GetSimilarTrainings', 'GetStipend', 'Profile', 'EmployerProfile','GetIndustries', 'GetJobTitle', 'GetQualification', 'GetTrainingTitle', 'CheckPercentage'));
            }else{
                $TotalViews = new TotalViews();
                $TotalViews->candidate_id = $GetTraineeId;
                $TotalViews->training_id = $GetTrainingId->id;
                $TotalViews->total_views = 1;
                $TotalViews->views_type = 1;

                $TotalViews->save();
            }
        }
        
        
        $GetTrainingBySlug = DB::select("SELECT training.id, training_title.title, employer.website, employer.mobile, employer.email, employer.profile_pic, employer.company_name, employer.city, training.duration, training.created_at, training.description, training.start_date, training.stipend, training.last_date_of_application, training.training_completion  FROM training, employer, training_title WHERE training.employer_id = employer.id AND training_title.id = training.title AND training.slug = '$slug'");

        $GetTotalViewsCount = TotalViews::where('training_id', $GetTrainingId->id)
                                        ->get();

        $CheckTrainingApplied = JobsApplied::where('candidate_id', $GetTraineeId)
                                        ->where('training_id', $GetTrainingId->id)
                                        ->first();

        $CheckApplicantsCount = JobsApplied::where('training_id', $GetTrainingId->id)
                                            ->get();

        // echo json_encode($GetJobsBySlug);
        // exit;
        $CheckFavourites = FavouriteModel::where('candidate_id', $GetTraineeId)
                                            ->where('training_id', $GetTrainingId->id)
                                            ->where('type', 2)
                                            ->first();

        $TraineeProfile = CandidateProfileModel::where("candidate_id", $GetTraineeId)->first();

        if(Auth::guard('trainee')->check()){
            $GetFavouriteTrainings = DB::select("SELECT employer.profile_pic, training.title, training.duration, employer.slug AS CompanySlug, training.slug, employer.company_name FROM favourite_jobs, training, employer WHERE favourite_jobs.candidate_id = $GetTraineeId AND favourite_jobs.training_id = training.id AND training.employer_id = employer.id AND favourite_jobs.status = 1 AND favourite_jobs.type = 2");
        }

        $GetSimilarTrainings = DB::select("SELECT employer.profile_pic, training.title, training.duration, employer.slug AS CompanySlug, training.slug, employer.company_name, employer.state, employer.city FROM training, employer WHERE training.slug != '$GetTrainingSegment' AND training.employer_id = employer.id ORDER BY training.id DESC LIMIT 4");

        $GetStipend = TrainingCompletionModel::get();

        return view("UI.layouts.training_details", compact('title', 'GetTrainingBySlug', 'GetTotalViewsCount', 'CheckTrainingApplied', 'CheckApplicantsCount', 'CheckFavourites', 'TraineeProfile', 'GetFavouriteTrainings', 'GetSimilarTrainings', 'GetStipend', 'Profile', 'EmployerProfile','GetIndustries', 'GetJobTitle', 'GetQualification', 'GetTrainingTitle', 'CheckPercentage'));
    }


    public function ApplyTrainings(Request $request){
        $GetTraineeId = Session::get("TraineeId");
        $GetTrainingId = $request->TrainingId;

        $CheckJobsApplied = JobsApplied::where('candidate_id', $GetTraineeId)
                                        ->where('training_id', $GetTrainingId)
                                        ->first();
        // echo json_encode($GetTrainingId);
        // exit;
        if($CheckJobsApplied != null){
            return response()->json(array(
                "error" => TRUE,
                "message" => "You have already applied this training..."
            ));
        }else{
            $JobsApplied = new JobsApplied();
            $JobsApplied->candidate_id = $GetTraineeId;
            $JobsApplied->training_id = $GetTrainingId;
    
            $ApplyJobs = $JobsApplied->save();
            if($ApplyJobs){
                return response()->json(array(
                    "error" => FALSE,
                    "message" => "Training applied successfully"
                ));
            }else{
                return response()->json(array(
                    "error" => TRUE,
                    "message" => "Failed"
                ));
            }
        }
    }

    



    


    // Trainee Selected Trainings
    public function ViewAllSelectedTrainingsByTrainees(){
        $title = "Selected Trainees";
        $GetTraineeId = Session::get('TraineeId');

        $GetCandidates = DB::select("SELECT training.id, candidate.name, training.title, training.duration, training.slug, selected_trainee.training_start_date, selected_trainee.training_end_date, selected_trainee.training_status, employer.slug AS CompanySlug, training.slug AS TrainingSlug FROM selected_trainee, candidate, employer, training WHERE selected_trainee.candidate_id = candidate.id AND selected_trainee.candidate_id = $GetTraineeId AND selected_trainee.training_id = training.id AND candidate.type = 2 AND selected_trainee.training_status = 0");

        // $GetRatings = DB::select("SELECT DISTINCT(reviews.review_stars) FROM reviews, candidate, training WHERE reviews.candidate_id = $GetTraineeId AND reviews.training_id = training.id AND candidate.type = 2");
        // echo json_encode($GetRatings);
        // exit;
        $TraineeProfile = CandidateProfileModel::where("candidate_id", $GetTraineeId)->first();
        $GetFavouriteTrainings = DB::select("SELECT employer.profile_pic, training.title, training.duration, employer.slug AS CompanySlug, training.slug, employer.company_name FROM favourite_jobs, training, employer WHERE favourite_jobs.candidate_id = $GetTraineeId AND favourite_jobs.training_id = training.id AND training.employer_id = employer.id AND favourite_jobs.status = 1 AND favourite_jobs.type = 2");

        return view("UI.layouts.trainee_selected_trainings", compact('title', 'GetCandidates', 'TraineeProfile', 'GetFavouriteTrainings'));
    }

    public function ViewAllCompletedTrainingsByTrainees(){
        $title = "Completed Trainings";
        $GetTraineeId = Session::get('TraineeId');

        $GetCandidates = DB::select("SELECT training.id, candidate.name, training.title, training.duration, training.slug, selected_trainee.training_start_date, selected_trainee.training_end_date, selected_trainee.training_status, employer.slug AS CompanySlug, training.slug AS TrainingSlug FROM selected_trainee, candidate, employer, training WHERE selected_trainee.candidate_id = candidate.id AND selected_trainee.candidate_id = $GetTraineeId AND selected_trainee.training_id = training.id AND candidate.type = 2 AND selected_trainee.training_status = 1");

        $GetRatings = DB::select("SELECT DISTINCT(reviews.review_stars) FROM reviews, candidate, training WHERE reviews.candidate_id = $GetTraineeId AND reviews.training_id = training.id AND candidate.type = 2");
        // echo json_encode($GetRatings);
        // exit;
        $TraineeProfile = CandidateProfileModel::where("candidate_id", $GetTraineeId)->first();

        $GetFavouriteTrainings = DB::select("SELECT employer.profile_pic, training.title, training.duration, employer.slug AS CompanySlug, training.slug, employer.company_name FROM favourite_jobs, training, employer WHERE favourite_jobs.candidate_id = $GetTraineeId AND favourite_jobs.training_id = training.id AND training.employer_id = employer.id AND favourite_jobs.status = 1 AND favourite_jobs.type = 2");

        return view("UI.layouts.trainee_completed_trainings", compact('title', 'GetCandidates', 'GetRatings', 'TraineeProfile', 'GetFavouriteTrainings'));
    }

    
    public function GetStudentDataByCertificates(){
        $title = "Certificates";
        $GetTraineeId = Session::get('TraineeId');

        $GetStudentDataByCertificates = DB::select("SELECT DISTINCT candidate.name, employer.company_name, employer.profile_pic,  training_title.title, jobs_applied.training_end_date FROM jobs_applied, candidate, employer, training_title WHERE jobs_applied.candidate_id = candidate.id AND jobs_applied.candidate_id = $GetTraineeId AND jobs_applied.training_id = training_title.id AND jobs_applied.candidate_id = $GetTraineeId AND candidate.type = 2 AND jobs_applied.employer_id = employer.id AND  jobs_applied.training_completion_status = 1");
        

        // echo json_encode($GetStudentDataByCertificates);
        // exit;
        return view('UI.layouts.certificates', compact('title', 'GetStudentDataByCertificates'));
    }



    public function LoadmoreTrainings(Request $request){
        $GetTrainingId = $request->TrainingId;
        
        $GetResults = "";

        $GetTrainings = DB::select("SELECT employer.profile_pic, training.slug, training.title, employer.company_name, employer.country, employer.city, training.duration, employer.slug AS CompanySlug, training.id FROM training, employer WHERE training.employer_id = employer.id AND training.id > $GetTrainingId ORDER BY training.created_at ASC LIMIT 1");

        if ($GetTrainings != null){
            foreach($GetTrainings as $Trainings){
                $GetResults .= "<div class='job-listing wtabs'>
                                    <div class='job-title-sec'>
                                        <div class='c-logo'> <img src='employer_profile/".$Trainings->profile_pic."' alt='' /> </div>
                                    <h3><a href='/TrainingDetails/".$Trainings->CompanySlug."/".$Trainings->slug."' target='_blank' title=''>".$Trainings->title."</a></h3>
                                        <span>".$Trainings->company_name."</span>
                                        <div class='job-lctn'><i class='la la-map-marker'></i>".$Trainings->country.", ".$Trainings->city."</div>
                                    </div>
                                    <div class='job-style-bx'>
                                    <span class='job-is ft'>".$Trainings->duration." Days</span>
                                        <i>5 months ago</i>
                                    </div>
                                </div>";
            }
            
            $GetResults .= "<div class='col-lg-12' id='remove_training_load_more'>
                                <div class='browse-all-cat'>
                                    <div class='text-center' id='ApplyBtnLoader' style='display: none;'>
                                        <img class='applyJobsLoader' src='{{URL::asset('UI/apply_btn_loader.gif')}}' alt=''>
                                    </div>
                                    <a href='javascript:void(0);' data-id='".$Trainings->id."' id='LoadmoreTrainings' title='' class='style2'>Load more trainings...</a>
                                </div>
                            </div>";

            echo $GetResults;
        }
    }


    public function TraineeDashboard(){
        $title = "Trainee Dashboard";
        $GetEmail = Session::get('TraineeEmail');
        $GetTraineeId = Session::get('TraineeId');
        $TraineeProfile = CandidateProfileModel::where("candidate_id", $GetTraineeId)->first();
        if($TraineeProfile){
            $GetCandidateId = Session::get('TraineeId');
        }else{
            $GetCandidateId = "0";
        }
        // echo $GetCandidateId;
        // exit;
        $Profile = CandidateModel::where("id", $GetTraineeId)->first();
        
        $GetTrainings = DB::select("SELECT employer.profile_pic, training.slug, training_title.title, employer.company_name, employer.country, employer.city, training.duration, training.start_date, training.stipend, training.training_completion, training.description, training.created_at, employer.slug AS CompanySlug, training.id FROM training, employer, training_title WHERE training.employer_id = employer.id AND training_title.id = training.title ORDER BY training.id");

        $GetStipend = TrainingCompletionModel::get();

        // $GetFavouriteJobs = DB::select("SELECT employer.profile_pic, jobs.title, employer.slug AS CompanySlug, jobs.slug, employer.company_name FROM favourite_jobs, jobs, employer WHERE favourite_jobs.candidate_id = $GetCandidateId AND favourite_jobs.jobs_id = jobs.id AND jobs.employer_id = employer.id AND favourite_jobs.status = 1 AND favourite_jobs.type = 1");

        $GetTrainingCity = DB::select("SELECT DISTINCT city FROM employer");

        return view("UI.layouts.trainee_dashboard", compact('title', "Profile", 'TraineeProfile', 'GetFavouriteJobs', 'GetTrainings', 'GetStipend', 'GetTrainingCity'));
    }


    public function FresherJobs(){
        $title = "Fresher Jobs";
        $GetEmail = Session::get('TraineeEmail');
        $GetTraineeId = Session::get('TraineeId');
        $TraineeProfile = CandidateProfileModel::where("candidate_id", $GetTraineeId)->first();
        if($TraineeProfile){
            $GetCandidateId = Session::get('TraineeId');
        }else{
            $GetCandidateId = "0";
        }
        // echo $GetCandidateId;
        // exit;
        $Profile = CandidateModel::where("id", $GetTraineeId)->first();
        
        $GetFresherJobs = DB::select("SELECT DISTINCT jobs.id, job_title.name AS title, employer.website, employer.mobile, employer.email, employer.profile_pic, jobs.total_positions, employer.company_name, jobs.city , jobs.salary_min, jobs.salary_max, jobs.job_type, jobs.created_at, jobs.job_description, jobs.experience, jobs.qualification, jobs.slug, employer.slug AS CompanySlug FROM jobs, employer, job_title WHERE jobs.employer_id = employer.id AND job_title.id = jobs.title AND jobs.employee_type = 1");

        $GetStipend = TrainingCompletionModel::get();

        // $GetFavouriteJobs = DB::select("SELECT employer.profile_pic, jobs.title, employer.slug AS CompanySlug, jobs.slug, employer.company_name FROM favourite_jobs, jobs, employer WHERE favourite_jobs.candidate_id = $GetCandidateId AND favourite_jobs.jobs_id = jobs.id AND jobs.employer_id = employer.id AND favourite_jobs.status = 1 AND favourite_jobs.type = 1");

        return view("UI.layouts.fresher_jobs", compact('title', "Profile", 'TraineeProfile', 'GetFavouriteJobs', 'GetFresherJobs', 'GetStipend'));
    }


    
    public function UpdatePassword(Request $request){
        $GetCandidateId = Session::get('TraineeId');
        $GetOldPassword = CandidateModel::where('id', $GetCandidateId)->first();

        $CheckPassword = Hash::check($request->OldPassword, $GetOldPassword->password);
        // echo json_encode($CheckPassword);
        // exit;
        $GetNewPassword = $request->NewPassword;
        $GetVerifyPassword = $request->VerifyPassword;


        if($GetNewPassword != $GetVerifyPassword){
            return response()->json(array(
                "error"=>TRUE,
                "message" => "Password mismatch"
            ));
        }else if($CheckPassword == false){
            return response()->json(array(
                "error"=>TRUE,
                "message" => "Old password is wrong"
            ));
        }else if($CheckPassword == true){
            $GetOldPassword->password = Hash::make($request->VerifyPassword);
            $GetOldPassword->save();
            return response()->json(array(
                "error"=>FALSE,
                "message" => "Password updated successfully"
            ));
        }
    }


    public function UpdateProfilePic(Request $request){
        if(Auth::guard('trainee')->check()){
            $GetTraineeId = Session::get('TraineeId');
        }elseif(Auth::guard('ca_article')->check()){
            $GetTraineeId = Session::get('CaArticleId');
        }
        

        $UpdateCandidateProfile = CandidateProfileModel::where('candidate_id', $GetTraineeId)->first();   
        
        $extension = $request->file('file')->getClientOriginalExtension();
        $dir = 'trainee_profile/';
        $filename = uniqid() . '_' . time() . '.' . $extension;
        $request->file('file')->move($dir, $filename);

        $UpdateCandidateProfile->profile_pic = $filename;

        if($UpdateCandidateProfile->profile_pic != null){
            $UpdateCandidateProfile->profile_completion = $UpdateCandidateProfile->profile_completion+5;
        }
        

        $UpdatedCandidateProfile = $UpdateCandidateProfile->save();
        if($UpdatedCandidateProfile){
            return response()->json(array(
                            "error"=>FALSE,
                            "message"=>"Profile updated successfully"
                        ));
        }else{
            return response()->json(array(
                            "error"=>TRUE,
                            "message"=>"Failed"
                        ));
        }
    }



    public function AddTraineeProfile(Request $request){

        if(Auth::guard('ca_article')->check()){
            $GetTraineeId = Session::get('CaArticleId');
        }elseif(Auth::guard('trainee')->check()){
            $GetTraineeId = Session::get('TraineeId');
        }
        // echo $GetTraineeId;
        // exit;
        // $GetTraineeId = Session::get('TraineeId');
        $CandidateProfile = new CandidateProfileModel();

        $UpdateCandidateProfile = CandidateProfileModel::where('candidate_id', $GetTraineeId)->first();
        
        if($UpdateCandidateProfile != null ){
            // echo json_encode($UpdateCandidateProfile->experience);
            // exit;

            if($UpdateCandidateProfile->desired_location ==null){
                $UpdateCandidateProfile->profile_completion = $UpdateCandidateProfile->profile_completion+15;
            }

            $UpdateCandidateProfile->desired_location = $request->desired_location;
            
            
            $UpdatedCandidateProfile = $UpdateCandidateProfile->save();
            if($UpdatedCandidateProfile){
                return response()->json(array(
                                "error"=>FALSE,
                                "message"=>"Profile updated successfully"
                            ));
            }else{
                return response()->json(array(
                                "error"=>TRUE,
                                "message"=>"Failed"
                            ));
            }
        }else{
            $CandidateProfile->candidate_id = $GetTraineeId;
            $CandidateProfile->desired_location = $request->desired_location;
    
            $UpdateCandidateProfile = $CandidateProfile->save();
            if($UpdateCandidateProfile){
                return response()->json(array(
                                "error"=>FALSE,
                                "message"=>"Profile updated successfully"
                            ));
            }else{
                return response()->json(array(
                                "error"=>TRUE,
                                "message"=>"Failed"
                            ));
            }
        }
        
    }



    public function InsertOrUpdateCV(Request $request){

        if(Auth::guard('trainee')->check()){
            $GetTraineeId = Session::get('TraineeId');
        }elseif(Auth::guard('ca_article')->check()){
            $GetTraineeId = Session::get('CaArticleId');
        }

        $CandidateCV = new CandidateResume();

        $GetCVId = $request->cv_id;

        // echo json_encode($GetCVId);
        // exit;

        $UpdateCandidateCV = CandidateResume::where('id', $GetCVId)->first();

        $UpdateCandidateProfile = CandidateProfileModel::where('candidate_id', $GetTraineeId)->first();
        // echo json_encode($GetCVId);
        // exit;
        if($UpdateCandidateCV["cv"] == null){
            $UpdateCandidateProfile->profile_completion = $UpdateCandidateProfile->profile_completion+20;

            $UpdateCandidateProfile->save();
        }

        if($UpdateCandidateCV != null){
            $File = $request->cv;
            // $UpdateCandidateProfile = CandidateProfileModel::findOrFail($GetCandidateId);
            // echo json_encode($File);
            // exit;
            if($File == null){
                $filename = $UpdateCandidateCV->cv;
            }else{
                
                if(Auth::guard('trainee')->check()){
                    $extension = $request->file('cv')->getClientOriginalExtension();
                    $dir = 'trainee_cv/';
                    $filename = uniqid() . '_' . time() . '.' . $extension;
                    $request->file('cv')->move($dir, $filename);

                }elseif(Auth::guard('ca_article')->check()){
                    $extension = $request->file('cv')->getClientOriginalExtension();
                    $dir = 'article_cv/';
                    $filename = uniqid() . '_' . time() . '.' . $extension;
                    $request->file('cv')->move($dir, $filename);
                }

                

            }
            // echo json_encode($filename);
            // exit;

            $UpdateCandidateCV->cv = $filename;
            
            $UpdatedCandidateCV = $UpdateCandidateCV->save();
            if($UpdatedCandidateCV){
                return response()->json(array(
                    "error"=>FALSE,
                    "message"=>"CV updated successfully"
                ));
            }else{
                return response()->json(array(
                    "error"=>TRUE,
                    "message"=>"Failed."
                ));
            }
        }else{
           
            $extension = $request->file('cv')->getClientOriginalExtension();
            $dir = 'trainee_cv/';
            $filename = uniqid() . '_' . time() . '.' . $extension;
            $request->file('cv')->move($dir, $filename);

            $CandidateCV->candidate_id = $GetTraineeId;
            $CandidateCV->cv = $filename;
            $CandidateCV->type = "4";
    
            $AddCandidateCV = $CandidateCV->save();
            if($AddCandidateCV){
                return response()->json(array(
                    "error"=>FALSE,
                    "message"=>"CV updated successfully"
                ));
            }else{
                return response()->json(array(
                    "error"=>TRUE,
                    "message"=>"Failed."
                ));
            }
        }
    }


    public function InsertOrUpdateCVTitle(Request $request){
        $GetTraineeId = Session::get('TraineeId');
        $CandidateCV = new CandidateResume();

        $GetCVId = $request->cv_id;
        $UpdateCandidateCV = CandidateResume::where('id', $GetCVId)->first();

        $UpdateCandidateProfile = CandidateProfileModel::where('candidate_id', $GetTraineeId)->first();

        if($UpdateCandidateCV["title"] == null){
            $UpdateCandidateProfile->profile_completion = $UpdateCandidateProfile->profile_completion+15;

            $UpdateCandidateProfile->save();
        }

        if($UpdateCandidateCV != null){

            $UpdateCandidateCV->title = $request->resume_title;
    
            $UpdatedCandidateCV = $UpdateCandidateCV->save();
            if($UpdatedCandidateCV){
                return response()->json(array(
                    "error"=>FALSE,
                    "message"=>"CV title updated successfully"
                ));
            }else{
                return response()->json(array(
                    "error"=>TRUE,
                    "message"=>"Failed."
                ));
            }
        }else{
            $UpdateCandidateProfile = CandidateProfileModel::where('candidate_id', $GetTraineeId)->first();
            
            $UpdateCandidateProfile->profile_completion = $UpdateCandidateProfile->profile_completion+5;

            $UpdateCandidateProfile->save();

            $CandidateCV->candidate_id = $GetTraineeId;
            $CandidateCV->title = $request->resume_title;
    
            $AddCandidateCV = $CandidateCV->save();
            if($AddCandidateCV){
                return response()->json(array(
                    "error"=>FALSE,
                    "message"=>"CV title updated successfully"
                ));
            }else{
                return response()->json(array(
                    "error"=>TRUE,
                    "message"=>"Failed."
                ));
            }
        }
    }


    // Education Details
    public function InsertEducation(Request $request){

        if(Auth::guard('trainee')->check()){
            $GetTraineeId = Session::get('TraineeId');
        }elseif(Auth::guard('ca_article')->check()){
            $GetTraineeId = Session::get('CaArticleId');
        }

        $CandidateResumeById = CandidateResume::where('candidate_id', $GetTraineeId)
                                ->where('type', 1)->first();

        $UpdateCandidateProfile = CandidateProfileModel::where('candidate_id', $GetTraineeId)->first();

        if($CandidateResumeById == null){
            $UpdateCandidateProfile->profile_completion = $UpdateCandidateProfile->profile_completion+25;

            $UpdateCandidateProfile->save();
        }

        
        $CandidateResume = new CandidateResume();
        $CandidateResume->candidate_id = $GetTraineeId;
        $CandidateResume->title = $request->course_name;
        $CandidateResume->department = $request->board;
        $CandidateResume->name = $request->school_clg;
        $CandidateResume->from_year = $request->passed_out_year;
        $CandidateResume->percentage = $request->percentage;
        $CandidateResume->type = "1";

        $InsertEducation = $CandidateResume->save();
        if($InsertEducation){
            return response()->json(array(
                "error"=>FALSE,
                "message"=>"Education updated successfully"
            ));
        }else{
            return response()->json(array(
                "error"=>TRUE,
                "message"=>"Failed"
            ));
        }
    }

    public function DeleteEducation($Id){
        

        if(Auth::guard('trainee')->check()){
            $GetTraineeId = Session::get('TraineeId');
        }elseif(Auth::guard('ca_article')->check()){
            $GetTraineeId = Session::get('CaArticleId');
        }elseif(Auth::guard('candidate')->check()){
            $GetTraineeId = Session::get('CandidateId');
        }
        

        $CheckExperience = CandidateResume::where('candidate_id', $GetTraineeId)
                                            ->where('type', 1)
                                            ->first();

        $UpdateCandidateProfile = CandidateProfileModel::where('candidate_id', $GetTraineeId)->first();

        if($CheckExperience == null){
            $UpdateCandidateProfile->profile_completion = $UpdateCandidateProfile->profile_completion-25;

            $UpdateCandidateProfile->save();
        }

        $DeleteEducation = CandidateResume::where('id', $Id)->delete();
        
        if($DeleteEducation){
            return response()->json(array(
                "error"=>FALSE,
                "message"=>"Education deleted successfully"
            ));
        }else{
            return response()->json(array(
                "error"=>TRUE,
                "message"=>"Failed"
            ));
        }
    }

    public function UpdatePersonalDetails(Request $request){
        $CandidateProfile = new CandidateProfileModel();

        if(Auth::guard('trainee')->check()){
            $GetTraineeId = Session::get('TraineeId');
        }elseif(Auth::guard('ca_article')->check()){
            $GetTraineeId = Session::get('CaArticleId');
        }

        $UpdateCandidateProfile = CandidateProfileModel::where("candidate_id", $GetTraineeId)->first();

        if($UpdateCandidateProfile->date_of_birth == null AND $UpdateCandidateProfile->gender == null AND $UpdateCandidateProfile->marital_status == null AND $UpdateCandidateProfile->father_name == null AND $UpdateCandidateProfile->address == null){
            $UpdateCandidateProfile->profile_completion = $UpdateCandidateProfile->profile_completion+30;
        }


        if($UpdateCandidateProfile != null){
            $UpdateCandidateProfile->candidate_id = $GetTraineeId;
            $UpdateCandidateProfile->date_of_birth = date('Y-m-d', strtotime($request->dob));
            $UpdateCandidateProfile->gender = $request->gender;
            $UpdateCandidateProfile->marital_status = $request->marital_status;
            $UpdateCandidateProfile->father_name = $request->father_name;
            $UpdateCandidateProfile->address = $request->address;
            $UpdateCandidateProfile->state = $request->state;
            $UpdateCandidateProfile->city = $request->city;
            $UpdateCandidateProfile->zip_code = $request->zipcode;
            
            $UpdatedCareerProfile = $UpdateCandidateProfile->save();
            if($UpdatedCareerProfile){
                return response()->json(array(
                    "error"=>FALSE,
                    "message"=>"Personal details updated successfully"
                ));
            }else{
                return response()->json(array(
                    "error"=>TRUE,
                    "message"=>"Failed."
                ));
            }
        }else{
            $CandidateProfile->candidate_id = $GetTraineeId;
            $CandidateProfile->date_of_birth = date('Y-m-d', strtotime($request->dob));
            $CandidateProfile->gender = $request->gender;
            $CandidateProfile->marital_status = $request->marital_status;
            $CandidateProfile->father_name = $request->father_name;
            $CandidateProfile->address = $request->address;
            $InsertCandidateProfile = $CandidateProfile->save();
            if($InsertCandidateProfile){
                return response()->json(array(
                    "error"=>FALSE,
                    "message"=>"Personal details updated successfully"
                ));
            }else{
                return response()->json(array(
                    "error"=>TRUE,
                    "message"=>"Failed."
                ));
            }
        }
    }

    public function AppliedTraineeDetails($id, $GetCandidateId, $slug){
        $title = "Trainee Details";
        $GetEmployerId = Session::get("EmployerId");
        // $GetCandidateId = CandidateModel::where('slug', $slug)->first();

        $UpdateEmployeId = DB::select("SELECT jobs_applied.id FROM jobs_applied, candidate WHERE jobs_applied.training_id = $id AND jobs_applied.candidate_id = candidate.id");

        $GetAppliedId = "";

        foreach($UpdateEmployeId as $UpdatedEmpId){
            $GetAppliedId = $UpdatedEmpId->id;
        }

        // echo json_encode($GetAppliedId);
        // exit;

        $GetUpdateRecruiterStatus = JobsApplied::where('id', $GetAppliedId)->first();

        $GetUpdateRecruiterStatus->employer_id = $GetEmployerId;
        $GetUpdateRecruiterStatus->recruiter_status = 1;
        $GetUpdateRecruiterStatus->application_view_date = date('Y-m-d');
        $GetUpdateRecruiterStatus->save();


        // echo json_encode($GetUpdateRecruiterStatus);
        // exit;

        $GetCandidateDetails  =  DB::table('candidate')
                                ->join('candidate_profile', 'candidate.id', '=', 'candidate_profile.candidate_id')
                                ->where('candidate.id', $GetCandidateId)
                                ->first();

        $GetProfessionalDetails = DB::table('candidate')
                                ->join('resume', 'candidate.id', '=', 'resume.candidate_id')
                                ->where('resume.worked_till', '=', 1)
                                ->where('resume.candidate_id', $GetCandidateId)
                                ->first();

        $GetEducation = DB::table('resume')
                        ->where('candidate_id', $GetCandidateId)
                        ->where('type', 1)
                        ->get();


        $GetCV = DB::table('resume')
                        ->where('resume.candidate_id', '=', $GetCandidateId)
                        ->where('resume.type', '=', 4)
                        ->first();

        $EmployerProfile = EmployerModel::where("id", $GetEmployerId)->first();

        $CheckShortlisted = ShortlistCandidatesModel::where('candidate_id', $GetCandidateId)
                                        ->where('employer_id', $GetEmployerId)
                                        ->first();

        return view("UI.layouts.trainee_details", compact('title', 'GetCandidateDetails', 'GetEducation', 'GetCV', 'CheckShortlisted', 'EmployerProfile', 'GetProfessionalDetails'));
    }


    public function DownloadCV($CandidateId, $TrainingId){
        $GetEmployerId = Session::get("EmployerId");

        $GetUpdateRecruiterStatus = JobsApplied::where('candidate_id', $CandidateId)
                                    // ->where('employer_id', $GetEmployerId)
                                    ->where('training_id', $TrainingId)
                                    ->first();

        // if($GetUpdateRecruiterStatus->application_view_date == null){
        //     echo "test";
        // }

        // // echo json_encode($GetUpdateRecruiterStatus->application_view_date);
        // exit;
        $GetUpdateRecruiterStatus->employer_id = $GetEmployerId;

        if($GetUpdateRecruiterStatus->recruiter_status == null){
            $GetUpdateRecruiterStatus->recruiter_status = 1;
        }

        
        $GetUpdateRecruiterStatus->download_cv_status = 1;

        if($GetUpdateRecruiterStatus->application_view_date == null){
            $GetUpdateRecruiterStatus->application_view_date = date('Y-m-d');
        }
        
        $GetUpdateRecruiterStatus->download_cv_date = date('Y-m-d');
        $UpdateStatus = $GetUpdateRecruiterStatus->save();

        if($UpdateStatus){
            return response()->json(array(
                "error"=>FALSE,
                "message"=>"Updated successfully"
            ));
        }else{
            return response()->json(array(
                "error"=>TRUE,
                "message"=>"Updated failed"
            ));
        }
    }


    public function ViewAllTrainings(){
        $title = "View all trainings";
        $GetEmployerId = Session::get("EmployerId");
        $EmployerProfile = EmployerModel::where("id", $GetEmployerId)->first();

        $GetAppliedTrainees = DB::select("SELECT candidate.name, candidate_profile.profile_pic,jobs_applied.created_at, training.title  FROM jobs_applied, candidate, candidate_profile, training WHERE jobs_applied.candidate_id = candidate.id AND candidate_profile.candidate_id = candidate.id AND training.id = jobs_applied.training_id ");


        return view("UI.layouts.view_all_trainees", compact('title', 'EmployerProfile', 'GetAppliedTrainees'));
    }



    public function FilterDatePostedTraining(Request $request){
        $GetDatePosted = $request->date_posted;

        $GetDataByDatePosted = [];

        if($GetDatePosted == 1){
            $GetDataByDatePosted[] =DB::table('training')
            ->select('employer.profile_pic', 'training.slug', 'training_title.title', 'employer.company_name', 'employer.country', 'employer.city', 'training.duration', 'training.start_date', 'training.stipend', 'training.training_completion', 'training.description', 'training.created_at', 'employer.slug AS CompanySlug', 'training.id', 'training_completion.training_completion')
            ->join('employer', 'employer.id', '=', 'training.employer_id')
            ->join('training_title', 'training_title.id', '=', 'training.title')

            ->whereRaw('training.created_at >= CURRENT_TIMESTAMP - INTERVAL 1 HOUR')

            ->get();
        }elseif($GetDatePosted == 2){

            $GetDataByDatePosted[] =DB::table('training')
            ->select('employer.profile_pic', 'training.slug', 'training_title.title', 'employer.company_name', 'employer.country', 'employer.city', 'training.duration', 'training.start_date', 'training.stipend', 'training.training_completion', 'training.description', 'training.created_at', 'employer.slug AS CompanySlug', 'training.id', 'training_completion.training_completion')
            ->join('employer', 'employer.id', '=', 'training.employer_id')
            ->join('training_title', 'training_title.id', '=', 'training.title')

            ->whereRaw('training.created_at >= CURRENT_TIMESTAMP - INTERVAL 24 HOUR')

            ->get();
        }elseif($GetDatePosted == 3){

            $GetDataByDatePosted[] =DB::table('training')
            ->select('employer.profile_pic', 'training.slug', 'training_title.title', 'employer.company_name', 'employer.country', 'employer.city', 'training.duration', 'training.start_date', 'training.stipend', 'training.training_completion', 'training.description', 'training.created_at', 'employer.slug AS CompanySlug', 'training.id')
            ->join('employer', 'employer.id', '=', 'training.employer_id')
            ->join('training_title', 'training_title.id', '=', 'training.title')

            ->whereRaw('training.created_at >= DATE_ADD(CURDATE(), INTERVAL -7 DAY)')

            ->get();
        }elseif($GetDatePosted == 4){

            $GetDataByDatePosted[] =DB::table('training')
            ->select('employer.profile_pic', 'training.slug', 'training_title.title', 'employer.company_name', 'employer.country', 'employer.city', 'training.duration', 'training.start_date', 'training.stipend', 'training.training_completion', 'training.description', 'training.created_at', 'employer.slug AS CompanySlug', 'training.id')
            ->join('employer', 'employer.id', '=', 'training.employer_id')
            ->join('training_title', 'training_title.id', '=', 'training.title')

            ->orderBy('training.created_at', 'DESC')

            ->get();
        }


        $GetPostedOn = "";
        $GetTrainingResults = "";

        $GetTrainingDuration = "";

        $GetStipend = "";
        $GetTrainingCompletion = "";

        foreach($GetDataByDatePosted as $GetData){
            foreach($GetData as $Trainings){
                $now = new DateTime;
            $full = false;	
            $ago = new DateTime($Trainings->created_at);
            $diff = $now->diff($ago);

            $diff->w = floor($diff->d / 7);
            $diff->d -= $diff->w * 7;

            $string = array(
            'y' => 'year',
            'm' => 'month',
            'w' => 'week',
            'd' => 'day',
            'h' => 'hour',
            'i' => 'minute',
            's' => 'second',
            );

            foreach ($string as $k => &$v) {
            if ($diff->$k) {
            $v = $diff->$k . ' ' . $v . ($diff->$k > 1 ? 's' : '');
            } else {
            unset($string[$k]);
            }
            }

            if (!$full) $string = array_slice($string, 0, 1);
            $GetPostedOn =  $string ? implode(', ', $string) . ' ago' : 'just now';


            // if($Trainings->duration == 1){
            //     $GetTrainingDuration = "30 Days";
            // }else{
            //     $GetTrainingDuration = "60 Days";
            // }

            $num = $Trainings->stipend;
            $units = ['', 'K', 'M'];
            for($i = 0; $num>=1000;$i++){
                $num /= 1000;
            }
            $GetStipend = round($num, 1).$units[$i];

            // $GetStipend = $Trainings->training_completion;
                
            
                      
            if($Trainings->training_completion == 1){
                $GetTrainingCompletion = "Certification";
            }else{
                $GetTrainingCompletion = "Full Time Job";
            }


                $GetTrainingResults = "<div class='job-listings-sec no-border text-center'>
                <div class='job-listing wtabs recommented-jobs'>
                    <div class='job-title-sec recommended-jobs-pad-left'>
                        <h3><a href='/TrainingDetails/".$Trainings->CompanySlug."/".$Trainings->slug."' target='_blank'>".$Trainings->title."</a></h3>
                        <p class='recommended-jobs-company-name'>".$Trainings->company_name."</p>
                        <div class='job-lctn job-attributes-recommended-jobs'><i class='fa fa-briefcase'></i>".date('d M Y', strtotime($Trainings->start_date))."</div>
                        <div class='job-lctn job-attributes-recommended-jobs'><i class='fa fa-clock-o'></i>
                            ".$Trainings->duration."
                        </div>
                        <div class='job-lctn job-attributes-recommended-jobs'><i class='fa fa-briefcase' ></i> 
                            ".$GetStipend."
                                / Month
                        </div>
                        <div class='job-lctn job-attributes-recommended-jobs'><i class='fa fa-industry'></i>
                            ".$GetTrainingCompletion."
                        </div>
                    </div>
                    <p class='recommended-jobs-posted-on'>Posted on: 
                        ".$GetPostedOn."
                    </p>
                </div>
                </div>";

                if($GetTrainingResults){
                    echo $GetTrainingResults;
                }else{
                    echo json_encode("No Results Found...");
                }
            }
        }

    }


    public function ClearAllTrainings(Request $request){
        $GetDataByDatePosted = [];

        $GetDataByDatePosted[] =DB::table('training')
            ->select('employer.profile_pic', 'training.slug', 'training.title', 'employer.company_name', 'employer.country', 'employer.city', 'training.duration', 'training.start_date', 'training.stipend', 'training.training_completion', 'training.description', 'training.created_at', 'employer.slug AS CompanySlug', 'training.id', 'training_completion.training_completion')
            ->join('employer', 'employer.id', '=', 'training.employer_id')
            ->join('training_completion', 'training_completion.id', '=', 'training.stipend')

            ->orderBy('training.created_at', 'DESC')

            ->get();


        $GetPostedOn = "";
        $GetTrainingResults = "";

        $GetTrainingDuration = "";

        $GetStipend = "";
        $GetTrainingCompletion = "";

        foreach($GetDataByDatePosted as $GetData){
            foreach($GetData as $Trainings){
                $now = new DateTime;
            $full = false;	
            $ago = new DateTime($Trainings->created_at);
            $diff = $now->diff($ago);

            $diff->w = floor($diff->d / 7);
            $diff->d -= $diff->w * 7;

            $string = array(
            'y' => 'year',
            'm' => 'month',
            'w' => 'week',
            'd' => 'day',
            'h' => 'hour',
            'i' => 'minute',
            's' => 'second',
            );

            foreach ($string as $k => &$v) {
            if ($diff->$k) {
            $v = $diff->$k . ' ' . $v . ($diff->$k > 1 ? 's' : '');
            } else {
            unset($string[$k]);
            }
            }

            if (!$full) $string = array_slice($string, 0, 1);
            $GetPostedOn =  $string ? implode(', ', $string) . ' ago' : 'just now';


            if($Trainings->duration == 1){
                $GetTrainingDuration = "30 Days";
            }else{
                $GetTrainingDuration = "60 Days";
            }

            // $num = $Trainings->stipend;
            // $units = ['', 'K', 'M'];
            // for($i = 0; $num>=1000;$i++){
            //     $num /= 1000;
            // }
            // $GetStipend = round($num, 1).$units[$i];

            $GetStipend = $Trainings->training_completion;
                
            
                      
            if($Trainings->training_completion == 1){
                $GetTrainingCompletion = "Certification";
            }else{
                $GetTrainingCompletion = "Full Time Job";
            }


                $GetTrainingResults = "<div class='job-listings-sec no-border text-center'>
                <div class='job-listing wtabs recommented-jobs'>
                    <div class='job-title-sec recommended-jobs-pad-left'>
                        <h3><a href='/TrainingDetails/".$Trainings->CompanySlug."/".$Trainings->slug."' target='_blank'>".$Trainings->title."</a></h3>
                        <p class='recommended-jobs-company-name'>".$Trainings->company_name."</p>
                        <div class='job-lctn job-attributes-recommended-jobs'><i class='fa fa-briefcase'></i>".date('d M Y', strtotime($Trainings->start_date))."</div>
                        <div class='job-lctn job-attributes-recommended-jobs'><i class='fa fa-clock-o'></i>
                            ".$GetTrainingDuration."
                        </div>
                        <div class='job-lctn job-attributes-recommended-jobs'><i class='fa fa-briefcase' ></i> 
                            ".$GetStipend."
                                / Month
                        </div>
                        <div class='job-lctn job-attributes-recommended-jobs'><i class='fa fa-industry'></i>
                            ".$GetTrainingCompletion."
                        </div>
                    </div>
                    <p class='recommended-jobs-posted-on'>Posted on: 
                        ".$GetPostedOn."
                    </p>
                </div>
                </div>";

                if($GetTrainingResults){
                    echo $GetTrainingResults;
                }else{
                    echo json_encode("No Results Found...");
                }
            }
        }

    }


    public function SortByTrainings(Request $request){
        $GetDatePosted = $request->sortby;

        $GetDataByDatePosted = [];

        if($GetDatePosted == 1){
            $GetDataByDatePosted[] =DB::table('training')
            ->select('employer.profile_pic', 'training.slug', 'training.title', 'employer.company_name', 'employer.country', 'employer.city', 'training.duration', 'training.start_date', 'training.stipend', 'training.training_completion', 'training.description', 'training.created_at', 'employer.slug AS CompanySlug', 'training.id', 'training_completion.training_completion')
            ->join('employer', 'employer.id', '=', 'training.employer_id')
            ->join('training_completion', 'training_completion.id', '=', 'training.stipend')

            ->orderBy('training.created_at', 'DESC')

            ->get();
        }elseif($GetDatePosted == 2){

            $GetDataByDatePosted[] =DB::table('training')
            ->select('employer.profile_pic', 'training.slug', 'training.title', 'employer.company_name', 'employer.country', 'employer.city', 'training.duration', 'training.start_date', 'training.stipend', 'training.training_completion', 'training.description', 'training.created_at', 'employer.slug AS CompanySlug', 'training.id', 'training_completion.training_completion')
            ->join('employer', 'employer.id', '=', 'training.employer_id')
            ->join('training_completion', 'training_completion.id', '=', 'training.stipend')

            ->orderBy('training.created_at', 'ASC')

            ->get();
        }


        $GetPostedOn = "";
        $GetTrainingResults = "";

        $GetTrainingDuration = "";

        $GetStipend = "";
        $GetTrainingCompletion = "";

        foreach($GetDataByDatePosted as $GetData){
            foreach($GetData as $Trainings){
                $now = new DateTime;
            $full = false;	
            $ago = new DateTime($Trainings->created_at);
            $diff = $now->diff($ago);

            $diff->w = floor($diff->d / 7);
            $diff->d -= $diff->w * 7;

            $string = array(
            'y' => 'year',
            'm' => 'month',
            'w' => 'week',
            'd' => 'day',
            'h' => 'hour',
            'i' => 'minute',
            's' => 'second',
            );

            foreach ($string as $k => &$v) {
            if ($diff->$k) {
            $v = $diff->$k . ' ' . $v . ($diff->$k > 1 ? 's' : '');
            } else {
            unset($string[$k]);
            }
            }

            if (!$full) $string = array_slice($string, 0, 1);
            $GetPostedOn =  $string ? implode(', ', $string) . ' ago' : 'just now';


            if($Trainings->duration == 1){
                $GetTrainingDuration = "30 Days";
            }else{
                $GetTrainingDuration = "60 Days";
            }

            // $num = $Trainings->stipend;
            // $units = ['', 'K', 'M'];
            // for($i = 0; $num>=1000;$i++){
            //     $num /= 1000;
            // }
            // $GetStipend = round($num, 1).$units[$i];

            $GetStipend = $Trainings->training_completion;
                
            
                      
            if($Trainings->training_completion == 1){
                $GetTrainingCompletion = "Certification";
            }else{
                $GetTrainingCompletion = "Full Time Job";
            }


                $GetTrainingResults = "<div class='job-listings-sec no-border text-center'>
                <div class='job-listing wtabs recommented-jobs'>
                    <div class='job-title-sec recommended-jobs-pad-left'>
                        <h3><a href='/JobDetails/companyslug/slug' target='_blank'>".$Trainings->title."</a></h3>
                        <p class='recommended-jobs-company-name'>".$Trainings->company_name."</p>
                        <div class='job-lctn job-attributes-recommended-jobs'><i class='fa fa-briefcase'></i>".date('d M Y', strtotime($Trainings->start_date))."</div>
                        <div class='job-lctn job-attributes-recommended-jobs'><i class='fa fa-clock-o'></i>
                            ".$GetTrainingDuration."
                        </div>
                        <div class='job-lctn job-attributes-recommended-jobs'><i class='fa fa-briefcase' ></i> 
                            ".$GetStipend."
                                / Month
                        </div>
                        <div class='job-lctn job-attributes-recommended-jobs'><i class='fa fa-industry'></i>
                            ".$GetTrainingCompletion."
                        </div>
                    </div>
                    <p class='recommended-jobs-posted-on'>Posted on: 
                        ".$GetPostedOn."
                    </p>
                </div>
                </div>";

                if($GetTrainingResults){
                    echo $GetTrainingResults;
                }else{
                    echo json_encode("No Results Found...");
                }
            }
        }

    }



    public function ChangeTrainingStatus(Request $request){
        $GetTrainingId = $request->TrainingId;

        $GetCheckTrainingStatus = $request->CheckTrainingStatus;

        if($GetCheckTrainingStatus == 1){
            $Training = TrainingModel::find($GetTrainingId);
            
            $Training->training_status = 1;
            $ChangeStatus = $Training->save();
            if($ChangeStatus){
                return response()->json(array(
                    "error"=>TRUE,
                    "message"=>"Internship closed successfully"
                ));
            }else{
                return response()->json(array(
                    "error"=>TRUE,
                    "message"=>"failed"
                ));
            }
        }else{
            $Training = TrainingModel::find($GetTrainingId);
            
            $Training->training_status = 0;
            $ChangeStatus = $Training->save();
            if($ChangeStatus){
                return response()->json(array(
                    "error"=>FALSE,
                    "message"=>"Internship live successfully"
                ));
            }else{
                return response()->json(array(
                    "error"=>TRUE,
                    "message"=>"failed"
                ));
            }
        }
    }



    public function ChangeTrainingCompletionStatus(Request $request){
        $GetTrainingId = $request->TrainingId;

        $GetCheckTrainingStatus = $request->CheckTrainingStatus;

        if($GetCheckTrainingStatus == 1){
            $Training = JobsApplied::where('candidate_id', $GetTrainingId)->first();
            
            $Training->training_completion_status = 1;
            $ChangeStatus = $Training->save();
            if($ChangeStatus){
                return response()->json(array(
                    "error"=>FALSE,
                    "message"=>"Sussfully updated"
                ));
            }else{
                return response()->json(array(
                    "error"=>TRUE,
                    "message"=>"failed"
                ));
            }
        }else{
            $Training = JobsApplied::where('candidate_id', $GetTrainingId)->first();
            
            $Training->training_completion_status = 0;
            $ChangeStatus = $Training->save();
            if($ChangeStatus){
                return response()->json(array(
                    "error"=>FALSE,
                    "message"=>"Sussfully updated"
                ));
            }else{
                return response()->json(array(
                    "error"=>TRUE,
                    "message"=>"failed"
                ));
            }
        }
    }


    public function ChangeAppliedTraineeTrainingStatus(Request $request){
        $GetTrainingId = $request->GetId;

        // $EndDate = $request->EndDate;
        $GetStatusId = $request->GetStatusId;

        if($GetStatusId == 1){
            $Training = JobsApplied::find($GetTrainingId);
            
            $Training->training_status = 1;
            // $Training->training_end_date = $EndDate;
            // $Training->training_completion_status = 0;
            $ChangeStatus = $Training->save();
            if($ChangeStatus){
                return response()->json(array(
                    "error"=>FALSE,
                    "message"=>"Candidate Approved"
                ));
            }else{
                return response()->json(array(
                    "error"=>TRUE,
                    "message"=>"failed"
                ));
            }
        }else{
            $Training = JobsApplied::find($GetTrainingId);
            
            $Training->training_status = 2;
            $ChangeStatus = $Training->save();
            if($ChangeStatus){
                return response()->json(array(
                    "error"=>TRUE,
                    "message"=>"Candidate Rejected"
                ));
            }else{
                return response()->json(array(
                    "error"=>TRUE,
                    "message"=>"failed"
                ));
            }
        }
    }



    public function ChangeTrainingEndDate(Request $request){
        $GetTrainingId = $request->GetId;

        $EndDate = $request->EndDate;
        // $GetStatusId = $request->GetStatusId;

        $Training = JobsApplied::find($GetTrainingId);
            
        // $Training->training_status = 1;
        $Training->training_end_date = $EndDate;
        // $Training->training_completion_status = 0;
        $ChangeStatus = $Training->save();
        if($ChangeStatus){
            return response()->json(array(
                "error"=>FALSE,
                "message"=>"Successfully"
            ));
        }else{
            return response()->json(array(
                "error"=>TRUE,
                "message"=>"failed"
            ));
        }
    }
}
