<?php

namespace App\Http\Controllers\UI;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Model\UI\EmployerModel;
use App\Model\UI\JobsModel;
use App\Model\UI\TotalViews;
use App\Model\UI\TrainingModel;
use App\Model\UI\JobsApplied;
use App\Model\UI\FavouriteModel;
use App\Model\UI\CandidateResume;
use Session;
use DB;
use DateTime;

// Admin Model
use App\Model\Admin\TrainingCompletionModel;
class FilterController extends Controller
{   
    public function __construct(){
        // $this->middleware('guest:employer')->except('InsertEmployerProfile', 'ShortlistCandidates');
    }


    public function FilterDesiredLocation(Request $request){
        $GetCity = $request->city;
        $GetDateFilterCity = array();
        if(is_array($GetCity)){
            foreach($GetCity as $City){
                $GetDateFilterCity[] = DB::select("SELECT employer.profile_pic, jobs.title, jobs.slug, employer.company_name, jobs.location, jobs.job_type FROM jobs, employer WHERE jobs.employer_id = employer.id AND jobs.city IN ('$City')");
            }
            
            $GetFilterResultData = array();
            $CheckCity = "";
    
            foreach($GetDateFilterCity as $DatePostedData){
                foreach($DatePostedData as $FilterCity){
                    if($FilterCity->job_type == 1){
                        $CheckCity = "<span class='job-is ft'>Full time</span>";
                    }else{
                        $CheckCity = "<span class='job-is pt'>Part time</span>";
                    }
        
                    $GetFilterResultData[] = "<div class='job-listing wtabs'><div class='job-title-sec'><div class='c-logo'> <img src='employer_profile/".$FilterCity->profile_pic."' alt='' /> </div><h3><a href='/JobDetails/".$FilterCity->slug."' target='_blank' title=''>".$FilterCity->title."</a></h3><span>".$FilterCity->company_name."</span><div class='job-lctn'><i class='la la-map-marker'></i>".$FilterCity->location."</div></div><div class='job-style-bx'>".$CheckCity."<span class='fav-job'><i class='la la-heart-o'></i></span><i>5 months ago</i></div></div>";
                }
                
            }
            echo json_encode($GetFilterResultData);
        }else{
            $GetDateFilterCity = DB::select("SELECT employer.profile_pic, jobs.title, jobs.slug, employer.company_name, jobs.location, jobs.job_type FROM jobs, employer WHERE jobs.employer_id = employer.id");

            $GetFilterResultData = array();
            $CheckCity = "";
    
            foreach($GetDateFilterCity as $DateFilterCity){
                    if($DateFilterCity->job_type == 1){
                        $CheckCity = "<span class='job-is ft'>Full time</span>";
                    }else{
                        $CheckCity = "<span class='job-is pt'>Part time</span>";
                    }
        
                    $GetFilterResultData[] = "<div class='job-listing wtabs'><div class='job-title-sec'><div class='c-logo'> <img src='employer_profile/".$DateFilterCity->profile_pic."' alt='' /> </div><h3><a href='/JobDetails/".$DateFilterCity->slug."' target='_blank' title=''>".$DateFilterCity->title."</a></h3><span>".$DateFilterCity->company_name."</span><div class='job-lctn'><i class='la la-map-marker'></i>".$DateFilterCity->location."</div></div><div class='job-style-bx'>".$CheckCity."<span class='fav-job'><i class='la la-heart-o'></i></span><i>5 months ago</i></div></div>";
                
            }
            
            echo json_encode($GetFilterResultData);
        }
    }


    public function FilterIndustry(Request $request){
        $GetIndustry = $request->industry;
        $GetDateFilterIndustry = array();
        if(is_array($GetIndustry)){
            foreach($GetIndustry as $Industry){
                $GetDateFilterIndustry[] = DB::select("SELECT employer.profile_pic, jobs.title, jobs.slug, employer.company_name, jobs.location, jobs.job_type FROM jobs, employer WHERE jobs.employer_id = employer.id AND jobs.industry IN ('$Industry')");
            }
            
            $GetFilterResultData = array();
            $CheckIndustry = "";
    
            foreach($GetDateFilterIndustry as $DateFilterIndustry){
                foreach($DateFilterIndustry as $FilterIndustry){
                    if($FilterIndustry->job_type == 1){
                        $CheckIndustry = "<span class='job-is ft'>Full time</span>";
                    }else{
                        $CheckIndustry = "<span class='job-is pt'>Part time</span>";
                    }
        
                    $GetFilterResultData[] = "<div class='job-listing wtabs'><div class='job-title-sec'><div class='c-logo'> <img src='employer_profile/".$FilterIndustry->profile_pic."' alt='' /> </div><h3><a href='/JobDetails/".$FilterIndustry->slug."' target='_blank' title=''>".$FilterIndustry->title."</a></h3><span>".$FilterIndustry->company_name."</span><div class='job-lctn'><i class='la la-map-marker'></i>".$FilterIndustry->location."</div></div><div class='job-style-bx'>".$CheckIndustry."<span class='fav-job'><i class='la la-heart-o'></i></span><i>5 months ago</i></div></div>";
                }
                
            }
            echo json_encode($GetFilterResultData);
        }else{
            $GetDateFilterIndustry = DB::select("SELECT employer.profile_pic, jobs.title, jobs.slug, employer.company_name, jobs.location, jobs.job_type FROM jobs, employer WHERE jobs.employer_id = employer.id");

            $GetFilterResultData = array();
            $CheckIndustry = "";
    
            foreach($GetDateFilterIndustry as $DateFilterIndustry){
                    if($DateFilterIndustry->job_type == 1){
                        $CheckIndustry = "<span class='job-is ft'>Full time</span>";
                    }else{
                        $CheckIndustry = "<span class='job-is pt'>Part time</span>";
                    }
        
                    $GetFilterResultData[] = "<div class='job-listing wtabs'><div class='job-title-sec'><div class='c-logo'> <img src='employer_profile/".$DateFilterIndustry->profile_pic."' alt='' /> </div><h3><a href='/JobDetails/".$DateFilterIndustry->slug."' target='_blank' title=''>".$DateFilterIndustry->title."</a></h3><span>".$DateFilterIndustry->company_name."</span><div class='job-lctn'><i class='la la-map-marker'></i>".$DateFilterIndustry->location."</div></div><div class='job-style-bx'>".$CheckIndustry."<span class='fav-job'><i class='la la-heart-o'></i></span><i>5 months ago</i></div></div>";
                
            }
            
            echo json_encode($GetFilterResultData);
        }
    }





    // Candidates Filter

    public function FilterCandidates(Request $request){
        $GetDatePosted = $request->date_posted;
        // $GetJobType = $request->job_type;
        // echo $GetDatePosted;
        // exit;

        if($GetDatePosted == 1){
            $GetDataByDatePosted = DB::select("SELECT * FROM candidate, candidate_profile WHERE candidate.id = candidate_profile.candidate_id AND candidate.type = 1 AND candidate_profile.created_at >= CURRENT_TIMESTAMP - INTERVAL '1' HOUR");
        }elseif($GetDatePosted == 2){
            $GetDataByDatePosted = DB::select("SELECT * FROM candidate, candidate_profile WHERE candidate.id = candidate_profile.candidate_id AND candidate.type = 1 AND candidate_profile.created_at >= CURRENT_TIMESTAMP - INTERVAL '24' HOUR");
        }elseif($GetDatePosted == 3){
            $GetDataByDatePosted = DB::select("SELECT * FROM candidate, candidate_profile WHERE candidate.id = candidate_profile.candidate_id AND candidate.type = 1 AND candidate_profile.created_at >= DATE_ADD(CURDATE(), INTERVAL -7 DAY)");
        }elseif($GetDatePosted == 4){
            $GetDataByDatePosted = DB::select("SELECT * FROM candidate, candidate_profile WHERE candidate.id = candidate_profile.candidate_id AND candidate.type = 1 AND candidate_profile.created_at >= DATE_ADD(CURDATE(), INTERVAL -14 DAY)");
        }elseif($GetDatePosted == 5){
            $GetDataByDatePosted = DB::select("SELECT * FROM candidate, candidate_profile WHERE candidate.id = candidate_profile.candidate_id AND candidate.type = 1 AND candidate_profile.created_at >= DATE_ADD(CURDATE(), INTERVAL -30 DAY)");
        }elseif($GetDatePosted == 6){
            $GetDataByDatePosted = DB::select("SELECT * FROM candidate, candidate_profile WHERE candidate.id = candidate_profile.candidate_id AND candidate.type = 1");
        }

        $GetFilterResultData = array();
        $CheckJobType = "";

        foreach($GetDataByDatePosted as $DatePostedData){

            $GetFilterResultData[] = "<div class='emply-resume-list square'><div class='row'>
            <div class='col-md-8'><div class='emply-resume-thumb'><img src='/candidate_profile/".$DatePostedData->profile_pic."' alt=''/></div><div class='emply-resume-info'><h3><a href='#' title=''>".$DatePostedData->name."</a></h3><span><i>".$DatePostedData->role."</i> at Atract Solutions</span><p><i class='la la-map-marker'></i>".$DatePostedData->address." / ".$DatePostedData->city."</p></div></div><div class='col-md-4'><div class='shortlists'><a href='/CandidateDetails/".$DatePostedData->slug."' id='' title='' target='_blank'>View Profile <i class='la la-plus'></i></a></div></div></div>";
        }

        echo json_encode($GetFilterResultData);
    }


    public function FilterCandidateDesiredLocation(Request $request){
        $GetCity = $request->city;
        $GetDateFilterCity = array();
        if(is_array($GetCity)){
            foreach($GetCity as $City){
                $GetDateFilterCity[] = DB::select("SELECT * FROM candidate, candidate_profile WHERE candidate.id = candidate_profile.candidate_id AND candidate.type = 1 AND candidate_profile.desired_location IN ('$City')");
            }
            
            $GetFilterResultData = array();
            $CheckCity = "";
    
            foreach($GetDateFilterCity as $DatePostedData){
                foreach($DatePostedData as $FilterCity){
        
                    $GetFilterResultData[] = "<div class='emply-resume-list square'><div class='row'><div class='col-md-8'><div class='emply-resume-thumb'><img src='/candidate_profile/".$FilterCity->profile_pic."' alt=''/></div><div class='emply-resume-info'><h3><a href='#' title=''>".$FilterCity->name."</a></h3><span><i>".$FilterCity->role."</i> at Atract Solutions</span><p><i class='la la-map-marker'></i>".$FilterCity->address." / ".$FilterCity->city."</p></div></div><div class='col-md-4'><div class='shortlists'><a href='/CandidateDetails/".$FilterCity->slug."' id='' title='' target='_blank'>View Profile <i class='la la-plus'></i></a></div></div></div>";
                }
                
            }
            echo json_encode($GetFilterResultData);
        }else{
            $GetDateFilterCity = DB::select("SELECT * FROM candidate, candidate_profile WHERE candidate.id = candidate_profile.candidate_id AND candidate.type = 1");

            $GetFilterResultData = array();
            $CheckCity = "";
    
            foreach($GetDateFilterCity as $DateFilterCity){
                    $GetFilterResultData[] = "<div class='emply-resume-list square'><div class='row'><div class='col-md-8'><div class='emply-resume-thumb'><img src='/candidate_profile/".$DateFilterCity->profile_pic."' alt=''/></div><div class='emply-resume-info'><h3><a href='#' title=''>".$DateFilterCity->name."</a></h3><span><i>".$DateFilterCity->role."</i> at Atract Solutions</span><p><i class='la la-map-marker'></i>".$DateFilterCity->address." / ".$DateFilterCity->city."</p></div></div><div class='col-md-4'><div class='shortlists'><a href='/CandidateDetails/".$DateFilterCity->slug."' id='' title='' target='_blank'>View Profile <i class='la la-plus'></i></a></div></div></div>";
                
            }
            
            echo json_encode($GetFilterResultData);
        }
    }


    public function FilterCandidateIndustry(Request $request){
        $GetIndustry = $request->industry;
        $GetDateFilterIndustry = array();
        if(is_array($GetIndustry)){
            foreach($GetIndustry as $Industry){
                $GetDateFilterIndustry[] = DB::select("SELECT * FROM candidate, candidate_profile WHERE candidate.id = candidate_profile.candidate_id AND candidate.type = 1 AND candidate_profile.industry IN ('$Industry')");
            }
            
            $GetFilterResultData = array();
            $CheckIndustry = "";
    
            foreach($GetDateFilterIndustry as $DateFilterIndustry){
                foreach($DateFilterIndustry as $FilterIndustry){
        
                    $GetFilterResultData[] = "<div class='emply-resume-list square'><div class='row'><div class='col-md-8'><div class='emply-resume-thumb'><img src='/candidate_profile/".$FilterIndustry->profile_pic."' alt=''/></div><div class='emply-resume-info'><h3><a href='#' title=''>".$FilterIndustry->name."</a></h3><span><i>".$FilterIndustry->role."</i> at Atract Solutions</span><p><i class='la la-map-marker'></i>".$FilterIndustry->address." / ".$FilterIndustry->city."</p></div></div><div class='col-md-4'><div class='shortlists'><a href='/CandidateDetails/".$FilterIndustry->slug."' id='' title='' target='_blank'>View Profile <i class='la la-plus'></i></a></div></div></div>";
                }
                
            }
            echo json_encode($GetFilterResultData);
        }else{
            $GetDateFilterIndustry = DB::select("SELECT * FROM candidate, candidate_profile WHERE candidate.id = candidate_profile.candidate_id AND candidate.type = 1");

            $GetFilterResultData = array();
            $CheckIndustry = "";
    
            foreach($GetDateFilterIndustry as $DateFilterIndustry){

                    $GetFilterResultData[] = "<div class='emply-resume-list square'><div class='row'><div class='col-md-8'><div class='emply-resume-thumb'><img src='/candidate_profile/".$DateFilterIndustry->profile_pic."' alt=''/></div><div class='emply-resume-info'><h3><a href='#' title=''>".$DateFilterIndustry->name."</a></h3><span><i>".$DateFilterIndustry->role."</i> at Atract Solutions</span><p><i class='la la-map-marker'></i>".$DateFilterIndustry->address." / ".$DateFilterIndustry->city."</p></div></div><div class='col-md-4'><div class='shortlists'><a href='/CandidateDetails/".$DateFilterIndustry->slug."' id='' title='' target='_blank'>View Profile <i class='la la-plus'></i></a></div></div></div>";
                
            }
            
            echo json_encode($GetFilterResultData);
        }
    }


    public function FilterEmployeeType(Request $request){
        
        $GetEmployeeType= $request->employee_type;
        $GetDataByDatePosted = array();
        if(is_array($GetEmployeeType)){
            foreach($GetEmployeeType as $EmployeeType){
                $GetDataByDatePosted[] = DB::select("SELECT * FROM candidate, candidate_profile WHERE candidate.id = candidate_profile.candidate_id AND candidate.type = 1 AND candidate_profile.employee_type IN ('$EmployeeType')");
            }
            
            $GetFilterResultData = array();
    
            foreach($GetDataByDatePosted as $DatePostedData){
                foreach($DatePostedData as $FilterEmployeeType){
                    $GetFilterResultData[] = "<div class='emply-resume-list square'><div class='row'><div class='col-md-8'><div class='emply-resume-thumb'><img src='/candidate_profile/".$FilterEmployeeType->profile_pic."' alt=''/></div><div class='emply-resume-info'><h3><a href='#' title=''>".$FilterEmployeeType->name."</a></h3><span><i>".$FilterEmployeeType->role."</i> at Atract Solutions</span><p><i class='la la-map-marker'></i>".$FilterEmployeeType->address." / ".$FilterEmployeeType->city."</p></div></div><div class='col-md-4'><div class='shortlists'><a href='/CandidateDetails/".$FilterEmployeeType->slug."' id='' title='' target='_blank'>View Profile <i class='la la-plus'></i></a></div></div></div>";
                }
                
            }
            echo json_encode($GetFilterResultData);
        }else{
            $GetDateFilterEmployeeType = DB::select("SELECT * FROM candidate, candidate_profile WHERE candidate.id = candidate_profile.candidate_id AND candidate.type = 1");

            $GetFilterResultData = array();
            $CheckIndustry = "";
    
            foreach($GetDateFilterEmployeeType as $DateFilterEmployeeType){

                    $GetFilterResultData[] = "<div class='emply-resume-list square'><div class='row'><div class='col-md-8'><div class='emply-resume-thumb'><img src='/candidate_profile/".$DateFilterEmployeeType->profile_pic."' alt=''/></div><div class='emply-resume-info'><h3><a href='#' title=''>".$DateFilterEmployeeType->name."</a></h3><span><i>".$DateFilterEmployeeType->role."</i> at Atract Solutions</span><p><i class='la la-map-marker'></i>".$DateFilterEmployeeType->address." / ".$DateFilterEmployeeType->city."</p></div></div><div class='col-md-4'><div class='shortlists'><a href='/CandidateDetails/".$DateFilterEmployeeType->slug."' id='' title='' target='_blank'>View Profile <i class='la la-plus'></i></a></div></div></div>";
                
            }
            
            echo json_encode($GetFilterResultData);
        }
        
    }

    public function FilterSalaryType(Request $request){
        
        $GetSalaryType= $request->salary_type;
        $GetDataByDatePosted = array();
        if(is_array($GetSalaryType)){
            foreach($GetSalaryType as $SalaryType){
                $GetDataByDatePosted[] = DB::select("SELECT * FROM candidate, candidate_profile WHERE candidate.id = candidate_profile.candidate_id AND candidate.type = 1 AND candidate_profile.expected_salary IN ('$SalaryType')");
            }
            
            $GetFilterResultData = array();
    
            foreach($GetDataByDatePosted as $DatePostedData){
                foreach($DatePostedData as $FilterSalaryType){
                    $GetFilterResultData[] = "<div class='emply-resume-list square'><div class='row'><div class='col-md-8'><div class='emply-resume-thumb'><img src='/candidate_profile/".$FilterSalaryType->profile_pic."' alt=''/></div><div class='emply-resume-info'><h3><a href='#' title=''>".$FilterSalaryType->name."</a></h3><span><i>".$FilterSalaryType->role."</i> at Atract Solutions</span><p><i class='la la-map-marker'></i>".$FilterSalaryType->address." / ".$FilterSalaryType->city."</p></div></div><div class='col-md-4'><div class='shortlists'><a href='/CandidateDetails/".$FilterSalaryType->slug."' id='' title='' target='_blank'>View Profile <i class='la la-plus'></i></a></div></div></div>";
                }
                
            }
            echo json_encode($GetFilterResultData);
        }else{
            $GetDateFilterSalaryType = DB::select("SELECT * FROM candidate, candidate_profile WHERE candidate.id = candidate_profile.candidate_id AND candidate.type = 1");

            $GetFilterResultData = array();
            $CheckIndustry = "";
    
            foreach($GetDateFilterSalaryType as $DateFilterSalaryType){

                    $GetFilterResultData[] = "<div class='emply-resume-list square'><div class='row'><div class='col-md-8'><div class='emply-resume-thumb'><img src='/candidate_profile/".$DateFilterSalaryType->profile_pic."' alt=''/></div><div class='emply-resume-info'><h3><a href='#' title=''>".$DateFilterSalaryType->name."</a></h3><span><i>".$DateFilterSalaryType->role."</i> at Atract Solutions</span><p><i class='la la-map-marker'></i>".$DateFilterSalaryType->address." / ".$DateFilterSalaryType->city."</p></div></div><div class='col-md-4'><div class='shortlists'><a href='/CandidateDetails/".$DateFilterSalaryType->slug."' id='' title='' target='_blank'>View Profile <i class='la la-plus'></i></a></div></div></div>";
            }
            
            echo json_encode($GetFilterResultData);
        }
        
    }


    // Trainings Filter

    public function FilterTrainings(Request $request){
        $GetDatePosted = $request->date_posted;
        $GetLocation = $request->location;
        // $GetDuration = $request->Duration;
        $GetDuration = preg_replace('/([0-9])/', '$1' . " ", $request->Duration);
        // $GetStipend = $request->Stipend;

        // $GetStipendModel = TrainingCompletionModel::get();

        // $GetJobType = $request->job_type;
        // echo json_encode($GetDuration);
        // exit;
       
        // if($GetDatePosted == 1){
        //     $GetDataByDatePosted = DB::select("SELECT employer.profile_pic, training.slug, training.title, employer.company_name, employer.country, employer.city, training.duration FROM training, employer WHERE training.employer_id = employer.id AND training.created_at >= CURRENT_TIMESTAMP - INTERVAL '1' HOUR");
        // }elseif($GetDatePosted == 2){
        //     $GetDataByDatePosted = DB::select("SELECT employer.profile_pic, training.slug, training.title, employer.company_name, employer.country, employer.city, training.duration FROM training, employer WHERE training.employer_id = employer.id AND training.created_at >= CURRENT_TIMESTAMP - INTERVAL '24' HOUR");
        // }elseif($GetDatePosted == 3){
        //     $GetDataByDatePosted = DB::select("SELECT employer.profile_pic, training.slug, training.title, employer.company_name, employer.country, employer.city, training.duration FROM training, employer WHERE training.employer_id = employer.id AND training.created_at >= DATE_ADD(CURDATE(), INTERVAL -7 DAY)");
        // }elseif($GetDatePosted == 4){
        //     $GetDataByDatePosted = DB::select("SELECT employer.profile_pic, training.slug, training.title, employer.company_name, employer.country, employer.city, training.duration FROM training, employer WHERE training.employer_id = employer.id AND training.created_at >= DATE_ADD(CURDATE(), INTERVAL -14 DAY)");
        // }elseif($GetDatePosted == 5){
        //     $GetDataByDatePosted = DB::select("SELECT employer.profile_pic, training.slug, training.title, employer.company_name, employer.country, employer.city, training.duration FROM training, employer WHERE training.employer_id = employer.id AND training.created_at >= DATE_ADD(CURDATE(), INTERVAL -30 DAY)");
        // }elseif($GetDatePosted == 6){
        //     $GetDataByDatePosted = DB::select("SELECT employer.profile_pic, training.slug, training.title, employer.company_name, employer.country, employer.city, training.duration FROM training, employer WHERE training.employer_id = employer.id");
        // }

        $GetDataByDatePosted[] =DB::table('training')
            ->select('employer.profile_pic', 'training.slug', 'training_title.title', 'employer.company_name', 'employer.country', 'employer.city', 'training.duration', 'training.start_date', 'training.stipend', 'training.training_completion', 'training.description', 'training.created_at', 'employer.slug AS CompanySlug', 'training.id')
            ->join('employer', 'employer.id', '=', 'training.employer_id')
            ->join('training_title', 'training_title.id', '=', 'training.title')
            // ->whereIn('job_type', $request->categories)
            ->when($GetLocation, function ($query) use ($GetLocation) {
                return $query->whereIn('city', $GetLocation);
            })
                    // ->WhereIn('job_role', $request->role)
            ->when($GetDuration, function ($query) use ($GetDuration) {
                return $query->whereIn('duration', $GetDuration);
            })
    
            // ->when($GetStipend, function ($query) use ($GetStipend) {
            //     return $query->whereIn('stipend', $GetStipend);
            // })
            ->get();

        $GetPostedOn = "";
        $GetTrainingResults = "";

        $GetTrainingDuration = "";

        $GetStipend = "";
        $GetTrainingCompletion = "";

        foreach($GetDataByDatePosted as $GetData){
            foreach($GetData as $Trainings){
                $now = new DateTime;
            $full = false;	
            $ago = new DateTime($Trainings->created_at);
            $diff = $now->diff($ago);

            $diff->w = floor($diff->d / 7);
            $diff->d -= $diff->w * 7;

            $string = array(
            'y' => 'year',
            'm' => 'month',
            'w' => 'week',
            'd' => 'day',
            'h' => 'hour',
            'i' => 'minute',
            's' => 'second',
            );

            foreach ($string as $k => &$v) {
            if ($diff->$k) {
            $v = $diff->$k . ' ' . $v . ($diff->$k > 1 ? 's' : '');
            } else {
            unset($string[$k]);
            }
            }

            if (!$full) $string = array_slice($string, 0, 1);
            $GetPostedOn =  $string ? implode(', ', $string) . ' ago' : 'just now';


            // if($Trainings->duration == 1){
            //     $GetTrainingDuration = "30 Days";
            // }else{
            //     $GetTrainingDuration = "60 Days";
            // }

            $num = $Trainings->stipend;
            $units = ['', 'K', 'M'];
            for($i = 0; $num>=1000;$i++){
                $num /= 1000;
            }
            $GetStipend = round($num, 1).$units[$i];
                
            
                      
            if($Trainings->training_completion == 1){
                $GetTrainingCompletion = "Certification";
            }else{
                $GetTrainingCompletion = "Full Time Job";
            }


                $GetTrainingResults = "<div class='job-listings-sec no-border text-center'>
                <div class='job-listing wtabs recommented-jobs'>
                    <div class='job-title-sec recommended-jobs-pad-left'>
                        <h3><a href='/TrainingDetails/".$Trainings->CompanySlug."/".$Trainings->slug."' target='_blank'>".$Trainings->title."</a></h3>
                        <p class='recommended-jobs-company-name'>".$Trainings->company_name."</p>
                        <div class='job-lctn job-attributes-recommended-jobs'><i class='fa fa-briefcase'></i>".date('d M Y', strtotime($Trainings->start_date))."</div>
                        <div class='job-lctn job-attributes-recommended-jobs'><i class='fa fa-clock-o'></i>
                            ".$Trainings->duration."
                        </div>
                        <div class='job-lctn job-attributes-recommended-jobs'><i class='fa fa-briefcase' ></i> 
                            ".$GetStipend."
                                / Month
                        </div>
                        <div class='job-lctn job-attributes-recommended-jobs'><i class='fa fa-industry'></i>
                            ".$GetTrainingCompletion."
                        </div>
                    </div>
                    <p class='recommended-jobs-posted-on'>Posted on: 
                        ".$GetPostedOn."
                    </p>
                </div>
                </div>";

                if($GetTrainingResults){
                    echo $GetTrainingResults;
                }else{
                    echo json_encode("No Results Found...");
                }
            }
        }
    }


    public function FilterDuration(Request $request){
        $GetDuration= $request->duration;

        // echo json_encode($GetDuration);
        // exit;

        $GetDataByDatePosted = array();
        if(is_array($GetDuration)){
            foreach($GetDuration as $Duration){
                $GetDataByDatePosted[] = DB::select("SELECT employer.profile_pic, training.slug, training.title, employer.company_name, employer.country, employer.city, training.duration FROM training, employer WHERE training.employer_id = employer.id AND training.duration IN ('$Duration')");
            }
            
            $GetFilterResultData = array();
    
            foreach($GetDataByDatePosted as $DatePostedData){
                foreach($DatePostedData as $FilterDuration){
                    $GetFilterResultData[] = "<div class='job-listing wtabs'><div class='job-title-sec'><div class='c-logo'> <img src='/employer_profile/".$FilterDuration->profile_pic."' alt='' /> </div><h3><a href='/TrainingDetails/".$FilterDuration->company_name."/".$FilterDuration->slug."' target='_blank' title=''>".$FilterDuration->title."</a></h3><span>".$FilterDuration->company_name."</span><div class='job-lctn'><i class='la la-map-marker'></i>".$FilterDuration->country."," .$FilterDuration->city."</div></div><div class='job-style-bx'><span class='job-is ft'>".$FilterDuration->duration." Days</span><span class='fav-job'></span><i>5 months ago</i></div></div>";
                }
                
            }
            echo json_encode($GetFilterResultData);
        }else{
            $GetDateFilterDuration = DB::select("SELECT employer.profile_pic, training.slug, training.title, employer.company_name, employer.country, employer.city, training.duration FROM training, employer WHERE training.employer_id = employer.id");

            $GetFilterResultData = array();
    
            foreach($GetDateFilterDuration as $DateFilterDuration){

                    $GetFilterResultData[] = "<div class='job-listing wtabs'><div class='job-title-sec'><div class='c-logo'> <img src='/employer_profile/".$DateFilterDuration->profile_pic."' alt='' /> </div><h3><a href='/TrainingDetails/".$DateFilterDuration->company_name."/".$DateFilterDuration->slug."' target='_blank' title=''>".$DateFilterDuration->title."</a></h3><span>".$DateFilterDuration->company_name."</span><div class='job-lctn'><i class='la la-map-marker'></i>".$DateFilterDuration->country."," .$DateFilterDuration->city."</div></div><div class='job-style-bx'><span class='job-is ft'>".$DateFilterDuration->duration." Days</span><span class='fav-job'></span><i>5 months ago</i></div></div>";
            }
            
            echo json_encode($GetFilterResultData);
        }
    }





    // Employer Manage Jobs Filter

    public function FilterDatePostedManageJobs(Request $request){
        $GetDatePosted = $request->date_posted;

        $GetEmployerId = Session::get("EmployerId");

        $GetDataByDatePosted = [];
        
            if($GetDatePosted == 1){
                $GetDataByDatePosted[] = DB::select("SELECT DISTINCT job_title.name AS title, jobs.experience ,jobs.job_type, jobs.city, jobs.total_positions, jobs.salary_min, jobs.salary_max, jobs.location, jobs.status,
                jobs.created_at, jobs.slug, jobs.id, employer.slug As CompanySlug, jobs.employee_type FROM jobs, employer, job_title WHERE jobs.employer_id = employer.id AND jobs.employer_id = $GetEmployerId AND job_title.id = jobs.title AND jobs.status = 0 AND jobs.created_at >= DATE_SUB(CURDATE(), INTERVAL 1 HOUR) ORDER BY jobs.created_at DESC");

            }elseif($GetDatePosted == 2){
                $GetDataByDatePosted[] = DB::select("SELECT DISTINCT job_title.name AS title, jobs.experience ,jobs.job_type, jobs.city, jobs.total_positions, jobs.salary_min, jobs.salary_max, jobs.location, jobs.status,
                jobs.created_at, jobs.slug, jobs.id, employer.slug As CompanySlug, jobs.employee_type FROM jobs, employer, job_title WHERE jobs.employer_id = employer.id AND jobs.employer_id = $GetEmployerId AND job_title.id = jobs.title AND jobs.status = 0 AND jobs.created_at >= (CURRENT_TIMESTAMP - INTERVAL '24' HOUR) ORDER BY jobs.created_at DESC");

            }elseif($GetDatePosted == 3){

                $GetDataByDatePosted[] = DB::select("SELECT DISTINCT job_title.name AS title, jobs.experience ,jobs.job_type, jobs.city, jobs.total_positions, jobs.salary_min, jobs.salary_max, jobs.location, jobs.status,
                jobs.created_at, jobs.slug, jobs.id, employer.slug As CompanySlug, jobs.employee_type FROM jobs, employer, job_title WHERE jobs.employer_id = employer.id AND jobs.employer_id = $GetEmployerId AND job_title.id = jobs.title AND jobs.status = 0 AND jobs.created_at >= DATE_ADD(CURDATE(), INTERVAL -7 DAY) ORDER BY jobs.created_at DESC");
            }elseif($GetDatePosted == 4){
                $GetDataByDatePosted[] = DB::select("SELECT DISTINCT job_title.name AS title, jobs.experience ,jobs.job_type, jobs.city, jobs.total_positions, jobs.salary_min, jobs.salary_max, jobs.location, jobs.status,
                jobs.created_at, jobs.slug, jobs.id, employer.slug As CompanySlug, jobs.employee_type FROM jobs, employer, job_title WHERE jobs.employer_id = employer.id AND jobs.employer_id = $GetEmployerId AND job_title.id = jobs.title AND jobs.status = 0 AND jobs.created_at ORDER BY jobs.created_at DESC");
            }

        $GetSearchResults = "";
        $GetJobType = "";
        $GetSalaryMin = "";
        $GetSalaryMax ="";

        $GetExperience ="";

        if($GetDataByDatePosted){
            foreach($GetDataByDatePosted as $GetData){
                foreach($GetData as $Results){
                    if($Results->job_type == 1){
                        $GetJobType = "Part time";
                    }else{
                        $GetJobType = "Full time";
                    }
                    
                    $num = $Results->salary_min;
                                $units = ['', 'K', 'M'];
                                for($i = 0; $num>=1000;$i++){
                                    $num /= 1000;
                                }


                    $GetSalaryMin =  round($num, 1).$units[$i];
                    
                    $num = $Results->salary_max;
                                $units = ['', 'K', 'M'];
                                for($i = 0; $num>=1000;$i++){
                                    $num /= 1000;
                                }
                    $GetSalaryMax = round($num, 1).$units[$i];

                    if($Results->employee_type == 1){
                        $GetExperience = "Fresher";
                    }else{
                        $GetExperience = $Results->experience."Years";
                    }


                    $GetSearchResults = "<div class='job-listings-sec no-border'>
                    <div class='job-listing wtabs pad-top0'>
                        <div class='job-title-sec width100'>
                        <h3 class='manage-jobs-list pad-bottom10'><a href='/JobDetails/".$Results->CompanySlug."/".$Results->slug."' title=''>".$Results->title."</a></h3>
                        <div class='job-lctn'><i class='fa fa-briefcase'></i> ".$GetExperience."</div>
                            
                            <div class='job-lctn'><i class='fa fa-industry'></i> 
                            ".$GetJobType."
                            </div>
                            <div class='job-lctn'><i class='fa fa-map-marker'></i> ".$Results->city."</div>

                            <div class='job-lctn'><i class='fa fa-briefcase'></i>
                            ".$GetSalaryMin." - ".$GetSalaryMax."
                             / Month</div>

                            
                            <div class='manage-job-attributes'>
                            <div class='job-lctn'><i class='fa fa-briefcase'></i> Openings: ".$Results->total_positions."</div>
                                
                                <div class='job-lctn'><i class='fa fa-industry'></i> Posted on: ".date('d M Y', strtotime($Results->created_at))." </div>
                                <div class='job-lctn'><i class='fa fa-map-marker'></i> Job Views: 105</div>

                                <div class='job-lctn'><i class='fa fa-briefcase'></i> Job Applicant: 1</div>
                            </div>
                            <br>
                            <div class='row'>
                                <div class='col-md-4'>
                                <a  href='/ViewAllAppliedCandidates/".$Results->id."/".$Results->slug."' target='_blank'  class='view-details-jobs'> <span class='view-details-dot'>...</span> View Details</a>
                                </div>
                                
                                <div class='col-md-3'>
                                    <button type='button' onclick='EditJobs(".$Results->id.")' class='view-details-jobs'> <i class='fa fa-pencil'></i> Edit</button>
                                </div>
                                <div class='col-md-3'>
                                    <h3 class='text-left manage-jobs-status'>Published</h3>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>";
    
                    if($GetSearchResults){
                        echo $GetSearchResults;
                    }
                }
            }
        }
    }


    // Employer Closed Jobs Filter

    public function FilterDatePostedClosedJobs(Request $request){
        $GetDatePosted = $request->date_posted;

        $GetEmployerId = Session::get("EmployerId");

        $GetDataByDatePosted = [];
        
        if($GetDatePosted == 1){
            $GetDataByDatePosted[] = DB::select("SELECT DISTINCT job_title.name AS title, jobs.experience ,jobs.job_type, jobs.city, jobs.total_positions, jobs.salary_min, jobs.salary_max, jobs.location, jobs.status,
            jobs.created_at, jobs.slug, jobs.id, employer.slug As CompanySlug, jobs.employee_type FROM jobs, employer, job_title WHERE jobs.employer_id = employer.id AND jobs.employer_id = $GetEmployerId AND jobs.status = 1 AND job_title.id = jobs.title AND jobs.created_at >= (CURRENT_TIMESTAMP - INTERVAL '1' HOUR) ORDER BY jobs.created_at DESC");

        }elseif($GetDatePosted == 2){
            $GetDataByDatePosted[] = DB::select("SELECT DISTINCT job_title.name AS title, jobs.experience ,jobs.job_type, jobs.city, jobs.total_positions, jobs.salary_min, jobs.salary_max, jobs.location, jobs.status,
            jobs.created_at, jobs.slug, jobs.id, employer.slug As CompanySlug, jobs.employee_type FROM jobs, employer, job_title WHERE jobs.employer_id = employer.id AND jobs.employer_id = $GetEmployerId AND jobs.status = 1 AND job_title.id = jobs.title AND jobs.created_at >= (CURRENT_TIMESTAMP - INTERVAL '24' HOUR) ORDER BY jobs.created_at DESC");

        }elseif($GetDatePosted == 3){

            $GetDataByDatePosted[] = DB::select("SELECT DISTINCT job_title.name AS title, jobs.experience ,jobs.job_type, jobs.city, jobs.total_positions, jobs.salary_min, jobs.salary_max, jobs.location, jobs.status,
            jobs.created_at, jobs.slug, jobs.id, employer.slug As CompanySlug, jobs.employee_type FROM jobs, employer, job_title WHERE jobs.employer_id = employer.id AND jobs.employer_id = $GetEmployerId AND jobs.status = 1 AND job_title.id = jobs.title AND jobs.created_at >= DATE_ADD(CURDATE(), INTERVAL -7 DAY) ORDER BY jobs.created_at DESC");
        }elseif($GetDatePosted == 4){
            $GetDataByDatePosted[] = DB::select("SELECT DISTINCT job_title.name AS title, jobs.experience ,jobs.job_type, jobs.city, jobs.total_positions, jobs.salary_min, jobs.salary_max, jobs.location, jobs.status,
            jobs.created_at, jobs.slug, jobs.id, employer.slug As CompanySlug, jobs.employee_type FROM jobs, employer, job_title WHERE jobs.employer_id = employer.id AND jobs.employer_id = $GetEmployerId AND jobs.status = 1 AND job_title.id = jobs.title AND jobs.created_at ORDER BY jobs.created_at DESC");
        }

        $GetSearchResults = "";
        $GetJobType = "";
        $GetSalaryMin = "";
        $GetSalaryMax ="";

        $GetExperience = "";

        if($GetDataByDatePosted){
            foreach($GetDataByDatePosted as $GetData){
                foreach($GetData as $Results){
                    if($Results->job_type == 1){
                        $GetJobType = "Part time";
                    }else{
                        $GetJobType = "Full time";
                    }
                    
                    $num = $Results->salary_min;
                                $units = ['', 'K', 'M'];
                                for($i = 0; $num>=1000;$i++){
                                    $num /= 1000;
                                }


                    $GetSalaryMin =  round($num, 1).$units[$i];
                    
                    $num = $Results->salary_max;
                                $units = ['', 'K', 'M'];
                                for($i = 0; $num>=1000;$i++){
                                    $num /= 1000;
                                }
                    $GetSalaryMax = round($num, 1).$units[$i];

                    if($Results->employee_type == 1){
                        $GetExperience = "Fresher";
                    }else{
                        $GetExperience = $Results->experience."Years";
                    }



                    $GetSearchResults = "<div class='job-listings-sec no-border'>
                    <div class='job-listing wtabs pad-top0'>
                        <div class='job-title-sec width100'>
                        <h3 class='manage-jobs-list pad-bottom10'><a href='/JobDetails/".$Results->CompanySlug."/".$Results->slug."' title=''>".$Results->title."</a></h3>
                        <div class='job-lctn'><i class='fa fa-briefcase'></i> ".$GetExperience."</div>
                            
                            <div class='job-lctn'><i class='fa fa-industry'></i> 
                            ".$GetJobType."
                            </div>
                            <div class='job-lctn'><i class='fa fa-map-marker'></i> ".$Results->city."</div>

                            <div class='job-lctn'><i class='fa fa-briefcase'></i>
                            ".$GetSalaryMin." - ".$GetSalaryMax."
                             / Month</div>

                            
                            <div class='manage-job-attributes'>
                            <div class='job-lctn'><i class='fa fa-briefcase'></i> Openings: ".$Results->total_positions."</div>
                                
                                <div class='job-lctn'><i class='fa fa-industry'></i> Posted on: ".date('d M Y', strtotime($Results->created_at))." </div>
                                <div class='job-lctn'><i class='fa fa-map-marker'></i> Job Views: 105</div>

                                <div class='job-lctn'><i class='fa fa-briefcase'></i> Job Applicant: 1</div>
                            </div>
                            <br>
                            <div class='row'>
                                <div class='col-md-4'>
                                <a  href='/ViewAllAppliedCandidates/".$Results->id."/".$Results->slug."' target='_blank'  class='view-details-jobs'> <span class='view-details-dot'>...</span> View Details</a>
                                </div>
                                
                                <div class='col-md-3'>
                                    <button type='button' onclick='EditJobs(".$Results->id.")' class='view-details-jobs'> <i class='fa fa-pencil'></i> Edit</button>
                                </div>
                                <div class='col-md-3'>
                                    <h3 class='text-left manage-jobs-status'>Published</h3>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>";
    
                    if($GetSearchResults){
                        echo $GetSearchResults;
                    }
                }
            }
        }
    }



    public function FilterManageJobs(Request $request){
        // $GetDatePosted = $request->date_posted;
        $GetEmployerId = Session::get("EmployerId");

        $GetLocation = $request->location;
        $GetEmpType = $request->EmpType;
        $GetTitle = $request->Title;
        
        

        $GetDataByDatePosted[] =DB::table('jobs')
            ->select('job_title.name AS title', 'jobs.experience' ,'jobs.job_type', 'jobs.city', 'jobs.total_positions', 'jobs.salary_min', 'jobs.salary_max', 'jobs.location', 'jobs.status', 'jobs.created_at', 'jobs.industry', 'jobs.slug', 'jobs.id', 'employer.slug As CompanySlug', 'jobs.employee_type', 'jobs.employee_type')
            ->join('employer', 'employer.id', '=', 'jobs.employer_id')
            ->join('job_title', 'job_title.id', '=', 'jobs.title')
            ->where('jobs.employer_id', $GetEmployerId)
            ->where('jobs.status', 0)

            // ->whereIn('job_type', $request->categories)
            ->when($GetLocation, function ($query) use ($GetLocation) {
                return $query->whereIn('jobs.city', $GetLocation);
            })
                    // ->WhereIn('job_role', $request->role)
            ->when($GetEmpType, function ($query) use ($GetEmpType) {
                return $query->whereIn('employee_type', $GetEmpType);
            })
    
            ->when($GetTitle, function ($query) use ($GetTitle) {
                return $query->whereIn('job_title.id', $GetTitle);
            })
            ->get();

        $GetSearchResults = "";
        $GetJobType = "";
        $GetSalaryMin = "";
        $GetSalaryMax ="";
        $GetExperience = "";

        if($GetDataByDatePosted){
            foreach($GetDataByDatePosted as $GetData){
                foreach($GetData as $Results){
                    if($Results->job_type == 1){
                        $GetJobType = "Part time";
                    }else{
                        $GetJobType = "Full time";
                    }
                    
                    $num = $Results->salary_min;
                                $units = ['', 'K', 'M'];
                                for($i = 0; $num>=1000;$i++){
                                    $num /= 1000;
                                }


                    $GetSalaryMin =  round($num, 1).$units[$i];
                    
                    $num = $Results->salary_max;
                                $units = ['', 'K', 'M'];
                                for($i = 0; $num>=1000;$i++){
                                    $num /= 1000;
                                }
                    $GetSalaryMax = round($num, 1).$units[$i];

                    if($Results->employee_type == 1){
                        $GetExperience = "Fresher";
                    }else{
                        $GetExperience = $Results->experience."Years";
                    }

                    $GetSearchResults = "<div class='job-listings-sec no-border'>
                    <div class='job-listing wtabs pad-top0'>
                        <div class='job-title-sec width100'>
                        <h3 class='manage-jobs-list pad-bottom10'><a href='/JobDetails/".$Results->CompanySlug."/".$Results->slug."' title=''>".$Results->title."</a></h3>
                        <div class='job-lctn'><i class='fa fa-briefcase'></i> ".$GetExperience."</div>
                            
                            <div class='job-lctn'><i class='fa fa-industry'></i> 
                            ".$GetJobType."
                            </div>
                            <div class='job-lctn'><i class='fa fa-map-marker'></i> ".$Results->city."</div>

                            <div class='job-lctn'><i class='fa fa-briefcase'></i>
                            ".$GetSalaryMin." - ".$GetSalaryMax."
                                / Month</div>

                            
                            <div class='manage-job-attributes'>
                            <div class='job-lctn'><i class='fa fa-briefcase'></i> Openings: ".$Results->total_positions."</div>
                                
                                <div class='job-lctn'><i class='fa fa-industry'></i> Posted on: ".date('d M Y', strtotime($Results->created_at))." </div>
                                <div class='job-lctn'><i class='fa fa-map-marker'></i> Job Views: 105</div>

                                <div class='job-lctn'><i class='fa fa-briefcase'></i> Job Applicant: 1</div>
                            </div>
                            <br>
                            <div class='row'>
                                <div class='col-md-4'>
                                <a  href='/ViewAllAppliedCandidates/".$Results->id."/".$Results->slug."' target='_blank'  class='view-details-jobs'> <span class='view-details-dot'>...</span> View Details</a>
                                </div>
                                
                                <div class='col-md-3'>
                                    <button type='button' onclick='EditJobs(".$Results->id.")' class='view-details-jobs'> <i class='fa fa-pencil'></i> Edit</button>
                                </div>
                                <div class='col-md-3'>
                                    <h3 class='text-left manage-jobs-status'>Published</h3>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>";
    
                    if($GetSearchResults){
                        echo $GetSearchResults;
                    }
                }
            }
        }
    }


    public function FilterCloseJobs(Request $request){
        // $GetDatePosted = $request->date_posted;
        $GetEmployerId = Session::get("EmployerId");

        $GetLocation = $request->location;
        $GetEmpType = $request->EmpType;
        $GetTitle = $request->Title;

        $GetDataByDatePosted[] =DB::table('jobs')
            ->select('job_title.name AS title', 'jobs.experience' ,'jobs.job_type', 'jobs.city', 'jobs.total_positions', 'jobs.salary_min', 'jobs.salary_max', 'jobs.location', 'jobs.status', 'jobs.created_at', 'jobs.industry', 'jobs.slug', 'jobs.id', 'employer.slug As CompanySlug', 'jobs.employee_type')
            ->join('employer', 'employer.id', '=', 'jobs.employer_id')
            ->join('job_title', 'job_title.id', '=', 'jobs.title')
            ->where('jobs.employer_id', $GetEmployerId)
            ->where('jobs.status', 1)

            // ->whereIn('job_type', $request->categories)
            ->when($GetLocation, function ($query) use ($GetLocation) {
                return $query->whereIn('jobs.city', $GetLocation);
            })
                    // ->WhereIn('job_role', $request->role)
            ->when($GetEmpType, function ($query) use ($GetEmpType) {
                return $query->whereIn('jobs.employee_type', $GetEmpType);
            })
    
            ->when($GetTitle, function ($query) use ($GetTitle) {
                return $query->whereIn('job_title.id', $GetTitle);
            })
            ->get();

        $GetSearchResults = "";
        $GetJobType = "";
        $GetSalaryMin = "";
        $GetSalaryMax ="";

        $GetExperience = "";

        if($GetDataByDatePosted){
            foreach($GetDataByDatePosted as $GetData){
                foreach($GetData as $Results){
                    if($Results->job_type == 1){
                        $GetJobType = "Part time";
                    }else{
                        $GetJobType = "Full time";
                    }
                    
                    $num = $Results->salary_min;
                                $units = ['', 'K', 'M'];
                                for($i = 0; $num>=1000;$i++){
                                    $num /= 1000;
                                }


                    $GetSalaryMin =  round($num, 1).$units[$i];
                    
                    $num = $Results->salary_max;
                                $units = ['', 'K', 'M'];
                                for($i = 0; $num>=1000;$i++){
                                    $num /= 1000;
                                }
                    $GetSalaryMax = round($num, 1).$units[$i];
                    
                    if($Results->employee_type == 1){
                        $GetExperience = "Fresher";
                    }else{
                        $GetExperience = $Results->experience."Years";
                    }

                    
                    $GetSearchResults = "<div class='job-listings-sec no-border'>
                    <div class='job-listing wtabs pad-top0'>
                        <div class='job-title-sec width100'>
                        <h3 class='manage-jobs-list pad-bottom10'><a href='/JobDetails/".$Results->CompanySlug."/".$Results->slug."' title=''>".$Results->title."</a></h3>
                        <div class='job-lctn'><i class='fa fa-briefcase'></i> ".$GetExperience."</div>
                            
                            <div class='job-lctn'><i class='fa fa-industry'></i> 
                            ".$GetJobType."
                            </div>
                            <div class='job-lctn'><i class='fa fa-map-marker'></i> ".$Results->city."</div>

                            <div class='job-lctn'><i class='fa fa-briefcase'></i>
                            ".$GetSalaryMin." - ".$GetSalaryMax."
                                / Month</div>

                            
                            <div class='manage-job-attributes'>
                            <div class='job-lctn'><i class='fa fa-briefcase'></i> Openings: ".$Results->total_positions."</div>
                                
                                <div class='job-lctn'><i class='fa fa-industry'></i> Posted on: ".date('d M Y', strtotime($Results->created_at))." </div>
                                <div class='job-lctn'><i class='fa fa-map-marker'></i> Job Views: 105</div>

                                <div class='job-lctn'><i class='fa fa-briefcase'></i> Job Applicant: 1</div>
                            </div>
                            <br>
                            <div class='row'>
                                <div class='col-md-4'>
                                <a  href='/ViewAllAppliedCandidates/".$Results->id."/".$Results->slug."' target='_blank'  class='view-details-jobs'> <span class='view-details-dot'>...</span> View Details</a>
                                </div>
                                
                                <div class='col-md-3'>
                                    <button type='button' onclick='EditJobs(".$Results->id.")' class='view-details-jobs'> <i class='fa fa-pencil'></i> Edit</button>
                                </div>
                                <div class='col-md-3'>
                                    <h3 class='text-left manage-jobs-status'>Published</h3>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>";
    
                    if($GetSearchResults){
                        echo $GetSearchResults;
                    }
                }
            }
        }
    }

    public function ClearAllManageJobs(Request $request){
        $GetEmployerId = Session::get("EmployerId");

        $GetDataByDatePosted[] = DB::select("SELECT DISTINCT job_title.name AS title, jobs.experience ,jobs.job_type, jobs.city, jobs.total_positions, jobs.salary_min, jobs.salary_max, jobs.location, jobs.status,
        jobs.created_at, jobs.slug, jobs.id, employer.slug As CompanySlug, jobs.employee_type FROM jobs, employer, job_title WHERE jobs.employer_id = $GetEmployerId AND job_title.id = jobs.title AND jobs.created_at AND jobs.employer_id = employer.id AND jobs.status = 0 ORDER BY jobs.created_at DESC");

        $GetSearchResults = "";
        $GetJobType = "";
        $GetSalaryMin = "";
        $GetSalaryMax ="";
        $GetExperience = "";

        if($GetDataByDatePosted){
            foreach($GetDataByDatePosted as $GetData){
                foreach($GetData as $Results){
                    if($Results->job_type == 1){
                        $GetJobType = "Part time";
                    }else{
                        $GetJobType = "Full time";
                    }
                    
                    $num = $Results->salary_min;
                                $units = ['', 'K', 'M'];
                                for($i = 0; $num>=1000;$i++){
                                    $num /= 1000;
                                }


                    $GetSalaryMin =  round($num, 1).$units[$i];
                    
                    $num = $Results->salary_max;
                                $units = ['', 'K', 'M'];
                                for($i = 0; $num>=1000;$i++){
                                    $num /= 1000;
                                }
                    $GetSalaryMax = round($num, 1).$units[$i];

                    if($Results->employee_type == 1){
                        $GetExperience = "Fresher";
                    }else{
                        $GetExperience = $Results->experience."Years";
                    }
                    
                    $GetSearchResults = "<div class='job-listings-sec no-border'>
                    <div class='job-listing wtabs pad-top0'>
                        <div class='job-title-sec width100'>
                        <h3 class='manage-jobs-list pad-bottom10'><a href='/JobDetails/".$Results->CompanySlug."/".$Results->slug."' title=''>".$Results->title."</a></h3>
                        <div class='job-lctn'><i class='fa fa-briefcase'></i> ".$GetExperience."</div>
                            
                            <div class='job-lctn'><i class='fa fa-industry'></i> 
                            ".$GetJobType."
                            </div>
                            <div class='job-lctn'><i class='fa fa-map-marker'></i> ".$Results->city."</div>

                            <div class='job-lctn'><i class='fa fa-briefcase'></i>
                            ".$GetSalaryMin." - ".$GetSalaryMax."
                                / Month</div>

                            
                            <div class='manage-job-attributes'>
                            <div class='job-lctn'><i class='fa fa-briefcase'></i> Openings: ".$Results->total_positions."</div>
                                
                                <div class='job-lctn'><i class='fa fa-industry'></i> Posted on: ".date('d M Y', strtotime($Results->created_at))." </div>
                                <div class='job-lctn'><i class='fa fa-map-marker'></i> Job Views: 105</div>

                                <div class='job-lctn'><i class='fa fa-briefcase'></i> Job Applicant: 1</div>
                            </div>
                            <br>
                            <div class='row'>
                                <div class='col-md-4'>
                                <a  href='/ViewAllAppliedCandidates/".$Results->id."/".$Results->slug."' target='_blank'  class='view-details-jobs'> <span class='view-details-dot'>...</span> View Details</a>
                                </div>
                                
                                <div class='col-md-3'>
                                    <button type='button' onclick='EditJobs(".$Results->id.")' class='view-details-jobs'> <i class='fa fa-pencil'></i> Edit</button>
                                </div>
                                <div class='col-md-3'>
                                    <h3 class='text-left manage-jobs-status'>Published</h3>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>";

                    if($GetSearchResults){
                        echo $GetSearchResults;
                    }
                }
            }
        }
    }


    public function ClearAllClosedJobs(Request $request){
        $GetEmployerId = Session::get("EmployerId");

        $GetDataByDatePosted[] = DB::select("SELECT DISTINCT job_title.name AS title, jobs.experience ,jobs.job_type, jobs.city, jobs.total_positions, jobs.salary_min, jobs.salary_max, jobs.location, jobs.status,
        jobs.created_at, jobs.slug, jobs.id, employer.slug As CompanySlug, jobs.employee_type FROM jobs, employer, job_title WHERE jobs.employer_id = employer.id AND jobs.employer_id = $GetEmployerId AND job_title.id = jobs.title AND jobs.status = 1 AND jobs.created_at ORDER BY jobs.created_at DESC");

        $GetSearchResults = "";
        $GetJobType = "";
        $GetSalaryMin = "";
        $GetSalaryMax ="";

        if($GetDataByDatePosted){
            foreach($GetDataByDatePosted as $GetData){
                foreach($GetData as $Results){
                    if($Results->job_type == 1){
                        $GetJobType = "Part time";
                    }else{
                        $GetJobType = "Full time";
                    }
                    
                    $num = $Results->salary_min;
                                $units = ['', 'K', 'M'];
                                for($i = 0; $num>=1000;$i++){
                                    $num /= 1000;
                                }


                    $GetSalaryMin =  round($num, 1).$units[$i];
                    
                    $num = $Results->salary_max;
                                $units = ['', 'K', 'M'];
                                for($i = 0; $num>=1000;$i++){
                                    $num /= 1000;
                                }
                    $GetSalaryMax = round($num, 1).$units[$i];

                    
                    $GetSearchResults = "<div class='job-listings-sec no-border'>
                    <div class='job-listing wtabs pad-top0'>
                        <div class='job-title-sec width100'>
                        <h3 class='manage-jobs-list pad-bottom10'><a href='/JobDetails/".$Results->CompanySlug."/".$Results->slug."' title=''>".$Results->title."</a></h3>
                        <div class='job-lctn'><i class='fa fa-briefcase'></i> ".$Results->experience." Years</div>
                            
                            <div class='job-lctn'><i class='fa fa-industry'></i> 
                                ".$GetJobType."
                            </div>
                            <div class='job-lctn'><i class='fa fa-map-marker'></i> ".$Results->city."</div>

                            <div class='job-lctn'><i class='fa fa-briefcase'></i>
                            ".$GetSalaryMin." - ".$GetSalaryMax."
                                / Month</div>

                            
                            <div class='manage-job-attributes'>
                            <div class='job-lctn'><i class='fa fa-briefcase'></i> Openings: ".$Results->total_positions."</div>
                                
                                <div class='job-lctn'><i class='fa fa-industry'></i> Posted on: ".date('d M Y', strtotime($Results->created_at))." </div>
                                <div class='job-lctn'><i class='fa fa-map-marker'></i> Job Views: 105</div>

                                <div class='job-lctn'><i class='fa fa-briefcase'></i> Job Applicant: 1</div>
                            </div>
                            <br>
                            <div class='row'>
                                <div class='col-md-4'>
                                <a  href='/ViewAllAppliedCandidates/".$Results->id."/".$Results->slug."' target='_blank'  class='view-details-jobs'> <span class='view-details-dot'>...</span> View Details</a>
                                </div>
                                
                                <div class='col-md-3'>
                                    <button type='button' onclick='EditJobs(".$Results->id.")' class='view-details-jobs'> <i class='fa fa-pencil'></i> Edit</button>
                                </div>
                                <div class='col-md-3'>
                                    <h3 class='text-left manage-jobs-status'>Published</h3>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>";

                    if($GetSearchResults){
                        echo $GetSearchResults;
                    }
                }
            }
        }
    }

    public function SearchManageJobs(Request $request){
        $Title = $request->title;
        // echo $Title;
        // exit;
        $GetEmployerId = Session::get("EmployerId");

        $GetDataByDatePosted[] = DB::select("SELECT DISTINCT job_title.name AS title, jobs.experience ,jobs.job_type, jobs.city, jobs.total_positions, jobs.salary_min, jobs.salary_max, jobs.location, jobs.status,
        jobs.created_at, jobs.slug, jobs.id, employer.slug As CompanySlug, jobs.employee_type FROM jobs, employer, job_title WHERE jobs.employer_id = $GetEmployerId AND job_title.id = jobs.title AND job_title.name LIKE '%{$Title}%' AND jobs.employer_id =employer.id AND jobs.status = 0 AND  jobs.created_at ORDER BY jobs.created_at DESC");

        
        $GetSearchResults = "";
        $GetJobType = "";
        $GetSalaryMin = "";
        $GetSalaryMax ="";
        $GetExperience = "";

        if($GetDataByDatePosted){
            foreach($GetDataByDatePosted as $GetData){
                foreach($GetData as $Results){
                    if($Results->job_type == 1){
                        $GetJobType = "Part time";
                    }else{
                        $GetJobType = "Full time";
                    }
                    
                    $num = $Results->salary_min;
                                $units = ['', 'K', 'M'];
                                for($i = 0; $num>=1000;$i++){
                                    $num /= 1000;
                                }


                    $GetSalaryMin =  round($num, 1).$units[$i];
                    
                    $num = $Results->salary_max;
                                $units = ['', 'K', 'M'];
                                for($i = 0; $num>=1000;$i++){
                                    $num /= 1000;
                                }
                    $GetSalaryMax = round($num, 1).$units[$i];
                    
                    if($Results->employee_type == 1){
                        $GetExperience = "Fresher";
                    }else{
                        $GetExperience = $Results->experience."Years";
                    }
                    
                    $GetSearchResults = "<div class='job-listings-sec no-border'>
                    <div class='job-listing wtabs pad-top0'>
                        <div class='job-title-sec width100'>
                        <h3 class='manage-jobs-list pad-bottom10'><a href='/JobDetails/".$Results->CompanySlug."/".$Results->slug."' title=''>".$Results->title."</a></h3>
                        <div class='job-lctn'><i class='fa fa-briefcase'></i> ".$GetExperience."</div>
                            
                            <div class='job-lctn'><i class='fa fa-industry'></i> 
                            ".$GetJobType."
                            </div>
                            <div class='job-lctn'><i class='fa fa-map-marker'></i> ".$Results->city."</div>

                            <div class='job-lctn'><i class='fa fa-briefcase'></i>
                            ".$GetSalaryMin." - ".$GetSalaryMax."
                                / Month</div>

                            
                            <div class='manage-job-attributes'>
                            <div class='job-lctn'><i class='fa fa-briefcase'></i> Openings: ".$Results->total_positions."</div>
                                
                                <div class='job-lctn'><i class='fa fa-industry'></i> Posted on: ".date('d M Y', strtotime($Results->created_at))." </div>
                                <div class='job-lctn'><i class='fa fa-map-marker'></i> Job Views: 105</div>

                                <div class='job-lctn'><i class='fa fa-briefcase'></i> Job Applicant: 1</div>
                            </div>
                            <br>
                            <div class='row'>
                                <div class='col-md-4'>
                                <a  href='/ViewAllAppliedCandidates/".$Results->id."/".$Results->slug."' target='_blank'  class='view-details-jobs'> <span class='view-details-dot'>...</span> View Details</a>
                                </div>
                                
                                <div class='col-md-3'>
                                    <button type='button' onclick='EditJobs(".$Results->id.")' class='view-details-jobs'> <i class='fa fa-pencil'></i> Edit</button>
                                </div>
                                <div class='col-md-3'>
                                    <h3 class='text-left manage-jobs-status'>Published</h3>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>";

                    if($GetSearchResults){
                        echo $GetSearchResults;
                    }
                }
            }
        }
    }

    // Search Close Jobs
    public function SearchCloseJobs(Request $request){
        $Title = $request->title;
        // echo $Title;
        // exit;
        $GetEmployerId = Session::get("EmployerId");

        $GetDataByDatePosted[] = DB::select("SELECT DISTINCT job_title.name AS title, jobs.experience ,jobs.job_type, jobs.city, jobs.total_positions, jobs.salary_min, jobs.salary_max, jobs.location, jobs.status,
        jobs.created_at, jobs.slug, jobs.id, employer.slug As CompanySlug, jobs.employee_type FROM jobs, employer, job_title WHERE jobs.employer_id = employer.id AND jobs.employer_id = $GetEmployerId AND job_title.id = jobs.title AND job_title.name LIKE '%{$Title}%' AND jobs.status = 1 AND jobs.created_at ORDER BY jobs.created_at DESC");
        

        
        $GetSearchResults = "";
        $GetJobType = "";
        $GetSalaryMin = "";
        $GetSalaryMax ="";
        $GetExperience = "";

        if($GetDataByDatePosted){
            foreach($GetDataByDatePosted as $GetData){
                foreach($GetData as $Results){
                    if($Results->job_type == 1){
                        $GetJobType = "Part time";
                    }else{
                        $GetJobType = "Full time";
                    }
                    
                    $num = $Results->salary_min;
                                $units = ['', 'K', 'M'];
                                for($i = 0; $num>=1000;$i++){
                                    $num /= 1000;
                                }


                    $GetSalaryMin =  round($num, 1).$units[$i];
                    
                    $num = $Results->salary_max;
                                $units = ['', 'K', 'M'];
                                for($i = 0; $num>=1000;$i++){
                                    $num /= 1000;
                                }
                    $GetSalaryMax = round($num, 1).$units[$i];
                    
                    if($Results->employee_type == 1){
                        $GetExperience = "Fresher";
                    }else{
                        $GetExperience = $Results->experience."Years";
                    }
                    
                    $GetSearchResults = "<div class='job-listings-sec no-border'>
                    <div class='job-listing wtabs pad-top0'>
                        <div class='job-title-sec width100'>
                        <h3 class='manage-jobs-list pad-bottom10'><a href='/JobDetails/".$Results->CompanySlug."/".$Results->slug."' title=''>".$Results->title."</a></h3>
                        <div class='job-lctn'><i class='fa fa-briefcase'></i> ".$GetExperience."</div>
                            
                            <div class='job-lctn'><i class='fa fa-industry'></i> 
                            ".$GetJobType."
                            </div>
                            <div class='job-lctn'><i class='fa fa-map-marker'></i> ".$Results->city."</div>

                            <div class='job-lctn'><i class='fa fa-briefcase'></i>
                            ".$GetSalaryMin." - ".$GetSalaryMax."
                                / Month</div>

                            
                            <div class='manage-job-attributes'>
                            <div class='job-lctn'><i class='fa fa-briefcase'></i> Openings: ".$Results->total_positions."</div>
                                
                                <div class='job-lctn'><i class='fa fa-industry'></i> Posted on: ".date('d M Y', strtotime($Results->created_at))." </div>
                                <div class='job-lctn'><i class='fa fa-map-marker'></i> Job Views: 105</div>

                                <div class='job-lctn'><i class='fa fa-briefcase'></i> Job Applicant: 1</div>
                            </div>
                            <br>
                            <div class='row'>
                                <div class='col-md-4'>
                                <a  href='/ViewAllAppliedCandidates/".$Results->id."/".$Results->slug."' target='_blank'  class='view-details-jobs'> <span class='view-details-dot'>...</span> View Details</a>
                                </div>
                                
                                <div class='col-md-3'>
                                    <button type='button' onclick='EditJobs(".$Results->id.")' class='view-details-jobs'> <i class='fa fa-pencil'></i> Edit</button>
                                </div>
                                <div class='col-md-3'>
                                    <h3 class='text-left manage-jobs-status'>Published</h3>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>";

                    if($GetSearchResults){
                        echo $GetSearchResults;
                    }
                }
            }
        }
    }



    // Employer Manage Trainings

    public function FilterDatePostedManageTrainings(Request $request){
        $GetDatePosted = $request->date_posted;

        $GetEmployerId = Session::get("EmployerId");

        $GetDataByDatePosted = [];
        
            if($GetDatePosted == 1){
                $GetDataByDatePosted[] = DB::select("SELECT DISTINCT training_title.title, training.duration, training.start_date, training.training_completion, training.stipend, training.last_date_of_application, training.training_status, training.id, training.slug, employer.slug AS CompanySlug, training.created_at, training.stipend FROM training, employer, training_title WHERE training.employer_id = $GetEmployerId AND training_title.id = training.title AND training.employer_id = employer.id AND training.training_status = 0 AND training.created_at >= (CURRENT_TIMESTAMP - INTERVAL '1' HOUR) ORDER BY training.created_at DESC");

            }elseif($GetDatePosted == 2){
                $GetDataByDatePosted[] = DB::select("SELECT DISTINCT training_title.title, training.duration, training.start_date, training.training_completion, training.stipend, training.last_date_of_application, training.training_status, training.id, training.slug, employer.slug AS CompanySlug, training.created_at, training.stipend FROM training, employer, training_title WHERE training.employer_id = $GetEmployerId AND training_title.id = training.title AND training.employer_id = employer.id AND training.training_status = 0 AND training.created_at >= (CURRENT_TIMESTAMP - INTERVAL '24' HOUR) ORDER BY training.created_at DESC");

            }elseif($GetDatePosted == 3){
                $GetDataByDatePosted[] = DB::select("SELECT DISTINCT training_title.title, training.duration, training.start_date, training.training_completion, training.stipend, training.last_date_of_application, training.training_status, training.id, training.slug, employer.slug AS CompanySlug, training.created_at, training. stipend FROM training, employer, training_title WHERE training.employer_id = $GetEmployerId AND training_title.id = training.title AND training.employer_id = employer.id AND training.training_status = 0 AND training.created_at >= DATE_ADD(CURDATE(), INTERVAL -7 DAY) ORDER BY training.created_at DESC");
            }elseif($GetDatePosted == 4){
                $GetDataByDatePosted[] = DB::select("SELECT DISTINCT training_title.title, training.duration, training.start_date, training.training_completion, training.stipend, training.last_date_of_application, training.training_status, training.id, training.slug, employer.slug AS CompanySlug, training.created_at, training.stipend FROM training, employer, training_title WHERE training.employer_id = $GetEmployerId AND training_title.id = training.title AND training.employer_id = employer.id AND training.training_status = 0 AND training.created_at ORDER BY training.created_at DESC");
            }

        // echo json_encode($GetDatePosted);
        // exit;

        $GetSearchResults = "";
        $GetTrainingCompletion = "";
        $GetDuration = "";
        $GetStipend ="";

        if($GetDataByDatePosted){
            foreach($GetDataByDatePosted as $GetData){
                foreach($GetData as $Results){
                    if($Results->training_completion == 1){
                        $GetTrainingCompletion = "Certification";
                    }else{
                        $GetTrainingCompletion = "Full Time Job";
                    }

                    // if($Results->duration == 1){
                    //     $GetDuration = "30 Days";
                    // }else{
                    //     $GetDuration = "60 Days";
                    // }
                    
                    
                    // $num = $Results->stipend;
                    //             $units = ['', 'K', 'M'];
                    //             for($i = 0; $num>=1000;$i++){
                    //                 $num /= 1000;
                    //             }
                    // $GetStipend = round($num, 1).$units[$i];

                    
                    $GetSearchResults = "<div class='job-listings-sec no-border'>
                    <div class='job-listing wtabs pad-top0'>
                        <div class='job-title-sec width100'>
                        <h3 class='manage-jobs-list pad-bottom10'><a href='#' title=''>".$Results->title."</a></h3>
                        <div class='job-lctn'><i class='fa fa-calendar'></i> ".date('d M Y', strtotime($Results->start_date))."</div>
                            
                            <div class='job-lctn'><i class='fa fa-clock-o'></i> 
                            ".$Results->duration."
                            </div>
                            <div class='job-lctn'><i class='fa fa-briefcase'></i> 
                            ".$Results->stipend."
                            
                             / Month</div>

                            <div class='job-lctn'><i class='fa fa-certificate'></i> 
                            ".$GetTrainingCompletion."
                            </div>

                            
                            <br>
                            <div class='row pad-top20'>
                                <div class='col-md-4'>
                                <a href='/ViewAllAppliedTrainees/".$Results->id."/".$Results->slug."' target='_blank'  class='view-details-jobs'> <span class='view-details-dot'>...</span> View Details</a>
                                </div>
                                
                                <div class='col-md-3'>
                                    <button type='button' onclick='EditTraining(".$Results->id.")' class='view-details-jobs'> <i class='fa fa-pencil'></i> Edit</button>
                                </div>
                                <div class='col-md-2'>
                                    <h3 class='text-left manage-jobs-status'>Published</h3>
                                </div>
                                
                                <div class='col-md-3'>
                                    <p class='last-date-taining'>Last Date: <span class='last-date-mar-pad-left'>".date('d M Y', strtotime($Results->last_date_of_application))."</span></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>";
    
                    if($GetSearchResults){
                        echo $GetSearchResults;
                    }
                }
            }
        }
    }


    // Employer Close Trainings

    public function FilterDatePostedCloseTrainings(Request $request){
        $GetDatePosted = $request->date_posted;

        $GetEmployerId = Session::get("EmployerId");

        $GetDataByDatePosted = [];
        
        if($GetDatePosted == 1){
            $GetDataByDatePosted[] = DB::select("SELECT DISTINCT training_title.title, training.duration, training.start_date, training.training_completion, training.stipend, training.last_date_of_application, training.training_status, training.id, training.slug, employer.slug AS CompanySlug, training.created_at, training.stipend FROM training, employer, training_title WHERE training.employer_id = employer.id AND training.employer_id = $GetEmployerId AND training.training_status = 1 AND training_title.id = training.title AND training.created_at >= (CURRENT_TIMESTAMP - INTERVAL '1' HOUR) ORDER BY training.created_at DESC");

        }elseif($GetDatePosted == 2){
            $GetDataByDatePosted[] = DB::select("SELECT DISTINCT training_title.title, training.duration, training.start_date, training.training_completion, training.stipend, training.last_date_of_application, training.training_status, training.id, training.slug, employer.slug AS CompanySlug, training.created_at, training.stipend FROM training, employer, training_title WHERE training.employer_id = employer.id AND training.employer_id = $GetEmployerId AND training.training_status = 1 AND training_title.id = training.title AND training.created_at >= (CURRENT_TIMESTAMP - INTERVAL '24' HOUR) ORDER BY training.created_at DESC");

        }elseif($GetDatePosted == 3){
            $GetDataByDatePosted[] = DB::select("SELECT DISTINCT training_title.title, training.duration, training.start_date, training.training_completion, training.stipend, training.last_date_of_application, training.training_status, training.id, training.slug, employer.slug AS CompanySlug, training.created_at, training. stipend FROM training, employer, training_title WHERE training.employer_id = employer.id AND training.employer_id = $GetEmployerId AND training.training_status = 1 AND training_title.id = training.title AND training.created_at >= DATE_ADD(CURDATE(), INTERVAL -7 DAY) ORDER BY training.created_at DESC");
        }elseif($GetDatePosted == 4){
            $GetDataByDatePosted[] = DB::select("SELECT DISTINCT training_title.title, training.duration, training.start_date, training.training_completion, training.stipend, training.last_date_of_application, training.training_status, training.id, training.slug, employer.slug AS CompanySlug, training.created_at, training.stipend FROM training, employer, training_title WHERE training.employer_id = employer.id AND training.employer_id = $GetEmployerId AND training.training_status = 1 AND training_title.id = training.title AND training.created_at ORDER BY training.created_at DESC");
        }

        $GetSearchResults = "";
        $GetTrainingCompletion = "";
        $GetDuration = "";
        $GetStipend ="";

        if($GetDataByDatePosted){
            foreach($GetDataByDatePosted as $GetData){
                foreach($GetData as $Results){
                    if($Results->training_completion == 1){
                        $GetTrainingCompletion = "Certification";
                    }else{
                        $GetTrainingCompletion = "Full Time Job";
                    }

                    // if($Results->duration == 1){
                    //     $GetDuration = "30 Days";
                    // }else{
                    //     $GetDuration = "60 Days";
                    // }
                    
                    
                    // $num = $Results->stipend;
                    //             $units = ['', 'K', 'M'];
                    //             for($i = 0; $num>=1000;$i++){
                    //                 $num /= 1000;
                    //             }
                    // $GetStipend = round($num, 1).$units[$i];

                    
                    $GetSearchResults = "<div class='job-listings-sec no-border'>
                    <div class='job-listing wtabs pad-top0'>
                        <div class='job-title-sec width100'>
                        <h3 class='manage-jobs-list pad-bottom10'><a href='#' title=''>".$Results->title."</a></h3>
                        <div class='job-lctn'><i class='fa fa-calendar'></i> ".date('d M Y', strtotime($Results->start_date))."</div>
                            
                            <div class='job-lctn'><i class='fa fa-clock-o'></i> 
                            ".$Results->duration."
                            </div>
                            <div class='job-lctn'><i class='fa fa-briefcase'></i> 
                            ".$Results->stipend."
                            
                             / Month</div>

                            <div class='job-lctn'><i class='fa fa-certificate'></i> 
                            ".$GetTrainingCompletion."
                            </div>

                            
                            <br>
                            <div class='row pad-top20'>
                                <div class='col-md-4'>
                                <a href='/ViewAllAppliedTrainees/".$Results->id."/".$Results->slug."' target='_blank'  class='view-details-jobs'> <span class='view-details-dot'>...</span> View Details</a>
                                </div>
                                
                                <div class='col-md-3'>
                                    <button type='button' onclick='EditTraining(".$Results->id.")' class='view-details-jobs'> <i class='fa fa-pencil'></i> Edit</button>
                                </div>
                                <div class='col-md-2'>
                                    <h3 class='text-left manage-jobs-status'>Published</h3>
                                </div>
                                
                                <div class='col-md-3'>
                                    <p class='last-date-taining'>Last Date: <span class='last-date-mar-pad-left'>".date('d M Y', strtotime($Results->last_date_of_application))."</span></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>";
    
                    if($GetSearchResults){
                        echo $GetSearchResults;
                    }
                }
            }
        }
    }


    public function FilterManageTrainings(Request $request){
        // $GetDatePosted = $request->date_posted;
        $GetEmployerId = Session::get("EmployerId");

        $GetLocation = $request->location;

        $GetDuration = preg_replace('/([0-9])/', '$1' . " ", $request->Duration);

        // echo json_encode($GetDuration);
        // exit;

        // foreach($GetDuration as $key => $value){
            
        //     $GetDurationName = explode(',', $value->skill_name);
        // }

        // $GetStipend = $request->Stipend;

        

        $GetDataByDatePosted[] =DB::table('training')
            ->select('training_title.title', 'training.duration', 'training.start_date', 'training.training_completion', 'training.stipend', 'training.last_date_of_application', 'training.training_status', 'training.id', 'training.slug', 'employer.slug AS CompanySlug', 'training.created_at','employer.city')
            ->join('employer', 'employer.id', '=', 'training.employer_id')
            ->join('training_title', 'training_title.id', '=', 'training.title')
            ->where('training.employer_id', $GetEmployerId)
            ->where('training.training_status', 0)

            // ->whereIn('job_type', $request->categories)
            ->when($GetLocation, function ($query) use ($GetLocation) {
                return $query->whereIn('employer.city', $GetLocation);
            })
                    // ->WhereIn('job_role', $request->role)
            ->when($GetDuration, function ($query) use ($GetDuration) {
                return $query->whereIn('training.duration', $GetDuration);
            })
    
            // ->when($GetStipend, function ($query) use ($GetStipend) {
            //     return $query->whereIn('stipend', $GetStipend);
            // })
            ->get();

        // echo json_encode($GetDataByDatePosted);
        // exit;

        $GetSearchResults = "";
        $GetTrainingCompletion = "";
        $GetDuration = "";
        $GetStipend ="";

        if($GetDataByDatePosted){
            foreach($GetDataByDatePosted as $GetData){
                foreach($GetData as $Results){
                    if($Results->training_completion == 1){
                        $GetTrainingCompletion = "Certification";
                    }else{
                        $GetTrainingCompletion = "Full Time Job";
                    }

                    // if($Results->duration == 1){
                    //     $GetDuration = "30 Days";
                    // }else{
                    //     $GetDuration = "60 Days";
                    // }
                    
                    
                    $num = $Results->stipend;
                                $units = ['', 'K', 'M'];
                                for($i = 0; $num>=1000;$i++){
                                    $num /= 1000;
                                }
                    $GetStipend = round($num, 1).$units[$i];

                    
                    $GetSearchResults = "<div class='job-listings-sec no-border'>
                    <div class='job-listing wtabs pad-top0'>
                        <div class='job-title-sec width100'>
                        <h3 class='manage-jobs-list pad-bottom10'><a href='#' title=''>".$Results->title."</a></h3>
                        <div class='job-lctn'><i class='fa fa-calendar'></i> ".date('d M Y', strtotime($Results->start_date))."</div>
                            
                            <div class='job-lctn'><i class='fa fa-clock-o'></i> 
                            ".$Results->duration."
                            </div>
                            <div class='job-lctn'><i class='fa fa-briefcase'></i> 
                            ".$GetStipend."
                            
                                / Month</div>

                            <div class='job-lctn'><i class='fa fa-certificate'></i> 
                            ".$GetTrainingCompletion."
                            </div>

                            
                            <br>
                            <div class='row pad-top20'>
                                <div class='col-md-4'>
                                <a href='/ViewAllAppliedTrainees/".$Results->id."/".$Results->slug."' target='_blank'  class='view-details-jobs'> <span class='view-details-dot'>...</span> View Details</a>
                                </div>
                                
                                <div class='col-md-3'>
                                    <button type='button' onclick='EditTraining(".$Results->id.")' class='view-details-jobs'> <i class='fa fa-pencil'></i> Edit</button>
                                </div>
                                <div class='col-md-2'>
                                    <h3 class='text-left manage-jobs-status'>Published</h3>
                                </div>
                                
                                <div class='col-md-3'>
                                    <p class='last-date-taining'>Last Date: <span class='last-date-mar-pad-left'>".date('d M Y', strtotime($Results->last_date_of_application))."</span></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>";
    
                    if($GetSearchResults){
                        echo $GetSearchResults;
                    }
                }
            }
        }
    }

    public function FilterCloseTrainings(Request $request){
        // $GetDatePosted = $request->date_posted;
        $GetEmployerId = Session::get("EmployerId");

        $GetLocation = $request->location;
        $GetDuration = preg_replace('/([0-9])/', '$1' . " ", $request->Duration);
        // $GetStipend = $request->Stipend;

        $GetDataByDatePosted[] =DB::table('training')
            ->select('training_title.title', 'training.duration', 'training.start_date', 'training.training_completion', 'training.stipend', 'training.last_date_of_application', 'training.training_status', 'training.id', 'training.slug', 'employer.slug AS CompanySlug', 'training.created_at','employer.city', 'training.stipend')
            ->join('employer', 'employer.id', '=', 'training.employer_id')
            ->join('training_title', 'training_title.id', '=', 'training.title')
            ->where('training.employer_id', $GetEmployerId)
            ->where('training.training_status', 1)

            // ->whereIn('job_type', $request->categories)
            ->when($GetLocation, function ($query) use ($GetLocation) {
                return $query->whereIn('city', $GetLocation);
            })
                    // ->WhereIn('job_role', $request->role)
            ->when($GetDuration, function ($query) use ($GetDuration) {
                return $query->whereIn('duration', $GetDuration);
            })
    
            // ->when($GetStipend, function ($query) use ($GetStipend) {
            //     return $query->whereIn('stipend', $GetStipend);
            // })
            ->get();

        $GetSearchResults = "";
        $GetTrainingCompletion = "";
        $GetDuration = "";
        $GetStipend ="";

        if($GetDataByDatePosted){
            foreach($GetDataByDatePosted as $GetData){
                foreach($GetData as $Results){
                    if($Results->training_completion == 1){
                        $GetTrainingCompletion = "Certification";
                    }else{
                        $GetTrainingCompletion = "Full Time Job";
                    }

                    // if($Results->duration == 1){
                    //     $GetDuration = "30 Days";
                    // }else{
                    //     $GetDuration = "60 Days";
                    // }
                    
                    
                    $num = $Results->stipend;
                                $units = ['', 'K', 'M'];
                                for($i = 0; $num>=1000;$i++){
                                    $num /= 1000;
                                }
                    $GetStipend = round($num, 1).$units[$i];

                    
                    $GetSearchResults = "<div class='job-listings-sec no-border'>
                    <div class='job-listing wtabs pad-top0'>
                        <div class='job-title-sec width100'>
                        <h3 class='manage-jobs-list pad-bottom10'><a href='#' title=''>".$Results->title."</a></h3>
                        <div class='job-lctn'><i class='fa fa-calendar'></i> ".date('d M Y', strtotime($Results->start_date))."</div>
                            
                            <div class='job-lctn'><i class='fa fa-clock-o'></i> 
                            ".$Results->duration."
                            </div>
                            <div class='job-lctn'><i class='fa fa-briefcase'></i> 
                            ".$GetStipend."
                            
                                / Month</div>

                            <div class='job-lctn'><i class='fa fa-certificate'></i> 
                            ".$GetTrainingCompletion."
                            </div>

                            
                            <br>
                            <div class='row pad-top20'>
                                <div class='col-md-4'>
                                <a href='/ViewAllAppliedTrainees/".$Results->id."/".$Results->slug."' target='_blank'  class='view-details-jobs'> <span class='view-details-dot'>...</span> View Details</a>
                                </div>
                                
                                <div class='col-md-3'>
                                    <button type='button' onclick='EditTraining(".$Results->id.")' class='view-details-jobs'> <i class='fa fa-pencil'></i> Edit</button>
                                </div>
                                <div class='col-md-2'>
                                    <h3 class='text-left manage-jobs-status'>Published</h3>
                                </div>
                                
                                <div class='col-md-3'>
                                    <p class='last-date-taining'>Last Date: <span class='last-date-mar-pad-left'>".date('d M Y', strtotime($Results->last_date_of_application))."</span></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>";
    
                    if($GetSearchResults){
                        echo $GetSearchResults;
                    }
                }
            }
        }
    }

    public function SearchManageTrainings(Request $request){
        $Title = $request->title;
        // echo $Title;
        // exit;
        $GetEmployerId = Session::get("EmployerId");

        $GetDataByDatePosted[] = DB::select("SELECT DISTINCT training_title.title, training.duration, training.start_date, training.training_completion, training.stipend, training.last_date_of_application, training.training_status, training.id, training.slug, employer.slug AS CompanySlug, training.created_at FROM training, employer, training_title WHERE training.employer_id = $GetEmployerId AND training_title.id = training.title AND training.training_status = 0 AND training.employer_id = employer.id AND training_title.title LIKE '%{$Title}%' AND training.created_at ORDER BY training.created_at DESC");

        $GetSearchResults = "";
        $GetTrainingCompletion = "";
        $GetDuration = "";
        $GetStipend ="";

        if($GetDataByDatePosted){
            foreach($GetDataByDatePosted as $GetData){
                foreach($GetData as $Results){
                    if($Results->training_completion == 1){
                        $GetTrainingCompletion = "Certification";
                    }else{
                        $GetTrainingCompletion = "Full Time Job";
                    }

                    // if($Results->duration == 1){
                    //     $GetDuration = "30 Days";
                    // }else{
                    //     $GetDuration = "60 Days";
                    // }
                    
                    
                    // $num = $Results->stipend;
                    //             $units = ['', 'K', 'M'];
                    //             for($i = 0; $num>=1000;$i++){
                    //                 $num /= 1000;
                    //             }
                    // $GetStipend = round($num, 1).$units[$i];

                    
                    $GetSearchResults = "<div class='job-listings-sec no-border'>
                    <div class='job-listing wtabs pad-top0'>
                        <div class='job-title-sec width100'>
                        <h3 class='manage-jobs-list pad-bottom10'><a href='#' title=''>".$Results->title."</a></h3>
                        <div class='job-lctn'><i class='fa fa-calendar'></i> ".date('d M Y', strtotime($Results->start_date))."</div>
                            
                            <div class='job-lctn'><i class='fa fa-clock-o'></i> 
                            ".$Results->duration."
                            </div>
                            <div class='job-lctn'><i class='fa fa-briefcase'></i> 
                            ".$Results->stipend."
                            
                                / Month</div>

                            <div class='job-lctn'><i class='fa fa-certificate'></i> 
                            ".$GetTrainingCompletion."
                            </div>

                            
                            <br>
                            <div class='row pad-top20'>
                                <div class='col-md-4'>
                                <a href='/ViewAllAppliedTrainees/".$Results->id."/".$Results->slug."' target='_blank'  class='view-details-jobs'> <span class='view-details-dot'>...</span> View Details</a>
                                </div>
                                
                                <div class='col-md-3'>
                                    <button type='button' onclick='EditTraining(".$Results->id.")' class='view-details-jobs'> <i class='fa fa-pencil'></i> Edit</button>
                                </div>
                                <div class='col-md-2'>
                                    <h3 class='text-left manage-jobs-status'>Published</h3>
                                </div>
                                
                                <div class='col-md-3'>
                                    <p class='last-date-taining'>Last Date: <span class='last-date-mar-pad-left'>".date('d M Y', strtotime($Results->last_date_of_application))."</span></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>";
    
                    if($GetSearchResults){
                        echo $GetSearchResults;
                    }
                }
            }
        }
    }


    public function SearchCloseTrainings(Request $request){
        $Title = $request->title;
        // echo $Title;
        // exit;
        $GetEmployerId = Session::get("EmployerId");

        $GetDataByDatePosted[] = DB::select("SELECT DISTINCT training_title.title, training.duration, training.start_date, training.training_completion, training.stipend, training.last_date_of_application, training.training_status, training.id, training.slug, employer.slug AS CompanySlug, training.created_at, training.stipend FROM training, employer, training_title WHERE training.employer_id = $GetEmployerId AND training.employer_id = employer.id AND training_title.id = training.title AND training_title.title LIKE '%{$Title}%' AND training.created_at AND training.training_status = 1 ORDER BY training.created_at DESC");

        $GetSearchResults = "";
        $GetTrainingCompletion = "";
        $GetDuration = "";
        $GetStipend ="";

        if($GetDataByDatePosted){
            foreach($GetDataByDatePosted as $GetData){
                foreach($GetData as $Results){
                    if($Results->training_completion == 1){
                        $GetTrainingCompletion = "Certification";
                    }else{
                        $GetTrainingCompletion = "Full Time Job";
                    }

                    if($Results->duration == 1){
                        $GetDuration = "30 Days";
                    }else{
                        $GetDuration = "60 Days";
                    }
                    
                    
                    $num = $Results->stipend;
                                $units = ['', 'K', 'M'];
                                for($i = 0; $num>=1000;$i++){
                                    $num /= 1000;
                                }
                    $GetStipend = round($num, 1).$units[$i];

                    
                    $GetSearchResults = "<div class='job-listings-sec no-border'>
                    <div class='job-listing wtabs pad-top0'>
                        <div class='job-title-sec width100'>
                        <h3 class='manage-jobs-list pad-bottom10'><a href='#' title=''>".$Results->title."</a></h3>
                        <div class='job-lctn'><i class='fa fa-calendar'></i> ".date('d M Y', strtotime($Results->start_date))."</div>
                            
                            <div class='job-lctn'><i class='fa fa-clock-o'></i> 
                            ".$GetDuration."
                            </div>
                            <div class='job-lctn'><i class='fa fa-briefcase'></i> 
                            ".$GetStipend."
                            
                                / Month</div>

                            <div class='job-lctn'><i class='fa fa-certificate'></i> 
                            ".$GetTrainingCompletion."
                            </div>

                            
                            <br>
                            <div class='row pad-top20'>
                                <div class='col-md-4'>
                                <a href='/ViewAllAppliedTrainees/".$Results->id."/".$Results->slug."' target='_blank'  class='view-details-jobs'> <span class='view-details-dot'>...</span> View Details</a>
                                </div>
                                
                                <div class='col-md-3'>
                                    <button type='button' onclick='EditTraining(".$Results->id.")' class='view-details-jobs'> <i class='fa fa-pencil'></i> Edit</button>
                                </div>
                                <div class='col-md-2'>
                                    <h3 class='text-left manage-jobs-status'>Published</h3>
                                </div>
                                
                                <div class='col-md-3'>
                                    <p class='last-date-taining'>Last Date: <span class='last-date-mar-pad-left'>".date('d M Y', strtotime($Results->last_date_of_application))."</span></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>";
    
                    if($GetSearchResults){
                        echo $GetSearchResults;
                    }
                }
            }
        }
    }


    public function ClearAllManageTrainings(Request $request){
        $GetEmployerId = Session::get("EmployerId");

        $GetDataByDatePosted[] = DB::select("SELECT DISTINCT training_title.title, training.duration, training.start_date, training.training_completion, training.stipend, training.last_date_of_application, training.training_status, training.id, training.slug, employer.slug AS CompanySlug, training.created_at, training.stipend FROM training, employer, training_title WHERE training.employer_id = employer.id AND training_title.id = training.title AND training.training_status = 0 AND training.employer_id = $GetEmployerId AND training.created_at ORDER BY training.created_at DESC");

        $GetSearchResults = "";
        $GetTrainingCompletion = "";
        $GetDuration = "";
        $GetStipend ="";

        if($GetDataByDatePosted){
            foreach($GetDataByDatePosted as $GetData){
                foreach($GetData as $Results){
                    if($Results->training_completion == 1){
                        $GetTrainingCompletion = "Certification";
                    }else{
                        $GetTrainingCompletion = "Full Time Job";
                    }

                    // if($Results->duration == 1){
                    //     $GetDuration = "30 Days";
                    // }else{
                    //     $GetDuration = "60 Days";
                    // }
                    
                    
                    // $num = $Results->stipend;
                    //             $units = ['', 'K', 'M'];
                    //             for($i = 0; $num>=1000;$i++){
                    //                 $num /= 1000;
                    //             }
                    // $GetStipend = round($num, 1).$units[$i];

                    
                    $GetSearchResults = "<div class='job-listings-sec no-border'>
                    <div class='job-listing wtabs pad-top0'>
                        <div class='job-title-sec width100'>
                        <h3 class='manage-jobs-list pad-bottom10'><a href='#' title=''>".$Results->title."</a></h3>
                        <div class='job-lctn'><i class='fa fa-calendar'></i> ".date('d M Y', strtotime($Results->start_date))."</div>
                            
                            <div class='job-lctn'><i class='fa fa-clock-o'></i> 
                            ".$Results->duration."
                            </div>
                            <div class='job-lctn'><i class='fa fa-briefcase'></i> 
                            ".$Results->stipend."
                            
                                / Month</div>

                            <div class='job-lctn'><i class='fa fa-certificate'></i> 
                            ".$GetTrainingCompletion."
                            </div>

                            
                            <br>
                            <div class='row pad-top20'>
                                <div class='col-md-4'>
                                <a href='/ViewAllAppliedTrainees/".$Results->id."/".$Results->slug."' target='_blank'  class='view-details-jobs'> <span class='view-details-dot'>...</span> View Details</a>
                                </div>
                                
                                <div class='col-md-3'>
                                    <button type='button' onclick='EditTraining(".$Results->id.")' class='view-details-jobs'> <i class='fa fa-pencil'></i> Edit</button>
                                </div>
                                <div class='col-md-2'>
                                    <h3 class='text-left manage-jobs-status'>Published</h3>
                                </div>
                                
                                <div class='col-md-3'>
                                    <p class='last-date-taining'>Last Date: <span class='last-date-mar-pad-left'>".date('d M Y', strtotime($Results->last_date_of_application))."</span></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>";
    
                    if($GetSearchResults){
                        echo $GetSearchResults;
                    }
                }
            }
        }
    }


    public function ClearAllCloseTrainings(Request $request){
        $GetEmployerId = Session::get("EmployerId");

        $GetDataByDatePosted[] = DB::select("SELECT DISTINCT training_title.title, training.duration, training.start_date, training.training_completion, training.stipend, training.last_date_of_application, training.training_status, training.id, training.slug, employer.slug AS CompanySlug, training.created_at FROM training, employer, training_title WHERE training.employer_id = $GetEmployerId AND training_title.id = training.title AND training.training_status = 1 AND training.created_at ORDER BY training.created_at DESC");

        $GetSearchResults = "";
        $GetTrainingCompletion = "";
        $GetDuration = "";
        $GetStipend ="";

        if($GetDataByDatePosted){
            foreach($GetDataByDatePosted as $GetData){
                foreach($GetData as $Results){
                    if($Results->training_completion == 1){
                        $GetTrainingCompletion = "Certification";
                    }else{
                        $GetTrainingCompletion = "Full Time Job";
                    }

                    // if($Results->duration == 1){
                    //     $GetDuration = "30 Days";
                    // }else{
                    //     $GetDuration = "60 Days";
                    // }
                    
                    
                    // $num = $Results->stipend;
                    //             $units = ['', 'K', 'M'];
                    //             for($i = 0; $num>=1000;$i++){
                    //                 $num /= 1000;
                    //             }
                    // $GetStipend = round($num, 1).$units[$i];

                    
                    $GetSearchResults = "<div class='job-listings-sec no-border'>
                    <div class='job-listing wtabs pad-top0'>
                        <div class='job-title-sec width100'>
                        <h3 class='manage-jobs-list pad-bottom10'><a href='#' title=''>".$Results->title."</a></h3>
                        <div class='job-lctn'><i class='fa fa-calendar'></i> ".date('d M Y', strtotime($Results->start_date))."</div>
                            
                            <div class='job-lctn'><i class='fa fa-clock-o'></i> 
                            ".$Results->duration."
                            </div>
                            <div class='job-lctn'><i class='fa fa-briefcase'></i> 
                            ".$Results->stipend."
                            
                                / Month</div>

                            <div class='job-lctn'><i class='fa fa-certificate'></i> 
                            ".$GetTrainingCompletion."
                            </div>

                            <br>
                            <div class='row pad-top20'>
                                <div class='col-md-4'>
                                <a href='/ViewAllAppliedTrainees/".$Results->id."/".$Results->slug."' target='_blank'  class='view-details-jobs'> <span class='view-details-dot'>...</span> View Details</a>
                                </div>
                                
                                <div class='col-md-3'>
                                    <button type='button' onclick='EditTraining(".$Results->id.")' class='view-details-jobs'> <i class='fa fa-pencil'></i> Edit</button>
                                </div>
                                <div class='col-md-2'>
                                    <h3 class='text-left manage-jobs-status'>Published</h3>
                                </div>
                                
                                <div class='col-md-3'>
                                    <p class='last-date-taining'>Last Date: <span class='last-date-mar-pad-left'>".date('d M Y', strtotime($Results->last_date_of_application))."</span></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>";
    
                    if($GetSearchResults){
                        echo $GetSearchResults;
                    }
                }
            }
        }
    }


    // Employer Manage Candidates

    public function FilterDatePostedCandidates(Request $request){
        $GetDatePosted = $request->date_posted;

        $GetEmployerId = Session::get("EmployerId");

        $GetCandidateId = "";

        $GetDataByDatePosted = [];
        
            if($GetDatePosted == 1){
                $GetDataByDatePosted[] = DB::select("SELECT * FROM candidate, candidate_profile WHERE candidate.id = candidate_profile.candidate_id AND candidate.type = 1 AND candidate.created_at >= (CURRENT_TIMESTAMP - INTERVAL '1' HOUR) ORDER BY candidate.id DESC");

            }elseif($GetDatePosted == 2){
                $GetDataByDatePosted[] = DB::select("SELECT * FROM candidate, candidate_profile WHERE candidate.id = candidate_profile.candidate_id AND candidate.type = 1 AND candidate.created_at >= (CURRENT_TIMESTAMP - INTERVAL '24' HOUR) ORDER BY candidate.created_at DESC");

            }elseif($GetDatePosted == 3){
                $GetDataByDatePosted[] = DB::select("SELECT * FROM candidate, candidate_profile WHERE candidate.id = candidate_profile.candidate_id AND candidate.type = 1 AND candidate.created_at >= DATE_ADD(CURDATE(), INTERVAL -7 DAY) ORDER BY candidate.created_at DESC");
            }elseif($GetDatePosted == 4){
                $GetDataByDatePosted[] = DB::select("SELECT * FROM candidate, candidate_profile WHERE candidate.id = candidate_profile.candidate_id AND candidate.type = 1 ORDER BY candidate.created_at DESC");
            }

        foreach($GetDataByDatePosted as $Posted){
            foreach($Posted as $Data){
                $GetCandidateId = $Data->id;
            }
        }

        $CheckShortlisted = DB::select("SELECT * FROM candidate, candidate_profile, shortlist_candidates WHERE candidate.id = candidate_profile.candidate_id AND shortlist_candidates.candidate_id = '$GetCandidateId' AND candidate.type = 1 ORDER BY candidate.id ASC");
        

        $GetProfessionalDetails = DB::table('candidate')
                                ->join('resume', 'candidate.id', '=', 'resume.candidate_id')
                                ->where('resume.worked_till', '=', 1)
                                ->first();

        $GetCV = DB::table('candidate')
                ->join('resume', 'candidate.id', '=', 'resume.candidate_id')
                ->where('resume.type', '=', 4)
                ->first();



        $GetSearchResults = "";
        $GetCanddiateProfessionalDetails = "";
        $GetCandidateCV = "";
        $GetShortlisted ="";

        if($GetDataByDatePosted){
            foreach($GetDataByDatePosted as $GetData){
                foreach($GetData as $Results){
                    if($CheckShortlisted){
                        $GetShortlisted = "<p class='shortlisted-success' id='ShortlistedSuccess'><i class='fa fa-check'></i> Shortlisted</p>";
                    }else{
                        $GetShortlisted = "<a href='javascript:void(0);' id='AddShortlisted' onclick='AddShortlisted(".$Results->candidate_id.")'>
                        <p class='width100 shortlist-jobs shortlist-pad-mar'>
                            <span class='shortlist-icon'>
                                <i class='fa fa-heart-o'> </i>
                            </span>
                        <span class='shortlist-bold pad-left5'> Shortlist</span></p>
                    </a>";
                    }

                    if($GetCV){
                        $GetCandidateCV = "<a href='/candidate_cv/".$GetCV->cv."' title='' class='for-employers-btn post-a-job-home text-center download-cv-mar' download>DOWNLOAD CV</a>";
                    }else{
                        $GetCandidateCV = "<a href='javascript:void(0);' title='' class='for-employers-btn post-a-job-home text-center download-cv-mar'>DOWNLOAD CV</a>";
                    }


                    if($GetProfessionalDetails){
                        $GetCanddiateProfessionalDetails = "<div class='job-lctn'> 
                        ".$GetProfessionalDetails->department." at ".$GetProfessionalDetails->name."
                    </div>";
                    }
                    
                    
                    $GetSearchResults = "<div class='job-listings-sec no-border'>
                    <div class='job-listing wtabs pad-top0 candidate-list-mar-top'>
                        <div class='job-title-sec width100'>
                            <div class='col-md-1 pull-left pad-left0'>
                                <img class='candidate-list-profile-pic' src='/candidate_profile/".$Results->profile_pic."' alt='' />
                            </div>
                            <div class='col-md-7 pull-left'>
                            <a href='/CandidateDetails/".$Results->id."/".$Results->slug."' target='_blank'>
                                <h3 class='candidate-list-name'>".$Results->name."</h3>
                            </a>
                                ".$GetCanddiateProfessionalDetails."
                                <br>
                                <div class='job-lctn'><i class='fa fa-map-marker'></i> ".$Results->desired_location."</div>

                                <div class='job-lctn'><i class='fa fa-briefcase'></i>  4-6 Years</div>

                                <div class='job-lctn'><i class='fa fa-briefcase'></i> 55K-60K / Month</div>

                            <div>
                        
                            </div>
                        </div>
                        <div class='col-md-4 pull-left'>
                            <img id='ApplyBtnLoader' class='candidate-shortlisted-loader' src='{{URL::asset('UI/otp_verify_loader.gif')}}' alt='' style='display:none;'>

                            <p class='shortlisted-success' id='ShortlistedSuccess' style='display:none;'><i class='fa fa-check'></i> Shortlisted</p>

                            ".$GetShortlisted."
                            ".$GetCandidateCV."
                        </div>
                        
                    </div>
                    <hr class='candidate-list-border-line'>
                </div>";
    
                    if($GetSearchResults){
                        echo $GetSearchResults;
                    }
                }
            }
        }
    }


    public function FilterManageCandidates(Request $request){
        // $GetDatePosted = $request->date_posted;
        $GetEmployerId = Session::get("EmployerId");

        // $GetLocation = $request->location;
        // $GetExperience = $request->Experience;
        // $GetIndustry = $request->Industry;
        $GetLocationByArr = preg_replace('/([-])/', '$1' . " ", $request->location);

        $GetLocation = str_replace('-', '', $GetLocationByArr);

        // echo json_encode($GetLocation);
        // exit;

        $GetCandidateId = "";

        $GetDataByDatePosted[] =DB::table('candidate')
            ->select('candidate.*', 'candidate_profile.*')

            ->join('candidate_profile', 'candidate_profile.candidate_id', '=', 'candidate.id')
            ->where('type', 1)

            // ->whereIn('job_type', $request->categories)
            ->when($GetLocation, function ($query) use ($GetLocation) {
                return $query->whereIn('candidate_profile.city', $GetLocation);
            })
                    // ->WhereIn('job_role', $request->role)
            // ->when($GetExperience, function ($query) use ($GetExperience) {
            //     return $query->whereIn('experience', $GetExperience);
            // })
    
            // ->when($GetIndustry, function ($query) use ($GetIndustry) {
            //     return $query->whereIn('industry', $GetIndustry);
            // })
            ->get();

        //     echo json_encode($GetLocation);
        // exit;
            
            foreach($GetDataByDatePosted as $Posted){
                foreach($Posted as $Data){
                    $GetCandidateId = $Data->id;
                }
            }
    
            $CheckShortlisted = DB::select("SELECT * FROM candidate, candidate_profile, shortlist_candidates WHERE candidate.id = candidate_profile.candidate_id AND shortlist_candidates.candidate_id = '$GetCandidateId' AND candidate.type = 1 ORDER BY candidate.id ASC");
            
    
            $GetProfessionalDetails = DB::table('candidate')
                                    ->join('resume', 'candidate.id', '=', 'resume.candidate_id')
                                    ->where('resume.worked_till', '=', 1)
                                    ->first();
    
            $GetCV = DB::table('candidate')
                    ->join('resume', 'candidate.id', '=', 'resume.candidate_id')
                    ->where('resume.type', '=', 4)
                    ->first();
    
    
    
            $GetSearchResults = "";
            $GetCanddiateProfessionalDetails = "";
            $GetCandidateCV = "";
            $GetShortlisted ="";
    
            if($GetDataByDatePosted){
                foreach($GetDataByDatePosted as $GetData){
                    foreach($GetData as $Results){
                        if($CheckShortlisted){
                            $GetShortlisted = "<p class='shortlisted-success' id='ShortlistedSuccess'><i class='fa fa-check'></i> Shortlisted</p>";
                        }else{
                            $GetShortlisted = "<a href='javascript:void(0);' id='AddShortlisted' onclick='AddShortlisted(".$Results->candidate_id.")'>
                            <p class='width100 shortlist-jobs shortlist-pad-mar'>
                                <span class='shortlist-icon'>
                                    <i class='fa fa-heart-o'> </i>
                                </span>
                            <span class='shortlist-bold pad-left5'> Shortlist</span></p>
                        </a>";
                        }
    
                        if($GetCV){
                            $GetCandidateCV = "<a href='/candidate_cv/".$GetCV->cv."' title='' class='for-employers-btn post-a-job-home text-center download-cv-mar' download>DOWNLOAD CV</a>";
                        }else{
                            $GetCandidateCV = "<a href='javascript:void(0);' title='' class='for-employers-btn post-a-job-home text-center download-cv-mar'>DOWNLOAD CV</a>";
                        }
    
    
                        if($GetProfessionalDetails){
                            $GetCanddiateProfessionalDetails = "<div class='job-lctn'> 
                            ".$GetProfessionalDetails->department." at ".$GetProfessionalDetails->name."
                        </div>";
                        }
                        
                        
                        $GetSearchResults = "<div class='job-listings-sec no-border'>
                        <div class='job-listing wtabs pad-top0 candidate-list-mar-top'>
                            <div class='job-title-sec width100'>
                                <div class='col-md-1 pull-left pad-left0'>
                                    <img class='candidate-list-profile-pic' src='/candidate_profile/".$Results->profile_pic."' alt='' />
                                </div>
                                <div class='col-md-7 pull-left'>
                                <a href='/CandidateDetails/".$Results->id."/".$Results->slug."' target='_blank'>
                                    <h3 class='candidate-list-name'>".$Results->name."</h3>
                                </a>
                                    ".$GetCanddiateProfessionalDetails."
                                    <br>
                                    <div class='job-lctn'><i class='fa fa-map-marker'></i> ".$Results->desired_location."</div>
    
                                    <div class='job-lctn'><i class='fa fa-briefcase'></i>  4-6 Years</div>
    
                                    <div class='job-lctn'><i class='fa fa-briefcase'></i> 55K-60K / Month</div>
    
                                <div>
                            
                                </div>
                            </div>
                            <div class='col-md-4 pull-left'>
                                <img id='ApplyBtnLoader' class='candidate-shortlisted-loader' src='{{URL::asset('UI/otp_verify_loader.gif')}}' alt='' style='display:none;'>
    
                                <p class='shortlisted-success' id='ShortlistedSuccess' style='display:none;'><i class='fa fa-check'></i> Shortlisted</p>
    
                                ".$GetShortlisted."
                                ".$GetCandidateCV."
                            </div>
                            
                        </div>
                        <hr class='candidate-list-border-line'>
                    </div>";
        
                        if($GetSearchResults){
                            echo $GetSearchResults;
                        }
                    }
                }
            }
    }


    public function SearchManageCandidates(Request $request){
        $Title = $request->title;
        // echo $Title;
        // exit;
        $GetCandidateId = "";


        $GetDataByDatePosted[] = DB::select("SELECT candidate.*,candidate_profile.*, resume.*, job_title.name AS JobTitle   FROM candidate, candidate_profile, resume, job_title WHERE candidate.id = resume.candidate_id AND candidate.id = candidate_profile.candidate_id AND candidate.type = 1 AND job_title.name LIKE '%{$Title}%' AND resume.department = job_title.id AND resume.worked_till = 1 ORDER BY candidate.created_at DESC");

        foreach($GetDataByDatePosted as $Posted){
            foreach($Posted as $Data){
                $GetCandidateId = $Data->id;
            }
        }

        $CheckShortlisted = DB::select("SELECT * FROM candidate, candidate_profile, shortlist_candidates WHERE candidate.id = candidate_profile.candidate_id AND shortlist_candidates.candidate_id = '$GetCandidateId' AND candidate.type = 1 ORDER BY candidate.id ASC");
        

        $GetProfessionalDetails = DB::table('candidate')
                                ->select('resume.name', 'job_title.name AS JobTitle')
                                ->join('resume', 'candidate.id', '=', 'resume.candidate_id')
                                ->join('job_title', 'job_title.id', '=', 'resume.department')
                                ->where('resume.worked_till', '=', 1)
                                ->first();

        $GetCV = DB::table('candidate')
                ->join('resume', 'candidate.id', '=', 'resume.candidate_id')
                ->where('resume.type', '=', 4)
                ->first();



        $GetSearchResults = "";
        $GetCanddiateProfessionalDetails = "";
        $GetCandidateCV = "";
        $GetShortlisted ="";

        if($GetDataByDatePosted){
            foreach($GetDataByDatePosted as $GetData){
                foreach($GetData as $Results){
                    if($CheckShortlisted){
                        $GetShortlisted = "<p class='shortlisted-success' id='ShortlistedSuccess'><i class='fa fa-check'></i> Shortlisted</p>";
                    }else{
                        $GetShortlisted = "<a href='javascript:void(0);' id='AddShortlisted' onclick='AddShortlisted(".$Results->candidate_id.")'>
                        <p class='width100 shortlist-jobs shortlist-pad-mar'>
                            <span class='shortlist-icon'>
                                <i class='fa fa-heart-o'> </i>
                            </span>
                        <span class='shortlist-bold pad-left5'> Shortlist</span></p>
                    </a>";
                    }

                    if($GetCV){
                        $GetCandidateCV = "<a href='/candidate_cv/".$GetCV->cv."' title='' class='for-employers-btn post-a-job-home text-center download-cv-mar' download>DOWNLOAD CV</a>";
                    }else{
                        $GetCandidateCV = "<a href='javascript:void(0);' title='' class='for-employers-btn post-a-job-home text-center download-cv-mar'>DOWNLOAD CV</a>";
                    }


                    if($GetProfessionalDetails){
                        $GetCanddiateProfessionalDetails = "<div class='job-lctn'> 
                        ".$GetProfessionalDetails->JobTitle." at ".$GetProfessionalDetails->name."
                    </div>";
                    }
                    
                    
                    $GetSearchResults = "<div class='job-listings-sec no-border'>
                    <div class='job-listing wtabs pad-top0 candidate-list-mar-top'>
                        <div class='job-title-sec width100'>
                            <div class='col-md-1 pull-left pad-left0'>
                                <img class='candidate-list-profile-pic' src='/candidate_profile/".$Results->profile_pic."' alt='' />
                            </div>
                            <div class='col-md-7 pull-left'>
                            <a href='/CandidateDetails/".$Results->id."/".$Results->slug."' target='_blank'>
                                <h3 class='candidate-list-name'>".$Results->JobTitle."</h3>
                            </a>
                                ".$GetCanddiateProfessionalDetails."
                                <br>
                                <div class='job-lctn'><i class='fa fa-map-marker'></i> ".$Results->desired_location."</div>

                                <div class='job-lctn'><i class='fa fa-briefcase'></i>  4-6 Years</div>

                                <div class='job-lctn'><i class='fa fa-briefcase'></i> 55K-60K / Month</div>

                            <div>
                        
                            </div>
                        </div>
                        <div class='col-md-4 pull-left'>
                            <img id='ApplyBtnLoader' class='candidate-shortlisted-loader' src='{{URL::asset('UI/otp_verify_loader.gif')}}' alt='' style='display:none;'>

                            <p class='shortlisted-success' id='ShortlistedSuccess' style='display:none;'><i class='fa fa-check'></i> Shortlisted</p>

                            ".$GetShortlisted."
                            ".$GetCandidateCV."
                        </div>
                        
                    </div>
                    <hr class='candidate-list-border-line'>
                </div>";
    
                    if($GetSearchResults){
                        echo $GetSearchResults;
                    }
                }
            }
        }
    }

    public function ClearAllManageCandidates(Request $request){

        $GetCandidateId = "";

        $GetDataByDatePosted[] = DB::select("SELECT * FROM candidate, candidate_profile WHERE candidate.id = candidate_profile.candidate_id AND candidate.type = 1 AND candidate.created_at ORDER BY candidate.created_at DESC");

        foreach($GetDataByDatePosted as $Posted){
            foreach($Posted as $Data){
                $GetCandidateId = $Data->id;
            }
        }

        $CheckShortlisted = DB::select("SELECT * FROM candidate, candidate_profile, shortlist_candidates WHERE candidate.id = candidate_profile.candidate_id AND shortlist_candidates.candidate_id = '$GetCandidateId' AND candidate.type = 1 ORDER BY candidate.id ASC");
        

        $GetProfessionalDetails = DB::table('candidate')
                                ->select('resume.name', 'job_title.name AS JobTitle')
                                ->join('resume', 'candidate.id', '=', 'resume.candidate_id')
                                ->join('job_title', 'job_title.id', '=', 'resume.department')
                                ->where('resume.worked_till', '=', 1)
                                ->first();

        $GetCV = DB::table('candidate')
                ->join('resume', 'candidate.id', '=', 'resume.candidate_id')
                ->where('resume.type', '=', 4)
                ->first();



        $GetSearchResults = "";
        $GetCanddiateProfessionalDetails = "";
        $GetCandidateCV = "";
        $GetShortlisted ="";

        if($GetDataByDatePosted){
            foreach($GetDataByDatePosted as $GetData){
                foreach($GetData as $Results){
                    if($CheckShortlisted){
                        $GetShortlisted = "<p class='shortlisted-success' id='ShortlistedSuccess'><i class='fa fa-check'></i> Shortlisted</p>";
                    }else{
                        $GetShortlisted = "<a href='javascript:void(0);' id='AddShortlisted' onclick='AddShortlisted(".$Results->candidate_id.")'>
                        <p class='width100 shortlist-jobs shortlist-pad-mar'>
                            <span class='shortlist-icon'>
                                <i class='fa fa-heart-o'> </i>
                            </span>
                        <span class='shortlist-bold pad-left5'> Shortlist</span></p>
                    </a>";
                    }

                    if($GetCV){
                        $GetCandidateCV = "<a href='/candidate_cv/".$GetCV->cv."' title='' class='for-employers-btn post-a-job-home text-center download-cv-mar' download>DOWNLOAD CV</a>";
                    }else{
                        $GetCandidateCV = "<a href='javascript:void(0);' title='' class='for-employers-btn post-a-job-home text-center download-cv-mar'>DOWNLOAD CV</a>";
                    }


                    if($GetProfessionalDetails){
                        $GetCanddiateProfessionalDetails = "<div class='job-lctn'> 
                        ".$GetProfessionalDetails->JobTitle." at ".$GetProfessionalDetails->name."
                    </div>";
                    }
                    
                    
                    $GetSearchResults = "<div class='job-listings-sec no-border'>
                    <div class='job-listing wtabs pad-top0 candidate-list-mar-top pad-bottom0'>
                        <div class='job-title-sec width100'>
                            <div class='col-md-1 pull-left pad-left0'>
                                <img class='candidate-list-profile-pic' src='/candidate_profile/".$Results->profile_pic."' alt='' />
                            </div>
                            <div class='col-md-7 pull-left'>
                            <a href='/CandidateDetails/".$Results->id."/".$Results->slug."' target='_blank'>
                                <h3 class='candidate-list-name'>".$Results->name."</h3>
                            </a>
                                ".$GetCanddiateProfessionalDetails."
                                <br>
                                <div class='job-lctn'><i class='fa fa-map-marker'></i> ".$Results->desired_location."</div>

                                <div class='job-lctn'><i class='fa fa-briefcase'></i>  4-6 Years</div>

                                <div class='job-lctn'><i class='fa fa-briefcase'></i> 55K-60K / Month</div>

                            <div>
                        
                            </div>
                        </div>
                        <div class='col-md-4 pull-left'>
                            <img id='ApplyBtnLoader' class='candidate-shortlisted-loader' src='{{URL::asset('UI/otp_verify_loader.gif')}}' alt='' style='display:none;'>

                            <p class='shortlisted-success' id='ShortlistedSuccess' style='display:none;'><i class='fa fa-check'></i> Shortlisted</p>

                            ".$GetShortlisted."
                            ".$GetCandidateCV."
                        </div>
                        
                    </div>
                    <hr class='candidate-list-border-line'>
                </div>";
    
                    if($GetSearchResults){
                        echo $GetSearchResults;
                    }
                }
            }
        }
    }

    public function FilterShortlistedCandidates(Request $request){
        // $GetDatePosted = $request->date_posted;
        $GetEmployerId = Session::get("EmployerId");

        $GetShortlisted = $request->candidateshortlistType;

        $GetCandidateId = "";

        $GetDataByDatePosted[] =DB::table('candidate')
            ->select('candidate.*', 'candidate_profile.*', 'shortlist_candidates.shortlisted_type')

            ->join('candidate_profile', 'candidate_profile.candidate_id', '=', 'candidate.id')
            ->join('shortlist_candidates', 'shortlist_candidates.candidate_id', '=', 'candidate.id')
            ->where('type', 1)

            // ->whereIn('job_type', $request->categories)
            ->when($GetShortlisted, function ($query) use ($GetShortlisted) {
                return $query->whereIn('shortlisted_type', $GetShortlisted);
            })
            ->get();

            
            foreach($GetDataByDatePosted as $Posted){
                foreach($Posted as $Data){
                    $GetCandidateId = $Data->id;
                }
            }
    
            $CheckShortlisted = DB::select("SELECT * FROM candidate, candidate_profile, shortlist_candidates WHERE candidate.id = candidate_profile.candidate_id AND shortlist_candidates.candidate_id = '$GetCandidateId' AND candidate.type = 1 ORDER BY candidate.id ASC");
            
    
            $GetProfessionalDetails = DB::table('candidate')
                                ->select('resume.name', 'job_title.name AS JobTitle')
                                ->join('resume', 'candidate.id', '=', 'resume.candidate_id')
                                ->join('job_title', 'job_title.id', '=', 'resume.department')
                                ->where('resume.worked_till', '=', 1)
                                ->first();
    
            $GetCV = DB::table('candidate')
                    ->join('resume', 'candidate.id', '=', 'resume.candidate_id')
                    ->where('resume.type', '=', 4)
                    ->first();
    
    
    
            $GetSearchResults = "";
            $GetCanddiateProfessionalDetails = "";
            $GetCandidateCV = "";
            $GetShortlisted ="";
    
            if($GetDataByDatePosted){
                foreach($GetDataByDatePosted as $GetData){
                    foreach($GetData as $Results){
                        if($CheckShortlisted){
                            $GetShortlisted = "<p class='shortlisted-success' id='ShortlistedSuccess'><i class='fa fa-check'></i> Shortlisted</p>";
                        }else{
                            $GetShortlisted = "<a href='javascript:void(0);' id='AddShortlisted' onclick='AddShortlisted(".$Results->candidate_id.")'>
                            <p class='width100 shortlist-jobs shortlist-pad-mar'>
                                <span class='shortlist-icon'>
                                    <i class='fa fa-heart-o'> </i>
                                </span>
                            <span class='shortlist-bold pad-left5'> Shortlist</span></p>
                        </a>";
                        }
    
                        if($GetCV){
                            $GetCandidateCV = "<a href='/candidate_cv/".$GetCV->cv."' title='' class='for-employers-btn post-a-job-home text-center download-cv-mar' download>DOWNLOAD CV</a>";
                        }else{
                            $GetCandidateCV = "<a href='javascript:void(0);' title='' class='for-employers-btn post-a-job-home text-center download-cv-mar'>DOWNLOAD CV</a>";
                        }
    
    
                        if($GetProfessionalDetails){
                            $GetCanddiateProfessionalDetails = "<div class='job-lctn'> 
                            ".$GetProfessionalDetails->JobTitle." at ".$GetProfessionalDetails->name."
                        </div>";
                        }
                        
                        
                        $GetSearchResults = "<div class='job-listings-sec no-border'>
                        <div class='job-listing wtabs pad-top0 candidate-list-mar-top'>
                            <div class='job-title-sec width100'>
                                <div class='col-md-1 pull-left pad-left0'>
                                    <img class='candidate-list-profile-pic' src='/candidate_profile/".$Results->profile_pic."' alt='' />
                                </div>
                                <div class='col-md-7 pull-left'>
                                <a href='/CandidateDetails/".$Results->id."/".$Results->slug."' target='_blank'>
                                    <h3 class='candidate-list-name'>".$Results->name."</h3>
                                </a>
                                    ".$GetCanddiateProfessionalDetails."
                                    <br>
                                    <div class='job-lctn'><i class='fa fa-map-marker'></i> ".$Results->desired_location."</div>
    
                                    <div class='job-lctn'><i class='fa fa-briefcase'></i>  4-6 Years</div>
    
                                    <div class='job-lctn'><i class='fa fa-briefcase'></i> 55K-60K / Month</div>
    
                                <div>
                            
                                </div>
                            </div>
                            <div class='col-md-4 pull-left'>
                                <img id='ApplyBtnLoader' class='candidate-shortlisted-loader' src='{{URL::asset('UI/otp_verify_loader.gif')}}' alt='' style='display:none;'>
    
                                <p class='shortlisted-success' id='ShortlistedSuccess' style='display:none;'><i class='fa fa-check'></i> Shortlisted</p>
    
                                ".$GetShortlisted."
                                ".$GetCandidateCV."
                            </div>
                            
                        </div>
                        <hr class='candidate-list-border-line'>
                    </div>";
        
                        if($GetSearchResults){
                            echo $GetSearchResults;
                        }
                    }
                }
            }
    }


    public function FilterAppliedCandidates(Request $request){
        // $GetDatePosted = $request->date_posted;
        $GetEmployerId = Session::get("EmployerId");

        $GetShortlisted = $request->CheckAppliedShortlisted;

        $GetJobId = $request->JobId;

        $GetCandidateId = "";

        $GetAllCandidateId = "";

        if($GetShortlisted == 1){
            $GetAllCandidates = DB::select("SELECT candidate_profile.profile_pic, candidate.id, candidate.name, candidate_profile.state, candidate_profile.city, candidate.slug FROM candidate, candidate_profile, jobs_applied WHERE candidate.id = candidate_profile.candidate_id AND jobs_applied.jobs_id = $GetJobId AND jobs_applied.candidate_id = candidate.id AND candidate.type = 1");

            foreach($GetAllCandidates as $Candidates){
                $GetAllCandidateId = $Candidates->id;
            }
            
            $GetCandidates[] = DB::select("SELECT candidate_profile.profile_pic, candidate.id, candidate.name, candidate_profile.state, candidate_profile.city, candidate.slug, candidate_profile.desired_location FROM candidate, candidate_profile, jobs_applied, shortlist_candidates WHERE shortlist_candidates.candidate_id = '$GetAllCandidateId' AND candidate.id = candidate_profile.candidate_id AND jobs_applied.jobs_id = $GetJobId AND jobs_applied.candidate_id = candidate.id AND candidate.type = 1");

            foreach($GetCandidates as $Candidateslist){
                foreach($Candidateslist as $Candidates){
                    $GetCandidateId = $Candidates->id;
                }
            }

            
            
        }else{

            $GetCandidates[] = DB::select("SELECT candidate_profile.profile_pic, candidate.id, candidate.name, candidate_profile.state, candidate_profile.city, candidate.slug, candidate_profile.desired_location  FROM candidate, candidate_profile, jobs_applied WHERE candidate.id = candidate_profile.candidate_id AND jobs_applied.jobs_id = $GetJobId AND jobs_applied.candidate_id = candidate.id AND candidate.type = 1");

            foreach($GetCandidates as $Candidateslist){
                foreach($Candidateslist as $Candidates){
                    $GetCandidateId = $Candidates->id;
                }
            }
    
            
        }

        // echo json_encode($GetCandidates);
        // exit;

        $GetProfessionalDetails = DB::table('candidate')
                                ->join('resume', 'candidate.id', '=', 'resume.candidate_id')
                                ->where('resume.worked_till', '=', 1)
                                ->first();

        $GetCV = DB::table('resume')
                ->where('resume.candidate_id', '=', $GetCandidateId)
                ->where('resume.type', '=', 4)
                ->first();

        $CheckShortlisted = DB::select("SELECT * FROM candidate, candidate_profile, shortlist_candidates WHERE candidate.id = candidate_profile.candidate_id AND shortlist_candidates.candidate_id = '$GetCandidateId' AND candidate.type = 1 ORDER BY candidate.id ASC");
    
    
            $GetSearchResults = "";
            $GetCanddiateProfessionalDetails = "";
            $GetCandidateCV = "";
            $GetShortlisted ="";
    
            if($GetCandidates){
                foreach($GetCandidates as $GetData){
                    foreach($GetData as $Results){
                        if($CheckShortlisted){
                            $GetShortlisted = "<p class='shortlisted-success text-right' id='ShortlistedSuccess'><i class='fa fa-check'></i> Shortlisted</p>";
                        }else{
                            $GetShortlisted = "<a href='javascript:void(0);' id='AddShortlisted' onclick='AddShortlisted(".$Results->candidate_id.")'>
                            <p class='width100 shortlist-jobs shortlist-pad-mar'>
                                <span class='shortlist-icon'>
                                    <i class='fa fa-heart-o'> </i>
                                </span>
                            <span class='shortlist-bold pad-left5'> Shortlist</span></p>
                        </a>";
                        }
    
                        if($GetCV){
                            $GetCandidateCV = "<a href='/candidate_cv/".$GetCV->cv."' title='' class='for-employers-btn post-a-job-home text-center download-cv-mar pull-right' download>DOWNLOAD CV</a>";
                        }else{
                            $GetCandidateCV = "<a href='javascript:void(0);' title='' class='for-employers-btn post-a-job-home text-center download-cv-mar pull-right'>DOWNLOAD CV</a>";
                        }
    
    
                        if($GetProfessionalDetails){
                            $GetCanddiateProfessionalDetails = "<div class='job-lctn'> 
                            ".$GetProfessionalDetails->department." at ".$GetProfessionalDetails->name."
                        </div>";
                        }
                        
                        
                        $GetSearchResults = "<div class='job-listings-sec no-border applied-candidates-width'>
                        <div class='job-listing wtabs pad-top0 candidate-list-mar-top pad-bottom25'>
                            <div class='width100 applied-candidates-list'>
                                <div class='col-md-1 pull-left'>
                                    <img class='candidate-list-profile-pic' src='/candidate_profile/".$Results->profile_pic."' alt='' />
                                </div>
                                <div class='col-md-7 pull-left'>
                                <a href='/AppliedCandidateDetails/".$Results->id."/".$Results->id."/".$Results->slug."' target='_blank'>
                                    <h3 class='candidate-list-name'>".$Results->name."</h3>
                                </a>
                                    ".$GetCanddiateProfessionalDetails."
                                    <br>
                                    <div class='job-lctn'><i class='fa fa-map-marker'></i> ".$Results->desired_location."</div>

                                    <div class='job-lctn'><i class='fa fa-briefcase'></i>  4-6 Years</div>

                                    <div class='job-lctn'><i class='fa fa-briefcase'></i> 55K-60K / Month</div>

                                <div>
                            
                                </div>
                            </div>
                            <div class='col-md-4 pull-left'>
                                <img id='ApplyBtnLoader' class='candidate-shortlisted-loader' src='{{URL::asset('UI/otp_verify_loader.gif')}}' alt='' style='display:none;'>

                                <p class='shortlisted-success text-right' id='ShortlistedSuccess' style='display:none;'><i class='fa fa-check'></i> Shortlisted</p>
                                
                                ".$GetShortlisted."
                                ".$GetCandidateCV."
                            </div>
                            
                        </div>
                    </div>
                </div>";
        
                        if($GetSearchResults){
                            echo $GetSearchResults;
                        }
                    }
                }
            }
    }


    public function FilterApprovedTrainees(Request $request){
        // $GetDatePosted = $request->date_posted;
        $GetEmployerId = Session::get("EmployerId");

        $GetApprovedTrainee = $request->CheckApprovedTrainee;

        $GetTrainingIdByApproved = $request->TrainingId;

        $GetTrainingId = TrainingModel::where('id', $GetTrainingIdByApproved)->first();

        $GetCandidateId = "";

        if($GetApprovedTrainee == 1){
            $GetCandidates[] = DB::select("SELECT candidate.id, candidate_profile.profile_pic, candidate.name, candidate_profile.state, candidate_profile.desired_location, candidate.slug AS CandidateSlug, training.slug AS TrainingSlug, jobs_applied.training_status FROM candidate, candidate_profile, jobs_applied, training WHERE candidate.id = candidate_profile.candidate_id AND jobs_applied.training_id = $GetTrainingIdByApproved AND training.id = $GetTrainingIdByApproved AND jobs_applied.candidate_id = candidate.id AND candidate.type = 2 AND jobs_applied.training_status = 1");
            
            foreach($GetCandidates as $Candidates){
                foreach($Candidates as $CanddiateData){
                    $GetCandidateId = $CanddiateData->id;
                }
            }
        }elseif($GetApprovedTrainee == 2){
            $GetCandidates[] = DB::select("SELECT candidate.id, candidate_profile.profile_pic, candidate.name, candidate_profile.state, candidate_profile.desired_location, candidate.slug AS CandidateSlug, training.slug AS TrainingSlug, jobs_applied.training_status FROM candidate, candidate_profile, jobs_applied, training WHERE candidate.id = candidate_profile.candidate_id AND jobs_applied.training_id = $GetTrainingIdByApproved AND training.id = $GetTrainingIdByApproved AND jobs_applied.candidate_id = candidate.id AND candidate.type = 2");

            foreach($GetCandidates as $Candidates){
                foreach($Candidates as $CanddiateData){
                    $GetCandidateId = $CanddiateData->id;
                }   
            }
        }
        
        // echo json_encode($GetCandidates);
        // exit;
        
        
        $GetEducationDetails = CandidateResume::where('candidate_id', $GetCandidateId)
                                        ->where('type', 1)
                                        ->orderBy('created_at', 'DESC')->first();
        
        $GetCV = DB::table('resume')
                ->where('resume.candidate_id', '=', $GetCandidateId)
                ->where('resume.type', '=', 4)
                ->first();
        
        // echo json_encode($GetEducationDetails);
        // exit;
    
        $GetSearchResults = "";
        $GetCandidateCV = "";
        $GetEducation ="";
        $GetTrainingStatus ="";

        if($GetCandidates){
            foreach($GetCandidates as $GetData){
                foreach($GetData as $Results){

                    if($GetEducationDetails['title'] = 1){
                        $GetEducation = "10th";
                    }elseif($GetEducation['title'] = 2){
                        $GetEducation = "12th";
                    }else{
                        $GetEducation = $GetEducationDetails['title'];
                    }

                    // echo json_encode($GetEducation);
                    // exit;
                    
                    // if($GetCV){
                    //     $GetCandidateCV = "<a href='/candidate_cv/".$GetCV->cv."' title='' class='for-employers-btn post-a-job-home text-center download-cv-mar pull-right' download>DOWNLOAD CV</a>";
                    // }else{
                    //     $GetCandidateCV = "<a href='javascript:void(0);' title='' class='for-employers-btn post-a-job-home text-center download-cv-mar pull-right'>DOWNLOAD CV</a>";
                    // }


                    if($Results->training_status == null){
                        $GetTrainingStatus = "<a href='javascript:void(0);' title='' class='for-employers-btn post-a-job-home text-center download-cv-mar pull-left Approve-trainee-bg' onclick='ChangeTraineeApplliedTrainingStatus(".$Results->id.", 1)'>Approve</a>

                        <a href='javascript:void(0);' title='' class='for-employers-btn post-a-job-home text-center download-cv-mar pull-right reject-trainee-bg' onclick='ChangeTraineeApplliedTrainingStatus(".$Results->id.", 2)'>Reject</a>";

                    }elseif($Results->training_status == 1){
                        $GetTrainingStatus = "<p class='applied-candiates-shortlisted-success approved-staus-font' id='ShortlistedSuccess'><i class='fa fa-check'></i> Approved</p>

                        <a href='javascript:void(0);' title='' class='for-employers-btn post-a-job-home text-center download-cv-mar pull-right reject-trainee-bg' onclick='ChangeTraineeApplliedTrainingStatus(".$Results->id.", 2)'>Reject</a>";
                    }else{
                        $GetTrainingStatus = "<a href='javascript:void(0);' title='' class='for-employers-btn post-a-job-home text-center download-cv-mar pull-left Approve-trainee-bg' onclick='ChangeTraineeApplliedTrainingStatus(".$Results->id.", 1)'>Approve</a>

                        <p class='applied-candiates-shortlisted-success rejected-status-font' id='ShortlistedSuccess'><i class='fa fa-check'></i> Rejected</p>";
                    }
                    
                    
                    $GetSearchResults = "<div class='job-listing wtabs pad-top0 candidate-list-mar-top pad-bottom25'>
                    <div class='width100 applied-candidates-list'>
                        <div class='col-md-1 pull-left'>
                            <img class='candidate-list-profile-pic' src='/trainee_profile/".$Results->profile_pic."' alt='' />
                        </div>
                        <div class='col-md-5 pull-left'>
                        <a href='/AppliedTraineeDetails/".$GetTrainingId->id."/".$Results->id."/".$Results->TrainingSlug."' target='_blank'>
                        <h3 class='candidate-list-name'>".$Results->name."</h3>
                        </a>
                    <p class='candidate-name-p'>
                       ".$GetEducation."
                    </p>
                            <br>
                            <div class='job-lctn'><i class='fa fa-map-marker'></i> ".$Results->desired_location."</div>

                        <div>
                    
                        </div>
                    </div>
                    <div class='col-md-6 pull-left approve-column-pad-left'>
                        <img id='ApplyBtnLoader' class='candidate-shortlisted-loader' src='UI/otp_verify_loader.gif' alt='' style='display:none;'>

                        <p class='shortlisted-success ' id='ShortlistedSuccess' style='display:none;'><i class='fa fa-check'></i> Shortlisted</p>

                        ".$GetTrainingStatus."
                        
                        <p id='DownloadCVPath' style='display:none;'>/trainee_cv/".$GetCV->cv."</p>
                        <a onclick='DownloadTraineeCV(".$Results->id.", ".$GetTrainingId->id.")' title='' class='for-employers-btn post-a-job-home text-center download-cv-mar pull-right' download>DOWNLOAD CV</a>
                    </div>
                    
                </div>";
                    
                    
                    
                    if($GetSearchResults){
                        echo $GetSearchResults;
                    }
                }
            }
        }
    }



    public function ChangeJobStatus(Request $request){
        $GetJobsId = $request->JobId;

        $GetCheckJobsStatus = $request->CheckJobsStatus;

        if($GetCheckJobsStatus == 1){
            $Jobs = JobsModel::find($GetJobsId);
            
            $Jobs->status = 1;
            $ChangeStatus = $Jobs->save();
            if($ChangeStatus){
                return response()->json(array(
                    "error"=>TRUE,
                    "message"=>"Job closed successfully"
                ));
            }else{
                return response()->json(array(
                    "error"=>TRUE,
                    "message"=>"failed"
                ));
            }
        }else{
            $Jobs = JobsModel::find($GetJobsId);
            
            $Jobs->status = 0;
            $ChangeStatus = $Jobs->save();
            if($ChangeStatus){
                return response()->json(array(
                    "error"=>FALSE,
                    "message"=>"Job live successfully"
                ));
            }else{
                return response()->json(array(
                    "error"=>TRUE,
                    "message"=>"failed"
                ));
            }
        }
    }


    public function ChangeCAJobStatus(Request $request){
        $GetJobsId = $request->JobId;

        $GetCheckJobsStatus = $request->CheckJobsStatus;

        if($GetCheckJobsStatus == 1){
            $Jobs = JobsModel::find($GetJobsId);
            
            $Jobs->status = 1;
            $ChangeStatus = $Jobs->save();
            if($ChangeStatus){
                return response()->json(array(
                    "error"=>TRUE,
                    "message"=>"CA Articleship closed successfully"
                ));
            }else{
                return response()->json(array(
                    "error"=>TRUE,
                    "message"=>"failed"
                ));
            }
        }else{
            $Jobs = JobsModel::find($GetJobsId);
            
            $Jobs->status = 0;
            $ChangeStatus = $Jobs->save();
            if($ChangeStatus){
                return response()->json(array(
                    "error"=>FALSE,
                    "message"=>"CA Articleship live successfully"
                ));
            }else{
                return response()->json(array(
                    "error"=>TRUE,
                    "message"=>"failed"
                ));
            }
        }
    }


    

// Applied Trainees

public function SearchAppliedTrainees(Request $request){
    $Title = $request->Trainingtitle;
    // echo $Title;
    // exit;
    $GetTrainees = DB::select("SELECT candidate.name, candidate_profile.profile_pic,jobs_applied.created_at, training.title  FROM jobs_applied, candidate, candidate_profile, training WHERE jobs_applied.candidate_id = candidate.id AND candidate_profile.candidate_id = candidate.id AND training.id = jobs_applied.training_id AND training.title LIKE '%{$Title}%'");

    

    $GetSearchResults = "";

    if($GetTrainees){
        foreach($GetTrainees as $Results){
            
            $GetSearchResults = "<div class='col-md-1 pull-left'>
            <img src='/trainee_profile/".$Results->profile_pic."' alt=''>
            </div>
            
            <div class='col-md-8 pull-left'>
            <h4 class='recent-candidate-name'>".$Results->name."</h4>
                <p class='pad-left10 col-md-6 max-width100'>Training : ".$Results->title."</p>
            </div>

            <div class='col-md-3 pull-left'>
            <p class='col-md-6 text-right recent-candidates-date view-all-training-date'>".date('d M Y', strtotime($Results->created_at))."</p>
            </div>

            <hr class='view-all-border-bottom'>";

            if($GetSearchResults){
                echo $GetSearchResults;
            }
        }
    }
}

public function SearchAppliedCandidates(Request $request){
    $Title = $request->Jobtitle;
    // echo $Title;
    // exit;
    $GetAppliedCandidates = DB::select("SELECT candidate.name, candidate_profile.profile_pic,jobs_applied.created_at, jobs.title  FROM jobs_applied, candidate, candidate_profile, jobs WHERE jobs_applied.candidate_id = candidate.id AND candidate_profile.candidate_id = candidate.id AND jobs.id = jobs_applied.jobs_id AND  jobs.title LIKE '%{$Title}%'");

    // echo json_encode($GetAppliedCandidates);
    // exit;

    $GetSearchResults = "";

    if($GetAppliedCandidates){
        foreach($GetAppliedCandidates as $Results){
            
            $GetSearchResults = "<div class='col-md-1 pull-left'>
            <img src='/candidate_profile/".$Results->profile_pic."' alt=''>
            </div>
            
            <div class='col-md-8 pull-left'>
            <h4 class='recent-candidate-name'>".$Results->name."</h4>
                <p class='pad-left10 col-md-6 max-width100'>Job title : ".$Results->title."</p>
            </div>

            <div class='col-md-3 pull-left'>
            <p class='col-md-6 text-right recent-candidates-date view-all-training-date'>".date('d M Y', strtotime($Results->created_at))."</p>
            </div>

            <hr class='view-all-border-bottom'>";

            if($GetSearchResults){
                echo $GetSearchResults;
            }
        }
    }
}




// SearchLocationByKeypress

public function SearchLocationByKeypress(Request $request){
    $GetLocationInput = $request->Location;

    $GetLocationByKeyPress = DB::select("SELECT DISTINCT city, id FROM jobs WHERE jobs.status = 0 AND city LIKE '%$GetLocationInput%'");

    $GetLocation = "";
    $GetResult = "";

    foreach($GetLocationByKeyPress as $Location){
        // $GetLocation = $Location->city;
        $GetResult = "<p><input type='checkbox' class='CheckManageJobFilters'  value=".$Location->city." name='Location' id=".$Location->id."><label for=".$Location->id." class='CheckBoxFilter'>".$Location->city."</label></p>";

    }
    if($GetResult){
        echo $GetResult;
    }else{
        echo json_encode("No Results Found...");
    }
    

}



public function SearchClosJobsLocationByKeypress(Request $request){
    $GetLocationInput = $request->Location;

    $GetLocationByKeyPress = DB::select("SELECT DISTINCT city, id FROM jobs WHERE jobs.status = 1 AND city LIKE '%$GetLocationInput%'");

    $GetLocation = "";
    $GetResult = "";

    foreach($GetLocationByKeyPress as $Location){
        // $GetLocation = $Location->city;
        $GetResult = "<p><input type='checkbox' class='CheckCloseJobFilters'  value=".$Location->city." name='Location' id=".$Location->id."><label for=".$Location->id." class='CheckBoxFilter'>".$Location->city."</label></p>";
    
        if($GetResult){
            echo $GetResult;
        }else{
            echo json_encode("No Results Found...");
        }

    }
    

}



public function SearchTrainingLocationByKeypress(Request $request){
    $GetLocationInput = $request->Location;

    $GetLocationByKeyPress = DB::select("SELECT DISTINCT city, id FROM employer WHERE city LIKE '%$GetLocationInput%'");

    $GetLocation = "";
    $GetResult = "";

    foreach($GetLocationByKeyPress as $Location){
        // $GetLocation = $Location->city;
        $GetResult = "<p><input type='checkbox' class='CheckManageTrainingFilters'  value=".$Location->city." name='Location' id=".$Location->id."><label for=".$Location->id." class='CheckBoxFilter'>".$Location->city."</label></p>";
    
        if($GetResult){
            echo $GetResult;
        }else{
            echo json_encode("No Results Found...");
        }

    }
}


public function SearchCloseTrainingLocationByKeypress(Request $request){
    $GetLocationInput = $request->Location;

    $GetLocationByKeyPress = DB::select("SELECT DISTINCT city, id FROM employer WHERE city LIKE '%$GetLocationInput%'");

    $GetLocation = "";
    $GetResult = "";

    foreach($GetLocationByKeyPress as $Location){
        // $GetLocation = $Location->city;
        $GetResult = "<p><input type='checkbox' class='CheckCloseTrainingFilters'  value=".$Location->city." name='Location' id=".$Location->id."><label for=".$Location->id." class='CheckBoxFilter'>".$Location->city."</label></p>";
    
        if($GetResult){
            echo $GetResult;
        }else{
            echo json_encode("No Results Found...");
        }

    }
}



public function SearchDurationByManageTraining(Request $request){
    $GetDurationInput = $request->Duration;

    $GetDurationByKeyPress = DB::select("SELECT DISTINCT training.duration, training.id FROM training, employer WHERE training.employer_id = employer.id AND duration LIKE '%$GetDurationInput%'");

    $GetDuration = "";
    $GetResult = "";
    
    
    foreach($GetDurationByKeyPress as $Duration){
        $GetDuration = str_replace(' ', '', $Duration->duration);

        $GetResult = "<p>
        <input type='checkbox' class='CheckManageTrainingFilters' value=".$GetDuration." name='Duration' id=".$Duration->duration."><label for=".$Duration->duration." class='CheckBoxFilter'>".$Duration->duration."</label>
        </p>";

        if($GetResult){
            echo $GetResult;
        }else{
            echo json_encode("No Results Found...");
        }
    }

}



public function SearchDurationByCloseTraining(Request $request){
    $GetDurationInput = $request->Duration;

    $GetDurationByKeyPress = DB::select("SELECT DISTINCT duration, id FROM training WHERE training.training_status = 1 AND duration LIKE '%$GetDurationInput%'");

    $GetDuration = "";
    $GetResult = "";
    
    
    foreach($GetDurationByKeyPress as $Duration){
        $GetDuration = str_replace(' ', '', $Duration->duration);

        $GetResult = "<p>
        <input type='checkbox' class='CheckCloseTrainingFilters' value=".$GetDuration." name='Duration' id=".$Duration->duration."><label for=".$Duration->duration." class='CheckBoxFilter'>".$Duration->duration."</label>
        </p>";

        if($GetResult){
            echo $GetResult;
        }else{
            echo json_encode("No Results Found...");
        }
    }

}




public function SearchFresherDurationByManageTraining(Request $request){
    $GetDurationInput = $request->Duration;

    $GetDurationByKeyPress = DB::select("SELECT DISTINCT duration, id FROM training WHERE duration LIKE '%$GetDurationInput%'");

    $GetDuration = "";
    $GetResult = "";
    
    
    foreach($GetDurationByKeyPress as $Duration){
        $GetDuration = str_replace(' ', '', $Duration->duration);

        $GetResult = "<p><input type='checkbox' class='CheckFilterTrainings' value=".$GetDuration." name='Duration' id=".$GetDuration."><label for=".$GetDuration." class='CheckBoxFilter'>".$Duration->duration."</label></p>";

        if($GetResult){
            echo $GetResult;
        }else{
            echo json_encode("No Results Found...");
        }
    }

}


public function SearchCandidateLocationByKeypress(Request $request){
    $GetLocationInput = $request->Location;

    $GetLocationByKeyPress = DB::select("SELECT DISTINCT city, id FROM candidate_profile WHERE city LIKE '%$GetLocationInput%'");

    $GetLocation = "";
    $GetResult = "";

    foreach($GetLocationByKeyPress as $Location){
        $GetLocation = str_replace(' ', '-', $Location->city);

        // echo json_encode($GetLocation);
        // exit;

        $GetResult = "<p><input type='checkbox' class='CheckManageCandidatesFilters' class='' value=".$GetLocation." name='Location' id=".$Location->id."><label for=".$Location->id." class='CheckBoxFilter'>".$Location->city."</label></p>";
    }

    if($GetResult){
        echo $GetResult;
    }else{
        echo json_encode("No Results Found...");
    }
    
}


public function SearchJobsLocationByKeypress(Request $request){
    $GetLocationInput = $request->Location;

    $GetLocationByKeyPress = DB::select("SELECT DISTINCT city, id FROM jobs WHERE employee_type = 2 AND city LIKE '%$GetLocationInput%'");

    $GetLocation = "";
    $GetResult = "";

    foreach($GetLocationByKeyPress as $Location){
        // $GetLocation = str_replace(' ', '-', $Location->city);

        // echo json_encode($GetLocation);
        // exit;

        $GetResult = "<p><input type='checkbox' class='CheckJobFilters' class='' value=".$Location->city." name='Location' id=".$Location->id."><label for=".$Location->id." class='CheckBoxFilter'>".$Location->city."</label></p>";
    
        if($GetResult){
            echo $GetResult;
        }else{
            echo json_encode("No Results Found...");
        }

    }
    
}

}





