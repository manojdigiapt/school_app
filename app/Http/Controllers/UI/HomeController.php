<?php

namespace App\Http\Controllers\UI;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use GuzzleHttp\Client;
use App\Model\Admin\JobTitleModel;

use App\Model\UI\NewsAndEvents;
use Artisan;

use DB;

class HomeController extends Controller
{
    public function home(){
        Artisan::call('cache:clear');
        Artisan::call('view:clear');

        if(Auth::guard('candidate')->check()){
            return redirect('/Candidate/Dashboard');
        }elseif(Auth::guard('employer')->check()){
            return redirect('/Employer/Dashboard');
        }elseif(Auth::guard('trainee')->check()){
            return redirect('/Trainee/Dashboard');
        }elseif(Auth::guard('ca_article')->check()){
            return redirect('/Article/Dashboard');
        }
        else{
            $title = "Home";

            $client = new Client();
            $GetNews = json_decode(file_get_contents('https://newsapi.org/v2/everything?q=gst&apiKey=514385d001164438a7688424ed2f5e6a'), true);

            $GetGstNews = $GetNews['articles'];

            

            foreach($GetGstNews as $News){
                $NewsAndEvents = new NewsAndEvents();
                $Title = $News['title'];
                $Url = $News['url'];

                $CheckNewsAndEvents = NewsAndEvents::where('title', $Title)
                                        ->where('url', $Url)
                                        ->get();
                // echo json_encode($News['title']);
                // exit;
                if($CheckNewsAndEvents->isEmpty()){
                    $NewsAndEvents->title = $Title;
                    $NewsAndEvents->description = $News['description'];
                    $NewsAndEvents->image = $News['urlToImage'];
                    $NewsAndEvents->published = $News['publishedAt'];
                    $NewsAndEvents->url = $Url;

                    $NewsAndEvents->save();
                }
                // echo json_encode($News);
            }
            
            $GetCityByJobPosted =  DB::select("SELECT DISTINCT city FROM jobs");

            $GetNews = NewsAndEvents::take(3)->get();
            // echo json_encode($GetNews);
            // exit;
            return view('UI.layouts.home', compact('title', 'GetNews', 'GetCityByJobPosted'));
        }
    }

    public function AboutUs(){
        $title = "About us";
        return view('UI.layouts.about_us', compact('title'));
    }

    public function PrivacyPolicy(){
        $title = "Privacy policy";
        return view('UI.layouts.privacy_policy', compact('title'));
    }

    public function TermsUse(){
        $title = "Terms of user";
        return view('UI.layouts.terms_and_use', compact('title'));
    }

    public function Contactus(){
        $title = "Contact us";
        return view('UI.layouts.contact_us', compact('title'));
    }

    public function Demo(){
        $title = "UI Demo";
        return view('UI.Demo.home', compact('title'));
    }

    public function CandidateDashboard(){
        $title = "Candidate Dashboard";
        return view('UI.Demo.candidate_dashboard', compact('title'));
    }

    
    public function SearchJobs(Request $request){
        $title = "Search Results";

        $JobTitle = $request->JobTitle;
        $JobCity = $request->City;

        $GetJobsBySlug = DB::select("SELECT job_title.name AS title, jobs.experience ,jobs.job_type, jobs.city, jobs.total_positions, jobs.salary_min, jobs.salary_max, jobs.location, jobs.status, jobs.created_at, jobs.industry, jobs.slug, jobs.id, employer.slug As CompanySlug, employer.company_name, jobs.job_description, jobs.employee_type, jobs.employee_type FROM jobs, employer, job_title WHERE jobs.employer_id = employer.id AND job_title.id = jobs.title AND job_title.name LIKE '%{$JobTitle}%' AND jobs.city = '{$JobCity}'");

        $GetJobTitle = JobTitleModel::get();
        // echo json_encode($GetJobsBySlug);
        // exit;

        $GetCityByJobPosted =  DB::select("SELECT DISTINCT city FROM jobs");

        return view('UI.layouts.search_jobs', compact('title', 'GetJobsBySlug', 'GetJobTitle', 'GetCityByJobPosted'));        
    }

}
