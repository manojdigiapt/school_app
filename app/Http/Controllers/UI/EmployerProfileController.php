<?php

namespace App\Http\Controllers\UI;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\UI\EmployerModel;
use App\Model\UI\ShortlistCandidatesModel;
use Illuminate\Support\Facades\Hash;
use App\Model\UI\CandidateProfileModel;
use App\Model\UI\CandidateModel;
use App\Model\UI\JobsModel;
use App\Model\UI\FavouriteModel;
use App\Model\UI\JobsApplied;
use App\Model\UI\TotalViews;
use App\Model\UI\TrainingModel;
use App\Model\UI\SelectedTraineeModel;
use App\Model\UI\ReviewsModel;
use App\Model\UI\CandidateResume;

use Session;
use DB;


// Admin Model
use App\Model\Admin\TrainingCompletionModel;
use App\Model\Admin\JobTypeModel;
use App\Model\Admin\IndustriesModel;
use App\Model\Admin\QualificationModel;
use App\Model\Admin\FunctionalAreasModel;
use App\Model\Admin\ExperienceLevelModel;

use App\Model\Admin\JobTitleModel;
use App\Model\Admin\TrainingTitleModel;

class EmployerProfileController extends Controller
{   
    public function __construct(){
        // $this->middleware('guest:employer')->except('InsertEmployerProfile', 'ShortlistCandidates');
    }


    public function InsertEmployerProfile(Request $request){
        $GetEmployerId = Session::get('EmployerId');
        $EmployerProfile = new EmployerModel();

        $UpdateEmployerProfile = EmployerModel::where('id', $GetEmployerId)->first();

        $UpdateEmployerProfile->company_name = $request->company_name;
        $UpdateEmployerProfile->slug = strtolower($request->company_name);
        $UpdateEmployerProfile->email = $request->email;
        $UpdateEmployerProfile->mobile = $request->mobile;
        $UpdateEmployerProfile->employer_type = $request->Emp_type;
        $UpdateEmployerProfile->pan_number = $request->pan_no;
        $UpdateEmployerProfile->gst_number = $request->gst_number;
        $UpdateEmployerProfile->team_size = $request->team_size;
        $UpdateEmployerProfile->address = $request->Emp_address;
        $UpdateEmployerProfile->zip_code = $request->Emp_zipcode;
        $UpdateEmployerProfile->country = $request->Emp_country;
        $UpdateEmployerProfile->state = $request->Emp_state;
        $UpdateEmployerProfile->city = $request->Emp_city;
        $UpdateEmployerProfile->facebook = $request->facebook;
        $UpdateEmployerProfile->twitter = $request->twitter;
        $UpdateEmployerProfile->linkedin = $request->linkedin;

        $UpdatedEmployerProfile = $UpdateEmployerProfile->save();
        if($UpdatedEmployerProfile){
            return response()->json(array(
                "error"=>FALSE,
                "message"=>"Profile updated successfully"
            ));
        }else{
            return response()->json(array(
                "error"=>TRUE,
                "message"=>"Failed."
            ));
        }
    }

    public function post_jobs(){
        $title = "Post Jobs";

        $GetEmployerId = Session::get('EmployerId');
        $EmployerProfile = EmployerModel::where("id", $GetEmployerId)->first();

        return view("UI.layouts.employer_post_jobs", compact('title', 'EmployerProfile'));
    }

    public function post_training(){
        $title = "Post Training";
        
        $GetEmployerId = Session::get('EmployerId');
        $EmployerProfile = EmployerModel::where("id", $GetEmployerId)->first();

        return view("UI.layouts.employer_post_training", compact('title', 'EmployerProfile'));
    }

    public function ShortlistCandidatesByJobList(Request $request){
        $GetEmployerId = Session::get("EmployerId");
        $GetCandidateId = $request->CandidateId;

        $CheckShortlist = ShortlistCandidatesModel::where('candidate_id', $GetCandidateId)
                                        ->where('employer_id', $GetEmployerId)
                                        ->first();
        // echo json_encode($GetCandidateId);
        // exit;
        if($CheckShortlist != null){
            return response()->json(array(
                "error" => TRUE,
                "message" => "Candidate is already shortlisted."
            ));
        }else{
            $ShortlistCandidates = new ShortlistCandidatesModel();
            $ShortlistCandidates->candidate_id = $GetCandidateId;
            $ShortlistCandidates->employer_id = $GetEmployerId;

            $ShortlistCandidates->shortlisted_type = "1";
    
            $ShortlistedCandidates = $ShortlistCandidates->save();
            if($ShortlistedCandidates){
                return response()->json(array(
                    "error" => FALSE,
                    "message" => "Candidates shortlisted successfully"
                ));
            }else{
                return response()->json(array(
                    "error" => TRUE,
                    "message" => "Failed"
                ));
            }
        }

        
    }

    public function ShortlistCandidatesByDirect(Request $request){
        $GetEmployerId = Session::get("EmployerId");
        $GetCandidateId = $request->CandidateId;

        $CheckShortlist = ShortlistCandidatesModel::where('candidate_id', $GetCandidateId)
                                        ->where('employer_id', $GetEmployerId)
                                        ->first();
        // echo json_encode($GetCandidateId);
        // exit;
        if($CheckShortlist != null){
            return response()->json(array(
                "error" => TRUE,
                "message" => "Candidate is already shortlisted."
            ));
        }else{
            $ShortlistCandidates = new ShortlistCandidatesModel();
            $ShortlistCandidates->candidate_id = $GetCandidateId;
            $ShortlistCandidates->employer_id = $GetEmployerId;

            $ShortlistCandidates->shortlisted_type = "2";
    
            $ShortlistedCandidates = $ShortlistCandidates->save();
            if($ShortlistedCandidates){
                return response()->json(array(
                    "error" => FALSE,
                    "message" => "Candidates shortlisted successfully"
                ));
            }else{
                return response()->json(array(
                    "error" => TRUE,
                    "message" => "Failed"
                ));
            }
        }

        
    }

    // public function GenerateCertificate(){
    //     $title = "Certificates";
    //     $GetTraineeId = Session::get('TraineeId');

    //     $GetStudentDataByCertificates = DB::select("SELECT candidate.name, employer.company_name, training.title FROM selected_trainee, candidate, employer, training WHERE selected_trainee.candidate_id = candidate.id AND selected_trainee.candidate_id = $GetTraineeId AND selected_trainee.training_id = training.id AND candidate.type = 2 AND selected_trainee.training_status = 1");

    //     return view('UI.layouts.certificates', compact('title'));
    // }

    



    // Candidate & Search
    public function GetAllCandidates(){
        $title = "All Candidates";
        $GetEmployerId = Session::get("EmployerId");
        $EmployerProfile = EmployerModel::where("id", $GetEmployerId)->first();

        $GetCandidates = DB::select("SELECT candidate.id AS CandidateId, candidate.slug, candidate.name, candidate.email, candidate.mobile, candidate_profile.* FROM candidate, candidate_profile WHERE candidate.id = candidate_profile.candidate_id AND candidate.type = 1 ORDER BY candidate.id ASC ");

        $GetProfessionalDetails = DB::table('candidate')
                                ->select('resume.name', 'job_title.name AS JobTitle')
                                ->join('resume', 'candidate.id', '=', 'resume.candidate_id')
                                ->join('job_title', 'job_title.id', '=', 'resume.department')
                                ->where('resume.worked_till', '=', 1)
                                ->first();
        
        $GetCV = DB::table('candidate')
                ->join('resume', 'candidate.id', '=', 'resume.candidate_id')
                ->where('resume.type', '=', 4)
                ->first();

        $GetCandidateId = "";
        foreach($GetCandidates as $Candidates){
            $GetCandidateId = $Candidates->id;
        }

        
        
        $CheckShortlisted = DB::select("SELECT candidate.*, candidate_profile.*, shortlist_candidates.candidate_id AS CandidateId FROM candidate, candidate_profile, shortlist_candidates WHERE candidate.id = candidate_profile.candidate_id AND shortlist_candidates.candidate_id = candidate.id AND candidate.type = 1 ORDER BY candidate.id ASC");

        // echo json_encode($CheckShortlisted);
        // exit;

        $GetIndustries = IndustriesModel::get();
        // $GetStipend = TrainingCompletionModel::get();
        $GetJobTitle = JobTitleModel::get();
        $GetQualification = QualificationModel::get();
        $GetTrainingTitle = TrainingTitleModel::get();
        // $GetExperience = ExperienceLevelModel::get();

        // echo json_encode($CheckShortlisted);
        // exit;
        return view("UI.layouts.candidate_list", compact('title', 'GetCandidates', 'CheckShortlisted', 'EmployerProfile', 'GetProfessionalDetails', 'GetCV', 'GetIndustries', 'GetJobTitle', 'GetQualification', 'GetTrainingTitle'));
    }


    public function GetAllShortlistedCandidates(){
        $title = "All Candidates";
        $GetEmployerId = Session::get("EmployerId");
        $EmployerProfile = EmployerModel::where("id", $GetEmployerId)->first();

        $GetCandidates = DB::select("SELECT candidate.id AS CandidateId, candidate.slug, candidate.name, candidate.email, candidate.mobile, candidate_profile.* FROM candidate, candidate_profile WHERE candidate.id = candidate_profile.candidate_id AND candidate.type = 1 ORDER BY candidate.id ASC ");

        $GetProfessionalDetails = DB::table('candidate')
                                ->select('resume.name', 'job_title.name AS JobTitle')
                                ->join('resume', 'candidate.id', '=', 'resume.candidate_id')
                                ->join('job_title', 'job_title.id', '=', 'resume.department')
                                ->where('resume.worked_till', '=', 1)
                                ->first();
        
        $GetCV = DB::table('candidate')
                ->join('resume', 'candidate.id', '=', 'resume.candidate_id')
                ->where('resume.type', '=', 4)
                ->first();

        $GetCandidateId = "";
        foreach($GetCandidates as $Candidates){
            $GetCandidateId = $Candidates->id;
        }

        
        
        $CheckShortlisted = DB::select("SELECT candidate.*, candidate_profile.*, shortlist_candidates.candidate_id AS CandidateId FROM candidate, candidate_profile, shortlist_candidates WHERE candidate.id = candidate_profile.candidate_id AND shortlist_candidates.candidate_id = candidate.id AND candidate.type = 1 ORDER BY candidate.id ASC");

        // echo json_encode($CheckShortlisted);
        // exit;

        $GetIndustries = IndustriesModel::get();
        // $GetStipend = TrainingCompletionModel::get();
        $GetJobTitle = JobTitleModel::get();
        $GetQualification = QualificationModel::get();
        $GetTrainingTitle = TrainingTitleModel::get();
        // $GetExperience = ExperienceLevelModel::get();

        // echo json_encode($CheckShortlisted);
        // exit;
        return view("UI.layouts.shortlisted_candidates", compact('title', 'GetCandidates', 'CheckShortlisted', 'EmployerProfile', 'GetProfessionalDetails', 'GetCV', 'GetIndustries', 'GetJobTitle', 'GetQualification', 'GetTrainingTitle'));
    }

    public function SearchCandidatesPositions(Request $request){
        $title = $request->term;

        $GetCandidates = DB::select("SELECT candidate_profile.role FROM candidate, candidate_profile WHERE candidate.id = candidate_profile.candidate_id AND candidate_profile.role LIKE '%$title%'");
        echo json_encode($GetCandidates);

    }

    public function SearchCandidates(Request $request){
        $Role = $request->role;
        $State = $request->state;
        $City = $request->city;

        $GetCandidatesBySearch = DB::select("SELECT * FROM candidate, candidate_profile WHERE candidate.id = candidate_profile.candidate_id AND candidate_profile.state = '$State' AND candidate_profile.city = '$City' AND candidate_profile.role LIKE '%$Role%'");

        if($GetCandidatesBySearch){
            echo json_encode($GetCandidatesBySearch);
        }else{
            echo json_encode("No Results Found...");
        }
        
    }


    public function CandidateDetails($Id, $slug){
        $title = "Candidates Details";
        $GetEmployerId = Session::get("EmployerId");
        // $GetCandidateId = CandidateModel::where('slug', $slug)->first();

        // $GetCandidateDetails = DB::select("SELECT * FROM candidate, candidate_profile WHERE candidate.id = candidate_profile.candidate_id AND candidate.id = $Id");

        $GetCandidateDetails  =  DB::table('candidate')
                                ->join('candidate_profile', 'candidate.id', '=', 'candidate_profile.candidate_id')
                                ->where('candidate.id', $Id)
                                ->first();

        $GetProfessionalDetails = DB::table('candidate')
                                ->join('resume', 'candidate.id', '=', 'resume.candidate_id')
                                ->where('resume.worked_till', '=', 1)
                                ->where('resume.candidate_id', $Id)
                                ->first();

        // $GetEducation = DB::table('resume')
        //                 ->where('candidate_id', $Id)
        //                 ->where('type', 1)
        //                 ->get();
        $GetEducation = DB::table('resume')
                            ->select('resume.*', 'qualifications.qualification')
                            ->join('qualifications', 'qualifications.id', '=', 'resume.title')
                            ->where('type', 1)
                            ->where('resume.candidate_id', $Id)
                            ->orderBy('id', 'DESC')
                            ->get();

        $GetQualification = DB::table('resume')
                            ->select('resume.*', 'qualifications.qualification')
                            ->join('qualifications', 'qualifications.id', '=', 'resume.title')
                            ->where('type', 1)
                            ->where('resume.candidate_id', $Id)
                            ->orderBy('id', 'DESC')
                            ->first();

        $GetExperience = DB::table('resume')
                        ->select('resume.*', 'job_title.name AS JobTitle')
                        ->join('job_title', 'job_title.id', '=', 'resume.department')
                        ->where('candidate_id', $Id)
                        ->where('type', 2)
                        ->get();

        $GetCV = DB::table('resume')
                    ->where('resume.candidate_id', '=', $Id)
                    ->where('resume.type', '=', 4)
                    ->first();

        $CheckShortlist = ShortlistCandidatesModel::where('candidate_id', $Id)
                                        ->where('employer_id', $GetEmployerId)
                                        ->first();
        // $GetSkillNames = "";

        $EmployerProfile = EmployerModel::where("id", $GetEmployerId)->first();

        $GetSkillNames = "";

        // foreach($CandidateProfile as $key => $value){
        //     $GetSkillNames = explode(',', $value['skills']);
        // }
        $GetSkillNames = explode(',', $GetCandidateDetails->skills);
        
        $CheckShortlisted = DB::select("SELECT * FROM candidate, candidate_profile, shortlist_candidates WHERE candidate.id = candidate_profile.candidate_id AND shortlist_candidates.candidate_id = '$Id' AND candidate.type = 1 ORDER BY candidate.id ASC");


        return view("UI.layouts.candidate_details", compact('title', 'GetCandidateDetails', 'GetEducationDetails', 'GetExperience', 'GetSkills', 'GetCV', 'CheckShortlist', 'EmployerProfile', 'GetSkillNames', 'GetProfessionalDetails', 'EmployerProfile', 'GetEducation', 'CheckShortlisted', 'GetQualification'));
    }


    public function AppliedCandidateDetails($Id, $GetCandidateId, $slug){
        $title = "Candidates Details";
        $GetEmployerId = Session::get("EmployerId");
        // $GetCandidateId = CandidateModel::where('slug', $slug)->first();

        $UpdateEmployeId = DB::select("SELECT jobs_applied.id FROM jobs_applied, candidate WHERE jobs_applied.jobs_id = $Id AND jobs_applied.candidate_id = candidate.id");

        $GetAppliedId = "";

        foreach($UpdateEmployeId as $UpdatedEmpId){
            $GetAppliedId = $UpdatedEmpId->id;
        }

        // echo json_encode($GetAppliedId);
        // exit;

        $GetUpdateRecruiterStatus = JobsApplied::where('id', $GetAppliedId)->first();

        $GetUpdateRecruiterStatus->employer_id = $GetEmployerId;
        $GetUpdateRecruiterStatus->recruiter_status = 1;
        $GetUpdateRecruiterStatus->application_view_date = date('Y-m-d');
        $GetUpdateRecruiterStatus->save();


        // echo json_encode($GetUpdateRecruiterStatus);
        // exit;
        $GetCandidateDetails  =  DB::table('candidate')
                                ->join('candidate_profile', 'candidate.id', '=', 'candidate_profile.candidate_id')
                                ->where('candidate.id', $GetCandidateId)
                                ->first();

        $GetProfessionalDetails = DB::table('candidate')
                                ->join('resume', 'candidate.id', '=', 'resume.candidate_id')
                                ->where('resume.worked_till', '=', 1)
                                ->where('resume.candidate_id', $GetCandidateId)
                                ->first();

        $GetEducation = DB::table('resume')
                                ->select('resume.*', 'qualifications.qualification')
                                ->join('qualifications', 'qualifications.id', '=', 'resume.title')
                                ->where('type', 1)
                                ->where('resume.candidate_id', $GetCandidateId)
                                ->orderBy('id', 'DESC')
                                ->get();
        
        // echo json_encode($GetEducation);
        // exit;

        $GetExperience = DB::table('resume')
                        ->select('resume.*', 'job_title.name AS JobTitle')
                        ->join('job_title', 'job_title.id', '=', 'resume.department')
                        ->where('candidate_id', $GetCandidateId)
                        ->where('type', 2)
                        ->get();

        $GetCV = DB::table('resume')
                    ->where('resume.candidate_id', '=', $GetCandidateId)
                    ->where('resume.type', '=', 4)
                    ->first();

        

        $CheckShortlist = ShortlistCandidatesModel::where('candidate_id', $GetCandidateId)
                                        ->where('employer_id', $GetEmployerId)
                                        ->first();

        
        // $GetSkillNames = "";

        $EmployerProfile = EmployerModel::where("id", $GetEmployerId)->first();

        $GetSkillNames = "";

        // foreach($CandidateProfile as $key => $value){
        //     $GetSkillNames = explode(',', $value['skills']);
        // }
        $GetSkillNames = explode(',', $GetCandidateDetails->skills);
        
        $CheckShortlisted = DB::select("SELECT * FROM candidate, candidate_profile, shortlist_candidates WHERE candidate.id = candidate_profile.candidate_id AND shortlist_candidates.candidate_id = '$GetCandidateId' AND candidate.type = 1 ORDER BY candidate.id ASC");
        
        $GetQualification = DB::table('resume')
                            ->select('resume.*', 'qualifications.qualification')
                            ->join('qualifications', 'qualifications.id', '=', 'resume.title')
                            ->where('type', 1)
                            ->where('resume.candidate_id', $Id)
                            ->orderBy('id', 'DESC')
                            ->first();

        return view("UI.layouts.candidate_details", compact('title', 'GetCandidateDetails', 'GetEducation', 'GetExperience', 'GetCV', 'CheckShortlisted', 'EmployerProfile', 'GetProfessionalDetails', 'GetQualification', 'GetUpdateRecruiterStatus'));
    }


    public function ManageJobs(){
        $title = "Manage Jobs";
        $GetEmployerId = Session::get("EmployerId");
        // $GetEmployerId = 1;
        $EmployerProfile = EmployerModel::where("id", $GetEmployerId)->first();

        $GetJobsByEmployerId = DB::select("SELECT DISTINCT job_title.name AS title, jobs.experience ,jobs.job_type, jobs.city, jobs.total_positions, jobs.salary_min, jobs.salary_max, jobs.location, jobs.status,
        jobs.created_at, jobs.slug, jobs.id, employer.slug As CompanySlug FROM jobs, employer, job_title WHERE jobs.employer_id = employer.id AND employer.id = $GetEmployerId AND job_title.id = jobs.title AND jobs.status = 0");
        // echo json_encode($GetJobsByEmployerId);
        // exit;
        $GetActiveJobsCount = DB::select("SELECT * FROM jobs WHERE jobs.employer_id = $GetEmployerId AND jobs.status = 1");  
        
        $GetJobType = JobTypeModel::get();
        $GetIndustries = IndustriesModel::get();
        $GetJobTitle = JobTitleModel::get();
        $GetQualification = QualificationModel::get();
        $GetTrainingTitle = TrainingTitleModel::get();


        // $GetJobsId = "";
        // foreach($GetJobsByEmployerId as $JobsId){
        //     $GetJobsId = $JobsId->id;
        // }

        $GetTotalViewsCount = DB::table('total_views')
                            ->join('candidate', 'candidate.id', '=', 'total_views.candidate_id')
                            ->join('jobs', 'jobs.id', '=', 'total_views.jobs_id')
                            ->where('jobs.employer_id', $GetEmployerId)
                            ->count();

        $CheckApplicantsCount = DB::table('jobs_applied')
                                ->join('candidate', 'candidate.id', '=', 'jobs_applied.candidate_id')
                                ->join('jobs', 'jobs.id', '=', 'jobs_applied.jobs_id')
                                ->where('jobs.employer_id', $GetEmployerId)
                                ->count();

        // echo json_encode($CheckApplicantsCount);
        // exit;

        return view("UI.layouts.employer_manage_jobs", compact('title', 'GetJobsByEmployerId', 'GetActiveJobsCount', 'EmployerProfile', 'GetJobType', 'GetIndustries', 'GetJobTitle', 'GetQualification', 'GetTrainingTitle', 'GetTotalViewsCount', 'CheckApplicantsCount'));
    }


    public function ManageCAArticleshipJobs(){
        $title = "Manage CA Article";
        $GetEmployerId = Session::get("EmployerId");
        // $GetEmployerId = 1;
        $EmployerProfile = EmployerModel::where("id", $GetEmployerId)->first();

        $GetJobsByEmployerId =  DB::select("SELECT DISTINCT employer.profile_pic, jobs.slug, jobs.id,  employer.company_name, employer.country, employer.city, jobs.stipend, jobs.job_description, jobs.stipend_type, jobs.salary_min, jobs.salary_max,  jobs.created_at, employer.slug AS CompanySlug FROM jobs, employer WHERE jobs.employer_id = employer.id AND jobs.employer_id = $GetEmployerId AND jobs.status = 0 AND employee_type = 3 ORDER BY jobs.id");
        // echo json_encode($GetJobsByEmployerId);
        // exit;
        $GetActiveJobsCount = DB::select("SELECT * FROM jobs WHERE jobs.employer_id = $GetEmployerId AND jobs.status = 0");  
        
        $GetJobType = JobTypeModel::get();
        $GetIndustries = IndustriesModel::get();
        $GetJobTitle = JobTitleModel::get();
        $GetQualification = QualificationModel::get();
        $GetTrainingTitle = TrainingTitleModel::get();


        // $GetJobsId = "";
        // foreach($GetJobsByEmployerId as $JobsId){
        //     $GetJobsId = $JobsId->id;
        // }

        $GetTotalViewsCount = DB::table('total_views')
                            ->join('candidate', 'candidate.id', '=', 'total_views.candidate_id')
                            ->join('jobs', 'jobs.id', '=', 'total_views.jobs_id')
                            ->where('jobs.employer_id', $GetEmployerId)
                            ->count();

        $CheckApplicantsCount = DB::table('jobs_applied')
                                ->join('candidate', 'candidate.id', '=', 'jobs_applied.candidate_id')
                                ->join('jobs', 'jobs.id', '=', 'jobs_applied.jobs_id')
                                ->where('jobs.employer_id', $GetEmployerId)
                                ->count();

        // echo json_encode($CheckApplicantsCount);
        // exit;

        return view("UI.layouts.employer_manage_ca_articleship", compact('title', 'GetJobsByEmployerId', 'GetActiveJobsCount', 'EmployerProfile', 'GetJobType', 'GetIndustries', 'GetJobTitle', 'GetQualification', 'GetTrainingTitle', 'GetTotalViewsCount', 'CheckApplicantsCount'));
    }


    public function CloseCAArticleshipJobs(){
        $title = "Manage CA Article";
        $GetEmployerId = Session::get("EmployerId");
        // $GetEmployerId = 1;
        $EmployerProfile = EmployerModel::where("id", $GetEmployerId)->first();

        $GetJobsByEmployerId =  DB::select("SELECT DISTINCT employer.profile_pic, jobs.slug, jobs.id,  employer.company_name, employer.country, employer.city, jobs.stipend, jobs.job_description, jobs.stipend_type, jobs.salary_min, jobs.salary_max,  jobs.created_at, employer.slug AS CompanySlug FROM jobs, employer WHERE jobs.employer_id = employer.id AND jobs.employer_id = $GetEmployerId AND jobs.status = 1 AND employee_type = 3 ORDER BY jobs.id");
        // echo json_encode($GetJobsByEmployerId);
        // exit;
        $GetActiveJobsCount = DB::select("SELECT * FROM jobs WHERE jobs.employer_id = $GetEmployerId AND jobs.status = 0");  
        
        $GetJobType = JobTypeModel::get();
        $GetIndustries = IndustriesModel::get();
        $GetJobTitle = JobTitleModel::get();
        $GetQualification = QualificationModel::get();
        $GetTrainingTitle = TrainingTitleModel::get();


        // $GetJobsId = "";
        // foreach($GetJobsByEmployerId as $JobsId){
        //     $GetJobsId = $JobsId->id;
        // }

        $GetTotalViewsCount = DB::table('total_views')
                            ->join('candidate', 'candidate.id', '=', 'total_views.candidate_id')
                            ->join('jobs', 'jobs.id', '=', 'total_views.jobs_id')
                            ->where('jobs.employer_id', $GetEmployerId)
                            ->count();

        $CheckApplicantsCount = DB::table('jobs_applied')
                                ->join('candidate', 'candidate.id', '=', 'jobs_applied.candidate_id')
                                ->join('jobs', 'jobs.id', '=', 'jobs_applied.jobs_id')
                                ->where('jobs.employer_id', $GetEmployerId)
                                ->count();

        // echo json_encode($CheckApplicantsCount);
        // exit;

        return view("UI.layouts.employer_close_ca_articleship", compact('title', 'GetJobsByEmployerId', 'GetActiveJobsCount', 'EmployerProfile', 'GetJobType', 'GetIndustries', 'GetJobTitle', 'GetQualification', 'GetTrainingTitle', 'GetTotalViewsCount', 'CheckApplicantsCount'));
    }

    public function CloseJobs(){
        $title = "Closed Jobs";
        $GetEmployerId = Session::get("EmployerId");
        // $GetEmployerId = 1;
        $EmployerProfile = EmployerModel::where("id", $GetEmployerId)->first();

        $GetJobsByEmployerId = DB::select("SELECT DISTINCT job_title.name AS title, jobs.experience ,jobs.job_type, jobs.city, jobs.total_positions, jobs.salary_min, jobs.salary_max, jobs.location, jobs.status,
        jobs.created_at, jobs.slug, jobs.id, employer.slug As CompanySlug, jobs.employee_type FROM jobs, employer, job_title WHERE jobs.employer_id = employer.id AND jobs.employer_id = $GetEmployerId AND job_title.id = jobs.title AND jobs.status = 1");
        // echo json_encode($GetJobsByEmployerId);
        // exit;
        $GetActiveJobsCount = DB::select("SELECT * FROM jobs WHERE jobs.employer_id = $GetEmployerId AND jobs.status = 1");  
        
        $GetJobType = JobTypeModel::get();
        $GetIndustries = IndustriesModel::get();
        
        $GetJobTitle = JobTitleModel::get();
        $GetQualification = QualificationModel::get();
        $GetTrainingTitle = TrainingTitleModel::get();
        
        return view("UI.layouts.employer_close_jobs", compact('title', 'GetJobsByEmployerId', 'GetActiveJobsCount', 'EmployerProfile', 'GetJobType', 'GetIndustries', 'GetJobTitle', 'GetQualification', 'GetTrainingTitle'));
    }

    public function ManageTrainings(){
        $title = "Manage Trainings";
        $GetEmployerId = Session::get("EmployerId");
        $EmployerProfile = EmployerModel::where("id", $GetEmployerId)->first();

        $GetTrainingByEmployerId = DB::select("SELECT DISTINCT training_title.title, training.duration,
        training.start_date, training.training_completion, training.stipend, training.last_date_of_application, training.training_status, training.id, training.slug, employer.slug AS CompanySlug FROM training, employer, training_title WHERE training.employer_id = employer.id AND training.employer_id = $GetEmployerId AND training_title.id = training.title AND training.training_status = 0");

        // echo json_encode($GetTrainingByEmployerId);
        // exit;

        $GetActiveTrainingsCount = DB::select("SELECT * FROM training WHERE training.employer_id = $GetEmployerId AND training.training_status = 0");
        
        $GetIndustries = IndustriesModel::get();
        $GetStipend = TrainingCompletionModel::get();
        $GetJobTitle = JobTitleModel::get();
        $GetQualification = QualificationModel::get();
        $GetTrainingTitle = TrainingTitleModel::get();

        return view("UI.layouts.employer_manage_trainings", compact('title', 'GetTrainingByEmployerId', 'GetActiveTrainingsCount', 'EmployerProfile', 'GetStipend', 'GetJobTitle', 'GetQualification', 'GetTrainingTitle', 'GetIndustries'));
    }


    public function CloseTrainings(){
        $title = "Closed Trainings";
        $GetEmployerId = Session::get("EmployerId");
        $EmployerProfile = EmployerModel::where("id", $GetEmployerId)->first();

        $GetTrainingByEmployerId = DB::select("SELECT DISTINCT training_title.title, training.duration,
        training.start_date, training.training_completion, training.stipend, training.last_date_of_application, training.training_status, training.id, training.slug, employer.slug AS CompanySlug FROM training, employer, training_title WHERE training.employer_id = employer.id AND training.employer_id = $GetEmployerId AND training_title.id = training.title AND training.training_status = 1");

        $GetActiveTrainingsCount = DB::select("SELECT * FROM training WHERE training.employer_id = $GetEmployerId AND training.training_status = 0");
        
        $GetStipend = TrainingCompletionModel::get();
        $GetIndustries = IndustriesModel::get();
        
        $GetJobTitle = JobTitleModel::get();
        $GetQualification = QualificationModel::get();
        $GetTrainingTitle = TrainingTitleModel::get();

        return view("UI.layouts.employer_close_trainings", compact('title', 'GetTrainingByEmployerId', 'GetActiveTrainingsCount', 'EmployerProfile', 'GetStipend', 'GetIndustries', 'GetJobTitle', 'GetQualification', 'GetTrainingTitle'));
    }

    public function EditJobs($id){
        $title = "Edit Jobs";
        $GetEmployerId = Session::get("EmployerId");

        $GetJobsById = JobsModel::where('id', $id)->first();

        $GetSalary = JobsModel::where('employer_id', $GetEmployerId)
                                ->where('id', $id)
                                ->first();

        $GetSalaries = "";

        // foreach($GetSalary as $key => $value){
        //     $GetSalaries = explode('-', $value["offered_salary"]);
        // }
        $GetSalaries = explode('-', $GetSalary->offered_salary);

        // print_r($GetSalaries[0]);
        // exit;

        return view("UI.layouts.employer_edit_jobs", compact('title', 'GetJobsById', 'GetSalaries'));
    }

    
    public function GetTrainingsById($id){
        // $GetJobsById = TrainingModel::where('id', $id)->first();
        $GetJobsById = DB::table('training')
                        ->select('training.*')
                        ->join('training_title', 'training_title.id', '=', 'training.title')
                        ->where('training.id', $id)
                        ->first();
        echo json_encode($GetJobsById);
    }


    public function ViewAllAppliedTrainees($id, $slug){
        $title = "Applied Trainees";
        $GetEmployerId = Session::get("EmployerId");
        // $GetTrainingId = TrainingModel::where('id', $id)->first();
        $GetTrainingId = DB::table('training')
                        ->select('training.*', 'employer.slug AS CompanySlug', 'training_title.title')
                        ->join('training_title', 'training_title.id', '=', 'training.title')
                        ->join('employer', 'employer.id', '=', 'training.employer_id')
                        ->where('training.id', '=', $id)
                        ->first();
        
        // echo json_encode($GetTrainingId);
        // exit;

        $EmployerProfile = EmployerModel::where("id", $GetEmployerId)->first();

        $GetCandidates = DB::select("SELECT candidate.id, candidate_profile.profile_pic, candidate.name, candidate_profile.state, candidate_profile.desired_location, candidate.slug AS CandidateSlug, training.slug AS TrainingSlug, jobs_applied.training_status, jobs_applied.training_completion_status, jobs_applied.training_end_date FROM candidate, candidate_profile, jobs_applied, training WHERE candidate.id = candidate_profile.candidate_id AND jobs_applied.training_id = $id AND training.id = $id AND jobs_applied.candidate_id = candidate.id AND candidate.type = 2");

        $GetTraineeId = "";
        foreach($GetCandidates as $Canddiates){
            $GetTraineeId = $Canddiates->id;
        }

        $GetEducation = CandidateResume::where('candidate_id', $GetTraineeId)
                                        ->where('type', 1)
                                        ->orderBy('created_at', 'DESC')->first();
        // echo json_encode($GetTrainingId);
        // exit;

        $CheckShortlisted = DB::select("SELECT * FROM candidate, candidate_profile, shortlist_candidates WHERE candidate.id = candidate_profile.candidate_id AND candidate.type = 2 AND shortlist_candidates.candidate_id = '$GetTraineeId'");

        $GetCV = DB::table('resume')
                ->where('resume.candidate_id', '=', $GetTraineeId)
                ->where('resume.type', '=', 4)
                ->first();

        $GetStipend = TrainingCompletionModel::get();
        $GetJobTitle = JobTitleModel::get();
        $GetQualification = QualificationModel::get();
        $GetTrainingTitle = TrainingTitleModel::get();

        return view("UI.layouts.employer_applied_trainees", compact('title', 'GetCandidates', 'EmployerProfile', 'GetTrainingId', 'GetEducation', 'CheckShortlisted', 'GetCV', 'GetStipend', 'GetJobTitle', 'GetQualification', 'GetTrainingTitle'));
    }

    
    public function ViewAllAppliedCACandidates($id, $slug){
        $title = "Applied CA Candidates";
        $GetEmployerId = Session::get("EmployerId");
        // $GetTrainingId = TrainingModel::where('id', $id)->first();
        $GetJobsId = DB::table('jobs')
                        ->select('jobs.*', 'employer.slug AS CompanySlug', 'employer.company_name')
                        ->join('employer', 'employer.id', '=', 'jobs.employer_id')
                        ->where('jobs.id', '=', $id)
                        ->first();
        
        // echo json_encode($GetTrainingId);
        // exit;

        $EmployerProfile = EmployerModel::where("id", $GetEmployerId)->first();

        $GetCandidates = DB::select("SELECT candidate.id, candidate_profile.profile_pic, candidate.name, candidate_profile.state, candidate_profile.desired_location, candidate.slug AS CandidateSlug, jobs.slug AS TrainingSlug, jobs_applied.training_status, jobs_applied.training_completion_status, jobs_applied.training_end_date, qualifications.qualification FROM candidate, candidate_profile, jobs_applied, jobs, resume, qualifications WHERE resume.department = qualifications.id AND candidate.id = candidate_profile.candidate_id AND jobs_applied.jobs_id = $id AND jobs.id = $id AND jobs_applied.candidate_id = candidate.id AND candidate.type = 3 AND resume.candidate_id = candidate.id");

        $GetTraineeId = "";
        foreach($GetCandidates as $Canddiates){
            $GetTraineeId = $Canddiates->id;
        }

        $GetEducation = DB::table('resume')
                            ->select('resume.*', 'qualifications.qualification')
                            ->join('qualifications', 'qualifications.id', '=', 'resume.title')
                            ->where('type', 1)
                            ->where('resume.candidate_id', $id)
                            ->orderBy('id', 'DESC')
                            ->get();
        // echo json_encode($GetTrainingId);
        // exit;

        $CheckShortlisted = DB::select("SELECT * FROM candidate, candidate_profile, shortlist_candidates WHERE candidate.id = candidate_profile.candidate_id AND candidate.type = 2 AND shortlist_candidates.candidate_id = '$GetTraineeId'");

        $GetCV = DB::table('resume')
                ->where('resume.candidate_id', '=', $GetTraineeId)
                ->where('resume.type', '=', 4)
                ->first();

        $GetStipend = TrainingCompletionModel::get();
        $GetJobTitle = JobTitleModel::get();
        $GetQualification = QualificationModel::get();
        $GetTrainingTitle = TrainingTitleModel::get();

        return view("UI.layouts.employer_applied_ca_job_candidates", compact('title', 'GetCandidates', 'EmployerProfile', 'GetJobsId', 'GetEducation', 'CheckShortlisted', 'GetCV', 'GetStipend', 'GetJobTitle', 'GetQualification', 'GetTrainingTitle'));
    }


    public function TraineeDetails($slug, $TrainingSlug){
        $title = "Candidates Details";
        $GetEmployerId = Session::get("EmployerId");
        $GetCandidateId = CandidateModel::where('slug', $slug)->first();
        $GetTraineeId = TrainingModel::where('slug', $TrainingSlug)->first();

        $GetCandidateDetails = DB::select("SELECT * FROM candidate, candidate_profile WHERE candidate.id = candidate_profile.candidate_id AND candidate.id = $GetCandidateId->id AND candidate.type = 2");

        // echo json_encode($GetCandidateId);
        // exit;
        // $GetCandidates = DB::select("SELECT * FROM candidate, candidate_profile WHERE candidate.id = candidate_profile.candidate_id AND candidate.type = 1");
        $CheckSelectedTrainee = SelectedTraineeModel::where('candidate_id', $GetCandidateId->id)
                                        ->where('training_id', $GetTraineeId->id)                                
                                        ->where('employer_id', $GetEmployerId)
                                        ->first();
        
        return view("UI.layouts.trainee_details", compact('title', 'GetCandidateDetails', 'CheckSelectedTrainee'));
    }

    public function post_trainings(Request $request){
        $GetEmployerId = Session::get("EmployerId");
        $Training = new TrainingModel();

        $Training->employer_id = $GetEmployerId;
        $Training->training_id = rand(4, 7645);
        $Training->title = $request->title;
        $Training->slug = strtolower(str_replace(' ', '_', $request->title));
        $Training->start_date = date('Y-m-d', strtotime($request->start_date));
        $Training->duration = $request->duration;
        $Training->stipend = $request->stipend;
        $Training->last_date_of_application = date('Y-m-d', strtotime($request->last_date));
        $Training->training_completion = $request->completion;
        $Training->description = $request->description;
        // echo json_encode($request->title);
        // exit;

        $InsertTrainings = $Training->save();
        if($InsertTrainings){
            return response()->json(array(
                "error"=>FALSE,
                "message" => "Training posted successfully"
            ));
        }else{
            return response()->json(array(
                "error"=>TRUE,
                "message" => "Failed"
            ));
        }
    }


    public function UpdateTrainings(Request $request){
        $GetEmployerId = Session::get("EmployerId");

        $id = $request->id;
        $Training = TrainingModel::where('id', $id)->first();
        
        $Training->title = $request->title;
        $Training->slug = strtolower(str_replace(' ', '_', $request->title));
        $Training->start_date = date('Y-m-d', strtotime($request->start_date));
        $Training->duration = $request->duration;
        $Training->stipend = $request->stipend;
        $Training->last_date_of_application = date('Y-m-d', strtotime($request->last_date));
        $Training->training_completion = $request->completion;
        $Training->description = $request->description;

        $InsertTrainings = $Training->save();
        if($InsertTrainings){
            return response()->json(array(
                "error"=>FALSE,
                "message" => "Training updated successfully"
            ));
        }else{
            return response()->json(array(
                "error"=>TRUE,
                "message" => "Failed"
            ));
        }
    }



    public function SelectTraineeForTraining(Request $request){
        $GetStartDate = date('Y-m-d', strtotime($request->start_date));
        $GetCandidateSlug = $request->CandidateId;
        $GetEmployerId = Session::get("EmployerId");
        $GetCourseSlug = $request->TrainingId;

        $SelectedTraining = new SelectedTraineeModel();
        $GetCandidateIdBySlug = CandidateModel::where('id', $GetCandidateSlug)->first();
        $GetCourseIdBySlug = TrainingModel::where('slug', $GetCourseSlug)->first();


        $CheckSelectedTraining = SelectedTraineeModel::where('candidate_id', $GetCandidateIdBySlug["id"])
                                                    ->where('training_id', $GetCourseIdBySlug->id)
                                                    ->where('employer_id', $GetEmployerId)
                                                    ->first();
        // echo json_encode($GetCandidateIdBySlug);
        // exit;
        if($CheckSelectedTraining != null){
            return response()->json(array(
                "error"=>TRUE,
                "message" => "Trainee already selected"
            ));
        }else{
            $SelectedTraining->training_start_date = $GetStartDate;
            $SelectedTraining->employer_id = $GetEmployerId;
            $SelectedTraining->candidate_id = $GetCandidateIdBySlug["id"];
            $SelectedTraining->training_id = $GetCourseIdBySlug->id;
            
            $SelectedTraining->training_end_date = date('Y-m-d', strtotime($GetStartDate. ' + '.$GetCourseIdBySlug->duration.' days'));

            // echo date('Y-m-d', strtotime($GetStartDate. ' + '.$GetCourseIdBySlug->duration.' days'));
            // exit;
            $SelectedTraining->training_status = 0;
    
            $SelectedTraining->save();

            return response()->json(array(
                "error"=>FALSE,
                "message" => "Trainee selected successfully"
            ));
        }
    }

    public function ViewAllSelectedTrainees(){
        $title = "Selected Trainees";
        $GetEmployerId = Session::get("EmployerId");
        $EmployerProfile = EmployerModel::where("id", $GetEmployerId)->first();

        $GetCandidates = DB::select("SELECT training.id AS TrainingId, candidate.id AS CandidateId, candidate.name, training.title, training.duration, training.slug, selected_trainee.training_start_date, selected_trainee.training_end_date, selected_trainee.training_status, employer.slug AS CompanySlug, training.slug AS TrainingSlug FROM selected_trainee, candidate, candidate_profile, employer, training WHERE selected_trainee.candidate_id = candidate.id AND selected_trainee.employer_id = $GetEmployerId AND selected_trainee.training_id = training.id AND candidate_profile.candidate_id = candidate.id AND candidate.type = 2");
        // echo json_encode($GetCandidates);
        // exit;
        $GetActiveTrainingsCount = DB::select("SELECT * FROM selected_trainee, candidate WHERE selected_trainee.candidate_id = candidate.id AND selected_trainee.training_status = 1");

        $GetTrainingByEmployerId = DB::select("SELECT training.title, training.duration,
        training.start_date, training.last_date_of_application, training.training_status, training.id, training.slug, employer.slug AS CompanySlug FROM training, employer WHERE training.employer_id = $GetEmployerId");

        return view("UI.layouts.selected_training_candidates", compact('title', 'GetCandidates', 'GetActiveTrainingsCount', 'GetTrainingByEmployerId', 'EmployerProfile'));
    }

    // Add Ratings

    public function AddStars(Request $request){
        $Reviews = new ReviewsModel();

        $GetRatings = $request->Ratings;
        $GetCandidateId = $request->CandidateId;
        $GetTrainingId = $request->TrainingId;
        $GetEmployerId = Session::get("EmployerId");

        $CheckRatings = ReviewsModel::where('candidate_id', $GetCandidateId)
                                    ->where('training_id', $GetTrainingId)
                                    ->first();
        if($CheckRatings != null){
            return response()->json(array(
                "error" => TRUE,
                "message" => "Already rating this student."
            ));
        }else{
            $Reviews->candidate_id = $GetCandidateId;
            $Reviews->training_id = $GetTrainingId;
            $Reviews->review_stars = $GetRatings;
            $Reviews->feedback = $request->feedback;;
    
            $AddReviews = $Reviews->save();

            $UpdateTrainingStatus = SelectedTraineeModel::where('candidate_id', $GetCandidateId)
                                                        ->where('training_id', $GetTrainingId)
                                                        ->where('employer_id', $GetEmployerId)
                                                        ->first();

            $UpdateTrainingStatus->training_status = 1;
            $UpdateTrainingStatus->save();
            if($AddReviews){
                return response()->json(array(
                    "error" => FALSE,
                    "message" => "Review successfully"
                ));
            }else{
                return response()->json(array(
                    "error" => TRUE,
                    "message" => "Failed"
                ));
            }
        }
        
    }

    public function ViewAllAppliedCandidates($GetJobsId, $slug){
        $title = "Applied Candidates";
        $GetEmployerId = Session::get("EmployerId");
        // $GetJobsId = JobsModel::where('slug', $slug)->first();
        $EmployerProfile = EmployerModel::where("id", $GetEmployerId)->first();

        $GetCandidates = DB::select("SELECT candidate_profile.profile_pic, candidate_profile.experience, candidate_profile.gross_salary, candidate.id, candidate.name, candidate_profile.state, candidate_profile.desired_location, candidate.slug, qualifications.qualification, resume.from_year FROM candidate, candidate_profile, jobs_applied, resume, qualifications WHERE candidate.id = candidate_profile.candidate_id AND jobs_applied.jobs_id = $GetJobsId AND jobs_applied.candidate_id = candidate.id AND candidate.type = 1 AND resume.candidate_id = candidate.id AND resume.type = 1 AND qualifications.id = resume.department");

        $GetJobsById = DB::select("SELECT jobs.id, jobs.jobs_id, job_title.name AS title, employer.website, employer.mobile, employer.email, employer.profile_pic, jobs.total_positions, employer.company_name, employer.city , jobs.salary_min, jobs.salary_max, jobs.job_type, jobs.created_at, jobs.job_description,  jobs.industry, jobs.experience, jobs.qualification, employer.slug AS CompanySlug, jobs.status, jobs.slug FROM jobs, employer, job_title WHERE jobs.employer_id = employer.id AND job_title.id = jobs.title AND jobs.id = '$GetJobsId'");

        // echo json_encode($GetCandidates);
        // exit;

        $GetProfessionalDetails = DB::table('candidate')
                                ->select('resume.name', 'job_title.name AS JobTitle')
                                ->join('resume', 'candidate.id', '=', 'resume.candidate_id')
                                ->join('job_title', 'job_title.id', '=', 'resume.department')
                                ->where('resume.worked_till', '=', 1)
                                ->first();

        $GetTotalViewsCount = TotalViews::where('jobs_id', $GetJobsId)
                                        ->get();

        $CheckApplicantsCount = JobsApplied::where('jobs_id', $GetJobsId)
                                        ->get();

                                        $GetCandidateId = "";
        foreach($GetCandidates as $Candidates){
            $GetCandidateId = $Candidates->id;
        }

        $GetCV = DB::table('resume')
                ->where('resume.candidate_id', '=', $GetCandidateId)
                ->where('resume.type', '=', 4)
                ->first();

        $CheckShortlisted = DB::select("SELECT * FROM candidate, candidate_profile, shortlist_candidates WHERE candidate.id = candidate_profile.candidate_id AND shortlist_candidates.candidate_id = '$GetCandidateId' AND candidate.type = 1 ORDER BY candidate.id ASC");

        $GetJobTitle = JobTitleModel::get();
        $GetQualification = QualificationModel::get();
        $GetTrainingTitle = TrainingTitleModel::get();
        $GetIndustries = IndustriesModel::get();
        
        return view("UI.layouts.employer_applied_jobs_candidates", compact('title', 'GetCandidates', 'GetJobsById', 'GetTotalViewsCount', 'CheckApplicantsCount', 'GetProfessionalDetails', 'CheckShortlisted', 'GetCV', 'EmployerProfile', 'GetJobTitle', 'GetQualification', 'GetTrainingTitle', 'GetIndustries'));
    }

    public function DownloadCV($CandidateId, $JobsId){
        $GetEmployerId = Session::get("EmployerId");

        $GetUpdateRecruiterStatus = JobsApplied::where('candidate_id', $CandidateId)
                                    // ->where('employer_id', $GetEmployerId)
                                    ->where('jobs_id', $JobsId)
                                    ->first();

        $GetUpdateRecruiterStatus->employer_id = $GetEmployerId;

        if($GetUpdateRecruiterStatus->recruiter_status ==null){
            $GetUpdateRecruiterStatus->recruiter_status = 1;
        }

        
        $GetUpdateRecruiterStatus->download_cv_status = 1;

        if($GetUpdateRecruiterStatus->application_view_date ==null){
            $GetUpdateRecruiterStatus->application_view_date = date('Y-m-d');
        }

        $GetUpdateRecruiterStatus->download_cv_date = date('Y-m-d');
        $UpdateStatus = $GetUpdateRecruiterStatus->save();

        if($UpdateStatus){
            return response()->json(array(
                "error"=>FALSE,
                "message"=>"Updated successfully"
            ));
        }else{
            return response()->json(array(
                "error"=>TRUE,
                "message"=>"Updated failed"
            ));
        }
    }


    public function LoadmoreCandidates(Request $request){
        $GetCandidatesId = $request->CandidateId;
        
        $GetResults = "";


        $GetCandidates = DB::select("SELECT * FROM candidate, candidate_profile WHERE candidate.id = candidate_profile.candidate_id AND candidate.type = 1 AND candidate.id > $GetCandidatesId AND candidate_profile.allow_search = 1 ORDER BY candidate.created_at ASC LIMIT 1");

        if ($GetCandidates != null){
            foreach($GetCandidates as $Candidates){
                $GetResults .= "<input type='hidden' id='CandidateId' value='".$Candidates->id."'>
                <div class='emply-resume-list square'>
                    <div class='row'>
                           <div class='col-md-8'>
                               <div class='emply-resume-thumb'>
                                   <img src='{{URL::asset('candidate_profile')}}/".$Candidates->profile_pic."' alt='' />
                               </div>
                               <div class='emply-resume-info'>
                               <h3><a href='/CandidateDetails/".$Candidates->slug."' title='' target='_blank'>".$Candidates->name."</a></h3>
                                   <span><i>".$Candidates->role."</i> at Atract Solutions</span>
                                   <p><i class='la la-map-marker'></i>".$Candidates->address." / ".$Candidates->country."</p>
                               </div>
                            </div>
                            <div class='col-md-4'>
                               <div class='shortlists'>
                                   <a href='/CandidateDetails/".$Candidates->slug."' id='' title='' target='_blank'>View Profile <i class='la la-plus'></i></a>
                               </div>
                           </div>
                    </div>
                </div>";
            }
            
            $GetResults .= "<div class='col-lg-12' id='remove_candidate_load_more'>
                                <div class='browse-all-cat'>
                                    <div class='text-center' id='ApplyBtnLoader' style='display: none;'>
                                        <img class='applyJobsLoader' src='{{URL::asset('UI/apply_btn_loader.gif')}}' alt=''>
                                    </div>
                                    <a href='javascript:void(0);' data-id='".$Candidates->id."' id='LoadmoreCandidates' title='' class='style2'>Load more candidates...</a>
                                </div>
                            </div>";

            echo $GetResults;
        }
    }

    public function UpdatePassword(Request $request){
        $GetEmployerId = Session::get('EmployerId');
        $GetOldPassword = EmployerModel::where('id', $GetEmployerId)->first();

        $CheckPassword = Hash::check($request->OldPassword, $GetOldPassword->password);
        // echo json_encode($CheckPassword);
        // exit;
        $GetNewPassword = $request->NewPassword;
        $GetVerifyPassword = $request->VerifyPassword;


        if($GetNewPassword != $GetVerifyPassword){
            return response()->json(array(
                "error"=>TRUE,
                "message" => "Password mismatch"
            ));
        }else if($CheckPassword == false){
            return response()->json(array(
                "error"=>TRUE,
                "message" => "Old password is wrong"
            ));
        }else if($CheckPassword == true){
            $GetOldPassword->password = Hash::make($request->VerifyPassword);
            $GetOldPassword->save();
            return response()->json(array(
                "error"=>FALSE,
                "message" => "Password updated successfully"
            ));
        }
    }




    public function UpdateProfilePic(Request $request){
        $GetEmployerId = Session::get('EmployerId');
        
        $GetEmployerProfile = EmployerModel::where('id', $GetEmployerId)->first();

        $extension = $request->file('pic')->getClientOriginalExtension();
        $dir = 'employer_profile/';
        $filename = uniqid() . '_' . time() . '.' . $extension;
        $request->file('pic')->move($dir, $filename);


        $GetEmployerProfile->profile_pic = $filename;

        $UpdatePic = $GetEmployerProfile->save();

        if($UpdatePic){
            return response()->json(array(
                "error"=>FALSE,
                "message"=>"Profile updated successfully"
            ));
        }else{
            return response()->json(array(
                "error"=>TRUE,
                "message"=>"Failed."
            ));
        }
    }


    public function GetEmpProfileById($Id){
        $GetEmpProfileById = EmployerModel::where('id', $Id)->first();

        echo json_encode($GetEmpProfileById);
    }
}
