<?php

namespace App\Http\Controllers\UI;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Model\UI\EmployerModel;
use App\Model\UI\JobsModel;
use App\Model\UI\TrainingModel;
use App\Model\UI\CandidateProfileModel;
use App\Model\UI\CandidateModel;
use App\Model\UI\TotalViews;
use App\Model\UI\JobsApplied;
use Illuminate\Support\Facades\Auth;
use App\Model\UI\ShortlistCandidatesModel;
use App\Model\UI\SelectedTraineeModel;
use App\Model\UI\ReviewsModel;
use Illuminate\Support\Facades\Hash;
use App\Model\UI\FavouriteModel;
use App\Model\UI\CandidateResume;

use DateTime;
use Session;
use DB;

// Admin Model
use App\Model\Admin\TrainingCompletionModel;
use App\Model\Admin\IndustriesModel;
use App\Model\Admin\JobTitleModel;
use App\Model\Admin\QualificationModel;
use App\Model\Admin\TrainingTitleModel;


class CaArticleController extends Controller
{

    public function logout(Request $request) {
        // Auth::logout();
        Auth::guard('ca_article')->logout();
        return redirect('/');
    }


    public function ArticleProfile(){
        $title = "Ca Article Profile";
        $GetEmail = Session::get('CaArticleEmail');
        $GetArticleId = Session::get('CaArticleId');
        $ArticleProfile = CandidateProfileModel::where("candidate_id", $GetArticleId)->first();
        if($ArticleProfile){
            $GetArticleId = Session::get('CaArticleId');
        }else{
            $GetArticleId = "0";
        }
        // echo $GetCandidateId;
        // exit;

        $GetFavouriteTrainings = DB::select("SELECT employer.profile_pic, training.title, training.duration, employer.slug AS CompanySlug, training.slug, employer.company_name FROM favourite_jobs, training, employer WHERE favourite_jobs.candidate_id = $GetArticleId AND favourite_jobs.training_id = training.id AND training.employer_id = employer.id AND favourite_jobs.status = 1 AND favourite_jobs.type = 2");

        
        $GetCV = CandidateResume::where('candidate_id', $GetArticleId)
        ->where('type', 4)->first();

        

        $Profile = CandidateModel::where("id", $GetArticleId)->first();
        // echo json_encode($GetArticleId);
        // exit;

        $GetQualification = QualificationModel::get();


        $GetEducation = DB::table('resume')
                            ->select('resume.*', 'qualifications.qualification')
                            ->join('qualifications', 'qualifications.id', '=', 'resume.department')
                            ->where('type', 1)
                            ->where('resume.candidate_id', $GetArticleId)
                            ->orderBy('id', 'DESC')
                            ->get();

        return view("UI.layouts.article_profile", compact('title', "Profile", 'ArticleProfile', 'GetFavouriteTrainings', 'GetCV', 'GetEducation', 'GetQualification'));
    }



    public function Dashboard(){
        $title = "Article Dashboard";
        $GetEmail = Session::get('CaArticleEmail');
        $GetArticleId = Session::get('CaArticleId');
        $ArticleProfile = CandidateProfileModel::where("candidate_id", $GetArticleId)->first();
        if($ArticleProfile){
            $GetArticleId = Session::get('CaArticleId');
        }else{
            $GetArticleId = "0";
        }
        // echo $GetCandidateId;
        // exit;
        $Profile = CandidateModel::where("id", $GetArticleId)->first();
        
        $GetCAJobs = DB::select("SELECT DISTINCT employer.profile_pic, jobs.slug, jobs.id,  employer.company_name, employer.country, employer.city, jobs.stipend, jobs.job_description, jobs.stipend_type, jobs.salary_min, jobs.salary_max,  jobs.created_at, employer.slug AS CompanySlug FROM jobs, employer WHERE jobs.employer_id = employer.id AND employee_type = 3 ORDER BY jobs.id");

        // $GetStipend = TrainingCompletionModel::get();

        // $GetFavouriteJobs = DB::select("SELECT employer.profile_pic, jobs.title, employer.slug AS CompanySlug, jobs.slug, employer.company_name FROM favourite_jobs, jobs, employer WHERE favourite_jobs.candidate_id = $GetCandidateId AND favourite_jobs.jobs_id = jobs.id AND jobs.employer_id = employer.id AND favourite_jobs.status = 1 AND favourite_jobs.type = 1");

        return view("UI.layouts.article_dashboard", compact('title', "Profile", 'ArticleProfile', 'GetFavouriteJobs', 'GetCAJobs', 'GetStipend'));
    }


    public function PostJobs(Request $request){
        $GetEmployerId = Session::get("EmployerId");

        $Jobs = new JobsModel();

        $Jobs->employer_id = $GetEmployerId;
        $Jobs->title = "Ca Article";
        $Jobs->slug = strtolower(str_replace(' ', '_', "Ca Article"));
        $Jobs->zip_code = $request->zipcode;
        $Jobs->country = $request->country;
        $Jobs->state = $request->state;
        $Jobs->city = $request->city;
        $Jobs->location = $request->address;
        $Jobs->job_description = $request->description;
        $Jobs->stipend_type = $request->stipend_type;
        $Jobs->salary_min = $request->salary_min;
        $Jobs->salary_max = $request->salary_max;
        $Jobs->salary_type = $request->salary_type;
        $Jobs->stipend = $request->stipend;
        $Jobs->qualification = $request->qualification;
        $Jobs->total_positions = $request->positions;
        $Jobs->employee_type = 3;

        $AddJobs = $Jobs->save();
        if($AddJobs){
            return response()->json(array(
                "error" => FALSE,
                "message" => "Jobs posted successfully"
            ));
        }else{
            return response()->json(array(
                "error" => TRUE,
                "message" => "Failed"
            ));
        }

    }


    public function FilterDatePostedCAJobs(Request $request){
        $GetDatePosted = $request->date_posted;

        $GetEmployerId = Session::get("EmployerId");

        $GetDataByDatePosted = [];
        // echo $GetDatePosted;
        // exit;

        if(Auth::guard('employer')->check()){
            if($GetDatePosted == 1){
                $GetDataByDatePosted[] = DB::select("SELECT DISTINCT employer.profile_pic, jobs.slug, jobs.id,  employer.company_name, employer.country, employer.city, jobs.stipend, jobs.job_description, jobs.stipend_type, jobs.salary_min, jobs.salary_max,  jobs.created_at, employer.slug AS CompanySlug FROM jobs, employer WHERE jobs.employer_id = employer.id AND jobs.employer_id = $GetEmployerId AND jobs.status = 0 AND employee_type = 3 AND  jobs.created_at >= DATE_SUB(CURDATE(), INTERVAL 1 HOUR)");
            }elseif($GetDatePosted == 2){
    
                $GetDataByDatePosted[] = DB::select("SELECT DISTINCT employer.profile_pic, jobs.slug, jobs.id,  employer.company_name, employer.country, employer.city, jobs.stipend, jobs.job_description, jobs.stipend_type, jobs.salary_min, jobs.salary_max,  jobs.created_at, employer.slug AS CompanySlug FROM jobs, employer WHERE jobs.employer_id = employer.id AND jobs.employer_id = $GetEmployerId AND jobs.status = 0 AND employee_type = 3 AND jobs.created_at >= CURRENT_TIMESTAMP - INTERVAL '24' HOUR");
    
            }elseif($GetDatePosted == 3){
    
                $GetDataByDatePosted[] = DB::select("SELECT DISTINCT employer.profile_pic, jobs.slug, jobs.id,  employer.company_name, employer.country, employer.city, jobs.stipend, jobs.job_description, jobs.stipend_type, jobs.salary_min, jobs.salary_max,  jobs.created_at, employer.slug AS CompanySlug FROM jobs, employer WHERE jobs.employer_id = employer.id AND jobs.employer_id = $GetEmployerId AND jobs.status = 0 AND employee_type = 3 AND jobs.created_at >= DATE_ADD(CURDATE(), INTERVAL -7 DAY)");
    
            }elseif($GetDatePosted == 4){
    
                $GetDataByDatePosted[] = DB::select("SELECT DISTINCT employer.profile_pic, jobs.slug, jobs.id,  employer.company_name, employer.country, employer.city, jobs.stipend, jobs.job_description, jobs.stipend_type, jobs.salary_min, jobs.salary_max,  jobs.created_at, employer.slug AS CompanySlug FROM jobs, employer WHERE jobs.employer_id = employer.id AND jobs.employer_id = $GetEmployerId AND jobs.status = 0 AND employee_type = 3 ORDER BY jobs.id");
            }
        }else{
            if($GetDatePosted == 1){
                $GetDataByDatePosted[] = DB::select("SELECT DISTINCT employer.profile_pic, jobs.slug, jobs.id,  employer.company_name, employer.country, employer.city, jobs.stipend, jobs.job_description, jobs.stipend_type, jobs.salary_min, jobs.salary_max,  jobs.created_at, employer.slug AS CompanySlug FROM jobs, employer WHERE jobs.employer_id = employer.id  AND jobs.status = 0 AND employee_type = 3 AND  jobs.created_at >= DATE_SUB(CURDATE(), INTERVAL 1 HOUR)");
            }elseif($GetDatePosted == 2){
    
                $GetDataByDatePosted[] = DB::select("SELECT DISTINCT employer.profile_pic, jobs.slug, jobs.id,  employer.company_name, employer.country, employer.city, jobs.stipend, jobs.job_description, jobs.stipend_type, jobs.salary_min, jobs.salary_max,  jobs.created_at, employer.slug AS CompanySlug FROM jobs, employer WHERE jobs.employer_id = employer.id  AND jobs.status = 0 AND employee_type = 3 AND jobs.created_at >= CURRENT_TIMESTAMP - INTERVAL '24' HOUR");
    
            }elseif($GetDatePosted == 3){
    
                $GetDataByDatePosted[] = DB::select("SELECT DISTINCT employer.profile_pic, jobs.slug, jobs.id,  employer.company_name, employer.country, employer.city, jobs.stipend, jobs.job_description, jobs.stipend_type, jobs.salary_min, jobs.salary_max,  jobs.created_at, employer.slug AS CompanySlug FROM jobs, employer WHERE jobs.employer_id = employer.id AND jobs.status = 0 AND employee_type = 3 AND jobs.created_at >= DATE_ADD(CURDATE(), INTERVAL -7 DAY)");
    
            }elseif($GetDatePosted == 4){
    
                $GetDataByDatePosted[] = DB::select("SELECT DISTINCT employer.profile_pic, jobs.slug, jobs.id,  employer.company_name, employer.country, employer.city, jobs.stipend, jobs.job_description, jobs.stipend_type, jobs.salary_min, jobs.salary_max,  jobs.created_at, employer.slug AS CompanySlug FROM jobs, employer WHERE jobs.employer_id = employer.id AND jobs.status = 0 AND employee_type = 3 ORDER BY jobs.id");
            }
        }


        $GetPostedOn = "";
        $GetTrainingResults = "";

        $GetTrainingDuration = "";

        $GetStipend = "";

        foreach($GetDataByDatePosted as $GetData){
            foreach($GetData as $Jobs){
                $now = new DateTime;
            $full = false;	
            $ago = new DateTime($Jobs->created_at);
            $diff = $now->diff($ago);

            $diff->w = floor($diff->d / 7);
            $diff->d -= $diff->w * 7;

            $string = array(
            'y' => 'year',
            'm' => 'month',
            'w' => 'week',
            'd' => 'day',
            'h' => 'hour',
            'i' => 'minute',
            's' => 'second',
            );

            foreach ($string as $k => &$v) {
            if ($diff->$k) {
            $v = $diff->$k . ' ' . $v . ($diff->$k > 1 ? 's' : '');
            } else {
            unset($string[$k]);
            }
            }

            if (!$full) $string = array_slice($string, 0, 1);
            $GetPostedOn =  $string ? implode(', ', $string) . ' ago' : 'just now';

            

            if($Jobs->stipend_type == 1){
                // $num = $Jobs->stipend;
                // $units = ['', 'K', 'M'];
                // for($i = 0; $num>=1000;$i++){
                //     $num /= 1000;
                // }
                
                // $GetStipend = round($num, 1).$units[$i];
                $GetStipend = "";

            }else
            {
                $GetStipend = "<i class='fa fa-money'></i>". $Jobs->salary_min."-".$Jobs->salary_max ."/ Month";
            }


                
            if(Auth::guard('employer')->check()){
                $GetJobsResults = "<div class='job-listings-sec no-border text-center'>
                <div class='job-listing wtabs recommented-jobs'>
                    <div class='job-title-sec recommended-jobs-pad-left'>
                    <h3 class='manage-jobs-list pad-bottom10'><a href='/CAJobDetails/".$Jobs->CompanySlug."/".$Jobs->id."/".$Jobs->slug."' title=''>CA Article</a></h3>
                    <p class='recommended-jobs-company-name'>".$Jobs->company_name."</p>
                        
                        <div class='job-lctn'> 
                           ".$GetStipend."
                        </div>
                        <p class='mar-bottom0'>".$Jobs->job_description."</p><br>
                        <div class='row'>
                            <div class='col-md-4'>
                            <button type='button' onclick='javascript:window.open('/ViewAllAppliedCACandidates/".$Jobs->id."/".$Jobs->slug."', '_blank');' formtarget='_blank'  class='view-details-jobs'> <span class='view-details-dot'>...</span> View Details</button>
                            </div>
                            
                            <div class='col-md-3'>
                                <button type='button' onclick='EditCAArticle(".$Jobs->id.")' class='view-details-jobs'> <i class='fa fa-pencil'></i> Edit</button>
                            </div>
                            <div class='col-md-2'>
                                <h3 class='text-left manage-jobs-status'>Published</h3>
                            </div>
                            
                        </div>
                    </div>
                </div>
            </div>";

            }else{
$GetJobsResults = "<div class='job-listings-sec no-border text-center'>          
                    <div class='job-listing wtabs recommented-jobs'>
                        <div class='job-title-sec recommended-jobs-pad-left'>
                            <h3><a href='/CAJobDetails/".$Jobs->CompanySlug."/".$Jobs->id."/".$Jobs->slug."' target='_blank' title=''>CA Article</a></h3>
                            <p class='recommended-jobs-company-name'>".$Jobs->company_name."</p>
                            <div class='job-lctn job-attributes-recommended-jobs'>
                            ".$GetStipend."
                            </div>
                            </div>
                        <p class='recommended-jobs-description'>".$Jobs->job_description."</p>
                        <p class='recommended-jobs-posted-on'>Posted on: 
                        ".$GetPostedOn."
                        </p>
                    </div>
                    </div>";
            }

                if($GetJobsResults){
                    echo $GetJobsResults;
                }else{
                    echo json_encode("No Results Found...");
                }
            }
        }

    }


    public function FilterDatePostedClosedCAJobs(Request $request){
        $GetDatePosted = $request->date_posted;

        $GetDataByDatePosted = [];

        if($GetDatePosted == 1){
            $GetDataByDatePosted[] = DB::select("SELECT DISTINCT employer.profile_pic, jobs.slug, jobs.id,  employer.company_name, employer.country, employer.city, jobs.stipend, jobs.job_description, jobs.stipend_type, jobs.salary_min, jobs.salary_max,  jobs.created_at, employer.slug AS CompanySlug FROM jobs, employer WHERE jobs.employer_id = employer.id AND jobs.status = 1 AND employee_type = 3 AND  jobs.created_at >= DATE_SUB(CURDATE(), INTERVAL 1 HOUR)");
        }elseif($GetDatePosted == 2){

            $GetDataByDatePosted[] = DB::select("SELECT DISTINCT employer.profile_pic, jobs.slug, jobs.id,  employer.company_name, employer.country, employer.city, jobs.stipend, jobs.job_description, jobs.stipend_type, jobs.salary_min, jobs.salary_max,  jobs.created_at, employer.slug AS CompanySlug FROM jobs, employer WHERE jobs.employer_id = employer.id AND jobs.status = 1 AND employee_type = 3 AND jobs.created_at >= CURRENT_TIMESTAMP - INTERVAL '24' HOUR");

        }elseif($GetDatePosted == 3){

            $GetDataByDatePosted[] = DB::select("SELECT DISTINCT employer.profile_pic, jobs.slug, jobs.id,  employer.company_name, employer.country, employer.city, jobs.stipend, jobs.job_description, jobs.stipend_type, jobs.salary_min, jobs.salary_max,  jobs.created_at, employer.slug AS CompanySlug FROM jobs, employer WHERE jobs.employer_id = employer.id AND jobs.status = 1 AND employee_type = 3 AND jobs.created_at >= DATE_ADD(CURDATE(), INTERVAL -7 DAY)");

        }elseif($GetDatePosted == 4){

            $GetDataByDatePosted[] = DB::select("SELECT DISTINCT employer.profile_pic, jobs.slug, jobs.id,  employer.company_name, employer.country, employer.city, jobs.stipend, jobs.job_description, jobs.stipend_type, jobs.salary_min, jobs.salary_max,  jobs.created_at, employer.slug AS CompanySlug FROM jobs, employer WHERE jobs.employer_id = employer.id AND jobs.status = 1 AND employee_type = 3 ORDER BY jobs.id");
        }


        $GetPostedOn = "";
            $GetTrainingResults = "";
    
            $GetTrainingDuration = "";
    
            $GetStipend = "";
    
            foreach($GetDataByDatePosted as $GetData){
                foreach($GetData as $Jobs){
                    $now = new DateTime;
                $full = false;	
                $ago = new DateTime($Jobs->created_at);
                $diff = $now->diff($ago);
    
                $diff->w = floor($diff->d / 7);
                $diff->d -= $diff->w * 7;
    
                $string = array(
                'y' => 'year',
                'm' => 'month',
                'w' => 'week',
                'd' => 'day',
                'h' => 'hour',
                'i' => 'minute',
                's' => 'second',
                );
    
                foreach ($string as $k => &$v) {
                if ($diff->$k) {
                $v = $diff->$k . ' ' . $v . ($diff->$k > 1 ? 's' : '');
                } else {
                unset($string[$k]);
                }
                }
    
                if (!$full) $string = array_slice($string, 0, 1);
                $GetPostedOn =  $string ? implode(', ', $string) . ' ago' : 'just now';
    
    
                if($Jobs->stipend_type == 1){
                    $num = $Jobs->stipend;
                    $units = ['', 'K', 'M'];
                    for($i = 0; $num>=1000;$i++){
                        $num /= 1000;
                    }
                    
                    $GetStipend = round($num, 1).$units[$i];
    
                }else
                {
                    $GetStipend = $Jobs->salary_min."-".$Jobs->salary_max;
                }
    
    
                    
    
                    $GetJobsResults = "<div class='job-listings-sec no-border'>
                    <div class='job-listing wtabs pad-top0'>
                        <div class='job-title-sec width100'>
                        <h3 class='manage-jobs-list pad-bottom10'><a href='/CAJobDetails/".$Jobs->CompanySlug."/".$Jobs->id."/".$Jobs->slug."' title=''>CA Article</a></h3>
                        <p class='recommended-jobs-company-name'>".$Jobs->company_name."</p>
                            
                            <div class='job-lctn'><i class='fa fa-money'></i> 
                               ".$GetStipend." / Month
                            </div>
                            <p class='mar-bottom0'>".$Jobs->job_description."</p><br>
                            <div class='row'>
                                <div class='col-md-4'>
                                <button type='button' onclick='javascript:window.open('/ViewAllAppliedCACandidates/".$Jobs->id."/".$Jobs->slug."', '_blank');' formtarget='_blank'  class='view-details-jobs'> <span class='view-details-dot'>...</span> View Details</button>
                                </div>
                                
                                <div class='col-md-3'>
                                    <button type='button' onclick='EditCAArticle(".$Jobs->id.")' class='view-details-jobs'> <i class='fa fa-pencil'></i> Edit</button>
                                </div>
                                <div class='col-md-2'>
                                    <h3 class='text-left manage-jobs-status'>Published</h3>
                                </div>
                                
                            </div>
                        </div>
                    </div>
                </div>";
    
                    if($GetJobsResults){
                        echo $GetJobsResults;
                    }else{
                        echo json_encode("No Results Found...");
                    }
            }
        }

    }



    public function SearchCAJobsLocationByKeypress(Request $request){
        $GetLocationInput = $request->Location;
    
        $GetLocationByKeyPress = DB::select("SELECT DISTINCT city, id FROM jobs WHERE jobs.status = 0 AND employee_type = 3 AND city LIKE '%$GetLocationInput%'");
        
        
        $GetLocation = "";
        $GetResult = "";
    
        foreach($GetLocationByKeyPress as $Location){
            // $GetLocation = $Location->city;
            $GetResult = "<p><input type='checkbox' class='CheckManageCAJobsFilters'  value=".$Location->city." name='Location' id=".$Location->id."><label for=".$Location->id." class='CheckBoxFilter'>".$Location->city."</label></p>";
        
        }
        if($GetResult){
            echo $GetResult;
        }else{
            echo json_encode("No Results Found...");
        }
        
    
    }


    public function SearchClosedCAJobsLocationByKeypress(Request $request){
        $GetLocationInput = $request->Location;
    
        $GetLocationByKeyPress = DB::select("SELECT DISTINCT city, id FROM jobs WHERE jobs.status = 1 AND employee_type = 3 AND city LIKE '%$GetLocationInput%'");
        
        
        $GetLocation = "";
        $GetResult = "";
    
        foreach($GetLocationByKeyPress as $Location){
            // $GetLocation = $Location->city;
            $GetResult = "<p><input type='checkbox' class='CheckCloseCAJobsFilters'  value=".$Location->city." name='Location' id=".$Location->id."><label for=".$Location->id." class='CheckBoxFilter'>".$Location->city."</label></p>";
        
        }
        if($GetResult){
            echo $GetResult;
        }else{
            echo json_encode("No Results Found...");
        }
        
    
    }


    public function SearchFresherJobsLocationByKeypress(Request $request){
        $GetLocationInput = $request->Location;
    
        $GetLocationByKeyPress = DB::select("SELECT DISTINCT city, id FROM jobs WHERE jobs.status = 0 AND employee_type = 1 AND city LIKE '%$GetLocationInput%'");
        
        
        $GetLocation = "";
        $GetResult = "";
    
        foreach($GetLocationByKeyPress as $Location){
            // $GetLocation = $Location->city;
            $GetResult = "<p><input type='checkbox' class='CheckManageFrehserJobsFilters'  value=".$Location->city." name='Location' id=".$Location->id."><label for=".$Location->id." class='CheckBoxFilter'>".$Location->city."</label></p>";
        
        }
        if($GetResult){
            echo $GetResult;
        }else{
            echo json_encode("No Results Found...");
        }
        
    
    }


    public function FilterManageCAJobs(Request $request){
        // $GetDatePosted = $request->date_posted;
        // $GetEmployerId = Session::get("EmployerId");

        $GetLocation = $request->location;
        $GetStipend = $request->Stipend;
        
        

        $GetDataByDatePosted[] =DB::table('jobs')
            ->select('employer.profile_pic', 'jobs.slug', 'jobs.id',  'employer.company_name', 'employer.country', 'employer.city', 'jobs.stipend', 'jobs.job_description', 'jobs.stipend_type', 'jobs.salary_min', 'jobs.salary_max',  'jobs.created_at', 'employer.slug AS CompanySlug')

            ->join('employer', 'jobs.employer_id', '=', 'employer.id')
            ->where('jobs.employee_type', 3)
            ->where('jobs.status', 0)

            // ->whereIn('job_type', $request->categories)
            ->when($GetLocation, function ($query) use ($GetLocation) {
                return $query->whereIn('jobs.city', $GetLocation);
            })
                    // ->WhereIn('job_role', $request->role)
            ->when($GetStipend, function ($query) use ($GetStipend) {
                return $query->whereIn('stipend_type', $GetStipend);
            })
    
            ->get();

        $GetPostedOn = "";
        $GetTrainingResults = "";

        $GetTrainingDuration = "";

        $GetStipend = "";

        foreach($GetDataByDatePosted as $GetData){
            foreach($GetData as $Jobs){
                $now = new DateTime;
            $full = false;	
            $ago = new DateTime($Jobs->created_at);
            $diff = $now->diff($ago);

            $diff->w = floor($diff->d / 7);
            $diff->d -= $diff->w * 7;

            $string = array(
            'y' => 'year',
            'm' => 'month',
            'w' => 'week',
            'd' => 'day',
            'h' => 'hour',
            'i' => 'minute',
            's' => 'second',
            );

            foreach ($string as $k => &$v) {
            if ($diff->$k) {
            $v = $diff->$k . ' ' . $v . ($diff->$k > 1 ? 's' : '');
            } else {
            unset($string[$k]);
            }
            }

            if (!$full) $string = array_slice($string, 0, 1);
            $GetPostedOn =  $string ? implode(', ', $string) . ' ago' : 'just now';


            if($Jobs->stipend_type == 1){
                $num = $Jobs->stipend;
                $units = ['', 'K', 'M'];
                for($i = 0; $num>=1000;$i++){
                    $num /= 1000;
                }
                
                $GetStipend = round($num, 1).$units[$i];

            }else
            {
                $GetStipend = $Jobs->salary_min."-".$Jobs->salary_max;
            }


                

                $GetJobsResults = "<div class='job-listings-sec no-border'>
                <div class='job-listing wtabs pad-top0'>
                    <div class='job-title-sec width100'>
                    <h3 class='manage-jobs-list pad-bottom10'><a href='/CAJobDetails/".$Jobs->CompanySlug."/".$Jobs->id."/".$Jobs->slug."' title=''>CA Article</a></h3>
                    <p class='recommended-jobs-company-name'>".$Jobs->company_name."</p>
                        
                        <div class='job-lctn'><i class='fa fa-money'></i> 
                           ".$GetStipend." / Month
                        </div>
                        <p class='mar-bottom0'>".$Jobs->job_description."</p><br>
                        <div class='row'>
                            <div class='col-md-4'>
                            <button type='button' onclick='javascript:window.open('/ViewAllAppliedCACandidates/".$Jobs->id."/".$Jobs->slug."', '_blank');' formtarget='_blank'  class='view-details-jobs'> <span class='view-details-dot'>...</span> View Details</button>
                            </div>
                            
                            <div class='col-md-3'>
                                <button type='button' onclick='EditCAArticle(".$Jobs->id.")' class='view-details-jobs'> <i class='fa fa-pencil'></i> Edit</button>
                            </div>
                            <div class='col-md-2'>
                                <h3 class='text-left manage-jobs-status'>Published</h3>
                            </div>
                            
                        </div>
                    </div>
                </div>
            </div>";

                if($GetJobsResults){
                    echo $GetJobsResults;
                }else{
                    echo json_encode("No Results Found...");
                }
            }
        }
    }


    public function FilterCloseCAJobs(Request $request){
        // $GetDatePosted = $request->date_posted;
        // $GetEmployerId = Session::get("EmployerId");

        $GetLocation = $request->location;
        $GetStipend = $request->Stipend;
        
        

        $GetDataByDatePosted[] =DB::table('jobs')
            ->select('employer.profile_pic', 'jobs.slug', 'jobs.id',  'employer.company_name', 'employer.country', 'employer.city', 'jobs.stipend', 'jobs.job_description', 'jobs.stipend_type', 'jobs.salary_min', 'jobs.salary_max',  'jobs.created_at', 'employer.slug AS CompanySlug')

            ->join('employer', 'jobs.employer_id', '=', 'employer.id')
            ->where('jobs.employee_type', 3)
            ->where('jobs.status', 1)

            // ->whereIn('job_type', $request->categories)
            ->when($GetLocation, function ($query) use ($GetLocation) {
                return $query->whereIn('jobs.city', $GetLocation);
            })
                    // ->WhereIn('job_role', $request->role)
            ->when($GetStipend, function ($query) use ($GetStipend) {
                return $query->whereIn('stipend_type', $GetStipend);
            })
    
            ->get();

            $GetPostedOn = "";
            $GetTrainingResults = "";
    
            $GetTrainingDuration = "";
    
            $GetStipend = "";
    
            foreach($GetDataByDatePosted as $GetData){
                foreach($GetData as $Jobs){
                    $now = new DateTime;
                $full = false;	
                $ago = new DateTime($Jobs->created_at);
                $diff = $now->diff($ago);
    
                $diff->w = floor($diff->d / 7);
                $diff->d -= $diff->w * 7;
    
                $string = array(
                'y' => 'year',
                'm' => 'month',
                'w' => 'week',
                'd' => 'day',
                'h' => 'hour',
                'i' => 'minute',
                's' => 'second',
                );
    
                foreach ($string as $k => &$v) {
                if ($diff->$k) {
                $v = $diff->$k . ' ' . $v . ($diff->$k > 1 ? 's' : '');
                } else {
                unset($string[$k]);
                }
                }
    
                if (!$full) $string = array_slice($string, 0, 1);
                $GetPostedOn =  $string ? implode(', ', $string) . ' ago' : 'just now';
    
    
                if($Jobs->stipend_type == 1){
                    $num = $Jobs->stipend;
                    $units = ['', 'K', 'M'];
                    for($i = 0; $num>=1000;$i++){
                        $num /= 1000;
                    }
                    
                    $GetStipend = round($num, 1).$units[$i];
    
                }else
                {
                    $GetStipend = $Jobs->salary_min."-".$Jobs->salary_max;
                }
    
    
                    
    
                    $GetJobsResults = "<div class='job-listings-sec no-border'>
                    <div class='job-listing wtabs pad-top0'>
                        <div class='job-title-sec width100'>
                        <h3 class='manage-jobs-list pad-bottom10'><a href='/CAJobDetails/".$Jobs->CompanySlug."/".$Jobs->id."/".$Jobs->slug."' title=''>CA Article</a></h3>
                        <p class='recommended-jobs-company-name'>".$Jobs->company_name."</p>
                            
                            <div class='job-lctn'><i class='fa fa-money'></i> 
                               ".$GetStipend." / Month
                            </div>
                            <p class='mar-bottom0'>".$Jobs->job_description."</p><br>
                            <div class='row'>
                                <div class='col-md-4'>
                                <button type='button' onclick='javascript:window.open('/ViewAllAppliedCACandidates/".$Jobs->id."/".$Jobs->slug."', '_blank');' formtarget='_blank'  class='view-details-jobs'> <span class='view-details-dot'>...</span> View Details</button>
                                </div>
                                
                                <div class='col-md-3'>
                                    <button type='button' onclick='EditCAArticle(".$Jobs->id.")' class='view-details-jobs'> <i class='fa fa-pencil'></i> Edit</button>
                                </div>
                                <div class='col-md-2'>
                                    <h3 class='text-left manage-jobs-status'>Published</h3>
                                </div>
                                
                            </div>
                        </div>
                    </div>
                </div>";
    
                    if($GetJobsResults){
                        echo $GetJobsResults;
                    }else{
                        echo json_encode("No Results Found...");
                    }
            }
        }
    }


    public function ClearAllCAJobs(Request $request){
        $GetDataByDatePosted = [];

        $GetDataByDatePosted[] =DB::table('jobs')
            ->select('employer.profile_pic', 'jobs.slug', 'jobs.id',  'employer.company_name', 'employer.country', 'employer.city', 'jobs.stipend', 'jobs.job_description', 'jobs.stipend_type', 'jobs.salary_min', 'jobs.salary_max',  'jobs.created_at', 'employer.slug AS CompanySlug')

            ->join('employer', 'jobs.employer_id', '=', 'employer.id')
            ->where('jobs.employee_type', 3)
            ->join('jobs.status', 0)

            ->orderBy('jobs.created_at', 'DESC')

            ->get();


            $GetPostedOn = "";
            $GetTrainingResults = "";
    
            $GetTrainingDuration = "";
    
            $GetStipend = "";
    
            foreach($GetDataByDatePosted as $GetData){
                foreach($GetData as $Jobs){
                    $now = new DateTime;
                $full = false;	
                $ago = new DateTime($Jobs->created_at);
                $diff = $now->diff($ago);
    
                $diff->w = floor($diff->d / 7);
                $diff->d -= $diff->w * 7;
    
                $string = array(
                'y' => 'year',
                'm' => 'month',
                'w' => 'week',
                'd' => 'day',
                'h' => 'hour',
                'i' => 'minute',
                's' => 'second',
                );
    
                foreach ($string as $k => &$v) {
                if ($diff->$k) {
                $v = $diff->$k . ' ' . $v . ($diff->$k > 1 ? 's' : '');
                } else {
                unset($string[$k]);
                }
                }
    
                if (!$full) $string = array_slice($string, 0, 1);
                $GetPostedOn =  $string ? implode(', ', $string) . ' ago' : 'just now';
    
    
                if($Jobs->stipend_type == 1){
                    $num = $Jobs->stipend;
                    $units = ['', 'K', 'M'];
                    for($i = 0; $num>=1000;$i++){
                        $num /= 1000;
                    }
                    
                    $GetStipend = round($num, 1).$units[$i];
    
                }else
                {
                    $GetStipend = $Jobs->salary_min."-".$Jobs->salary_max;
                }
    
    
                    
    
                    $GetJobsResults = "<div class='job-listings-sec no-border'>
                    <div class='job-listing wtabs pad-top0'>
                        <div class='job-title-sec width100'>
                        <h3 class='manage-jobs-list pad-bottom10'><a href='/CAJobDetails/".$Jobs->CompanySlug."/".$Jobs->id."/".$Jobs->slug."' title=''>CA Article</a></h3>
                        <p class='recommended-jobs-company-name'>".$Jobs->company_name."</p>
                            
                            <div class='job-lctn'><i class='fa fa-money'></i> 
                               ".$GetStipend." / Month
                            </div>
                            <p class='mar-bottom0'>".$Jobs->job_description."</p><br>
                            <div class='row'>
                                <div class='col-md-4'>
                                <button type='button' onclick='javascript:window.open('/ViewAllAppliedCACandidates/".$Jobs->id."/".$Jobs->slug."', '_blank');' formtarget='_blank'  class='view-details-jobs'> <span class='view-details-dot'>...</span> View Details</button>
                                </div>
                                
                                <div class='col-md-3'>
                                    <button type='button' onclick='EditCAArticle(".$Jobs->id.")' class='view-details-jobs'> <i class='fa fa-pencil'></i> Edit</button>
                                </div>
                                <div class='col-md-2'>
                                    <h3 class='text-left manage-jobs-status'>Published</h3>
                                </div>
                                
                            </div>
                        </div>
                    </div>
                </div>";
    
                    if($GetJobsResults){
                        echo $GetJobsResults;
                    }else{
                        echo json_encode("No Results Found...");
                    }
            }
        }

    }


    public function ClearAllClosedCAJobs(Request $request){
        $GetDataByDatePosted = [];

        $GetDataByDatePosted[] =DB::table('jobs')
            ->select('employer.profile_pic', 'jobs.slug', 'jobs.id',  'employer.company_name', 'employer.country', 'employer.city', 'jobs.stipend', 'jobs.job_description', 'jobs.stipend_type', 'jobs.salary_min', 'jobs.salary_max',  'jobs.created_at', 'employer.slug AS CompanySlug')

            ->join('employer', 'jobs.employer_id', '=', 'employer.id')
            ->where('jobs.employee_type', 3)
            ->where('jobs.status', 1)

            ->orderBy('jobs.created_at', 'DESC')

            ->get();


            $GetPostedOn = "";
            $GetTrainingResults = "";
    
            $GetTrainingDuration = "";
    
            $GetStipend = "";
    
            foreach($GetDataByDatePosted as $GetData){
                foreach($GetData as $Jobs){
                    $now = new DateTime;
                $full = false;	
                $ago = new DateTime($Jobs->created_at);
                $diff = $now->diff($ago);
    
                $diff->w = floor($diff->d / 7);
                $diff->d -= $diff->w * 7;
    
                $string = array(
                'y' => 'year',
                'm' => 'month',
                'w' => 'week',
                'd' => 'day',
                'h' => 'hour',
                'i' => 'minute',
                's' => 'second',
                );
    
                foreach ($string as $k => &$v) {
                if ($diff->$k) {
                $v = $diff->$k . ' ' . $v . ($diff->$k > 1 ? 's' : '');
                } else {
                unset($string[$k]);
                }
                }
    
                if (!$full) $string = array_slice($string, 0, 1);
                $GetPostedOn =  $string ? implode(', ', $string) . ' ago' : 'just now';
    
    
                if($Jobs->stipend_type == 1){
                    $num = $Jobs->stipend;
                    $units = ['', 'K', 'M'];
                    for($i = 0; $num>=1000;$i++){
                        $num /= 1000;
                    }
                    
                    $GetStipend = round($num, 1).$units[$i];
    
                }else
                {
                    $GetStipend = $Jobs->salary_min."-".$Jobs->salary_max;
                }
    
    
                    
    
                    $GetJobsResults = "<div class='job-listings-sec no-border'>
                    <div class='job-listing wtabs pad-top0'>
                        <div class='job-title-sec width100'>
                        <h3 class='manage-jobs-list pad-bottom10'><a href='/CAJobDetails/".$Jobs->CompanySlug."/".$Jobs->id."/".$Jobs->slug."' title=''>CA Article</a></h3>
                        <p class='recommended-jobs-company-name'>".$Jobs->company_name."</p>
                            
                            <div class='job-lctn'><i class='fa fa-money'></i> 
                               ".$GetStipend." / Month
                            </div>
                            <p class='mar-bottom0'>".$Jobs->job_description."</p><br>
                            <div class='row'>
                                <div class='col-md-4'>
                                <button type='button' onclick='javascript:window.open('/ViewAllAppliedCACandidates/".$Jobs->id."/".$Jobs->slug."', '_blank');' formtarget='_blank'  class='view-details-jobs'> <span class='view-details-dot'>...</span> View Details</button>
                                </div>
                                
                                <div class='col-md-3'>
                                    <button type='button' onclick='EditCAArticle(".$Jobs->id.")' class='view-details-jobs'> <i class='fa fa-pencil'></i> Edit</button>
                                </div>
                                <div class='col-md-2'>
                                    <h3 class='text-left manage-jobs-status'>Published</h3>
                                </div>
                                
                            </div>
                        </div>
                    </div>
                </div>";
    
                    if($GetJobsResults){
                        echo $GetJobsResults;
                    }else{
                        echo json_encode("No Results Found...");
                    }
            }
        }

    }


    public function FresherCAJobs(){
        $title = "Fresher Jobs";
        $GetEmail = Session::get('CaArticleEmail');
        $GetArticleId = Session::get('CaArticleId');
        $ArticleProfile = CandidateProfileModel::where("candidate_id", $GetArticleId)->first();
        if($ArticleProfile){
            $GetCandidateId = Session::get('TraineeId');
        }else{
            $GetCandidateId = "0";
        }
        // echo $GetCandidateId;
        // exit;
        $Profile = CandidateModel::where("id", $GetArticleId)->first();
        
        $GetFresherJobs = DB::select("SELECT DISTINCT jobs.id, job_title.name AS title, employer.website, employer.mobile, employer.email, employer.profile_pic, jobs.total_positions, employer.company_name, jobs.city , jobs.salary_min, jobs.salary_max, jobs.job_type, jobs.created_at, jobs.job_description, jobs.experience, jobs.qualification, jobs.slug, employer.slug AS CompanySlug FROM jobs, employer, job_title WHERE jobs.employer_id = employer.id AND job_title.id = jobs.title AND jobs.employee_type = 1");

        $GetStipend = TrainingCompletionModel::get();

        // $GetFavouriteJobs = DB::select("SELECT employer.profile_pic, jobs.title, employer.slug AS CompanySlug, jobs.slug, employer.company_name FROM favourite_jobs, jobs, employer WHERE favourite_jobs.candidate_id = $GetCandidateId AND favourite_jobs.jobs_id = jobs.id AND jobs.employer_id = employer.id AND favourite_jobs.status = 1 AND favourite_jobs.type = 1");

        return view("UI.layouts.ca_fresher_jobs", compact('title', "Profile", 'ArticleProfile', 'GetFavouriteJobs', 'GetFresherJobs', 'GetStipend'));
    }


    public function GetAppliedCAJobs(){
        $title = "Applied Jobs";
        $GetCandidateId = Session::get('CaArticleId');

        $GetEmail = Session::get('CaArticleEmail');

        $ArticleProfile = CandidateProfileModel::where("candidate_id", $GetCandidateId)->first();
        $Profile = CandidateModel::where("email", $GetEmail)->first();

        $GetAppliedJobs = DB::select("SELECT DISTINCT jobs.title, jobs.experience, jobs.job_type, employer.company_name, jobs.location, jobs.city, jobs_applied.created_at, jobs_applied.recruiter_status, employer.slug AS CompanySlug, jobs.slug, jobs_applied.download_cv_status, jobs_applied.application_view_date , jobs_applied.download_cv_date, jobs.stipend_type, jobs.stipend, jobs.salary_min, jobs.salary_max FROM jobs_applied, jobs, employer, job_title WHERE jobs_applied.jobs_id = jobs.id AND jobs.employer_id = employer.id AND jobs_applied.candidate_id = $GetCandidateId");

        $GetRecruiterViewed = JobsApplied::where('candidate_id', $GetCandidateId)
                                        ->where('recruiter_status', 1)
                                        ->count();

        $GetApplicationDownload = JobsApplied::where('candidate_id', $GetCandidateId)
                                        ->where('download_cv_status', 1)
                                        ->count();

        $GetFavouriteJobs = DB::select("SELECT employer.profile_pic, jobs.title, employer.slug AS CompanySlug, jobs.slug, employer.company_name FROM favourite_jobs, jobs, employer WHERE favourite_jobs.candidate_id = $GetCandidateId AND favourite_jobs.jobs_id = jobs.id AND jobs.employer_id = employer.id AND favourite_jobs.status = 1 AND favourite_jobs.type = 1");

        $GetProfessionalDetails = DB::table('candidate')
                                ->select('resume.name', 'job_title.name AS JobTitle')
                                ->join('resume', 'candidate.id', '=', 'resume.candidate_id')
                                ->join('job_title', 'job_title.id', '=', 'resume.department')
                                ->where('resume.worked_till', '=', 1)
                                ->first();
        // echo json_encode($GetAppliedJobs);p
        // exit;
        return view("UI.layouts.candidates_applied_ca_jobs", compact('title', 'GetAppliedJobs', 'ArticleProfile', 'GetFavouriteJobs', 'GetRecruiterViewed', 'GetApplicationDownload', 'GetProfessionalDetails', 'Profile'));
    }


    public function CAJobDetails($company_name, $JobId, $slug){
        $title = "Jobs Details";
        $GetCandidateId = Session::get('CaArticleId');
        $GetEmail = Session::get('CaArticleEmail');

        $GetEmployerId = Session::get("EmployerId");
        // $GetEmployerId = 1;
        $EmployerProfile = EmployerModel::where("id", $GetEmployerId)->first();

        $ArticleProfile = CandidateProfileModel::where("candidate_id", $GetCandidateId)->first();
    
        $CheckPercentage = DB::table('candidate_profile')
                            ->where('candidate_id', $GetCandidateId)
                            ->where('candidate_profile.profile_completion', '>=', '90')
                            ->first();
        // echo json_encode($CheckPercentage);
        // exit;

        $GetJobsId = JobsModel::where('id', $JobId)->first();
        // $GetJobsId = DB::table('jobs')
        //             ->select('jobs.*', 'job_title.*')
        //             ->join('job_title', 'job_title.id', '=', 'jobs.title')
        //             ->where('jobs.id', $JobId)
        //             ->first();

        // echo json_encode($GetJobsId);
        // exit;
        $GetIndustrySegment = request()->segment(4);

        

        $Profile = CandidateModel::where("email", $GetEmail)->first();

        $GetRoles = JobsModel::where('id', $JobId)->get();

        $GetJobTitle = JobTitleModel::get();
        $GetQualificationAdmin = QualificationModel::get();
        $GetTrainingTitle = TrainingTitleModel::get();
        $GetIndustries = IndustriesModel::get();

        if(Auth::guard('candidate')->check()){
            $CheckTotalViews = DB::select("SELECT * FROM total_views WHERE total_views.candidate_id = '$GetCandidateId' AND total_views.jobs_id = $JobId");
            if($CheckTotalViews != null){
                $GetJobsBySlug = DB::select("SELECT jobs.id, job_title.name AS title, employer.website, employer.mobile, employer.email, employer.profile_pic, jobs.total_positions, employer.company_name, jobs.city , jobs.salary_min, jobs.salary_max, jobs.job_type, jobs.created_at, jobs.job_description,  jobs.industry, jobs.experience, jobs.qualification, employer.slug AS CompanySlug FROM jobs, employer, job_title WHERE jobs.employer_id = employer.id AND job_title.id = jobs.title AND jobs.id = '$JobId'");

                

                $GetTotalViewsCount = TotalViews::where('jobs_id', $JobId)
                                            ->get();

                $CheckJobsApplied = JobsApplied::where('candidate_id', $GetCandidateId)
                                                ->where('jobs_id', $JobId)
                                                ->first();

                $CheckApplicantsCount = JobsApplied::where('jobs_id', $JobId)
                                                    ->get();

                $CheckFavourites = FavouriteModel::where('candidate_id', $GetCandidateId)
                ->where('jobs_id', $JobId)
                ->where('type', 1)
                ->first();

                $GetSimilarJobs = DB::select("SELECT jobs.id, job_title.name AS title, employer.website, employer.mobile, employer.email, employer.profile_pic, jobs.total_positions, employer.company_name, jobs.city , jobs.salary_min, jobs.salary_max, jobs.job_type, jobs.created_at, jobs.job_description,  jobs.industry, jobs.experience,  jobs.qualification,jobs.slug, jobs.slug, employer.slug AS CompanySlug FROM jobs, employer, job_title WHERE jobs.industry = '$GetJobsId->industry' AND jobs.slug != '$GetIndustrySegment' AND job_title.id = jobs.title AND jobs.employer_id = employer.id");

                $GetFavouriteJobs = DB::select("SELECT employer.profile_pic, jobs.title, employer.slug AS CompanySlug, jobs.slug, employer.company_name FROM favourite_jobs, jobs, employer WHERE favourite_jobs.candidate_id = $GetCandidateId AND favourite_jobs.jobs_id = jobs.id AND jobs.employer_id = employer.id AND favourite_jobs.status = 1 AND favourite_jobs.type = 1");
                // echo json_encode($GetSimilarJobs);
                // exit;    
                $GetProfessionalDetails = DB::table('candidate')
                                ->select('resume.name', 'job_title.name AS JobTitle')
                                ->join('resume', 'candidate.id', '=', 'resume.candidate_id')
                                ->join('job_title', 'job_title.id', '=', 'resume.department')
                                ->where('resume.worked_till', '=', 1)
                                ->first();

                $GetRole = "";

                foreach($GetRoles as $key => $value){
                    $GetRole = explode(',', $value->role);
                }

                $GetQualification = "";

                foreach($GetRoles as $key => $value){
                    $GetQualification = explode(',', $value->qualification);
                }

                // echo json_encode($GetJobsBySlug);
                // exit;

                return view("UI.layouts.job_details", compact('title', 'GetJobsBySlug', 'GetTotalViewsCount', 'CheckJobsApplied', 'CheckApplicantsCount', 'CheckFavourites', 'GetSimilarJobs', 'CandidateProfile', 'GetFavouriteJobs', 'GetRole', 'GetQualification', 'GetProfessionalDetails', 'Profile', 'EmployerProfile', 'GetJobTitle', 'GetQualificationAdmin', 'GetTrainingTitle', 'GetIndustries', 'CheckPercentage'));
            }else{
                $TotalViews = new TotalViews();
                $TotalViews->candidate_id = $GetCandidateId;
                $TotalViews->jobs_id = $JobId;
                $TotalViews->total_views = 1;
                $TotalViews->views_type = 1;

                $TotalViews->save();
            }
        }
        
        
        $GetJobsBySlug = DB::select("SELECT jobs.id, jobs.stipend_type,  employer.website, employer.mobile, employer.email, employer.profile_pic, jobs.total_positions, employer.company_name, jobs.city , jobs.salary_min, jobs.salary_max, jobs.job_type, jobs.created_at, jobs.job_description,  jobs.industry, jobs.experience,jobs.qualification, employer.slug AS CompanySlug FROM jobs, employer WHERE jobs.employer_id = employer.id AND jobs.id = '$JobId'");

        // echo json_encode($GetJobsBySlug);
        // exit;

        $GetTotalViewsCount = TotalViews::where('jobs_id', $JobId)
                                        ->get();

        $CheckJobsApplied = JobsApplied::where('candidate_id', $GetCandidateId)
                                        ->where('jobs_id', $JobId)
                                        ->first();

        $CheckApplicantsCount = JobsApplied::where('jobs_id', $JobId)
                                            ->get();

        $CheckFavourites = FavouriteModel::where('candidate_id', $GetCandidateId)
                                        ->where('jobs_id', $JobId)
                                        ->where('type', 1)
                                        ->first();
        
        $GetSimilarJobs = DB::select("SELECT jobs.id, job_title.name AS title, employer.website, employer.mobile, employer.email, employer.profile_pic, jobs.total_positions, employer.company_name, jobs.city , jobs.salary_min, jobs.salary_max, jobs.job_type, jobs.created_at, jobs.job_description,  jobs.industry, jobs.experience,  jobs.qualification,jobs.slug, jobs.slug, employer.slug AS CompanySlug FROM jobs, employer, job_title WHERE jobs.industry = '$GetJobsId->industry' AND jobs.slug != '$GetIndustrySegment' AND job_title.id = jobs.title AND jobs.employer_id = employer.id");

        if(Auth::guard('candidate')->check()){
            $GetFavouriteJobs = DB::select("SELECT employer.profile_pic, jobs.title, employer.slug AS CompanySlug, jobs.slug, employer.company_name FROM favourite_jobs, jobs, employer WHERE favourite_jobs.candidate_id = $GetCandidateId AND favourite_jobs.jobs_id = jobs.id AND jobs.employer_id = employer.id AND favourite_jobs.status = 1 AND favourite_jobs.type = 1");
        }
        // echo json_encode($GetSimilarJobs);
        // exit;
        // $GetRole = "";

        // foreach($GetRoles as $key => $value){
        //     $GetRole = explode(',', $value->role);
        // }

        $GetProfessionalDetails = DB::table('candidate')
                                ->select('resume.name', 'job_title.name AS JobTitle')
                                ->join('resume', 'candidate.id', '=', 'resume.candidate_id')
                                ->join('job_title', 'job_title.id', '=', 'resume.department')
                                ->where('resume.worked_till', '=', 1)
                                ->first();
        
        return view("UI.layouts.ca_job_details", compact('title', 'GetJobsBySlug', 'GetTotalViewsCount', 'CheckJobsApplied', 'CheckApplicantsCount', 'CheckFavourites', 'GetSimilarJobs', 'ArticleProfile', 'GetFavouriteJobs', 'GetRole', 'GetQualification', 'GetProfessionalDetails', 'Profile', 'EmployerProfile', 'GetJobTitle', 'GetQualificationAdmin', 'GetTrainingTitle', 'GetIndustries', 'CheckPercentage'));
    }



    public function FresherJobDetails($company_name, $JobId, $slug){
        $title = "Jobs Details";

        if(Auth::guard('ca_article')->check()){
            $GetCandidateId = Session::get('CaArticleId');
            $GetEmail = Session::get('CaArticleEmail');

            $GetEmployerId = Session::get("EmployerId");
            // $GetEmployerId = 1;
            $EmployerProfile = EmployerModel::where("id", $GetEmployerId)->first();

            $ArticleProfile = CandidateProfileModel::where("candidate_id", $GetCandidateId)->first();
        }elseif(Auth::guard('trainee')->check()){
            $GetCandidateId = Session::get('TraineeId');
            $GetEmail = Session::get('TraineeEmail');

            $GetEmployerId = Session::get("EmployerId");
            // $GetEmployerId = 1;
            $EmployerProfile = EmployerModel::where("id", $GetEmployerId)->first();

            $TraineeProfile = CandidateProfileModel::where("candidate_id", $GetCandidateId)->first();
        }
        
    
        $CheckPercentage = DB::table('candidate_profile')
                            ->where('candidate_id', $GetCandidateId)
                            ->where('candidate_profile.profile_completion', '>=', '90')
                            ->first();
        // echo json_encode($CheckPercentage);
        // exit;

        $GetJobsId = JobsModel::where('id', $JobId)->first();
        // $GetJobsId = DB::table('jobs')
        //             ->select('jobs.*', 'job_title.*')
        //             ->join('job_title', 'job_title.id', '=', 'jobs.title')
        //             ->where('jobs.id', $JobId)
        //             ->first();

        // echo json_encode($GetJobsId);
        // exit;
        $GetIndustrySegment = request()->segment(4);

        

        $Profile = CandidateModel::where("email", $GetEmail)->first();

        $GetRoles = JobsModel::where('id', $JobId)->get();

        $GetJobTitle = JobTitleModel::get();
        $GetQualificationAdmin = QualificationModel::get();
        $GetTrainingTitle = TrainingTitleModel::get();
        $GetIndustries = IndustriesModel::get();

        if(Auth::guard('candidate')->check()){
            $CheckTotalViews = DB::select("SELECT * FROM total_views WHERE total_views.candidate_id = '$GetCandidateId' AND total_views.jobs_id = $JobId");
            if($CheckTotalViews != null){
                $GetJobsBySlug = DB::select("SELECT jobs.id, job_title.name AS title, employer.website, employer.mobile, employer.email, employer.profile_pic, jobs.total_positions, employer.company_name, jobs.city , jobs.salary_min, jobs.salary_max, jobs.job_type, jobs.created_at, jobs.job_description,  jobs.industry, jobs.experience, jobs.qualification, employer.slug AS CompanySlug FROM jobs, employer, job_title WHERE jobs.employer_id = employer.id AND job_title.id = jobs.title AND jobs.id = '$JobId'");

                

                $GetTotalViewsCount = TotalViews::where('jobs_id', $JobId)
                                            ->get();

                $CheckJobsApplied = JobsApplied::where('candidate_id', $GetCandidateId)
                                                ->where('jobs_id', $JobId)
                                                ->first();

                $CheckApplicantsCount = JobsApplied::where('jobs_id', $JobId)
                                                    ->get();

                $CheckFavourites = FavouriteModel::where('candidate_id', $GetCandidateId)
                ->where('jobs_id', $JobId)
                ->where('type', 1)
                ->first();

                $GetSimilarJobs = DB::select("SELECT jobs.id, job_title.name AS title, employer.website, employer.mobile, employer.email, employer.profile_pic, jobs.total_positions, employer.company_name, jobs.city , jobs.salary_min, jobs.salary_max, jobs.job_type, jobs.created_at, jobs.job_description,  jobs.industry, jobs.experience,  jobs.qualification,jobs.slug, jobs.slug, employer.slug AS CompanySlug FROM jobs, employer, job_title WHERE jobs.industry = '$GetJobsId->industry' AND jobs.slug != '$GetIndustrySegment' AND job_title.id = jobs.title AND jobs.employer_id = employer.id");

                $GetFavouriteJobs = DB::select("SELECT employer.profile_pic, jobs.title, employer.slug AS CompanySlug, jobs.slug, employer.company_name FROM favourite_jobs, jobs, employer WHERE favourite_jobs.candidate_id = $GetCandidateId AND favourite_jobs.jobs_id = jobs.id AND jobs.employer_id = employer.id AND favourite_jobs.status = 1 AND favourite_jobs.type = 1");
                // echo json_encode($GetSimilarJobs);
                // exit;    
                $GetProfessionalDetails = DB::table('candidate')
                                ->select('resume.name', 'job_title.name AS JobTitle')
                                ->join('resume', 'candidate.id', '=', 'resume.candidate_id')
                                ->join('job_title', 'job_title.id', '=', 'resume.department')
                                ->where('resume.worked_till', '=', 1)
                                ->first();

                $GetRole = "";

                foreach($GetRoles as $key => $value){
                    $GetRole = explode(',', $value->role);
                }

                $GetQualification = "";

                foreach($GetRoles as $key => $value){
                    $GetQualification = explode(',', $value->qualification);
                }

                // echo json_encode($GetJobsBySlug);
                // exit;

                return view("UI.layouts.job_details", compact('title', 'GetJobsBySlug', 'GetTotalViewsCount', 'CheckJobsApplied', 'CheckApplicantsCount', 'CheckFavourites', 'GetSimilarJobs', 'CandidateProfile', 'GetFavouriteJobs', 'GetRole', 'GetQualification', 'GetProfessionalDetails', 'Profile', 'EmployerProfile', 'GetJobTitle', 'GetQualificationAdmin', 'GetTrainingTitle', 'GetIndustries', 'CheckPercentage'));
            }else{
                $TotalViews = new TotalViews();
                $TotalViews->candidate_id = $GetCandidateId;
                $TotalViews->jobs_id = $JobId;
                $TotalViews->total_views = 1;
                $TotalViews->views_type = 1;

                $TotalViews->save();
            }
        }
        
        
        $GetJobsBySlug = DB::select("SELECT jobs.id, job_title.name AS title, employer.website, employer.mobile, employer.email, employer.profile_pic, jobs.total_positions, employer.company_name, jobs.city , jobs.salary_min, jobs.salary_max, jobs.job_type, jobs.created_at, jobs.job_description,  jobs.industry, jobs.experience,jobs.qualification, employer.slug AS CompanySlug FROM jobs, employer, job_title WHERE jobs.employer_id = employer.id AND job_title.id = jobs.title AND jobs.id = '$JobId'");

        // echo json_encode($GetJobsBySlug);
        // exit;

        $GetTotalViewsCount = TotalViews::where('jobs_id', $JobId)
                                        ->get();

        $CheckJobsApplied = JobsApplied::where('candidate_id', $GetCandidateId)
                                        ->where('jobs_id', $JobId)
                                        ->first();

        $CheckApplicantsCount = JobsApplied::where('jobs_id', $JobId)
                                            ->get();

        $CheckFavourites = FavouriteModel::where('candidate_id', $GetCandidateId)
                                        ->where('jobs_id', $JobId)
                                        ->where('type', 1)
                                        ->first();
        
        $GetSimilarJobs = DB::select("SELECT jobs.id, job_title.name AS title, employer.website, employer.mobile, employer.email, employer.profile_pic, jobs.total_positions, employer.company_name, jobs.city , jobs.salary_min, jobs.salary_max, jobs.job_type, jobs.created_at, jobs.job_description,  jobs.industry, jobs.experience,  jobs.qualification,jobs.slug, jobs.slug, employer.slug AS CompanySlug FROM jobs, employer, job_title WHERE jobs.industry = '$GetJobsId->industry' AND jobs.slug != '$GetIndustrySegment' AND job_title.id = jobs.title AND jobs.employer_id = employer.id");

        if(Auth::guard('candidate')->check()){
            $GetFavouriteJobs = DB::select("SELECT employer.profile_pic, jobs.title, employer.slug AS CompanySlug, jobs.slug, employer.company_name FROM favourite_jobs, jobs, employer WHERE favourite_jobs.candidate_id = $GetCandidateId AND favourite_jobs.jobs_id = jobs.id AND jobs.employer_id = employer.id AND favourite_jobs.status = 1 AND favourite_jobs.type = 1");
        }
        // echo json_encode($GetSimilarJobs);
        // exit;
        // $GetRole = "";

        // foreach($GetRoles as $key => $value){
        //     $GetRole = explode(',', $value->role);
        // }

        $GetProfessionalDetails = DB::table('candidate')
                                ->select('resume.name', 'job_title.name AS JobTitle')
                                ->join('resume', 'candidate.id', '=', 'resume.candidate_id')
                                ->join('job_title', 'job_title.id', '=', 'resume.department')
                                ->where('resume.worked_till', '=', 1)
                                ->first();
        
        return view("UI.layouts.fresher_job_details", compact('title', 'GetJobsBySlug', 'GetTotalViewsCount', 'CheckJobsApplied', 'CheckApplicantsCount', 'CheckFavourites', 'GetSimilarJobs', 'ArticleProfile', 'GetFavouriteJobs', 'GetRole', 'GetQualification', 'GetProfessionalDetails', 'Profile', 'EmployerProfile', 'GetJobTitle', 'GetQualificationAdmin', 'GetTrainingTitle', 'GetIndustries', 'CheckPercentage', 'TraineeProfile'));
    }


    public function FilterDatePostedFreshJobs(Request $request){
        $GetDatePosted = $request->date_posted;


        $GetDataByDatePosted = [];

        if($GetDatePosted == 1){
            $GetDataByDatePosted[] = DB::select("SELECT DISTINCT jobs.id, job_title.name AS title, employer.website, employer.mobile, employer.email, employer.profile_pic, jobs.total_positions, employer.company_name, jobs.city , jobs.salary_min, jobs.salary_max, jobs.job_type, jobs.created_at, jobs.job_description, jobs.experience, jobs.qualification, jobs.slug, employer.slug AS CompanySlug FROM jobs, employer, job_title WHERE jobs.employer_id = employer.id AND job_title.id = jobs.title AND jobs.employee_type = 1 AND jobs.created_at >= DATE_SUB(CURDATE(), INTERVAL 1 HOUR)");

        }elseif($GetDatePosted == 2){

            $GetDataByDatePosted[] = DB::select("SELECT DISTINCT jobs.id, job_title.name AS title, employer.website, employer.mobile, employer.email, employer.profile_pic, jobs.total_positions, employer.company_name, jobs.city , jobs.salary_min, jobs.salary_max, jobs.job_type, jobs.created_at, jobs.job_description, jobs.experience, jobs.qualification, jobs.slug, employer.slug AS CompanySlug FROM jobs, employer, job_title WHERE jobs.employer_id = employer.id AND job_title.id = jobs.title AND jobs.employee_type = 1 AND jobs.created_at >= CURRENT_TIMESTAMP - INTERVAL 24 HOUR");
        }elseif($GetDatePosted == 3){

            $GetDataByDatePosted[] = DB::select("SELECT DISTINCT jobs.id, job_title.name AS title, employer.website, employer.mobile, employer.email, employer.profile_pic, jobs.total_positions, employer.company_name, jobs.city , jobs.salary_min, jobs.salary_max, jobs.job_type, jobs.created_at, jobs.job_description, jobs.experience, jobs.qualification, jobs.slug, employer.slug AS CompanySlug FROM jobs, employer, job_title WHERE jobs.employer_id = employer.id AND job_title.id = jobs.title AND jobs.employee_type = 1 AND jobs.created_at >= DATE_ADD(CURDATE(), INTERVAL -7 DAY)");
        }elseif($GetDatePosted == 4){

            $GetDataByDatePosted[] = DB::select("SELECT DISTINCT jobs.id, job_title.name AS title, employer.website, employer.mobile, employer.email, employer.profile_pic, jobs.total_positions, employer.company_name, jobs.city , jobs.salary_min, jobs.salary_max, jobs.job_type, jobs.created_at, jobs.job_description, jobs.experience, jobs.qualification, jobs.slug, employer.slug AS CompanySlug FROM jobs, employer, job_title WHERE jobs.employer_id = employer.id AND job_title.id = jobs.title AND jobs.employee_type = 1 ORDER BY jobs.created_at DESC");

        }

        // echo json_encode($GetDataByDatePosted);
        // exit;

        $GetPostedOn = "";
        $GetTrainingResults = "";

        $GetTrainingDuration = "";

        $GetStipend = "";
        $GetTrainingCompletion = "";

        foreach($GetDataByDatePosted as $GetData){
            foreach($GetData as $Trainings){
                $now = new DateTime;
            $full = false;	
            $ago = new DateTime($Trainings->created_at);
            $diff = $now->diff($ago);

            $diff->w = floor($diff->d / 7);
            $diff->d -= $diff->w * 7;

            $string = array(
            'y' => 'year',
            'm' => 'month',
            'w' => 'week',
            'd' => 'day',
            'h' => 'hour',
            'i' => 'minute',
            's' => 'second',
            );

            foreach ($string as $k => &$v) {
            if ($diff->$k) {
            $v = $diff->$k . ' ' . $v . ($diff->$k > 1 ? 's' : '');
            } else {
            unset($string[$k]);
            }
            }

            if (!$full) $string = array_slice($string, 0, 1);
            $GetPostedOn =  $string ? implode(', ', $string) . ' ago' : 'just now';


            // if($Trainings->duration == 1){
            //     $GetTrainingDuration = "30 Days";
            // }else{
            //     $GetTrainingDuration = "60 Days";
            // }

            // $num = $Trainings->stipend;
            // $units = ['', 'K', 'M'];
            // for($i = 0; $num>=1000;$i++){
            //     $num /= 1000;
            // }
            // $GetStipend = round($num, 1).$units[$i];

            // $GetStipend = $Trainings->training_completion;
                
            
                      
            if($Trainings->job_type == 1){
                $JobType = "Part time";
            }else{
                $JobType = "Full time";
            }


                $GetTrainingResults = "<div class='job-listing wtabs recommented-jobs'>
                <div class='job-title-sec recommended-jobs-pad-left'>
                     <h3><a href='/CAJobDetails/".$Trainings->CompanySlug."/".$Trainings->id."/".$Trainings->slug." target='_blank' title=''>".$Trainings->title."</a></h3>
                    <p class='recommended-jobs-company-name'>".$Trainings->company_name."</p>
                    <div class='job-lctn job-attributes-recommended-jobs'><i class='fa fa-briefcase'></i>".$Trainings->experience." Years</div>
                    <div class='job-lctn job-attributes-recommended-jobs'><i class='fa fa-user-circle'></i>
                         ".$JobType."
                     </div>
                    <div class='job-lctn job-attributes-recommended-jobs'><i class='fa fa-map-marker' ></i> ".$Trainings->city."</div>
                    
                 </div>
             <div class='recommended-jobs-description'>".implode(' ', array_slice(explode(' ', $Trainings->job_description), 0, 20))."</div>
                 <p class='recommended-jobs-posted-on'>Posted on: 
                        ".$GetPostedOn."
                 </p>
            </div>";

                if($GetTrainingResults){
                    echo $GetTrainingResults;
                }else{
                    echo json_encode("No Results Found...");
                }
            }
        }

    }




    // Fresher Jobs

    public function FilterFresherJobs(Request $request){
        $GetDatePosted = $request->date_posted;
        $GetLocation = $request->location;
        // $GetStipend = $request->Stipend;

        $GetDataByDatePosted[] =DB::table('jobs')
            ->select('jobs.id', 'job_title.name AS title', 'employer.website', 'employer.mobile', 'employer.email', 'employer.profile_pic', 'jobs.total_positions', 'employer.company_name', 'jobs.city' , 'jobs.salary_min', 'jobs.salary_max', 'jobs.job_type', 'jobs.created_at', 'jobs.job_description', 'jobs.experience', 'jobs.qualification', 'jobs.slug', 'employer.slug AS CompanySlug')
            ->join('employer', 'employer.id', '=', 'jobs.employer_id')
            ->join('job_title', 'job_title.id', '=', 'jobs.title')
            ->where('jobs.employee_type', 1)
            // ->whereIn('job_type', $request->categories)
            ->when($GetLocation, function ($query) use ($GetLocation) {
                return $query->whereIn('jobs.city', $GetLocation);
            })
    
            ->get();

        $GetPostedOn = "";
        $GetTrainingResults = "";

        $GetTrainingDuration = "";

        $GetStipend = "";
        $GetTrainingCompletion = "";

        foreach($GetDataByDatePosted as $GetData){
            foreach($GetData as $Trainings){
                $now = new DateTime;
            $full = false;	
            $ago = new DateTime($Trainings->created_at);
            $diff = $now->diff($ago);

            $diff->w = floor($diff->d / 7);
            $diff->d -= $diff->w * 7;

            $string = array(
            'y' => 'year',
            'm' => 'month',
            'w' => 'week',
            'd' => 'day',
            'h' => 'hour',
            'i' => 'minute',
            's' => 'second',
            );

            foreach ($string as $k => &$v) {
            if ($diff->$k) {
            $v = $diff->$k . ' ' . $v . ($diff->$k > 1 ? 's' : '');
            } else {
            unset($string[$k]);
            }
            }

            if (!$full) $string = array_slice($string, 0, 1);
            $GetPostedOn =  $string ? implode(', ', $string) . ' ago' : 'just now';


            // if($Trainings->duration == 1){
            //     $GetTrainingDuration = "30 Days";
            // }else{
            //     $GetTrainingDuration = "60 Days";
            // }

            // $num = $Trainings->stipend;
            // $units = ['', 'K', 'M'];
            // for($i = 0; $num>=1000;$i++){
            //     $num /= 1000;
            // }
            // $GetStipend = round($num, 1).$units[$i];

            // $GetStipend = $Trainings->training_completion;
                
            
                        
            if($Trainings->job_type == 1){
                $JobType = "Part time";
            }else{
                $JobType = "Full time";
            }


                $GetTrainingResults = "<div class='job-listing wtabs recommented-jobs'>
                <div class='job-title-sec recommended-jobs-pad-left'>
                        <h3><a href='/CAJobDetails/".$Trainings->CompanySlug."/".$Trainings->id."/".$Trainings->slug." target='_blank' title=''>".$Trainings->title."</a></h3>
                    <p class='recommended-jobs-company-name'>".$Trainings->company_name."</p>
                    <div class='job-lctn job-attributes-recommended-jobs'><i class='fa fa-briefcase'></i>".$Trainings->experience." Years</div>
                    <div class='job-lctn job-attributes-recommended-jobs'><i class='fa fa-user-circle'></i>
                            ".$JobType."
                        </div>
                    <div class='job-lctn job-attributes-recommended-jobs'><i class='fa fa-map-marker' ></i> ".$Trainings->city."</div>
                    
                    </div>
                <div class='recommended-jobs-description'>".implode(' ', array_slice(explode(' ', $Trainings->job_description), 0, 20))."</div>
                    <p class='recommended-jobs-posted-on'>Posted on: 
                        ".$GetPostedOn."
                    </p>
            </div>";

                if($GetTrainingResults){
                    echo $GetTrainingResults;
                }else{
                    echo json_encode("No Results Found...");
                }
            }
        }
    }
}
