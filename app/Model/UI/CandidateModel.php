<?php

namespace App\Model\UI;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;

class CandidateModel extends Authenticatable
{   
    protected $guard = 'candidate';

    protected $table = "candidate";

    protected $fillable = ['name', 'password', 'email', 'mobile', 'gender', 'aadhar_no', 'pan_no', 'mobile_otp_verify', 'email_verify', 'status'];
}
