<?php

namespace App\Model\UI;

use Illuminate\Database\Eloquent\Model;

class CandidateProfileModel extends Model
{
    protected $table = "candidate_profile";

    protected $fillable = ['candidate_id', 'father_name', 'profile_pic', 'job_title', 'allow_search', 'experience', 'minimum_salary', 'gender', 'date_of_birth', 'age', 'address', 'zip_code', 'country', 'state', 'city', 'gross_salary', 'expected_salary', 'education_level', 'languages', 'specialisms', 'description', 'website', 'facebook', 'twitter', 'linkedin', 'googleplus'];
}
