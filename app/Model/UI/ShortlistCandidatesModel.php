<?php

namespace App\Model\UI;

use Illuminate\Database\Eloquent\Model;

class ShortlistCandidatesModel extends Model
{
    protected $table = "shortlist_candidates";

    protected $fillable = ['candidate_id', 'employer_id', 'shortlist_status', 'shortlisted_type'];
}
