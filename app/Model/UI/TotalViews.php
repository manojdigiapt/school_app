<?php

namespace App\Model\UI;

use Illuminate\Database\Eloquent\Model;

class TotalViews extends Model
{
    protected $table = "total_views";

    protected $fillable = ['candidate_id', 'jobs_id', 'training_id', 'views_type'];
}
