<?php

namespace App\Model\UI;

use Illuminate\Database\Eloquent\Model;

class JobsModel extends Model
{
    protected $table = "jobs";

    protected $fillable = ['employer_id', 'title', 'slug', 'location', 'email', 'mobile', 'job_description', 'job_type', 'industry', 'role', 'offered_salary', 'salary_type', 'career_level', 'experience', 'gender', 'qualification', 'job_expires', 'total_positions', 'total_views', 'status'];
}
