<?php

namespace App\Model\UI;

use Illuminate\Database\Eloquent\Model;

class SelectedTraineeModel extends Model
{
    protected $table = "selected_trainee";

    protected $fillable = ['candidate_id', 'employer_id', 'training_id', 'training_start_date', 'training_end_date', 'training_status', 'certificate'];
}
