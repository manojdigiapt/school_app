<?php

namespace App\Model\UI;

use Illuminate\Database\Eloquent\Model;

class TrainingModel extends Model
{
    protected $table = "training";

    protected $fillable = ['employer_id', 'title', 'slug', 'start_date', 'duration', 'stipend', 'last_date_of_application', 'training_completion', 'training_status'];
}
