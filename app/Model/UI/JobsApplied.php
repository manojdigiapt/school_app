<?php

namespace App\Model\UI;

use Illuminate\Database\Eloquent\Model;

class JobsApplied extends Model
{
    protected $table = "jobs_applied";

    protected $fillable = ['candidate_id', 'jobs_id', 'training_id', 'training_status', 'certificate'];
}
