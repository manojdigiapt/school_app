<?php

namespace App\Model\UI;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;

class EmployerModel extends Authenticatable
{
    protected $guard = 'employer';

    protected $table = "employer";

    protected $fillable = ['profile_pic', 'user_name', 'company_name', 'password', 'slug', 'allow_in_search', 'since', 'team_size', 'categories', 'description', 'website', 'email', 'mobile', 'phone', 'address', 'zip_code', 'state', 'city', 'facebook', 'twitter', 'googleplus', 'linkedin', 'otp_verify', 'email_verify', 'status'];
}
