<?php

namespace App\Model\UI;

use Illuminate\Database\Eloquent\Model;

class ReviewsModel extends Model
{
    protected $table = "reviews";

    protected $fillable = ['candidate_id', 'training_id', 'review_stars', 'feedback'];
}
