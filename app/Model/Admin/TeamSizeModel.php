<?php

namespace App\Model\Admin;

use Illuminate\Database\Eloquent\Model;

class TeamSizeModel extends Model
{
    protected $table = 'team_size';

    protected $fillable = ['id', 'team_size', 'status'];
}
