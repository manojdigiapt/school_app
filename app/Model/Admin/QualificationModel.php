<?php

namespace App\Model\Admin;

use Illuminate\Database\Eloquent\Model;

class QualificationModel extends Model
{
    protected $table = 'qualifications';

    protected $fillable = ['id', 'qualification', 'status'];
}
