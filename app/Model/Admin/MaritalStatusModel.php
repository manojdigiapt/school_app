<?php

namespace App\Model\Admin;

use Illuminate\Database\Eloquent\Model;

class MaritalStatusModel extends Model
{
    protected $table = 'marital_status';

    protected $fillable = ['id', 'marital_status', 'status'];
}
