<?php

namespace App\Model\Admin;

use Illuminate\Database\Eloquent\Model;

class TrainingDurationModel extends Model
{
    protected $table = 'training_duration';

    protected $fillable = ['id', 'training_duration', 'status'];
}
