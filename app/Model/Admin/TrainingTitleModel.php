<?php

namespace App\Model\Admin;

use Illuminate\Database\Eloquent\Model;

class TrainingTitleModel extends Model
{
    protected $table = 'training_title';

    protected $fillable = ['id', 'title', 'status'];
}
