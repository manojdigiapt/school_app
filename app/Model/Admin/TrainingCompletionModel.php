<?php

namespace App\Model\Admin;

use Illuminate\Database\Eloquent\Model;

class TrainingCompletionModel extends Model
{
    protected $table = 'training_completion';

    protected $fillable = ['id', 'training_completion', 'status'];
}
