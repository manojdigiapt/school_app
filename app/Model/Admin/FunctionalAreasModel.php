<?php

namespace App\Model\Admin;

use Illuminate\Database\Eloquent\Model;

class FunctionalAreasModel extends Model
{
    protected $table = 'functional_areas';

    protected $fillable = ['id', 'functional_areas', 'status'];
}
