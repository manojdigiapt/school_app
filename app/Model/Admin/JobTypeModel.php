<?php

namespace App\Model\Admin;

use Illuminate\Database\Eloquent\Model;

class JobTypeModel extends Model
{
    protected $table = 'job_types';

    protected $fillable = ['id', 'job_type', 'status'];
}
