$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});


$(document).on("click", "#AddEducation", function(){
    var university = $("#university").val();
    var school_clg = $("#school_clg").val();
    var department = $("#department").val();
    var from_date = $("#from_date").val();
    var to_date = $("#to_date").val();
    
    if(university == ""){
        danger_msg("University should not empty");
        $("#university").focus();
        return false;
        
    }

    if(school_clg == ""){
        danger_msg("School/Clg should not empty");
        $("#school_clg").focus();
        return false;
    }

    if(department == ""){
        danger_msg("Department should not empty");
        $("#department").focus();
        return false;
    }

    if(from_date == ""){
        danger_msg("From date should not empty");
        $("#from_date").focus();
        return false;
    }

    if(to_date == ""){
        danger_msg("To date should not empty");
        $("#to_date").focus();
        return false;
    }

    var EducationData = {
        university: university,
        school_clg: school_clg,
        department: department,
        from_date: from_date,
        to_date: to_date
    }

    $.ajax({
        type: "POST",
        url: "/Candidate/InsertEducation",
        data: EducationData,
        dataType: "JSON",
        success: function (data) {
            if(data.error){
                danger_msg(data.message);
                return false;
            }else{
                success_msg(data.message);
                location.reload();
                // return false;
            }
        }
    });
});

function EditEducation(id){
    $.ajax({
        type: "POST",
        url: "/Candidate/GetEducationById/"+id,
        data: {id: id},
        dataType: "JSON",
        success: function (data) {
            if(data.error){
                danger_msg(data.message);
                return false;
            }else{
                $("#edu_id").val(data.id);

                $("#edit_university").val(data.title);
                $("#edit_school_clg").val(data.name);
                $("#edit_department").val(data.department);
                $("#edu_from_date").val(data.from_date);
                $("#edu_to_date").val(data.to_date);

                $("#edit_education").modal("show");
                return false;
            }
        }
    });
}

$(document).on("click", "#UpdateEducation", function(){
    var id = $("#edu_id").val();
    var university = $("#edit_university").val();
    var school_clg = $("#edit_school_clg").val();
    var department = $("#edit_department").val();
    var from_date = $("#edu_from_date").val();
    var to_date = $("#edu_to_date").val();

    if(university == ""){
        danger_msg("University should not empty");
        $("#edit_university").focus();
        return false;
    }

    if(school_clg == ""){
        danger_msg("School/Clg should not empty");
        $("#edit_school_clg").focus();
        return false;
    }

    if(department == ""){
        danger_msg("Department should not empty");
        $("#edit_department").focus();
        return false;
    }

    if(from_date == ""){
        danger_msg("From date should not empty");
        $("#edu_from_date").focus();
        return false;
    }

    if(to_date == ""){
        danger_msg("To date should not empty");
        $("#edu_to_date").focus();
        return false;
    }

    var UpdateEducationData = {
        id: id,
        university: university,
        school_clg: school_clg,
        department: department,
        from_date: from_date,
        to_date: to_date
    }

    $.ajax({
        type: "POST",
        url: "/Candidate/UpdateEducation",
        data: UpdateEducationData,
        dataType: "JSON",
        success: function (data) {
            if(data.error){
                danger_msg(data.message);
                return false;
            }else{
                success_msg(data.message);
                location.reload();
                // return false;
            }
        }
    });
});

function DeleteEducation(id){
    swal({
        title: "Are you sure?",
        text: "You will not be able to recover this imaginary file!",
        type: "warning",
        showCancelButton: true,
        confirmButtonClass: "btn-danger",
        confirmButtonText: "Yes, delete it!",
        cancelButtonText: "No",
        closeOnConfirm: false,
        closeOnCancel: true
      },
      function(isConfirm) {
        if (isConfirm) {
            $.ajax({
                type: "GET",
                url: "/Candidate/DeleteEducation/"+id,
                dataType: "JSON",
                success: function (data) {
                    if(data.error){
                        danger_msg(data.message);
                        return false;
                    }else{
                        swal("Deleted!", "Data deleted successfully", "success");
                        location.reload();
                        return false;
                    }
                }
            });
        }
      });
}

// Experience

$(document).on("click", "#AddExperience", function(){
    var company_name = $("#company_name").val();
    var designation = $("#designation").val();
    var exp_from_date = $("#exp_from_date").val();
    var exp_to_date = $("#exp_to_date").val();

    if(company_name == ""){
        danger_msg("Company name should not empty");
        $("#company_name").focus();
        return false;
    }

    if(designation == ""){
        danger_msg("Designation name should not empty");
        $("#designation").focus();
        return false;
    }

    if(exp_from_date == ""){
        danger_msg("Experiece from date should not empty");
        $("#exp_from_date").focus();
        return false;
    }

    if(exp_to_date == ""){
        danger_msg("Experiece to date should not empty");
        $("#exp_to_date").focus();
        return false;
    }

    var ExperienceData = {
        company_name: company_name,
        designation: designation,
        exp_from_date: exp_from_date,
        exp_to_date: exp_to_date
    }

    $.ajax({
        type: "POST",
        url: "/Candidate/InsertExperience",
        data: ExperienceData,
        dataType: "JSON",
        success: function (data) {
            if(data.error){
                danger_msg(data.message);
                return false;
            }else{
                success_msg(data.message);
                location.reload();
                // return false;
            }
        }
    });
});

function EditExperience(id){
    $.ajax({
        type: "POST",
        url: "/Candidate/GetExperienceById/"+id,
        data: {id: id},
        dataType: "JSON",
        success: function (data) {
            if(data.error){
                danger_msg(data.message);
                return false;
            }else{
                $("#exp_id").val(data.id);

                $("#edit_company_name").val(data.name);
                $("#edit_designation").val(data.department);
                $("#edit_exp_from_date").val(data.from_date);
                $("#edit_exp_to_date").val(data.to_date);

                $("#edit_experience").modal("show");
                return false;
            }
        }
    });
}

$(document).on("click", "#UpdateExperience", function(){
    var id = $("#exp_id").val();
    var company_name = $("#edit_company_name").val();
    var designation = $("#edit_designation").val();
    var exp_from_date = $("#edit_exp_from_date").val();
    var exp_to_date = $("#edit_exp_to_date").val();

    if(company_name == ""){
        danger_msg("Company name should not empty");
        $("#edit_company_name").focus();
        return false;
    }

    if(designation == ""){
        danger_msg("Designation name should not empty");
        $("#edit_designation").focus();
        return false;
    }

    if(exp_from_date == ""){
        danger_msg("Experiece from date should not empty");
        $("#edit_exp_from_date").focus();
        return false;
    }

    if(exp_to_date == ""){
        danger_msg("Experiece to date should not empty");
        $("#edit_exp_to_date").focus();
        return false;
    }

    var UpdateEducationData = {
        id: id,
        company_name: company_name,
        designation: designation,
        exp_from_date: exp_from_date,
        exp_to_date: exp_to_date
    }

    $.ajax({
        type: "POST",
        url: "/Candidate/UpdateExperience",
        data: UpdateEducationData,
        dataType: "JSON",
        success: function (data) {
            if(data.error){
                danger_msg(data.message);
                return false;
            }else{
                success_msg(data.message);
                location.reload();
                // return false;
            }
        }
    });
});

function DeleteExperience(id){
    swal({
        title: "Are you sure?",
        text: "You will not be able to recover this imaginary file!",
        type: "warning",
        showCancelButton: true,
        confirmButtonClass: "btn-danger",
        confirmButtonText: "Yes, delete it!",
        cancelButtonText: "No",
        closeOnConfirm: false,
        closeOnCancel: true
      },
      function(isConfirm) {
        if (isConfirm) {
            $.ajax({
                type: "GET",
                url: "/Candidate/DeleteExperience/"+id,
                dataType: "JSON",
                success: function (data) {
                    if(data.error){
                        danger_msg(data.message);
                        return false;
                    }else{
                        swal("Deleted!", "Data deleted successfully", "success");
                        location.reload();
                        return false;
                    }
                }
            });
        }
      });
}


// Skills


$(document).on("click", "#AddSkills", function(){
    var SkillId = $("#SkillId").val();
    var skill_name = $("#skill_name").val();
    // var percentage = $("#percentage").val();
    // var skill_name = new Array();
    // $('#skill_name').each(function(){
    //     skill_name.push($(this).val());
    //  });
    //  alert(skill_name);

    //  return false;
    var SkillData = {
        SkillId: SkillId,
        skill_name: skill_name
        
        // percentage: percentage
    }

    $.ajax({
        type: "POST",
        url: "/Candidate/InsertSkills",
        data: SkillData,
        dataType: "JSON",
        success: function (data) {
            if(data.error){
                danger_msg(data.message);
                return false;
            }else{
                success_msg(data.message);
                location.reload();
                return false;
            }
        }
    });
});




// function CheckSkills(){
//     var skill_name = $("#skill_name").val();
//     $.ajax({
//         type: "POST",
//         url: "/Candidate/CheckSkills",
//         data: {skill_name: skill_name},
//         dataType: "JSON",
//         success: function (data) {
//             // console.log(data);
            
//         }
//     });
// }

function BrowseCandidateCV(elemId) {
    var elem = document.getElementById(elemId);
    if(elem && document.createEvent) {
       var evt = document.createEvent("MouseEvents");
       evt.initEvent("click", true, false);
       elem.dispatchEvent(evt);
    }
    // readURL(this);
 }

function success_msg(data){
    $.notify({
        title: '<strong>'+ data +'</strong>',
        message: ''
    },{
        type: 'success'
    });
}

function danger_msg(data){
    $.notify({
        title: '<strong>'+ data +'</strong>',
        message: ''
    },{
        type: 'danger'
    });
}



$(document).ready(function () {
    var date=new Date();
    $('input[type="checkbox"]').click(function(){
        if($(this).prop("checked") == true){
            var val=date.getFullYear()+"-"+date.getDate()+"-"+(date.getMonth()+1);
		    $("#exp_to_date").val(val);
        }
        else if($(this).prop("checked") == false){
            $("#exp_to_date").val("");
        }
    });
});



function InsertOrUpdateCV(){
    var cv_id = $("#id").val();
    var resume_title = $("#resume_title").val();
    // var percentage = $("#percentage").val();

    var CVData  = new FormData();

    CVData.append('cv_id', cv_id);
    CVData.append('resume_title', resume_title);
    CVData.append('cv', $('#file')[0].files[0]);
    CVData.append('_token', $("input[name=_token]").val());


    $.ajax({
        type: "POST",
        url: "/Candidate/InsertCV",
        data: CVData,
        dataType: "JSON",
        contentType: false,
        processData: false,
        success: function (data) {
            if(data.error){
                danger_msg(data.message);
                return false;
            }else{
                success_msg(data.message);
                location.reload();
                return false;
            }
        }
    });
}