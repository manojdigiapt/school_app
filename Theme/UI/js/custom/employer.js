// HOME_URL = "/";
$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

$(document).on("click", "#ForgotEmployer", function(){
    $("#EmployerForm").hide();
    $("#EmployerEmailForm").show();
});

$(document).on("click", "#BackToLoginEmployer", function(){
    $("#EmployerForm").show();
    $("#EmployerEmailForm").hide();
});


$(document).on("click", "#LoginEmployer", function(){
    var employerusername = $("#EmployerUserName").val();
    var employerpassword = $("#EmployerUserPassword").val();

    if(employerusername == ""){
        danger_msg("Please type username");
        $("#EmployerUserName").focus();
        return false;
    }

    if(employerpassword == ""){
        danger_msg("Please type password");
        $("#EmployerUserPassword").focus();
        return false;
    }

    var EmployerLogin = {
        employerusername: employerusername,
        employerpassword: employerpassword
    }

    $.ajax({
        type: "POST",
        url: "/Employer/EmployerLogin",
        data: EmployerLogin,
        dataType: "JSON",
        success: function (data) {
            if(data.error){
                danger_msg(data.message);
                return false;
            }else{
                success_msg(data.message);
                window.location.href="/Employer/Dashboard";
                return false;
            }
        }
    });
});

$(document).on("click", "#SubmitEmployerForgotEmail", function(){
    

    var employeremail = $("#EmployerForgotEmail").val();

    if(employeremail == ""){
        danger_msg("Please type email address");
        $("#EmployerForgotEmail").focus();
        return false;
    }else if(IsEmail(employeremail)==false){
        danger_msg("Please check your email address");
        $('#EmployerForgotEmail').focus();
        return false;
    }


    var EmployerData = {
        employeremail: employeremail
    }

    $.ajax({
        type: "POST",
        url: "/Employer/CheckEmployerForgotEmail",
        data: EmployerData,
        dataType: "JSON",
        success: function (data) {
            if(data.error){
                danger_msg(data.message);
                return false;
            }else{
                success_msg(data.message);
                return false;
            }
        }
    });
});

$(document).on("click", "#SignupEmployer", function(){
    // var username = $("#UserName").val();
    var companyname = $("#CompanyName").val();
    var employerpassword = $("#EmployerPassword").val();
    var employeremail = $("#EmployerEmail").val();
    var employerphonenumber = $("#EmployerPhone").val();

    var MobileOTP = $("#EmployerMobileOTP").val();
    var EmailOTP = $("#EmployerEmailOTP").val();
    

    if(MobileOTP == ""){
        danger_msg("Please enter your Mobile OTP");
        $("#EmployerMobileOTP").focus();
        return false;
    }

    if(EmailOTP == ""){
        danger_msg("Please enter your Email OTP");
        $("#EmployerEmailOTP").focus();
        return false;
    }

    var EmployerData = {
        companyname: companyname,
        employerpassword: employerpassword, 
        employeremail: employeremail,
        employerphonenumber: employerphonenumber,
        MobileOTP: MobileOTP,
        EmailOTP: EmailOTP
    }
    $.ajax({
        type: "POST",
        url: "/Employer/InsertEmployer",
        data: EmployerData,
        dataType: "JSON",
        success: function (data) {
            if(data.error){
                danger_msg(data.message);
                return false;
            }else{
                success_msg(data.message);
                window.location.href="/Employer/Profile";
                return false;
            }
        }
    });
});



$(document).on("click", "#SignupEmployerOtp", function(){
    
    var companyname = $("#CompanyName").val();
    var employerpassword = $("#EmployerPassword").val();
    var employeremail = $("#EmployerEmail").val();
    var employerphonenumber = $("#EmployerPhone").val();

    if(companyname == ""){
        danger_msg("Please type company name");
        $("#CompanyName").focus();
        return false;
    }

    if(employerpassword == ""){
        danger_msg("Please type password");
        $("#EmployerPassword").focus();
        return false;
    }

    if(employeremail == ""){
        danger_msg("Please type email address");
        $("#EmployerEmail").focus();
        return false;
    }else if(IsEmail(employeremail)==false){
        danger_msg("Please check your email address");
        $('#EmployerEmail').focus();
        return false;
    }

    if(employerphonenumber == ""){
        danger_msg("Please type phone number");
        $("#EmployerPhone").focus();
        return false;
    }

    var EmployerData = {
        companyname: companyname,
        employerpassword: employerpassword, 
        employeremail: employeremail,
        employerphonenumber: employerphonenumber
    }


    $.ajax({
        type: "POST",
        url: "/SendEmployerOtp",
        data: EmployerData,
        dataType: "JSON",
        success: function (data) {
            if(data.error){
                danger_msg(data.message);
                return false;
            }else{
                $("#EmployerRegister").hide();
                $("#EmployerOtpLoader").show();
                $("#EmployerOTP").show();
                return false;
            }
        },
        complete:function(data){
        // Hide image container
            $("#EmployerOtpLoader").hide();
        }
    });
});



function success_msg(data){
    $.notify({
        title: '<strong>'+ data +'</strong>',
        message: ''
    },{
        type: 'success'
    });
}

function danger_msg(data){
    $.notify({
        title: '<strong>'+ data +'</strong>',
        message: ''
    },{
        type: 'danger'
    });
}

function IsEmail(email) {
    var regex = /^([a-zA-Z0-9_\.\-\+])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    if(!regex.test(email)) {
       return false;
    }else{
       return true;
    }
  }

  function validatePhone(txtPhone) {
    var a = document.getElementById(txtPhone).value;
    var filter = /[1-9]{1}[0-9]{9}/;
    if (filter.test(a)) {
        return true;
    }
    else {
        return false;
    }
}