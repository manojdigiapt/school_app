// HOME_URL = "/";
$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});





function filterSystem(minPrice, maxPrice) {
$("#computers div.system").hide().filter(function() {
    var price = parseInt($(this).data("price"), 10);
    return price >= minPrice && price <= maxPrice;
}).show();
}


// Jobs

$(document).on("change", ".CheckFilterJobs", function(){
    var date_posted = $(this).val();
    // var job_type = $('.CheckJobType:checked').val();

    var FiltersData = {
        date_posted: date_posted
    }

    $.ajax({
        type: "POST",
        url: "/FilterJobs",
        data: FiltersData,
        dataType: "JSON",
        beforeSend: function(){
            // Show image container
            $("#AjaxLoader").show();
        },
        success: function (data) {
            if(data.error){
                danger_msg(data.message);
                return false;
            }else{
                $("#SearchJobsResults").html(data);
                console.log(data);
                return false;
            }
        },
        complete:function(data){
        // Hide image container
            $("#AjaxLoader").hide();
        }
    });
});



$(document).ready(function () {

	var job_type = [];
	// var role = [];
	// var job_title = $('#job_title').val();
    // Listen for 'change' event, so this triggers when the user clicks on the checkboxes labels
    $('.CheckJobType').on('change', function (e) {

    	e.preventDefault();
        job_type = new Array();

        $('input[name="CheckJobType[]"]:checked').each(function()
        {
        	job_type.push($(this).val());
        	// job_role.push($('#role').val());
            // alert(JSON.stringify(job_type));
        });
        // alert(JSON.stringify(job_type));

        $.ajax({
            type: "POST",
            url: "/FilterJobType",
            data: {job_type: job_type},
            dataType: "JSON",
            beforeSend: function(){
                // Show image container
                $("#AjaxLoader").show();
            },
            success: function (data) {
                if(data.error){
                    console.log(data);
                    return false;
                }else{
                    $("#SearchJobsResults").html(data);
                    return false;
                }
            },
            complete:function(data){
                // Hide image container
                $("#AjaxLoader").hide();
            }
        });
	// parent.location.hash = "jobs/search";

});

});




$(document).ready(function () {

	var city = [];
	// var role = [];
	// var job_title = $('#job_title').val();
    // Listen for 'change' event, so this triggers when the user clicks on the checkboxes labels
    $('.CheckCity').on('change', function (e) {

    	e.preventDefault();
        city = new Array();

        $('input[name="CheckCity[]"]:checked').each(function()
        {
        	city.push($(this).val());
        	// job_role.push($('#role').val());
            // alert(JSON.stringify(job_type));
        });
        // alert(JSON.stringify(job_type));

        $.ajax({
            type: "POST",
            url: "/FilterDesiredLocation",
            data: {city: city},
            dataType: "JSON",
            beforeSend: function(){
                // Show image container
                $("#AjaxLoader").show();
            },
            success: function (data) {
                if(data.error){
                    console.log(data);
                    return false;
                }else{
                    $("#SearchJobsResults").html(data);
                    return false;
                }
            },
            complete:function(data){
                // Hide image container
                $("#AjaxLoader").hide();
            }
        });
	// parent.location.hash = "jobs/search";

});

});



$(document).ready(function () {

	var industry = [];
	// var role = [];
	// var job_title = $('#job_title').val();
    // Listen for 'change' event, so this triggers when the user clicks on the checkboxes labels
    $('.CheckIndustry').on('change', function (e) {

    	e.preventDefault();
        industry = new Array();

        $('input[name="CheckIndustry[]"]:checked').each(function()
        {
        	industry.push($(this).val());
        	// job_role.push($('#role').val());
            // alert(JSON.stringify(job_type));
        });
        // alert(JSON.stringify(job_type));

        $.ajax({
            type: "POST",
            url: "/FilterIndustry",
            data: {industry: industry},
            dataType: "JSON",
            beforeSend: function(){
                // Show image container
                $("#AjaxLoader").show();
            },
            success: function (data) {
                if(data.error){
                    console.log(data);
                    return false;
                }else{
                    $("#SearchJobsResults").html(data);
                    return false;
                }
            },
            complete:function(data){
                // Hide image container
                $("#AjaxLoader").hide();
            }
        });
	// parent.location.hash = "jobs/search";

});

});







// Candidates

$(document).on("change", ".CheckFilterCandidate", function(){
    var date_posted = $(this).val();
    // var job_type = $('.CheckJobType:checked').val();

    var FiltersData = {
        date_posted: date_posted
    }

    $.ajax({
        type: "POST",
        url: "/FilterCandidates",
        data: FiltersData,
        dataType: "JSON",
        beforeSend: function(){
            // Show image container
            $("#AjaxLoader").show();
        },
        success: function (data) {
            if(data.error){
                danger_msg(data.message);
                return false;
            }else{
                $("#SearchCandidatesResults").html(data);
                console.log(data);
                return false;
            }
        },
        complete:function(data){
        // Hide image container
            $("#AjaxLoader").hide();
        }
    });
});



$(document).ready(function () {

	var city = [];
	// var role = [];
	// var job_title = $('#job_title').val();
    // Listen for 'change' event, so this triggers when the user clicks on the checkboxes labels
    $('.CheckCandidateCity').on('change', function (e) {

    	e.preventDefault();
        city = new Array();

        $('input[name="CheckCandidateCity[]"]:checked').each(function()
        {
        	city.push($(this).val());
        	// job_role.push($('#role').val());
            // alert(JSON.stringify(job_type));
        });
        // alert(JSON.stringify(job_type));

        $.ajax({
            type: "POST",
            url: "/FilterCandidateDesiredLocation",
            data: {city: city},
            dataType: "JSON",
            beforeSend: function(){
                // Show image container
                $("#AjaxLoader").show();
            },
            success: function (data) {
                if(data.error){
                    console.log(data);
                    return false;
                }else{
                    $("#SearchCandidatesResults").html(data);
                    return false;
                }
            },
            complete:function(data){
                // Hide image container
                $("#AjaxLoader").hide();
            }
        });
	// parent.location.hash = "jobs/search";

});

});



$(document).ready(function () {

	var industry = [];
	// var role = [];
	// var job_title = $('#job_title').val();
    // Listen for 'change' event, so this triggers when the user clicks on the checkboxes labels
    $('.CheckCandidateIndustry').on('change', function (e) {

    	e.preventDefault();
        industry = new Array();

        $('input[name="CheckCandidateIndustry[]"]:checked').each(function()
        {
        	industry.push($(this).val());
        	// job_role.push($('#role').val());
            // alert(JSON.stringify(job_type));
        });
        // alert(JSON.stringify(job_type));

        $.ajax({
            type: "POST",
            url: "/FilterCandidateIndustry",
            data: {industry: industry},
            dataType: "JSON",
            beforeSend: function(){
                // Show image container
                $("#AjaxLoader").show();
            },
            success: function (data) {
                if(data.error){
                    console.log(data);
                    return false;
                }else{
                    $("#SearchCandidatesResults").html(data);
                    return false;
                }
            },
            complete:function(data){
                // Hide image container
                $("#AjaxLoader").hide();
            }
        });
	// parent.location.hash = "jobs/search";

});

});


$(document).ready(function () {

	var employee_type = [];
	// var role = [];
	// var job_title = $('#job_title').val();
    // Listen for 'change' event, so this triggers when the user clicks on the checkboxes labels
    $('.CheckEmployeeType').on('change', function (e) {

    	e.preventDefault();
        employee_type = new Array();

        $('input[name="CheckEmployeeType[]"]:checked').each(function()
        {
        	employee_type.push($(this).val());
        	// job_role.push($('#role').val());
            // alert(JSON.stringify(job_type));
        });
        // alert(JSON.stringify(job_type));

        $.ajax({
            type: "POST",
            url: "/FilterEmployeeType",
            data: {employee_type: employee_type},
            dataType: "JSON",
            beforeSend: function(){
                // Show image container
                $("#AjaxLoader").show();
            },
            success: function (data) {
                if(data.error){
                    console.log(data);
                    return false;
                }else{
                    $("#SearchCandidatesResults").html(data);
                    return false;
                }
            },
            complete:function(data){
                // Hide image container
                $("#AjaxLoader").hide();
            }
        });
	// parent.location.hash = "jobs/search";

});

});


$(document).ready(function () {

	var salary_type = [];
	// var role = [];
	// var job_title = $('#job_title').val();
    // Listen for 'change' event, so this triggers when the user clicks on the checkboxes labels
    $('.CheckSalary').on('change', function (e) {

    	e.preventDefault();
        salary_type = new Array();

        $('input[name="CheckSalary[]"]:checked').each(function()
        {
        	salary_type.push($(this).val());
        	// job_role.push($('#role').val());
            // alert(JSON.stringify(job_type));
        });
        // alert(JSON.stringify(job_type));

        $.ajax({
            type: "POST",
            url: "/FilterSalaryType",
            data: {salary_type: salary_type},
            dataType: "JSON",
            beforeSend: function(){
                // Show image container
                $("#AjaxLoader").show();
            },
            success: function (data) {
                if(data.error){
                    console.log(data);
                    return false;
                }else{
                    $("#SearchCandidatesResults").html(data);
                    return false;
                }
            },
            complete:function(data){
                // Hide image container
                $("#AjaxLoader").hide();
            }
        });
	// parent.location.hash = "jobs/search";

});

});




// Trainings

$(document).on("change", ".CheckFilterTrainings", function(){
    var date_posted = $(this).val();
    // var job_type = $('.CheckJobType:checked').val();

    var FiltersData = {
        date_posted: date_posted
    }

    $.ajax({
        type: "POST",
        url: "/FilterTrainings",
        data: FiltersData,
        dataType: "JSON",
        beforeSend: function(){
            // Show image container
            $("#AjaxLoader").show();
        },
        success: function (data) {
            if(data.error){
                danger_msg(data.message);
                return false;
            }else{
                $("#SearchTrainingResults").html(data);
                console.log(data);
                return false;
            }
        },
        complete:function(data){
        // Hide image container
            $("#AjaxLoader").hide();
        }
    });
});


$(document).ready(function () {

	var duration = [];
	// var role = [];
	// var job_title = $('#job_title').val();
    // Listen for 'change' event, so this triggers when the user clicks on the checkboxes labels
    $('.Duration').on('change', function (e) {

    	e.preventDefault();
        duration = new Array();

        $('input[name="Duration[]"]:checked').each(function()
        {
        	duration.push($(this).val());
        	// job_role.push($('#role').val());
            // alert(JSON.stringify(job_type));
        });
        // alert(JSON.stringify(job_type));

        $.ajax({
            type: "POST",
            url: "/FilterDuration",
            data: {duration: duration},
            dataType: "JSON",
            beforeSend: function(){
                // Show image container
                $("#AjaxLoader").show();
            },
            success: function (data) {
                $("#SearchTrainingResults").html("");
                if(data.error){
                    console.log(data);
                    return false;
                }else{
                    $("#SearchTrainingResults").html(data);
                    return false;
                }
            },
            complete:function(data){
                // Hide image container
                $("#AjaxLoader").hide();
            }
        });
	// parent.location.hash = "jobs/search";

});

});




// Sort By
function SortByJobs(){
    var sortby = $("#sortby").val();

    $.ajax({
        type: "POST",
        url: "/SortByJobs",
        data: {sortby: sortby},
        dataType: "JSON",
        beforeSend: function(){
            // Show image container
            $("#AjaxLoader").show();
        },
        success: function (data) {
            if(data.error){
                console.log(data);
                return false;
            }else{
                $("#SearchJobsResults").html(data);
                return false;
            }
        },
        complete:function(data){
        // Hide image container
            $("#AjaxLoader").hide();
        }
    });
}


function SortByCandidates(){
    var sortby = $("#sortby").val();

    $.ajax({
        type: "POST",
        url: "/SortByCandidates",
        data: {sortby: sortby},
        dataType: "JSON",
        beforeSend: function(){
            // Show image container
            $("#AjaxLoader").show();
        },
        success: function (data) {
            if(data.error){
                console.log(data);
                return false;
            }else{
                $("#SearchCandidatesResults").html(data);
                return false;
            }
        },
        complete:function(data){
        // Hide image container
            $("#AjaxLoader").hide();
        }
    });
}



function success_msg(data){
    $.notify({
        title: '<strong>'+ data +'</strong>',
        message: ''
    },{
        type: 'success'
    });
}

function danger_msg(data){
    $.notify({
        title: '<strong>'+ data +'</strong>',
        message: ''
    },{
        type: 'danger'
    });
}
