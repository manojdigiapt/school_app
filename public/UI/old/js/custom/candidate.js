// HOME_URL = "/";
$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

  



$(document).on("click", "#ForgotCandidate", function(){
    $("#CandidateForm").hide();
    $("#CandidateEmailForm").show();
});

$(document).on("click", "#BackToLoginCandidate", function(){
    $("#CandidateForm").show();
    $("#CandidateEmailForm").hide();
});

$(document).on("click", "#LoginCandidate", function(){
    

    var candidateusername = $("#CandidateUserName").val();
    var candidatepassword = $("#CandidateUserPassword").val();

    if(candidateusername == ""){
        danger_msg("Please type username");
        $("#CandidateUserName").focus();
        return false;
    }

    if(candidatepassword == ""){
        danger_msg("Please type password");
        $("#CandidateUserPassword").focus();
        return false;
    }

    var CandidateLogin = {
        candidateusername: candidateusername,
        candidatepassword: candidatepassword
    }

    $.ajax({
        type: "POST",
        url: "/Candidate/CandidateLogin",
        data: CandidateLogin,
        dataType: "JSON",
        success: function (data) {
            if(data.error){
                danger_msg(data.message);
                return false;
            }else{
                success_msg(data.message);
                if(data.type == 1){
                    window.location.href="/Candidate/Dashboard";
                    return false;
                }else if(data.type == 2){
                    window.location.href="Trainee/Dashboard";
                    return false;
                }
                
            }
        }
    });
});

$(document).on("click", "#SubmitCandidateForgotEmail", function(){
    
    var candidateemail = $("#CandidateForgotEmail").val();

    if(candidateemail == ""){
        danger_msg("Please type email address");
        $("#CandidateForgotEmail").focus();
        return false;
    }else if(IsEmail(candidateemail)==false){
        danger_msg("Please check your email address");
        $('#CandidateForgotEmail').focus();
        return false;
    }


    var CandidateData = {
        candidateemail: candidateemail
    }

    $.ajax({
        type: "POST",
        url: "/Candidate/CheckForgotEmail",
        data: CandidateData,
        dataType: "JSON",
        success: function (data) {
            if(data.error){
                danger_msg(data.message);
                return false;
            }else{
                success_msg(data.message);
                return false;
            }
        }
    });
});


$(document).on("click", "#SignupCandidate", function(){
    var candidatename = $("#CandidateName").val();
    var candidatepassword = $("#CandidatePassword").val();
    var candidateemail = $("#CandidateEmail").val();
    var candidatetype = $("#CandidateType :selected").val();
    var candidatephonenumber = $("#CandidatePhone").val();
    var MobileOTP = $("#MobileOTP").val();
    var EmailOTP = $("#EmailOTP").val();
    

    if(MobileOTP == ""){
        danger_msg("Please enter your Mobile OTP");
        $("#MobileOTP").focus();
        return false;
    }

    if(EmailOTP == ""){
        danger_msg("Please enter your Email OTP");
        $("#EmailOTP").focus();
        return false;
    }

   

    var CandidateData = {
        candidatename: candidatename,
        candidatepassword: candidatepassword, 
        candidateemail: candidateemail,
        candidatetype: candidatetype,
        candidatephonenumber: candidatephonenumber,
        MobileOTP: MobileOTP,
        EmailOTP: EmailOTP,
        _token: $("input[name=_token]").val()
    }
    $.ajax({
        type: "POST",
        url: "/Candidate/InsertCandidate",
        data: CandidateData,
        dataType: "JSON",
        success: function (data) {
            if(data.error){
                danger_msg(data.message);
                return false;
            }else{
                if(data.type == 1){
                    window.location.href="/Candidate/Profile";
                    return false;
                }else if(data.type == 2){
                    window.location.href="/Trainee/Profile";
                    return false;
                }
            }
        }
    });
});



$(document).on("click", "#SignupOtp", function(){
    
   
    var candidatename = $("#CandidateName").val();
    var candidatepassword = $("#CandidatePassword").val();
    var candidateemail = $("#CandidateEmail").val();
    var candidatetype = $("#CandidateType :selected").val();
    var candidatephonenumber = $("#CandidatePhone").val();
    

    if(candidatename == ""){
        danger_msg("Please type username");
        $("#CandidateName").focus();
        return false;
    }

    if(candidatepassword == ""){
        danger_msg("Please type password");
        $("#CandidatePassword").focus();
        return false;
    }

    if(candidateemail == ""){
        danger_msg("Please type email address");
        $("#CandidateEmail").focus();
        return false;
    }else if(IsEmail(candidateemail)==false){
        danger_msg("Please check your email address");
        $('#CandidateEmail').focus();
        return false;
    }

    if(candidatetype == "Please Select Candidate Type"){
        danger_msg("Please Select Candidate Type");
        $("#CandidateType").focus();
        return false;
    }

    if(candidatephonenumber == ""){
        danger_msg("Please type phone number");
        $("#CandidatePhone").focus();
        return false;
    }

   

    var CandidateData = {
        candidatename: candidatename,
        candidatetype: candidatetype,
        candidateemail: candidateemail,
        candidatephonenumber: candidatephonenumber,
        _token: $("input[name=_token]").val()
    }
    $.ajax({
        type: "POST",
        url: "/SendOtp",
        data: CandidateData,
        dataType: "JSON",
        success: function (data) {
            if(data.error){
                danger_msg(data.message);
                return false;
            }else{
                $("#CandidateRegister").hide();
                $("#OtpLoader").show();
                $("#CandidateOTP").show();
                return false;
            }
        },
        complete:function(data){
        // Hide image container
            $("#OtpLoader").hide();
        }
    });
});




// $("#CandidatePhone").keypress(function() {
//     var minLength = 9;
//     var maxLength = 9;

//     var char = $(this).val();
//     var charLength = $(this).val().length;
//     if(charLength < minLength){
//         // danger_msg('Length is short, minimum '+minLength+' required.');
//         $("#LinkVerify").hide();
//     }else if(charLength > maxLength){
//         danger_msg('Length is not valid, maximum '+maxLength+' allowed.');
//         $(this).val(char.substring(0, maxLength));
//     }else{
//         // success_msg('Length is valid');
//         $("#LinkVerify").show();
//     }
// });

// $(document).on("click", "#LinkVerify", function(){
//     var mobile = $("#CandidatePhone").val();

//     $.ajax({
//         type: "POST",
//         url: "/GenerateOtp",
//         data: {mobile: mobile},
//         dataType: "JSON",
//         beforeSend: function(){
//             // Show image container
//             $("#OtpLoader").show();
//         },
//         success: function (data) {
//             if(data.error){ 
//                 danger_msg(data.message);
//                 return false;
//             }else{
//                 $("#OtpVerify").show();
//                 success_msg(data.message);
//                 return false;
//             }
//         },
//         complete:function(data){
//         // Hide image container
//             $("#OtpLoader").hide();
//         }
//     });
    
// });
    








function success_msg(data){
    $.notify({
        title: '<strong>'+ data +'</strong>',
        message: ''
    },{
        type: 'success'
    });
}

function danger_msg(data){
    $.notify({
        title: '<strong>'+ data +'</strong>',
        message: ''
    },{
        type: 'danger'
    });
}



function IsEmail(email) {
    var regex = /^([a-zA-Z0-9_\.\-\+])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    if(!regex.test(email)) {
       return false;
    }else{
       return true;
    }
  }

  function validatePhone(txtPhone) {
    var a = document.getElementById(txtPhone).value;
    var filter = /[1-9]{1}[0-9]{9}/;
    if (filter.test(a)) {
        return true;
    }
    else {
        return false;
    }
}