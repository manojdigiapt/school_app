$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});



function BrowseEmployerPic(elemId) {
    var elem = document.getElementById(elemId);
    if(elem && document.createEvent) {
       var evt = document.createEvent("MouseEvents");
       evt.initEvent("click", true, false);
       elem.dispatchEvent(evt);
    }
    // readURL(this);
 }


 function readEmployerURL(input) {
    if (input.files && input.files[0]) {
      var reader = new FileReader();
      
      reader.onload = function(e) {
        $('#EmployerProfilePic').attr('src', e.target.result);
        $('.profile-head-width').attr('src', e.target.result);
      }
      
      reader.readAsDataURL(input.files[0]);
    }
  }

$("#file").change(function() {
    readEmployerURL(this);
  });
  
  

$(document).on("click", "#AddEmployerProfile", function(){
    // var fullname = $("#fullname").val();
    // var id = $("#id").val();
    var company_name = $("#company_name").val();
    var allow_search = $("#allow_search").val();
    var since = $("#since").val();
    var team_size = $("#team_size").val();
    var categories = $("#categories").val();
    var description = CKEDITOR.instances['description'].getData();
    var facebook = $("#facebook").val();
    var twitter = $("#twitter").val();
    var linkedin = $("#linkedin").val();
    var googleplus = $("#googleplus").val();
    var phone = $("#phone").val();
    var mobile = $("#mobile").val();
    var email = $("#email").val();
    var website = $("#website").val();
    var address = $("#address").val();
    var zipcode = $("#zipcode").val();
    var country = $("#country").val();
    var state = $("#state").val();
    var city = $("#city").val();
    var file = $("#file").val();

    if(company_name == ""){
        danger_msg("Company name should not empty");
        $("#company_name").focus();
        return false;
    }

    if(since == ""){
        danger_msg("Since should not empty");
        $("#since").focus();
        return false;
    }

    if(team_size == ""){
        danger_msg("Team size should not empty");
        $("#team_size").focus();
        return false;
    }

    if(categories == ""){
        danger_msg("Categories should not empty");
        $("#categories").focus();
        return false;
    }

    if(description == ""){
        danger_msg("Description should not empty");
        $("#description").focus();
        return false;
    }

    if(phone == ""){
        danger_msg("Phone should not empty");
        $("#phone").focus();
        return false;
    }

    if(mobile == ""){
        danger_msg("Mobile should not empty");
        $("#mobile").focus();
        return false;
    }

    if(email == ""){
        danger_msg("Email should not empty");
        $("#email").focus();
        return false;
    }

    if(address == ""){
        danger_msg("Address should not empty");
        $("#address").focus();
        return false;
    }

    if(zipcode == ""){
        danger_msg("Zipcode should not empty");
        $("#zipcode").focus();
        return false;
    }

    if(country == ""){
        danger_msg("Country should not empty");
        $("#country").focus();
        return false;
    }

    if(state == ""){
        danger_msg("State should not empty");
        $("#state").focus();
        return false;
    }

    if(city == ""){
        danger_msg("City should not empty");
        $("#city").focus();
        return false;
    }

    var EmployerProfileData = new FormData();

    // EmployerProfileData.append('id', id);
    EmployerProfileData.append('file', $('#file')[0].files[0]);
    EmployerProfileData.append('company_name', company_name);
    EmployerProfileData.append('allow_search', allow_search);
    EmployerProfileData.append('since', since);
    EmployerProfileData.append('team_size', team_size);
    EmployerProfileData.append('categories', categories);
    EmployerProfileData.append('description', description);
    EmployerProfileData.append('facebook', facebook);
    EmployerProfileData.append('twitter', twitter);
    EmployerProfileData.append('linkedin', linkedin);
    EmployerProfileData.append('googleplus', googleplus);
    EmployerProfileData.append('phone', phone);
    EmployerProfileData.append('mobile', mobile);
    EmployerProfileData.append('email', email);
    EmployerProfileData.append('website', website);
    EmployerProfileData.append('address', address);
    EmployerProfileData.append('zipcode', zipcode);
    EmployerProfileData.append('country', country);
    EmployerProfileData.append('state', state);
    EmployerProfileData.append('city', city);
    EmployerProfileData.append('_token', $("input[name=_token]").val());
    

    // console.log($('#file')[0].files[0]);
    // return false;

    $.ajax({
        type: "POST",
        url: "/Employer/InsertEmployerProfile",
        data: EmployerProfileData,
        dataType: "JSON",
        contentType: false,
        processData: false,
        success: function (data) {
            if(data.error){
                danger_msg(data.message);
                return false;
            }else{
                success_msg(data.message);
                return false;
            }
        }
    });
});







// Jobs
$(document).on("click", "#PostJobs", function(){
    var title = $("#title").val();
    var address = $("#address").val();
    var zipcode = $("#zipcode").val();
    var country = $("#country").val();
    var state = $("#state").val();
    var city = $("#city").val();
    var email = $("#email").val();
    var mobile = $("#mobile").val();
    var job_type = $("#job_type :selected").val();
    var industry = $("#industry :selected").val();
    var role = $("#role").val();
    var min_salary = $("#salary_min").val();
    var max_salary = $("#salary_max").val();
    var offered_salary = min_salary+ "-" + max_salary;
    var salary_type = $("#salary_type").val();
    var gender = $("#gender :selected").val();
    var career_level = $("#career_level :selected").val();
    var experience_years = $("#experience_year").val();
    var experience_months = $("#experience_month").val();
    var experience = experience_years+ "-" +experience_months;
    var last_date = $("#last_date").val();
    var qualification = $("#qualification").val();
    var positions = $("#positions").val();
    var description = CKEDITOR.instances['description'].getData();


    if(title == ""){
        danger_msg("Title should not empty");
        $("#title").focus();
        return false;
    }

    if(address == ""){
        danger_msg("Address should not empty");
        $("#address").focus();
        return false;
    }

    if(zipcode == ""){
        danger_msg("Zipcode should not empty");
        $("#zipcode").focus();
        return false;
    }

    if(country == ""){
        danger_msg("Country should not empty");
        $("#country").focus();
        return false;
    }

    if(state == ""){
        danger_msg("State should not empty");
        $("#state").focus();
        return false;
    }

    if(city == ""){
        danger_msg("City should not empty");
        $("#city").focus();
        return false;
    }

    if(email == ""){
        danger_msg("Email should not empty");
        $("#email").focus();
        return false;
    }

    if(mobile == ""){
        danger_msg("Mobile should not empty");
        $("#mobile").focus();
        return false;
    }

    if(job_type == "Select job type"){
        danger_msg("Job type should not empty");
        $("#job_type").focus();
        return false;
    }

    if(industry == "Select industry"){
        danger_msg("Industry should not empty");
        $("#industry").focus();
        return false;
    }

    if(role == ""){
        danger_msg("Role should not empty");
        $("#role").focus();
        return false;
    }

    if(min_salary == ""){
        danger_msg("Salary min should not empty");
        $("#salary_min").focus();
        return false;
    }

    if(max_salary == ""){
        danger_msg("Salary max should not empty");
        $("#salary_max").focus();
        return false;
    }

    if(gender == "Select gender"){
        danger_msg("Gender should not empty");
        $("#gender").focus();
        return false;
    }

    if(career_level == "2"){
        if($("#experience_year").val() == ""){
            danger_msg("Experience year should not empty");
            $("#experience_year").focus();
            return false;
        }

        if($("#experience_month").val() == ""){
            danger_msg("Experience month should not empty");
            $("#experience_month").focus();
            return false;
        }
    }

    if(last_date == ""){
        danger_msg("Last date should not empty");
        $("#last_date").focus();
        return false;
    }

    if(qualification == ""){
        danger_msg("Qualification should not empty");
        $("#qualification").focus();
        return false;
    }

    if(positions == ""){
        danger_msg("Positions should not empty");
        $("#positions").focus();
        return false;
    }

    if(description == ""){
        danger_msg("Description should not empty");
        $("#description").focus();
        return false;
    }

    var JobsData = {
        title: title,
        address: address,
        zipcode: zipcode,
        country: country,
        state: state,
        city: city,
        email: email,
        mobile: mobile,
        job_type: job_type,
        industry: industry,
        role: role,
        offered_salary: offered_salary,
        salary_type: salary_type,
        gender: gender,
        career_level: career_level,
        experience: experience,
        last_date: last_date,
        qualification: qualification,
        positions: positions,
        description: description
    }


    $.ajax({
        type: "POST",
        url: "/Employer/AddJobs",
        data: JobsData,
        dataType: "JSON",
        success: function (data) {
            if(data.error){
                danger_msg(data.message);
                return false;
            }else{
                success_msg(data.message);
                return false;
            }
        }
    });

});



$(document).on("click", "#UpdateJobs", function(){
    var id = $("#id").val();
    var title = $("#title").val();
    var address = $("#address").val();
    var zipcode = $("#zipcode").val();
    var country = $("#country").val();
    var state = $("#state").val();
    var city = $("#city").val();
    var email = $("#email").val();
    var mobile = $("#mobile").val();
    var job_type = $("#job_type :selected").val();
    var industry = $("#industry :selected").val();
    var role = $("#role").val();
    var min_salary = $("#salary_min").val();
    var max_salary = $("#salary_max").val();
    var offered_salary = min_salary+ "-" + max_salary;
    var salary_type = $("#salary_type").val();
    var gender = $("#gender :selected").val();
    var career_level = $("#career_level :selected").val();
    var experience_years = $("#experience_year").val();
    var experience_months = $("#experience_month").val();
    var experience = experience_years+ "-" +experience_months;
    var last_date = $("#last_date").val();
    var qualification = $("#qualification").val();
    var positions = $("#positions").val();
    var description = CKEDITOR.instances['description'].getData();

    if(title == ""){
        danger_msg("Title should not empty");
        $("#title").focus();
        return false;
    }

    if(address == ""){
        danger_msg("Address should not empty");
        $("#address").focus();
        return false;
    }

    if(zipcode == ""){
        danger_msg("Zipcode should not empty");
        $("#zipcode").focus();
        return false;
    }

    if(country == ""){
        danger_msg("Country should not empty");
        $("#country").focus();
        return false;
    }

    if(state == ""){
        danger_msg("State should not empty");
        $("#state").focus();
        return false;
    }

    if(city == ""){
        danger_msg("City should not empty");
        $("#city").focus();
        return false;
    }

    if(email == ""){
        danger_msg("Email should not empty");
        $("#email").focus();
        return false;
    }

    if(mobile == ""){
        danger_msg("Mobile should not empty");
        $("#mobile").focus();
        return false;
    }

    if(job_type == "Select job type"){
        danger_msg("Job type should not empty");
        $("#job_type").focus();
        return false;
    }

    if(industry == "Select industry"){
        danger_msg("Industry should not empty");
        $("#industry").focus();
        return false;
    }

    if(role == ""){
        danger_msg("Role should not empty");
        $("#role").focus();
        return false;
    }

    if(min_salary == ""){
        danger_msg("Salary min should not empty");
        $("#salary_min").focus();
        return false;
    }

    if(max_salary == ""){
        danger_msg("Salary max should not empty");
        $("#salary_max").focus();
        return false;
    }

    if(gender == "Select gender"){
        danger_msg("Gender should not empty");
        $("#gender").focus();
        return false;
    }

    if(career_level == "2"){
        if($("#experience_year").val() == ""){
            danger_msg("Experience year should not empty");
            $("#experience_year").focus();
            return false;
        }

        if($("#experience_month").val() == ""){
            danger_msg("Experience month should not empty");
            $("#experience_month").focus();
            return false;
        }
    }

    if(last_date == ""){
        danger_msg("Last date should not empty");
        $("#last_date").focus();
        return false;
    }

    if(qualification == ""){
        danger_msg("Qualification should not empty");
        $("#qualification").focus();
        return false;
    }

    if(positions == ""){
        danger_msg("Positions should not empty");
        $("#positions").focus();
        return false;
    }

    if(description == ""){
        danger_msg("Description should not empty");
        $("#description").focus();
        return false;
    }

    var JobsData = {
        id: id,
        title: title,
        address: address,
        zipcode: zipcode,
        country: country,
        state: state,
        city: city,
        email: email,
        mobile: mobile,
        job_type: job_type,
        industry: industry,
        role: role,
        offered_salary: offered_salary,
        salary_type: salary_type,
        gender: gender,
        career_level: career_level,
        experience: experience,
        last_date: last_date,
        qualification: qualification,
        positions: positions,
        description: description
    }


    $.ajax({
        type: "POST",
        url: "/Employer/UpdateJobs",
        data: JobsData,
        dataType: "JSON",
        success: function (data) {
            if(data.error){
                danger_msg(data.message);
                return false;
            }else{
                success_msg(data.message);
                return false;
            }
        }
    });

});


function CheckCareerLevel(){
    if($("#career_level").val() == 2){
        // $("#exp_show").show();
        $("#experience_year").css("background","#fff");
        $('#experience_year').attr("disabled", false);
        $("#experience_month").css("background","#fff");
        $('#experience_month').attr("disabled", false);
    }else{
        // $("#exp_show").hide();
        $("#experience_year").css("background","#8080803b");
        $('#experience_year').attr("disabled", true);
        $("#experience_month").css("background","#8080803b");
        $('#experience_month').attr("disabled", true);
    }
}


$("#CheckSameAddress").change(function() {
    if($(this).prop('checked')) {
        $("#address").val($("#GetAddress").val());
        $("#zipcode").val($("#GetZipcode").val());
        $("#country").val($("#GetCountry").val());
        $("#state").val($("#GetState").val());
        $("#city").val($("#GetCity").val());
    } else {
        $("#address").val("");
        $("#zipcode").val("");
        $("#country").val("");
        $("#state").val("");
        $("#city").val("");
    }
});


// Apply Jobs
$(document).on("click", "#ApplyJobs", function(){
    

    var JobsId = $("#JobsId").val();

    $.ajax({
        type: "POST",
        url: "/ApplyJobs",
        data: {JobsId: JobsId},
        dataType: "JSON",
        beforeSend: function(){
            // Show image container
            $("#ApplyBtnLoader").show();
            $("#ApplyJobs").hide();
        },
        success: function (data) {
            if(data.error){
                danger_msg(data.message);
                return false;
            }else{
                success_msg(data.message);
                return false;
            }
        },
        complete:function(data){
        // Hide image container
            $("#ApplyBtnLoader").hide();
            $("#ApplyJobs").hide();
            $("#JobsAppliedTxt").html("Your job is applied successfully.");
            $("#JobsAppliedTxt").show();
        }
    });
});











// Trainings
$(document).on("click", "#PostTrainings", function(){
    var title = $("#training_title").val();
    var start_date = $("#start_date").val();
    var duration = $("#duration").val();
    var stipend = $("#stipend").val();
    var last_date = $("#last_date").val();
    var completion = $("#completion :selected").val();
    var description = CKEDITOR.instances['description'].getData();

    if(title == "Select training"){
        danger_msg("Title should not empty");
        $("#training_title").focus();
        return false;
    }

    if(start_date == ""){
        danger_msg("Start date should not empty");
        $("#start_date").focus();
        return false;
    }

    if(duration == ""){
        danger_msg("Duration should not empty");
        $("#duration").focus();
        return false;
    }

    if(stipend == ""){
        danger_msg("Stipend should not empty");
        $("#stipend").focus();
        return false;
    }

    if(last_date == ""){
        danger_msg("Last date should not empty");
        $("#last_date").focus();
        return false;
    }

    if(completion == "Select type"){
        danger_msg("Completion should not empty");
        $("#completion").focus();
        return false;
    }

    if(description == ""){
        danger_msg("Description should not empty");
        $("#description").focus();
        return false;
    }


    var TrainingData = {
        title: title,
        start_date: start_date,
        duration: duration,
        stipend: stipend,
        last_date: last_date,
        completion: completion,
        description: description
    }


    $.ajax({
        type: "POST",
        url: "/Employer/PostTrainings",
        data: TrainingData,
        dataType: "JSON",
        success: function (data) {
            if(data.error){
                danger_msg(data.message);
                return false;
            }else{
                success_msg(data.message);
                return false;
            }
        }
    });

});


$(document).on("click", "#UpdateTrainings", function(){
    var id = $("#id").val();
    var title = $("#training_title :selected").val();
    var start_date = $("#start_date").val();
    var duration = $("#duration").val();
    var stipend = $("#stipend").val();
    var last_date = $("#last_date").val();
    var completion = $("#completion :selected").val();
    var description = CKEDITOR.instances['description'].getData();

    if(title == "Select training"){
        danger_msg("Title should not empty");
        $("#training_title").focus();
        return false;
    }

    if(start_date == ""){
        danger_msg("Start date should not empty");
        $("#start_date").focus();
        return false;
    }

    if(duration == ""){
        danger_msg("Duration should not empty");
        $("#duration").focus();
        return false;
    }

    if(stipend == ""){
        danger_msg("Stipend should not empty");
        $("#stipend").focus();
        return false;
    }

    if(last_date == ""){
        danger_msg("Last date should not empty");
        $("#last_date").focus();
        return false;
    }

    if(completion == "Please select"){
        danger_msg("Completion should not empty");
        $("#completion").focus();
        return false;
    }

    if(description == ""){
        danger_msg("Description should not empty");
        $("#description").focus();
        return false;
    }

    var TrainingData = {
        id: id,
        title: title,
        start_date: start_date,
        duration: duration,
        stipend: stipend,
        last_date: last_date,
        completion: completion,
        description: description
    }


    $.ajax({
        type: "POST",
        url: "/Employer/UpdateTrainings",
        data: TrainingData,
        dataType: "JSON",
        success: function (data) {
            if(data.error){
                danger_msg(data.message);
                return false;
            }else{
                success_msg(data.message);
                return false;
            }
        }
    });

});


function CheckTrainings(){
    var training_title = $("#training_title").val();

    if(training_title == "others"){
        // alert("others");
        $("#otherTitleShow").show();
        $("#other_title").attr("id", "training_title");
        $("#training_title").attr("id", "other_title");
        return false;
    }else{
        $("#otherTitleShow").hide();
        $(".title").attr("id", "training_title");
        $(".other_title").attr("id", "other_title");
        return false;
    }
}



// Apply Trainings
$(document).on("click", "#ApplyTrainings", function(){
   

    var TrainingId = $("#TrainingId").val();

    $.ajax({
        type: "POST",
        url: "/ApplyTrainings",
        data: {TrainingId: TrainingId},
        dataType: "JSON",
        beforeSend: function(){
            // Show image container
            $("#ApplyBtnLoader").show();
            $("#ApplyTrainings").hide();
        },
        success: function (data) {
            if(data.error){
                danger_msg(data.message);
                return false;
            }else{
                success_msg(data.message);
                return false;
            }
        },
        complete:function(data){
        // Hide image container
            $("#ApplyBtnLoader").hide();
            $("#ApplyTrainings").hide();
            $("#JobsAppliedTxt").html("Your training is applied successfully.");
            $("#JobsAppliedTxt").show();
        }
    });
});




// Rating Stars

// $(document).ready(function(event) {
//     event.preventDefault();
//     $('#AddReviews').on('click', function() {
//        alert( $(this).data('caniddate_id') , $(this).data('train_id'));
//     });
//  });

function AddReviews(CandidateId, TrainingId){
    $("#TrainingId").val(TrainingId);
    $("#CandidateId").val(CandidateId);

    $("#AddReviews").modal("show");
}

 
$(document).on("click", "#SubmitRatings", function(){
    
    var Ratings = $("#RatingStars").val();
    var feedback = $("#feedback").val();
    var TrainingId = $("#TrainingId").val();
    var CandidateId = $("#CandidateId").val();


    var RatingsData = {
        Ratings: Ratings,
        feedback: feedback,
        TrainingId: TrainingId,
        CandidateId: CandidateId
    }

    $.ajax({
        type: "POST",
        url: "/AddStars",
        data: RatingsData,
        dataType: "JSON",
        success: function (data) {
            if(data.error){
                danger_msg(data.message);
                return false;
            }else{
                success_msg(data.message);
                location.reload();
                return false;
            }
        }
    });
    
});






//   function GenerateCertificate(){
    
//     $.ajax({
//         type: "POST",
//         url: "/GetStudentDataByCertificates",
//         data: "data",
//         dataType: "JSON",
//         success: function (data) {
//             $.each(data, function (i, key) { 
//                  $("#CandidateName").text(key.name);
//                  $("#CourseName").text(key.title);
//             });
//             window.open("/GenerateCertificate");
//         }
//     });

//   }

$(document).on("click", "#LoadmoreCandidates", function(){
    var CandidateId = $(this).data('id');


    $.ajax({
        type: "POST",
        url: "/LoadmoreCandidates",
        data: {CandidateId: CandidateId},
        dataType: "text",
        beforeSend: function(){
            $("#ApplyBtnLoader").show();
        },
        success: function (data) {
            if(data != ''){
                console.log(data);
                $('#remove_candidate_load_more').remove();
                $("#SearchCandidatesResults").append(data);
            }else{
                console.log(data);
                $("#LoadmoreCandidates").html("No candidates are found...");
                return false;
            }
        },
        complete:function(data){
            $("#ApplyBtnLoader").hide();
        }
    });
    
});



function SalaryType(){
    var SalaryType = $("#salary_type").val();
    if(SalaryType == 1){
        $("#salary_min").val("");
        $("#salary_max").val("");
        $("#salary_min").attr("maxlength", 5);
        $("#salary_max").attr("maxlength", 5);
    }else{
        $("#salary_min").val("");
        $("#salary_max").val("");
        $("#salary_min").attr("maxlength", 6);
        $("#salary_max").attr("maxlength", 6);
    }
}   



$("#salary_min").keypress(function (e) {
    //if the letter is not digit then display error and don't type anything
    if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
       //display error message
    //    $("#errmsg").html("Digits Only").show().fadeOut("slow");
              return false;
   }
  });

  $("#salary_max").keypress(function (e) {
    //if the letter is not digit then display error and don't type anything
    if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
       //display error message
    //    $("#errmsg").html("Digits Only").show().fadeOut("slow");
              return false;
   }
  });


  $(document).on("click", "#UpdateEmployerPassword", function(){
    var OldPassword = $("#old_password").val();
    var NewPassword = $("#new_password").val();
    var VerifyPassword = $("#verify_password").val();

    var CandidatePassword = {
        OldPassword: OldPassword,
        NewPassword: NewPassword,
        VerifyPassword: VerifyPassword
    }

    $.ajax({
        type: "POST",
        url: "/UpdateEmployerPassword",
        data: CandidatePassword,
        dataType: "JSON",
        success: function (data) {
            if(data.error){
                danger_msg(data.message);
                return false;
            }else{
                success_msg(data.message);
                return false;
            }
        }
    });

});


function success_msg(data){
    $.notify({
        title: '<strong>'+ data +'</strong>',
        message: ''
    },{
        type: 'success'
    });
}

function danger_msg(data){
    $.notify({
        title: '<strong>'+ data +'</strong>',
        message: ''
    },{
        type: 'danger'
    });
}


