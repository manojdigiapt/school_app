// HOME_URL = "/";
$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

$(document).on("click", "#ForgotEmployer", function(){
    $("#login_employer").hide();
    $("#EmployerEmailForm").show();
    $(".extra-login").hide();
});

$(document).on("click", "#BackToLoginEmployer", function(){
    $("#login_employer").show();
    $("#EmployerEmailForm").hide();
    $(".extra-login").show();
});


$(document).on("click", "#LoginEmployer", function(){
    var employerusername = $("#EmployerUserName").val();
    var employerpassword = $("#EmployerUserPassword").val();

    if(employerusername == ""){
        danger_toast_msg("Please type email address");
        $("#EmployerUserName").focus();
        return false;
    }else if(IsEmail(employerusername)==false){
        danger_toast_msg("Please check your email address");
        $('#EmployerUserName').focus();
        return false;
    }

    if(employerpassword == ""){
        danger_toast_msg("Please type password");
        $("#EmployerUserPassword").focus();
        return false;
    }

    var EmployerLogin = {
        employerusername: employerusername,
        employerpassword: employerpassword
    }

    $.ajax({
        type: "POST",
        url: "/Employer/EmployerLogin",
        data: EmployerLogin,
        dataType: "JSON",
        success: function (data) {
            if(data.error){
                danger_toast_msg(data.message);
                return false;
            }else{
                success_toast_msg(data.message);
                window.location.href="/Employer/Dashboard";
                return false;
            }
        }
    });
});

// $(document).on("click", "#SubmitEmployerForgotEmail", function(){
    

//     var employeremail = $("#EmployerForgotEmail").val();

//     if(employeremail == ""){
//         danger_toast_msg("Please type email address");
//         $("#EmployerForgotEmail").focus();
//         return false;
//     }else if(IsEmail(employeremail)==false){
//         danger_toast_msg("Please check your email address");
//         $('#EmployerForgotEmail').focus();
//         return false;
//     }


//     var EmployerData = {
//         employeremail: employeremail
//     }

//     $.ajax({
//         type: "POST",
//         url: "/Employer/CheckEmployerForgotEmail",
//         data: EmployerData,
//         dataType: "JSON",
//         success: function (data) {
//             if(data.error){
//                 danger_toast_msg(data.message);
//                 return false;
//             }else{
//                 success_msg(data.message);
//                 return false;
//             }
//         }
//     });
// });

$('#EmployerPhone').keypress(function (event) {
    var keycode = event.which;
    if (!(event.shiftKey == false && (keycode == 46 || keycode == 8 || keycode == 37 || keycode == 39 || (keycode >= 48 && keycode <= 57)))) {
        event.preventDefault();
    }
});

$(document).on("click", "#SignupEmployer", function(){
    // var username = $("#UserName").val();
    var companyname = $("#CompanyName").val();
    var employerpassword = $("#EmployerPassword").val();
    var employeremail = $("#EmployerEmail").val();
    var employerphonenumber = $("#EmployerPhone").val();

    var MobileOTP = $("#EmployerMobileOTP").val();
    var EmailOTP = $("#EmployerEmailOTP").val();
    

    if(MobileOTP == ""){
        danger_toast_msg("Please enter your Mobile OTP");
        $("#EmployerMobileOTP").focus();
        return false;
    }

    if(EmailOTP == ""){
        danger_toast_msg("Please enter your Email OTP");
        $("#EmployerEmailOTP").focus();
        return false;
    }

    var EmployerData = {
        companyname: companyname,
        employerpassword: employerpassword, 
        employeremail: employeremail,
        employerphonenumber: employerphonenumber,
        MobileOTP: MobileOTP,
        EmailOTP: EmailOTP
    }
    $.ajax({
        type: "POST",
        url: "/Employer/InsertEmployer",
        data: EmployerData,
        dataType: "JSON",
        success: function (data) {
            if(data.error){
                danger_toast_msg(data.message);
                return false;
            }else{
                success_toast_msg(data.message);
                window.location.href="/Employer/Profile";
                return false;
            }
        }
    });
});



$(document).on("click", "#SignupEmployerOtp", function(){
    
    var companyname = $("#CompanyName").val();
    var employerpassword = $("#EmployerPassword").val();
    var employeremail = $("#EmployerEmail").val();
    var employerphonenumber = $("#EmployerPhone").val();

    if(companyname == ""){
        danger_toast_msg("Please type company name");
        $("#CompanyName").focus();
        return false;
    }

    if(employerpassword == ""){
        danger_toast_msg("Please type password");
        $("#EmployerPassword").focus();
        return false;
    }

    if(employeremail == ""){
        danger_toast_msg("Please type email address");
        $("#EmployerEmail").focus();
        return false;
    }else if(IsEmail(employeremail)==false){
        danger_toast_msg("Please check your email address");
        $('#EmployerEmail').focus();
        return false;
    }

    if(employerphonenumber == ""){
        danger_toast_msg("Please type phone number");
        $("#EmployerPhone").focus();
        return false;
    }

    var EmployerData = {
        companyname: companyname,
        employerpassword: employerpassword, 
        employeremail: employeremail,
        employerphonenumber: employerphonenumber
    }


    $.ajax({
        type: "POST",
        url: "/SendEmployerOtp",
        data: EmployerData,
        dataType: "JSON",
        beforeSend: function(){
            // Show image container
            $(".extra-login").hide();
            $("#register_employer").hide();
            $("#EmployerOtpLoader").show();
        },
        success: function (data) {
            if(data.error){
                $("#register_employer").show();
                danger_toast_msg(data.message);
                return false;
            }else{
                $("#register_employer").hide();
                $("#EmployerOtpLoader").show();
                // $(".extra-login").hide();

                $("#CancelEmployer").addClass("register_form");
                $("#CancelEmployer").removeClass("register_btn_clr");
                $("#EmployerOTP").show();
                $("#EmployerShowOtp").text(data.MobileOtp);
                return false;
            }
        },
        complete:function(data){
        // Hide image container
        // $(".extra-login").show();
            $("#EmployerOtpLoader").hide();
        }
    });
});




$(document).on("click", "#SubmitEmployerForgotPassword", function(){
    
    var employeremail = $("#EmployerForgotEmail").val();

    if(employeremail == ""){
        danger_toast_msg("Please type email address");
        $("#EmployerForgotEmail").focus();
        return false;
    }else if(IsEmail(employeremail)==false){
        danger_toast_msg("Please check your email address");
        $('#EmployerForgotEmail').focus();
        return false;
    }


    var EmployerData = {
        employeremail: employeremail
    }

    $.ajax({
        type: "POST",
        url: "/Employer/CheckForgotEmail",
        data: EmployerData,
        dataType: "JSON",
        beforeSend: function(){
            $("#ForgotEmailEmployerLoader").show();
        },
        success: function (data) {
            if(data.error){
                danger_toast_msg(data.message);
                return false;
            }else{
                success_toast_msg(data.message);
                return false;
            }
        },
        complete: function(){
            $("#ForgotEmailEmployerLoader").hide();
        }
    });
});


$(document).on("click", "#ResetEmployerPassword", function(){
    var id = $("#id").val();
    var new_password = $("#NewPassword").val();
    var confirm_password = $("#ConfirmPassword").val();

    if(new_password == ""){
        danger_toast_msg("Please type new password");
        $("#NewPassword").focus();
        return false;
    }
    if(confirm_password == ""){
        danger_toast_msg("Please type confirm password");
        $("#ConfirmPassword").focus();
        return false;
    }


    var CandidateData = {
        id: id,
        new_password: new_password,
        confirm_password: confirm_password
    }

    $.ajax({
        type: "POST",
        url: "/Employer/ResetPassword",
        data: CandidateData,
        dataType: "JSON",
        success: function (data) {
            if(data.error){
                danger_toast_msg(data.message);
                return false;
            }else{
                success_toast_msg(data.message);
                return false;
            }
        }
    });
});



// Resend OTP

$(document).on("click", "#ResendOTP", function(){
    var employerphonenumber = $("#EmployerPhone").val();

    $.ajax({
        type: "POST",
        url: "/ResendLink",
        data: {employerphonenumber: employerphonenumber},
        dataType: "JSON",
        beforeSend: function(){
            // Show image container
            // $(".extra-login").hide();
            $("#ResendOTP").hide();
            $("#EmployerResendOtpLoader").show();
            // return false;
        },
        success: function (data) {
            if(data.error){
                danger_toast_msg(data.message);
                return false;
            }else{
                success_toast_msg(data.message);
                return false;
            }
        },
        complete:function(data){
        // Hide image container
            $("#ResendOTP").show();
            $("#EmployerResendOtpLoader").hide();
        }
    });
});

$(document).on("click", "#ResendEmailOTP", function(){
    var employeremail = $("#EmployerEmail").val();

    $.ajax({
        type: "POST",
        url: "/ResendEmailLink",
        data: {employeremail: employeremail},
        dataType: "JSON",
        beforeSend: function(){
            // Show image container
            // $(".extra-login").hide();
            $("#ResendEmailOTP").hide();
            $("#EmployerResendEmailOtpLoader").show();
            // return false;
        },
        success: function (data) {
            if(data.error){
                danger_toast_msg(data.message);
                return false;
            }else{
                success_toast_msg(data.message);
                return false;
            }
        },
        complete:function(data){
            // Hide image container
            $("#ResendEmailOTP").show();
            $("#EmployerResendEmailOtpLoader").hide();
        }
    });
});


$(document).on("click", "#SearchHomeJobs", function(){
    var JobTitle = $("#JobTitle").val();
    var Locations = $("#Locations :selected").val();


    if(JobTitle == ""){
        danger_toast_msg("Please type jobtitle");
        $("#JobTitle").focus();
        return false;
    }

    if(Locations == "Location"){
        danger_toast_msg("Please type locations");
        $("#Locations").focus();
        return false;
    }


});



function success_toast_msg(data){
    var toast = new iqwerty.toast.Toast();
toast
.setText(data)
.stylize({
  background: '#37BFA7',
  color: 'white',
  'box-shadow': '0 0 50px rgba(0, 0, 0, .7)'
})
.show();
}



// function success_msg(data){
//     $.notify({
//         title: '<strong>'+ data +'</strong>',
//         message: ''
//     },{
//         type: 'success'
//     });
// }

function danger_toast_msg(data){
    var toast = new iqwerty.toast.Toast();
toast
.setText(data)
.stylize({
  background: 'red',
  color: 'white',
  'box-shadow': '0 0 50px rgba(0, 0, 0, .7)'
})
.show();
}

function IsEmail(email) {
    var regex = /^([a-zA-Z0-9_\.\-\+])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    if(!regex.test(email)) {
       return false;
    }else{
       return true;
    }
  }

  function validatePhone(txtPhone) {
    var a = document.getElementById(txtPhone).value;
    var filter = /[1-9]{1}[0-9]{9}/;
    if (filter.test(a)) {
        return true;
    }
    else {
        return false;
    }
}