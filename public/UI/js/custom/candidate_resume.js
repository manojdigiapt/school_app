$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});


$(document).on("click", "#AddEducation", function(){
    var board = $("#board").val();
    if((board == 1) || (board == 2)){
        var course_name = $("#board").val();
    }else{
        var course_name = $("#course_name").val();
    }

    var school_clg = $("#school_clg").val();
    var passed_out_year = $("#passed_out_year").val();
    var percentage = $("#percentage").val();
    
    // if(university == ""){
    //     danger_toast_msg("University should not empty");
    //     $("#university").focus();
    //     return false;
        
    // }

    // if(school_clg == ""){
    //     danger_toast_msg("School/Clg should not empty");
    //     $("#school_clg").focus();
    //     return false;
    // }

    // if(department == ""){
    //     danger_toast_msg("Department should not empty");
    //     $("#department").focus();
    //     return false;
    // }

    // if(from_date == ""){
    //     danger_toast_msg("From date should not empty");
    //     $("#from_date").focus();
    //     return false;
    // }

    // if(to_date == ""){
    //     danger_toast_msg("To date should not empty");
    //     $("#to_date").focus();
    //     return false;
    // }

    var EducationData = {
        board: board,
        course_name: course_name,
        school_clg: school_clg,
        passed_out_year: passed_out_year,
        percentage: percentage
    }

    $.ajax({
        type: "POST",
        url: "/Candidate/InsertEducation",
        data: EducationData,
        dataType: "JSON",
        success: function (data) {
            if(data.error){
                danger_toast_msg(data.message);
                return false;
            }else{
                success_toast_msg(data.message);
                location.reload();
                // return false;
            }
        }
    });
});

function EditEducation(id){
    $.ajax({
        type: "POST",
        url: "/Candidate/GetEducationById/"+id,
        data: {id: id},
        dataType: "JSON",
        success: function (data) {
            if(data.error){
                danger_toast_msg(data.message);
                return false;
            }else{
                $("#edu_id").val(data.id);

                if((data.title == 1) || (data.title == 2)){
                    $("#Edit_CourseNameRow").hide();
                }else{
                    $("#Edit_CourseNameRow").show();
                }
                $(".board option[value="+data.department+"]").attr('selected', 'selected');

                $("#Edit_board option[value="+data.title+"]").attr('selected', 'selected');
                // console.log(data.title);
                // $("#edit_course_name").val(data.title);

                $("#Edit_school_clg").val(data.name);

                $("#Edit_passed_out_year option[value="+data.from_year+"]").attr('selected', 'selected');
                $("#Edit_percentage").val(data.percentage);

                $("#EditEducationModal").modal("show");
                return false;
            }
        }
    });
}

$(document).on("click", "#UpdateEducation", function(){
    var id = $("#edu_id").val();
    var course_name = $("#Edit_board").val();
    // if((board == 1) || (board == 2)){
    //     var course_name = $("#Edit_board").val();
    // }else{
    //     var course_name = $("#edit_course_name").val();
    // }
    // alert(course_name);
    // return false;
    var school_clg = $("#Edit_school_clg").val();
    var passed_out_year = $("#Edit_passed_out_year").val();
    var percentage = $("#Edit_percentage").val();
    
    // if(university == ""){
    //     danger_toast_msg("University should not empty");
    //     $("#university").focus();
    //     return false;
        
    // }

    // if(school_clg == ""){
    //     danger_toast_msg("School/Clg should not empty");
    //     $("#school_clg").focus();
    //     return false;
    // }

    // if(department == ""){
    //     danger_toast_msg("Department should not empty");
    //     $("#department").focus();
    //     return false;
    // }

    // if(from_date == ""){
    //     danger_toast_msg("From date should not empty");
    //     $("#from_date").focus();
    //     return false;
    // }

    // if(to_date == ""){
    //     danger_toast_msg("To date should not empty");
    //     $("#to_date").focus();
    //     return false;
    // }

    var EducationData = {
        id: id,
        // board: board,
        course_name: course_name,
        school_clg: school_clg,
        passed_out_year: passed_out_year,
        percentage: percentage
    }

    $.ajax({
        type: "POST",
        url: "/Candidate/UpdateEducation",
        data: EducationData,
        dataType: "JSON",
        success: function (data) {
            if(data.error){
                danger_toast_msg(data.message);
                return false;
            }else{
                success_toast_msg(data.message);
                location.reload();
                // return false;
            }
        }
    });
});

function DeleteEducation(id){
    swal({
        title: "Are you sure?",
        text: "You will not be able to recover this imaginary file!",
        type: "warning",
        showCancelButton: true,
        confirmButtonClass: "btn-danger",
        confirmButtonText: "Yes, delete it!",
        cancelButtonText: "No",
        closeOnConfirm: false,
        closeOnCancel: true
      },
      function(isConfirm) {
        if (isConfirm) {
            $.ajax({
                type: "GET",
                url: "/Trainee/DeleteEducation/"+id,
                dataType: "JSON",
                success: function (data) {
                    if(data.error){
                        danger_toast_msg(data.message);
                        return false;
                    }else{
                        swal("Deleted!", "Data deleted successfully", "success");
                        location.reload();
                        return false;
                    }
                }
            });
        }
      });
}

// Experience

$(document).on("click", "#AddExperience", function(){
    var company_name = $("#company_name").val();
    var designation = $("#designation").val();
    var exp_from_year = $("#exp_from_year").val();
    var exp_from_month = $("#exp_from_month").val();
    var exp_to_year = $("#exp_to_year").val();
    var exp_to_month = $("#exp_to_month").val();
    var worked_till = $("#worked_till").val();
    var current_salary_month = $("#current_salary_month").val();
    // var current_salary_thousand = $("#current_salary_thousand").val();
    var notice_period = $("#notice_period").val();
    var description = $("#exp_description").val();

    if(company_name == ""){
        danger_toast_msg("Company name should not empty");
        $("#company_name").focus();
        return false;
    }

    if(designation == ""){
        danger_toast_msg("Designation name should not empty");
        $("#designation").focus();
        return false;
    }

    // if(exp_from_date == ""){
    //     danger_toast_msg("Experiece from date should not empty");
    //     $("#exp_from_date").focus();
    //     return false;
    // }

    // if(exp_to_date == ""){
    //     danger_toast_msg("Experiece to date should not empty");
    //     $("#exp_to_date").focus();
    //     return false;
    // }
    if(description == ""){
        danger_toast_msg("Description should not empty");
        $("#description").focus();
        return false;
    }

    var ExperienceData = {
        company_name: company_name,
        designation: designation,
        exp_from_year: exp_from_year,
        exp_from_month: exp_from_month,
        exp_to_year: exp_to_year,
        exp_to_month: exp_to_month,
        worked_till: worked_till,
        current_salary_month: current_salary_month,
        // current_salary_thousand: current_salary_thousand,
        notice_period: notice_period,
        description: description
    }

    $.ajax({
        type: "POST",
        url: "/Candidate/InsertExperience",
        data: ExperienceData,
        dataType: "JSON",
        success: function (data) {
            if(data.error){
                danger_toast_msg(data.message);
                return false;
            }else{
                success_toast_msg(data.message);
                location.reload();
                // return false;
            }
        }
    });
});

function EditWorkExperience(id){

    $.ajax({
        type: "POST",
        url: "/Candidate/GetExperienceById/"+id,
        data: {id: id},
        dataType: "JSON",
        success: function (data) {
            if(data.error){
                danger_toast_msg(data.message);
                return false;
            }else{
                $("#Edit_exp_id").val(data.id);

                $("#Edit_designation option[value="+data.department+"]").attr('selected', 'selected');

                $("#Edit_company_name").val(data.name);
                console.log(data.to_month);
                $("#Edit_exp_from_year option[value="+data.from_year+"]").attr('selected', 'selected');
                $("#Edit_exp_from_month option[value="+data.from_month+"]").attr('selected', 'selected');

                if(data.worked_till == 1){
                    $( "#Edit_yes" ).prop( "checked", true );

                    $("#Edit_WorkingTo").hide();
                    $("#Edit_WorkedTill").show();
                    $("#Edit_NoticePeriod").show();
                    $("#Edit_CurrentSalaryRow").show();
                }else{
                    $( "#Edit_no" ).prop( "checked", true );

                    $("#Edit_WorkingTo").show();
                    $("#Edit_WorkedTill").hide();
                    $("#Edit_NoticePeriod").hide();
                    $("#Edit_CurrentSalaryRow").hide();
                }
                
                $("#Edit_exp_to_year option[value="+data.to_year+"]").attr('selected', 'selected');
                $("#Edit_exp_to_month option[value="+data.to_month+"]").attr('selected', 'selected');

                $("#Edit_worked_till option[value="+data.worked_till+"]").attr('selected', 'selected');
                
                // $("#Edit_current_salary_month").val(data.current_salary_lacs);

                // $("#Edit_current_salary_thousand option[value="+data.current_salary_thousand+"]").attr('selected', 'selected');

                $("#Edit_exp_description").val(data.description);
                $("#edit_exp_to_date").val(data.to_date);

                $("#Edit_notice_period option[value="+data.notice_period+"]").attr('selected', 'selected');

                $("#EditWorkExperience").modal("show");
                return false;
            }
        }
    });
}




$(document).on("click", "#UpdateExperience", function(){
    var exp_id = $("#Edit_exp_id").val();
    var company_name = $("#Edit_company_name").val();
    var designation = $("#Edit_designation").val();
    var exp_from_year = $("#Edit_exp_from_year").val();
    var exp_from_month = $("#Edit_exp_from_month").val();
    var exp_to_year = $("#Edit_exp_to_year").val();
    var exp_to_month = $("#Edit_exp_to_month").val();
    var worked_till = $("#Edit_worked_till").val();
    var current_salary_month = $("#Edit_current_salary_month").val();
    // var current_salary_thousand = $("#Edit_current_salary_thousand").val();
    var notice_period = $("#Edit_notice_period").val();
    var description = $("#Edit_exp_description").val();

    // if(company_name == ""){
    //     danger_toast_msg("Company name should not empty");
    //     $("#company_name").focus();
    //     return false;
    // }

    // if(designation == ""){
    //     danger_toast_msg("Designation name should not empty");
    //     $("#designation").focus();
    //     return false;
    // }

    // // if(exp_from_date == ""){
    // //     danger_toast_msg("Experiece from date should not empty");
    // //     $("#exp_from_date").focus();
    // //     return false;
    // // }

    // // if(exp_to_date == ""){
    // //     danger_toast_msg("Experiece to date should not empty");
    // //     $("#exp_to_date").focus();
    // //     return false;
    // // }
    // if(description == ""){
    //     danger_toast_msg("Description should not empty");
    //     $("#description").focus();
    //     return false;
    // }

    var ExperienceData = {
        exp_id: exp_id,
        company_name: company_name,
        designation: designation,
        exp_from_year: exp_from_year,
        exp_from_month: exp_from_month,
        exp_to_year: exp_to_year,
        exp_to_month: exp_to_month,
        worked_till: worked_till,
        current_salary_month: current_salary_month,
        // current_salary_thousand: current_salary_thousand,
        notice_period: notice_period,
        description: description
    }

    $.ajax({
        type: "POST",
        url: "/Candidate/UpdateExperience",
        data: ExperienceData,
        dataType: "JSON",
        success: function (data) {
            if(data.error){
                danger_toast_msg(data.message);
                return false;
            }else{
                success_toast_msg(data.message);
                location.reload();
                // return false;
            }
        }
    });
});

function DeleteExperience(id){
    swal({
        title: "Are you sure?",
        text: "You will not be able to recover this imaginary file!",
        type: "warning",
        showCancelButton: true,
        confirmButtonClass: "btn-danger",
        confirmButtonText: "Yes, delete it!",
        cancelButtonText: "No",
        closeOnConfirm: false,
        closeOnCancel: true
      },
      function(isConfirm) {
        if (isConfirm) {
            $.ajax({
                type: "GET",
                url: "/Candidate/DeleteExperience/"+id,
                dataType: "JSON",
                success: function (data) {
                    if(data.error){
                        danger_toast_msg(data.message);
                        return false;
                    }else{
                        swal("Deleted!", "Data deleted successfully", "success");
                        location.reload();
                        return false;
                    }
                }
            });
        }
      });
}


// Skills


$(document).on("click", "#AddSkills", function(){
    var SkillId = $("#SkillId").val();
    var skill_name = $("#skill_name").val();
    // var percentage = $("#percentage").val();
    // var skill_name = new Array();
    // $('#skill_name').each(function(){
    //     skill_name.push($(this).val());
    //  });
    //  alert(skill_name);

    //  return false;
    var SkillData = {
        SkillId: SkillId,
        skill_name: skill_name
        
        // percentage: percentage
    }

    $.ajax({
        type: "POST",
        url: "/Candidate/InsertSkills",
        data: SkillData,
        dataType: "JSON",
        success: function (data) {
            if(data.error){
                danger_toast_msg(data.message);
                return false;
            }else{
                success_msg(data.message);
                location.reload();
                return false;
            }
        }
    });
});




// function CheckSkills(){
//     var skill_name = $("#skill_name").val();
//     $.ajax({
//         type: "POST",
//         url: "/Candidate/CheckSkills",
//         data: {skill_name: skill_name},
//         dataType: "JSON",
//         success: function (data) {
//             // console.log(data);
            
//         }
//     });
// }

function BrowseCandidateCV(elemId) {
    var elem = document.getElementById(elemId);
    if(elem && document.createEvent) {
       var evt = document.createEvent("MouseEvents");
       evt.initEvent("click", true, false);
       elem.dispatchEvent(evt);
    }

    // readURL(this);
 }

 $("#cv").change(function() {

    var cv_id = $("#cv_id").val();
    var CVData  = new FormData();

    CVData.append('cv_id', cv_id);
    CVData.append('cv', $('#cv')[0].files[0]);

    // console.log(CVData);
    // return false;
    $.ajax({
        type: "POST",
        url: "/Candidate/InsertCV",
        data: CVData,
        dataType: "JSON",
        contentType: false,
        processData: false,
        success: function (data) {
            if(data.error){
                danger_toast_msg(data.message);
                return false;
            }else{
                success_toast_msg(data.message);
                location.reload();
                return false;
            }
        }
    });
 });




//  Trainee CV
function BrowseTraineeCV(elemId) {
    var elem = document.getElementById(elemId);
    if(elem && document.createEvent) {
       var evt = document.createEvent("MouseEvents");
       evt.initEvent("click", true, false);
       elem.dispatchEvent(evt);
    }

    // readURL(this);
 }

 $("#trainee_cv").change(function() {

    var cv_id = $("#cv_id").val();
    var CVData  = new FormData();

    CVData.append('cv_id', cv_id);
    CVData.append('cv', $('#trainee_cv')[0].files[0]);

    // console.log(CVData);
    // return false;
    $.ajax({
        type: "POST",
        url: "/Trainee/InsertCV",
        data: CVData,
        dataType: "JSON",
        contentType: false,
        processData: false,
        success: function (data) {
            if(data.error){
                danger_toast_msg(data.message);
                return false;
            }else{
                success_toast_msg(data.message);
                location.reload();
                return false;
            }
        }
    });
 });


function success_msg(data){
    $.notify({
        title: '<strong>'+ data +'</strong>',
        message: ''
    },{
        type: 'success'
    });
}

function danger_toast_msg(data){
    $.notify({
        title: '<strong>'+ data +'</strong>',
        message: ''
    },{
        type: 'danger'
    });
}



$(document).ready(function () {
    var date=new Date();
    $('input[type="checkbox"]').click(function(){
        if($(this).prop("checked") == true){
            var val=date.getFullYear()+"-"+date.getDate()+"-"+(date.getMonth()+1);
		    $("#exp_to_date").val(val);
        }
        else if($(this).prop("checked") == false){
            $("#exp_to_date").val("");
        }
    });
});


function EditResumeHeadline(id){
    $.ajax({
        type: "POST",
        url: "/Candidate/ResumeHeadline/"+id,
        data: {id: id},
        dataType: "JSON",
        success: function (data) {
            if(data.error){
                danger_toast_msg(data.message);
                return false;
            }else{
                CKEDITOR.instances['description'].setData(data.title);

                $("#ResumeHeadline").modal("show");
            }
        }
    });
}

function AddOrEditCareerProfile(){

    $("#CareerProfile").modal("show");
    return false;
}

$(document).on("click", "#UpdateCareerProfile", function(){
    // var industry = $("#industry").val();
    // var functional_area = $("#functional_area").val();
    // var role = $("#role").val();
    var skills = $("#SkillNameInput").val();

    var CareerData = {
        // industry: industry,
        // functional_area: functional_area,
        // role: role,
        skills: skills
    }

    $.ajax({
        type: "POST",
        url: "/Candidate/UpdateCareerProfile",
        data: CareerData,
        dataType: "JSON",
        success: function (data) {
            if(data.error){
                danger_toast_msg(data.message);
                return false;
            }else{
                success_toast_msg(data.message);
                // $("#CareerProfile").modal("hide");
                // $("#CareerProfileColumn").load(location.href + " #CareerProfileColumn");
                location.reload();
                return false;
            }
        }
    });
});


function InsertOrUpdateCV(){
    var cv_id = $("#cv_id").val();
    var resume_title = CKEDITOR.instances['description'].getData();
    // var percentage = $("#percentage").val();

    var CVData = {
        cv_id: cv_id,
        resume_title: resume_title
    }


    $.ajax({
        type: "POST",
        url: "/Candidate/InsertOrUpdateCVTitle",
        data: CVData,
        dataType: "JSON",
        success: function (data) {
            if(data.error){
                danger_toast_msg(data.message);
                return false;
            }else{
                success_toast_msg(data.message);
                return false;
            }
        }
    });
}


function CheckBoard(){
    var board = $("#board").val();

    if((board == '10th') || (board == '12th')){
        // $(".course_name").attr("id", "course_name");
        // $(".board").attr("id", "board");
        $("#CourseNameRow").hide();
    }else{
        // $("#course_name").attr("id", "board");
        // $("#board").attr("id", "course_name");
        $("#CourseNameRow").show();
    }
}

function Edit_CheckBoard(){
    var board = $("#Edit_board").val();

    if((board == 1) || (board == 2)){
        // $(".course_name").attr("id", "course_name");
        // $(".board").attr("id", "board");
        $("#Edit_CourseNameRow").hide();
    }else{
        // $("#course_name").attr("id", "board");
        // $("#board").attr("id", "course_name");
        $("#Edit_CourseNameRow").show();
    }
}








// First Register Candidate
function BrowseFirstRegisterCandidateCV(elemId) {
    var elem = document.getElementById(elemId);
    if(elem && document.createEvent) {
       var evt = document.createEvent("MouseEvents");
       evt.initEvent("click", true, false);
       elem.dispatchEvent(evt);
       
    }
    // readURL(this);
 }


 $("#first_cv").change(function() {

    $("#myProgress").show();

    var elem = document.getElementById("myBar");   
    var width = 0;
    var id = setInterval(frame, 30);
    function frame() {
      if (width >= 100) {
        clearInterval(id);
      } else {
        width++; 
        elem.style.width = width + '%'; 
        elem.innerHTML = width * 1  + '%';
      }
    }

 });


$(document).on("click", ".Next_Btn_Resume", function(){
    
    var cv_id = $("#cv_id").val();
    var CVData  = new FormData();

    CVData.append('cv_id', cv_id);
    CVData.append('cv', $('#first_cv')[0].files[0]);

    // console.log(CVData);
    // return false;
    $.ajax({
        type: "POST",
        url: "/Candidate/InsertCV",
        data: CVData,
        dataType: "JSON",
        contentType: false,
        processData: false,
        success: function (data) {
            if(data.error){
                danger_toast_msg(data.message);
                return false;
            }else{
                success_toast_msg(data.message);
                return false;
            }
        }
    });
 });



 $(document).on("click", ".UpdateFirstCareerProfile", function(){
    // var industry = $("#industry").val();
    // var functional_area = $("#functional_area").val();
    // var role = $("#role").val();
    var skills = $("#SkillNameInput").val();

    var CareerData = {
        // industry: industry,
        // functional_area: functional_area,
        // role: role,
        skills: skills
    }

    $.ajax({
        type: "POST",
        url: "/Candidate/UpdateCareerProfile",
        data: CareerData,
        dataType: "JSON",
        success: function (data) {
            if(data.error){
                danger_toast_msg(data.message);
                return false;
            }else{
                success_toast_msg(data.message);
                // $("#CareerProfile").modal("hide");
                // $("#CareerProfileColumn").load(location.href + " #CareerProfileColumn");
                // location.reload();
                return false;
            }
        }
    });
});


$(document).on("click", ".AddExperience", function(){
    var company_name = $("#company_name").val();
    var designation = $("#designation :selected").val();
    var exp_from_year = $("#exp_from_year :selected").val();
    var exp_from_month = $("#exp_from_month :selected").val();
    var exp_to_year = $("#exp_to_year :selected").val();
    var exp_to_month = $("#exp_to_month :selected").val();
    var worked_till = $("#worked_till").val();
    var current_salary_month = $("#current_salary_month").val();
    // var current_salary_thousand = $("#current_salary_thousand").val();
    var notice_period = $("#notice_period :selected").val();
    var description = $("#exp_description").val();

    var yes = $('input[name=choose]#yes:checked').val();
    var no = $('input[name=choose]#no:checked').val();

    
    

    if(designation == "Select Title"){
        danger_toast_msg("Designation name should not empty");
        $("#designation").focus();
        return false;
    }

    if(company_name == ""){
        danger_toast_msg("Company name should not empty");
        $("#company_name").focus();
        return false;
    }

    

    if(exp_from_year == "Select Year"){
        danger_toast_msg("Experience from year should not empty");
        $("#exp_from_year").focus();
        return false;
    }

    if(exp_from_month == "Select Month"){
        danger_toast_msg("Experience from month should not empty");
        $("#exp_from_month").focus();
        return false;
    }

    
    if(yes == "on"){
        if(description == ""){
            danger_toast_msg("Description should not empty");
            $("#exp_description").focus();
            return false;
        }

        if(notice_period == "Please select"){
            danger_toast_msg("Notice period should not empty");
            $("#notice_period").focus();
            return false;
        }
    }else if(no == "on"){
        if(exp_to_year == "Select Year"){
            danger_toast_msg("Experience to year should not empty");
            $("#exp_to_year").focus();
            return false;
        }
    
        if(exp_to_month == "Select Month"){
            danger_toast_msg("Experience to month should not empty");
            $("#exp_to_month").focus();
            return false;
        }

        if(description == ""){
            danger_toast_msg("Description should not empty");
            $("#exp_description").focus();
            return false;
        }
    }

    

    if(yes == "on"){
        var ExperienceData = {
            company_name: company_name,
            designation: designation,
            exp_from_year: exp_from_year,
            exp_from_month: exp_from_month,
            worked_till: worked_till,
            current_salary_month: current_salary_month,
            // current_salary_thousand: current_salary_thousand,
            notice_period: notice_period,
            description: description
        }
    }else if(no == "on"){
        var ExperienceData = {
            company_name: company_name,
            designation: designation,
            exp_from_year: exp_from_year,
            exp_from_month: exp_from_month,
            exp_to_year: exp_to_year,
            exp_to_month: exp_to_month,
            worked_till: worked_till,
            current_salary_month: current_salary_month,
            // current_salary_thousand: current_salary_thousand,
            description: description
        }
    }
    

    

    

    $.ajax({
        type: "POST",
        url: "/Candidate/InsertExperience",
        data: ExperienceData,
        dataType: "JSON",
        success: function (data) {
            if(data.error){
                danger_toast_msg(data.message);
                return false;
            }else{
                success_toast_msg(data.message);
                // $(this).find('.nextBtn').trigger('click');
                // $(".nextBtn").trigger('click');
                // GoToStep4();
                // location.reload();
                $("#ProfileCompletion").load();
                $("#ExperienceDetails").load(location.href + " #ExperienceDetails");
                return false;
            }
        }
    });
});



$(document).on("click", ".AddEducation", function(){
    var board = $("#board").val();
    if((board == 1) || (board == 2)){
        var course_name = $("#board").val();
    }else{
        var course_name = $("#course_name").val();
    }

    var school_clg = $("#school_clg").val();
    var passed_out_year = $("#passed_out_year").val();
    var percentage = $("#percentage").val();
    
    // if(university == ""){
    //     danger_toast_msg("University should not empty");
    //     $("#university").focus();
    //     return false;
        
    // }

    // if(school_clg == ""){
    //     danger_toast_msg("School/Clg should not empty");
    //     $("#school_clg").focus();
    //     return false;
    // }

    // if(department == ""){
    //     danger_toast_msg("Department should not empty");
    //     $("#department").focus();
    //     return false;
    // }

    // if(from_date == ""){
    //     danger_toast_msg("From date should not empty");
    //     $("#from_date").focus();
    //     return false;
    // }

    // if(to_date == ""){
    //     danger_toast_msg("To date should not empty");
    //     $("#to_date").focus();
    //     return false;
    // }

    var EducationData = {
        board: board,
        course_name: course_name,
        school_clg: school_clg,
        passed_out_year: passed_out_year,
        percentage: percentage
    }

    $.ajax({
        type: "POST",
        url: "/Candidate/InsertEducation",
        data: EducationData,
        dataType: "JSON",
        success: function (data) {
            if(data.error){
                danger_toast_msg(data.message);
                return false;
            }else{
                success_toast_msg(data.message);
                $("#EducationDetails").load(location.href + " #EducationDetails");
                // location.reload();
                return false;
            }
        }
    });
});



$(document).on("click", ".UpdateCandidatePersonalDetails", function(){
    var dob = $("#Edit_dob").val();
    var gender = $("#Edit_gender").val();
    var marital_status = $("#Edit_marital_status").val();
    var father_name = $("#Edit_father_name").val();
    var address = $("#Edit_address").val();
    var state = $("#Edit_state").val();
    var city = $("#Edit_city").val();
    var zipcode = $("#Edit_zipcode").val();

    var PersonalDetails = {
        dob: dob,
        gender: gender,
        marital_status: marital_status,
        father_name: father_name,
        address: address,
        state: state,
        city: city,
        zipcode: zipcode
    }

    $.ajax({
        type: "POST",
        url: "/Candidate/UpdatePersonalDetails",
        data: PersonalDetails,
        dataType: "JSON",
        success: function (data) {
            if(data.error){
                danger_toast_msg(data.message);
                return false;
            }else{
                success_toast_msg(data.message);
                window.location.href="/Candidate/Dashboard";
                // location.reload();
                return false;
            }
        }
    });

});