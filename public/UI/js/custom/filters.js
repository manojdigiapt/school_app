// HOME_URL = "/";
$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});





function filterSystem(minPrice, maxPrice) {
$("#computers div.system").hide().filter(function() {
    var price = parseInt($(this).data("price"), 10);
    return price >= minPrice && price <= maxPrice;
}).show();
}


// Jobs

$(document).on("change", ".CheckJobFilters", function(){
// function CheckJobFilters(){
    var date_posted = $(this).val();
    var location = $('.Location:checked').val();
    // var qualification = $('.Qualification:checked').val();
    var JobType = $('.JobType:checked').val();
    var Industry = $('.Industry:checked').val();

    // qualification = [];

    // $('input[name="Qualification"]:checked').each(function()
    // {
    //     qualification.push($(this).val());
    // });

    location = [];

    $('input[name="Location"]:checked').each(function()
    {
        location.push($(this).val());
    });

    // JobType = [];

    // $('input[name="JobType"]:checked').each(function()
    // {
    //     JobType.push($(this).val());
    // });

    Title = [];

    $('input[name="Title"]:checked').each(function()
    {
        Title.push($(this).val());
    });

    var FiltersData = {
        date_posted: date_posted,
        location: location,
        // qualification: qualification,
        // JobType: JobType,
        Title: Title
    }

    $.ajax({
        type: "POST",
        url: "/FilterJobs",
        data: FiltersData,
        dataType: "html",
        beforeSend: function(){
            // Show image container
            $("#AjaxLoader").show();
            $("#SearchJobsResults").hide();
            // return false;
        },
        success: function (data) {
            if(data.error){
                danger_msg(data.message);
                return false;
            }else{
                $("#SearchJobsResults").html("");
                $("#SearchJobsResults").append(data);
                console.log(data);
                return false;
            }
        },
        complete:function(data){
        // Hide image container
            $("#AjaxLoader").hide();
            $("#SearchJobsResults").show();
        }
    });
// }
});

$(document).on("change", ".CheckDatePostedJobFilters", function(){
    // function CheckJobFilters(){
        var date_posted = $(this).val();
    
    
        var FiltersData = {
            date_posted: date_posted
        }
    
        $.ajax({
            type: "POST",
            url: "/FilterDatePostedJobs",
            data: FiltersData,
            dataType: "html",
            beforeSend: function(){
                // Show image container
                $("#AjaxLoader").show();
                $("#SearchJobsResults").hide();
                // return false;
            },
            success: function (data) {
                if(data){
                    $("#SearchJobsResults").html("");
                    $("#SearchJobsResults").append(data);
                    
                    // $("#JobsCount").text(data.length)/1000;
                    return false;
                }else{
                    // $("#JobsCount").text('0');
                    $("#SearchJobsResults").html("No results found...");
                    return false;
                }
            },
            complete:function(data){
            // Hide image container
                $("#AjaxLoader").hide();
                $("#SearchJobsResults").show();
            }
        });
    // }
    });



$(document).ready(function () {

	var job_type = [];
	// var role = [];
	// var job_title = $('#job_title').val();
    // Listen for 'change' event, so this triggers when the user clicks on the checkboxes labels
    $('.CheckJobType').on('change', function (e) {

    	e.preventDefault();
        job_type = new Array();

        $('input[name="CheckJobType[]"]:checked').each(function()
        {
        	job_type.push($(this).val());
        	// job_role.push($('#role').val());
            // alert(JSON.stringify(job_type));
        });
        // alert(JSON.stringify(job_type));

        $.ajax({
            type: "POST",
            url: "/FilterJobType",
            data: {job_type: job_type},
            dataType: "html",
            beforeSend: function(){
                // Show image container
                $("#AjaxLoader").show();
            },
            success: function (data) {
                if(data.error){
                    console.log(data);
                    return false;
                }else{
                    $("#SearchJobsResults").html(data);
                    return false;
                }
            },
            complete:function(data){
                // Hide image container
                $("#AjaxLoader").hide();
            }
        });
	// parent.location.hash = "jobs/search";

});

});




$(document).ready(function () {

	var city = [];
	// var role = [];
	// var job_title = $('#job_title').val();
    // Listen for 'change' event, so this triggers when the user clicks on the checkboxes labels
    $('.CheckCity').on('change', function (e) {

    	e.preventDefault();
        city = new Array();

        $('input[name="CheckCity[]"]:checked').each(function()
        {
        	city.push($(this).val());
        	// job_role.push($('#role').val());
            // alert(JSON.stringify(job_type));
        });
        // alert(JSON.stringify(job_type));

        $.ajax({
            type: "POST",
            url: "/FilterDesiredLocation",
            data: {city: city},
            dataType: "JSON",
            beforeSend: function(){
                // Show image container
                $("#AjaxLoader").show();
            },
            success: function (data) {
                if(data.error){
                    console.log(data);
                    return false;
                }else{
                    $("#SearchJobsResults").html(data);
                    return false;
                }
            },
            complete:function(data){
                // Hide image container
                $("#AjaxLoader").hide();
            }
        });
	// parent.location.hash = "jobs/search";

});

});



$(document).ready(function () {

	var industry = [];
	// var role = [];
	// var job_title = $('#job_title').val();
    // Listen for 'change' event, so this triggers when the user clicks on the checkboxes labels
    $('.CheckIndustry').on('change', function (e) {

    	e.preventDefault();
        industry = new Array();

        $('input[name="CheckIndustry[]"]:checked').each(function()
        {
        	industry.push($(this).val());
        	// job_role.push($('#role').val());
            // alert(JSON.stringify(job_type));
        });
        // alert(JSON.stringify(job_type));

        $.ajax({
            type: "POST",
            url: "/FilterIndustry",
            data: {industry: industry},
            dataType: "JSON",
            beforeSend: function(){
                // Show image container
                $("#AjaxLoader").show();
            },
            success: function (data) {
                if(data.error){
                    console.log(data);
                    return false;
                }else{
                    $("#SearchJobsResults").html(data);
                    return false;
                }
            },
            complete:function(data){
                // Hide image container
                $("#AjaxLoader").hide();
            }
        });
	// parent.location.hash = "jobs/search";

});

});







// Candidates

$(document).on("change", ".CheckFilterCandidate", function(){
    var date_posted = $(this).val();
    // var job_type = $('.CheckJobType:checked').val();

    var FiltersData = {
        date_posted: date_posted
    }

    $.ajax({
        type: "POST",
        url: "/FilterCandidates",
        data: FiltersData,
        dataType: "JSON",
        beforeSend: function(){
            // Show image container
            $("#AjaxLoader").show();
        },
        success: function (data) {
            if(data.error){
                danger_msg(data.message);
                return false;
            }else{
                $("#SearchCandidatesResults").html(data);
                console.log(data);
                return false;
            }
        },
        complete:function(data){
        // Hide image container
            $("#AjaxLoader").hide();
        }
    });
});



$(document).ready(function () {

	var city = [];
	// var role = [];
	// var job_title = $('#job_title').val();
    // Listen for 'change' event, so this triggers when the user clicks on the checkboxes labels
    $('.CheckCandidateCity').on('change', function (e) {

    	e.preventDefault();
        city = new Array();

        $('input[name="CheckCandidateCity[]"]:checked').each(function()
        {
        	city.push($(this).val());
        	// job_role.push($('#role').val());
            // alert(JSON.stringify(job_type));
        });
        // alert(JSON.stringify(job_type));

        $.ajax({
            type: "POST",
            url: "/FilterCandidateDesiredLocation",
            data: {city: city},
            dataType: "JSON",
            beforeSend: function(){
                // Show image container
                $("#AjaxLoader").show();
            },
            success: function (data) {
                if(data.error){
                    console.log(data);
                    return false;
                }else{
                    $("#SearchCandidatesResults").html(data);
                    return false;
                }
            },
            complete:function(data){
                // Hide image container
                $("#AjaxLoader").hide();
            }
        });
	// parent.location.hash = "jobs/search";

});

});



$(document).ready(function () {

	var industry = [];
	// var role = [];
	// var job_title = $('#job_title').val();
    // Listen for 'change' event, so this triggers when the user clicks on the checkboxes labels
    $('.CheckCandidateIndustry').on('change', function (e) {

    	e.preventDefault();
        industry = new Array();

        $('input[name="CheckCandidateIndustry[]"]:checked').each(function()
        {
        	industry.push($(this).val());
        	// job_role.push($('#role').val());
            // alert(JSON.stringify(job_type));
        });
        // alert(JSON.stringify(job_type));

        $.ajax({
            type: "POST",
            url: "/FilterCandidateIndustry",
            data: {industry: industry},
            dataType: "JSON",
            beforeSend: function(){
                // Show image container
                $("#AjaxLoader").show();
            },
            success: function (data) {
                if(data.error){
                    console.log(data);
                    return false;
                }else{
                    $("#SearchCandidatesResults").html(data);
                    return false;
                }
            },
            complete:function(data){
                // Hide image container
                $("#AjaxLoader").hide();
            }
        });
	// parent.location.hash = "jobs/search";

});

});


$(document).ready(function () {

	var employee_type = [];
	// var role = [];
	// var job_title = $('#job_title').val();
    // Listen for 'change' event, so this triggers when the user clicks on the checkboxes labels
    $('.CheckEmployeeType').on('change', function (e) {

    	e.preventDefault();
        employee_type = new Array();

        $('input[name="CheckEmployeeType[]"]:checked').each(function()
        {
        	employee_type.push($(this).val());
        	// job_role.push($('#role').val());
            // alert(JSON.stringify(job_type));
        });
        // alert(JSON.stringify(job_type));

        $.ajax({
            type: "POST",
            url: "/FilterEmployeeType",
            data: {employee_type: employee_type},
            dataType: "JSON",
            beforeSend: function(){
                // Show image container
                $("#AjaxLoader").show();
            },
            success: function (data) {
                if(data.error){
                    console.log(data);
                    return false;
                }else{
                    $("#SearchCandidatesResults").html(data);
                    return false;
                }
            },
            complete:function(data){
                // Hide image container
                $("#AjaxLoader").hide();
            }
        });
	// parent.location.hash = "jobs/search";

});

});


$(document).ready(function () {

	var salary_type = [];
	// var role = [];
	// var job_title = $('#job_title').val();
    // Listen for 'change' event, so this triggers when the user clicks on the checkboxes labels
    $('.CheckSalary').on('change', function (e) {

    	e.preventDefault();
        salary_type = new Array();

        $('input[name="CheckSalary[]"]:checked').each(function()
        {
        	salary_type.push($(this).val());
        	// job_role.push($('#role').val());
            // alert(JSON.stringify(job_type));
        });
        // alert(JSON.stringify(job_type));

        $.ajax({
            type: "POST",
            url: "/FilterSalaryType",
            data: {salary_type: salary_type},
            dataType: "JSON",
            beforeSend: function(){
                // Show image container
                $("#AjaxLoader").show();
            },
            success: function (data) {
                if(data.error){
                    console.log(data);
                    return false;
                }else{
                    $("#SearchCandidatesResults").html(data);
                    return false;
                }
            },
            complete:function(data){
                // Hide image container
                $("#AjaxLoader").hide();
            }
        });
	// parent.location.hash = "jobs/search";

});

});




// Trainings

$(document).on("change", ".CheckFilterTrainings", function(){
    // function CheckJobFilters(){
    var date_posted = $(this).val();
    // var qualification = $('.Qualification:checked').val();
    // var Duration = $('.JobType:checked').val();
    // var Spend = $('.Industry:checked').val();

    var location = [];

    $('input[name="Location"]:checked').each(function()
    {
        location.push($(this).val());
    });

    var Duration = [];

    $('input[name="Duration"]:checked').each(function()
    {
        Duration.push($(this).val());
    });

    var Stipend = [];

    $('input[name="Stipend"]:checked').each(function()
    {
        Stipend.push($(this).val());
    });

    var FiltersData = {
        date_posted: date_posted,
        location: location,
        Duration: Duration,
        Stipend: Stipend
    }

    $.ajax({
        type: "POST",
        url: "/FilterTrainings",
        data: FiltersData,
        dataType: "html",
        beforeSend: function(){
            // Show image container
            $("#AjaxLoader").show();
            $("#SearchTrainingResults").hide();
            // return false
        },
        success: function (data) {
            if(data.error){
                danger_msg(data.message);
                return false;
            }else{
                $("#SearchTrainingResults").html("");
                $("#SearchTrainingResults").append(data);
                console.log(data);
                return false;
            }
        },
        complete:function(data){
        // Hide image container
            $("#AjaxLoader").hide();
            $("#SearchTrainingResults").show();
        }
    });
// }
});


$(document).ready(function () {

	var duration = [];
	// var role = [];
	// var job_title = $('#job_title').val();
    // Listen for 'change' event, so this triggers when the user clicks on the checkboxes labels
    $('.Duration').on('change', function (e) {

    	e.preventDefault();
        duration = new Array();

        $('input[name="Duration[]"]:checked').each(function()
        {
        	duration.push($(this).val());
        	// job_role.push($('#role').val());
            // alert(JSON.stringify(job_type));
        });
        // alert(JSON.stringify(job_type));

        $.ajax({
            type: "POST",
            url: "/FilterDuration",
            data: {duration: duration},
            dataType: "JSON",
            beforeSend: function(){
                // Show image container
                $("#AjaxLoader").show();
            },
            success: function (data) {
                $("#SearchTrainingResults").html("");
                if(data.error){
                    console.log(data);
                    return false;
                }else{
                    $("#SearchTrainingResults").html(data);
                    return false;
                }
            },
            complete:function(data){
                // Hide image container
                $("#AjaxLoader").hide();
            }
        });
	// parent.location.hash = "jobs/search";

});

});




// Sort By
// function SortByJobs(){
//     var sortby = $("#sortby").val();

//     $.ajax({
//         type: "POST",
//         url: "/SortByJobs",
//         data: {sortby: sortby},
//         dataType: "JSON",
//         beforeSend: function(){
//             // Show image container
//             $("#AjaxLoader").show();
//         },
//         success: function (data) {
//             if(data.error){
//                 console.log(data);
//                 return false;
//             }else{
                
//                 $("#SearchJobsResults").html("");
//                 $("#SearchJobsResults").append(data);
//                 return false;
//             }
//         },
//         complete:function(data){
//         // Hide image container
//             $("#AjaxLoader").hide();
//         }
//     });
// }


function SortByCandidates(){
    var sortby = $("#sortby").val();

    $.ajax({
        type: "POST",
        url: "/SortByCandidates",
        data: {sortby: sortby},
        dataType: "JSON",
        beforeSend: function(){
            // Show image container
            $("#AjaxLoader").show();
        },
        success: function (data) {
            if(data.error){
                console.log(data);
                return false;
            }else{
                $("#SearchCandidatesResults").html(data);
                return false;
            }
        },
        complete:function(data){
        // Hide image container
            $("#AjaxLoader").hide();
        }
    });
}





// Employer Manage jobs Filters
$(document).on("change", ".CheckDatePostedManageJobsFilters", function(){
    // function CheckJobFilters(){
        var date_posted = $(this).val();
    
        var FiltersData = {
            date_posted: date_posted
        }
    
        $.ajax({
            type: "POST",
            url: "/FilterDatePostedManageJobs",
            data: FiltersData,
            dataType: "html",
            beforeSend: function(){
                // Show image container
                $("#AjaxLoader").show();
                $("#ManageJobResults").hide();
                // return false;
            },
            success: function (data) {
                if(data){
                    $("#ManageJobResults").html("");
                    $("#ManageJobResults").append(data);
                    // console.log(Math.round(data.length/1000));
                    $("#JobsCount").text(Math.round(data.length/1000));
                    return false;
                }else{
                    // $("#JobsCount").text('0');
                    $("#ManageJobResults").html("No results found...");
                    return false;
                }
            },
            complete:function(data){
            // Hide image container
                $("#AjaxLoader").hide();
                $("#ManageJobResults").show();
            }
        });
    // }
    });


    $(document).on("change", ".CheckManageJobFilters", function(){
        // function CheckJobFilters(){
            // var date_posted = $(this).val();
            var location = $('.Location:checked').val();
            // var qualification = $('.Qualification:checked').val();
            var JobType = $('.JobType:checked').val();
            var Industry = $('.Industry:checked').val();
        
            location = [];
        
            $('input[name="Location"]:checked').each(function()
            {
                location.push($(this).val());
            });
        
            EmpType = [];
        
            $('input[name="EmpType"]:checked').each(function()
            {
                EmpType.push($(this).val());
            });
        
            Title = [];
        
            $('input[name="Title"]:checked').each(function()
            {
                Title.push($(this).val());
            });
        
            var FiltersData = {
                location: location,
                EmpType: EmpType,
                Title: Title
            }
        
            $.ajax({
                type: "POST",
                url: "/FilterManageJobs",
                data: FiltersData,
                dataType: "html",
                beforeSend: function(){
                    // Show image container
                    $("#AjaxLoader").show();
                    $("#ManageJobResults").hide();
                    // return false;
                },
                success: function (data) {
                    if(data.error){
                        danger_msg(data.message);
                        return false;
                    }else{
                        $("#ManageJobResults").html("");
                        $("#ManageJobResults").append(data);
                        console.log(data);
                        return false;
                    }
                },
                complete:function(data){
                // Hide image container
                    $("#AjaxLoader").hide();
                    $("#ManageJobResults").show();
                }
            });
        // }
        });



// Employer Manage Trainings 

$(document).on("change", ".CheckDatePostedManageTrainingFilters", function(){
    // function CheckJobFilters(){
        var date_posted = $(this).val();
    
    
        var FiltersData = {
            date_posted: date_posted
        }
    
        $.ajax({
            type: "POST",
            url: "/FilterDatePostedManageTrainings",
            data: FiltersData,
            dataType: "html",
            beforeSend: function(){
                // Show image container
                $("#AjaxLoader").show();
                $("#ManageTrainings").hide();
                // return false;
            },
            success: function (data) {
                if(data){
                    $("#ManageTrainings").html("");
                    $("#ManageTrainings").append(data);
                    
                    // $("#JobsCount").text(data.length)/1000;
                    return false;
                }else{
                    // $("#JobsCount").text('0');
                    $("#ManageTrainings").html("No results found...");
                    return false;
                }
            },
            complete:function(data){
            // Hide image container
                $("#AjaxLoader").hide();
                $("#ManageTrainings").show();
            }
        });
    // }
    });


    // Employer Close Trainings 

$(document).on("change", ".CheckDatePostedCloseTrainingFilters", function(){
    // function CheckJobFilters(){
        var date_posted = $(this).val();
    
    
        var FiltersData = {
            date_posted: date_posted
        }
    
        $.ajax({
            type: "POST",
            url: "/FilterDatePostedCloseTrainings",
            data: FiltersData,
            dataType: "html",
            beforeSend: function(){
                // Show image container
                $("#AjaxLoader").show();
                $("#ManageTrainings").hide();
                // return false;
            },
            success: function (data) {
                if(data){
                    $("#ManageTrainings").html("");
                    $("#ManageTrainings").append(data);
                    
                    // $("#JobsCount").text(data.length)/1000;
                    return false;
                }else{
                    // $("#JobsCount").text('0');
                    $("#ManageTrainings").html("No results found...");
                    return false;
                }
            },
            complete:function(data){
            // Hide image container
                $("#AjaxLoader").hide();
                $("#ManageTrainings").show();
            }
        });
    // }
    });


    $(document).on("change", ".CheckManageTrainingFilters", function(){
        // function CheckJobFilters(){
            // var date_posted = $(this).val();
            var location = $('.Location:checked').val();
            // var qualification = $('.Qualification:checked').val();
            var JobType = $('.JobType:checked').val();
            // var Duration = $('.Duration:checked').val();
        
            location = [];
        
            $('input[name="Location"]:checked').each(function()
            {
                location.push($(this).val());
            });
        
            Duration = [];
        
            $('input[name="Duration"]:checked').each(function()
            {
                Duration.push($(this).val());
            });
        
            // Stipend = [];
        
            // $('input[name="Stipend"]:checked').each(function()
            // {
            //     Stipend.push($(this).val());
            // });
        
            var FiltersData = {
                location: location,
                Duration: Duration
                // Stipend: Stipend
            }
        
            $.ajax({
                type: "POST",
                url: "/FilterManageTrainings",
                data: FiltersData,
                dataType: "html",
                beforeSend: function(){
                    // Show image container
                    $("#AjaxLoader").show();
                    $("#ManageTrainings").hide();
                    // return false;
                },
                success: function (data) {
                    if(data.error){
                        danger_msg(data.message);
                        return false;
                    }else{
                        $("#ManageTrainings").html("");
                        $("#ManageTrainings").append(data);
                        console.log(data);
                        return false;
                    }
                },
                complete:function(data){
                // Hide image container
                    $("#AjaxLoader").hide();
                    $("#ManageTrainings").show();
                }
            });
        // }
        });


        $(document).on("change", ".CheckCloseTrainingFilters", function(){
            // function CheckJobFilters(){
                // var date_posted = $(this).val();
                var location = $('.Location:checked').val();
                // var qualification = $('.Qualification:checked').val();
                var JobType = $('.JobType:checked').val();
                var Industry = $('.Industry:checked').val();
            
                location = [];
            
                $('input[name="Location"]:checked').each(function()
                {
                    location.push($(this).val());
                });
            
                Duration = [];
            
                $('input[name="Duration"]:checked').each(function()
                {
                    Duration.push($(this).val());
                });
            
                // Stipend = [];
            
                // $('input[name="Stipend"]:checked').each(function()
                // {
                //     Stipend.push($(this).val());
                // });
            
                var FiltersData = {
                    location: location,
                    Duration: Duration
                    // Stipend: Stipend
                }
            
                $.ajax({
                    type: "POST",
                    url: "/FilterCloseTrainings",
                    data: FiltersData,
                    dataType: "html",
                    beforeSend: function(){
                        // Show image container
                        $("#AjaxLoader").show();
                        $("#ManageTrainings").hide();
                        // return false;
                    },
                    success: function (data) {
                        if(data.error){
                            danger_msg(data.message);
                            return false;
                        }else{
                            $("#ManageTrainings").html("");
                            $("#ManageTrainings").append(data);
                            console.log(data);
                            return false;
                        }
                    },
                    complete:function(data){
                    // Hide image container
                        $("#AjaxLoader").hide();
                        $("#ManageTrainings").show();
                    }
                });
            // }
            });



// Employer Manage Candidates

$(document).on("change", ".CheckDatePostedCandidatesFilters", function(){
    // function CheckJobFilters(){
        var date_posted = $(this).val();
    
    
        var FiltersData = {
            date_posted: date_posted
        }
    
        $.ajax({
            type: "POST",
            url: "/FilterDatePostedCandidates",
            data: FiltersData,
            dataType: "html",
            beforeSend: function(){
                // Show image container
                $("#AjaxLoader").show();
                $("#ManageCandidates").hide();
                // return false;
            },
            success: function (data) {
                if(data){
                    $("#ManageCandidates").html("");
                    $("#ManageCandidates").append(data);
                    
                    // $("#JobsCount").text(data.length)/1000;
                    return false;
                }else{
                    // $("#JobsCount").text('0');
                    $("#ManageCandidates").html("No results found...");
                    return false;
                }
            },
            complete:function(data){
            // Hide image container
                $("#AjaxLoader").hide();
                $("#ManageCandidates").show();
            }
        });
    // }
    });


    $(document).on("change", ".CheckManageCandidatesFilters", function(){
        // function CheckJobFilters(){
            // var date_posted = $(this).val();
            var location = $('.Location:checked').val();
            // var qualification = $('.Qualification:checked').val();
            var JobType = $('.JobType:checked').val();
            var Industry = $('.Industry:checked').val();
        
            location = [];
        
            $('input[name="Location"]:checked').each(function()
            {
                location.push($(this).val());
            });
        
            // Experience = [];
        
            // $('input[name="Experience"]:checked').each(function()
            // {
            //     Experience.push($(this).val());
            // });
        
            // Industry = [];
        
            // $('input[name="Industry"]:checked').each(function()
            // {
            //     Industry.push($(this).val());
            // });
        
            var FiltersData = {
                location: location
                // Experience: Experience,
                // Industry: Industry
            }
        
            $.ajax({
                type: "POST",
                url: "/FilterManageCandidates",
                data: FiltersData,
                dataType: "html",
                beforeSend: function(){
                    // Show image container
                    $("#AjaxLoader").show();
                    $("#ManageCandidates").hide();
                    // return false;
                },
                success: function (data) {
                    if(data){
                        $("#ManageCandidates").html("");
                        $("#ManageCandidates").append(data);
                        
                        // $("#JobsCount").text(data.length)/1000;
                        return false;
                    }else{
                        // $("#JobsCount").text('0');
                        $("#ManageCandidates").html("No results found...");
                        return false;
                    }
                },
                complete:function(data){
                // Hide image container
                    $("#AjaxLoader").hide();
                    $("#ManageCandidates").show();
                }
            });
        // }
        });


$(document).on("change", ".CheckShortlistedByCanddiates", function(){

    candidateshortlistType = [];

    $('input[name="ShortlistType"]:checked').each(function()
    {
        candidateshortlistType.push($(this).val());
    });

    $.ajax({
        type: "POST",
        url: "/FilterShortlistedCandidates",
        data: {candidateshortlistType: candidateshortlistType},
        dataType: "html",
        beforeSend: function(){
            // Show image container
            $("#ShortlistAjaxLoader").show();
            $("#ShortlistManageCandidates").hide();
            // return false;
        },
        success: function (data) {
            if(data){
                $("#ShortlistManageCandidates").html("");
                $("#ShortlistManageCandidates").append(data);
                
                // $("#JobsCount").text(data.length)/1000;
                return false;
            }else{
                // $("#JobsCount").text('0');
                $("#ShortlistManageCandidates").html("No results found...");
                return false;
            }
        },
        complete:function(data){
        // Hide image container
            $("#ShortlistAjaxLoader").hide();
            $("#ShortlistManageCandidates").show();
        }
    });
});



$(document).on("change", ".CheckAppliedShortlisted", function(){

    // var CheckAppliedShortlisted = $("#CheckAppliedShortlisted").val();
    var JobId = $("#JobId").val();

    if($('input[name="CheckAppliedShortlisted"]').is(':checked'))
    {
        var CheckAppliedShortlisted = 1;
    }else
    {
        var CheckAppliedShortlisted = 0;
    }

    $.ajax({
        type: "POST",
        url: "/FilterAppliedCandidates",
        data: {CheckAppliedShortlisted: CheckAppliedShortlisted, JobId: JobId},
        dataType: "html",
        beforeSend: function(){
            // Show image container
            $("#AjaxLoader").show();
            $("#AppliedCandidates").hide();
            // return false;
        },
        success: function (data) {
            if(data){
                $("#AppliedCandidates").html("");
                $("#AppliedCandidates").append(data);
                
                // $("#JobsCount").text(data.length)/1000;
                return false;
            }else{
                // $("#JobsCount").text('0');
                $("#AppliedCandidates").html("No results found...");
                return false;
            }
        },
        complete:function(data){
        // Hide image container
            $("#AjaxLoader").hide();
            $("#AppliedCandidates").show();
        }
    });
});



$(document).on("change", ".CheckApprovedTrainee", function(){

    // var CheckAppliedShortlisted = $("#CheckAppliedShortlisted").val();
    var TrainingId = $("#TrainingId").val();

    if($('input[name="CheckApprovedTrainee"]').is(':checked'))
    {
        var CheckApprovedTrainee = 1;
    }else
    {
        var CheckApprovedTrainee = 2;
    }

    $.ajax({
        type: "POST",
        url: "/FilterApprovedTrainees",
        data: {CheckApprovedTrainee: CheckApprovedTrainee, TrainingId: TrainingId},
        dataType: "html",
        beforeSend: function(){
            // Show image container
            $("#AjaxLoader").show();
            $("#AppliedTrainees").hide();
            // return false;
        },
        success: function (data) {
            if(data){
                $("#AppliedTrainees").html("");
                $("#AppliedTrainees").append(data);
                
                // $("#JobsCount").text(data.length)/1000;
                return false;
            }else{
                // $("#JobsCount").text('0');
                $("#AppliedTrainees").html("No results found...");
                return false;
            }
        },
        complete:function(data){
        // Hide image container
            $("#AjaxLoader").hide();
            $("#AppliedTrainees").show();
        }
    });
});



$(document).on("change", ".ChangeJobStatus", function(){

    // var CheckAppliedShortlisted = $("#CheckAppliedShortlisted").val();
    var JobId = $("#JobId").val();

    if($('input[name="ChangeJobStatus"]').is(':checked'))
    {
        var CheckJobsStatus = 1;
    }else
    {
        var CheckJobsStatus = 0;
    }

    $.ajax({
        type: "POST",
        url: "/ChangeJobStatus",
        data: {CheckJobsStatus: CheckJobsStatus, JobId: JobId},
        dataType: "JSON",
        success: function (data) {
            if(data.error){
                danger_toast_msg(data.message);
                return false;
            }else{
                // $("#JobsCount").text('0');
                success_toast_msg(data.message);
                return false;
            }
        }
    });
});



$(document).on("change", ".ChangeCAJobStatus", function(){

    // var CheckAppliedShortlisted = $("#CheckAppliedShortlisted").val();
    var JobId = $("#JobId").val();

    if($('input[name="ChangeCAJobStatus"]').is(':checked'))
    {
        var CheckJobsStatus = 1;
    }else
    {
        var CheckJobsStatus = 0;
    }

    $.ajax({
        type: "POST",
        url: "/ChangeCAJobStatus",
        data: {CheckJobsStatus: CheckJobsStatus, JobId: JobId},
        dataType: "JSON",
        success: function (data) {
            if(data.error){
                danger_toast_msg(data.message);
                return false;
            }else{
                // $("#JobsCount").text('0');
                success_toast_msg(data.message);
                return false;
            }
        }
    });
});



$(document).on("change", ".ChangeTrainingStatus", function(){

    // var CheckAppliedShortlisted = $("#CheckAppliedShortlisted").val();
    var TrainingId = $("#TrainingId").val();

    if($('input[name="ChangeTrainingStatus"]').is(':checked'))
    {
        var CheckTrainingStatus = 1;
    }else
    {
        var CheckTrainingStatus = 0;
    }

    $.ajax({
        type: "POST",
        url: "/ChangeTrainingStatus",
        data: {CheckTrainingStatus: CheckTrainingStatus, TrainingId: TrainingId},
        dataType: "JSON",
        success: function (data) {
            if(data.error){
                danger_toast_msg(data.message);
                return false;
            }else{
                // $("#JobsCount").text('0');
                success_toast_msg(data.message);
                return false;
            }
        }
    });
});



// $(document).on("change", ".ChangeTrainingCompletionStatus", function(){
    function ChangeTrainingCompletionStatus(TrainingId){

    // var CheckAppliedShortlisted = $("#CheckAppliedShortlisted").val();
    // var TrainingId = $("#TrainingId").val();

    if($('input[name="ChangeTrainingCompletionStatus"]').is(':checked'))
    {
        var CheckTrainingStatus = 1;
    }else
    {
        var CheckTrainingStatus = 0;
    }

    $.ajax({
        type: "POST",
        url: "/ChangeTrainingCompletionStatus",
        data: {CheckTrainingStatus: CheckTrainingStatus, TrainingId: TrainingId},
        dataType: "JSON",
        success: function (data) {
            if(data.error){
                danger_toast_msg(data.message);
                return false;
            }else{
                // $("#JobsCount").text('0');
                success_toast_msg(data.message);
                return false;
            }
        }
    });
// });
    }




// Closed Jobs

// Employer Manage jobs Filters
$(document).on("change", ".CheckDatePostedCloseJobsFilters", function(){
    // function CheckJobFilters(){
        var date_posted = $(this).val();
    
        var FiltersData = {
            date_posted: date_posted
        }
    
        $.ajax({
            type: "POST",
            url: "/FilterDatePostedCloseJobs",
            data: FiltersData,
            dataType: "html",
            beforeSend: function(){
                // Show image container
                $("#AjaxLoader").show();
                $("#ManageJobResults").hide();
                // return false;
            },
            success: function (data) {
                if(data){
                    $("#ManageJobResults").html("");
                    $("#ManageJobResults").append(data);
                    
                    // $("#JobsCount").text(data.length)/1000;
                    return false;
                }else{
                    // $("#JobsCount").text('0');
                    $("#ManageJobResults").html("No results found...");
                    return false;
                }
            },
            complete:function(data){
            // Hide image container
                $("#AjaxLoader").hide();
                $("#ManageJobResults").show();
            }
        });
    // }
    });

    $(document).on("change", ".CheckCloseJobFilters", function(){
        // function CheckJobFilters(){
            // var date_posted = $(this).val();
            var location = $('.Location:checked').val();
            // var qualification = $('.Qualification:checked').val();
            var JobType = $('.JobType:checked').val();
            var Industry = $('.Industry:checked').val();
        
            location = [];
        
            $('input[name="Location"]:checked').each(function()
            {
                location.push($(this).val());
            });
        
            EmpType = [];
        
            $('input[name="EmpType"]:checked').each(function()
            {
                EmpType.push($(this).val());
            });
        
            Title = [];
        
            $('input[name="Title"]:checked').each(function()
            {
                Title.push($(this).val());
            });
        
            var FiltersData = {
                location: location,
                EmpType: EmpType,
                Title: Title
            }
        
            $.ajax({
                type: "POST",
                url: "/FilterCloseJobs",
                data: FiltersData,
                dataType: "html",
                beforeSend: function(){
                    // Show image container
                    $("#AjaxLoader").show();
                    $("#ManageJobResults").hide();
                    // return false;
                },
                success: function (data) {
                    if(data.error){
                        danger_msg(data.message);
                        return false;
                    }else{
                        $("#ManageJobResults").html("");
                        $("#ManageJobResults").append(data);
                        console.log(data);
                        return false;
                    }
                },
                complete:function(data){
                // Hide image container
                    $("#AjaxLoader").hide();
                    $("#ManageJobResults").show();
                }
            });
        // }
        });




// Fresher Jobs

$(document).on("change", ".CheckManageFrehserJobsFilters", function(){
    // function CheckJobFilters(){
    var date_posted = $(this).val();
    // var qualification = $('.Qualification:checked').val();
    // var Duration = $('.JobType:checked').val();
    // var Spend = $('.Industry:checked').val();

    var location = [];

    $('input[name="Location"]:checked').each(function()
    {
        location.push($(this).val());
    });

    var FiltersData = {
        location: location
    }

    $.ajax({
        type: "POST",
        url: "/FilterFresherJobs",
        data: FiltersData,
        dataType: "html",
        beforeSend: function(){
            // Show image container
            $("#AjaxLoader").show();
            $("#SearchCAFresherJobsResults").hide();
            // return false
        },
        success: function (data) {
            if(data.error){
                danger_msg(data.message);
                return false;
            }else{
                $("#SearchCAFresherJobsResults").html("");
                $("#SearchCAFresherJobsResults").append(data);
                console.log(data);
                return false;
            }
        },
        complete:function(data){
        // Hide image container
            $("#AjaxLoader").hide();
            $("#SearchCAFresherJobsResults").show();
        }
    });
// }
});



function success_msg(data){
    $.notify({
        title: '<strong>'+ data +'</strong>',
        message: ''
    },{
        type: 'success'
    });
}

function danger_msg(data){
    $.notify({
        title: '<strong>'+ data +'</strong>',
        message: ''
    },{
        type: 'danger'
    });
}
