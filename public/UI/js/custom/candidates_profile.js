$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});



function CheckZipCode(){
    var zipcode = $("#zipcode").val();

    $.ajax({
        type: "GET",
        url: "https://api.postalpincode.in/pincode/"+zipcode,
        // url:"https://autocomplete.geocoder.api.here.com/6.2/suggest.json?query="+zipcode+"&beginHighlight=%3Cmark%3E&endHighlight=%3C%2Fmark%3E&maxresults=5&app_id=bUrARPgKIZUgzZzTZHo8&app_code=EHzEXK8m7NJ9ghYYJ8Q0_w",
        data: {zipcode: zipcode},
        dataType: "JSON",
        success: function (data) {
            
            if(data.error){
                console.log(data);
                return false;
            }else{
                // console.log(data);
                $.each(data, function (key, val) { 
                    console.log(val);
                    $.each(val.PostOffice, function (keys, i) { 
                        console.log(i.Country);
                        console.log(i.State);
                        $("#country").val(i.Country);
                        $("#state").val(i.State);
                        $("#city").val(i.District);
                        // console.log(i.District);
                    });
                });
                return false;
            }
            
        }
    });
}


function CheckEmpZipCode(){
    var zipcode = $("#Emp_zipcode").val();

    $.ajax({
        type: "GET",
        url: "https://api.postalpincode.in/pincode/"+zipcode,
        // url:"https://autocomplete.geocoder.api.here.com/6.2/suggest.json?query="+zipcode+"&beginHighlight=%3Cmark%3E&endHighlight=%3C%2Fmark%3E&maxresults=5&app_id=bUrARPgKIZUgzZzTZHo8&app_code=EHzEXK8m7NJ9ghYYJ8Q0_w",
        data: {zipcode: zipcode},
        dataType: "JSON",
        success: function (data) {
            
            if(data.error){
                console.log(data);
                return false;
            }else{
                // console.log(data);
                $.each(data, function (key, val) { 
                    console.log(val);
                    $.each(val.PostOffice, function (keys, i) { 
                        console.log(i.Country);
                        console.log(i.State);
                        $("#Emp_country").val(i.Country);
                        $("#Emp_state").val(i.State);
                        $("#Emp_city").val(i.District);
                        // console.log(i.District);
                    });
                });
                return false;
            }
            
        }
    });
}



function CheckArticleshipZipCode(){
    var zipcode = $("#Ca_Edit_zipcode").val();

    $.ajax({
        type: "GET",
        url: "https://api.postalpincode.in/pincode/"+zipcode,
        // url:"https://autocomplete.geocoder.api.here.com/6.2/suggest.json?query="+zipcode+"&beginHighlight=%3Cmark%3E&endHighlight=%3C%2Fmark%3E&maxresults=5&app_id=bUrARPgKIZUgzZzTZHo8&app_code=EHzEXK8m7NJ9ghYYJ8Q0_w",
        data: {zipcode: zipcode},
        dataType: "JSON",
        success: function (data) {
            
            if(data.error){
                console.log(data);
                return false;
            }else{
                // console.log(data);
                $.each(data, function (key, val) { 
                    console.log(val);
                    $.each(val.PostOffice, function (keys, i) { 
                        console.log(i.Country);
                        console.log(i.State);
                        $("#Ca_Edit_country").val(i.Country);
                        $("#Ca_Edit_state").val(i.State);
                        $("#Ca_Edit_city").val(i.District);
                        // console.log(i.District);
                    });
                });
                return false;
            }
            
        }
    });
}


function CheckEditJobsZip(){
    var zipcode = $("#Edit_zipcode").val();

    $.ajax({
        type: "GET",
        url: "https://api.postalpincode.in/pincode/"+zipcode,
        // url:"https://autocomplete.geocoder.api.here.com/6.2/suggest.json?query="+zipcode+"&beginHighlight=%3Cmark%3E&endHighlight=%3C%2Fmark%3E&maxresults=5&app_id=bUrARPgKIZUgzZzTZHo8&app_code=EHzEXK8m7NJ9ghYYJ8Q0_w",
        data: {zipcode: zipcode},
        dataType: "JSON",
        success: function (data) {
            
            if(data.error){
                console.log(data);
                return false;
            }else{
                // console.log(data);
                $.each(data, function (key, val) { 
                    console.log(val);
                    $.each(val.PostOffice, function (keys, i) { 
                        console.log(i.Country);
                        console.log(i.State);
                        $("#Edit_country").val(i.Country);
                        $("#Edit_state").val(i.State);
                        $("#Edit_city").val(i.District);
                        // console.log(i.District);
                    });
                });
                return false;
            }
            
        }
    });
}



function CheckCAJobZipcode(){
    var zipcode = $("#Edit_Ca_zipcode").val();

    $.ajax({
        type: "GET",
        url: "https://api.postalpincode.in/pincode/"+zipcode,
        // url:"https://autocomplete.geocoder.api.here.com/6.2/suggest.json?query="+zipcode+"&beginHighlight=%3Cmark%3E&endHighlight=%3C%2Fmark%3E&maxresults=5&app_id=bUrARPgKIZUgzZzTZHo8&app_code=EHzEXK8m7NJ9ghYYJ8Q0_w",
        data: {zipcode: zipcode},
        dataType: "JSON",
        success: function (data) {
            
            if(data.error){
                console.log(data);
                return false;
            }else{
                // console.log(data);
                $.each(data, function (key, val) { 
                    console.log(val);
                    $.each(val.PostOffice, function (keys, i) { 
                        console.log(i.Country);
                        console.log(i.State);
                        $("#Edit_Ca_country").val(i.Country);
                        $("#Edit_Ca_state").val(i.State);
                        $("#Edit_Ca_city").val(i.District);
                        // console.log(i.District);
                    });
                });
                return false;
            }
            
        }
    });
}



function CheckSalaryMax(){
    var salary_min = $("#salary_min").val();

    var salary_max = $("#salary_max").val();
    console.log(salary_min);
    console.log(salary_max);

    if(parseInt(salary_min) > parseInt(salary_max)){
        danger_toast_msg("it can't be less than minimum salary");
        return false;
    }else{
        success_toast_msg("valid");
        return false;
    }
    
    
}



function BrowseCandidatePic(elemId) {
    var elem = document.getElementById(elemId);
    if(elem && document.createEvent) {
       var evt = document.createEvent("MouseEvents");
       evt.initEvent("click", true, false);
       elem.dispatchEvent(evt);
    }
    // readURL(this);
 }



 function readURL(input) {
    if (input.files && input.files[0]) {
      var reader = new FileReader();
      
      reader.onload = function(e) {
        $('#CandidateProfilePic').attr('src', e.target.result);
        $('.profile-head-width').attr('src', e.target.result);
      }
      
      reader.readAsDataURL(input.files[0]);
    }
  }
  
  $("#file").change(function() {
    readURL(this);

    var CandidateProfileData = new FormData();
    CandidateProfileData.append('file', $('#file')[0].files[0]);

    $.ajax({
        type: "POST",
        url: "/Candidate/UpdateProfilePic",
        data: CandidateProfileData,
        dataType: "JSON",
        contentType: false,
        processData: false,
        success: function (data) {
            if(data.error){
                danger_toast_msg(data.message);
                return false;
            }else{
                success_toast_msg(data.message);
                location.reload();
                return false;
            }
        }
    });
  });
  

$(document).on("click", "#AddCandidateProfile", function(){
    // var fullname = $("#fullname").val();
    var id = $("#id").val();
    var name = $("#fullname").val();
    var experience = $("#experience :selected").val();
    var current_salary = $("#current_salary :selected").val();
    var desired_location = $("#desired_location").val();
    

    if(experience == "Select Experience"){
        danger_toast_msg("Experience should not empty");
        $("#experience").focus();
        return false;
    }

    if(current_salary == "Please Select"){
        danger_toast_msg("Current salary should not empty");
        $("#current_salary").focus();
        return false;
    }


    if(desired_location == ""){
        danger_toast_msg("Desired location should not empty");
        $("#desired_location").focus();
        return false;
    }

   
    var CandidateProfileData = {
        // name: name,
        experience: experience,
        current_salary: current_salary,
        desired_location: desired_location
    }

    $.ajax({
        type: "POST",
        url: "/Candidate/InsertCandidateProfile",
        data: CandidateProfileData,
        dataType: "JSON",
        success: function (data) {
            if(data.error){
                danger_toast_msg(data.message);
                return false;
            }else{
                success_toast_msg(data.message);
                return false;
            }
        }
    });
});


// Update Canddiate Profile
$(document).on("click", "#UpdateCandidateProfile", function(){
    // var fullname = $("#fullname").val();
    var id = $("#id").val();
    var name = $("#fullname").val();
    var experience_year = $("#Edit_experience :selected").val();
    var experience_month = $("#Edit_experience_months :selected").val();

    var experience = experience_year+","+experience_month;

    var current_salary = $("#Edit_current_salary").val();
    // var current_salary_thousand = $("#Edit_current_salary_thousand").val();

    // var current_salary = current_salary_lacs+","+current_salary_thousand;

    // var current_salary = $("#Edit_current_salary :selected").val();
    var desired_location = $("#Edit_desired_location").val();
    

    if(experience == "Select Experience"){
        danger_toast_msg("Experience should not empty");
        $("#Edit_experience").focus();
        return false;
    }

    if(current_salary == "Please Select"){
        danger_toast_msg("Current salary should not empty");
        $("#Edit_current_salary").focus();
        return false;
    }


    if(desired_location == ""){
        danger_toast_msg("Desired location should not empty");
        $("#Edit_desired_location").focus();
        return false;
    }

   
    var CandidateProfileData = {
        // name: name,
        experience: experience,
        current_salary: current_salary,
        desired_location: desired_location
    }

    $.ajax({
        type: "POST",
        url: "/Candidate/InsertCandidateProfile",
        data: CandidateProfileData,
        dataType: "JSON",
        success: function (data) {
            if(data.error){
                danger_toast_msg(data.message);
                return false;
            }else{
                success_toast_msg(data.message);
                location.reload();
                return false;
            }
        }
    });
});



// Add Shortlisted

function AddShortlisted(CandidateId){
    // var CandidateId = $("#CandidateId").val();

    $.ajax({
        type: "POST",
        url: "/ShortlistCandidatesByJobList",
        data: {CandidateId: CandidateId},
        dataType: "JSON",
        beforeSend: function(){
            // Show image container
            $("#ApplyBtnLoader").show();
            $("#AddShortlisted").hide();
        },
        success: function (data) {
            if(data.error){
                // danger_msg(data.message);
                return false;
            }else{
                // success_msg(data.message);
                return false;
            }
        },
        complete:function(data){
        // Hide image container
            $("#ApplyBtnLoader").hide();
            $("#AddShortlisted").hide();
            $("#ShortlistedSuccess").show();
        }
    });
}


function AddShortlistedDirect(CandidateId){
    // var CandidateId = $("#CandidateId").val();
    
    $.ajax({
        type: "POST",
        url: "/ShortlistCandidatesByDirect",
        data: {CandidateId: CandidateId},
        dataType: "JSON",
        beforeSend: function(){
            // Show image container
            $("#ApplyBtnLoader").show();
            $("#AddShortlistedDirect").hide();
        },
        success: function (data) {
            if(data.error){
                // danger_msg(data.message);
                return false;
            }else{
                // success_msg(data.message);
                return false;
            }
        },
        complete:function(data){
        // Hide image container
            $("#ApplyBtnLoader").hide();
            $("#AddShortlistedDirect").hide();
            $("#ShortlistedSuccess").show();
        }
    });
}



$(function() {
    $("#AddToFavouriteJobs").on("click", function() {
        var JobsId = $("#JobsId").val();
        if($(this).toggleClass("is-active")){
            $.ajax({
                type: "POST",
                url: "/AddToFavourite",
                data: {JobsId: JobsId},
                dataType: "JSON",
                beforeSend: function(){
                    // Show image container
                    $("#ApplyShortlistBtnLoader").show();
                    $("#AddToFavouriteJobs").hide();
                },
                success: function (data) {
                    if(data.error){
                        danger_toast_msg(data.message);
                        return false;
                    }else{
                        success_toast_msg(data.message);
                        return false;
                    }
                },
                complete:function(data){
                // Hide image container
                    $("#ApplyShortlistBtnLoader").hide();
                    $("#AddToFavouriteJobs").hide();
                    $("#AppliedShortlistedSuccessfully").show();
                }
            });
        }
    //   $(this).toggleClass("is-active");
    });
  });


  function ChangeJob(){
    //   var status = "";
    //     if($('#ChangeJob').is(':checked')){
    //         status = 1;
    //     }else{
    //         status = 0;
    //     }

        $.ajax({
            type: "POST",
            url: "/ChangeJobChange",
            // data: {status: status},
            dataType: "JSON",
            success: function (data) {
                if(data.error){
                    danger_msg(data.message);
                    return false;
                }else{
                    success_msg(data.message);
                    return false;
                }
            }
        });
  }


  $(document).on("click", "#LoadmoreJobs", function(){
        var JobsId = $(this).data('id');

        $.ajax({
            type: "POST",
            url: "/LoadMoreJobs",
            data: {JobsId: JobsId},
            dataType: "text",
            beforeSend: function(){
                $("#ApplyBtnLoader").show();
            },
            success: function (data) {
                if(data != ''){
                    console.log(data);
                    $('#remove-row').remove();
                    $("#SearchJobsResults").append(data);
                }else{
                    console.log(data);
                    $("#LoadmoreJobs").html("No jobs found...");
                    return false;
                }
            },
            complete:function(data){
                $("#ApplyBtnLoader").hide();
            }
        });
        
  });



  $(document).on("click", "#UpdateCandidatePassword", function(){
        var OldPassword = $("#old_password").val();
        var NewPassword = $("#new_password").val();
        var VerifyPassword = $("#verify_password").val();


        if(OldPassword == ""){
            danger_toast_msg("Old password should not empty");
            $("#old_password").focus();
            return false;
        }
    
        if(NewPassword == ""){
            danger_toast_msg("New password should not empty");
            $("#new_password").focus();
            return false;
        }
    
        if(VerifyPassword == ""){
            danger_toast_msg("Verify password should not empty");
            $("#verify_password").focus();
            return false;
        }

        
        var CandidatePassword = {
            OldPassword: OldPassword,
            NewPassword: NewPassword,
            VerifyPassword: VerifyPassword
        }

        $.ajax({
            type: "POST",
            url: "/UpdatePassword",
            data: CandidatePassword,
            dataType: "JSON",
            success: function (data) {
                if(data.error){
                    danger_toast_msg(data.message);
                    return false;
                }else{
                    success_toast_msg(data.message);
                    return false;
                }
            }
        });

  });


  $(document).on("click", "#AddCandidatePersonalDetails", function(){
    var dob = $("#dob").val();
    var gender = $("#gender").val();
    var marital_status = $("#marital_status").val();
    var father_name = $("#father_name").val();
    var address = $("#address").val();

    var PersonalDetails = {
        dob: dob,
        gender: gender,
        marital_status: marital_status,
        father_name: father_name,
        address: address
    }

    $.ajax({
        type: "POST",
        url: "/Candidate/UpdatePersonalDetails",
        data: PersonalDetails,
        dataType: "JSON",
        success: function (data) {
            if(data.error){
                danger_toast_msg(data.message);
                return false;
            }else{
                success_toast_msg(data.message);
                return false;
            }
        }
    });

});

function EditPersonalDetailsModal(id){
    $.ajax({
        type: "POST",
        url: "/Candidate/GetPersonalDetailsById/"+id,
        data: {id: id},
        dataType: "JSON",
        success: function (data) {
            if(data.error){
                danger_msg(data.message);
                return false;
            }else{
                // if((data.title == 1) || (data.title == 2)){
                //     $("#Edit_CourseNameRow").hide();
                // }else{
                //     $("#Edit_CourseNameRow").show();
                // }
                // $(".board option[value="+data.department+"]").attr('selected', 'selected');

                $("#Edit_dob").val(data.date_of_birth);

                $("#Edit_gender option[value="+data.gender+"]").attr('selected', 'selected');
                $("#Edit_marital_status option[value="+data.marital_status+"]").attr('selected', 'selected');

                $("#Edit_father_name").val(data.father_name);
                $("#Edit_address").val(data.address);
                $("#Edit_state").val(data.state);
                $("#Edit_city").val(data.city);
                $("#Edit_zipcode").val(data.zip_code);

                $("#EditPersonalDetailsModal").modal("show");
                return false;
            }
        }
    });
}

$(document).on("click", "#UpdateCandidatePersonalDetails", function(){
    var dob = $("#Edit_dob").val();
    var gender = $("#Edit_gender").val();
    var marital_status = $("#Edit_marital_status").val();
    var father_name = $("#Edit_father_name").val();
    var address = $("#Edit_address").val();
    var state = $("#Edit_state").val();
    var city = $("#Edit_city").val();
    var zipcode = $("#Edit_zipcode").val();

    var PersonalDetails = {
        dob: dob,
        gender: gender,
        marital_status: marital_status,
        father_name: father_name,
        address: address,
        state: state,
        city: city,
        zipcode: zipcode
    }

    $.ajax({
        type: "POST",
        url: "/Candidate/UpdatePersonalDetails",
        data: PersonalDetails,
        dataType: "JSON",
        success: function (data) {
            if(data.error){
                danger_toast_msg(data.message);
                return false;
            }else{
                success_toast_msg(data.message);
                location.reload();
                return false;
            }
        }
    });

});

  $("#yes, #no").change(function () {
    if ($("#yes").is(":checked")) {
        $("#WorkedTill").show();
        $("#WorkingTo").hide();
        $("#CurrentSalaryRow").show();
        $("#NoticePeriod").show();
    }
    else if ($("#no").is(":checked")) {
        $("#WorkedTill").hide();
        $("#WorkingTo").show();
        $("#CurrentSalaryRow").hide();
        $("#NoticePeriod").hide();
    }
}); 


$("#Edit_yes, #Edit_no").change(function () {
    if ($("#Edit_yes").is(":checked")) {
        $("#Edit_WorkedTill").show();
        $("#Edit_WorkingTo").hide();
        $("#Edit_CurrentSalaryRow").show();
        $("#Edit_NoticePeriod").show();
    }
    else if ($("#Edit_no").is(":checked")) {
        $("#Edit_WorkedTill").hide();
        $("#Edit_WorkingTo").show();
        $("#Edit_CurrentSalaryRow").hide();
        $("#Edit_NoticePeriod").hide();
    }
}); 



  $(document).on("click", "#Currentlocation", function(){
    $.getJSON("http://ip-api.io/api/json",
        function(data){
            console.log(data);
        }
    );
  });




  /* Edit Candidate Profile  Popup */
$('.EditCandidateProfile').on('click', function(){
    $('.edit_candidate-profile-popup-box').fadeIn('fast');
    $('html').addClass('no-scroll');

    var id = $("#pro_id").val();

    $.ajax({
        type: "POST",
        url: "/Candidate/GetPersonalDetailsById/"+id,
        data: {id: id},
        dataType: "JSON",
        success: function (data) {
            if(data.error){
                danger_msg(data.message);
                return false;
            }else{
                var experience_data = data.experience
                var experience = experience_data.split(",");

                // var salary_data = data.gross_salary
                // var salary = salary_data.split(",");

                $("#Edit_experience option[value="+experience[0]+"]").attr('selected', 'selected');

            $("#Edit_experience_months option[value="+experience[1]+"]").attr('selected', 'selected');

            $("#Edit_current_salary").val(data.gross_salary);
            // $("#Edit_current_salary_thousand").val(salary[1]);
                // $("#Edit_current_salary option[value="+data.gross_salary+"]").attr('selected', 'selected');

                $("#Edit_desired_location").val(data.desired_location);
                return false;
            }
        }
    });

});
$('.close-popup').on('click', function(){
    $('.edit_candidate-profile-popup-box').fadeOut('fast');
    $('html').removeClass('no-scroll');
});


$('input#Edit_current_salary').keyup(function(event) {

    // skip for arrow keys
    if(event.which >= 37 && event.which <= 40) return;
  
    // format number
    $(this).val(function(index, value) {
      return value
      .replace(/\D/g, "")
      .replace(/\B(?=(\d{3})+(?!\d))/g, ",")
      ;
    });
  });


  $('input#current_salary_month').keyup(function(event) {

    // skip for arrow keys
    if(event.which >= 37 && event.which <= 40) return;
  
    // format number
    $(this).val(function(index, value) {
      return value
      .replace(/\D/g, "")
      .replace(/\B(?=(\d{3})+(?!\d))/g, ",")
      ;
    });
  });


  $('input#Edit_current_salary_month').keyup(function(event) {

    // skip for arrow keys
    if(event.which >= 37 && event.which <= 40) return;
  
    // format number
    $(this).val(function(index, value) {
      return value
      .replace(/\D/g, "")
      .replace(/\B(?=(\d{3})+(?!\d))/g, ",")
      ;
    });
  });


function success_msg(data){
    $.notify({
        title: '<strong>'+ data +'</strong>',
        message: ''
    },{
        type: 'success'
    });
}

function danger_msg(data){
    $.notify({
        title: '<strong>'+ data +'</strong>',
        message: ''
    },{
        type: 'danger'
    });
}
