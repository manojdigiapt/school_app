$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

// Trainings

// $(this).ready( function() {  
//     $("#SearchTrainingTitleOrCompanyName").autocomplete({  
//         minLength: 1,  
//         source:   
//         function(req, add){  
//             $.ajax({  
//                 url: "/SearchTrainingTitle",  
//                 dataType: 'json',  
//                 type: 'POST',  
//                 data: req,  
//                 success:      
//                 function(data){  
//                     if(data.response =="true"){  
//                         $.each(data, function (i, key) { 
//                             add(key);  
//                             console.log(key);
//                         });
//                     }else{
//                         $.each(data, function (i, key) { 
//                             add(key);  
//                             console.log(key);
//                         });
                        
//                     }
//                 },  
//             });  
//         },  
             
//     });  
// });  


// Qualification

// $(this).ready( function() {  
//     $("#qualification").autocomplete({  
//         minLength: 1,  
//         source:   
//         function(req, add){  
//             $.ajax({  
//                 url: "/SearchQualification",  
//                 dataType: 'json',  
//                 type: 'POST',  
//                 data: req,  
//                 headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
//                 success:      
//                 function(data){  
//                     if(data.response =="true"){  
//                         $.each(data, function (i, key) { 
//                             add(key);  
//                             console.log(key);
//                         });
//                     }else{
//                         $.each(data, function (i, key) { 
//                             add(key);  
//                             console.log(key);
//                         });
                        
//                     }
//                 },  
//             });  
//         },  
             
//     });  
// });  


$(document).ready(function() {
    src = "/searchajax";
     $("#qualification").autocomplete({
        source: function(request, response) {
            $.ajax({
                url: src,
                dataType: "json",
                data: {
                    term : request.term
                },
                success: function(data) {
                    response(data);
                   
                }
            });
        },
        minLength: 1,
       
    });
});


$(document).ready(function() {
    src = "/searchajax";
     $("#Ca_qualification").autocomplete({
        source: function(request, response) {
            $.ajax({
                url: src,
                dataType: "json",
                data: {
                    term : request.term
                },
                success: function(data) {
                    response(data);
                   
                }
            });
        },
        minLength: 1,
       
    });
});


$(document).ready(function() {
    src = "/searchajax";
     $("#Edit_Ca_qualification").autocomplete({
        source: function(request, response) {
            $.ajax({
                url: src,
                dataType: "json",
                data: {
                    term : request.term
                },
                success: function(data) {
                    response(data);
                   
                }
            });
        },
        minLength: 1,
       
    });
});


$(document).ready(function() {
    src = "/searchajax";
     $("#Edit_qualification").autocomplete({
        source: function(request, response) {
            $.ajax({
                url: src,
                dataType: "json",
                data: {
                    term : request.term
                },
                success: function(data) {
                    response(data);
                   
                }
            });
        },
        minLength: 1,
       
    });
});


// var path = "{{ route('SearchQualification') }}";
//     $('input#qualification').typeahead({
//         source:  function (query, process) {
//         return $.get(path, { query: query }, function (data) {
//                 console.log(data);
//                 return process(data);
//             });
//         }
//     });


$(document).on("click", "#SearchTrainings", function(){
    var title = $("#SearchTrainingTitleOrCompanyName").val();
    var city = $("#city :selected").val();


    if(title == ""){
        danger_toast_msg("Title should not empty");
        $("#SearchTrainingTitleOrCompanyName").focus();
        return false;
    }

    if(city == "Location"){
        danger_toast_msg("City should not empty");
        $("#city").focus();
        return false;
    }

    var SearchTrainingData = {
        title: title,
        city: city
    }

    $.ajax({
        type: "POST",
        url: "/SearchTrainings",
        data: SearchTrainingData,
        dataType: "html",
        beforeSend: function(){
            // Show image container
            $("#AjaxLoader").show();
        },
        success: function (data) {
            if(data.error){
                console.log(data);
                return false;
            }else{
                $("#SearchTrainingResults").html("");
                $("#SearchTrainingResults").append(data);
                return false;
            }
        },
        complete:function(data){
        // Hide image container
        $("#AjaxLoader").hide();
        }
    });
});













// Job Search
// $(this).ready( function() {  
//     $("#SearchJobsTitleOrCompanyName").autocomplete({  
//         minLength: 1,  
//         source:   
//         function(req, add){  
//             $.ajax({  
//                 url: "/SearchJobsTitle",  
//                 dataType: 'json',  
//                 type: 'POST',  
//                 data: req,  
//                 success:      
//                 function(data){  
//                     if(data.response =="true"){  
//                         $.each(data, function (i, key) { 
//                             add(key);  
//                             console.log(key);
//                         });
//                     }else{
//                         $.each(data, function (i, key) { 
//                             add(key);  
//                             console.log(key);
//                         });
                        
//                     }
//                 },  
//             });  
//         },  
             
//     });  
// });  


$(document).on("click", "#SearchJobs", function(){
    var title = $("#SearchJobsTitleOrCompanyName").val();
    var city = $("#city :selected").val();

    if(title == ""){
        danger_toast_msg("Title should not empty");
        $("#SearchJobsTitleOrCompanyName").focus();
        return false;
    }

    if(city == "Location"){
        danger_toast_msg("City should not empty");
        $("#city").focus();
        return false;
    }

    var SearchJobsData = {
        title: title,
        city: city
    }

    $.ajax({
        type: "POST",
        url: "/SearchJobs",
        data: SearchJobsData,
        dataType: "html",
        beforeSend: function(){
            // Show image container
            $("#AjaxLoader").show();
            $("#SearchJobsResults").hide();
            // return false;
        },
        success: function (data) {
            if(data.error){
                console.log(data);
                return false;
            }else{
                $("#SearchJobsResults").html("");
                $("#SearchJobsResults").append(data);
                return false;
            }
        },
        complete:function(data){
        // Hide image container
            $("#AjaxLoader").hide();
            $("#SearchJobsResults").show();
        }
    });
});



// Sort By Jobs

$(document).on("change", ".SortByJobs", function(){
    var sortby = $(this).val();

    $.ajax({
        type: "POST",
        url: "/SortByJobs",
        data: {sortby: sortby},
        dataType: "html",
        beforeSend: function(){
            // Show image container
            $("#AjaxLoader").show();
            $("#SearchJobsResults").hide();
            // return false;
        },
        success: function (data) {
            if(data.error){
                console.log(data);
                return false;
            }else{
                $("#SearchJobsResults").html("");
                $("#SearchJobsResults").append(data);
                return false;
            }
        },
        complete:function(data){
        // Hide image container
            $("#AjaxLoader").hide();
            $("#SearchJobsResults").show();
        }
    });
});

    
    
// Sort By Traininigs

$(document).on("change", ".SortByTrainings", function(){
    var sortby = $(this).val();

    $.ajax({
        type: "POST",
        url: "/SortByTrainings",
        data: {sortby: sortby},
        dataType: "html",
        beforeSend: function(){
            // Show image container
            $("#AjaxLoader").show();
            $("#SearchTrainingResults").hide();
            // return false;
        },
        success: function (data) {
            if(data.error){
                console.log(data);
                return false;
            }else{
                $("#SearchTrainingResults").html("");
                $("#SearchTrainingResults").append(data);
                return false;
            }
        },
        complete:function(data){
        // Hide image container
        $("#AjaxLoader").hide();
        $("#SearchTrainingResults").show();
        }
    });
});







// Candidate Search
// $(this).ready( function() {  
//     $("#SearchCandidatesRole").autocomplete({  
//         minLength: 1,  
//         source:   
//         function(req, add){  
//             $.ajax({  
//                 url: "/SearchCandidatesPositions",  
//                 dataType: 'json',  
//                 type: 'POST',  
//                 data: req,  
//                 success:      
//                 function(data){  
//                     if(data.response =="true"){  
//                         $.each(data, function (i, key) { 
//                             add(key);  
//                             console.log(key);
//                         });
//                     }else{
//                         $.each(data, function (i, key) { 
//                             add(key);  
//                             console.log(key);
//                         });
                        
//                     }
//                 },  
//             });  
//         },  
             
//     });  
// });  


$(document).on("click", "#SearchCandidates", function(){
    var role = $("#SearchCandidatesRole").val();
    var state = $("#state").val();
    var city = $("#city").val();


    var SearchCandidatesData = {
        role: role,
        state: state,
        city: city
    }

    $.ajax({
        type: "POST",
        url: "/SearchCandidates",
        data: SearchCandidatesData,
        dataType: "JSON",
        beforeSend: function(){
            // Show image container
            $("#AjaxLoader").show();
        },
        success: function (data) {
            if(data.error){
                console.log(data);
                return false;
            }else{
                $("#SearchCandidatesResults").html("");
                $.each(data, function (i, value) { 
                    
                    var SearchCandidates = "<div class='emply-resume-list square'><div class='emply-resume-thumb'><img src='candidate_profile/"+value.profile_pic+"' alt=''/></div><div class='emply-resume-info'><h3><a href='#' title=''>"+value.name+"</a></h3><span><i>"+value.role+"</i> at Atract Solutions</span><p><i class='la la-map-marker'></i>"+value.address+" / "+value.city+"</p></div><div class='shortlists'><a href='#'title=''>Shortlist <i class='la la-plus'></i></a></div></div>";

                    $("#SearchCandidatesResults").append(SearchCandidates);
                });
                return false;
            }
        },
        complete:function(data){
        // Hide image container
        $("#AjaxLoader").hide();
        }
    });
});











// Manage Jobs Search

$(document).on("click", "#SearchManagejobs", function(){
    var title = $("#Managejobs").val();

    if(title == ""){
        danger_toast_msg("Title should not empty");
        $("#Managejobs").focus();
        return false;
    }

    var SearchJobsData = {
        title: title
    }

    $.ajax({
        type: "POST",
        url: "/SearchManageJobs",
        data: SearchJobsData,
        dataType: "html",
        beforeSend: function(){
            // Show image container
            $("#AjaxLoader").show();
            $("#ManageJobResults").hide();
            // return false;
        },
        success: function (data) {
            if(data.error){
                danger_msg(data.message);
                return false;
            }else{
                $("#ManageJobResults").html("");
                $("#ManageJobResults").append(data);
                console.log(data);
                return false;
            }
        },
        complete:function(data){
        // Hide image container
            $("#AjaxLoader").hide();
            $("#ManageJobResults").show();
        }
    });
});


// Manage Trainings Search

$(document).on("click", "#SearchManagetrainings", function(){
    var title = $("#Managetrainings").val();

    if(title == ""){
        danger_toast_msg("Title should not empty");
        $("#Managetrainings").focus();
        return false;
    }

    var SearchJobsData = {
        title: title
    }

    $.ajax({
        type: "POST",
        url: "/SearchManageTrainings",
        data: SearchJobsData,
        dataType: "html",
        beforeSend: function(){
            // Show image container
            $("#AjaxLoader").show();
            $("#ManageTrainings").hide();
            // return false;
        },
        success: function (data) {
            if(data.error){
                danger_msg(data.message);
                return false;
            }else{
                $("#ManageTrainings").html("");
                $("#ManageTrainings").append(data);
                console.log(data);
                return false;
            }
        },
        complete:function(data){
        // Hide image container
            $("#AjaxLoader").hide();
            $("#ManageTrainings").show();
        }
    });
});


// Close Trainings Search

$(document).on("click", "#SearchClosetrainings", function(){
    var title = $("#Closetrainings").val();

    if(title == ""){
        danger_toast_msg("Title should not empty");
        $("#Closetrainings").focus();
        return false;
    }

    var SearchJobsData = {
        title: title
    }

    $.ajax({
        type: "POST",
        url: "/SearchCloseTrainings",
        data: SearchJobsData,
        dataType: "html",
        beforeSend: function(){
            // Show image container
            $("#AjaxLoader").show();
            $("#ManageTrainings").hide();
            // return false;
        },
        success: function (data) {
            if(data.error){
                danger_msg(data.message);
                return false;
            }else{
                $("#ManageTrainings").html("");
                $("#ManageTrainings").append(data);
                console.log(data);
                return false;
            }
        },
        complete:function(data){
        // Hide image container
            $("#AjaxLoader").hide();
            $("#ManageTrainings").show();
        }
    });
});




// Candidate Search

$(document).on("click", "#SearchManageCanddiates", function(){
    var title = $("#ManageInputCandidates").val();

    if(title == ""){
        danger_toast_msg("Candidate name should not empty");
        $("#ManageInputCandidates").focus();
        return false;
    }

    var SearchJobsData = {
        title: title
    }

    $.ajax({
        type: "POST",
        url: "/SearchManageCandidates",
        data: SearchJobsData,
        dataType: "html",
        beforeSend: function(){
            // Show image container
            $("#AjaxLoader").show();
            $("#ManageCandidates").hide();
            // return false;
        },
        success: function (data) {
            if(data){
                $("#ManageCandidates").html("");
                $("#ManageCandidates").append(data);
                
                // $("#JobsCount").text(data.length)/1000;
                return false;
            }else{
                // $("#JobsCount").text('0');
                $("#ManageCandidates").html("No results found...");
                return false;
            }
        },
        complete:function(data){
        // Hide image container
            $("#AjaxLoader").hide();
            $("#ManageCandidates").show();
        }
    });
});




function SearchAppliedTrainees(){
    var Trainingtitle = $("#TrainingName").val();
    

    $.ajax({
        type: "POST",
        url: "/SearchAppliedTrainees",
        data: {Trainingtitle: Trainingtitle},
        dataType: "html",
        beforeSend: function(){
            // Show image container
            $("#AjaxLoader").show();
            $("#AppliedTrainees").hide();
            // return false;
        },
        success: function (data) {
            if(data){
                $("#AppliedTrainees").html("");
                $("#AppliedTrainees").append(data);
                
                // $("#JobsCount").text(data.length)/1000;
                return false;
            }else{
                // $("#JobsCount").text('0');
                $("#AppliedTrainees").html("No results found...");
                return false;
            }
        },
        complete:function(data){
        // Hide image container
            $("#AjaxLoader").hide();
            $("#AppliedTrainees").show();
        }
    });
}


function SearchAppliedCandidates(){
    var Jobtitle = $("#JobsName").val();
    

    $.ajax({
        type: "POST",
        url: "/SearchAppliedCandidates",
        data: {Jobtitle: Jobtitle},
        dataType: "html",
        beforeSend: function(){
            // Show image container
            $("#AjaxLoader").show();
            $("#AppliedCandidates").hide();
            // return false;
        },
        success: function (data) {
            if(data){
                $("#AppliedCandidates").html("");
                $("#AppliedCandidates").append(data);
                
                // $("#JobsCount").text(data.length)/1000;
                return false;
            }else{
                // $("#JobsCount").text('0');
                $("#AppliedCandidates").html("No results found...");
                return false;
            }
        },
        complete:function(data){
        // Hide image container
            $("#AjaxLoader").hide();
            $("#AppliedCandidates").show();
        }
    });
}




// Close Jobs Search

$(document).on("click", "#SearchClosejobs", function(){
    var title = $("#Closejobs").val();

    if(title == ""){
        danger_toast_msg("Title should not empty");
        $("#Closejobs").focus();
        return false;
    }

    var SearchJobsData = {
        title: title
    }

    $.ajax({
        type: "POST",
        url: "/SearchCloseJobs",
        data: SearchJobsData,
        dataType: "html",
        beforeSend: function(){
            // Show image container
            $("#AjaxLoader").show();
            $("#ManageJobResults").hide();
            // return false;
        },
        success: function (data) {
            if(data.error){
                danger_msg(data.message);
                return false;
            }else{
                $("#ManageJobResults").html("");
                $("#ManageJobResults").append(data);
                console.log(data);
                return false;
            }
        },
        complete:function(data){
        // Hide image container
            $("#AjaxLoader").hide();
            $("#ManageJobResults").show();
        }
    });
});