/* Signup Popup */
$('.signin-candidate').on('click', function(){
    $('.signin-candidate-popup-box').fadeIn('fast');
    $('html').addClass('no-scroll');
});
$('.CandidatePopUpClose').on('click', function(){
    $('.signin-candidate-popup-box').fadeOut('fast');

    $("#register_candidate").hide();
    $("#login_candidate").show();
    $("#CandidateOTP").hide();
    $("#CandidateEmailForm").hide();

    $("#registerCandidate").show();
    $(".extra-login").show();
    $("#loginCandidate").hide();
    $("#registerCandidate").addClass('register-btn');
    $("#registerCandidate").removeClass('register_btn_clr');
    $("#login_form").addClass('register_form');

    $("#CandidateName").val("");
     $("#CandidatePassword").val("");
     $("#CandidateEmail").val("");
    $("#CandidatePhone").val("");
    $("#CandidateType option[value='sel']").prop('selected', true);
    // $("#CandidateType").val("Please Select Candidate Type").change();;

    $('html').removeClass('no-scroll');
});


/* Signin Popup */
$('.signin-employer').on('click', function(){
    $('.signup-employer-popup-box').fadeIn('fast');
    $('html').addClass('no-scroll');
});
$('#register_close').on('click', function(){

    $('.signup-employer-popup-box').fadeOut('fast');
    $("#register_employer").hide();
    $("#login_employer").show();

    $("#register_form").show();
    $(".extra-login").show();
    $("#EmployerEmailForm").hide();
    $("#EmployerOtpLoader").hide();
    $("#EmployerOTP").hide();

    $("#login_form").hide();
    $("#register_form").addClass('register_form');
    $("#login_form").addClass('register_form');

     $("#CompanyName").val("");
     $("#EmployerPassword").val("");
     $("#EmployerEmail").val("");
    $("#EmployerPhone").val("");

    // $('#login_form').removeClass('register_btn_clr');
    $('html').removeClass('no-scroll');
});


$('#CancelEmployer').on('click', function(){

    // $('.signup-employer-popup-box').fadeOut('fast');
    // $("#register_employer").hide();
    // $("#login_employer").show();

    // $("#register_form").show();
    // $(".extra-login").show();
    // $("#EmployerEmailForm").hide();
    // $("#EmployerOtpLoader").hide();

    // $("#register_form").show();
    // $("#login_form").hide();
    // $("#register_form").addClass('register_form');
    // $("#login_form").addClass('register_form');

    //  $("#CompanyName").val("");
    //  $("#EmployerPassword").val("");
    //  $("#EmployerEmail").val("");
    // $("#EmployerPhone").val("");

    $('.signup-employer-popup-box').fadeOut('fast');
    $("#register_employer").hide();
    $("#login_employer").show();

    $("#register_form").show();
    $(".extra-login").show();
    $("#EmployerEmailForm").hide();
    $("#EmployerOtpLoader").hide();
    $("#EmployerOTP").hide();

    $("#login_form").hide();
    $("#register_form").addClass('register_form');
    $("#login_form").addClass('register_form');

     $("#CompanyName").val("");
     $("#EmployerPassword").val("");
     $("#EmployerEmail").val("");
    $("#EmployerPhone").val("");

    
    // $('#login_form').removeClass('register_btn_clr');
    $('html').removeClass('no-scroll');
});


// Register Popup
$('#register_form').on('click', function(){
    $('#register_employer').fadeIn('fast');
    $("#login_employer").hide();
    $("#login_form").show();
    $("#register_form").hide();
    $(".register_form").addClass('register_btn_clr');
    $('.register_form').removeClass('register_form');

    $("#login_form").addClass('register_form');
    $('#login_form').removeClass('register_btn_clr');
    $('html').addClass('no-scroll');
    return false;
});

// Login Popup
$('#login_form').on('click', function(){
    $('#login_employer').fadeIn('fast');
    $("#register_employer").hide();
    $("#login_form").hide();
    $("#register_form").show();
    
    $(".register_form").addClass('register_btn_clr');
    $('.register_form').removeClass('register_form');

    $("#register_form").addClass('register_form');
    $('#register_form').removeClass('register_btn_clr');
    $('html').addClass('no-scroll');
    return false;
});





// Candidate Register Popup
$('#registerCandidate').on('click', function(){
    $('#register_candidate').fadeIn('fast');
    $("#login_candidate").hide();
    $("#loginCandidate").show();
    $("#registerCandidate").hide();
    $(".registerCandidate").addClass('register_btn_clr');
    $('.registerCandidate').removeClass('register_form');

    $(".register-candidate-form").addClass('Login-clr');
    $('.register-candidate-form').removeClass('register-clr');
    $('html').addClass('no-scroll');
    return false;
});

// Candidate Login Popup
$('#loginCandidate').on('click', function(){
    $('#login_candidate').fadeIn('fast');
    $("#register_candidate").hide();
    $("#loginCandidate").hide();
    $("#CandidateOTP").hide();
    $("#registerCandidate").show();
    $(".registerCandidate").addClass('register-btn');
    $('.registerCandidate').removeClass('register_btn_clr');

    // $("#register_form").addClass('register_btn_clr');
    // $('#register_form').removeClass('register_form');
    $('html').addClass('no-scroll');
    return false;
});



/* Candidate Profile  Popup */
$('.CandidateProfile').on('click', function(){
    $('.candidate-profile-popup-box').fadeIn('fast');
    $('html').addClass('no-scroll');
});
$('.close-popup').on('click', function(){
    $('.candidate-profile-popup-box').fadeOut('fast');
    $('html').removeClass('no-scroll');
});


/* Post new jobs  Popup */
$('.Post-New-Jobs').on('click', function(){
    $('.post-jobs-popup-box').fadeIn('fast');
    $('html').addClass('no-scroll');
});
$('.close-popup').on('click', function(){
    $('.post-jobs-popup-box').fadeOut('fast');
    $('html').removeClass('no-scroll');
});




/* Post training  Popup */
$('.Post-New-Training').on('click', function(){
    $('.post-trainings-popup-box').fadeIn('fast');
    $('html').addClass('no-scroll');
});
$('.close-popup').on('click', function(){
    $('.post-trainings-popup-box').fadeOut('fast');
    $('html').removeClass('no-scroll');
});

/* Post CA Jobs  Popup */
$('.Post-New-CAJobs').on('click', function(){
    $('.post-ca-jobs-popup-box').fadeIn('fast');
    $('html').addClass('no-scroll');
});
$('.close-popup').on('click', function(){
    $('.post-ca-jobs-popup-box').fadeOut('fast');
    $('html').removeClass('no-scroll');
});




/* Post training  Popup */
$('#CheckPercentage').on('click', function(){
    $('.percentage-notify-profile-popup-box').fadeIn('fast');
    $('html').addClass('no-scroll');
});
$('.close-popup').on('click', function(){
    $('.percentage-notify-profile-popup-box').fadeOut('fast');
    $('html').removeClass('no-scroll');
});



// /* Post New Link */
$('.post-new-dropsec > a').on('click', function(){
    $('.wishlist-dropdown').fadeToggle();
});
$('body').on('click', function(){
    $('.post-new-dropdown').fadeOut();
});
$(".post-new-dropsec").on("click",function(e){
    e.stopPropagation();
});



/* Employer profile  Popup */
// $('#EmployerProfileEdit').on('click', function(){

//     $.ajax({
//         type: "POST",
//         url: "/GetEmpProfileById",
//         data: "data",
//         dataType: "dataType",
//         success: function (data) {
            
//         }
//     });


//     $('.employer-profile-popup-box').fadeIn('fast');
//     $('html').addClass('no-scroll');
// });
// $('.close-popup').on('click', function(){
//     $('.employer-profile-popup-box').fadeOut('fast');
//     $('html').removeClass('no-scroll');
// });


/* EmployTraining End date Popup */
// $('#TrainingApprove').on('click', function(){
    function SetTrainingEndDate(TraineeId){
    // var TraineeId = $(this).data("traineeId");
    // console.log(TraineeId);
    $("#GetCandidateId").val(TraineeId);

    $('.approve-date-employer').fadeIn('fast');
    $('html').addClass('no-scroll');
}
    // });
$('.close-popup').on('click', function(){
    $('.approve-date-employer').fadeOut('fast');
    $('html').removeClass('no-scroll');
});




// Progress Bar
$(function() {

    $(".progress").each(function() {
  
      var value = $(this).attr('data-value');
      var left = $(this).find('.progress-left .progress-bar');
      var right = $(this).find('.progress-right .progress-bar');
  
      if (value > 0) {
        if (value <= 50) {
          right.css('transform', 'rotate(' + percentageToDegrees(value) + 'deg)')
        } else {
          right.css('transform', 'rotate(180deg)')
          left.css('transform', 'rotate(' + percentageToDegrees(value - 50) + 'deg)')
        }
      }
  
    })
  
    function percentageToDegrees(percentage) {
  
      return percentage / 100 * 360
  
    }
  
  });
  




  $('.SkillNames').click(function(){
    var text = "";
    $('.SkillNames:checked').each(function(){
        text += $(this).val()+',';
    });
    text = text.substring(0,text.length-1);
    
    $('#SkillNameInput').val(text);
});

 



$(document).on("click", "#SendMail", function(){
    var name = $("#Name").val();
    var email = $("#Email").val();
    var subject = $("#Subject").val();
    var message = $("#Message").val();

    if(name == ""){
        danger_toast_msg("Please type name");
        $("#Name").focus();
        return false;
    }

    if(email == ""){
        danger_toast_msg("Please type email address");
        $("#Email").focus();
        return false;
    }else if(IsEmail(email)==false){
        danger_toast_msg("Please check your email address");
        $('#Email').focus();
        return false;
    }

    if(subject == ""){
        danger_toast_msg("Please type subject");
        $("#Subject").focus();
        return false;
    }

    if(message == ""){
        danger_toast_msg("Please type message");
        $("#Message").focus();
        return false;
    }


    $.ajax({
        type: "POST",
        url: "/ContactUsMail",
        data: {name: name, email: email, subject: subject, message: message},
        dataType: "JSON",
        success: function (data) {
            if(data.error){
                danger_toast_msg(data.message);
                return false;
            }else{
                success_toast_msg(data.message);
                return false;
            }
        }
    });


});