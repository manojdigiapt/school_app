$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});



function BrowseEmployerPic(elemId) {
    var elem = document.getElementById(elemId);
    if(elem && document.createEvent) {
       var evt = document.createEvent("MouseEvents");
       evt.initEvent("click", true, false);
       elem.dispatchEvent(evt);
    }
    // readURL(this);
 }


 function readEmployerURL(input) {
    if (input.files && input.files[0]) {
      var reader = new FileReader();
      
      reader.onload = function(e) {
        $('#EmployerProfilePic').attr('src', e.target.result);
        $('.profile-head-width').attr('src', e.target.result);
      }
      
      reader.readAsDataURL(input.files[0]);
    }
  }

$("#file").change(function() {
    readEmployerURL(this);
  });
  
  

$(document).on("click", "#AddEmployerProfile", function(){
    // var fullname = $("#fullname").val();
    // var id = $("#id").val();
    var company_name = $("#company_name").val();
    var facebook = $("#facebook").val();
    var twitter = $("#twitter").val();
    var linkedin = $("#linkedin").val();
    var mobile = $("#mobile").val();
    var email = $("#email").val();
    var Emp_type = $("#Emp_type").val();
    var pan_no = $("#pan_no").val();
    var gst_number = $("#gst_number").val();
    var team_size = $("#team_size").val();
    var Emp_address = $("#Emp_address").val();
    var Emp_zipcode = $("#Emp_zipcode").val();
    var Emp_country = $("#Emp_country").val();
    var Emp_state = $("#Emp_state").val();
    var Emp_city = $("#Emp_city").val();

    if(company_name == ""){
        danger_toast_msg("Company name should not empty");
        $("#company_name").focus();
        return false;
    }

    if(mobile == ""){
        danger_toast_msg("Mobile should not empty");
        $("#mobile").focus();
        return false;
    }

    if(email == ""){
        danger_toast_msg("Please type email address");
        $("#email").focus();
        return false;
    }else if(IsEmail(email)==false){
        danger_toast_msg("Please check your email address");
        $('#email').focus();
        return false;
    }

    if(pan_no == ""){
        danger_toast_msg("Pan number should not empty");
        $("#pan_no").focus();
        return false;
    }

    if(gst_number == ""){
        danger_toast_msg("GST number should not empty");
        $("#gst_number").focus();
        return false;
    }

    if(team_size == ""){
        danger_toast_msg("Team size should not empty");
        $("#team_size").focus();
        return false;
    }

    if(Emp_zipcode == ""){
        danger_toast_msg("Zipcode should not empty");
        $("#Emp_zipcode").focus();
        return false;
    }
    
    if(Emp_country == ""){
        danger_toast_msg("Country should not empty");
        $("#Emp_country").focus();
        return false;
    }   

    if(Emp_state == ""){
        danger_toast_msg("State should not empty");
        $("#Emp_state").focus();
        return false;
    }   

    if(Emp_city == ""){
        danger_toast_msg("City should not empty");
        $("#Emp_city").focus();
        return false;
    }   

    if(Emp_address == ""){
        danger_toast_msg("Address should not empty");
        $("#Emp_address").focus();
        return false;
    }   

    var regExp = /[a-zA-z]{5}\d{4}[a-zA-Z]{1}/; 
    var txtpan = $("#pan_no").val(); 
    if (txtpan.length == 10 ) { 
     if( txtpan.match(regExp) ){ 
        // danger_toast_msg('PAN match found');
     }
     else {
        danger_toast_msg("Not a valid PAN number");
      event.preventDefault(); 
      $("#pan_no").focus();
          return false;
     } 
    } 
    else { 
        danger_toast_msg('Please enter 10 digits for a valid PAN number');
          event.preventDefault(); 
          $("#pan_no").focus();
          return false;
    } 


    var EmployerProfileData = {
        // name: name,
        company_name: company_name,
        facebook: facebook,
        twitter: twitter,
        linkedin: linkedin,
        mobile: mobile,
        email: email,
        Emp_type: Emp_type,
        pan_no: pan_no,
        gst_number: gst_number,
        team_size: team_size,
        Emp_address: Emp_address,
        Emp_zipcode: Emp_zipcode,
        Emp_country: Emp_country,
        Emp_state: Emp_state,
        Emp_city: Emp_city
    }


    $.ajax({
        type: "POST",
        url: "/Employer/InsertEmployerProfile",
        data: EmployerProfileData,
        dataType: "JSON",
        success: function (data) {
            if(data.error){
                danger_toast_msg(data.message);
                return false;
            }else{
                success_toast_msg(data.message);
                location.reload();
                return false;
            }
        }
    });
});







// Jobs
$(document).on("click", "#PostJobs", function(){
    var title = $("#title :selected").val();
    var address = $("#address").val();
    var zipcode = $("#zipcode").val();
    var country = $("#country").val();
    var state = $("#state").val();
    var city = $("#city").val();
    var job_type = $("#job_type :selected").val();
    var employee_type = $("#employee_type :selected").val();
    var salary_min = $("#salary_min").val();
    var salary_max = $("#salary_max").val();
    // var offered_salary = min_salary+ "-" + max_salary;
    var salary_type = $("#salary_type").val();
    var experience = $("#experience").val();
    var qualification = $("#qualification").val();
    var positions = $("#positions").val();
    // var description = CKEDITOR.instances['description'].getData();
    var description = $("#description").val();


    if(title == "Select job title"){
        danger_toast_msg("Title should not empty");
        $("#title").focus();
        return false;
    }

    if(employee_type == "Select Type"){
        danger_toast_msg("Employee type should not empty");
        $("#employee_type").focus();
        return false;
    }

    if(job_type == "Select job type"){
        danger_toast_msg("Job type should not empty");
        $("#job_type").focus();
        return false;
    }

    if(description == ""){
        danger_toast_msg("Description should not empty");
        $("#description").focus();
        return false;
    }

    if(qualification == ""){
        danger_toast_msg("Qualification should not empty");
        $("#qualification").focus();
        return false;
    }
    
    

    if(zipcode == ""){
        danger_toast_msg("Zipcode should not empty");
        $("#zipcode").focus();
        return false;
    }

    if(country == ""){
        danger_toast_msg("Country should not empty");
        $("#country").focus();
        return false;
    }

    if(state == ""){
        danger_toast_msg("State should not empty");
        $("#state").focus();
        return false;
    }

    if(city == ""){
        danger_toast_msg("City should not empty");
        $("#city").focus();
        return false;
    }

    if(address == ""){
        danger_toast_msg("Area should not empty");
        $("#address").focus();
        return false;
    }
    
    if(salary_min == ""){
        danger_toast_msg("Salary min should not empty");
        $("#salary_min").focus();
        return false;
    }

    if(salary_max == ""){
        danger_toast_msg("Salary max should not empty");
        $("#salary_max").focus();
        return false;
    }

    if(employee_type == "1"){
        if(positions == ""){
            danger_toast_msg("Positions should not empty");
            $("#positions").focus();
            return false;
        }
    }else{
        if(experience == ""){
            danger_toast_msg("Experience should not empty");
            $("#experience").focus();
            return false;
        }
        if(positions == ""){
            danger_toast_msg("Positions should not empty");
            $("#positions").focus();
            return false;
        }
    }
    
    if(parseInt(salary_min) > parseInt(salary_max)){
        danger_toast_msg("it can't be less than minimum salary");
        $("#salary_max").focus();
        return false;
    }
    
    

    var JobsData = {
        title: title,
        address: address,
        zipcode: zipcode,
        country: country,
        state: state,
        city: city,
        job_type: job_type,
        employee_type: employee_type,
        salary_min: salary_min,
        salary_max: salary_max,
        salary_type: salary_type,
        experience: experience,
        qualification: qualification,
        positions: positions,
        description: description
    }


    $.ajax({
        type: "POST",
        url: "/Employer/AddJobs",
        data: JobsData,
        dataType: "JSON",
        success: function (data) {
            if(data.error){
                danger_toast_msg(data.message);
                return false;
            }else{
                $('.post-jobs-popup-box').fadeOut('fast');
                $('html').removeClass('no-scroll');
                success_toast_msg(data.message);
                location.reload();
                return false;
            }
        }
    });

});

function EditJobs(id){
    $.ajax({
        type: "POST",
        url: "/Jobs/GetJobsById/"+id,
        data: {id: id},
        dataType: "JSON",
        success: function (data) {
            if(data.error){
                danger_msg(data.message);
                return false;
            }else{
                $("#JobId").val(data.id);

                if(data.employee_type == 1){
                    $("#EditExperienceRow").hide();
                }else{
                    $("#EditExperienceRow").show();
                }

                $("#Edit_JobTitle option[value="+data.title+"]").attr('selected', 'selected');

                $("#Edit_job_type option[value="+data.job_type+"]").attr('selected', 'selected');

                $("#Edit_title option[value="+data.title+"]").attr('selected', 'selected');

                $("#Edit_employee_type option[value="+data.employee_type+"]").attr('selected', 'selected');

                // $("#Edit_title").val(data.title);
                // CKEDITOR.instances['Edit_description'].setData(data.job_description);
                // $("#Edit_qualification option[value="+data.qualification+"]").attr('selected', 'selected');
                $("#Edit_description").val(data.job_description);

                $("#Edit_qualification").val(data.qualification);
                
                
                $("#Edit_address").val(data.location);
                $("#Edit_zipcode").val(data.zip_code);
                $("#Edit_country").val(data.country);
                $("#Edit_state").val(data.state);
                $("#Edit_city").val(data.city);

                $("#Edit_salary_min").val(data.salary_min);
                $("#Edit_salary_max").val(data.salary_max);

                $("#Edit_salary_type option[value="+data.salary_type+"]").attr('selected', 'selected');

                $("#Edit_experience").val(data.experience);
                $("#Edit_positions").val(data.total_positions);

                

                $('.edit-jobs-popup-box').fadeIn('fast');
                $('html').addClass('no-scroll');
                return false;
            }
        }
    });
}

$('.close-popup').on('click', function(){
    $('.edit-jobs-popup-box').fadeOut('fast');
    $('html').removeClass('no-scroll');
});



$(document).on("keypress", "#SearchLocationByKeypress", function(){
    var Location = $(this).val();

    $.ajax({
        type: "POST",
        url: "/SearchLocationByKeypress",
        data: {Location: Location},
        dataType: "html",
        success: function (data) {
            if(data){
                $("#AppendLocation").html(data);
                return false;
            }else{
                $("#AppendLocation").html("No results");
                return false;
            }
        }
    });
});



$(document).on("keypress", "#SearchClosJobsLocationByKeypress", function(){
    var Location = $(this).val();

    $.ajax({
        type: "POST",
        url: "/SearchClosJobsLocationByKeypress",
        data: {Location: Location},
        dataType: "html",
        success: function (data) {
            if(data){
                $("#AppendLocation").html(data);
                return false;
            }else{
                $("#AppendLocation").html("No results");
                return false;
            }
        }
    });
});


$(document).on("keypress", "#SearchTrainingLocationByKeypress", function(){
    var Location = $(this).val();

    $.ajax({
        type: "POST",
        url: "/SearchTrainingLocationByKeypress",
        data: {Location: Location},
        dataType: "html",
        success: function (data) {
            if(data){
                $("#AppendLocation").html(data);
                return false;
            }else{
                $("#AppendLocation").html("No results");
                return false;
            }
        }
    });
});


$(document).on("keypress", "#SearchCloseTrainingLocationByKeypress", function(){
    var Location = $(this).val();

    $.ajax({
        type: "POST",
        url: "/SearchCloseTrainingLocationByKeypress",
        data: {Location: Location},
        dataType: "html",
        success: function (data) {
            if(data){
                $("#AppendLocation").html(data);
                return false;
            }else{
                $("#AppendLocation").html("No results");
                return false;
            }
        }
    });
});


$(document).on("keypress", "#SearchDurationByManageTraining", function(){
    var Duration = $(this).val();

    $.ajax({
        type: "POST",
        url: "/SearchDurationByManageTraining",
        data: {Duration: Duration},
        dataType: "html",
        success: function (data) {
            if(data){
                $("#AppendDuration").html(data);
                return false;
            }else{
                $("#AppendDuration").html("No results");
                return false;
            }
        }
    });
});


$(document).on("keypress", "#SearchDurationByCloseTraining", function(){
    var Duration = $(this).val();

    $.ajax({
        type: "POST",
        url: "/SearchDurationByCloseTraining",
        data: {Duration: Duration},
        dataType: "html",
        success: function (data) {
            if(data){
                $("#AppendDuration").html(data);
                return false;
            }else{
                $("#AppendDuration").html("No results");
                return false;
            }
        }
    });
});



$(document).on("keypress", "#SearchFresherDurationByManageTraining", function(){
    var Duration = $(this).val();

    $.ajax({
        type: "POST",
        url: "/SearchFresherDurationByManageTraining",
        data: {Duration: Duration},
        dataType: "html",
        success: function (data) {
            if(data){
                $("#AppendDuration").html(data);
                return false;
            }else{
                $("#AppendDuration").html("No results");
                return false;
            }
        }
    });
});



$(document).on("keypress", "#SearchCandidateLocationByKeypress", function(){
    var Location = $(this).val();

    $.ajax({
        type: "POST",
        url: "/SearchCandidateLocationByKeypress",
        data: {Location: Location},
        dataType: "html",
        success: function (data) {
            if(data){
                $("#AppendLocation").html(data);
                return false;
            }else{
                $("#AppendLocation").html("No results");
                return false;
            }
        }
    });
});



$(document).on("keypress", "#SearchJobsLocationByKeypress", function(){
    var Location = $(this).val();

    $.ajax({
        type: "POST",
        url: "/SearchJobsLocationByKeypress",
        data: {Location: Location},
        dataType: "html",
        success: function (data) {
            if(data){
                $("#AppendLocation").html(data);
                return false;
            }else{
                $("#AppendLocation").html("No results");
                return false;
            }
        }
    });
});





$(document).on("click", "#UpdateJobs", function(){
    var id = $("#JobId").val();
    var title = $("#Edit_JobTitle :selected").val();
    var EmployeeType = $("#Edit_employee_type").val();
    var zipcode = $("#Edit_zipcode").val();
    var country = $("#Edit_country").val();
    var state = $("#Edit_state").val();
    var city = $("#Edit_city").val();
    var address = $("#Edit_address").val();
    var job_type = $("#Edit_job_type :selected").val();
    var industry = $("#Edit_industry :selected").val();
    var salary_min = $("#Edit_salary_min").val();
    var salary_max = $("#Edit_salary_max").val();
    // var offered_salary = min_salary+ "-" + max_salary;
    var salary_type = $("#Edit_salary_type").val();
    var experience = $("#Edit_experience").val();
    var qualification = $("#Edit_qualification").val();
    var positions = $("#Edit_positions").val();
    // var description = CKEDITOR.instances['Edit_description'].getData();
    var description =  $("#Edit_description").val();


    if(title == "Select job title"){
        danger_toast_msg("Title should not empty");
        $("#Edit_JobTitle").focus();
        return false;
    }

    if(employee_type == "Select Type"){
        danger_toast_msg("Employee type should not empty");
        $("#Edit_employee_type").focus();
        return false;
    }

    if(job_type == "Select job type"){
        danger_toast_msg("Job type should not empty");
        $("#Edit_job_type").focus();
        return false;
    }

    if(description == ""){
        danger_toast_msg("Description should not empty");
        $("#Edit_description").focus();
        return false;
    }

    if(qualification == ""){
        danger_toast_msg("Qualification should not empty");
        $("#Edit_qualification").focus();
        return false;
    }
    
    if(address == ""){
        danger_toast_msg("Area should not empty");
        $("#Edit_address").focus();
        return false;
    }

    if(zipcode == ""){
        danger_toast_msg("Zipcode should not empty");
        $("#Edit_zipcode").focus();
        return false;
    }

    if(country == ""){
        danger_toast_msg("Country should not empty");
        $("#Edit_country").focus();
        return false;
    }

    if(state == ""){
        danger_toast_msg("State should not empty");
        $("#Edit_state").focus();
        return false;
    }

    if(city == ""){
        danger_toast_msg("City should not empty");
        $("#Edit_city").focus();
        return false;
    }


    if(salary_min == ""){
        danger_toast_msg("Salary min should not empty");
        $("#Edit_salary_min").focus();
        return false;
    }

    if(salary_max == ""){
        danger_toast_msg("Salary max should not empty");
        $("#Edit_salary_max").focus();
        return false;
    }

    // if(employee_type == "1"){
    //     if(positions == ""){
    //         danger_toast_msg("Positions should not empty");
    //         $("#Edit_positions").focus();
    //         return false;
    //     }
    // }else{
    //     if(experience == ""){
    //         danger_toast_msg("Experience should not empty");
    //         $("#Edit_experience").focus();
    //         return false;
    //     }
    //     if(positions == ""){
    //         danger_toast_msg("Positions should not empty");
    //         $("#Edit_positions").focus();
    //         return false;
    //     }
    // }

    if(parseInt(salary_min) > parseInt(salary_max)){
        danger_toast_msg("it can't be less than minimum salary");
        $("#Edit_salary_max").focus();
        return false;
    }

    var JobsData = {
        id: id,
        title: title,
        EmployeeType: EmployeeType,
        address: address,
        zipcode: zipcode,
        country: country,
        state: state,
        city: city,
        job_type: job_type,
        industry: industry,
        salary_min: salary_min,
        salary_max: salary_max,
        salary_type: salary_type,
        experience: experience,
        qualification: qualification,
        positions: positions,
        description: description
    }


    $.ajax({
        type: "POST",
        url: "/Employer/UpdateJobs",
        data: JobsData,
        dataType: "JSON",
        success: function (data) {
            if(data.error){
                danger_toast_msg(data.message);
                return false;
            }else{
                $('.post-jobs-popup-box').fadeOut('fast');
                $('html').removeClass('no-scroll');
                success_toast_msg(data.message);
                location.reload();
                return false;
            }
        }
    });


});


function CheckEmployeeType(){
    if($("#employee_type").val() == 1){
        // $("#exp_show").show();
        $("#CheckExperienced").hide();
        return false;
    }else{
        $("#CheckExperienced").show();
        return false;
    }
}

function CheckEditEmployeeType(){
    if($("#Edit_employee_type").val() == 1){
        // $("#exp_show").show();
        $("#EditExperienceRow").hide();
        return false;
    }else{
        $("#EditExperienceRow").show();
        return false;
    }
}


function CheckCareerLevel(){
    if($("#career_level").val() == 2){
        // $("#exp_show").show();
        $("#experience_year").css("background","#fff");
        $('#experience_year').attr("disabled", false);
        $("#experience_month").css("background","#fff");
        $('#experience_month').attr("disabled", false);
    }else{
        // $("#exp_show").hide();
        $("#experience_year").css("background","#8080803b");
        $('#experience_year').attr("disabled", true);
        $("#experience_month").css("background","#8080803b");
        $('#experience_month').attr("disabled", true);
    }
}


$("#CheckSameAddress").change(function() {
    if($(this).prop('checked')) {
        $("#address").val($("#GetAddress").val());
        $("#zipcode").val($("#GetZipcode").val());
        $("#country").val($("#GetCountry").val());
        $("#state").val($("#GetState").val());
        $("#city").val($("#GetCity").val());
    } else {
        $("#address").val("");
        $("#zipcode").val("");
        $("#country").val("");
        $("#state").val("");
        $("#city").val("");
    }
});



// Apply Jobs
$(document).on("click", "#ApplyJobs", function(){
    
    var JobsId = $("#JobsId").val();

    $.ajax({
        type: "POST",
        url: "/ApplyJobs",
        data: {JobsId: JobsId},
        dataType: "JSON",
        beforeSend: function(){
            // Show image container
            $("#ApplyBtnLoader").show();
            $("#ApplyJobs").hide();
        },
        success: function (data) {
            if(data.error){
                danger_toast_msg(data.message);
                return false;
            }else{
                success_toast_msg(data.message);
                return false;
            }
        },
        complete:function(data){
        // Hide image container
            $("#ApplyBtnLoader").hide();
            $("#ApplyJobs").hide();
            $("#AppliedSuccessfully").show();
        }
    });
});








// function CheckLastDate(){
//     var start_date = $("#start_date").val();
//     var last_date = $("#last_date").val();

//     if (start_date > last_date){
//         // Do something
//         danger_toast_msg("Last date must be greater than start date");
//         $("#last_date").focus();
//         return false;
//     }
// }




$("#stipend").keypress(function (e) {
    //if the letter is not digit then display error and don't type anything
    if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
       //display error message
    //    $("#errmsg").html("Digits Only").show().fadeOut("slow");
              return false;
   }
  });

  $("#Edit_stipend").keypress(function (e) {
    //if the letter is not digit then display error and don't type anything
    if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
       //display error message
    //    $("#errmsg").html("Digits Only").show().fadeOut("slow");
              return false;
   }
  });


// Trainings
$(document).on("click", "#PostTrainings", function(){
    var title = $("#training_title :selected").val();
    var start_date = $("#start_date").val();
    var duration_days = $("#duration_days").val();
    var duration_day_month = $("#duration").val();
    var duration = duration_days + " "+ duration_day_month;
    var stipend = $("#stipend").val();
    var last_date = $("#last_date").val();
    var completion = $("#completion :selected").val();
    // var description = CKEDITOR.instances['training_description'].getData();
    var description = $("#training_description").val();

    if(title == "Select training title"){
        danger_toast_msg("Title should not empty");
        $("#training_title").focus();
        return false;
    }

    if(start_date == ""){
        danger_toast_msg("Start date should not empty");
        $("#start_date").focus();
        return false;
    }

    if(duration == ""){
        danger_toast_msg("Duration should not empty");
        $("#duration").focus();
        return false;
    }

    if(stipend == ""){
        danger_toast_msg("Stipend should not empty");
        $("#stipend").focus();
        return false;
    }

    if(last_date == ""){
        danger_toast_msg("Last date should not empty");
        $("#last_date").focus();
        return false;
    }

    if(description == ""){
        danger_toast_msg("Description should not empty");
        $("#training_description").focus();
        return false;
    }

    if(completion == "Select type"){
        danger_toast_msg("Completion should not empty");
        $("#completion").focus();
        return false;
    }

    

    // if (start_date > last_date){
    //     // Do something
    //     danger_toast_msg("Last date must be greater than start date");
    //     $("#last_date").focus();
    //     return false;
    // }

    var TrainingData = {
        title: title,
        start_date: start_date,
        duration: duration,
        stipend: stipend,
        last_date: last_date,
        completion: completion,
        description: description
    }


    $.ajax({
        type: "POST",
        url: "/Employer/PostTrainings",
        data: TrainingData,
        dataType: "JSON",
        success: function (data) {
            if(data.error){
                danger_toast_msg(data.message);
                return false;
            }else{
                $('.post-trainings-popup-box').fadeOut('fast');
                $('html').removeClass('no-scroll');
                success_toast_msg(data.message);
                location.reload();
                return false;
            }
        }
    });

});

function EditTraining(id){
    $.ajax({
        type: "POST",
        url: "/Employer/GetTrainingsById/"+id,
        data: {id: id},
        dataType: "JSON",
        success: function (data) {
            if(data.error){
                danger_msg(data.message);
                return false;
            }else{
                var duration_data = data.duration
                var getduration = duration_data.split(" ");
                // var str1 = ret[0];
                // var str2 = ret[1];

                $("#TrainingId").val(data.id);

                $("#Edit_training_title option[value="+data.title+"]").attr('selected', 'selected');

                

                $("#Edit_start_date").val(data.start_date);
                
                $("#Edit_duration_days option[value="+getduration[0]+"]").attr('selected', 'selected');

                $("#Edit_duration option[value="+getduration[1]+"]").attr('selected', 'selected');

                // $("#Edit_stipend option[value="+data.stipend+"]").attr('selected', 'selected');

                $("#Edit_stipend").val(data.stipend);

                $("#Edit_last_date").val(data.last_date_of_application);

                // CKEDITOR.instances['Edit_description'].setData(data.description);

                $("#Edit_completion option[value="+data.training_completion+"]").attr('selected', 'selected');

                $("#Edit_description").val(data.description);


                $('.edit-trainings-popup-box').fadeIn('fast');
                $('html').addClass('no-scroll');
                return false;
            }
        }
    });
}

$('.close-popup').on('click', function(){
    $('.edit-trainings-popup-box').fadeOut('fast');
    $('html').removeClass('no-scroll');
});

$(document).on("click", "#UpdateTrainings", function(){
    var id = $("#TrainingId").val();
    var title = $("#Edit_training_title").val();
    var start_date = $("#Edit_start_date").val();
    // var duration = $("#Edit_duration").val();
    var duration_days = $("#Edit_duration_days").val();
    var duration_day_month = $("#Edit_duration").val();
    var duration = duration_days + " "+ duration_day_month;

    var stipend = $("#Edit_stipend").val();
    var last_date = $("#Edit_last_date").val();
    var completion = $("#Edit_completion :selected").val();
    // var description = CKEDITOR.instances['Edit_description'].getData();
    var description = $("#Edit_description").val();

    if(title == "Select training title"){
        danger_toast_msg("Title should not empty");
        $("#Edit_training_title").focus();
        return false;
    }

    if(start_date == ""){
        danger_toast_msg("Start date should not empty");
        $("#Edit_start_date").focus();
        return false;
    }

    if(duration == ""){
        danger_toast_msg("Duration should not empty");
        $("#Edit_duration_days").focus();
        return false;
    }

    if(stipend == ""){
        danger_toast_msg("Stipend should not empty");
        $("#Edit_stipend").focus();
        return false;
    }

    if(last_date == ""){
        danger_toast_msg("Last date should not empty");
        $("#Edit_last_date").focus();
        return false;
    }

    if(description == ""){
        danger_toast_msg("Description should not empty");
        $("#Edit_description").focus();
        return false;
    }

    if(completion == "Select type"){
        danger_toast_msg("Completion should not empty");
        $("#Edit_completion").focus();
        return false;
    }

    

    if (start_date > last_date){
        // Do something
        danger_toast_msg("Last date must be greater than start date");
        $("#Edit_last_date").focus();
        return false;
    }

    var TrainingData = {
        id: id,
        title: title,
        start_date: start_date,
        duration: duration,
        stipend: stipend,
        last_date: last_date,
        completion: completion,
        description: description
    }


    $.ajax({
        type: "POST",
        url: "/Employer/UpdateTrainings",
        data: TrainingData,
        dataType: "JSON",
        success: function (data) {
            if(data.error){
                danger_toast_msg(data.message);
                return false;
            }else{
                $('.post-trainings-popup-box').fadeOut('fast');
                $('html').removeClass('no-scroll');
                success_toast_msg(data.message);
                location.reload();
                return false;
            }
        }
    });

});


function CheckTrainings(){
    var training_title = $("#training_title").val();

    if(training_title == "others"){
        // alert("others");
        $("#otherTitleShow").show();
        $("#other_title").attr("id", "training_title");
        $("#training_title").attr("id", "other_title");
        return false;
    }else{
        $("#otherTitleShow").hide();
        $(".title").attr("id", "training_title");
        $(".other_title").attr("id", "other_title");
        return false;
    }
}



// Apply Trainings
$(document).on("click", "#ApplyTrainings", function(){
   

    var TrainingId = $("#TrainingId").val();

    $.ajax({
        type: "POST",
        url: "/ApplyTrainings",
        data: {TrainingId: TrainingId},
        dataType: "JSON",
        beforeSend: function(){
            // Show image container
            $("#ApplyBtnLoader").show();
            $("#ApplyTrainings").hide();
        },
        success: function (data) {
            if(data.error){
                danger_toast_msg(data.message);
                return false;
            }else{
                success_toast_msg(data.message);
                return false;
            }
        },
        complete:function(data){
        // Hide image container
            $("#ApplyBtnLoader").hide();
            $("#ApplyTrainings").hide();
            $("#AppliedSuccessfully").show();
        }
    });
});




// Rating Stars

// $(document).ready(function(event) {
//     event.preventDefault();
//     $('#AddReviews').on('click', function() {
//        alert( $(this).data('caniddate_id') , $(this).data('train_id'));
//     });
//  });

function AddReviews(CandidateId, TrainingId){
    $("#TrainingId").val(TrainingId);
    $("#CandidateId").val(CandidateId);

    $("#AddReviews").modal("show");
}

 
$(document).on("click", "#SubmitRatings", function(){
    
    var Ratings = $("#RatingStars").val();
    var feedback = $("#feedback").val();
    var TrainingId = $("#TrainingId").val();
    var CandidateId = $("#CandidateId").val();


    var RatingsData = {
        Ratings: Ratings,
        feedback: feedback,
        TrainingId: TrainingId,
        CandidateId: CandidateId
    }

    $.ajax({
        type: "POST",
        url: "/AddStars",
        data: RatingsData,
        dataType: "JSON",
        success: function (data) {
            if(data.error){
                danger_msg(data.message);
                return false;
            }else{
                success_msg(data.message);
                location.reload();
                return false;
            }
        }
    });
    
});






//   function GenerateCertificate(){
    
//     $.ajax({
//         type: "POST",
//         url: "/GetStudentDataByCertificates",
//         data: "data",
//         dataType: "JSON",
//         success: function (data) {
//             $.each(data, function (i, key) { 
//                  $("#CandidateName").text(key.name);
//                  $("#CourseName").text(key.title);
//             });
//             window.open("/GenerateCertificate");
//         }
//     });

//   }

$(document).on("click", "#LoadmoreCandidates", function(){
    var CandidateId = $(this).data('id');


    $.ajax({
        type: "POST",
        url: "/LoadmoreCandidates",
        data: {CandidateId: CandidateId},
        dataType: "text",
        beforeSend: function(){
            $("#ApplyBtnLoader").show();
        },
        success: function (data) {
            if(data != ''){
                console.log(data);
                $('#remove_candidate_load_more').remove();
                $("#SearchCandidatesResults").append(data);
            }else{
                console.log(data);
                $("#LoadmoreCandidates").html("No candidates are found...");
                return false;
            }
        },
        complete:function(data){
            $("#ApplyBtnLoader").hide();
        }
    });
    
});



function SalaryType(){
    var SalaryType = $("#salary_type").val();
    if(SalaryType == 1){
        $("#salary_min").val("");
        $("#salary_max").val("");
        $("#salary_min").attr("maxlength", 5);
        $("#salary_max").attr("maxlength", 5);
    }else{
        $("#salary_min").val("");
        $("#salary_max").val("");
        $("#salary_min").attr("maxlength", 6);
        $("#salary_max").attr("maxlength", 6);
    }
}   


$("#mobile").keypress(function (e) {
    //if the letter is not digit then display error and don't type anything
    if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
       //display error message
    //    $("#errmsg").html("Digits Only").show().fadeOut("slow");
              return false;
   }
  });

  $("#Edit_positions").keypress(function (e) {
    //if the letter is not digit then display error and don't type anything
    if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
       //display error message
    //    $("#errmsg").html("Digits Only").show().fadeOut("slow");
              return false;
   }
  });

  $("#Edit_zipcode").keypress(function (e) {
    //if the letter is not digit then display error and don't type anything
    if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
       //display error message
    //    $("#errmsg").html("Digits Only").show().fadeOut("slow");
              return false;
   }
  });

  $("#positions").keypress(function (e) {
    //if the letter is not digit then display error and don't type anything
    if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
       //display error message
    //    $("#errmsg").html("Digits Only").show().fadeOut("slow");
              return false;
   }
  });

  $("#Ca_positions").keypress(function (e) {
    //if the letter is not digit then display error and don't type anything
    if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
       //display error message
    //    $("#errmsg").html("Digits Only").show().fadeOut("slow");
              return false;
   }
  });


  $("#team_size").keypress(function (e) {
    //if the letter is not digit then display error and don't type anything
    if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
       //display error message
    //    $("#errmsg").html("Digits Only").show().fadeOut("slow");
              return false;
   }
  });


  $("#Emp_zipcode").keypress(function (e) {
    //if the letter is not digit then display error and don't type anything
    if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
       //display error message
    //    $("#errmsg").html("Digits Only").show().fadeOut("slow");
              return false;
   }
  });


  $("#zipcode").keypress(function (e) {
    //if the letter is not digit then display error and don't type anything
    if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
       //display error message
    //    $("#errmsg").html("Digits Only").show().fadeOut("slow");
              return false;
   }
  });


  $("#Ca_Edit_zipcode").keypress(function (e) {
    //if the letter is not digit then display error and don't type anything
    if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
       //display error message
    //    $("#errmsg").html("Digits Only").show().fadeOut("slow");
              return false;
   }
  });



$("#salary_min").keypress(function (e) {
    //if the letter is not digit then display error and don't type anything
    if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
       //display error message
    //    $("#errmsg").html("Digits Only").show().fadeOut("slow");
              return false;
   }
  });

  $("#salary_max").keypress(function (e) {
    //if the letter is not digit then display error and don't type anything
    if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
       //display error message
    //    $("#errmsg").html("Digits Only").show().fadeOut("slow");
              return false;
   }
  });


  $("#Edit_salary_min").keypress(function (e) {
    //if the letter is not digit then display error and don't type anything
    if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
       //display error message
    //    $("#errmsg").html("Digits Only").show().fadeOut("slow");
              return false;
   }
  });

  $("#Edit_salary_max").keypress(function (e) {
    //if the letter is not digit then display error and don't type anything
    if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
       //display error message
    //    $("#errmsg").html("Digits Only").show().fadeOut("slow");
              return false;
   }
  });


  $("#Edit_Ca_zipcode").keypress(function (e) {
    //if the letter is not digit then display error and don't type anything
    if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
       //display error message
    //    $("#errmsg").html("Digits Only").show().fadeOut("slow");
              return false;
   }
  });

  $("#Edit_Ca_positions").keypress(function (e) {
    //if the letter is not digit then display error and don't type anything
    if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
       //display error message
    //    $("#errmsg").html("Digits Only").show().fadeOut("slow");
              return false;
   }
  });





  

  $(document).on("click", "#UpdateEmployerPassword", function(){
    var OldPassword = $("#old_password").val();
    var NewPassword = $("#new_password").val();
    var VerifyPassword = $("#verify_password").val();

    if(OldPassword == ""){
        danger_toast_msg("Old password should not empty");
        $("#old_password").focus();
        return false;
    }

    if(NewPassword == ""){
        danger_toast_msg("New password should not empty");
        $("#new_password").focus();
        return false;
    }

    if(VerifyPassword == ""){
        danger_toast_msg("Verify password should not empty");
        $("#verify_password").focus();
        return false;
    }
    
    var CandidatePassword = {
        OldPassword: OldPassword,
        NewPassword: NewPassword,
        VerifyPassword: VerifyPassword
    }

    $.ajax({
        type: "POST",
        url: "/UpdateEmployerPassword",
        data: CandidatePassword,
        dataType: "JSON",
        success: function (data) {
            if(data.error){
                danger_msg(data.message);
                return false;
            }else{
                success_msg(data.message);
                return false;
            }
        }
    });

});



// DownloadCV
function DownloadCV(id, JobsId){
    $.ajax({
        type: "POST",
        url: "/DownloadCV/"+id+"/"+JobsId,
        data: {id: id},
        dataType: "JSON",
        success: function (data) {
            if(data.error){
                danger_toast_msg(data.message);
                return false;
            }else{
                // success_toast_msg(data.message);
                window.open($("#DownloadCVPath").text(), '_blank')
                // return false;
            }
        }
    });
}






//  Update Profile Pic
function BrowseProfilePic(elemId) {
    var elem = document.getElementById(elemId);
    if(elem && document.createEvent) {
       var evt = document.createEvent("MouseEvents");
       evt.initEvent("click", true, false);
       elem.dispatchEvent(evt);
    }

    // readURL(this);
 }

 $("#updateProfilePic").change(function() {

    // var cv_id = $("#trainee_cv_id").val();
    var CVData  = new FormData();

    CVData.append('pic', $('#updateProfilePic')[0].files[0]);

    // console.log(CVData);
    // return false;
    $.ajax({
        type: "POST",
        url: "/Employer/UpdateProfiePic",
        data: CVData,
        dataType: "JSON",
        contentType: false,
        processData: false,
        success: function (data) {
            if(data.error){
                danger_toast_msg(data.message);
                return false;
            }else{
                success_toast_msg(data.message);
                location.reload();
                return false;
            }
        }
    });
 });




//  Clear all Manage jobs
$(document).on("click", "#ClearAllManageJobs", function(){
    $('input[type="checkbox"]').removeAttr('checked');
    $('#AppliedCount').text(1);



    $.ajax({
        type: "POST",
        url: "/ClearAllManageJobs",
        dataType: "html",
        beforeSend: function(){
            // Show image container
            $("#AjaxLoader").show();
            $("#ManageJobResults").hide();
            // return false;
        },
        success: function (data) {
            if(data.error){
                danger_msg(data.message);
                return false;
            }else{
                $("#ManageJobResults").html("");
                $("#ManageJobResults").append(data);
                console.log(data);
                return false;
            }
        },
        complete:function(data){
        // Hide image container
            $("#AjaxLoader").hide();
            $("#ManageJobResults").show();
        }
    });
});



//  Clear all Manage trainings
$(document).on("click", "#ClearAllManageTrainings", function(){
    $('input[type="checkbox"]').removeAttr('checked');
    $('#AppliedCount').text(1);



    $.ajax({
        type: "POST",
        url: "/ClearAllManageTrainings",
        dataType: "html",
        beforeSend: function(){
            // Show image container
            $("#AjaxLoader").show();
            $("#ManageTrainings").hide();
            // return false;
        },
        success: function (data) {
            if(data.error){
                danger_msg(data.message);
                return false;
            }else{
                $("#ManageTrainings").html("");
                $("#ManageTrainings").append(data);
                console.log(data);
                return false;
            }
        },
        complete:function(data){
        // Hide image container
            $("#AjaxLoader").hide();
            $("#ManageTrainings").show();
        }
    });
});




//  Clear all Manage Candidates
$(document).on("click", "#ClearAllManageCandidates", function(){
    $('input[type="checkbox"]').removeAttr('checked');
    $('#AppliedCount').text(1);



    $.ajax({
        type: "POST",
        url: "/ClearAllManageCandidates",
        dataType: "html",
        beforeSend: function(){
            // Show image container
            $("#AjaxLoader").show();
            $("#ManageCandidates").hide();
            // return false;
        },
        success: function (data) {
            if(data){
                $("#ManageCandidates").html("");
                $("#ManageCandidates").append(data);
                
                // $("#JobsCount").text(data.length)/1000;
                return false;
            }else{
                // $("#JobsCount").text('0');
                $("#ManageCandidates").html("No results found...");
                return false;
            }
        },
        complete:function(data){
        // Hide image container
            $("#AjaxLoader").hide();
            $("#ManageCandidates").show();
        }
    });
});





//  Clear all Closed jobs
$(document).on("click", "#ClearAllClosedJobs", function(){
    $('input[type="checkbox"]').removeAttr('checked');
    $('#AppliedCount').text(1);



    $.ajax({
        type: "POST",
        url: "/ClearAllCloseJobs",
        dataType: "html",
        beforeSend: function(){
            // Show image container
            $("#AjaxLoader").show();
            $("#ManageJobResults").hide();
            // return false;
        },
        success: function (data) {
            if(data.error){
                danger_msg(data.message);
                return false;
            }else{
                $("#ManageJobResults").html("");
                $("#ManageJobResults").append(data);
                console.log(data);
                return false;
            }
        },
        complete:function(data){
        // Hide image container
            $("#AjaxLoader").hide();
            $("#ManageJobResults").show();
        }
    });
});



//  Clear all Closed training
$(document).on("click", "#ClearAllClosedTrainings", function(){
    $('input[type="checkbox"]').removeAttr('checked');
    $('#AppliedCount').text(1);



    $.ajax({
        type: "POST",
        url: "/ClearAllCloseTrainings",
        dataType: "html",
        beforeSend: function(){
            // Show image container
            $("#AjaxLoader").show();
            $("#ManageJobResults").hide();
            // return false;
        },
        success: function (data) {
            if(data.error){
                danger_msg(data.message);
                return false;
            }else{
                $("#ManageJobResults").html("");
                $("#ManageJobResults").append(data);
                console.log(data);
                return false;
            }
        },
        complete:function(data){
        // Hide image container
            $("#AjaxLoader").hide();
            $("#ManageJobResults").show();
        }
    });
});



$('#EmployerProfileEdit').on('click', function(){
// function EmployerProfileEdit(id){
    // alert("test");
    var id = $("#EmpId").val();

    $.ajax({
        type: "POST",
        url: "/GetEmpProfileById/"+id,
        dataType: "JSON",
        success: function (data) {
            if(data.error){
                danger_msg(data.message);
                return false;
            }else{
                // $("#JobId").val(data.id);

                $("#company_name").val(data.company_name);

                $("#mobile").val(data.mobile);

                $("#email").val(data.email);

                $("#Emp_type option[value="+data.employer_type+"]").attr('selected', 'selected');

                $("#pan_no").val(data.pan_number);

                $("#gst_number").val(data.gst_number);

                $("#team_size").val(data.team_size);
                
                $("#Emp_address").val(data.address);

                $("#Emp_zipcode").val(data.zip_code);

                $("#Emp_country").val(data.country);

                $("#Emp_state").val(data.state);

                $("#Emp_city").val(data.city);

                $("#facebook").val(data.facebook);

                $("#twitter").val(data.twitter);
                $("#linkedin").val(data.linkedin);


                $('.employer-profile-popup-box').fadeIn('fast');
                $('html').addClass('no-scroll');
                return false;
            }
        }
    });


    
// }
});
$('.close-popup').on('click', function(){
    $('.employer-profile-popup-box').fadeOut('fast');
    $('html').removeClass('no-scroll');
});



function EditCAArticle(id){
    $.ajax({
        type: "POST",
        url: "/Jobs/GetJobsById/"+id,
        data: {id: id},
        dataType: "JSON",
        success: function (data) {
            if(data.error){
                danger_msg(data.message);
                return false;
            }else{
                $("#CAJobId").val(data.id);

                if(data.stipend_type == 2){
                    $("#Edit_SalaryShow").show();
                }else{
                    $("#Edit_SalaryShow").hide();
                }

                $("#Edit_stipend_type option[value="+data.stipend_type+"]").attr('selected', 'selected');

                $("#Edit_Ca_salary_min").val(data.salary_min);

                $("#Edit_Ca_salary_max").val(data.salary_max);

                $("#Edit_Ca_salary_type option[value="+data.salary_type+"]").attr('selected', 'selected');

                $("#Edit_Ca_address").val(data.location);
                $("#Edit_Ca_zipcode").val(data.zip_code);
                $("#Edit_Ca_country").val(data.country);
                $("#Edit_Ca_state").val(data.state);
                $("#Edit_Ca_city").val(data.city);

                $("#Edit_Ca_positions").val(data.total_positions);
                // $("#Edit_job_type option[value="+data.job_type+"]").attr('selected', 'selected');

                // $("#Edit_title option[value="+data.title+"]").attr('selected', 'selected');

                // $("#Edit_employee_type option[value="+data.employee_type+"]").attr('selected', 'selected');

                // // $("#Edit_title").val(data.title);
                // // CKEDITOR.instances['Edit_description'].setData(data.job_description);
                // $("#Edit_Ca_qualification option[value="+data.qualification+"]").attr('selected', 'selected');
                
                $("#Edit_Ca_qualification").val(data.qualification);
                // $("#Edit_zipcode").val(data.zip_code);
                // $("#Edit_country").val(data.country);
                // $("#Edit_state").val(data.state);
                // $("#Edit_city").val(data.city);

                // $("#Edit_salary_min").val(data.salary_min);
                // $("#Edit_salary_max").val(data.salary_max);

                // $("#Edit_salary_type option[value="+data.salary_type+"]").attr('selected', 'selected');

                // $("#Edit_experience").val(data.experience);
                // $("#Edit_positions").val(data.total_positions);

                $("#Edit_Ca_description").val(data.job_description);

                $('.edit-ca-jobs-popup-box').fadeIn('fast');
                $('html').addClass('no-scroll');
                return false;
            }
        }
    });
}

$('.close-popup').on('click', function(){
    $('.edit-ca-jobs-popup-box').fadeOut('fast');
    $('html').removeClass('no-scroll');
});



$(document).on("click", "#UpdateCAJobs", function(){
    var id = $("#CAJobId").val();
    var StipendType = $("#Edit_stipend_type").val();
    var zipcode = $("#Edit_Ca_zipcode").val();
    var country = $("#Edit_Ca_country").val();
    var state = $("#Edit_Ca_state").val();
    var city = $("#Edit_Ca_city").val();
    var address = $("#Edit_Ca_address").val();
    var salary_min = $("#Edit_Ca_salary_min").val();
    var salary_max = $("#Edit_Ca_salary_max").val();
    // var offered_salary = min_salary+ "-" + max_salary;
    var salary_type = $("#Edit_Ca_salary_type").val();
    var qualification = $("#Edit_Ca_qualification").val();
    var positions = $("#Edit_Ca_positions").val();
    // var description = CKEDITOR.instances['Edit_description'].getData();
    var description =  $("#Edit_Ca_description").val();


    if(description == ""){
        danger_toast_msg("Description should not empty");
        $("#Edit_Ca_description").focus();
        return false;
    }

    if(qualification == ""){
        danger_toast_msg("Qualification should not empty");
        $("#Edit_Ca_positions").focus();
        return false;
    }

    if(stipend_type == "2"){
        if(salary_min == ""){
            danger_toast_msg("Salary min should not empty");
            $("#Edit_Ca_salary_min").focus();
            return false;
        }
    
        if(salary_max == ""){
            danger_toast_msg("Salary max should not empty");
            $("#Edit_Ca_salary_max").focus();
            return false;
        }
    }

    if(address == ""){
        danger_toast_msg("Area should not empty");
        $("#Edit_Ca_address").focus();
        return false;
    }


    if(zipcode == ""){
        danger_toast_msg("Zipcode should not empty");
        $("#Edit_Ca_zipcode").focus();
        return false;
    }

    if(country == ""){
        danger_toast_msg("Country should not empty");
        $("#Edit_Ca_country").focus();
        return false;
    }

    if(state == ""){
        danger_toast_msg("State should not empty");
        $("#Edit_Ca_state").focus();
        return false;
    }

    if(city == ""){
        danger_toast_msg("City should not empty");
        $("#Edit_Ca_city").focus();
        return false;
    }


    if(positions == ""){
        danger_toast_msg("Positions should not empty");
        $("#Edit_Ca_positions").focus();
        return false;
    }

    var JobsData = {
        id: id,
        StipendType: StipendType,
        address: address,
        zipcode: zipcode,
        country: country,
        state: state,
        city: city,
        salary_min: salary_min,
        salary_max: salary_max,
        salary_type: salary_type,
        qualification: qualification,
        positions: positions,
        description: description
    }


    $.ajax({
        type: "POST",
        url: "/Employer/UpdateCAJobs",
        data: JobsData,
        dataType: "JSON",
        success: function (data) {
            if(data.error){
                danger_toast_msg(data.message);
                return false;
            }else{
                $('.post-ca-jobs-popup-box').fadeOut('fast');
                $('html').removeClass('no-scroll');
                success_toast_msg(data.message);
                location.reload();
                return false;
            }
        }
    });


});



$("#Ca_qualification").keypress(function(event){
    var inputValue = event.charCode;
    if(!(inputValue >= 65 && inputValue <= 120) && (inputValue != 32 && inputValue != 0)){
        event.preventDefault();
    }
});

$("#qualification").keypress(function(event){
    var inputValue = event.charCode;
    if(!(inputValue >= 65 && inputValue <= 120) && (inputValue != 32 && inputValue != 0)){
        event.preventDefault();
    }
});


$("#Edit_qualification").keypress(function(event){
    var inputValue = event.charCode;
    if(!(inputValue >= 65 && inputValue <= 120) && (inputValue != 32 && inputValue != 0)){
        event.preventDefault();
    }
});

$("#Edit_Ca_qualification").keypress(function(event){
    var inputValue = event.charCode;
    if(!(inputValue >= 65 && inputValue <= 120) && (inputValue != 32 && inputValue != 0)){
        event.preventDefault();
    }
});

$("#Ca_Edit_country").keypress(function(event){
    var inputValue = event.charCode;
    if(!(inputValue >= 65 && inputValue <= 120) && (inputValue != 32 && inputValue != 0)){
        event.preventDefault();
    }
});

$("#Ca_Edit_state").keypress(function(event){
    var inputValue = event.charCode;
    if(!(inputValue >= 65 && inputValue <= 120) && (inputValue != 32 && inputValue != 0)){
        event.preventDefault();
    }
});

$("#Ca_Edit_city").keypress(function(event){
    var inputValue = event.charCode;
    if(!(inputValue >= 65 && inputValue <= 120) && (inputValue != 32 && inputValue != 0)){
        event.preventDefault();
    }
});

$("#country").keypress(function(event){
    var inputValue = event.charCode;
    if(!(inputValue >= 65 && inputValue <= 120) && (inputValue != 32 && inputValue != 0)){
        event.preventDefault();
    }
});

$("#state").keypress(function(event){
    var inputValue = event.charCode;
    if(!(inputValue >= 65 && inputValue <= 120) && (inputValue != 32 && inputValue != 0)){
        event.preventDefault();
    }
});

$("#city").keypress(function(event){
    var inputValue = event.charCode;
    if(!(inputValue >= 65 && inputValue <= 120) && (inputValue != 32 && inputValue != 0)){
        event.preventDefault();
    }
});


$("#Emp_country").keypress(function(event){
    var inputValue = event.charCode;
    if(!(inputValue >= 65 && inputValue <= 120) && (inputValue != 32 && inputValue != 0)){
        event.preventDefault();
    }
});

$("#Emp_state").keypress(function(event){
    var inputValue = event.charCode;
    if(!(inputValue >= 65 && inputValue <= 120) && (inputValue != 32 && inputValue != 0)){
        event.preventDefault();
    }
});

$("#Emp_city").keypress(function(event){
    var inputValue = event.charCode;
    if(!(inputValue >= 65 && inputValue <= 120) && (inputValue != 32 && inputValue != 0)){
        event.preventDefault();
    }
});


// $('#pan_no').change(function (event) {     
//     var regExp = /[a-zA-z]{5}\d{4}[a-zA-Z]{1}/; 
//     var txtpan = $(this).val(); 
//     if (txtpan.length == 10 ) { 
//      if( txtpan.match(regExp) ){ 
//       alert('PAN match found');
//      }
//      else {
//       alert("Not a valid PAN number");
//       event.preventDefault(); 
//      } 
//     } 
//     else { 
//           alert('Please enter 10 digits for a valid PAN number');
//           event.preventDefault(); 
//     } 
   
//    });


function success_msg(data){
    $.notify({
        title: '<strong>'+ data +'</strong>',
        message: ''
    },{
        type: 'success'
    });
}

function danger_msg(data){
    $.notify({
        title: '<strong>'+ data +'</strong>',
        message: ''
    },{
        type: 'danger'
    });
}


