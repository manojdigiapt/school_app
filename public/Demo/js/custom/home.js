/* Signup Popup */
$('.signin-candidate').on('click', function(){
    $('.signin-candidate-popup-box').fadeIn('fast');
    $('html').addClass('no-scroll');
});
$('.close-popup').on('click', function(){
    $('.signin-candidate-popup-box').fadeOut('fast');
    $('html').removeClass('no-scroll');
});


/* Signin Popup */
$('.signin-employer').on('click', function(){
    $('.signup-employer-popup-box').fadeIn('fast');
    $('html').addClass('no-scroll');
});
$('#register_close').on('click', function(){
    $('.signup-employer-popup-box').fadeOut('fast');
    $('html').removeClass('no-scroll');
});


// Register Popup
$('#register_form').on('click', function(){
    $('#register_employer').fadeIn('fast');
    $("#login_employer").hide();
    $("#login_form").show();
    $("#register_form").hide();
    $(".register_form").addClass('register_btn_clr');
    $('.register_form').removeClass('register_form');

    $("#login_form").addClass('register_form');
    $('#login_form').removeClass('register_btn_clr');
    $('html').addClass('no-scroll');
    return false;
});

// Login Popup
$('#login_form').on('click', function(){
    $('#login_employer').fadeIn('fast');
    $("#register_employer").hide();
    $("#login_form").hide();
    $("#register_form").show();
    
    $(".register_form").addClass('register_btn_clr');
    $('.register_form').removeClass('register_form');

    $("#register_form").addClass('register_form');
    $('#register_form').removeClass('register_btn_clr');
    $('html').addClass('no-scroll');
    return false;
});





// Candidate Register Popup
$('#registerCandidate').on('click', function(){
    $('#register_candidate').fadeIn('fast');
    $("#login_candidate").hide();
    $("#loginCandidate").show();
    $("#registerCandidate").hide();
    $(".registerCandidate").addClass('register_btn_clr');
    $('.registerCandidate').removeClass('register_form');

    $(".register-candidate-form").addClass('Login-clr');
    $('.register-candidate-form').removeClass('register-clr');
    $('html').addClass('no-scroll');
    return false;
});

// Candidate Login Popup
$('#loginCandidate').on('click', function(){
    $('#login_candidate').fadeIn('fast');
    $("#register_candidate").hide();
    $("#loginCandidate").hide();
    $("#registerCandidate").show();
    $(".registerCandidate").addClass('register-btn');
    $('.registerCandidate').removeClass('register_btn_clr');

    // $("#register_form").addClass('register_btn_clr');
    // $('#register_form').removeClass('register_form');
    $('html').addClass('no-scroll');
    return false;
});