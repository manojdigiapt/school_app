$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});



$(document).on("click", "#AdminLogin", function(){
    var email = $("#email").val();
    var password = $("#password").val();

    var AuthendicationData = {
        email: email,
        password: password
    }

    $.ajax({
        type: "POST",
        url: "/AdminLogin",
        data: AuthendicationData,
        dataType: "JSON",
        success: function (data) {
            if(data.error){
                danger_msg(data.message);
                return false;
            }else{
                if(data.type == 1){
                    success_msg(data.message);
                    window.location.href = "/Admin/Dashboard";
                    return false;
                }else if(data.type == 2){
                    success_msg(data.message);
                    alert("sub admin");
                    return false;
                    // window.location.href = "/Admin/Dashboard";
                    // return false;
                }
            }
        }
    });
});



function danger_msg(data){
    $.notify(data, {type:"danger"});
}

function success_msg(data){
    $.notify(data, {type:"success"});
}