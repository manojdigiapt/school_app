$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});


$(document).on("click", "#AddAdminUsers", function(){
    var name = $("#name").val();
    var email = $("#email").val();
    var role = $("#role :selected").val();
    var password = $("#password").val();

    if(name == ""){
        danger_msg("Name should not be empty");
        $("#name").focus();
        return false;
    }

    if(email == ""){
        danger_msg("Email should not be empty");
        $("#email").focus();
        return false;
    }

    if(role == "Choose Admin Role"){
        danger_msg("Role should not be empty");
        $("#role").focus();
        return false;
    }

    if(password == ""){
        danger_msg("Password should not be empty");
        $("#password").focus();
        return false;
    }

    var AdminUsersData = {
        name: name,
        email: email,
        role: role,
        password: password
    }

    $.ajax({
        type: "POST",
        url: "/AddAdminUsers",
        data: AdminUsersData,
        dataType: "JSON",
        success: function (data) {
            if(data.error){
                danger_msg(data.message);
                return false;
            }else{
                success_msg(data.message);
                location.reload();
                return false;
            }
        }
    });
});



function EditAdminUsers(id){
    
    $.ajax({
        type: "POST",
        url: "/GetEditAdminUser/"+id,
        dataType: "JSON",
        success: function (data) {
            if(data.error){
                danger_msg(data.message);
                return false;
            }else{
                $("#EditId").val(data.id);
                $("#Editname").val(data.name);
                $("#Editemail").val(data.email);
                // $("#Editrole").val(data.role_id);
                $("#Editpassword").val(data.password);

                $("#EditUsers").modal("show");
            }
        }
    });
}


$(document).on("click", "#UpdateAdminUsers", function(){
    var id = $("#EditId").val();
    var name = $("#Editname").val();
    var email = $("#Editemail").val();
    var role = $("#Editrole :selected").val();
    var password = $("#Editpassword").val();

    if(name == ""){
        danger_msg("Name should not be empty");
        $("#name").focus();
        return false;
    }

    if(email == ""){
        danger_msg("Email should not be empty");
        $("#email").focus();
        return false;
    }

    if(role == "Choose Admin Role"){
        danger_msg("Role should not be empty");
        $("#role").focus();
        return false;
    }

    if(password == ""){
        danger_msg("Password should not be empty");
        $("#password").focus();
        return false;
    }

    var AdminUsersData = {
        id: id,
        name: name,
        email: email,
        role: role,
        password: password
    }

    $.ajax({
        type: "POST",
        url: "/UpdateAdmins",
        data: AdminUsersData,
        dataType: "JSON",
        success: function (data) {
            if(data.error){
                danger_msg(data.message);
                return false;
            }else{
                success_msg(data.message);
                location.reload();
                return false;
            }
        }
    });
});


function DeleteAdminUsers(id){
    swal({
        title: 'Are you sure?',
        text: 'You won\'t be able to revert this!',
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, delete it!'
    }).then(function (isConfirm) {
        if (isConfirm) {
            $.ajax({
                type: "GET",
                url: "/DeleteAdminUsers/"+id,
                dataType: "JSON",
                success: function (data) {
                    if(data.error){
                        danger_msg(data.message);
                        return false;
                    }else{
                        swal("Deleted!", "Data deleted successfully", "success");
                        location.reload();
                        return false;
                    }
                }
            });
        }
    }).catch(swal.noop)

}




function danger_msg(data){
    $.notify(data, {type:"danger"});
}

function success_msg(data){
    $.notify(data, {type:"success"});
}