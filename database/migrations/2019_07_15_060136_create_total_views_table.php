<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTotalViewsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('total_views', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('candidate_id')->unsigned();
            $table->bigInteger('jobs_id')->unsigned()->nullable();
            $table->bigInteger('training_id')->unsigned()->nullable();
            $table->boolean('total_views')->nullable();
            $table->string('views_type')->nullable();
            
            $table->foreign('training_id')->references('id')->on('training');
            $table->foreign('jobs_id')->references('id')->on('jobs');
            $table->foreign('candidate_id')->references('id')->on('candidate');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('total_views');
    }
}
