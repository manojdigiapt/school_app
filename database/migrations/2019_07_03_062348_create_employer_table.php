<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmployerTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('employer', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('profile_pic')->nullable();
            $table->string('user_name')->nullable();
            $table->string('company_name');
            $table->string('password');
            $table->string('slug')->nullable();
            // $table->boolean('allow_in_search')->nullable();
            $table->string('employer_type')->nullable();
            $table->string('pan_number')->nullable();
            $table->string('gst_number')->nullable();
            $table->bigInteger('since')->nullable();
            $table->string('team_size')->nullable();
            $table->longText('categories')->nullable();
            $table->longText('description')->nullable();
            $table->string('website')->nullable();
            $table->string('email');
            $table->string('mobile');
            $table->string('phone')->nullable();
            // $table->string('pan_no')->nullable();
            $table->string('address')->nullable();
            $table->bigInteger('zip_code')->nullable();
            $table->string('country')->nullable();
            $table->string('state')->nullable();
            $table->string('city')->nullable();
            $table->string('facebook')->nullable();
            $table->string('twitter')->nullable();
            $table->string('googleplus')->nullable();
            $table->string('linkedin')->nullable();
            // $table->longText('interest')->nullable();
            $table->boolean('otp_verify')->nullable();
            $table->boolean('email_verify')->nullable();
            $table->string('_token')->nullable();
            $table->boolean('status')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('employer');
    }
}
