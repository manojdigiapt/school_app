<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCandidateTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('candidate', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->string('password');
            $table->string('slug')->nullable();
            $table->string('email');
            $table->string('mobile');
            // $table->string('aadhar_no')->nullable();
            // $table->string('pan_no')->nullable();
            $table->boolean('mobile_otp_verify')->nullable();
            $table->boolean('email_verify')->nullable();
            $table->string('_token')->nullable();
            $table->boolean('type')->nullable();
            $table->boolean('status')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('candidate');
    }
}
