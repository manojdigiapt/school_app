<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReviewsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reviews', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('candidate_id')->unsigned()->nullable();
            $table->bigInteger('training_id')->unsigned()->nullable();
            // $table->string('title');
            $table->string('review_stars');
            $table->longText('feedback');
            $table->foreign('candidate_id')->references('id')->on('candidate');
            $table->foreign('training_id')->references('id')->on('training');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('reviews');
    }
}
