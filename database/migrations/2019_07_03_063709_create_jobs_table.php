<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateJobsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('jobs', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('employer_id')->unsigned();
            $table->string('jobs_id')->nullable();
            $table->string('title');
            $table->string('slug')->nullable();
            $table->longText('location')->nullable();
            $table->string('zip_code');
            $table->string('country');
            $table->string('state');
            $table->string('city');
            // $table->string('email');
            // $table->string('mobile');
            $table->longText('job_description');
            $table->boolean('job_type')->nullable();
            $table->string('industry')->nullable();
            $table->boolean('stipend_type')->nullable();
            $table->string('stipend')->nullable();
            // $table->longText('role')->nullable();
            $table->string('salary_min')->nullable();
            $table->string('salary_max')->nullable();
            $table->boolean('salary_type')->nullable();

            $table->boolean('employee_type')->nullable();
            // $table->boolean('career_level');
            $table->string('experience')->nullable();
            // $table->string('gender')->nullable();
            $table->longText('qualification');
            // $table->date('job_expires');
            $table->bigInteger('total_positions');
            $table->bigInteger('total_views')->nullable();
            $table->boolean('status')->nullable();
            $table->foreign('employer_id')->references('id')->on('employer');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('jobs');
    }
}
