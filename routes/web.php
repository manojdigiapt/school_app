<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// UI Routes
// Route::get('/', function () {
//     return view('welcome');
// });


// Demo

Route::get('/UI/Demo', "UI\HomeController@Demo");

Route::get('/Candidate/CandidateDashboard', "UI\HomeController@CandidateDashboard");


Route::get('/Candidate/CandidateFirstRegister', "UI\CandidateController@CandidateFirstRegister");


Route::get('/Trainee/TraineeFirstRegister', "UI\CandidateController@TraineeFirstRegister");

Route::get('/Articleship/ArticleshipFirstRegister', "UI\CandidateController@ArticleshipFirstRegister");


Route::get('/', "UI\HomeController@home");
Route::get('/AboutUs', "UI\HomeController@AboutUs");

Route::get('/PrivacyPolicy', "UI\HomeController@PrivacyPolicy");

Route::get('/TermsOfUse', "UI\HomeController@TermsUse");

Route::get('/Contactus', "UI\HomeController@Contactus");


Route::get('/Candidate/Profile', "UI\CandidateController@CandidateProfile");

Route::get('/Candidate/Dashboard', "UI\CandidateController@CandidateDashboard");

Route::get('/Candidate/Education', "UI\CandidateProfileController@CandidateEducation");

Route::get('/CandidateChangePassword', "UI\CandidateController@CandidateChangePassword");

Route::post('/UpdatePassword', "UI\CandidateProfileController@UpdatePassword");

Route::post('/Candidate/InsertCandidate', "UI\CandidateController@AddCandidate");

Route::post('/Candidate/InsertCandidateProfile', "UI\CandidateProfileController@AddCandidateProfile");

Route::post('/Candidate/UpdateProfilePic', 'UI\CandidateProfileController@UpdateProfilePic');


Route::get('/Candidate/CandidateFavouriteJobs', "UI\CandidateController@CandidateFavouriteJobs");


Route::get('Candidate/logout', 'UI\CandidateController@logout');

// Route::prefix('candidate')->group(function () {
    
// });
Route::post('/ResendCandidateLink', "UI\CandidateController@ResendLink");

Route::post('/ResendCandidateEmailLink', "UI\CandidateController@ResendEmailOTP");



Route::post('/Candidate/CandidateLogin', "UI\CandidateController@CandidateLogin");

Route::post('/Candidate/CheckForgotEmail', "UI\CandidateController@CheckForgotEmail");

Route::get('/Candidate/Reset_password/{id}/{token}', "UI\CandidateController@CheckResetPassword");

Route::post('/Candidate/ResetPassword', "UI\CandidateController@ResetPassword");
// Candidate Resume
Route::get('/Candidate/Resume', "UI\CandidateProfileController@CandidateResume");

Route::post('/Candidate/InsertEducation', "UI\CandidateProfileController@InsertEducation");
Route::post('/Candidate/GetEducationById/{id}', "UI\CandidateProfileController@GetEducationById");

Route::post('/Candidate/UpdateEducation', "UI\CandidateProfileController@UpdateEducation");

Route::get('/Candidate/DeleteEducation/{id}', "UI\CandidateProfileController@DeleteEducation");




Route::post('/Candidate/InsertExperience', "UI\CandidateProfileController@InsertExperience");
Route::post('/Candidate/GetExperienceById/{id}', "UI\CandidateProfileController@GetExperienceById");

Route::post('/Candidate/UpdateExperience', "UI\CandidateProfileController@UpdateExperience");

Route::get('/Candidate/DeleteExperience/{id}', "UI\CandidateProfileController@DeleteExperience");



Route::post('/Candidate/InsertSkills', "UI\CandidateProfileController@InsertSkills");
Route::post('/Candidate/CheckSkills', "UI\CandidateProfileController@CheckSkills");


Route::post("/SendOtp", "UI\CandidateController@SendOtp");


// Upload CV
Route::get('/Candidate/UploadCv', "UI\CandidateProfileController@UploadCv");
Route::post('/Candidate/InsertCV', "UI\CandidateProfileController@InsertOrUpdateCV");
Route::post('/Candidate/InsertOrUpdateCVTitle', "UI\CandidateProfileController@InsertOrUpdateCVTitle");

Route::post('/Candidate/ResumeHeadline/{id}', "UI\CandidateProfileController@ResumeHeadline");

Route::post('/Candidate/UpdateCareerProfile', "UI\CandidateProfileController@UpdateCareerProfile");


Route::post('/Candidate/GetPersonalDetailsById/{id}', "UI\CandidateProfileController@GetPersonalDetailsById");

Route::post('/Candidate/UpdatePersonalDetails', "UI\CandidateProfileController@UpdatePersonalDetails");

// Candidate list & Search
Route::get('/Candidates', "UI\EmployerProfileController@GetAllCandidates");

Route::get('/GetAllShortlistedCandidates', "UI\EmployerProfileController@GetAllShortlistedCandidates");


// Route::get('/Candidates', "UI\CandidateController@GetAllCandidates");

Route::post('/SearchCandidatesPositions', "UI\EmployerProfileController@SearchCandidatesPositions");

Route::post('/SearchCandidates', "UI\EmployerProfileController@SearchCandidates");


// Candidate Details
Route::get('/CandidateDetails/{id}/{slug}', "UI\EmployerProfileController@CandidateDetails");


Route::get('/AppliedCandidateDetails/{id}/{CandidateId}/{slug}', "UI\EmployerProfileController@AppliedCandidateDetails");


Route::post('/DownloadCV/{id}/{JobsId}', "UI\EmployerProfileController@DownloadCV");

Route::post('/ChangeJobChange', "UI\CandidateProfileController@ChangeJobChange");

Route::get('/SearchJobResults', "UI\HomeController@SearchJobs");




// Employer Routes
Route::get('Employer/logout', 'UI\EmployerController@logout');

Route::post('/GetEmpProfileById/{id}', "UI\EmployerProfileController@GetEmpProfileById");

Route::post('/ResendLink', "UI\EmployerController@ResendLink");

Route::post('/ResendEmailLink', "UI\EmployerController@ResendEmailOTP");

Route::post('/ContactUsMail', "UI\EmployerController@ContactUsMail");

Route::post('/Employer/CheckForgotEmail', "UI\EmployerController@CheckForgotEmail");

Route::get('/Employer/Reset_password/{id}/{token}', "UI\EmployerController@CheckResetPassword");

Route::post('/Employer/ResetPassword', "UI\EmployerController@ResetPassword");

Route::get("/Employer/Profile", "UI\EmployerController@EmployerProfile");

Route::get('/Employer/Dashboard', "UI\EmployerController@Dashboard");

Route::get('/EmployerChangePassword', "UI\EmployerController@EmployerChangePassword");

Route::post('/UpdateEmployerPassword', "UI\EmployerProfileController@UpdatePassword");

Route::post('/Employer/CheckEmployerForgotEmail', "UI\EmployerController@CheckEmployerForgotEmail");

Route::post('/Employer/EmployerLogin', "UI\EmployerController@EmployerLogin");

Route::post('/Employer/InsertEmployer', "UI\EmployerController@AddEmployer");

Route::post('/Employer/InsertEmployerProfile', "UI\EmployerProfileController@InsertEmployerProfile");



Route::get('/Employer/Post_Jobs', "UI\EmployerProfileController@post_jobs");

Route::get('/Employer/Post_Training', "UI\EmployerProfileController@post_training");


Route::post('/ShortlistCandidatesByJobList', "UI\EmployerProfileController@ShortlistCandidatesByJobList");


Route::post('/ShortlistCandidatesByDirect', "UI\EmployerProfileController@ShortlistCandidatesByDirect");

Route::post('/FilterShortlistedCandidates', "UI\FilterController@FilterShortlistedCandidates");


Route::post('/FilterAppliedCandidates', "UI\FilterController@FilterAppliedCandidates");


Route::post('/FilterApprovedTrainees', "UI\FilterController@FilterApprovedTrainees");


Route::get('/ViewAllAppliedCandidates/{id}/{slug}', "UI\EmployerProfileController@ViewAllAppliedCandidates");


Route::post('/ChangeJobStatus', "UI\FilterController@ChangeJobStatus");

Route::post('/ChangeCAJobStatus', "UI\FilterController@ChangeCAJobStatus");


Route::post('/ChangeTrainingStatus', "UI\TrainingController@ChangeTrainingStatus");


Route::post('/ChangeTrainingCompletionStatus', "UI\TrainingController@ChangeTrainingCompletionStatus");


Route::post('/ChangeAppliedTraineeTrainingStatus', "UI\TrainingController@ChangeAppliedTraineeTrainingStatus");

Route::post('/ChangeTrainingEndDate', "UI\TrainingController@ChangeTrainingEndDate");


Route::get('/GetShortlistedJobs', "UI\CandidateProfileController@GetShortlistedJobs");

Route::get('/EmployerDetails/{slug}', "UI\CandidateProfileController@EmployerDetails");


Route::post('/Employer/UpdateProfiePic', "UI\EmployerProfileController@UpdateProfilePic");









Route::post('/Employer/AddJobs', "UI\JobsController@PostJobs");
Route::get('/Employer/ManageJobs', "UI\EmployerProfileController@ManageJobs");

Route::get('/Employer/ManageCAArticleshipJobs', "UI\EmployerProfileController@ManageCAArticleshipJobs");

Route::get('/Employer/CloseCAArticleshipJobs', "UI\EmployerProfileController@CloseCAArticleshipJobs");

Route::post('/SearchLocationByKeypress', "UI\FilterController@SearchLocationByKeypress");

Route::post('/SearchClosJobsLocationByKeypress', "UI\FilterController@SearchClosJobsLocationByKeypress");


Route::post('/SearchTrainingLocationByKeypress', "UI\FilterController@SearchTrainingLocationByKeypress");


Route::post('/SearchCloseTrainingLocationByKeypress', "UI\FilterController@SearchCloseTrainingLocationByKeypress");


Route::post('/SearchDurationByManageTraining', "UI\FilterController@SearchDurationByManageTraining");


Route::post('/SearchDurationByCloseTraining', "UI\FilterController@SearchDurationByCloseTraining");


Route::post('/SearchFresherDurationByManageTraining', "UI\FilterController@SearchFresherDurationByManageTraining");

Route::post('/SearchCandidateLocationByKeypress', "UI\FilterController@SearchCandidateLocationByKeypress");


Route::post('/SearchJobsLocationByKeypress', "UI\FilterController@SearchJobsLocationByKeypress");

Route::get('/Employer/CloseJobs', "UI\EmployerProfileController@CloseJobs");

Route::get('JobDetails/{company_name}/{JobId}/{slug}', "UI\JobsController@JobDetails");
Route::get('/Employer/EditJobs/{id}', "UI\EmployerProfileController@EditJobs");

Route::post('/Employer/UpdateJobs', "UI\JobsController@UpdateJobs");

Route::post('/Employer/UpdateCAJobs', "UI\JobsController@UpdateCAJobs");

Route::get('/Jobs', "UI\JobsController@GetAllJobs");

Route::post('/Jobs/GetJobsById/{id}', "UI\JobsController@GetJobsById");


Route::post('/SearchJobsTitle', "UI\JobsController@SearchJobsTitle");

Route::post('/SearchJobs', "UI\JobsController@SearchJobs");

Route::post('/ApplyJobs', "UI\JobsController@ApplyJobs");

Route::get('/GetAppliedJobs', "UI\CandidateProfileController@GetAppliedJobs");

Route::post('/AddToFavourite', "UI\CandidateProfileController@AddToFavourite");

Route::post('/AddToTrainingFavourite', "UI\CandidateProfileController@AddToTrainingFavourite");











Route::post('/Employer/PostTrainings', "UI\EmployerProfileController@post_trainings");
Route::get('/Employer/ManageTrainings', "UI\EmployerProfileController@ManageTrainings");

Route::get('/Employer/CloseTrainings', "UI\EmployerProfileController@CloseTrainings");

Route::post('/Employer/GetTrainingsById/{id}', "UI\EmployerProfileController@GetTrainingsById");
Route::post('/Employer/UpdateTrainings', "UI\EmployerProfileController@UpdateTrainings");

Route::get('/Trainings', "UI\TrainingController@GetAllTrainings");


Route::post('/ApplyTrainings', "UI\TrainingController@ApplyTrainings");







// Trainee Dashboard
Route::get('/Trainee/Profile', "UI\TrainingController@TraineeProfile");

Route::get('/Trainee/Dashboard', "UI\TrainingController@TraineeDashboard");

Route::get('/FresherJobs', "UI\TrainingController@FresherJobs");

Route::get('/Trainee/ChangePassword', "UI\TrainingController@TraineeChangePassword");

Route::post('/UpdateTraineePassword', "UI\TrainingController@UpdatePassword");

Route::post('/Trainee/AddOrUpdateTrainee', "UI\TrainingController@AddTraineeProfile");

Route::get('Trainee/logout', 'UI\TrainingController@logout');

Route::get('/GetAppliedTrainings', "UI\TrainingController@GetAppliedTrainings");

Route::get('/ViewAllAppliedTrainees/{id}/{slug}', "UI\EmployerProfileController@ViewAllAppliedTrainees");


Route::get('/ViewAllAppliedCACandidates/{id}/{slug}', "UI\EmployerProfileController@ViewAllAppliedCACandidates");

// Route::get('/GenerateCertificate', "UI\EmployerProfileController@GenerateCertificate");

Route::get('/GetStudentDataByCertificates', "UI\TrainingController@GetStudentDataByCertificates");


Route::post('/Trainee/UpdateProfilePic', "UI\TrainingController@UpdateProfilePic");


Route::post('/Trainee/AddTraineeProfile', "UI\TrainingController@AddTraineeProfile");


Route::post('/Trainee/InsertCV', "UI\TrainingController@InsertOrUpdateCV");

Route::get('/Trainee/DeleteEducation/{id}', "UI\TrainingController@DeleteEducation");



Route::post('/Trainee/InsertOrUpdateCVTitle', "UI\TrainingController@InsertOrUpdateCVTitle");

Route::post('/Trainee/InsertEducation', "UI\TrainingController@InsertEducation");


Route::post('/Trainee/UpdatePersonalDetails', "UI\TrainingController@UpdatePersonalDetails");


Route::get('/AppliedTraineeDetails/{id}/{TraineeId}/{slug}', "UI\TrainingController@AppliedTraineeDetails");

Route::post('/DownloadTraineeCV/{id}/{TraineeId}', "UI\TrainingController@DownloadCV");









// Trainee List & Search
Route::get('/Trainees', "UI\TrainingController@GetAllTrainee");

Route::post('/SearchTrainingTitle', "UI\TrainingController@SearchTrainingTitle");

Route::post('/SearchTrainings', "UI\TrainingController@SearchTrainings");

Route::get('TrainingDetails/{company_name}/{slug}', "UI\TrainingController@TrainingDetails");

Route::get('/TraineeDetails/{slug}/{TrainingSlug}', "UI\EmployerProfileController@TraineeDetails");

Route::post("/SelectTraineeForTraining", "UI\EmployerProfileController@SelectTraineeForTraining");

Route::get('/ViewAllSelectedTrainees', "UI\EmployerProfileController@ViewAllSelectedTrainees");

Route::get('/ViewAllSelectedTrainingsByTrainees', "UI\TrainingController@ViewAllSelectedTrainingsByTrainees");

Route::get('/ViewAllCompletedTrainingsByTrainees', "UI\TrainingController@ViewAllCompletedTrainingsByTrainees");


Route::post('/AddStars',"UI\EmployerProfileController@AddStars");


Route::post('/FilterDatePostedTraining', "UI\TrainingController@FilterDatePostedTraining");

Route::post('/ClearAllTrainings', "UI\TrainingController@ClearAllTrainings");

Route::post('/SortByTrainings', "UI\TrainingController@SortByTrainings");








Route::get('/ViewAllTrainings', "UI\TrainingController@ViewAllTrainings");

Route::get('/ViewAllCandidates', "UI\CandidateController@ViewAllCandidates");






// Filter Jobs
Route::post('/FilterJobs', "UI\JobsController@FilterJobs");

Route::post('/FilterDatePostedJobs', "UI\JobsController@FilterDatePostedJobs");

Route::post('/SortByJobs', "UI\JobsController@SortByJobs");

Route::post('/ClearAllFilter', "UI\JobsController@ClearAllFilter");



Route::post('/FilterJobType', "UI\JobsController@FilterJobType");

Route::post('/FilterDesiredLocation', "UI\FilterController@FilterDesiredLocation");

Route::post('/FilterIndustry', "UI\FilterController@FilterIndustry");



// Filter Candidates
Route::post('/FilterCandidates', "UI\FilterController@FilterCandidates");

Route::post('/FilterCandidateDesiredLocation', "UI\FilterController@FilterCandidateDesiredLocation");

Route::post('/FilterCandidateIndustry', "UI\FilterController@FilterCandidateIndustry");

Route::post('/FilterEmployeeType', "UI\FilterController@FilterEmployeeType");

Route::post('/FilterSalaryType', "UI\FilterController@FilterSalaryType");


// Filter Trainings
Route::post('/FilterTrainings', "UI\FilterController@FilterTrainings");

Route::post('/FilterDuration', "UI\FilterController@FilterDuration");






// Filter ManageJobs
Route::post('/FilterDatePostedManageJobs', "UI\FilterController@FilterDatePostedManageJobs");

Route::post('/FilterManageJobs', "UI\FilterController@FilterManageJobs");

Route::post('/ClearAllManageJobs', "UI\FilterController@ClearAllManageJobs");

Route::post('/SearchManageJobs', "UI\FilterController@SearchManageJobs");


// Filter CloseJobs
Route::post('/FilterDatePostedCloseJobs', "UI\FilterController@FilterDatePostedClosedJobs");

Route::post('/FilterCloseJobs', "UI\FilterController@FilterCloseJobs");

Route::post('/ClearAllCloseJobs', "UI\FilterController@ClearAllClosedJobs");

Route::post('/SearchCloseJobs', "UI\FilterController@SearchCloseJobs");


// Filter Manage Trainings

Route::post('/FilterDatePostedManageTrainings', "UI\FilterController@FilterDatePostedManageTrainings");

Route::post('/FilterManageTrainings', "UI\FilterController@FilterManageTrainings");

Route::post('/ClearAllManageTrainings', "UI\FilterController@ClearAllManageTrainings");

Route::post('/SearchManageTrainings', "UI\FilterController@SearchManageTrainings");


// Filter Close Trainings

Route::post('/FilterDatePostedCloseTrainings', "UI\FilterController@FilterDatePostedCloseTrainings");

Route::post('/FilterCloseTrainings', "UI\FilterController@FilterCloseTrainings");

Route::post('/ClearAllCloseTrainings', "UI\FilterController@ClearAllCloseTrainings");

Route::post('/SearchCloseTrainings', "UI\FilterController@SearchCloseTrainings");


// Filter Manage Candidates

Route::post('/FilterDatePostedCandidates', "UI\FilterController@FilterDatePostedCandidates");

Route::post('/FilterManageCandidates', "UI\FilterController@FilterManageCandidates");

Route::post('/ClearAllManageCandidates', "UI\FilterController@ClearAllManageCandidates");

Route::post('/SearchManageCandidates', "UI\FilterController@SearchManageCandidates");


Route::post('/SearchAppliedTrainees', "UI\FilterController@SearchAppliedTrainees");

Route::post('/SearchAppliedCandidates', "UI\FilterController@SearchAppliedCandidates");






// Load more 
Route::post("/LoadMoreJobs", "UI\CandidateProfileController@LoadMoreJobs");

Route::post("/LoadmoreCandidates", "UI\EmployerProfileController@LoadmoreCandidates");

Route::post("/LoadmoreTrainings", "UI\TrainingController@LoadmoreTrainings");



// Sort BY
Route::post("/SortByJobs", "UI\JobsController@SortByJobs");

Route::post("/SortByCandidates", "UI\CandidateController@SortByCandidates");






// OTP
Route::post("/SendEmployerOtp", "UI\EmployerController@SendEmployerOtp");




// Autocomplete

// Route::post('/SearchQualification', "UI\JobsController@SearchQualification");

Route::get('searchajax',array('as'=>'searchajax','uses'=>'UI\JobsController@SearchQualification'));
// End














// CA Article Routes
Route::get('/ArticleProfile', "UI\CaArticleController@ArticleProfile");

Route::get('/Article/Dashboard', "UI\CaArticleController@Dashboard");

Route::get('Article/logout', 'UI\CaArticleController@logout');

Route::post('/Article/AddCAJobs', "UI\CaArticleController@PostJobs");

Route::post('/FilterDatePostedCAJobs', "UI\CaArticleController@FilterDatePostedCAJobs");

Route::post('/FilterDatePostedClosedCAJobs', "UI\CaArticleController@FilterDatePostedClosedCAJobs");

Route::post('/SearchCAJobsLocationByKeypress', "UI\CaArticleController@SearchCAJobsLocationByKeypress");

Route::post('/SearchClosedCAJobsLocationByKeypress', "UI\CaArticleController@SearchClosedCAJobsLocationByKeypress");

Route::post('/FilterManageCAJobs', "UI\CaArticleController@FilterManageCAJobs");

Route::post('/FilterCloseCAJobs', "UI\CaArticleController@FilterCloseCAJobs");

Route::post('/ClearAllCAJobs', "UI\CaArticleController@ClearAllCAJobs");

Route::post('/ClearAllClosedCAJobs', "UI\CaArticleController@ClearAllClosedCAJobs");

Route::get('/Article/FresherJobs', "UI\CaArticleController@FresherCAJobs");

Route::get('/GetAppliedCAJobs', "UI\CaArticleController@GetAppliedCAJobs");

Route::get('CAJobDetails/{company_name}/{JobId}/{slug}', "UI\CaArticleController@CAJobDetails");


Route::get('FresherJobDetails/{company_name}/{JobId}/{slug}', "UI\CaArticleController@FresherJobDetails");


Route::post('/FilterDatePostedFreshJobs', "UI\CaArticleController@FilterDatePostedFreshJobs");


Route::post('/SearchFresherJobsLocationByKeypress', "UI\CaArticleController@SearchFresherJobsLocationByKeypress");


Route::post('/FilterFresherJobs', "UI\CaArticleController@FilterFresherJobs");

// End

































































// Admin


Route::get('/Admin/Login', "Admin\AdminController@login");

Route::get('/Admin/Dashboard', "Admin\AdminController@Dashboard");

Route::post('/AdminLogin', "Admin\AdminController@AdminLogin");

Route::post('/AddAdminUsers', "Admin\AdminController@AddAdmins");

Route::post('/GetEditAdminUser/{id}', "Admin\AdminController@GetEditAdminUser");

Route::post('/UpdateAdmins', "Admin\AdminController@UpdateAdmins");

Route::get('/DeleteAdminUsers/{id}', "Admin\AdminController@DeleteAdminUsers");

Route::get('/Admin/Admin_lists', "Admin\AdminController@Adminlists");

// Admin Jobs

Route::get('/JobsList', 'Admin\JobsController@JobsList');

Route::get('/Admin/add_jobs', 'Admin\JobsController@add_jobs');


// Job Attributes
// Gender
Route::get('/Admin/GetJobTitle', 'Admin\JobsAttributesController@GetJobTitle');

Route::post('/Admin/AddJobTitle', 'Admin\JobsAttributesController@AddJobTitle');

Route::post('/Admin/GetJobTitlebyId/{id}', 'Admin\JobsAttributesController@GetJobTitlebyId');

Route::post('/Admin/UpdateJobTitle', 'Admin\JobsAttributesController@UpdateJobTitle');

Route::get('/Admin/DeleteJobTitle/{id}', 'Admin\JobsAttributesController@DeleteJobTitle');

// Job Types
Route::get('/Admin/GetJobType', 'Admin\JobsAttributesController@GetJobType');

Route::post('/Admin/AddJobType', 'Admin\JobsAttributesController@AddJobType');

Route::post('/Admin/GetJobTypebyId/{id}', 'Admin\JobsAttributesController@GetJobTypebyId');

Route::post('/Admin/UpdateJobType', 'Admin\JobsAttributesController@UpdateJobType');

Route::get('/Admin/DeleteJobType/{id}', 'Admin\JobsAttributesController@DeleteJobType');


// Industries
Route::get('/Admin/GetIndustries', 'Admin\JobsAttributesController@GetIndustries');

Route::post('/Admin/AddIndustries', 'Admin\JobsAttributesController@AddIndustries');

Route::post('/Admin/GetIndustrybyId/{id}', 'Admin\JobsAttributesController@GetIndustrybyId');

Route::post('/Admin/UpdateIndustries', 'Admin\JobsAttributesController@UpdateIndustries');

Route::get('/Admin/DeleteIndustries/{id}', 'Admin\JobsAttributesController@DeleteIndustries');


// Qualification
Route::get('/Admin/GetQualification', 'Admin\JobsAttributesController@GetQualification');

Route::post('/Admin/AddQualification', 'Admin\JobsAttributesController@AddQualification');

Route::post('/Admin/GetQualificationbyId/{id}', 'Admin\JobsAttributesController@GetQualificationbyId');

Route::post('/Admin/UpdateQualification', 'Admin\JobsAttributesController@UpdateQualification');

Route::get('/Admin/DeleteQualification/{id}', 'Admin\JobsAttributesController@DeleteQualification');


// Functional Areas
Route::get('/Admin/GetFunctionalAreas', 'Admin\JobsAttributesController@GetFunctionalAreas');

Route::post('/Admin/AddFunctionalAreas', 'Admin\JobsAttributesController@AddFunctionalAreas');

Route::post('/Admin/GetFunctionalAreasbyId/{id}', 'Admin\JobsAttributesController@GetFunctionalAreasbyId');

Route::post('/Admin/UpdateFunctionalAreas', 'Admin\JobsAttributesController@UpdateFunctionalAreas');

Route::get('/Admin/DeleteFunctionalAreas/{id}', 'Admin\JobsAttributesController@DeleteFunctionalAreas');


// Training Completion
Route::get('/Admin/GetTrainingCompletion', 'Admin\JobsAttributesController@GetTrainingCompletion');

Route::post('/Admin/AddTrainingCompletion', 'Admin\JobsAttributesController@AddTrainingCompletion');

Route::post('/Admin/GetTrainingCompletionbyId/{id}', 'Admin\JobsAttributesController@GetTrainingCompletionbyId');

Route::post('/Admin/UpdateTrainingCompletion', 'Admin\JobsAttributesController@UpdateTrainingCompletion');

Route::get('/Admin/DeleteTrainingCompletion/{id}', 'Admin\JobsAttributesController@DeleteTrainingCompletion');


// Career Level
Route::get('/Admin/GetCareerLevel', 'Admin\JobsAttributesController@GetCareerLevel');

Route::post('/Admin/AddCareerLevel', 'Admin\JobsAttributesController@AddCareerLevel');

Route::post('/Admin/GetCareerLevelbyId/{id}', 'Admin\JobsAttributesController@GetCareerLevelbyId');

Route::post('/Admin/UpdateCareerLevel', 'Admin\JobsAttributesController@UpdateCareerLevel');

Route::get('/Admin/DeleteCareerLevel/{id}', 'Admin\JobsAttributesController@DeleteCareerLevel');



// Training Duration
Route::get('/Admin/GetTrainingDuration', 'Admin\JobsAttributesController@GetTrainingDuration');

Route::post('/Admin/AddTrainingDuration', 'Admin\JobsAttributesController@AddTrainingDuration');

Route::post('/Admin/GetTrainingDurationbyId/{id}', 'Admin\JobsAttributesController@GetTrainingDurationbyId');

Route::post('/Admin/UpdateTrainingDuration', 'Admin\JobsAttributesController@UpdateTrainingDuration');

Route::get('/Admin/DeleteTrainingDuration/{id}', 'Admin\JobsAttributesController@DeleteTrainingDuration');


// Team size
Route::get('/Admin/GetTeamSize', 'Admin\JobsAttributesController@GetTeamSize');

Route::post('/Admin/AddTeamSize', 'Admin\JobsAttributesController@AddTeamSize');

Route::post('/Admin/GetTeamSizebyId/{id}', 'Admin\JobsAttributesController@GetTeamSizebyId');

Route::post('/Admin/UpdateTeamSize', 'Admin\JobsAttributesController@UpdateTeamSize');

Route::get('/Admin/DeleteTeamSize/{id}', 'Admin\JobsAttributesController@DeleteTeamSize');


// Marital Status
Route::get('/Admin/GetMarital', 'Admin\JobsAttributesController@GetMarital');

Route::post('/Admin/AddMarital', 'Admin\JobsAttributesController@AddMarital');

Route::post('/Admin/GetMaritalbyId/{id}', 'Admin\JobsAttributesController@GetMaritalbyId');

Route::post('/Admin/UpdateMarital', 'Admin\JobsAttributesController@UpdateMarital');

Route::get('/Admin/DeleteMarital/{id}', 'Admin\JobsAttributesController@DeleteMarital');

// Training Title
Route::get('/Admin/GetTrainingTitle', 'Admin\JobsAttributesController@GetTrainingTitle');

Route::post('/Admin/AddTrainingTitle', 'Admin\JobsAttributesController@AddTrainingTitle');

Route::post('/Admin/GetTrainingTitlebyId/{id}', 'Admin\JobsAttributesController@GetTrainingTitlebyId');

Route::post('/Admin/UpdateTrainingTitle', 'Admin\JobsAttributesController@UpdateTrainingTitle');

Route::get('/Admin/DeleteTrainingTitle/{id}', 'Admin\JobsAttributesController@DeleteTrainingTitle');


// Languages
Route::get('/Admin/GetLanguages', 'Admin\JobsAttributesController@GetLanguages');

Route::post('/Admin/AddLanguages', 'Admin\JobsAttributesController@AddLanguages');

Route::post('/Admin/GetLanguagesbyId/{id}', 'Admin\JobsAttributesController@GetLanguagesbyId');

Route::post('/Admin/UpdateLanguages', 'Admin\JobsAttributesController@UpdateLanguages');

Route::get('/Admin/DeleteLanguages/{id}', 'Admin\JobsAttributesController@DeleteLanguages');






Route::get('/clear_cache', function() {
    Artisan::call('cache:clear');
    Artisan::call('view:clear');
    return "Cache is cleared";
});