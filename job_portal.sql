-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Sep 13, 2019 at 04:14 PM
-- Server version: 10.3.16-MariaDB
-- PHP Version: 7.3.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `job_portal`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `role_id` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`id`, `name`, `email`, `password`, `role_id`, `created_at`, `updated_at`) VALUES
(1, 'Manoj', 'manosiva5488@gmail.com', '$2y$10$IliB5YQNwOebH6ycb7SBnO9Fy0CUnPV2GIehIpftKtq37126pEoMS', 1, '2019-09-03 08:01:37', '2019-09-03 08:01:37');

-- --------------------------------------------------------

--
-- Table structure for table `candidate`
--

CREATE TABLE `candidate` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mobile` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mobile_otp_verify` tinyint(1) DEFAULT NULL,
  `email_verify` tinyint(1) DEFAULT NULL,
  `_token` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `type` tinyint(1) DEFAULT NULL,
  `status` tinyint(1) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `candidate`
--

INSERT INTO `candidate` (`id`, `name`, `password`, `slug`, `email`, `mobile`, `mobile_otp_verify`, `email_verify`, `_token`, `type`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Manoj', '$2y$10$BTNfRyVU7UQKP9.Yo9o..utg069cMlmIpZ3fihZfOYYkPayKrde2O', 'manoj', 'manojkumars.p@digiapt.com', '9698631624', 1, 1, NULL, 1, NULL, '2019-09-03 05:24:22', '2019-09-03 05:24:22'),
(2, 'Mano', '$2y$10$tMw49ZnkOaS5V3pRuuVKdOSDMyPLHQTvU5BPCKKpT7AFCTk.Zgk.e', 'mano', 'manosiva54889@gmail.com', '9698631623', 1, 1, NULL, 2, NULL, '2019-09-03 05:37:58', '2019-09-03 05:37:58'),
(3, 'Suresh', '$2y$10$Eh1NV.oV9SBHqHykmPzDweVQgTtrBa0tQHFuAOw2jUMEDzu2enMQi', 'suresh', 'manojkumars.p@digiapt.com', '9698631622', 1, 1, NULL, 1, NULL, '2019-09-09 04:34:09', '2019-09-09 04:34:09'),
(4, 'Siva', '$2y$10$yfHqxF7yUrjOjVAwhJVIQOTOYWdg3ue3uAiule1aGfufPKvSHs00K', 'siva', 'manojkumar.p@digiapt.com', '9698631626', 1, 1, NULL, 2, NULL, '2019-09-09 06:08:26', '2019-09-09 06:08:26'),
(7, 'CaMano', '$2y$10$3WFbaNrFx3KfQhcekroXuu0f14U6HYQmAC57QDNyx4c6HMmdKMLmm', 'camano', 'manojkumar.p@digiapt.com', '9698631625', 1, 1, NULL, 3, NULL, '2019-09-12 02:22:08', '2019-09-12 02:22:08'),
(11, 'CanSiva', '$2y$10$s.zJU63uwJf14pikffZDM.1kxvjrtN2fsEwPlibGlaLVCyUZCq0mO', 'cansiva', 'manojkumar.p@digiapt.com', '9698631620', 1, 1, NULL, 1, NULL, '2019-09-13 04:15:24', '2019-09-13 04:15:24'),
(12, 'TraSiva', '$2y$10$eh8sD0E4FzRFVlgBu2j1Ve8AEHGFncU.PlfN.BDY1lxGEsuIIZMQ6', 'trasiva', 'manojkumar.p@digiapt.com', '9698631654', 1, 1, NULL, 2, NULL, '2019-09-13 05:05:19', '2019-09-13 05:05:19'),
(13, 'CASiva', '$2y$10$2ZJ0gWPrMtRswwoCOxXnXu5yo4h9NJQ//4WtMvBW1gK5gjFsp4deq', 'casiva', 'manojkumar.p@digiapt.com', '9698631687', 1, 1, NULL, 3, NULL, '2019-09-13 06:09:34', '2019-09-13 06:09:34');

-- --------------------------------------------------------

--
-- Table structure for table `candidate_profile`
--

CREATE TABLE `candidate_profile` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `candidate_id` bigint(20) UNSIGNED NOT NULL,
  `father_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `profile_pic` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `industry` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `functional_area` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `role` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `skills` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `allow_search` tinyint(1) DEFAULT NULL,
  `experience` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `minimum_salary` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `gender` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `date_of_birth` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `age` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `aadhar` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pan` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `zip_code` bigint(20) DEFAULT NULL,
  `country` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `state` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `city` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `gross_salary` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `expected_salary` bigint(20) DEFAULT NULL,
  `education_level` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `languages` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `specialisms` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `website` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `facebook` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `twitter` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `linkedin` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `googleplus` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `job_type` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `employee_type` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `desired_location` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `preferred_shift` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `marital_status` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `profile_status` tinyint(1) DEFAULT NULL,
  `profile_completion` tinyint(1) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `candidate_profile`
--

INSERT INTO `candidate_profile` (`id`, `candidate_id`, `father_name`, `profile_pic`, `industry`, `functional_area`, `role`, `skills`, `allow_search`, `experience`, `minimum_salary`, `gender`, `date_of_birth`, `age`, `aadhar`, `pan`, `address`, `zip_code`, `country`, `state`, `city`, `gross_salary`, `expected_salary`, `education_level`, `languages`, `specialisms`, `description`, `website`, `facebook`, `twitter`, `linkedin`, `googleplus`, `job_type`, `employee_type`, `desired_location`, `preferred_shift`, `marital_status`, `profile_status`, `profile_completion`, `created_at`, `updated_at`) VALUES
(1, 1, 'Name', '5d6e47dae7c7b_1567508442.jpg', '1', '2', 'UI Developer', 'Tax Audit,ROC Work,Office Administrative work,Vendor Management,Payroll', NULL, '3,5', NULL, '1', '1994-05-01', NULL, NULL, NULL, 'Bangalore', 560068, NULL, 'Ka', 'Bangalore, madiwala', '65,000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Bangalore', NULL, '2', NULL, 95, '2019-09-03 05:24:22', '2019-09-10 05:12:59'),
(2, 2, 'Name', '5d6e49e79067b_1567508967.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1', '1994-05-01', NULL, NULL, NULL, 'Bangalore', 560102, NULL, 'Ka', 'Bangalore, HSR', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Bangalore', NULL, '2', NULL, 80, '2019-09-03 05:37:58', '2019-09-11 23:55:23'),
(3, 3, NULL, '5d762ee1c38c0_1568026337.jpg', NULL, NULL, NULL, NULL, NULL, '3,9', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '3,40000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Bangalore', NULL, NULL, NULL, 35, '2019-09-09 04:34:09', '2019-09-09 05:27:29'),
(4, 4, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 5, '2019-09-09 06:08:26', '2019-09-09 06:08:26'),
(7, 7, 'Name', '5d7a002429a24_1568276516.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1', '1994-05-01', NULL, NULL, NULL, 'Bangalore', 560102, NULL, 'Ka', 'Bangalore, HSR', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2', NULL, 95, '2019-09-12 02:22:08', '2019-09-12 06:02:19'),
(11, 11, 'Name', '5d7b666a59500_1568368234.png', NULL, NULL, NULL, 'Office Administrative work,Auditing,eTDS', NULL, '4,6', NULL, '1', '1994-05-01', NULL, NULL, NULL, 'Bangalore, City', 560102, NULL, 'Ka', 'Bangalore, HSR', '75,000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Bangalore', NULL, '2', NULL, 100, '2019-09-13 04:15:24', '2019-09-13 04:59:00'),
(12, 12, 'Name', '5d7b72e5a008d_1568371429.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1', '1994-05-01', NULL, NULL, NULL, 'Bangalore', 560102, NULL, 'Ka', 'Bangalore, HSR', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Bangalore', NULL, '2', NULL, 100, '2019-09-13 05:05:19', '2019-09-13 05:37:01'),
(13, 13, 'Name', '5d7b805bca3e0_1568374875.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1', '1994-05-01', NULL, NULL, NULL, 'Bangalore', 560102, NULL, 'Ka', 'Bangalore, HSR', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Bangalore', NULL, '2', NULL, 100, '2019-09-13 06:09:34', '2019-09-13 06:22:52');

-- --------------------------------------------------------

--
-- Table structure for table `career_level`
--

CREATE TABLE `career_level` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `career_level` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `employer`
--

CREATE TABLE `employer` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `profile_pic` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `company_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `allow_in_search` tinyint(1) DEFAULT NULL,
  `since` bigint(20) DEFAULT NULL,
  `team_size` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `categories` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `website` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mobile` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `zip_code` bigint(20) DEFAULT NULL,
  `country` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `state` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `city` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `facebook` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `twitter` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `googleplus` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `linkedin` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `otp_verify` tinyint(1) DEFAULT NULL,
  `email_verify` tinyint(1) DEFAULT NULL,
  `_token` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(1) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `employer`
--

INSERT INTO `employer` (`id`, `profile_pic`, `user_name`, `company_name`, `password`, `slug`, `allow_in_search`, `since`, `team_size`, `categories`, `description`, `website`, `email`, `mobile`, `phone`, `address`, `zip_code`, `country`, `state`, `city`, `facebook`, `twitter`, `googleplus`, `linkedin`, `otp_verify`, `email_verify`, `_token`, `status`, `created_at`, `updated_at`) VALUES
(1, '5d6e4c0cf11a6_1567509516.jpg', NULL, 'Digiapt', '$2y$10$rt78kM0.iQcEu9W7LoXvSOBm.f.zRpmEYRCuB8Yc2S6RaIvI2UKjW', 'digiapt', NULL, NULL, NULL, NULL, NULL, NULL, 'rohit.ghosh@digiapt.com', '9876543210', NULL, NULL, NULL, NULL, NULL, 'Bangalore', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-09-03 05:47:53', '2019-09-03 07:35:58'),
(2, NULL, NULL, 'Digiapt', '$2y$10$GmpHUbchwoBKx.nI9/6dZ.tsOV2.M5/tDMIUBBpZtWuL8HZ5LnmNG', 'digiapt', NULL, NULL, NULL, NULL, NULL, NULL, 'manojkumar@digiapt.com', '9876543213', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-09-09 06:25:41', '2019-09-09 06:25:41'),
(9, '5d7b9afc22862_1568381692.png', NULL, 'Ubisoft', '$2y$10$90DeoYrI48KLHJKm19mR0eMnevywJ8ViLWwRtAKFYmlbxGL9yOpVK', 'ubisoft', NULL, NULL, NULL, NULL, NULL, NULL, 'manojkumar.p@digiapt.com', '9876543215', NULL, NULL, NULL, NULL, NULL, 'Bangalore, Karnataka, India', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-09-13 06:38:13', '2019-09-13 08:05:14');

-- --------------------------------------------------------

--
-- Table structure for table `experience_level`
--

CREATE TABLE `experience_level` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `experience_level` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `favourite_jobs`
--

CREATE TABLE `favourite_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `candidate_id` bigint(20) UNSIGNED DEFAULT NULL,
  `jobs_id` bigint(20) UNSIGNED DEFAULT NULL,
  `training_id` bigint(20) UNSIGNED DEFAULT NULL,
  `type` tinyint(1) DEFAULT NULL,
  `status` tinyint(1) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `favourite_jobs`
--

INSERT INTO `favourite_jobs` (`id`, `candidate_id`, `jobs_id`, `training_id`, `type`, `status`, `created_at`, `updated_at`) VALUES
(1, 1, 1, NULL, 1, 1, '2019-09-10 04:07:27', '2019-09-10 04:07:27');

-- --------------------------------------------------------

--
-- Table structure for table `functional_areas`
--

CREATE TABLE `functional_areas` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `functional_areas` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `functional_areas`
--

INSERT INTO `functional_areas` (`id`, `functional_areas`, `status`, `created_at`, `updated_at`) VALUES
(1, 'IT Hardware', 1, '2019-09-03 05:32:14', '2019-09-03 05:32:14'),
(2, 'IT Software', 1, '2019-09-03 05:32:22', '2019-09-03 05:32:22');

-- --------------------------------------------------------

--
-- Table structure for table `genders`
--

CREATE TABLE `genders` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `genders`
--

INSERT INTO `genders` (`id`, `name`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Audit Assistant', 1, '2019-09-05 00:26:58', '2019-09-05 00:26:58');

-- --------------------------------------------------------

--
-- Table structure for table `industries`
--

CREATE TABLE `industries` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `industries`
--

INSERT INTO `industries` (`id`, `name`, `status`, `created_at`, `updated_at`) VALUES
(3, 'Tally Accounting', 1, '2019-09-10 01:14:57', '2019-09-10 01:14:57'),
(4, 'GST Filing', 1, '2019-09-10 01:15:08', '2019-09-10 01:15:08'),
(5, 'eTDS', 1, '2019-09-10 01:15:18', '2019-09-10 01:15:18'),
(6, 'PF/ESI', 1, '2019-09-10 01:15:28', '2019-09-10 01:15:28'),
(7, 'Quickbook Accounting', 1, '2019-09-10 01:15:37', '2019-09-10 01:15:37'),
(8, 'SAP', 1, '2019-09-10 01:16:07', '2019-09-10 01:16:07'),
(9, 'MIS Reporting', 1, '2019-09-10 01:16:13', '2019-09-10 01:16:13'),
(10, 'Cash Flow Management', 1, '2019-09-10 01:16:23', '2019-09-10 01:16:23'),
(11, 'Auditing', 1, '2019-09-10 01:16:31', '2019-09-10 01:16:31'),
(12, 'Tax Audit', 1, '2019-09-10 01:16:40', '2019-09-10 01:16:40'),
(13, 'ROC Work', 1, '2019-09-10 01:16:50', '2019-09-10 01:16:50'),
(14, 'Office Administrative work', 1, '2019-09-10 01:16:58', '2019-09-10 01:16:58'),
(15, 'Vendor Management', 1, '2019-09-10 01:17:08', '2019-09-10 01:17:08'),
(16, 'Books Finalisation', 1, '2019-09-10 01:17:17', '2019-09-10 01:17:17'),
(17, 'Payroll', 1, '2019-09-10 01:17:26', '2019-09-10 01:17:26');

-- --------------------------------------------------------

--
-- Table structure for table `jobs`
--

CREATE TABLE `jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `employer_id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `location` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `zip_code` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `country` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `state` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `city` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `job_description` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `job_type` tinyint(1) DEFAULT NULL,
  `industry` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `stipend_type` tinyint(3) DEFAULT 0,
  `stipend` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `salary_min` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `salary_max` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `salary_type` tinyint(1) DEFAULT NULL,
  `employee_type` tinyint(3) DEFAULT NULL,
  `experience` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `qualification` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `total_positions` bigint(20) NOT NULL,
  `total_views` bigint(20) DEFAULT NULL,
  `status` tinyint(1) DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `jobs`
--

INSERT INTO `jobs` (`id`, `employer_id`, `title`, `slug`, `location`, `zip_code`, `country`, `state`, `city`, `job_description`, `job_type`, `industry`, `stipend_type`, `stipend`, `salary_min`, `salary_max`, `salary_type`, `employee_type`, `experience`, `qualification`, `total_positions`, `total_views`, `status`, `created_at`, `updated_at`) VALUES
(1, 1, '1', '1', NULL, '560068', 'India', 'Karnataga', 'Bangalore', '<p>test</p>', 2, NULL, 0, NULL, '40000', '60000', 1, 2, '4-8', 'Graduate', 15, NULL, 0, '2019-09-03 05:55:56', '2019-09-13 03:01:40'),
(2, 1, '2', 'php_developer', NULL, '560068', 'India', 'Karnataga', 'Chennai', '<p>Test content</p>', 1, '1', 0, NULL, '40000', '60000', 1, 2, '4-7', 'Graduate', 20, NULL, 0, '2019-09-03 08:04:07', '2019-09-04 01:17:54'),
(3, 1, '4', '4', NULL, '560068', 'India', 'Karnataga', 'Mumbai', '<p>Demo content</p>', 2, NULL, 0, NULL, '20000', '80000', 1, 1, NULL, '2', 20, NULL, 0, '2019-09-10 07:08:01', '2019-09-10 07:08:01'),
(5, 1, '15', '15', '27th Main Road', '560102', 'Bangalore Urban', 'Karnataka', 'Bengaluru', '<p>content</p>', 1, NULL, 0, NULL, '40000', '60000', 1, 1, NULL, '2', 50, NULL, 0, '2019-09-12 04:50:02', '2019-09-12 04:50:02'),
(7, 1, 'Ca Article', 'ca_article', '27th Main Road', '560102', 'Bangalore Urban', 'Karnataka', 'Bengaluru', 'Test Content', NULL, NULL, 2, NULL, '3000', '7000', 1, 3, NULL, '4', 20, NULL, 0, '2019-09-12 07:00:20', '2019-09-12 07:00:20'),
(8, 1, 'Ca Article', 'ca_article', 'Madiwala', '560068', 'Bangalore Urban', 'Karnataka', 'Bengaluru', 'Content ca jobs', NULL, NULL, 1, '4000', NULL, NULL, 1, 3, NULL, '4', 40, NULL, 0, '2019-09-12 07:02:37', '2019-09-12 07:02:37'),
(9, 9, '5', '5', '27th Main Road', '560102', 'Bangalore Urban', 'Karnataka', 'Bengaluru', '<p>Test</p>', 1, NULL, 0, NULL, '40000', '80000', 1, 1, NULL, '3', 20, NULL, 1, '2019-09-13 07:11:39', '2019-09-13 07:30:45'),
(10, 9, '7', '7', '27th Main Road', '560102', 'Bangalore Urban', 'Karnataka', 'Chennai', '<p>Demo Content</p>', 2, NULL, 0, NULL, '50000', '70000', 1, 2, '3-6', '4', 30, NULL, 0, '2019-09-13 07:16:30', '2019-09-13 07:16:30');

-- --------------------------------------------------------

--
-- Table structure for table `jobs_applied`
--

CREATE TABLE `jobs_applied` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `candidate_id` bigint(20) UNSIGNED NOT NULL,
  `employer_id` bigint(20) UNSIGNED DEFAULT NULL,
  `jobs_id` bigint(20) UNSIGNED DEFAULT NULL,
  `training_id` bigint(20) UNSIGNED DEFAULT NULL,
  `training_status` tinyint(1) DEFAULT 0,
  `certificate` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `recruiter_status` tinyint(1) DEFAULT NULL,
  `training_completion_status` tinyint(2) DEFAULT NULL,
  `download_cv_status` tinyint(1) DEFAULT NULL,
  `application_view_date` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `training_end_date` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `download_cv_date` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `jobs_applied`
--

INSERT INTO `jobs_applied` (`id`, `candidate_id`, `employer_id`, `jobs_id`, `training_id`, `training_status`, `certificate`, `recruiter_status`, `training_completion_status`, `download_cv_status`, `application_view_date`, `training_end_date`, `download_cv_date`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 1, NULL, 0, NULL, 1, NULL, NULL, '2019-09-06', NULL, NULL, '2019-09-03 07:35:19', '2019-09-06 01:35:00'),
(2, 2, 1, NULL, 1, 1, NULL, 1, 1, NULL, '2019-09-05', '2019-09-28', NULL, '2019-09-03 23:58:48', '2019-09-13 01:01:51'),
(3, 3, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-09-09 06:02:13', '2019-09-09 06:02:13'),
(4, 4, 1, NULL, 1, 1, NULL, 1, NULL, NULL, '2019-09-09', NULL, NULL, '2019-09-09 06:16:44', '2019-09-13 00:36:53'),
(7, 11, NULL, 1, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-09-13 05:02:55', '2019-09-13 05:02:55'),
(8, 12, NULL, NULL, 2, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-09-13 05:56:41', '2019-09-13 05:56:41'),
(9, 13, NULL, 7, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-09-13 06:25:36', '2019-09-13 06:25:36');

-- --------------------------------------------------------

--
-- Table structure for table `job_title`
--

CREATE TABLE `job_title` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `job_title`
--

INSERT INTO `job_title` (`id`, `name`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Audit Assistant', 1, '2019-09-05 00:31:25', '2019-09-05 00:31:25'),
(2, 'Audit Executive', 1, '2019-09-05 00:33:27', '2019-09-05 00:33:40'),
(4, 'Auditor', 1, '2019-09-05 00:36:18', '2019-09-05 00:36:18'),
(5, 'Accounts Assistant', 1, '2019-09-05 00:36:28', '2019-09-05 00:36:28'),
(6, 'Accounts Executive', 1, '2019-09-05 00:36:38', '2019-09-05 00:36:38'),
(7, 'Accounts Manager', 1, '2019-09-05 00:36:47', '2019-09-05 00:36:47'),
(8, 'Finance Executive', 1, '2019-09-05 00:37:00', '2019-09-05 00:37:00'),
(9, 'Finance Manager', 1, '2019-09-05 00:37:12', '2019-09-05 00:37:12'),
(10, 'Taxation Executive', 1, '2019-09-05 00:37:20', '2019-09-05 00:37:20'),
(11, 'Taxation Manager', 1, '2019-09-05 00:37:30', '2019-09-05 00:37:30'),
(12, 'CFO', 1, '2019-09-05 00:37:38', '2019-09-05 00:37:38'),
(13, 'CA Fresher', 1, '2019-09-05 00:37:47', '2019-09-05 00:37:47'),
(14, 'Company Secretary', 1, '2019-09-05 00:37:58', '2019-09-05 00:37:58'),
(15, 'Book keeper', 1, '2019-09-05 00:38:08', '2019-09-05 00:38:08'),
(16, 'Quickbook Accountant', 1, '2019-09-05 00:38:17', '2019-09-05 00:38:17'),
(17, 'Cashier', 1, '2019-09-05 00:38:26', '2019-09-05 00:38:26');

-- --------------------------------------------------------

--
-- Table structure for table `job_types`
--

CREATE TABLE `job_types` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `job_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `job_types`
--

INSERT INTO `job_types` (`id`, `job_type`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Part Time', 1, '2019-09-03 05:33:18', '2019-09-03 05:33:18'),
(2, 'Full Time', 1, '2019-09-03 05:33:23', '2019-09-03 05:33:23');

-- --------------------------------------------------------

--
-- Table structure for table `languages`
--

CREATE TABLE `languages` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `languages` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `marital_status`
--

CREATE TABLE `marital_status` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `marital_status` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(29, '2019_07_03_045616_create_candidate_table', 1),
(30, '2019_07_03_062348_create_employer_table', 1),
(31, '2019_07_03_062913_create_training_table', 1),
(32, '2019_07_03_063709_create_jobs_table', 1),
(33, '2019_07_03_065316_create_jobs_applied_table', 1),
(34, '2019_07_03_065317_create_candidate_profile_table', 1),
(35, '2019_07_03_072927_create_reviews_table', 1),
(36, '2019_07_08_082002_create_resume_table', 1),
(37, '2019_07_15_060136_create_total_views_table', 1),
(38, '2019_07_15_100230_create_shortlist_candidates_table', 1),
(39, '2019_07_16_064031_create_favourite_jobs_table', 1),
(40, '2019_07_16_113234_create_selected_trainee_table', 1),
(41, '2019_07_29_134508_create_admin_table', 1),
(42, '2019_07_30_105016_create_genders_table', 1),
(43, '2019_07_30_115223_create_job_types_table', 1),
(44, '2019_07_30_122347_create_industries_table', 1),
(45, '2019_07_30_125301_create_qualifications_table', 1),
(46, '2019_07_30_132405_create_functional_areas_table', 1),
(47, '2019_07_31_051037_create_training_completion_table', 1),
(48, '2019_08_05_072704_create_career_level_table', 1),
(49, '2019_08_08_065356_create_training_duration_table', 1),
(50, '2019_08_08_071923_create_team_size_table', 1),
(51, '2019_08_08_074042_create_marital_status_table', 1),
(52, '2019_08_08_092943_create_experience_level_table', 1),
(53, '2019_08_08_095437_create_languages_table', 1),
(54, '2019_07_30_105016_create_job_title_table', 2),
(55, '2019_08_08_092943_create_training_title_table', 3),
(56, '2019_09_06_112223_create_news_and_updates_table', 4);

-- --------------------------------------------------------

--
-- Table structure for table `news_and_updates`
--

CREATE TABLE `news_and_updates` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `published` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `url` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `news_and_updates`
--

INSERT INTO `news_and_updates` (`id`, `title`, `description`, `image`, `published`, `url`, `created_at`, `updated_at`) VALUES
(171, 'Arun Jaitley: Former India finance minister dies', 'Jaitley, 66, oversaw the most significant tax reform in India since independence.', 'https://ichef.bbci.co.uk/news/1024/branded_news/6179/production/_108335942_gettyimages-1141897994-1.jpg', '2019-08-24T07:26:21Z', 'https://www.bbc.co.uk/news/world-asia-india-49367972', '2019-09-06 07:41:12', '2019-09-06 07:41:12'),
(172, 'Former Indian finance minister Jaitley dies at 66', 'Former Indian finance minister Arun Jaitley, a key ally of Prime Minister Narendra Modi, died in hospital in New Delhi on Saturday aged 66, the government said. Serving from 2014 until elections earlier this year, Jaitley oversaw the sudden withdrawal of vast…', 'https://s.yimg.com/ny/api/res/1.2/oDTd67SaKI04q9J40uzx1Q--/YXBwaWQ9aGlnaGxhbmRlcjt3PTEyODA7aD04NzEuNjY2NjY2NjY2NjY2Ng--/https://s.yimg.com/uu/api/res/1.2/SrANRJ2B1sJcrJ4U4gPhCw--~B/aD01MjM7dz03Njg7c209MTthcHBpZD15dGFjaHlvbg--/http://media.zenfs.com/en_us/News/afp.com/498f275c490b331cec4b78eef071ddc6ff08d6fb.jpg', '2019-08-24T10:39:33Z', 'https://news.yahoo.com/former-indian-finance-minister-jaitley-dies-66-103933142.html', '2019-09-06 07:41:12', '2019-09-06 07:41:12'),
(173, 'GST: non-compliance will attract heavy penalty', 'With the deadline for the payment of Goods and Services Tax (GST) just ten days away, the Tirupati GST Commissionerate has cautioned the public about the perils of non-payment.GST payers (both normal', 'https://www.thehindu.com/static/theme/default/base/img/og-image.jpg', '2019-08-20T18:51:02Z', 'https://www.thehindu.com/news/national/andhra-pradesh/gst-non-compliance-will-attract-heavy-penalty/article29185544.ece', '2019-09-06 07:41:12', '2019-09-06 07:41:12'),
(174, 'India’s government is scrambling to revive the economy', 'What is has offered may keep businesses on their feet—for a while', 'https://www.economist.com/sites/default/files/images/print-edition/20190831_FND001_0.jpg', '2019-08-29T14:50:43Z', 'https://www.economist.com/finance-and-economics/2019/08/31/indias-government-is-scrambling-to-revive-the-economy', '2019-09-06 07:41:12', '2019-09-06 07:41:12'),
(175, 'India’s government is scrambling to revive the economy', 'What is has offered may keep businesses on their feet—for a while', 'https://www.economist.com/sites/default/files/images/print-edition/20190831_FND001_0.jpg', '2019-08-29T14:45:35Z', 'https://www.economist.com/finance-and-economics/2019/08/29/indias-government-is-scrambling-to-revive-the-economy', '2019-09-06 07:41:12', '2019-09-06 07:41:12'),
(176, 'GST collections drop below Rs 1 lakh crore in Aug', 'India\'s gross GST collections slipped below Rs 1 lakh crore to Rs 98,202 crore in August, according to official data released on Sunday. Gross revenue collections from the Goods and Services Tax (GST) in July stood at Rs 1.02 lakh crore. The August 2019 mop-u…', 'https://static.toiimg.com/thumb/msid-70934650,width-1070,height-580,imgsize-179656,resizemode-6,overlay-toi_sw,pt-32,y_pad-40/photo.jpg', '2019-09-01T11:16:03Z', 'https://timesofindia.indiatimes.com/business/india-business/gst-collections-drop-below-rs-1-lakh-crore-in-august/articleshow/70934649.cms', '2019-09-06 07:41:12', '2019-09-06 07:41:12'),
(177, 'FM\'s GST move that no one saw coming', 'FM\'s GST move that no one saw comingTo ease GST woes for the MSMEs FM announced that all pending GST refunds will be paid within 30 days.', 'https://img.etimg.com/thumb/msid-70806616,width-1070,height-580,imgsize-423007,overlay-etrise/photo.jpg', '2019-08-23T13:00:54Z', 'https://economictimes.indiatimes.com/small-biz/gst/all-pending-gst-refunds-for-msmes-to-be-paid-within-30-days-fm/articleshow/70806629.cms', '2019-09-06 07:41:12', '2019-09-06 07:41:12'),
(178, 'Set up task force with state, Central officers: Amit Mitra to Nirmala Sitharaman', 'Amit Mitra has requested that the topic of GST frauds and its impact on overall GST collections needs to be discussed in depth in the upcoming GST Council meeting.', 'https://images.indianexpress.com/2018/06/amit-mitra-759.jpg?w=759', '2019-08-28T22:22:07Z', 'https://indianexpress.com/article/business/economy/set-up-task-force-with-state-central-officers-amit-mitra-to-nirmala-sitharaman-5946263/', '2019-09-06 07:41:12', '2019-09-06 07:41:12'),
(179, '‘Voluntary Aadhaar eKYC for bank a/cs, mobile soon’', 'India Business News: Ajay Bhushan Pandey wears many hats — he is revenue secy, heads GST Network & is also the CEO of UIDAI.', 'https://static.toiimg.com/thumb/msid-70730158,width-1070,height-580,imgsize-129910,resizemode-6,overlay-toi_sw,pt-32,y_pad-40/photo.jpg', '2019-08-18T21:26:29Z', 'https://timesofindia.indiatimes.com/business/india-business/voluntary-aadhaar-ekyc-for-bank-a/cs-mobile-mfs-soon/articleshow/70730154.cms', '2019-09-06 07:41:12', '2019-09-06 07:41:12'),
(180, 'Provincial sales tax and carbon levy needed to help balance Alberta\'s books: U of C professor - Calgary Herald', 'Provincial sales tax and carbon levy needed to help balance Alberta\'s books: U of C professor Calgary Herald \'It\'s worrisome\': Alberta nurses union slams MacKinnon report recommendations Edmonton Journal Alberta is entering a time of restraint, says finance m…', 'https://postmediacalgaryherald2.files.wordpress.com/2019/09/mckenzie.jpg', '2019-09-05T20:04:04Z', 'https://calgaryherald.com/news/local-news/provincial-sales-tax-and-carbon-levy-needed-to-help-balance-albertas-books-u-of-c-professor', '2019-09-06 07:41:12', '2019-09-06 07:41:12'),
(181, 'Govt may not offer GST concessions to auto firms', 'The government is unlikely to offer goods and services tax (GST) concessions to the auto sector. The reason for this is that the government believes that companies have been slow in responding to consumer demand on BS-VI.', 'https://static.toiimg.com/thumb/msid-70625219,width-1070,height-580,imgsize-1328798,resizemode-6,overlay-toi_sw,pt-32,y_pad-40/photo.jpg', '2019-08-10T20:47:28Z', 'https://timesofindia.indiatimes.com/business/india-business/government-unlikely-to-offer-gst-concessions-to-auto-firms/articleshow/70625216.cms', '2019-09-06 07:41:12', '2019-09-06 07:41:12'),
(182, 'GST collections in Aug drop below 1 lakh crore', 'Goods and services tax (GST) collections dropped below the psychological Rs 1 lakh crore in August 2019, which was 4.5% higher than a year ago but 3.8% lower than the mop-up in July.', 'https://static.toiimg.com/thumb/msid-70940856,width-1070,height-580,imgsize-123104,resizemode-6,overlay-toi_sw,pt-32,y_pad-40/photo.jpg', '2019-09-01T23:24:33Z', 'https://timesofindia.indiatimes.com/business/india-business/gst-collections-in-august-drop-below-key-level-of-1-lakh-crore/articleshow/70940852.cms', '2019-09-06 07:41:13', '2019-09-06 07:41:13'),
(183, 'Delhi businessman loses Rs 7.8L after \'GST call\'', 'A businessman, while on a trip to Philippines, was in for a shock when he found that someone had siphoned off approximately Rs 7.8 lakh from his bank account. As soon as he returned and contacted the bank, he found that someone had got the registered mobile n…', 'https://static.toiimg.com/thumb/msid-70824321,width-1070,height-580,imgsize-436644,resizemode-6,overlay-toi_sw,pt-32,y_pad-40/photo.jpg', '2019-08-25T01:40:18Z', 'https://timesofindia.indiatimes.com/city/delhi/bizman-loses-rs-7-8l-following-gst-call/articleshow/70823549.cms', '2019-09-06 07:41:13', '2019-09-06 07:41:13'),
(184, 'Don\'t overreach, overstate: FM to tax authorities', 'Finance minister Nirmala Sitharaman said that she has asked the tax authorities to observe a \"bit of restraint\" and not to \"overreach\" while going about tax collection. \"Businesses create jobs, create wealth for the country and therefore it is important for t…', 'https://static.toiimg.com/thumb/msid-70864086,width-1070,height-580,imgsize-1647811,resizemode-6,overlay-toi_sw,pt-32,y_pad-40/photo.jpg', '2019-08-27T16:32:06Z', 'https://timesofindia.indiatimes.com/business/india-business/dont-overreach-overstate-nirmala-sitharaman-to-tax-authorities/articleshow/70863994.cms', '2019-09-06 07:41:13', '2019-09-06 07:41:13'),
(185, 'Stern action if GST returns are not filed, warns Chief Commissioner', 'August 31 last date for 2017-18 returns', 'https://www.thehindu.com/news/national/andhra-pradesh/bjymzn/article29114642.ece/ALTERNATES/LANDSCAPE_615/17-VZ-GSTWARNING', '2019-08-16T20:08:09Z', 'https://www.thehindu.com/news/national/andhra-pradesh/stern-action-if-gst-returns-are-not-filed-warns-chief-commissioner/article29114643.ece', '2019-09-06 07:41:13', '2019-09-06 07:41:13'),
(186, 'Demand down, government seeks solution in new GST math', 'Finance ministry studying revenue impact of lower rates; some states back cut in GST.', 'https://img.etimg.com/thumb/msid-70986245,width-1070,height-580,imgsize-392088,overlay-economictimes/photo.jpg', '2019-09-05T01:39:17Z', 'https://economictimes.indiatimes.com/news/economy/policy/demand-down-government-seeks-solution-in-new-gst-math/articleshow/70986166.cms', '2019-09-06 07:41:13', '2019-09-06 07:41:13'),
(187, 'August GST revenue sees 4.5% rise over last year, but lowest so far in FY20', 'GST collections, however, recorded 4.51 per cent growth from Rs 93,960 crore collected in August last year.', 'https://images.indianexpress.com/2019/06/gst.jpg?w=759', '2019-09-01T21:30:13Z', 'https://indianexpress.com/article/business/economy/august-gst-revenue-sees-4-5-per-cent-rise-over-last-year-but-lowest-so-far-in-fy20-5957359/', '2019-09-06 07:41:13', '2019-09-06 07:41:13'),
(188, 'Automobile sales drop 18.71% in July', 'This is amid a deepening crisis in the auto sector that has triggered massive job losses.', 'https://www.thehindu.com/todays-paper/tp-business/ado18y/article29048306.ece/ALTERNATES/LANDSCAPE_615/01thgmn01BIZ-LGF66894RD3jpgjpg', '2019-08-13T16:57:57Z', 'https://www.thehindu.com/news/national/automobile-sales-drop-1871-in-july/article29047942.ece', '2019-09-06 07:41:13', '2019-09-06 07:41:13'),
(189, 'Not lack of money, risk aversion kept investors away: Kunj Bansal', 'Pending GST cut, whatever could be done by the finance minister, has been done for auto sector.', 'https://img.etimg.com/thumb/msid-70853101,width-1070,height-580,imgsize-136286,overlay-etmarkets/photo.jpg', '2019-08-27T05:02:26Z', 'https://economictimes.indiatimes.com/markets/expert-view/not-lack-of-money-risk-aversion-kept-investors-away-kunj-bansal/articleshow/70853100.cms', '2019-09-06 07:41:13', '2019-09-06 07:41:13'),
(190, 'India’s tax admin needs a mindset change: FM', 'Amid allegations of “tax terrorism”, finance minister Nirmala Sitharaman has sought a mindset change in tax officials while advocating a technology-driven approach. Echoing PM Modi’s resolve that honest taxpayers should not be harassed, Sitharaman underscored…', 'https://static.toiimg.com/thumb/msid-70708198,width-1070,height-580,imgsize-1675091,resizemode-6,overlay-toi_sw,pt-32,y_pad-40/photo.jpg', '2019-08-16T19:55:52Z', 'https://timesofindia.indiatimes.com/business/india-business/indias-tax-administration-needs-a-mindset-change-fm/articleshow/70708201.cms', '2019-09-06 07:41:13', '2019-09-06 07:41:13'),
(191, 'GST rate cut on auto to be political call', 'India Business News: NEW DELHI: A decision to reduce goods and services tax (GST) on automobiles and other products will be a political call even as finance ministry offic.', 'https://static.toiimg.com/thumb/msid-71040690,width-1070,height-580,imgsize-170262,resizemode-6,overlay-toi_sw,pt-32,y_pad-40/photo.jpg', '2019-09-08T22:54:56Z', 'https://timesofindia.indiatimes.com/business/india-business/gst-rate-cut-on-auto-other-products-to-be-political-call/articleshow/71040689.cms', '2019-09-08 23:55:41', '2019-09-08 23:55:41'),
(192, 'Thakur to automakers: Take up GST cut demand with state FMs', 'The Centre has already indicated that it will take up the proposal of Goods and Services Tax (GST) rate cut on automobiles to the GST Council, which is meeting later this month in Goa.', 'https://images.indianexpress.com/2017/06/thakur-759.jpg?w=759', '2019-09-06T22:01:09Z', 'https://indianexpress.com/article/business/economy/anurag-thakur-automakers-gst-cut-demand-with-state-fms-5973936/', '2019-09-08 23:55:42', '2019-09-08 23:55:42'),
(193, 'GST collections in State miss the target', 'Slump in automobile sector and lull in construction activity said to be the cause', 'https://www.thehindu.com/static/theme/default/base/img/og-image.jpg', '2019-09-10T17:58:59Z', 'https://www.thehindu.com/news/national/andhra-pradesh/gst-collections-in-state-miss-the-target/article29386155.ece', '2019-09-10 23:38:44', '2019-09-10 23:38:44'),
(194, 'BJP misusing power to establish one-party rule: Prithviraj Chavan', 'Congress demands slashing of GST rates for struggling automobile, construction sectors.', 'https://images.indianexpress.com/2019/06/prithviraj-chavan-759.jpg?w=759', '2019-09-10T21:04:31Z', 'https://indianexpress.com/article/cities/mumbai/bjp-misusing-power-to-establish-one-party-rule-prithviraj-chavan-5984402/', '2019-09-10 23:38:44', '2019-09-10 23:38:44'),
(195, 'GST relief for auto cos may raise burden on others', 'India Business News: NEW DELHI: Amid intense lobbying for a reduction in goods and services tax (GST), the government is expected to send a message that a tax cut is only .', 'https://static.toiimg.com/thumb/msid-71089545,width-1070,height-580,imgsize-11880447,resizemode-6,overlay-toi_sw,pt-32,y_pad-40/photo.jpg', '2019-09-12T01:22:47Z', 'https://timesofindia.indiatimes.com/business/india-business/gst-relief-for-auto-cos-may-raise-burden-on-other-goods/articleshow/71089543.cms', '2019-09-11 23:36:02', '2019-09-11 23:36:02'),
(196, 'Raids unearth Rs 470cr fraudulent GST claims', 'In a massive nationwide search operation carried out on exporters and their suppliers covering 336 locations on Wednesday, nearly 1,200 officers from the Directorate of Revenue Intelligence and Directorate General of GST Intelligence unearthed fraudulent clai…', 'https://static.toiimg.com/thumb/msid-71104418,width-1070,height-580,imgsize-1764869,resizemode-6,overlay-toi_sw,pt-32,y_pad-40/photo.jpg', '2019-09-12T20:42:52Z', 'https://timesofindia.indiatimes.com/business/india-business/raids-unearth-rs-470-crore-fraudulent-gst-claims/articleshow/71104407.cms', '2019-09-12 23:45:51', '2019-09-12 23:45:51'),
(197, 'Govt unlikely to offer GST concessions to auto cos', 'India Business News: NEW DELHI: The government is unlikely to offer GST concessions to the auto sector as it believes that the current slowdown is the result of higher gro.', 'https://static.toiimg.com/thumb/msid-71104835,width-1070,height-580,imgsize-12231550,resizemode-6,overlay-toi_sw,pt-32,y_pad-40/photo.jpg', '2019-09-13T01:10:01Z', 'https://timesofindia.indiatimes.com/business/india-business/govt-unlikely-to-offer-gst-concessions-to-auto-companies/articleshow/71104834.cms', '2019-09-12 23:45:51', '2019-09-12 23:45:51');

-- --------------------------------------------------------

--
-- Table structure for table `qualifications`
--

CREATE TABLE `qualifications` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `qualification` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `qualifications`
--

INSERT INTO `qualifications` (`id`, `qualification`, `status`, `created_at`, `updated_at`) VALUES
(1, '12th Pass', 1, '2019-09-05 00:57:56', '2019-09-05 00:57:56'),
(2, 'B.Com', 1, '2019-09-05 00:58:05', '2019-09-05 00:58:05'),
(3, 'M.Com', 1, '2019-09-05 00:58:16', '2019-09-05 00:58:16'),
(4, 'BBA', 1, '2019-09-05 00:58:28', '2019-09-05 00:58:28'),
(5, 'MBA', 1, '2019-09-05 00:58:34', '2019-09-05 00:58:34'),
(6, 'CA Inter', 1, '2019-09-05 00:58:43', '2019-09-05 00:58:43'),
(7, 'Chartered Accountant', 1, '2019-09-05 00:58:53', '2019-09-05 00:58:53'),
(8, 'Company Secretary', 1, '2019-09-05 00:59:04', '2019-09-05 00:59:04'),
(9, 'ICWA', 1, '2019-09-05 00:59:16', '2019-09-05 00:59:16'),
(10, 'GST Certified professional', 1, '2019-09-05 00:59:21', '2019-09-05 00:59:21'),
(11, 'CPT', 1, NULL, NULL),
(12, 'CA IPCC', 1, NULL, NULL),
(13, 'CA IPCC Single Group', 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `resume`
--

CREATE TABLE `resume` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `candidate_id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `department` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `from_year` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `from_month` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `to_year` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `to_month` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `worked_till` tinyint(1) DEFAULT NULL,
  `current_salary_lacs` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `current_salary_thousand` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cv` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `notice_period` bigint(20) DEFAULT NULL,
  `course_type` tinyint(1) DEFAULT NULL,
  `percentage` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `type` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `resume`
--

INSERT INTO `resume` (`id`, `candidate_id`, `title`, `name`, `department`, `from_year`, `from_month`, `to_year`, `to_month`, `worked_till`, `current_salary_lacs`, `current_salary_thousand`, `cv`, `description`, `notice_period`, `course_type`, `percentage`, `type`, `created_at`, `updated_at`) VALUES
(1, 1, '<p>Resume is most important document recruiter look for. Recruiters generally do not look at profiles without resumes.</p>', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '5d6e47ef7ff6b_1567508463.pdf', NULL, NULL, NULL, NULL, 4, '2019-09-03 05:31:03', '2019-09-03 05:31:16'),
(3, 1, NULL, 'Digiapt software technologies', '4', '2010', '8', '2019', '09', 1, '45,000', '1', NULL, 'Test', 2, NULL, NULL, 2, '2019-09-03 05:35:05', '2019-09-10 02:06:28'),
(5, 2, '<p>Resume is most important document recruiter look for. Recruiters generally do not look at profiles without resum</p>', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '5d6e49b2cc9db_1567508914.pdf', NULL, NULL, NULL, NULL, 4, '2019-09-03 05:38:34', '2019-09-03 05:38:45'),
(10, 3, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '5d7630198de61_1568026649.pdf', NULL, NULL, NULL, NULL, 4, '2019-09-09 05:27:29', '2019-09-09 05:27:29'),
(11, 1, '2', 'GHS school', '2', '2018', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '50%', 1, '2019-09-09 11:45:13', '2019-09-09 11:45:13'),
(16, 2, '4', 'GHS school', '2', '2014', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '80%', 1, '2019-09-10 05:52:49', '2019-09-10 05:55:19'),
(17, 3, '4', 'GHS school', '2', '2019', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '50%', 1, '2019-09-09 11:45:13', '2019-09-09 11:45:13'),
(19, 7, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '5d79fedc6ee3d_1568276188.pdf', NULL, NULL, NULL, NULL, 4, '2019-09-12 02:46:28', '2019-09-12 02:46:28'),
(20, 7, '11', 'BSR Technology', '11', '2018', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '90%', 1, '2019-09-12 02:56:31', '2019-09-12 03:00:17'),
(21, 11, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '5d7b66c7c2d29_1568368327.pdf', NULL, NULL, NULL, NULL, 4, '2019-09-13 04:21:27', '2019-09-13 04:22:07'),
(24, 11, NULL, 'Digiapt software technologies', '2', '2016', '03', '2019', '09', 1, NULL, NULL, NULL, 'Test', 1, NULL, NULL, 2, '2019-09-13 04:43:41', '2019-09-13 04:43:41'),
(31, 11, NULL, 'Vlbjcas', '4', '2007', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '50%', 1, '2019-09-13 04:56:59', '2019-09-13 04:56:59'),
(33, 12, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '5d7b752bc16bb_1568372011.pdf', NULL, NULL, NULL, NULL, 4, '2019-09-13 05:19:01', '2019-09-13 05:23:31'),
(39, 12, NULL, 'Vlbjcas', '3', '2014', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '50%', 1, '2019-09-13 05:35:12', '2019-09-13 05:35:12'),
(40, 13, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '5d7b81ff1c885_1568375295.pdf', NULL, NULL, NULL, NULL, 4, '2019-09-13 06:16:16', '2019-09-13 06:18:15'),
(41, 13, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '5d7b818c33614_1568375180.pdf', NULL, NULL, NULL, NULL, 4, '2019-09-13 06:16:20', '2019-09-13 06:16:20'),
(44, 13, NULL, 'Vlbjcas', '3', '2016', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '70%', 1, '2019-09-13 06:22:26', '2019-09-13 06:22:26');

-- --------------------------------------------------------

--
-- Table structure for table `reviews`
--

CREATE TABLE `reviews` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `candidate_id` bigint(20) UNSIGNED DEFAULT NULL,
  `training_id` bigint(20) UNSIGNED DEFAULT NULL,
  `review_stars` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `feedback` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `selected_trainee`
--

CREATE TABLE `selected_trainee` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `candidate_id` bigint(20) UNSIGNED NOT NULL,
  `employer_id` bigint(20) UNSIGNED DEFAULT NULL,
  `training_id` bigint(20) UNSIGNED DEFAULT NULL,
  `training_start_date` date DEFAULT NULL,
  `training_end_date` date DEFAULT NULL,
  `training_status` tinyint(1) DEFAULT NULL,
  `certificate` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `shortlist_candidates`
--

CREATE TABLE `shortlist_candidates` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `candidate_id` bigint(20) UNSIGNED DEFAULT NULL,
  `employer_id` bigint(20) UNSIGNED DEFAULT NULL,
  `shortlist_status` tinyint(1) DEFAULT NULL,
  `shortlisted_type` tinyint(1) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `shortlist_candidates`
--

INSERT INTO `shortlist_candidates` (`id`, `candidate_id`, `employer_id`, `shortlist_status`, `shortlisted_type`, `created_at`, `updated_at`) VALUES
(1, 1, 1, NULL, 2, '2019-09-03 05:54:10', '2019-09-03 05:54:10');

-- --------------------------------------------------------

--
-- Table structure for table `team_size`
--

CREATE TABLE `team_size` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `team_size` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `total_views`
--

CREATE TABLE `total_views` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `candidate_id` bigint(20) UNSIGNED NOT NULL,
  `jobs_id` bigint(20) UNSIGNED DEFAULT NULL,
  `training_id` bigint(20) UNSIGNED DEFAULT NULL,
  `total_views` tinyint(1) DEFAULT NULL,
  `views_type` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `total_views`
--

INSERT INTO `total_views` (`id`, `candidate_id`, `jobs_id`, `training_id`, `total_views`, `views_type`, `created_at`, `updated_at`) VALUES
(1, 1, 1, NULL, 1, '1', '2019-09-03 07:18:36', '2019-09-03 07:18:36'),
(2, 2, NULL, 1, 1, '1', '2019-09-03 23:58:45', '2019-09-03 23:58:45'),
(3, 2, NULL, 2, 1, '1', '2019-09-05 05:06:23', '2019-09-05 05:06:23'),
(4, 3, 1, NULL, 1, '1', '2019-09-09 05:49:18', '2019-09-09 05:49:18'),
(5, 4, NULL, 1, 1, '1', '2019-09-09 06:13:39', '2019-09-09 06:13:39'),
(6, 1, 2, NULL, 1, '1', '2019-09-11 05:37:21', '2019-09-11 05:37:21'),
(8, 11, 1, NULL, 1, '1', '2019-09-13 04:23:59', '2019-09-13 04:23:59'),
(9, 12, NULL, 1, 1, '1', '2019-09-13 05:51:43', '2019-09-13 05:51:43'),
(10, 12, NULL, 2, 1, '1', '2019-09-13 05:56:31', '2019-09-13 05:56:31');

-- --------------------------------------------------------

--
-- Table structure for table `training`
--

CREATE TABLE `training` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `employer_id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `start_date` date NOT NULL,
  `duration` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `stipend` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_date_of_application` date NOT NULL,
  `training_completion` tinyint(1) NOT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `training_status` tinyint(1) DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `training`
--

INSERT INTO `training` (`id`, `employer_id`, `title`, `slug`, `start_date`, `duration`, `stipend`, `last_date_of_application`, `training_completion`, `description`, `training_status`, `created_at`, `updated_at`) VALUES
(1, 1, '3', '2', '2019-09-04', '2 Months', '2000', '2019-09-29', 1, '<p>Test</p>', 0, '2019-09-03 06:27:15', '2019-09-13 00:21:53'),
(2, 1, '1', '1', '2019-09-07', '6 Months', '6000', '2019-09-13', 2, '<p>Test content</p>', 0, '2019-09-05 03:06:50', '2019-09-05 03:06:50'),
(3, 9, '1', '1', '2019-09-19', '2 Months', '6000', '2019-09-27', 2, 'Test', 0, '2019-09-13 07:48:38', '2019-09-13 08:24:49');

-- --------------------------------------------------------

--
-- Table structure for table `training_completion`
--

CREATE TABLE `training_completion` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `training_completion` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `training_completion`
--

INSERT INTO `training_completion` (`id`, `training_completion`, `status`, `created_at`, `updated_at`) VALUES
(1, '0-10000', 1, '2019-09-03 05:32:40', '2019-09-03 05:32:40'),
(2, '10000-15000', 1, '2019-09-03 05:32:46', '2019-09-03 05:32:46'),
(3, '15000-20000', 1, '2019-09-03 05:32:53', '2019-09-03 05:32:53'),
(4, '20000-25000', 1, '2019-09-03 05:33:03', '2019-09-03 05:33:03');

-- --------------------------------------------------------

--
-- Table structure for table `training_duration`
--

CREATE TABLE `training_duration` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `training_duration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `training_title`
--

CREATE TABLE `training_title` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `training_title`
--

INSERT INTO `training_title` (`id`, `title`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Internship for Audit Executive', 1, '2019-09-05 02:07:23', '2019-09-05 02:10:15'),
(3, 'Internship for Accounts Executive', 1, '2019-09-05 02:11:56', '2019-09-05 02:11:56'),
(4, 'Internship for Taxation Executive', 1, '2019-09-05 02:12:05', '2019-09-05 02:12:05'),
(5, 'Internship for GST Expert', 1, '2019-09-05 02:12:15', '2019-09-05 02:12:15'),
(6, 'Internship for ROC Compliance', 1, '2019-09-05 02:12:25', '2019-09-05 02:12:25'),
(7, 'Internship for Payroll Management', 1, '2019-09-05 02:12:33', '2019-09-05 02:12:33');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `candidate`
--
ALTER TABLE `candidate`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `candidate_profile`
--
ALTER TABLE `candidate_profile`
  ADD PRIMARY KEY (`id`),
  ADD KEY `candidate_profile_candidate_id_foreign` (`candidate_id`);

--
-- Indexes for table `career_level`
--
ALTER TABLE `career_level`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `employer`
--
ALTER TABLE `employer`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `experience_level`
--
ALTER TABLE `experience_level`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `favourite_jobs`
--
ALTER TABLE `favourite_jobs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `favourite_jobs_candidate_id_foreign` (`candidate_id`),
  ADD KEY `favourite_jobs_jobs_id_foreign` (`jobs_id`),
  ADD KEY `favourite_jobs_training_id_foreign` (`training_id`);

--
-- Indexes for table `functional_areas`
--
ALTER TABLE `functional_areas`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `genders`
--
ALTER TABLE `genders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `industries`
--
ALTER TABLE `industries`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `jobs`
--
ALTER TABLE `jobs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `jobs_employer_id_foreign` (`employer_id`);

--
-- Indexes for table `jobs_applied`
--
ALTER TABLE `jobs_applied`
  ADD PRIMARY KEY (`id`),
  ADD KEY `jobs_applied_training_id_foreign` (`training_id`),
  ADD KEY `jobs_applied_jobs_id_foreign` (`jobs_id`),
  ADD KEY `jobs_applied_candidate_id_foreign` (`candidate_id`),
  ADD KEY `jobs_applied_employer_id_foreign` (`employer_id`);

--
-- Indexes for table `job_title`
--
ALTER TABLE `job_title`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `job_types`
--
ALTER TABLE `job_types`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `languages`
--
ALTER TABLE `languages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `marital_status`
--
ALTER TABLE `marital_status`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `news_and_updates`
--
ALTER TABLE `news_and_updates`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `qualifications`
--
ALTER TABLE `qualifications`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `resume`
--
ALTER TABLE `resume`
  ADD PRIMARY KEY (`id`),
  ADD KEY `resume_candidate_id_foreign` (`candidate_id`);

--
-- Indexes for table `reviews`
--
ALTER TABLE `reviews`
  ADD PRIMARY KEY (`id`),
  ADD KEY `reviews_candidate_id_foreign` (`candidate_id`),
  ADD KEY `reviews_training_id_foreign` (`training_id`);

--
-- Indexes for table `selected_trainee`
--
ALTER TABLE `selected_trainee`
  ADD PRIMARY KEY (`id`),
  ADD KEY `selected_trainee_training_id_foreign` (`training_id`),
  ADD KEY `selected_trainee_employer_id_foreign` (`employer_id`),
  ADD KEY `selected_trainee_candidate_id_foreign` (`candidate_id`);

--
-- Indexes for table `shortlist_candidates`
--
ALTER TABLE `shortlist_candidates`
  ADD PRIMARY KEY (`id`),
  ADD KEY `shortlist_candidates_candidate_id_foreign` (`candidate_id`),
  ADD KEY `shortlist_candidates_employer_id_foreign` (`employer_id`);

--
-- Indexes for table `team_size`
--
ALTER TABLE `team_size`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `total_views`
--
ALTER TABLE `total_views`
  ADD PRIMARY KEY (`id`),
  ADD KEY `total_views_training_id_foreign` (`training_id`),
  ADD KEY `total_views_jobs_id_foreign` (`jobs_id`),
  ADD KEY `total_views_candidate_id_foreign` (`candidate_id`);

--
-- Indexes for table `training`
--
ALTER TABLE `training`
  ADD PRIMARY KEY (`id`),
  ADD KEY `training_employer_id_foreign` (`employer_id`);

--
-- Indexes for table `training_completion`
--
ALTER TABLE `training_completion`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `training_duration`
--
ALTER TABLE `training_duration`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `training_title`
--
ALTER TABLE `training_title`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `candidate`
--
ALTER TABLE `candidate`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `candidate_profile`
--
ALTER TABLE `candidate_profile`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `career_level`
--
ALTER TABLE `career_level`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `employer`
--
ALTER TABLE `employer`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `experience_level`
--
ALTER TABLE `experience_level`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `favourite_jobs`
--
ALTER TABLE `favourite_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `functional_areas`
--
ALTER TABLE `functional_areas`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `genders`
--
ALTER TABLE `genders`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `industries`
--
ALTER TABLE `industries`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `jobs`
--
ALTER TABLE `jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `jobs_applied`
--
ALTER TABLE `jobs_applied`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `job_title`
--
ALTER TABLE `job_title`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `job_types`
--
ALTER TABLE `job_types`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `languages`
--
ALTER TABLE `languages`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `marital_status`
--
ALTER TABLE `marital_status`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=57;

--
-- AUTO_INCREMENT for table `news_and_updates`
--
ALTER TABLE `news_and_updates`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=198;

--
-- AUTO_INCREMENT for table `qualifications`
--
ALTER TABLE `qualifications`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `resume`
--
ALTER TABLE `resume`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=45;

--
-- AUTO_INCREMENT for table `reviews`
--
ALTER TABLE `reviews`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `selected_trainee`
--
ALTER TABLE `selected_trainee`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `shortlist_candidates`
--
ALTER TABLE `shortlist_candidates`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `team_size`
--
ALTER TABLE `team_size`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `total_views`
--
ALTER TABLE `total_views`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `training`
--
ALTER TABLE `training`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `training_completion`
--
ALTER TABLE `training_completion`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `training_duration`
--
ALTER TABLE `training_duration`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `training_title`
--
ALTER TABLE `training_title`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `candidate_profile`
--
ALTER TABLE `candidate_profile`
  ADD CONSTRAINT `candidate_profile_candidate_id_foreign` FOREIGN KEY (`candidate_id`) REFERENCES `candidate` (`id`);

--
-- Constraints for table `favourite_jobs`
--
ALTER TABLE `favourite_jobs`
  ADD CONSTRAINT `favourite_jobs_candidate_id_foreign` FOREIGN KEY (`candidate_id`) REFERENCES `candidate` (`id`),
  ADD CONSTRAINT `favourite_jobs_jobs_id_foreign` FOREIGN KEY (`jobs_id`) REFERENCES `jobs` (`id`),
  ADD CONSTRAINT `favourite_jobs_training_id_foreign` FOREIGN KEY (`training_id`) REFERENCES `training` (`id`);

--
-- Constraints for table `jobs`
--
ALTER TABLE `jobs`
  ADD CONSTRAINT `jobs_employer_id_foreign` FOREIGN KEY (`employer_id`) REFERENCES `employer` (`id`);

--
-- Constraints for table `jobs_applied`
--
ALTER TABLE `jobs_applied`
  ADD CONSTRAINT `jobs_applied_candidate_id_foreign` FOREIGN KEY (`candidate_id`) REFERENCES `candidate` (`id`),
  ADD CONSTRAINT `jobs_applied_employer_id_foreign` FOREIGN KEY (`employer_id`) REFERENCES `employer` (`id`),
  ADD CONSTRAINT `jobs_applied_jobs_id_foreign` FOREIGN KEY (`jobs_id`) REFERENCES `jobs` (`id`),
  ADD CONSTRAINT `jobs_applied_training_id_foreign` FOREIGN KEY (`training_id`) REFERENCES `training` (`id`);

--
-- Constraints for table `resume`
--
ALTER TABLE `resume`
  ADD CONSTRAINT `resume_candidate_id_foreign` FOREIGN KEY (`candidate_id`) REFERENCES `candidate` (`id`);

--
-- Constraints for table `reviews`
--
ALTER TABLE `reviews`
  ADD CONSTRAINT `reviews_candidate_id_foreign` FOREIGN KEY (`candidate_id`) REFERENCES `candidate` (`id`),
  ADD CONSTRAINT `reviews_training_id_foreign` FOREIGN KEY (`training_id`) REFERENCES `training` (`id`);

--
-- Constraints for table `selected_trainee`
--
ALTER TABLE `selected_trainee`
  ADD CONSTRAINT `selected_trainee_candidate_id_foreign` FOREIGN KEY (`candidate_id`) REFERENCES `candidate` (`id`),
  ADD CONSTRAINT `selected_trainee_employer_id_foreign` FOREIGN KEY (`employer_id`) REFERENCES `employer` (`id`),
  ADD CONSTRAINT `selected_trainee_training_id_foreign` FOREIGN KEY (`training_id`) REFERENCES `training` (`id`);

--
-- Constraints for table `shortlist_candidates`
--
ALTER TABLE `shortlist_candidates`
  ADD CONSTRAINT `shortlist_candidates_candidate_id_foreign` FOREIGN KEY (`candidate_id`) REFERENCES `candidate` (`id`),
  ADD CONSTRAINT `shortlist_candidates_employer_id_foreign` FOREIGN KEY (`employer_id`) REFERENCES `employer` (`id`);

--
-- Constraints for table `total_views`
--
ALTER TABLE `total_views`
  ADD CONSTRAINT `total_views_candidate_id_foreign` FOREIGN KEY (`candidate_id`) REFERENCES `candidate` (`id`),
  ADD CONSTRAINT `total_views_jobs_id_foreign` FOREIGN KEY (`jobs_id`) REFERENCES `jobs` (`id`),
  ADD CONSTRAINT `total_views_training_id_foreign` FOREIGN KEY (`training_id`) REFERENCES `training` (`id`);

--
-- Constraints for table `training`
--
ALTER TABLE `training`
  ADD CONSTRAINT `training_employer_id_foreign` FOREIGN KEY (`employer_id`) REFERENCES `employer` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
