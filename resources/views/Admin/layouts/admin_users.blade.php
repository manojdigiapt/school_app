@extends('Admin.base')

@section('Content')
<div id="page-content-wrapper">
        <div class="page-content">
            <!-- Content Header (Page header) -->
            {{-- <section class="content-header z-depth-1">
                <div class="header-icon">
                    <i class="fa fa-table"></i>
                </div>
                <div class="header-title">
                    <h1> Admin Users List</h1>
                    <ul class="link hidden-xs">
                        <li><a href="/Admin/Dashboard"><i class="fa fa-home"></i>Home</a></li>
                        <li><a href="table.html">Table</a></li>
                    </ul>
                </div>
            </section> --}}
            <!-- page section -->
            <br>
            <div class="container-fluid">
                <div class="row">
                    <div class="fixed-action-btn">
                        <a href="javascript:void(0);" class="btn-floating btn-large red" data-toggle="modal" data-target="#AddUsers" title="Add new admin users">
                            <i class="large material-icons">add</i>
                        </a>
                    </div>
                    <!-- bootstrap table -->
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="card">
                            <div class="card-header">
                                <i class="fa fa-table fa-lg"></i>
                            <h2>{{$title}}</h2>
                            </div>
                            <div class="card-content">
                                <div class="table-responsive">
                                    <table class="table table-bordered table-hover">
                                        <thead>
                                            <tr>
                                                {{-- <th>Select</th> --}}
                                                <th>Name</th>
                                                <th>Email </th>
                                                <th>Role </th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($GetAdmins as $Admins)
                                            <tr>
                                                <td>{{$Admins->name}}</td>
                                                <td>{{$Admins->email}}</td>
                                                <td>@if($Admins->role_id == 1)
                                                        Super Admin
                                                    @else
                                                        Sub Admin
                                                    @endif
                                                </td>
                                                <td>
                                                    <a href="javascript:void(0);" class="btn btn-sm" data-toggle="tooltip" data-placement="left" title="Update" onclick="EditAdminUsers({{$Admins->id}});"><i class="fa fa-pencil" aria-hidden="true"></i></a>

                                                    <a href="javascript:void(0);" class="btn btn-danger btn-sm" data-toggle="tooltip" data-placement="right" title="Delete " onclick="DeleteAdminUsers({{$Admins->id}});"><i class="fa fa-trash-o" aria-hidden="true"></i></a>
                                                </td>
                                            </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- ./bootstrap table -->
                    <!-- ./data table -->
                </div>
                <!-- ./row -->
            </div>
            <!-- ./cotainer -->

        </div>
        <!-- ./page-content -->
    </div>




    {{-- Add user Modal --}}
    <div id="AddUsers" class="modal fade" role="dialog">
            <div class="modal-dialog modal-md">
          
              <!-- Modal content-->
              <div class="modal-content">
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                  <h4 class="modal-title">Add Users</h4>
                </div>
                <div class="modal-body">
                        <div class="card-content" id="color-button">
                                <div class="row">
                                    <form class="col s11 m-t-20">
                                        <div class="input-field form-input col s12">
                                            <i class="material-icons prefix">account_circle</i>
                                            <input id="name" type="tel" class="validate" placeholder="Admin name">
                                            {{--  <label for="icon_prefix">Name</label>  --}}
                                        </div>
                                        <div class="input-field form-input col s12">
                                            <i class="material-icons prefix">email</i>
                                            <input type="email" id="email" class="validate" placeholder="Admin email">
                                            {{--  <label>Email</label>  --}}
                                        </div>
                                        <div class="input-field form-input form-input-select col s12">
                                            <i class="material-icons prefix">account_circle</i>
                                            <select class="icons" id="role">
                                                <option selected>Choose Admin Role</option>
                                                <option value="1">Super Admin</option>
                                                <option value="2">Sub Admin</option>
                                            </select>
                                            {{--  <label>Select Role</label>  --}}
                                        </div>
                                        <div class="input-field form-input col s12">
                                            <i class="material-icons prefix">lock</i>
                                            <input type="password" id="password" class="validate" placeholder="Admin password">
                                            {{--  <label>Password</label>  --}}
                                        </div>
                                        <a href="javascript:void(0);" id="AddAdminUsers" class="waves-effect waves-light btn teal pull-right"><i class="material-icons left">save</i>submit</a>

                                    </form>
                                </div>

                            </div>
                </div>
                {{--  <div class="modal-footer">
                  <button type="button" id="AddAdminUsers" class="btn btn-success modal-save-btn-mar-right">Save</button>  --}}
                </div>
              </div>
          
            </div>
          </div>


          {{-- Edit user Modal --}}
    <div id="EditUsers" class="modal fade" role="dialog">
            <div class="modal-dialog modal-md">
          
              <!-- Modal content-->
              <div class="modal-content">
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                  <h4 class="modal-title">Edit Users</h4>
                </div>
                <div class="modal-body">
                        <div class="card-content" id="color-button">
                                <div class="row">
                                    <form class="col s11 m-t-20">
                                        <div class="input-field form-input col s12">
                                            <i class="material-icons prefix">account_circle</i>
                                            <input id="EditId" type="tel" class="validate" placeholder="Admin name">
                                            <input id="Editname" type="tel" class="validate" placeholder="Admin name">
                                            {{--  <label for="icon_prefix">Name</label>  --}}
                                        </div>
                                        <div class="input-field form-input col s12">
                                            <i class="material-icons prefix">email</i>
                                            <input type="email" id="Editemail" class="validate" placeholder="Admin email">
                                            {{--  <label>Email</label>  --}}
                                        </div>
                                        <div class="input-field form-input form-input-select col s12">
                                            <i class="material-icons prefix">account_circle</i>
                                            <select class="icons" id="Editrole">
                                                <option selected>Choose Admin Role</option>
                                                <option value="1">Super Admin</option>
                                                <option value="2">Sub Admin</option>
                                            </select>
                                            {{--  <label>Select Role</label>  --}}
                                        </div>
                                        <div class="input-field form-input col s12">
                                            <i class="material-icons prefix">lock</i>
                                            <input type="password" id="Editpassword" class="validate" placeholder="Admin password" disabled>
                                            {{--  <label>Password</label>  --}}
                                        </div>
                                        <a href="javascript:void(0);" id="UpdateAdminUsers" class="waves-effect waves-light btn teal pull-right"><i class="material-icons left">refresh</i>Update</a>

                                    </form>
                                </div>

                            </div>
                </div>
                {{--  <div class="modal-footer">
                  <button type="button" id="AddAdminUsers" class="btn btn-success modal-save-btn-mar-right">Save</button>  --}}
                </div>
              </div>
          
            </div>
          </div>
@endsection


@section('JSScript')
<script>
    "use strict";
    $(function () {
        $('select').material_select();
        Materialize.updateTextFields();

        //datepicker
        $('.datepicker').pickadate({
            selectMonths: true, // Creates a dropdown to control month
            selectYears: 15 // Creates a dropdown of 15 years to control year
        });
    });
</script>
@endsection