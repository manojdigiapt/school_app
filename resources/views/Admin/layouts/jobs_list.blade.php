@extends('Admin.base')

@section('Content')
<div id="page-content-wrapper">
        <div class="page-content">
            <!-- Content Header (Page header) -->
            {{-- <section class="content-header z-depth-1">
                <div class="header-icon">
                    <i class="fa fa-table"></i>
                </div>
                <div class="header-title">
                    <h1> Admin Users List</h1>
                    <ul class="link hidden-xs">
                        <li><a href="/Admin/Dashboard"><i class="fa fa-home"></i>Home</a></li>
                        <li><a href="table.html">Table</a></li>
                    </ul>
                </div>
            </section> --}}
            <!-- page section -->
            <br>
            <div class="container-fluid">
                <div class="row">
                    <div class="fixed-action-btn">
                        <a href="/Admin/add_jobs" class="btn-floating btn-large red" title="Add new jobs">
                            <i class="large material-icons">add</i>
                        </a>
                    </div>
                    <!-- bootstrap table -->
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="card">
                            <div class="card-header">
                                <i class="fa fa-table fa-lg"></i>
                            <h2>{{$title}}</h2>
                            </div>
                            <div class="card-content">
                                <div class="table-responsive">
                                    <table class="table table-bordered table-hover">
                                        <thead>
                                            <tr>
                                                {{-- <th>Select</th> --}}
                                                <th>Company Name</th>
                                                <th>Job Title</th>
                                                <th>Email</th>
                                                <th>City</th>
                                                <th>Job type </th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($GetJobs as $Jobs)
                                            <tr>
                                                <td>{{$Jobs->company_name}}</td>
                                                <td>{{$Jobs->title}}</td>
                                                <td>{{$Jobs->email}}</td>
                                                <td>{{$Jobs->city}}</td>
                                                <td>
                                                    @if($Jobs->job_type == 1)
                                                        Full Time
                                                    @else
                                                        Part Time
                                                    @endif
                                                </td>
                                                <td>
                                                    <a href="javascript:void(0);" class="btn btn-sm" data-toggle="tooltip" data-placement="left" title="Update" onclick="EditAdminJobs({{$Jobs->id}});"><i class="fa fa-pencil" aria-hidden="true"></i></a>

                                                    <a href="javascript:void(0);" class="btn btn-danger btn-sm" data-toggle="tooltip" data-placement="right" title="Delete " onclick="DeleteAdminJobs({{$Jobs->id}});"><i class="fa fa-trash-o" aria-hidden="true"></i></a>
                                                </td>
                                            </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- ./bootstrap table -->
                    <!-- ./data table -->
                </div>
                <!-- ./row -->
            </div>
            <!-- ./cotainer -->

        </div>
        <!-- ./page-content -->
    </div>



@endsection


@section('JSScript')
<script>
    "use strict";
    $(function () {
        $('select').material_select();
        Materialize.updateTextFields();

        //datepicker
        $('.datepicker').pickadate({
            selectMonths: true, // Creates a dropdown to control month
            selectYears: 15 // Creates a dropdown of 15 years to control year
        });
    });
</script>
@endsection