<div id="sidebar-wrapper" class="waves-effect" data-simplebar>
        <div class="navbar-default sidebar" role="navigation">
            <div class="sidebar-nav navbar-collapse">
                <ul class="nav" id="side-menu">
                    <li class="active-link"><a href="index-2.html"><i class="material-icons">dashboard</i>Dashboard</a></li>
                    <li class="list-header">Admin Users ---</li>
                    <li>
                        <a><i class="material-icons">email</i>Admin users list<span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                            <li><a href="/Admin/Admin_lists">List of admins users</a></li>
                        </ul>
                    </li>
                    <li class="list-divider"></li>
                    <li class="list-header">Modules ---</li>
                    <li><a href="#"><i class="material-icons">format_color_fill</i> Jobs<span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                            <li><a href="/JobsList">Jobs list</a></li>
                            <li><a href="/Admin/add_jobs">Add new jobs</a></li>
                        </ul>
                    </li>
                    <li><a><i class="material-icons">layers_clear</i>Companies<span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                            <li><a href="maps_data.html">Companies list </a></li>
                            <li><a href="maps_jvector.html">Add new companies</a></li>
                        </ul>
                    </li>
                    <li>
                        <a><i class="material-icons">pie_chart</i>User profiles<span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                            <li><a href="charts_flot.html">Users list</a></li>
                            <li><a href="charts_sparkline.html">Add new users</a></li>
                        </ul>
                    </li>

                    <li>
                        <a><i class="material-icons">insert_emoticon</i>SEO<span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                            <li><a href="icons_bootstrap.html">SEO details</a></li>
                        </ul>
                    </li>
                    <li class="list-divider"></li>
                    <li class="list-header">Manage location ---</li>
                    <li>
                        <a><i class="fa fa-newspaper-o"></i>
                            Country<span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                            <li><a href="login.html">List countries</a></li>
                            <li><a href="register.html">Add new countries</a></li>
                        </ul>
                    </li>
                    <li>
                        <a><i class="fa fa-newspaper-o"></i>
                            State<span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                            <li><a href="login.html">List state</a></li>
                            <li><a href="register.html">Add new state</a></li>
                        </ul>
                    </li>
                    <li>
                        <a><i class="fa fa-newspaper-o"></i>
                            City<span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                            <li><a href="login.html">List cities</a></li>
                            <li><a href="register.html">Add new city</a></li>
                        </ul>
                    </li>
                    <li class="list-divider"></li>
                    <li class="list-header">Job Arrtibutes ---</li>
                    <li>
                        <a><i class="fa fa-newspaper-o"></i>
                            Job Title<span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                            <li><a href="/Admin/GetJobTitle">List of job titles</a></li>
                        </ul>
                    </li>
                    <li>
                        <a><i class="fa fa-newspaper-o"></i>
                            Job types<span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                            <li><a href="/Admin/GetJobType">List job types</a></li>
                        </ul>
                    </li>
                    <li>
                        <a><i class="fa fa-newspaper-o"></i>
                            Skill Names<span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                            <li><a href="/Admin/GetIndustries">List of skill names</a></li>
                        </ul>
                    </li>
                    <li>
                        <a><i class="fa fa-newspaper-o"></i>
                            Qualification<span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                            <li><a href="/Admin/GetQualification">List of qualification</a></li>
                        </ul>
                    </li>
                    <li>
                        <a><i class="fa fa-newspaper-o"></i>
                            Functional Areas<span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                            <li><a href="/Admin/GetFunctionalAreas">List of functional areas</a></li>
                        </ul>
                    </li>
                    <li>
                        <a><i class="fa fa-newspaper-o"></i>
                            Training completion<span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                            <li><a href="/Admin/GetTrainingCompletion">List of training completion</a></li>
                        </ul>
                    </li>
                    <li>
                        <a><i class="fa fa-newspaper-o"></i>
                            Career Level<span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                            <li><a href="/Admin/GetCareerLevel">List of career level</a></li>
                        </ul>
                    </li>
                    <li>
                        <a><i class="fa fa-newspaper-o"></i>
                            Offered salary<span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                            <li><a href="login.html">List of offered salary</a></li>
                            <li><a href="register.html">Add new offered salary</a></li>
                        </ul>
                    </li>
                    <li>
                        <a><i class="fa fa-newspaper-o"></i>
                            Training duration<span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                            <li><a href="/Admin/GetTrainingDuration">List of training duration</a></li>
                        </ul>
                    </li>
                    <li>
                        <a><i class="fa fa-newspaper-o"></i>
                            Team size<span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                            <li><a href="/Admin/GetTeamSize">List of team size</a></li>
                        </ul>
                    </li>
                    <li>
                        <a><i class="fa fa-newspaper-o"></i>
                            Marital Status<span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                            <li><a href="/Admin/GetMarital">List of marital status</a></li>
                        </ul>
                    </li>
                    <li>
                        <a><i class="fa fa-newspaper-o"></i>
                            Training Title<span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                            <li><a href="/Admin/GetTrainingTitle">List of training title</a></li>
                        </ul>
                    </li>
                    <li>
                        <a><i class="fa fa-newspaper-o"></i>
                            Languages<span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                            <li><a href="/Admin/GetLanguages">List of languages</a></li>
                        </ul>
                    </li>
                    
                    <li class="list-divider"></li>
                    <li class="list-header">Site settings ---</li>

                    <li class="active-link"><a href="index-2.html"><i class="material-icons">settings</i>Settings</a></li>

                    <li class="side-last"></li>
                </ul>
                <!-- ./sidebar-nav -->
            </div>
            <!-- ./sidebar -->
        </div>
        <!-- ./sidebar-nav -->
    </div>