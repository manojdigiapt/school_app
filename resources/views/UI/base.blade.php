<!DOCTYPE html>
<html>

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>{{ $title }}</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="description" content="">
	<meta name="keywords" content="">
	<meta name="author" content="CreativeLayers">
	<meta name="csrf-token" content="{{ csrf_token() }}">

	<!-- Styles -->
	<link rel="stylesheet" type="text/css" href="{{URL::asset('UI/css/bootstrap-grid.css')}}" />
	<link rel="stylesheet" href="{{URL::asset('UI/css/icons.css')}}">
	<link rel="stylesheet" href="{{URL::asset('UI/css/animate.min.css')}}">
	<link rel="stylesheet" type="text/css" href="{{URL::asset('UI/css/style.css')}}" />
	<link rel="stylesheet" type="text/css" href="{{URL::asset('UI/css/responsive.css')}}" />
	<link rel="stylesheet" type="text/css" href="{{URL::asset('UI/css/chosen.css')}}" />
	<link rel="stylesheet" type="text/css" href="{{URL::asset('UI/css/colors/colors.css')}}" />
	<link rel="stylesheet" type="text/css" href="{{URL::asset('UI/css/bootstrap.css')}}" />
{{--  <link rel="stylesheet" href="{{URL::asset('UI/css/font-awesome.css')}}" />  --}}

{{--  <link rel="stylesheet" type="text/css" href="{{URL::asset('UI/css/animate-notify.css')}}" />  --}}
	
	 {{-- DatePicker CSS  --}} 
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  {{-- <link rel="stylesheet" href="/resources/demos/style.css"> --}}

  {{-- Sweet alert CSS --}}
<link rel="stylesheet" href="https://lipis.github.io/bootstrap-sweetalert/dist/sweetalert.css">

{{-- Autocomplete CSS--}}
<link href="http://demo.expertphp.in/css/jquery.ui.autocomplete.css" rel="stylesheet">

			 
		<link href="https://www.jqueryscript.net/demo/Google-Address-Autocomplete-Plugin-geocomplete/assets/jquery.geocomplete.min.css" rel="stylesheet">
		

{{-- Slider Range --}}
{{-- <link rel="stylesheet" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.7.2/themes/ui-lightness/jquery-ui.css"> --}}

{{-- Input Tags  --}}
<link rel="stylesheet" href="https://bootstrap-tagsinput.github.io/bootstrap-tagsinput/dist/bootstrap-tagsinput.css">

{{-- CK Editor --}}


{{-- Font Awesome --}}
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">

</head>
<body>

{{-- <div class="page-loading">
	<img src="{{URL::asset('UI/images/loader.gif')}}" alt="" />
</div> --}}

<div class="theme-layout" id="scrollup">
	
	@include('UI.common.header')

    @yield('Content')
	
	@if(!(request()->is('Candidate/CandidateFirstRegister')) ? 'active' : '')
	@if(!(request()->is('Trainee/TraineeFirstRegister')) ? 'active' : '')
	@if(!(request()->is('Articleship/ArticleshipFirstRegister')) ? 'active' : '')

    @include('UI.common.footer')
	
	@endif
	@endif
	@endif
</div>

{{-- <div class="account-popup-area signin-popup-box">
	<div class="account-popup">
		<span class="close-popup"><i class="la la-close"></i></span>
		<h3>User Login</h3>
		<span>Click To Login With Demo User</span>
		<ul class="nav select-user" role="tablist">
				<li role="presentation" class="active"><a href="#candidatelogin" role="tab" class="btn btn-block blue-btn active-btn" data-toggle="tab">Candidate</a></li>
				<li role="presentation"><a href="#employerlogin" class="btn btn-block blue-btn" role="tab" data-toggle="tab">Employer</a></li>
			</ul>
	
			<div class="tab-content">
				<div role="tabpanel" class="tab-pane active account-popup signup-margin" id="candidatelogin">
					<form id="CandidateForm">
						<div class="cfield">
							<input type="text" id="CandidateUserName" placeholder="Cadidate / Trainee Username" />
							<input type="hidden" name="_token" value="{{ csrf_token() }}">
							<i class="la la-user"></i>
						</div>
						<div class="cfield">
							<input type="password" id="CandidateUserPassword" placeholder="********" />
							<i class="la la-key"></i>
						</div>
						<p class="remember-label">
							<input type="checkbox" name="cb" id="cb1"><label for="cb1">Remember me</label>
						</p>
						<a href="javascript:void(0);" id="ForgotCandidate" title="">Forgot Password?</a>
						<button type="button" id="LoginCandidate">Login</button>
					</form>

					<form id="CandidateEmailForm" style="display:none;">
							<div class="cfield">
								<input type="text" id="CandidateForgotEmail" placeholder="Cadidate email address" />
								<i class="la la-user"></i>
							</div>
							<a href="javascript:void(0);" id="BackToLoginCandidate" title="">Back to login</a>
							<button type="button" id="SubmitCandidateForgotEmail">Submit</button>
						</form>
				</div>
				<div role="tabpanel" class="tab-pane account-popup signup-margin" id="employerlogin">
					<form id="EmployerForm">
						<div class="cfield">
							<input type="text" id="EmployerUserName" placeholder="Employer Username" />
							<i class="la la-user"></i>
						</div>
						<div class="cfield">
							<input type="password" id="EmployerUserPassword" placeholder="********" />
							<i class="la la-key"></i>
						</div>
						<p class="remember-label">
							<input type="checkbox" name="cb" id="cb1"><label for="cb1">Remember me</label>
						</p>
						<a href="javascript:void(0);" id="ForgotEmployer" title="">Forgot Password?</a>
						<button type="button" id="LoginEmployer">Login</button>
					</form>

					<form id="EmployerEmailForm" style="display:none;">
							<div class="cfield">
								<input type="text" id="EmployerForgotEmail" placeholder="Cadidate email address" />
								<i class="la la-user"></i>
							</div>
							<a href="javascript:void(0);" id="BackToLoginEmployer" title="">Back to login</a>
							<button type="button" id="SubmitEmployerForgotEmail">Submit</button>
						</form>
				</div>
			</div>
	</div>
</div>

<div class="account-popup-area signup-popup-box">
	<div class="account-popup signup-popup-margin-top">
		<span class="close-popup"><i class="la la-close"></i></span>
		<h3>Sign Up</h3>
		<ul class="nav select-user" role="tablist">
			<li role="presentation" class="active"><a href="#work" role="tab" class="btn btn-block blue-btn active-btn" data-toggle="tab">Candidate</a></li>
			<li role="presentation"><a href="#hire" class="btn btn-block blue-btn" role="tab" data-toggle="tab">Employer</a></li>
		</ul>

		<div class="tab-content">
			<div role="tabpanel" class="tab-pane active account-popup signup-margin" id="work">
				<form>
					<div id="CandidateRegister"> 
						<div class="cfield">
							<input type="text" id="CandidateName" placeholder="Candidate Fullname" />
							<i class="la la-user"></i>
						</div>
						
						<div class="cfield">
							<input type="password" id="CandidatePassword" placeholder="Candidate Password" />
							<i class="la la-key"></i>
						</div>
						<div class="cfield">
							<input type="text" placeholder="Email" id="CandidateEmail" />
							<i class="la la-envelope-o"></i>
						</div>
						<div class="dropdown-field">
								<select data-placeholder="Please Select Candidate Type" class="chosen" id="CandidateType">
									<option selected disabled>Please Select Candidate Type</option>
									<option  value="1">Candidate</option>
									<option value="2">Trainee</option>
								</select>
							</div>
						<div class="cfield">
							<input type="text" placeholder="Phone Number" id="CandidatePhone" />
							<i class="la la-phone"></i>
						</div>
						<button type="button" id="SignupOtp">Signup</button>
					</div>

					<img id="OtpLoader" src="{{URL::asset('UI/otp_verify_loader.gif')}}" alt="" style="display:none;">

					<div id="CandidateOTP" style="display:none;"> 
						<h6 class="text-left">Mobile Otp</h6>
						<div class="cfield">
							<input type="text" placeholder="Mobile OTP" id="MobileOTP" />
						</div>

						<h6 class="text-left">Email Otp</h6>
						<div class="cfield">
							<input type="text" placeholder="Email OTP" id="EmailOTP" />
						</div>

						<button type="button" id="SignupCandidate">Submit</button>
					</div>
					
				</form>
			</div>
			<div role="tabpanel" class="tab-pane account-popup signup-margin" id="hire">
				<form>
					<div id="EmployerRegister"> 
						<div class="cfield">
							<input type="text" id="CompanyName" placeholder="Organization Name" />
							<i class="la la-user"></i>
						</div>
						<div class="cfield">
							<input type="password" id="EmployerPassword" placeholder="********" />
							<i class="la la-key"></i>
						</div>
						<div class="cfield">
							<input type="text" id="EmployerEmail" placeholder="Email" />
							<i class="la la-envelope-o"></i>
						</div>
						<div class="cfield">
							<input type="text" id="EmployerPhone" placeholder="Phone Number" />
							<i class="la la-phone"></i>
						</div>
						<button type="button" id="SignupEmployerOtp">Signup</button>
					</div>

					<img id="EmployerOtpLoader" src="{{URL::asset('UI/otp_verify_loader.gif')}}" alt="" style="display:none;">

					<div id="EmployerOTP" style="display:none;"> 
						<h6 class="text-left">Mobile Otp</h6>
						<div class="cfield">
							<input type="text" placeholder="Mobile OTP" id="EmployerMobileOTP" />
						</div>

						<h6 class="text-left">Email Otp</h6>
						<div class="cfield">
							<input type="text" placeholder="Email OTP" id="EmployerEmailOTP" />
						</div>

						<button type="button" id="SignupEmployer">Submit</button>
					</div>
				</form>
			</div>
		</div>
	</div>
</div> --}}

<div class="account-popup-area signin-candidate-popup-box">
	<div class="account-popup">
		<span class="close-popup CandidatePopUpClose" id="CandidatePopUpClose"><i class="la la-close"></i></span>
		<h3>Login</h3>
		<form id="login_candidate">
			<div class="cfield">
				<input type="text" placeholder="Username" id="CandidateUserName"/>
				<i class="la la-user"></i>
			</div>
			<div class="cfield">
				<input type="password" placeholder="Password" id="CandidateUserPassword"/>
				<i class="la la-key"></i>
			</div>
			<p class="remember-label">
				<input type="checkbox" name="cb" id="cb1"><label for="cb1">Remember me</label>
			</p>
			<a href="javascript:void(0);" title="" id="ForgotCandidate">Forgot Password?</a>
			<button type="button" id="LoginCandidate" class="Login-clr">Login</button>
		</form>

		<form id="CandidateEmailForm" style="display:none;">
			<div class="cfield">
				<input type="text" id="CandidateForgotEmail" placeholder="Cadidate email address" />
				<i class="la la-user"></i>
			</div>
			<img id="ForgotEmailCandidateLoader" class="forgot-email-loader" src="{{URL::asset('UI/otp_verify_loader.gif')}}" alt="" style="display:none;">
			<a href="javascript:void(0);" id="BackToLoginCandidate" title="">Back to login</a>
			<button type="button" id="SubmitCandidateForgotEmail" class="Login-clr">Submit</button>
		</form>

		<form id="register_candidate" style="display: none;">
			<div class="cfield">
				<input type="text" placeholder="Username" id="CandidateName">
				<i class="la la-user"></i>
			</div>
			<div class="cfield">
				<input type="password" placeholder="Password" id="CandidatePassword">
				<i class="la la-key"></i>
			</div>
			<div class="cfield">
				<input type="text" placeholder="Email" id="CandidateEmail">
				<i class="la la-envelope"></i>
			</div>
			<div class="cfield">
				<input type="text" placeholder="Phone" id="CandidatePhone">
				<i class="la la-mobile"></i>
			</div>
			<div class="dropdown-field">
				<select data-placeholder="Please Select Candidate Type" class="chosen" id="CandidateType">
					<option value="sel" selected >Please Select Candidate Type</option>
					<option  value="1">Experienced</option>
					<option value="2">Fresher</option>
					<option value="3">CA Articleship</option>
				</select>
			</div>
			{{-- <p class="remember-label">
				<input type="checkbox" name="cb" id="cb1"><label for="cb1">Remember me</label>
			</p>
			<a href="#" title="">Forgot Password?</a> --}}
			
			<button type="button" id="SignupOtp" class="register-clr register-candidate-form">Register</button>
		</form>

		<img id="CandidateOtpLoader" class="signup-otp-width" src="{{URL::asset('UI/otp_verify_loader.gif')}}" alt="" style="display:none;">

		<form id="CandidateOTP" style="display:none;"> 
			<h6 class="text-left">Mobile Otp : </h6>
			<div class="cfield">
				<input type="text" placeholder="Mobile OTP" id="MobileOTP" />
			</div>
			<a href="javascript:void(0);" id="ResendCandidateOTP" class="resend-link-mar-top text-right">Resend link</a>

			<img id="CandidateResendOtpLoader" class="resend_link" src="{{URL::asset('UI/otp_verify_loader.gif')}}" alt="" style="display:none;">

			{{-- <h6 class="text-left">Email Otp</h6>
			<div class="cfield">
				<input type="text" placeholder="Email OTP" id="EmailOTP" />
			</div>
			<a href="javascript:void(0);" id="ResendCandidateEmailOTP" class="resend-link-mar-top text-right">Resend link</a>

			<img id="CandidateResendEmailOtpLoader" class="resend_link" src="{{URL::asset('UI/otp_verify_loader.gif')}}" alt="" style="display:none;"> --}}

			<button type="button" id="SignupCandidate" class="register-clr register-candidate-form">Submit</button>

			<button type="button" class="CandidatePopUpClose register-btn">Cancel</button>
		</form>

		<div class="extra-login">
			<span>Don't have a account yet?</span>
			<div class="account-popup register-btn-top mobile-register-btn">
				<form class="form-register-btn-martop">
					<button type="button" id="registerCandidate" class="register-btn registerCandidate">Register Now</button>

					<button type="button" id="loginCandidate" class="register-btn" style="display:none;">Login</button>
				</form>
			</div>
		</div>
	</div>
</div><!-- LOGIN POPUP -->

<div class="account-popup-area signup-employer-popup-box">
	<div class="account-popup">
		<span class="close-popup" id="register_close"><i class="la la-close"></i></span>
		<h3>For CA Firm</h3>
		<form id="login_employer">
			<div class="cfield">
				<input type="text" id="EmployerUserName" placeholder="Username" />
				<i class="la la-user"></i>
			</div>
			<div class="cfield">
				<input type="password" id="EmployerUserPassword" placeholder="Password" />
				<i class="la la-key"></i>
			</div>
			<p class="remember-label">
				<input type="checkbox" name="cb" id="remember"><label for="remember">Remember me</label>
			</p>
			<a href="javascript:void(0);" id="ForgotEmployer" title="">Forgot Password?</a>
			<button type="button" id="LoginEmployer" class="register_btn_clr">Login</button>
		</form>

		<form id="EmployerEmailForm" style="display:none;">
				<div class="cfield">
					<input type="text" id="EmployerForgotEmail" placeholder="Employer email address" />
					<i class="la la-user"></i>
				</div>
				<img id="ForgotEmailEmployerLoader" class="forgot-email-loader" src="{{URL::asset('UI/otp_verify_loader.gif')}}" alt="" style="display:none;">
				<a href="javascript:void(0);" id="BackToLoginEmployer" title="">Back to login</a>
				<button type="button" id="SubmitEmployerForgotPassword" class="register_btn_clr">Submit</button>
			</form>

		<form id="register_employer" style="display: none;">
			<div class="cfield">
				<input type="text" placeholder="Organization Name" id="CompanyName" />
				<i class="la la-user"></i>
			</div>
			<div class="cfield">
				<input type="text" placeholder="Email" id="EmployerEmail" />
				<i class="la la-envelope"></i>
			</div>
			<div class="cfield">
				<input type="text" maxlength="10" placeholder="Phone number" id="EmployerPhone" />
				<i class="la la-phone"></i>
			</div>
			<div class="cfield">
				<input type="password" placeholder="Password" id="EmployerPassword" />
				<i class="la la-key"></i>
			</div>
			{{-- <p class="remember-label">
				<input type="checkbox" name="cb" id="cb1"><label for="cb1">Remember me</label>
			</p> --}}
			{{-- <a href="#" title="">Forgot Password?</a> --}}
			<button type="button" id="SignupEmployerOtp" class="register_form">Register Now</button>
		</form>
		<img id="EmployerOtpLoader" class="signup-otp-width" src="{{URL::asset('UI/otp_verify_loader.gif')}}" alt="" style="display:none;">

		<form id="EmployerOTP" style="display:none;"> 
			<h6 class="text-left">Mobile Otp :</h6>
			<div class="cfield">
				<input type="text" placeholder="Mobile OTP" id="EmployerMobileOTP" />
			</div>
			<a href="javascript:void(0);" id="ResendOTP" class="resend-link-mar-top text-right">Resend link</a>

			<img id="EmployerResendOtpLoader" class="resend_link" src="{{URL::asset('UI/otp_verify_loader.gif')}}" alt="" style="display:none;">

			<h6 class="text-left">Email Otp</h6>
			<div class="cfield">
				<input type="text" placeholder="Email OTP" id="EmployerEmailOTP" />
			</div>
			<p class="countdown-left">Time left: <span class="countdown"></span></p>

			<a href="javascript:void(0);" id="ResendEmailOTP" class="resend-link-mar-top text-right">Resend link</a>

			<img id="EmployerResendEmailOtpLoader" class="resend_link" src="{{URL::asset('UI/otp_verify_loader.gif')}}" alt="" style="display:none;">

			<button type="button" id="SignupEmployer" class="register_form">Submit</button>

			<button type="button" id="CancelEmployer" class="register_form">Cancel</button>
		</form>

		<div class="extra-login">
			<span>Don't have an account yet?</span>
			<div class="account-popup register-btn-top mobile-register-btn">
				<form class="form-register-btn-martop">
					<button id="register_form" class="register_form" type="button" class="register-btn">Register Now</button>

					<button id="login_form" type="button" class="register_btn_clr" style="display: none;">Login</button>
				</form>
			</div>
		</div>
	</div>
</div><!-- SIGNUP POPUP -->




@if(Auth::guard('candidate')->check())
{{-- Candidate Profile Sidebar --}}
<div class="profile-sidebar">
	<span class="close-profile"><i class="la la-close"></i></span>
	<div class="can-detail-s">
		<div class="cst">
			@if($CandidateProfile->profile_pic)
				<img src="{{URL::asset('candidate_profile')}}/{{$CandidateProfile->profile_pic}}" alt="" />
			@else
				<img src="{{URL::asset('UI/images/resource/es1.jpg')}}" alt="" />
			@endif
		</div>
		<h3>{{$CandidateProfile->name}}</h3>
		<span>@if($GetProfessionalDetails)
				<i>{{$GetProfessionalDetails->JobTitle}}</i> at {{$GetProfessionalDetails->name}}
		@endif</span>
		{{-- @if(isset($Profile)) --}}
		<p><a href="mailto:{{$Profile->email}}">{{$Profile->email}}</a></p>
		{{-- @endif --}}
		{{-- <p>Member Since, 2017</p> --}}
		<p>
			@if($CandidateProfile->desired_location)
			<i class="la la-map-marker"></i>{{$CandidateProfile->desired_location}}
			@endif
		</p>
	</div>
	<div class="tree_widget-sec">
		<ul>
			<li><a href="/Candidate/Dashboard" title=""><i class="la la-file-text"></i>Dashboard</a></li>

			<li><a href="/Candidate/Profile" title=""><i class="la la-file-text"></i>My Profile</a></li>
			{{-- <li><a href="/Candidate/CandidateFavouriteJobs" title=""><i class="la la-heart"></i>Saved Jobs</a></li> --}}
			{{-- <li><a href="candidates_job_alert.html" title=""><i class="la la-user"></i>Job Alerts</a></li> --}}
			<li><a href="/CandidateChangePassword" title=""><i class="la la-flash"></i>Change Password</a></li>
			<li><a href="/Candidate/logout" title=""><i class="la la-unlink"></i>Logout</a></li>
		</ul>
	</div>
</div><!-- Profile Sidebar -->
@elseif(Auth::guard('employer')->check())
{{-- Employer Profile Sidebar --}}
<div class="profile-sidebar">
		<span class="close-profile"><i class="la la-close"></i></span>
		<div class="can-detail-s">
			<div class="cst">
				@if($EmployerProfile->profile_pic)
					<img src="{{URL::asset('employer_profile')}}/{{$EmployerProfile->profile_pic}}" alt="" />
				@else
					<img src="{{URL::asset('UI/images/resource/es1.jpg')}}" alt="" />
				@endif
			</div>
			<span>{{$EmployerProfile->company_name}}</span>
			<p><a href="mailto:{{$EmployerProfile->email}}">{{$EmployerProfile->email}}</a></p>
		<p>Member Since, {{date('Y', strtotime($EmployerProfile->created_at))}}</p>
			<p><i class="la la-map-marker"></i>{{$EmployerProfile->city}}</p>
		</div>
		<div class="tree_widget-sec">
			<ul>
				<li><a href="/Employer/Dashboard" title=""><i class="la la-file-text"></i>Dashboard</a></li>
				<li><a href="/Employer/Profile" title=""><i class="la la-briefcase"></i>My Profile</a></li>
				<li><a href="/EmployerChangePassword" title=""><i class="la la-flash"></i>Change Password</a></li>
				<li><a href="/Employer/logout" title=""><i class="la la-unlink"></i>Logout</a></li>
			</ul>
		</div>
	</div><!-- Profile Sidebar -->
@elseif(Auth::guard('trainee')->check())
{{-- Candidate Profile Sidebar --}}
<div class="profile-sidebar">
	<span class="close-profile"><i class="la la-close"></i></span>
	<div class="can-detail-s">
		<div class="cst">
			@if($TraineeProfile->profile_pic)
				<img src="{{URL::asset('trainee_profile')}}/{{$TraineeProfile->profile_pic}}" alt="" />
			@else
				<img src="{{URL::asset('UI/images/resource/es1.jpg')}}" alt="" />
			@endif
		</div>
		<h3>{{$TraineeProfile->name}}</h3>
		{{-- <span><i>UX / UI Designer</i> at Atract Solutions</span> --}}
		<p><a href="mailto:{{$Profile->email}}">{{$Profile->email}}</a></p>
		{{-- <p>Member Since, 2017</p> --}}
	<p><i class="la la-map-marker"></i>{{$TraineeProfile->desired_location}}</p>
	</div>
	<div class="tree_widget-sec">
		<ul>
			<li><a href="/Trainee/Dashboard" title=""><i class="la la-file-text"></i>Dashboard</a></li>
			<li><a href="/Trainee/Profile" title=""><i class="la la-briefcase"></i>My Profile</a></li>
			<li><a href="/Trainee/ChangePassword" title=""><i class="la la-flash"></i>Change Password</a></li>
			<li><a href="/Trainee/logout" title=""><i class="la la-unlink"></i>Logout</a></li>
		</ul>
	</div>
</div><!-- Profile Sidebar -->

@elseif(Auth::guard('ca_article')->check())
{{-- Candidate Profile Sidebar --}}
<div class="profile-sidebar">
	<span class="close-profile"><i class="la la-close"></i></span>
	<div class="can-detail-s">
		<div class="cst">
			@if($ArticleProfile->profile_pic)
				<img src="{{URL::asset('trainee_profile')}}/{{$ArticleProfile->profile_pic}}" alt="" />
			@else
				<img src="{{URL::asset('UI/images/resource/es1.jpg')}}" alt="" />
			@endif
		</div>
		<h3>{{$ArticleProfile->name}}</h3>
		{{-- <span><i>UX / UI Designer</i> at Atract Solutions</span> --}}
		<p><a href="mailto:{{$Profile->email}}">{{$Profile->email}}</a></p>
		{{-- <p>Member Since, 2017</p> --}}
	<p><i class="la la-map-marker"></i>{{$ArticleProfile->desired_location}}</p>
	</div>
	<div class="tree_widget-sec">
		<ul>
			<li><a href="/Article/Dashboard" title=""><i class="la la-file-text"></i>Dashboard</a></li>
			<li><a href="/ArticleProfile" title=""><i class="la la-briefcase"></i>My Profile</a></li>
			<li><a href="/Trainee/ChangePassword" title=""><i class="la la-flash"></i>Change Password</a></li>
			<li><a href="/Article/logout" title=""><i class="la la-unlink"></i>Logout</a></li>
		</ul>
	</div>
</div><!-- Profile Sidebar -->
@endif



<script src="{{URL::asset('UI/js/jquery.min.js')}}" type="text/javascript"></script>
<script src="{{URL::asset('UI/js/modernizr.js')}}" type="text/javascript"></script>
<script src="{{URL::asset('UI/js/script.js')}}" type="text/javascript"></script>
<script src="{{URL::asset('UI/js/bootstrap.min.js')}}" type="text/javascript"></script>
<script src="{{URL::asset('UI/js/wow.min.js')}}" type="text/javascript"></script>
<script src="{{URL::asset('UI/js/slick.min.js')}}" type="text/javascript"></script>
<script src="{{URL::asset('UI/js/parallax.js')}}" type="text/javascript"></script>
<script src="{{URL::asset('UI/js/select-chosen.js')}}" type="text/javascript"></script>
<script src="{{URL::asset('UI/js/counter.js')}}" type="text/javascript"></script>
<script src="{{URL::asset('UI/js/bootstrap-notify.min.js')}}" type="text/javascript"></script>

<script src="https://www.cssscript.com/demo/super-simple-javascript-message-toaster-toast-js/toast.js"></script>

{{-- PDF Plugin --}}
 

{{-- Sweet alert JS --}}
<script src="https://lipis.github.io/bootstrap-sweetalert/dist/sweetalert.js"></script>


{{-- Custom JS --}}
<script src="{{URL::asset('UI/js/custom/candidate.js')}}"></script>
<script src="{{URL::asset('UI/js/custom/employer.js')}}"></script>
<script src="{{URL::asset('UI/js/custom/candidates_profile.js')}}"></script>
<script src="{{URL::asset('UI/js/custom/candidate_resume.js')}}"></script>

<script src="{{URL::asset('UI/js/custom/employer_profile.js')}}"></script>

<script src="{{URL::asset('UI/js/custom/autocomplete_search.js')}}"></script>

<script src="{{URL::asset('UI/js/custom/trainee_profile.js')}}"></script>
<script src="{{URL::asset('UI/js/custom/filters.js')}}"></script>
<script src="{{URL::asset('UI/js/custom/home.js')}}" type="text/javascript"></script>
<script src="{{URL::asset('UI/js/custom/ca_jobs.js')}}" type="text/javascript"></script>



<script type="text/javascript"
	src="https://maps.googleapis.com/maps/api/js?libraries=places&key=AIzaSyDZZVHgTPdhZzlW774_lMrZGusJcTIi1-w"></script>
	

<script src="https://www.jqueryscript.net/demo/Google-Address-Autocomplete-Plugin-geocomplete/assets/jquery.geocomplete.min.js"></script>


<script src="https://bootstrap-tagsinput.github.io/bootstrap-tagsinput/dist/bootstrap-tagsinput.min.js"></script>


{{-- Autocomplete JS--}}

{{--  <script src = "https://code.jquery.com/jquery-1.10.2.js"></script>  --}}
{{--  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>  --}}

{{--  <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>  --}}
{{--  <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-3-typeahead/4.0.1/bootstrap3-typeahead.min.js"></script>  --}}

<script src="http://demo.expertphp.in/js/jquery-ui.min.js"></script>

{{-- CK Editor --}}
<script src="https://cdn.ckeditor.com/4.12.1/basic/ckeditor.js"></script>

{{--  <script>
$("#Edit_address").geocomplete({
	formId: "#GetAddress"
});
</script>  --}}

<script>

// $("#address").geocomplete({
//           formId: "#GetAddress"
// 		});

// $("#city").geocomplete({
// 	formId: "#GetAddress"
// });


// $("#Edit_address").geocomplete({
// 	formId: "#GetProfileAddress"
// });

// $("#Ca_Edit_address").geocomplete({
// 	formId: "#GetCAProfileAddress"
// });

// $("#Emp_address").geocomplete({
// 	formId: "#GetEmpAddress"
// });

		

// CKEDITOR.replace( 'description', {
// 				height: 250
// 			} );

// CKEDITOR.replace( 'training_description', {
// 	height: 250
// } );

// CKEDITOR.replace( 'Edit_description', {
// 	height: 250
// } );
</script>




@yield('JSScript')

<script>
		var timer2 = "5:01";
var interval = setInterval(function() {


  var timer = timer2.split(':');
  //by parsing integer, I avoid all extra string processing
  var minutes = parseInt(timer[0], 10);
  var seconds = parseInt(timer[1], 10);
  --seconds;
  minutes = (seconds < 0) ? --minutes : minutes;
  if (minutes < 0) clearInterval(interval);
  seconds = (seconds < 0) ? 59 : seconds;
  seconds = (seconds < 10) ? '0' + seconds : seconds;
  //minutes = (minutes < 10) ?  minutes : minutes;
  $('.countdown').html(minutes + ':' + seconds);
  timer2 = minutes + ':' + seconds;
}, 1000);
		</script>


<!--Start of Tawk.to Script-->
<script type="text/javascript">
	var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
	(function(){
	var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
	s1.async=true;
	s1.src='https://embed.tawk.to/5d836976c22bdd393bb6ad2e/default';
	s1.charset='UTF-8';
	s1.setAttribute('crossorigin','*');
	s0.parentNode.insertBefore(s1,s0);
	})();
	</script>
	<!--End of Tawk.to Script-->
</body>

</html>

