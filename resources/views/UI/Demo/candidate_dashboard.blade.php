<!DOCTYPE html>
<html>

<!-- Mirrored from grandetest.com/theme/jobhunt-html/index3.html by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 03 Jul 2019 11:09:15 GMT -->
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>Candidate Dashboard</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="description" content="">
	<meta name="keywords" content="">
	<meta name="author" content="CreativeLayers">

	<!-- Styles -->
	<link rel="stylesheet" type="text/css" href="{{URL::asset('Demo/css/bootstrap-grid.css')}}" />
	<link rel="stylesheet" href="{{URL::asset('Demo/css/icons.css')}}">
	<link rel="stylesheet" href="{{URL::asset('Demo/css/animate.min.css')}}">
	<link rel="stylesheet" type="text/css" href="{{URL::asset('Demo/css/style.css')}}" />
	<link rel="stylesheet" type="text/css" href="{{URL::asset('Demo/css/responsive.css')}}" />
	<link rel="stylesheet" type="text/css" href="{{URL::asset('Demo/css/chosen.css')}}" />
	<link rel="stylesheet" type="text/css" href="{{URL::asset('Demo/css/colors/colors.css')}}" />
	<link rel="stylesheet" type="text/css" href="{{URL::asset('Demo/css/bootstrap.css')}}" />
<link rel="stylesheet" href="../../../maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css" />
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" />
	
</head>
<body class="newbg">

<div class="page-loading">
	<img src="images/loader.gif" alt="" />
</div>

<div class="theme-layout" id="scrollup">
	
	<div class="responsive-header">
		<div class="responsive-menubar">
			<div class="res-logo"><a href="index.html" title=""><img src="{{URL::asset('Demo/images/resource/logo.png')}}" alt="" /></a></div>
			<div class="menu-resaction">
				<div class="res-openmenu">
					<img src="{{URL::asset('Demo/images/icon.png')}}" alt="" /> Menu
				</div>
				<div class="res-closemenu">
					<img src="{{URL::asset('Demo/images/icon2.png')}}" alt="" /> Close
				</div>
			</div>
		</div>
		<div class="responsive-opensec">
			<div class="btn-extars">
				<a href="#" title="" class="post-job-btn"><i class="la la-plus"></i>Post Jobs</a>
				<ul class="account-btns">
					<li class="signup-popup"><a title=""><i class="la la-key"></i> Sign Up</a></li>
					<li class="signin-popup"><a title=""><i class="la la-external-link-square"></i> Login</a></li>
				</ul>
			</div><!-- Btn Extras -->
			<form class="res-search">
				<input type="text" placeholder="Job title, keywords or company name" />
				<button type="submit"><i class="la la-search"></i></button>
			</form>
			<div class="responsivemenu">
				<ul>
						<li class="menu-item-has-children">
							<a href="#" title="">Home</a>
							<ul>
								<li><a href="index.html" title="">Home Layout 1</a></li>
								<li><a href="index2.html" title="">Home Layout 2</a></li>
								<li><a href="index3.html" title="">Home Layout 3</a></li>
								<li><a href="index4.html" title="">Home Layout 4</a></li>
								<li><a href="index5.html" title="">Home Layout 5</a></li>
								<li><a href="index6.html" title="">Home Layout 6</a></li>
								<li><a href="index7.html" title="">Home Layout 7</a></li>
								<li><a href="index8.html" title="">Home Layout 8</a></li>
							</ul>
						</li>
						<li class="menu-item-has-children">
							<a href="#" title="">Employers</a>
							<ul>
								<li><a href="employer_list1.html" title=""> Employer List 1</a></li>
								<li><a href="employer_list2.html" title="">Employer List 2</a></li>
								<li><a href="employer_list3.html" title="">Employer List 3</a></li>
								<li><a href="employer_list4.html" title="">Employer List 4</a></li>
								<li><a href="employer_single1.html" title="">Employer Single 1</a></li>
								<li><a href="employer_single2.html" title="">Employer Single 2</a></li>
								<li class="menu-item-has-children">
									<a href="#" title="">Employer Dashboard</a>
									<ul>
										<li><a href="employer_manage_jobs.html" title="">Employer Job Manager</a></li>
										<li><a href="employer_packages.html" title="">Employer Packages</a></li>
										<li><a href="employer_post_new.html" title="">Employer Post New</a></li>
										<li><a href="employer_profile.html" title="">Employer Profile</a></li>
										<li><a href="employer_resume.html" title="">Employer Resume</a></li>
										<li><a href="employer_transactions.html" title="">Employer Transaction</a></li>
										<li><a href="employer_job_alert.html" title="">Employer Job Alert</a></li>
										<li><a href="employer_change_password.html" title="">Employer Change Password</a></li>
									</ul>
								</li>
							</ul>
						</li>
						<li class="menu-item-has-children">
							<a href="#" title="">Candidates</a>
							<ul>
								<li><a href="candidates_list.html" title="">Candidates List 1</a></li>
								<li><a href="candidates_list2.html" title="">Candidates List 2</a></li>
								<li><a href="candidates_list3.html" title="">Candidates List 3</a></li>
								<li><a href="candidates_single.html" title="">Candidates Single 1</a></li>
								<li><a href="candidates_single2.html" title="">Candidates Single 2</a></li>
								<li class="menu-item-has-children">
									<a href="#" title="">Candidates Dashboard</a>
									<ul>
										<li><a href="candidates_my_resume.html" title="">Candidates Resume</a></li>
										<li><a href="candidates_my_resume_add_new.html" title="">Candidates Resume new</a></li>
										<li><a href="candidates_profile.html" title="">Candidates Profile</a></li>
										<li><a href="candidates_shortlist.html" title="">Candidates Shortlist</a></li>
										<li><a href="candidates_job_alert.html" title="">Candidates Job Alert</a></li>
										<li><a href="candidates_dashboard.html" title="">Candidates Dashboard</a></li>
										<li><a href="candidates_cv_cover_letter.html" title="">CV Cover Letter</a></li>
										<li><a href="candidates_change_password.html" title="">Change Password</a></li>
										<li><a href="candidates_applied_jobs.html" title="">Candidates Applied Jobs</a></li>
									</ul>
								</li>
							</ul>
						</li>
						<li class="menu-item-has-children">
							<a href="#" title="">Blog</a>
							<ul>
								<li><a href="blog_list.html"> Blog List 1</a></li>
								<li><a href="blog_list2.html">Blog List 2</a></li>
								<li><a href="blog_list3.html">Blog List 3</a></li>
								<li><a href="blog_single.html">Blog Single</a></li>
							</ul>
						</li>
						<li class="menu-item-has-children">
							<a href="#" title="">Job</a>
							<ul>
								<li><a href="job_list_classic.html">Job List Classic</a></li>
								<li><a href="job_list_grid.html">Job List Grid</a></li>
								<li><a href="job_list_modern.html">Job List Modern</a></li>
								<li><a href="job_single1.html">Job Single 1</a></li>
								<li><a href="job_single2.html">Job Single 2</a></li>
								<li><a href="job-single3.html">Job Single 3</a></li>
							</ul>
						</li>
						<li class="menu-item-has-children">
							<a href="#" title="">Pages</a>
							<ul>
								<li><a href="about.html" title="">About Us</a></li>
								<li><a href="404.html" title="">404 Error</a></li>
								<li><a href="contact.html" title="">Contact Us 1</a></li>
								<li><a href="contact2.html" title="">Contact Us 2</a></li>
								<li><a href="faq.html" title="">FAQ's</a></li>
								<li><a href="how_it_works.html" title="">How it works</a></li>
								<li><a href="login.html" title="">Login</a></li>
								<li><a href="pricing.html" title="">Pricing Plans</a></li>
								<li><a href="register.html" title="">Register</a></li>
								<li><a href="terms_and_condition.html" title="">Terms & Condition</a></li>
							</ul>
						</li>
					</ul>
			</div>
		</div>
	</div>
	
	<header class="stick-top forsticky new-header">
		<div class="menu-sec">
			<div class="container">
				<div class="logo">
					<a href="index.html" title=""><img class="hidesticky" src="{{URL::asset('Demo/images/resource/logo.png')}}" alt="" /><img class="showsticky" src="{{URL::asset('Demo/images/resource/logo10.png')}}" alt="" /></a>
                </div><!-- Logo -->
                <div class="my-profiles-sec">
                        <span><img src="{{URL::asset('Demo/images/resource/mp1.jpg')}}" alt="" /> <i class="la la-bars"></i></span>
                    </div>
                    <div class="wishlist-dropsec notification-mar-right">
                        <span><i class="fa fa-bell-o"></i><strong class="notification-bg-clr">3</strong></span>
                    </div>
                    <div class="wishlist-dropsec">
                        <span><i class="fa fa-heart-o"></i><strong>3</strong></span>
                    </div>
				<nav class="candidates-dashboard-nav-center">
					<ul>
						<li class="active">
							<a href="#" title="">Jobs</a>
                        </li>
                        <li class="">
                            <a href="#" title="">Applied Jobs</a>
                        </li>
					</ul>
				</nav><!-- Menus -->
			</div>
		</div>
	</header>

	<section>
		<div class="block no-padding">
			<div class="container fluid">
				<div class="row">
					<div class="col-lg-12">
						<div class="main-featured-sec style2 mobile-height-sec">
							<ul class="main-slider-sec style2 text-arrows home-img candidate-dashboard-banner-height mobile-height-slider">
								<li class="slideHome"><img src="{{URL::asset('Demo/images/resource/mslider3.jpg')}}" alt="" /></li>
							</ul>
							
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

	<section id="scroll-here">
		<div class="block">
			<div class="container">
				<div class="row candidate-row-mar-top">
					<div class="col-lg-4 col-md-6">
						<div class="reviews-sec candiates-card-profile-top">
							<div class="col-lg-12">
                                <div class="reviews candidates-profile-card-border">
                                    <div class="col-md-3 pull-left">
                                        
                                        <div class="progress blue">
                                            <span class="progress-left">
                                                <span class="progress-bar"></span>
                                            </span>
                                            <span class="progress-right">
                                                <span class="progress-bar"></span>
                                            </span>
                                            <div class="progress-value"><img class="candidate-profile-img" src="{{URL::asset('Demo/images/resource/r1.jpg')}}" alt="" /></div>
                                            <h5 class="profile-completion-percentage">90%</h5>
                                            <p class="profile-completion-p">Profile Completion</p>
                                        </div>
                                    </div>

                                    <div class="col-md-9 pull-left mobile-profile-center">
                                        <h3 class="mobile-review-head">Augusta Silva </h3>
                                        <span class="candidate-profile-position">Web designer</span>
                                        <br>
                                        <span class="candidate-profile-position">Artfact Solutions</span>  
                                        <br>   
                                        <a href="" class="profile-complete-link">Complete Profile Now</a>
                                    </div>
                                </div>
							</div>
                        </div>
                        
                        <div class="widget border filter-card-candidate-profile">
                                <h3 class="sb-title open">Filters</h3>
                                <br>
                                <h4 class="sb-title open">Date Posted</h4>
                                <div class="posted_widget">
                                   <input type="radio" name="choose" id="232"><label for="232">Last Hour</label><br />
                                   <input type="radio" name="choose" id="wwqe"><label for="wwqe">Last 24 hours</label><br />
                                   <input type="radio" name="choose" id="erewr"><label for="erewr">Last 7 days</label><br />
                                </div>
                                <hr class="filter-border-bottom-line">
                                <h3 class="sb-title open">Job Type</h3>
                                <div class="type_widget">
                                    <p><input type="checkbox" name="spealism" id="ft"><label for="ft">Full Time</label></p>
                                    <p><input type="checkbox" name="spealism" id="pt"><label for="pt">Part Time</label></p>
                                    <p><input type="checkbox" name="spealism" id="fl"><label for="fl">Freelance</label></p>
								</div>
								<hr class="filter-border-bottom-line">
                                <h3 class="sb-title open">Industry</h3>
                                <div class="type_widget">
                                    <p><input type="checkbox" name="spealism" id="ft"><label for="ft">Software Design</label></p>
                                    <p><input type="checkbox" name="spealism" id="pt"><label for="pt">Banking</label></p>
                                    <p><input type="checkbox" name="spealism" id="fl"><label for="fl">Charity & Voluntary</label></p>
                                </div>
                            </div>
					</div>
					<div class="col-lg-8 col-md-6">
						<div class="job-search-sec candidate-job-search-sec-width">
                            <div class="job-search style2">
                                <div class="search-job2 caniddate-dashboard-job-search">	
                                    <form class="home-search-box">
                                        <div class="row no-gape tab-view-search-top">
                                            <div class="col-lg-6 col-md-6 col-sm-4 col-xs-6">
                                                <div class="job-field">
                                                    <input type="text" class="candidate-search-input-border-radius" placeholder="Skill, Designation" />
                                                </div>
                                            </div>
                                            <div class="col-lg-3 col-md-3 col-sm-4">
                                                <div class="job-field">
                                                    <select data-placeholder="Any category" class="chosen-city">
                                                        <option selected disabled>Location</option>
                                                        <option>Bangalore</option>
                                                        <option>Mumbai</option>
                                                        <option>Delhi</option>
                                                        <option>Pune</option>
                                                        <option>Hyderabad</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-lg-3  col-md-3 col-sm-4">
                                                <button type="submit" class="home-search-btn"><img src="images/custom/search.png" alt=""> SEARCH</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <div class="filterbar candidate-profile-filter-pad">
                            <div class="sortby-sec candidate-profile-sortby">
                                <select data-placeholder="20 Per Page" class="chosen">
                                    <option>Sort by</option>
                                    <option>40 Per Page</option>
                                    <option>50 Per Page</option>
                                    <option>60 Per Page</option>
                                </select>
                            </div>
                            <h5>50 recommended jobs</h5>
                        </div>

                        <div class="job-list-modern">
                                <div class="job-listings-sec no-border">
                                   <div class="job-listing wtabs recommented-jobs">
                                       <div class="job-title-sec recommended-jobs-pad-left">
                                           <h3><a href="#" title="">Web Designer / Developer</a></h3>
                                           <p class="recommended-jobs-company-name">Massimo Artemisis</p>
                                           <div class="job-lctn job-attributes-recommended-jobs"><i class="fa fa-briefcase"></i>4-6 Years</div>
                                           <div class="job-lctn job-attributes-recommended-jobs"><i class="fa fa-user-circle"></i>Full Time</div>
                                           <div class="job-lctn job-attributes-recommended-jobs"><i class="fa fa-map-marker" ></i> Bangalore</div>
                                           <div class="job-lctn job-attributes-recommended-jobs"><i class="fa fa-industry"></i>Industry</div>
                                        </div>
                                        <p class="recommended-jobs-description">user interface ux designer,mobile apps designer,Mobile application designer,ui ux designer,Senior Ui Ux Designer</p>
                                        <p class="recommended-jobs-posted-on">Posted on: Today</p>
                                   </div>
                                   <div class="job-listing wtabs recommented-jobs">
                                        <div class="job-title-sec recommended-jobs-pad-left">
                                            <h3><a href="#" title="">Web Designer / Developer</a></h3>
                                            <p class="recommended-jobs-company-name">Massimo Artemisis</p>
                                            <div class="job-lctn job-attributes-recommended-jobs"><i class="fa fa-briefcase"></i>4-6 Years</div>
                                            <div class="job-lctn job-attributes-recommended-jobs"><i class="fa fa-user-circle"></i>Full Time</div>
                                            <div class="job-lctn job-attributes-recommended-jobs"><i class="fa fa-map-marker" ></i> Bangalore</div>
                                            <div class="job-lctn job-attributes-recommended-jobs"><i class="fa fa-industry"></i>Industry</div>
                                         </div>
                                         <p class="recommended-jobs-description">user interface ux designer,mobile apps designer,Mobile application designer,ui ux designer,Senior Ui Ux Designer</p>
                                         <p class="recommended-jobs-posted-on">Posted on: Today</p>
									</div>
									<div class="job-listing wtabs recommented-jobs">
                                        <div class="job-title-sec recommended-jobs-pad-left">
                                            <h3><a href="#" title="">Web Designer / Developer</a></h3>
                                            <p class="recommended-jobs-company-name">Massimo Artemisis</p>
                                            <div class="job-lctn job-attributes-recommended-jobs"><i class="fa fa-briefcase"></i>4-6 Years</div>
                                            <div class="job-lctn job-attributes-recommended-jobs"><i class="fa fa-user-circle"></i>Full Time</div>
                                            <div class="job-lctn job-attributes-recommended-jobs"><i class="fa fa-map-marker" ></i> Bangalore</div>
                                            <div class="job-lctn job-attributes-recommended-jobs"><i class="fa fa-industry"></i>Industry</div>
                                         </div>
                                         <p class="recommended-jobs-description">user interface ux designer,mobile apps designer,Mobile application designer,ui ux designer,Senior Ui Ux Designer</p>
                                         <p class="recommended-jobs-posted-on">Posted on: Today</p>
                                    </div>
                               </div>
                            </div>
					</div>
				</div>
			</div>
		</div>
	</section>


	<footer>
		<div class="block">
			<div class="container">
				<div class="row">
					<div class="col-lg-3 column">
						<div class="widget">
							<div class="about_widget">
								<div class="logo">
									<a href="index.html" title=""><img src="images/resource/logo.png" alt="" /></a>
								</div>
								<span>Collin Street West, Victor 8007, Australia.</span>
								<span>+1 246-345-0695</span>
								<span><a href="https://grandetest.com/cdn-cgi/l/email-protection" class="__cf_email__" data-cfemail="e58c8b838aa58f8a878d908b91cb868a88">[email&#160;protected]</a></span>
								<div class="social">
									<a href="#" title=""><i class="fa fa-facebook"></i></a>
									<a href="#" title=""><i class="fa fa-twitter"></i></a>
									<a href="#" title=""><i class="fa fa-linkedin"></i></a>
									<a href="#" title=""><i class="fa fa-pinterest"></i></a>
									<a href="#" title=""><i class="fa fa-behance"></i></a>
								</div>
							</div><!-- About Widget -->
						</div>
					</div>
					<div class="col-lg-4 column">
						<div class="widget">
							<h3 class="footer-title">Frequently Asked Questions</h3>
							<div class="link_widgets">
								<div class="row">
									<div class="col-lg-6">
										<a href="#" title="">Privacy & Seurty </a>
										<a href="#" title="">Terms of Serice</a>
										<a href="#" title="">Communications </a>
										<a href="#" title="">Referral Terms </a>
										<a href="#" title="">Lending Licnses </a>
										<a href="#" title="">Disclaimers </a>	
									</div>
									<div class="col-lg-6">
										<a href="#" title="">Support </a>
										<a href="#" title="">How It Works </a>
										<a href="#" title="">For Employers </a>
										<a href="#" title="">Underwriting </a>
										<a href="#" title="">Contact Us</a>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="col-lg-2 column">
						<div class="widget">
							<h3 class="footer-title">Find Jobs</h3>
							<div class="link_widgets">
								<div class="row">
									<div class="col-lg-12">
										<a href="#" title="">US Jobs</a>	
										<a href="#" title="">Canada Jobs</a>	
										<a href="#" title="">UK Jobs</a>	
										<a href="#" title="">Emplois en Fnce</a>	
										<a href="#" title="">Jobs in Deuts</a>	
										<a href="#" title="">Vacatures China</a>	
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="col-lg-3 column">
						<div class="widget">
							<div class="download_widget">
								<a href="#" title=""><img src="images/resource/dw1.png" alt="" /></a>
								<a href="#" title=""><img src="images/resource/dw2.png" alt="" /></a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="bottom-line">
			<span>© 2018 Jobhunt All rights reserved. Design by Creative Layers</span>
			<a href="#scrollup" class="scrollup" title=""><i class="la la-arrow-up"></i></a>
		</div>
	</footer>

</div>

<div class="account-popup-area signin-popup-box">
	<div class="account-popup">
		<span class="close-popup"><i class="la la-close"></i></span>
		<h3>User Login</h3>
		<span>Click To Login With Demo User</span>
		<div class="select-user">
			<span>Candidate</span>
			<span>Employer</span>
		</div>
		<form>
			<div class="cfield">
				<input type="text" placeholder="Username" />
				<i class="la la-user"></i>
			</div>
			<div class="cfield">
				<input type="password" placeholder="********" />
				<i class="la la-key"></i>
			</div>
			<p class="remember-label">
				<input type="checkbox" name="cb" id="cb1"><label for="cb1">Remember me</label>
			</p>
			<a href="#" title="">Forgot Password?</a>
			<button type="submit">Login</button>
		</form>
		<div class="extra-login">
			<span>Or</span>
			<div class="login-social">
				<a class="fb-login" href="#" title=""><i class="fa fa-facebook"></i></a>
				<a class="tw-login" href="#" title=""><i class="fa fa-twitter"></i></a>
			</div>
		</div>
	</div>
</div><!-- LOGIN POPUP -->

<div class="account-popup-area signup-popup-box">
	<div class="account-popup">
		<span class="close-popup"><i class="la la-close"></i></span>
		<h3>Sign Up</h3>
		<div class="select-user">
			<span>Candidate</span>
			<span>Employer</span>
		</div>
		<form>
			<div class="cfield">
				<input type="text" placeholder="Username" />
				<i class="la la-user"></i>
			</div>
			<div class="cfield">
				<input type="password" placeholder="********" />
				<i class="la la-key"></i>
			</div>
			<div class="cfield">
				<input type="text" placeholder="Email" />
				<i class="la la-envelope-o"></i>
			</div>
			<div class="dropdown-field">
				<select data-placeholder="Please Select Specialism" class="chosen">
					<option>Web Development</option>
					<option>Web Designing</option>
					<option>Art & Culture</option>
					<option>Reading & Writing</option>
				</select>
			</div>
			<div class="cfield">
				<input type="text" placeholder="Phone Number" />
				<i class="la la-phone"></i>
			</div>
			<button type="submit">Signup</button>
		</form>
		<div class="extra-login">
			<span>Or</span>
			<div class="login-social">
				<a class="fb-login" href="#" title=""><i class="fa fa-facebook"></i></a>
				<a class="tw-login" href="#" title=""><i class="fa fa-twitter"></i></a>
			</div>
		</div>
	</div>
</div><!-- SIGNUP POPUP -->

<script data-cfasync="false" src="../../cdn-cgi/scripts/5c5dd728/cloudflare-static/email-decode.min.js"></script><script src="{{URL::asset('Demo/js/jquery.min.js')}}" type="text/javascript"></script>
<script src="{{URL::asset('Demo/js/modernizr.js')}}" type="text/javascript"></script>
<script src="{{URL::asset('Demo/js/script.js')}}" type="text/javascript"></script>
<script src="{{URL::asset('Demo/js/bootstrap.min.js')}}" type="text/javascript"></script>
<script src="{{URL::asset('Demo/js/wow.min.js')}}" type="text/javascript"></script>
<script src="{{URL::asset('Demo/js/slick.min.js')}}" type="text/javascript"></script>
<script src="{{URL::asset('Demo/js/parallax.js')}}" type="text/javascript"></script>
<script src="{{URL::asset('Demo/js/select-chosen.js')}}" type="text/javascript"></script>
<script src="{{URL::asset('Demo/js/counter.js')}}" type="text/javascript"></script>
<script src="{{URL::asset('Demo/js/custom/home.js')}}" type="text/javascript"></script>

</body>

<!-- Mirrored from grandetest.com/theme/jobhunt-html/index3.html by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 03 Jul 2019 11:09:16 GMT -->
</html>

