
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <title>jQuery Geocomplete Examples</title>
  <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/semantic-ui@2.4.2/dist/semantic.min.css">
  <link rel="stylesheet" href="assets/css-util.min.css">
  <link rel="stylesheet" href="https://www.jqueryscript.net/demo/Google-Address-Autocomplete-Plugin-geocomplete/assets/jquery.geocomplete.min.css">
  <style type="text/css">
    #pageWrapper {
      max-width: 800px !important;
    }

    #myMap {
      max-width: 800px !important;
      max-height: 800px !important;
    }

    .padding {
      height: 100vh;
    }
  </style>
</head>

<body>

  <div id="pageWrapper" class="ui container h-100 py-2 px-0">


    <!-- Header
    ===========================================-->
    <h1 class="ui header text-center">jQuery Geocomplete Examples</h1>
<div class="jquery-script-ads" style="margin:30px auto" align="center"><script type="text/javascript"><!--
google_ad_client = "ca-pub-2783044520727903";
/* jQuery_demo */
google_ad_slot = "2780937993";
google_ad_width = 728;
google_ad_height = 90;
//-->
</script>
<script type="text/javascript"
src="https://pagead2.googlesyndication.com/pagead/show_ads.js">
</script></div>


    <!-- Default Data Attributes
    ===========================================-->
    <span data-modal="default" class="ui top attached fluid primary button">Default Data Attributes</span>
    <div class="ui bottom attached segment">
      <pre class="m-0">        $(<span class="text-green text-darken-1">"#txtAddress1"</span>).geocomplete({
          formId: <span class="text-green text-darken-1">"#myForm"</span>
        });

        &lt;div id=<span class="text-green text-darken-1">"myForm"</span>&gt;
          &lt;input id=<span class="text-green text-darken-1">"txtAddress1"</span> <b>data-geocomplete=<span class="text-green text-darken-1">"street address"</span></b> /&gt;
          &lt;select <b>data-geocomplete=<span class="text-green text-darken-1">"country"</span></b>&gt;&lt;/select&gt;
          ...
        &lt;/div&gt;
      </pre>
    </div>

    <div data-modal="default" class="ui small modal">
      <i class="close icon"></i>
      <div class="header">
        Default Data Attributes
      </div>

      <div class="content p-2">
        <div id="myForm" class="ui form">
          <div class="field">
            <label>Street Address:</label>
            <div class="ui input">
              <input id="txtAddress1" type="text" placeholder="Start typing a street address"
                data-geocomplete="street address">
            </div>
          </div>

          <div class="two fields">
            <div class="field">
              <label>Country:</label>
              <select id='ddlCountry1' class="ui dropdown" data-geocomplete="country">
                <option value="">Select Country</option>
                <option value="1">United States</option>
                <option value="2">Mexico</option>
                <option value="3">Canada</option>
                <option value="4">Chile</option>
              </select>
            </div>

            <div class="field">
              <label>State:</label>
              <div class="ui input">
                <input type="text" data-geocomplete="state">
              </div>
            </div>
          </div>

          <div class="three fields">
            <div class="field">
              <label>County:</label>
              <div class="ui input">
                <input type="text" data-geocomplete="county">
              </div>
            </div>

            <div class="field">
              <label>City:</label>
              <div class="ui input">
                <input type="text" data-geocomplete="city">
              </div>
            </div>

            <div class="field">
              <label>Zip Code:</label>
              <div class="ui input">
                <input type="text" data-geocomplete="zip code">
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>


    <!-- Custom Data Attributes
    ===========================================-->
    <span data-modal="custom" class="ui top attached fluid primary button">Custom Data Attributes</span>
    <div class="ui bottom attached segment">
      <pre class="m-0">        $(<span class="text-green text-darken-1">"#txtAddress2"</span>).geocomplete({
          formId: <span class="text-green text-darken-1">"#myForm2"</span>
          addressDataKey: <span class="text-green text-darken-1">"geo-address"</span>
        });

        &lt;div id=<span class="text-green text-darken-1">"myForm2"</span>&gt;
          &lt;input id=<span class="text-green text-darken-1">"txtAddress2"</span> <b>data-geo-address=<span class="text-green text-darken-1">"street address"</span></b> /&gt;
          &lt;select <b>data-geo-address=<span class="text-green text-darken-1">"country"</span></b>&gt;&lt;/select&gt;
          ...
        &lt;/div&gt;
      </pre>
    </div>

    <div data-modal="custom" class="ui small modal">
      <i class="close icon"></i>
      <div class="header">
        Custom Data Attributes
      </div>

      <div class="content p-2">
        <div id="myForm2" class="ui form">
          <div class="field">
            <label>Street Address:</label>
            <div class="ui input">
              <input id="txtAddress2" type="text" placeholder="Start typing a street address"
                data-geo-address="street address">
            </div>
          </div>

          <div class="two fields">
            <div class="field">
              <label>Country:</label>
              <select class="ui dropdown" data-geo-address="country">
                <option value="">Select Country</option>
                <option value="1">United States</option>
                <option value="2">Mexico</option>
                <option value="3">Canada</option>
                <option value="4">Chile</option>
              </select>
            </div>

            <div class="field">
              <label>State:</label>
              <div class="ui input">
                <input type="text" data-geo-address="state">
              </div>
            </div>
          </div>

          <div class="three fields">
            <div class="field">
              <label>County:</label>
              <div class="ui input">
                <input type="text" data-geo-address="county">
              </div>
            </div>

            <div class="field">
              <label>City:</label>
              <div class="ui input">
                <input type="text" data-geo-address="city">
              </div>
            </div>

            <div class="field">
              <label>Zip Code:</label>
              <div class="ui input">
                <input type="text" data-geo-address="zip code">
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>


    <!-- Appended to Parent
    ===========================================-->
    <span data-modal="append" class="ui top attached fluid primary button">Appended To Parent</span>
    <div class="ui bottom attached segment">
      <pre class="m-0">        $(<span class="text-green text-darken-1">"#txtAddress3"</span>).geocomplete({
          <b>appendToParent: true</b>
        });
      </pre>
    </div>

    <div data-modal="append" class="ui small modal">
      <i class="close icon"></i>
      <div class="header">
        Appended to Parent
      </div>

      <div class="content p-2">
        <div class="ui form">
          <div class="field">
            <label>Street Address:</label>
            <div class="ui input">
              <input id="txtAddress3" type="text" placeholder="Start typing a street address">
            </div>
          </div>
        </div>

        <div class="padding"></div>
      </div>
    </div>


    <!-- Not Appended to Parent
    ===========================================-->
    <span data-modal="no-append" class="ui top attached fluid primary button">Not Appended To Parent</span>
    <div class="ui bottom attached segment">
      <pre class="m-0">        $(<span class="text-green text-darken-1">"#txtAddress4"</span>).geocomplete({
          <b>appendToParent: false</b>
        });
      </pre>
    </div>

    <div data-modal="no-append" class="ui small modal">
      <i class="close icon"></i>
      <div class="header">
        Not Appended to Parent
      </div>

      <div class="content p-2">
        <div class="ui form">
          <div class="field">
            <label>Street Address:</label>
            <div class="ui input">
              <input id="txtAddress4" type="text" placeholder="Start typing a street address">
            </div>
          </div>
        </div>

        <div class="padding"></div>
      </div>
    </div>


    <!-- Strict Bounds
    ===========================================-->
    <span data-modal="strict" class="ui top attached fluid primary button">Strict Bounds</span>
    <div class="ui bottom attached segment">
      <pre class="m-0">        $(<span class="text-green text-darken-1">"#txtAddress5"</span>).geocomplete({
          <b>geolocate    : true,</b>
          <b>strictBounds : true</b>
        });
      </pre>
    </div>

    <div data-modal="strict" class="ui small modal">
      <i class="close icon"></i>
      <div class="header">
        Strict Bounds
      </div>

      <div class="content p-2">
        <div id="myStrictForm" class="ui form">
          <div class="field">
            <label>Street Address:</label>
            <div class="ui input">
              <input id="txtAddress5" type="text" placeholder="Start typing a street address"
                data-geocomplete="street address">
            </div>
          </div>

          <div class="two fields">
            <div class="field">
              <label>Country:</label>
              <select class="ui dropdown" data-geocomplete="country">
                <option value="">Select Country</option>
                <option value="1">United States</option>
                <option value="2">Mexico</option>
                <option value="3">Canada</option>
                <option value="4">Chile</option>
              </select>
            </div>

            <div class="field">
              <label>State:</label>
              <div class="ui input">
                <input type="text" data-geocomplete="state">
              </div>
            </div>
          </div>

          <div class="three fields">
            <div class="field">
              <label>County:</label>
              <div class="ui input">
                <input type="text" data-geocomplete="county">
              </div>
            </div>

            <div class="field">
              <label>City:</label>
              <div class="ui input">
                <input type="text" data-geocomplete="city">
              </div>
            </div>

            <div class="field">
              <label>Zip Code:</label>
              <div class="ui input">
                <input type="text" data-geocomplete="zip code">
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>


    <!-- Callbacks
    ===========================================-->
    <span data-modal="callbacks" class="ui top attached fluid primary button">Callbacks</span>
    <div class="ui bottom attached segment">
      <pre class="m-0">        $(<span class="text-green text-darken-1">"#txtAddress6"</span>).geocomplete({
          <b>onChange</b>: function(<span class="text-orange text-darken-1">name</span>, <span class="text-orange text-darken-1">result</span>) {
            console.log(name, result)
          },
          <b>onNoResult</b>: function(<span class="text-orange text-darken-1">name</span>) {
            console.log(<span class="text-green text-darken-1">"Could not find a result for "</span> + name)
          }
        });
      </pre>
    </div>

    <div data-modal="callbacks" class="ui small modal">
      <i class="close icon"></i>
      <div class="header">
        Callbacks
      </div>

      <div class="content p-2">
        <div class="ui form">
          <div class="field">
            <label>Street Address:</label>
            <div class="ui input">
              <input id="txtAddress6" type="text" placeholder="Start typing a street address">
            </div>
          </div>
        </div>
      </div>
    </div>


    <!-- Google Map
    ===========================================-->
    <a href="#txtAddress7" class="ui top attached fluid primary button">Google Map</a>
    <div class="ui bottom attached segment">
      <pre class="m-0">        var myMap = new google.maps.Map(document.getElementById(<span class="text-green text-darken-1">"myMap"</span>), {
          center : { lat: 37.5, lng: -120 },
          zoom   : 6
        });

        $(<span class="text-green text-darken-1">"#txtAddress7"</span>).geocomplete({
          map: myMap
        });
      </pre>
    </div>

    <div class="ui fluid input">
      <input id="txtAddress7" type="text" placeholder="Start typing a street address">
    </div>
    <div id="myMap" class="h-100 w-100 mb-2"></div>

  </div>

  <script type="text/javascript"
    src="https://maps.googleapis.com/maps/api/js?libraries=places&key=AIzaSyDK8rPfI9E6f1dCzMhNsOjxFzs4WcX02Ic"></script>
  <script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
  <script src="https://cdn.jsdelivr.net/npm/semantic-ui@2.4.2/dist/semantic.min.js"></script>
  <script src="https://www.jqueryscript.net/demo/Google-Address-Autocomplete-Plugin-geocomplete/assets/jquery.geocomplete.min.js"></script>
  <script type="text/javascript">
    $(document).ready(function () {
      $(".ui.dropdown").dropdown();
      $(".ui.modal").modal({ autofocus: false })

      $(document).on("click", ".button", function () {
        var modal = $(this).attr("data-modal");
        $(".modal[data-modal='" + modal + "']").modal("show");
      });

      $("#txtAddress1").geocomplete({
        formId: "#myForm"
      });

      $("#txtAddress2").geocomplete({
        formId: "#myForm2",
        addressDataKey: "geo-address"
      });

      $("#txtAddress3").geocomplete({
        appendToParent: true
      });

      $("#txtAddress4").geocomplete({
        appendToParent: false
      });

      $("#txtAddress5").geocomplete({
        geolocate: true,
        strictBounds: true,
        formId: "#myStrictForm"
      });

      $("#txtAddress6").geocomplete({
        onChange: function (name, result) {
          console.log(name, result)
        },
        onNoResult: function (name) {
          console.log("Could not find a result for " + name)
        }
      });

      var myMap = new google.maps.Map(document.getElementById("myMap"), {
        center: { lat: 37.5, lng: -120 },
        zoom: 6
      });

      $("#txtAddress7").geocomplete({
        map: myMap
      });
    });
  </script>
</body>
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-36251023-1']);
  _gaq.push(['_setDomainName', 'jqueryscript.net']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
</html>
