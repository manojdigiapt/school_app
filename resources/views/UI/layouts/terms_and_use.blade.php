@extends('UI.base')

@section('Content')
<section class="home-section-height">
        <div class="block no-padding overlape">
            <div class="container fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="main-featured-sec style2">
                            <ul class="main-slider-sec style2 text-arrows home-img">
                                <li class="slideHome"><img src="{{URL::asset('UI/images/resource/mslider3.jpg')}}" alt="" /></li>
                            </ul>
                            <div class="job-search-sec">
                                <div class="job-search style2">
                                    <h3>The Easiest Way to Get Your New Job</h3>
                                    <span>Find Jobs, Employment & Career Opportunities</span>
                                    <div class="search-job2">	
                                        <form class="home-search-box">
                                            <div class="row no-gape">
                                                <div class="col-lg-6 col-md-6 col-sm-4 col-xs-6">
                                                    <div class="job-field">
                                                        <input type="text" placeholder="Skill, Designation" />
                                                    </div>
                                                </div>
                                                <div class="col-lg-3 col-md-3 col-sm-4">
                                                    <div class="job-field">
                                                        <select data-placeholder="Any category" class="chosen-city">
                                                            <option selected disabled>Location</option>
                                                            <option>Bangalore</option>
                                                            <option>Mumbai</option>
                                                            <option>Delhi</option>
                                                            <option>Pune</option>
                                                            <option>Hyderabad</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-lg-3  col-md-3 col-sm-4">
                                                    <button type="submit" class="home-search-btn"><img src="{{URL::asset('UI/images/custom/search.png')}}" alt=""> SEARCH</button>
                                                </div>
                                            </div>
                                        </form>
                                    </div><!-- Job Search 2 -->
                                    <div class="quick-select-sec quick-select-pad">
                                        <div class="row">
                                            <div class="col-lg-3 col-md-3 col-sm-3">
                                                <div class="quick-select border-right-quick-select">
                                                    <a href="#" title="">
                                                        <p class="quick-select1"><img src="{{URL::asset('UI/images/custom/bank-building.svg')}}" alt=""></p>
                                                        <span>Accounting / Finance</span>
                                                        <p>(22 open positions)</p>
                                                    </a>
                                                </div><!-- Quick Select -->
                                            </div>
                                            <div class="col-lg-3 col-md-3 col-sm-3">
                                                <div class="quick-select border-right-quick-select">
                                                    <a href="#" title="">
                                                        <p class="quick-select2"><img src="{{URL::asset('UI/images/custom/teamwork.svg')}}" alt=""></p>
                                                        <span>Human Resources</span>
                                                        <p>(06 open positions)</p>
                                                    </a>
                                                </div><!-- Quick Select -->
                                            </div>
                                            <div class="col-lg-3 col-md-3 col-sm-3">
                                                <div class="quick-select border-right-quick-select">
                                                    <a href="#" title="">
                                                        <p class="quick-select3"><img src="{{URL::asset('UI/images/custom/college-graduation.svg')}}" alt=""></p>
                                                        <span>Education Training</span>
                                                        <p>(03 open positions)</p>
                                                    </a>
                                                </div><!-- Quick Select -->
                                            </div>
                                            <div class="col-lg-3 col-md-3 col-sm-3">
                                                <div class="quick-select">
                                                    <a href="#" title="">
                                                        <p class="quick-select4"><img src="{{URL::asset('UI/images/custom/pencil.svg')}}" alt=""></p>
                                                        <span>Design/Art & Multimedia</span>
                                                        <p>(03 open positions)</p>
                                                    </a>
                                                </div><!-- Quick Select -->
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

<section>
		<div class="block">
			<div class="container">
				 <div class="row">
				 	<div class="col-lg-12 pad-top8percentage">
				 		<div class="about-us aboutus-pad-left-right">
				 			<div class="row">
				 				{{-- <div class="col-lg-12">
				 					<h3>Terms of Use                                        </h3>
                                 </div> --}}
                                 
                                 <div class="col-lg-12">
                                        <h3>Terms of use Agreement for ACCOUNTSWALE Services                                       </h3>
                                    </div>
				 				<div class="col-lg-12">
                                     <p>TECHTALENTS SOFTWARE TECHNOLOGIES PVT LTD Pvt. ltd, ("TECHTALENTS SOFTWARE TECHNOLOGIES PVT LTD", "ACCOUNTSWALE’, "us", or "we") provides its services under the brand name of ACCOUNTSWALE.IN to Jobseekers , Employers and Advertisers based on their requirement and does not take any responsibility of the content posted by them on our website ACCOUNTSWALE.IN , the sole responsibility of the content posted by candidates , employers , advertisers or partners stays with them and TECHTALENTS SOFTWARE TECHNOLOGIES PVT LTD , any of its sister concern or any brand cannot be held liable for it. If any of the content results into loss , damage or dispute to any party due to content posted on our website , that would be the matter between concerned parties and TECHTALENTS SOFTWARE TECHNOLOGIES PVT LTD cannot be considered as a party in dispute in whatsoever case , ACCOUNTSWALE.IN is only a content display platform for various parties and does not hold any right on any content posted by any of our stakeholders (Candidates , Employer , Advertisers , Partners etc.) but if any party raised any objection on content on website , we hold the right to remove or delete the content from the website in whatsoever case without answerable to any party.

                                        This Terms of Use Agreement ("Agreement"),including the ACCOUNTSWALE.IN Privacy Policy which is hereby incorporated into this Agreement by reference, sets forth the legally binding terms for your use of the Services. The current version of this Agreement was first updated on 5th SEPTEMBER, 2019 and becomes effective from 5th SEPTEMBER, 2019. By accessing and using the Services, you agree to comply with and be bound by this Agreement, whether you are visiting the Site ("Visitor") or you are a "Member" (which means that you have registered on the Site). The term "User" refers to both Visitors and Members or any of our partners. If you sign up for additional features and services that are governed by additional terms and conditions, we will inform you accordingly when you sign up for these additional features and services. Unless otherwise provided by the additional terms and conditions, they are hereby incorporated into this Agreement by reference. Please read this Agreement carefully before using the Services or parts thereof. This Agreement is a legally binding agreement between you (the individual using the Services, and the company (if any) for which they are used) and ACCOUNTSWALE.IN.</p>
                                     
                                     <p>Job Seekers</p>
                                     
                                     <ul>
                                         <li>Your ACCOUNTSWALE.IN Account</li>
                                         <li>Additional/Paid Services</li>
                                         <li>Ownership; Use of Services</li>
                                         <li>Third Parties and Other Users</li>
                                         <li>Refund Policy</li>
                                         <li>Acceptable Use and Conduct</li>
                                         <li>Third-Party Websites</li>
                                         <li>Copyright Policy</li>
                                         <li>Disclaimers</li>
                                         <li>Limitation on Liability</li>
                                         <li>Miscellaneous</li>
                                         <li>Copyright/Trademark Information</li>
                                     </ul>

                                     <p>Employers</p>
                                     
                                     <ul>
                                         <li>Job Posting</li>
                                         <li>Database Package services</li>
                                         <li>Charges</li>
                                         <li>Disclaimer of Warranties and Liability</li>
                                         <li>Advertising Material</li>
                                         <li>Manual Shortlisting</li>
                                         <li>The subscriber/Recruiter</li>
                                         <li>ANTI SPAM POLICY</li>
                                     </ul>
                                 </div>
                                 

                                 <div class="col-lg-12">
                                        <h3>Your ACCOUNTSWALE.IN Account                                       </h3>
                                    </div>
				 				<div class="col-lg-12">
                                     <p>In order to use the Services (and to become a Member),you must create a ACCOUNTSWALE.IN account on the Site ("Account")</p>
                                     
                                 </div>
                                 
                                 <div class="col-lg-12">
                                        <h3>Eligibility                                       </h3>
                                    </div>
				 				<div class="col-lg-12">
                                     <p>By using the Services, you represent and warrant that: (a) all registration information you submit is truthful and accurate; (b) you will maintain the accuracy of such information; (c) you are thirteen (13) years of age or older upon registration; and (d) your use of the Services does not violate any applicable law or regulation, or any other obligation (including contractual obligation) you might have towards third parties. Any Account you have created by registering on the Site may be deleted without warning if we believe that any representation and warranty you make hereunder is breached or inaccurate.</p>
                                 </div>
                                 

                                 <div class="col-lg-12">
                                        <h3>Password                                       </h3>
                                    </div>
				 				<div class="col-lg-12">
                                     <p>When you sign up to become a Member, you will also be asked to choose a password for your Account. You are entirely responsible for maintaining the confidentiality of your password. You agree not to use the Account or password of another Member at any time. You agree to notify us immediately if you suspect any unauthorized use of your Account or access to your password. You are solely responsible for any and all use of your Account.</p>
                                    
                                 </div>
                                 

                                 <div class="col-lg-12">
                                        <h3>Term and Termination                                      </h3>
                                    </div>
				 				<div class="col-lg-12">
                                     <p>.  This Agreement shall remain in full force and effect (a) while you are using the Services IN ANY FORM, if you are a Visitor or User, and (b) for the duration of your membership, if you are a Member. You may delete your Account and end your membership at any time, for any or no reason by sending your request to Customer Care or from your account ; please note that even if you delete your Account but continue to use the Services as a Visitor, your use of the Services is still subject to this Agreement. Unless ACCOUNTSWALE.IN has terminated your Account, you can start a new membership by registering and providing new details about the account. We may terminate your membership for any or no reason at any time by ceasing to provide the Services to you. You understand that termination of this Agreement and the Account you have created with us may involve deletion of your Account information from our live databases. We will not have any liability whatsoever to you for any termination of your Account or related deletion of your information.</p>

                                     <p>Rejection and Non Acceptance of Registration</p>

                                     <p>.  ACCOUNTSWALE.IN can at any point of time reject a particular registration (Candidates, users, advertisers and partners) if the party is found to use the site for actions that hampers the reputation of ACCOUNTSWALE.IN business.</p>
                                    
                                 </div>
                                 

                                 <div class="col-lg-12">
                                        <h3>Additional/Paid Services:                                       </h3>
                                    </div>
				 				<div class="col-lg-12">
                                     <h5>For Candidates</h5>
                                     <p>:  The site is a public site with free access and ACCOUNTSWALE.IN assumes no liability for the quality and genuineness of responses. ACCOUNTSWALE.IN cannot monitor the responses that a person may receive in response to information he/she has displayed on the site. The individual/company would have to conduct its own background checks on the bonafide nature of all response(s).</p>
                                    
                                 </div>
                                 

                                 <div class="col-lg-12">
                                        <h3>Ownership; Use of Services                                      </h3>
                                    </div>
				 				<div class="col-lg-12">
                                     <h5>Your Grant</h5>
                                     <p>.  We do not claim ownership in any Content that you upload, provide, make available, or otherwise transfer ("post") on the Services, but to be able to legally provide our Users with the Services, we have to have certain rights to use such Content in connection with the Services, as set forth below. In return, we also grant you certain use rights as set forth in Section 2.2 to the Content that we (or our licensors) own and use to provide the Services to you and other Users. By posting any Content on the Services, you hereby grant to us an unrestricted, irrevocable, perpetual, non-exclusive, fully-paid and royalty-free, license (with the right to sublicense through unlimited levels of sublicenses) to use, copy, perform, display, create derivative works of, and distribute such Content in any and all media (now known or later developed) throughout the world. No compensation will be paid with respect to the Content that you post through the Services. You should only post Content to the Services that you are comfortable sharing with others under the terms and conditions set forth herein.</p>
                                    
                                 </div>
                                 

                                 <div class="col-lg-12">
                                        <h3>Third Party Content                                      </h3>
                                    </div>
				 				<div class="col-lg-12">
                                     <p>.  The Services contain Content provided by us and our licensors ("ACCOUNTSWALE Content"). We and our licensors (including other Users) own and retain all proprietary rights in the ACCOUNTSWALE.IN Content and we own and retain all property rights in the Services. Provided you are a User, and subject to the terms and conditions of this Agreement, we hereby grant you a limited, revocable, non-sublicensable license under the intellectual property rights licensable by us to download, view, copy and print ACCOUNTSWALE.IN Content from the Services solely for your personal use in connection with using the Services.</p>
                                    
                                 </div>
                                 

                                 <div class="col-lg-12">
                                        <h3>Third Parties and Other Users                                      </h3>
                                    </div>
				 				<div class="col-lg-12">
                                     <h5>Services - Ownership and Restrictions</h5>
                                     <p>.  You acknowledge that all the intellectual property rights in the Services (excluding any Content provided by Users) are owned by ACCOUNTSWALE.IN, or ACCOUNTSWALE.IN content licensors. You agree not to (a) reproduce, modify, publish, transmit, distribute, publicly perform or display, sell, or create derivative works based on the Services or the ACCOUNTSWALE Content and (b) rent, lease, loan, or sell access to the Services. “Content” means any work of authorship or information, including Jobs, Company Placement Papers, Interview Reviews, reports , Community comments, ads, opinions, postings, messages, text, files, images, photos, works of authorship, e-mail, or other materials.</p>
                                    
				 				</div>
                                 
                                 
                                 <div class="col-lg-12">
                                        <h3>ACCOUNTSWALE.IN Content                                      </h3>
                                    </div>
				 				<div class="col-lg-12">
                                     <p>.  Content from other Users, advertisers, and other third parties is made available to you through the Services. Because we do not control such Content, (a) you agree that we are not responsible for any such Content, including advertising and information about third party products or service, employer, interview process , job description or job posting and any other information provided by other Members through Job posting, comments, and (b) we make no guarantees about the accuracy, currency, suitability, or quality of the information in such Content, and we assume no responsibility for unintended, objectionable, inaccurate, misleading, or unlawful Content made available by other Users, advertisers, and third parties.</p>
                                    
				 				</div>
                                 
                                 

                                 <div class="col-lg-12">
                                        <h3>Responsibility                                     </h3>
                                    </div>
				 				<div class="col-lg-12">
                                     <p>.  Your interactions with other Users on the Services or with advertisers, including payment and delivery of goods or services, and any other terms, conditions, warranties or representations associated with such dealings, are solely between you and the other User or advertiser. You agree that we are not responsible for any loss or damage incurred as the result of any such dealings or with respect to any other User's use or disclosure of information about you that you have provided to publicly available sections of the Services. If there is a dispute between you and any third party (including any User),we are under no obligation to become involved; however, we reserve the right, but have no obligation, to monitor disputes between you and other Users.</p>
                                    
				 				</div>
                                 
                                 

                                 <div class="col-lg-12">
                                        <h3>ACCOUNTSWALE Member Interaction                                      </h3>
                                    </div>
				 				<div class="col-lg-12">
                                     <p>.  You will not use any information obtained from the Services in order to harass, abuse, or harm another person, or in order to contact, advertise to, solicit, or sell to any User without their prior explicit consent. In order to protect our Members from such advertising or solicitation, we reserve the right to restrict the number of communications which a Member may send to other Members and the sharing of any Content in any period to a number and amount which we deem appropriate in our sole discretion.</p>
                                    
				 				</div>
                                 
                                 

                                 <div class="col-lg-12">
                                        <h3>Refund Policy.                                      </h3>
                                    </div>
				 				<div class="col-lg-12">
                                     <h5>Refund & Cancellation Policy: Corporates, Employers & Partners.</h5>
                                     <p>Incase of the purchase of online package from ACCOUNTSWALE.IN site, there will be no cancellation once the amount is paid. Under no condition will ACCOUNTSWALE.IN , refund the amount once paid.</p>

                                     <p>Incase of multiple payments for a single product, you are requested to bring the same to our notice. After your notification we will be able to refund the extra amount within 30 days on a case to case basis. The same needs to be brought to our notice within 5 days of the transaction, after which we will not be able to refund the same.</p>
                                    
				 				</div>
                                 
                                 
                                 <div class="col-lg-12">
                                        <h3>Refund & Cancellation Policy: Candidates                                      </h3>
                                    </div>
				 				<div class="col-lg-12">
                                     <p>Once bought under no circumstances your premium membership can be considered for cancellation. In extension, under no circumstances any refund will be made either part or in full.</p>

                                     <p>In case you have bought Premium Services multiple times under any circumstances, our team will investigate the matter thoroughly and decision will be made on a case to case basis Again, under no circumstances it is guaranteed that you payment will be returned.</p>

                                     <p>Under no circumstances your Premium Membership is transferrable to another account holder or any other account you wish to create. Any attempts leading to such conclusion may force ACCOUNTSWALE.IN to cancel your account and withdraw all services.</p>

                                     <p>Premium membership is valid for certain period and certain usage. It will expire automatically if either is utilised. One can renew the premium membership by paying the required amount. Under no circumstances will there be any extension of the same.</p>

                                     <p>Incase of multiple payments for a single product, you are requested to bring the same to our notice. After your notification we will be able to refund the extra amount within 7 days on a case to case basis. The same needs to be brought to our notice within 7 days of the transaction, after which we will not be able to refund the same.</p>
                                    
                                 </div>
                                 
                                 <div class="col-lg-12">
                                        <h3>Acceptable Use and Conduct                                      </h3>
                                    </div>
				 				<div class="col-lg-12">
                                     <p>You are solely responsible for any and all Content that is posted through your Account on the Services and for your interactions with other Users</p>
                                    
                                 </div>
                                 
                                 <div class="col-lg-12">
                                        <h3>Prohibited Content                                      </h3>
                                    </div>
				 				<div class="col-lg-12">
                                     <p>.  You agree that you will not post any Prohibited Content or use any Prohibited Content in connection with the Services. "Prohibited Content" is Content that: (i) is offensive or promotes racism, bigotry, hatred or physical harm of any kind against any group or individual, or is pornographic or sexually explicit in nature; (ii) bullies, harasses or advocates stalking, bullying, or harassment of another person; (iii) involves the transmission of "junk mail":, "chain letters," or unsolicited mass mailing, or ,"spamming,"; (iv) is false or misleading or promotes, endorses or furthers illegal activities or conduct that is abusive, threatening, obscene, defamatory or libellous; (v) promotes, copies, performs or distributes an illegal or unauthorized copy of another person's work that is protected by copyright or trade secret law, such as providing pirated computer programs or links to them, providing information to circumvent manufacturer-installed copy-protection devices, or providing pirated music, videos, or movies, or links to such pirated music, videos, or movies; (vi) is involved in the exploitation of persons under the age of eighteen (18) in a sexual or violent manner, or solicits personal information from anyone under eighteen (18); (vii) provides instructional information about illegal activities such as making or buying illegal weapons, violating someone's privacy, or providing or creating computer viruses and other harmful code; (viii) solicits passwords or personally identifying information for commercial or unlawful purposes from other Users; (ix) except as expressly approved by us, involves commercial activities and/or promotions such as contests, sweepstakes, barter, advertising, or pyramid schemes; (x) contains viruses, Trojan horses, worms, time bombs, cancelbots, corrupted files, or similar software; or ; (xi) posts or distributes information which would violate any confidentiality, non-disclosure or other contractual restrictions or rights of any third party, including any current or former employers or potential employers, or (xii) otherwise violates the terms of this Agreement or creates liability for us.</p>
                                    
                                 </div>
                                 
                                 <div class="col-lg-12">
                                        <h3>Representations Regarding Your Content                                     </h3>
                                    </div>
				 				<div class="col-lg-12">
                                     <p>.  You represent and warrant that: (a) you own the Content posted by you on the Service or otherwise have the right to grant the license set forth in this Agreement, (b) your Content does not violate the privacy rights, publicity rights, copyright rights, or other rights of any person, (c) by providing or posting your Content, you do not violate any confidentiality, non-disclosure, or contractual obligations you might have towards a third party, including your current or former employer or any potential employer, (d) any information you provide in a Job posting, advertisements is correct, and (e) any information you provide about your current, past or potential status as an user is correct and complete. Please make sure that you only provide information to the Services that you are allowed to provide without violating any obligations you might have towards a third party, including any confidentiality, non-disclosure or contractual obligations. Please do not provide any information that you are not allowed to share with others, including by contract or law; please note that any information you provide will be accessible by to employers to ensure that you will get more job opportunities but the ownership of the content will stays with you.</p>
                                    
                                 </div>
                                 

                                 <div class="col-lg-12">
                                        <h3>Enforcement by ACCOUNTSWALE.IN                                     </h3>
                                    </div>
				 				<div class="col-lg-12">
                                     <p>.  Any use of the Services in violation of this Agreement may result in, among other consequences, termination or suspension of your rights to use the Services. We may disclose information about your use of the Services in accordance with our privacy policy. We have the right (but not the obligation) to review any Content and delete (or modify) any Content that in our sole discretion violates this Agreement or which is Prohibited Content, or may otherwise violate the rights, harm, or threaten the safety of any User or any other person, or create liability for us or any User. We reserve the right (but have no obligation) to investigate and take appropriate legal action in our sole discretion against you if you violate this provision or any other provision of this Agreement, including without limitation, removing Content from the Services (or modifying it),terminating your membership and Account, reporting you to law enforcement authorities, and taking legal action against you. You are solely responsible for creating backup copies of and replacing any Content you post on the Services at your sole cost and expense.</p>
                                    
                                 </div>
                                 

                                 <div class="col-lg-12">
                                        <h3>Lawful Use                                    </h3>
                                    </div>
				 				<div class="col-lg-12">
                                     <p>.  You will use the Services in a manner consistent with any and all applicable laws and regulations and solely for lawful purposes. The Services are for the personal use of Users only. Commercial advertisements, affiliate links, and other forms of solicitation may be removed from your Content without notice and may result in suspension or termination of your Account.</p>
                                    
                                 </div>
                                 

                                 <div class="col-lg-12">
                                        <h3>No Disruption                                     </h3>
                                    </div>
				 				<div class="col-lg-12">
                                     <p>You will not: (i) cover or obscure any banner advertisements on the Services, or any ACCOUNTSWALE.IN page via HTML/CSS, scripting, or any other means, (ii) interfere with, disrupt, or create an undue burden on the Services or the networks or services connected to the Services; (iii) introduce software or automated agents to the Services, or access the Service so as to produce multiple accounts, generate automated messages, or to strip or mine data from the Services; or (iv) interfere with, disrupt, or modify any data or functionality of the Services.</p>
                                    
                                 </div>
                                 
                                 <div class="col-lg-12">
                                        <h3>Miscellaneous                                    </h3>
                                    </div>
				 				<div class="col-lg-12">
                                     <p>.  You will not attempt to impersonate another User or person, including any of our employees. You will use the Services in a manner consistent with any and all applicable laws and regulations.

                                        </p>
                                    
                                 </div>
                                 
                                 <div class="col-lg-12">
                                        <h3>Third-Party Websites                                    </h3>
                                    </div>
				 				<div class="col-lg-12">
                                     <p>The Services may contain links to third-party websites (“Third-Party Websites”) (a) placed by us as a service to those interested in this information; or (b) posted by other Members. You use all such links to Third-Party Websites at your own risk. We do not monitor or have any control over, and make no claim or representation regarding Third-Party Websites. To the extent such links are provided by us, they are provided only as a convenience, and such link to a Third-Party Website does not imply our endorsement, adoption or sponsorship of, or affiliation with, such Third-Party Website. When you leave the Site, our terms and policies no longer govern.

                                        </p>
                                    
                                 </div>
                                 

                                 <div class="col-lg-12">
                                        <h3>Copyright Policy                                    </h3>
                                    </div>
				 				<div class="col-lg-12">
                                     <p>It is our policy to terminate membership privileges of any Member who repeatedly infringes copyright upon prompt notification to us by the copyright owner or the copyright owner's legal agent. Without limiting the foregoing, if you believe that your work has been copied and posted on the Services in a way that constitutes copyright infringement, please provide our Copyright Agent with the following information: (i) an electronic or physical signature of the person authorized to act on behalf of the owner of the copyright interest; (ii) an identification of the copyrighted work that you claim has been infringed; (iii) a description of where the material that you claim is infringing is located on the Services; (iv) your address, telephone number, and e-mail address; (v) a written statement by you that you have a good faith belief that the disputed use is not authorized by the copyright owner, its agent, or the law; (vi) a statement by you, made under penalty of perjury, that the above information in your notice is accurate and that you are the copyright owner or authorized to act on the copyright owner's behalf.

                                        </p>
                                    
                                 </div>
                                 

                                 <div class="col-lg-12">
                                        <h3>Disclaimers                                    </h3>
                                    </div>
				 				<div class="col-lg-12">
                                     <p>We are not responsible for any incorrect or inaccurate Content (including any information in profiles) posted on the Services, whether caused by Users or by any of the equipment or programming associated with or utilized in the Services. We are not responsible for the conduct, whether online or offline, of any User of the Services. We assume no responsibility for any error, omission, interruption, deletion, defect, delay in operation or transmission, communications line failure, theft or destruction or unauthorized access to, or alteration of, any communication with other Users. We are not responsible for any problems or technical malfunction of any hardware and software due to technical problems on the Internet or at the Site or combination thereof, including any injury or damage to Users or to any person's computer related to or resulting from participation or downloading materials in connection with the Services. Under no circumstances shall we be responsible for any loss or damage, including personal injury or death, resulting from use of the Services or from any Content posted on the Site or transmitted to Users, or any interactions between Users of the Services, whether online or offline. The Services are provided “As-Is” and as available. We expressly disclaim any warranties and conditions of any kind, whether express or implied, including the warranties or conditions of merchantability, fitness for a particular purpose, title, quiet enjoyment, accuracy, or non-infringement. We make no warranty that: (a) the Services will meet your requirements; (b) the Services will be available on an uninterrupted, timely, secure, or error-free basis; or (c) the results that may be obtained from the use of the Services will be accurate or reliable. Some jurisdictions do not allow the exclusion of implied warranties, so the above exclusion may not apply to you.</p>
                                    
                                 </div>
                                 

                                 <div class="col-lg-12">
                                        <h3>Limitation on Liability                                    </h3>
                                    </div>
				 				<div class="col-lg-12">
                                     <p>In no event shall we be liable to you or any third party for any lost profit or any indirect, consequential, exemplary, incidental, special or punitive damages arising from your use of the Services, even if we have been advised of the possibility of such damages. Notwithstanding anything to the contrary contained herein, our liability to you for any damages arising from or related to your use of the Services (for any cause whatsoever and regardless of the form of the action.</p>

                                    <p>Some jurisdictions do not allow the limitation or exclusion of liability for incidental of consequential damages, so the above limitation or exclusion may not apply to you and you may also have other legal rights that vary from jurisdiction to jurisdiction.</p>
                                    
                                 </div>
                                 
                                 <div class="col-lg-12">
                                        <h3>Miscellaneous                                    </h3>
                                    </div>
				 				<div class="col-lg-12">
                                     <h5>Amendments</h5>
                                     <p>.  This Agreement may be modified by us from time to time. If we make material changes to the Agreement, we will notify you by sending an e-mail or you need to check the website for any updates. You agree that such amended Agreement will be effective thirty (30) days after being sent to you, and your continued use of the Services after that time shall constitute your acceptance of the amended Agreement.</p>
                                    
                                 </div>
                                 
                                 <div class="col-lg-12">
                                        <h3>Force Majeure                                   </h3>
                                    </div>
				 				<div class="col-lg-12">
                                     <p>.  Any delay in the performance of any duties or obligations of either party will not be considered a breach of this Agreement if such delay is caused by a labor dispute, shortage of materials, fire, earthquake, flood, war, terrorism, governmental act, failures of common carriers (including Internet service providers),acts of God, or any other event beyond the control of such party, provided that such party uses reasonable efforts, under the circumstances, to notify the other party of the circumstances causing the delay and to resume performance as soon as possible.</p>
                                    
                                 </div>
                                 
                                 <div class="col-lg-12">
                                        <h3>Release                                    </h3>
                                    </div>
				 				<div class="col-lg-12">
                                     <p>.  To the maximum extent permitted by applicable law, you hereby release us, our officers, employees, agents and successors from claims, demands any and all losses, damages, rights, claims, and actions of any kind including personal injuries, death, and property damage, that is either directly or indirectly related to or arises from (i) any interactions with other Users, or (ii) your participation in any of our offline events.</p>
                                    
                                 </div>
                                 
                                 <div class="col-lg-12">
                                        <h3>Indemnity                                   </h3>
                                    </div>
				 				<div class="col-lg-12">
                                     <p>.  You agree to defend, indemnify, and hold us, our subsidiaries, affiliates, officers, agents, and other partners and employees, harmless from any loss, liability, claim, or demand, including reasonable attorney's fees, made by any third party due to or arising out of your use of the Services and arising from your breach of any provision of this Agreement.</p>
                                    
                                 </div>
                                 
                                 <div class="col-lg-12">
                                        <h3>Governing Law and Arbitration                                    </h3>
                                    </div>
				 				<div class="col-lg-12">
                                     <p>.  This Agreement shall be governed by the laws of the State of Karnataka without giving effect to any conflict of laws principles that may provide the application of the law of another jurisdiction. You agree to submit to the personal jurisdiction of the state courts located within Karnataka and the federal courts in the Karnataka. Any claim or dispute in connection with this Agreement shall be resolved in a cost effective manner through binding non-appearance-based arbitration. The arbitration shall be initiated through an established alternative dispute resolution provider mutually agreed upon by the parties. The alternative dispute resolution provider and the parties must comply with the following rules: a) the arbitration shall be conducted by telephone, online and/or be solely based on written submissions, the specific manner shall be chosen by the party initiating the arbitration; b) the arbitration shall not involve any personal appearance by the parties or witnesses unless otherwise mutually agreed by the parties; and c) any judgment on the award rendered by the arbitrator may be entered in any court of competent jurisdiction. Notwithstanding the foregoing, we may seek injunctive or other equitable relief to protect its intellectual property rights in any court of competent jurisdiction.</p>
                                    
                                 </div>
                                 
                                 <div class="col-lg-12">
                                        <h3>Other                                   </h3>
                                    </div>
				 				<div class="col-lg-12">
                                     <p>.  This Agreement constitutes the entire agreement between you and us regarding the use of the Services. Our failure to exercise or enforce any right or provision of this Agreement shall not operate as a waiver of such right or provision. The section titles in this Agreement are for convenience only and have no legal or contractual effect; as used in the Agreement, the word “including” means “including but not limited to”. Please contact us with any questions regarding this Agreement by emailing us at : SERVICES@ACCOUNTSWALE.IN</p>
                                    
                                 </div>
                                 
                                 <div class="col-lg-12">
                                        <h3>Copyright/Trademark Information                                   </h3>
                                    </div>
				 				<div class="col-lg-12">
                                     <p>Copyright ©2019, ACCOUNTSWALE.IN, All rights reserved. ACCOUNTSWALE  is a registered trademark of TECHTALENTS SOFTWARE TECHNOLOGIES PVT LTD. The trademarks, logos and service marks (“Marks”) displayed on the Services are our property or the property of other third parties. You are not permitted to use these Marks without our prior written consent or the consent of such third party which may own the Mark.</p>
                                    
                                 </div>
                                 
                                 <div class="col-lg-12">
                                        <h3>Job Posting</h3>
                                    </div>
				 				<div class="col-lg-12">
                                     <ul>
                                            <li>ACCOUNTSWALE.IN shall place the information relating to job vacancy in the Job opening section & such other job related sections on the website ACCOUNTSWALE.IN or sites which we have collaboration with or/and as ACCOUNTSWALE.IN may deem fit and proper but such additional web hosting shall be without any extra cost to the employer/consultant/HR. ACCOUNTSWALE.IN has premium job listing features in some top websites and this may be limited to only hot job posting.</li>

                                            <li>The insertion so displayed in the job posting section of ACCOUNTSWALE.IN shall be for a fixed period (currently 60 days),which period is subject to change without notice. Every instance of posting a new job gives you an additional fixed period (currently 30 days) starting from the date on which the listing is refreshed and shall be counted as fresh posting and no duplicate job shall be allowed under this.</li>
                                            
                                            <li>ACCOUNTSWALE.IN reserves its right to reject any job posting provided by the subscriber without assigning any reason either before uploading or after uploading the vacancy details, but in such an eventuality, any amount so paid for, may be refunded to the subscriber on a pro-rata basis at the sole discretion of ACCOUNTSWALE.IN.</li>
                                            
                                            <li>By posting/uploading a job posting on the website you confirm that you have obtained all licenses/permits as are necessary for recruitment and ACCOUNTSWALE.IN shall not be held responsible against any claims, damages arising out of actions/claims that may be made in relation to the same.</li>
                                            
                                            <li>ACCOUNTSWALE.IN has the right to make all such modifications/editing of the vacancy details in order to fit its database.</li>
                                            
                                            <li>The Subscriber/Recruiter will provide minimum of 1 email id for vacancies posted on ACCOUNTSWALE.IN in the job section to collect response(s) if any. The contact information given by the subscriber for all listing should be the same and the subscriber cannot give multiple contact information/data for the purpose of listing. Also, we don’t allow posting of personal information like email-id or phone/mobile no. on the job display page. You can post such information by choosing Walk-in option while making the purchase.</li>
                                            
                                            <li>All information intimated by the subscriber / recruiter and displayed by ACCOUNTSWALE.IN on ACCOUNTSWALE.IN becomes public knowledge and ACCOUNTSWALE.IN may at its sole discretion include the vacancy intimated by a subscriber for display on ACCOUNTSWALE.IN in any other media including the print media at no extra costs to the subscriber and ACCOUNTSWALE.IN shall not be held liable for usage/publicity of such information.</li>
                                            
                                            <li>ACCOUNTSWALE.IN offers neither guarantee nor warranties that there would be a satisfactory response or any at all response once the job is put on display. However, we do provide a paid package under “Hot Job posting” which entitles the user to a guarantee of 4 days hiring. Decision to refund the amount will be under sole discretion of ACCOUNTSWALE.IN and will be based upon following of the set conditions by the user/subscriber/HR/employer.</li>
                                            
                                            <li>ACCOUNTSWALE.IN shall in no way be held liable for any information received by the subscriber and it shall be the sole responsibility of the subscriber to check, authenticate and verify the information/response received at its own cost and expense.</li>
                                            
                                            <li>ACCOUNTSWALE.IN would not be held liable for any loss of data, technical or otherwise, information, particulars supplied by the subscriber, due the reasons beyond its control like corruption of data or delay or failure to perform as a result of any causes or conditions that are beyond ACCOUNTSWALE.IN's reasonable control including but not limited to strike, riots, civil unrest, Govt. policies, tampering of data by unauthorized persons like hackers, war and natural calamities.</li>
                                            
                                            <li>ACCOUNTSWALE.IN will commence providing services only upon receipt of amount/charges upfront either from the subscriber or from a third party on behalf of the subscriber.</li>
                                            
                                            <li>The subscriber/Recruiter shall be deemed to give an undertaking to ACCOUNTSWALE.IN that the jobs sought to be advertised on the classified section of ACCOUNTSWALE.IN are in existence, are genuine and that the subscriber / recruiter has the authority to advertise for such jobs. Posting a job on ACCOUNTSWALE.IN automatically activates this undertaking.</li>
                                            
                                            <li>The subscriber/Recruiter must give an undertaking to ACCOUNTSWALE.IN that there will be no fee charged from any person who responds to jobs advertised on the classified section of ACCOUNTSWALE.IN for processing of applications / responses from such person. Posting a job on ACCOUNTSWALE.IN automatically activates this undertaking.</li>
                                            
                                            <li>ACCOUNTSWALE.IN reserves its right to change the look, feel, design, prominence, depiction, classification of the job posting section of ACCOUNTSWALE.IN at any time without assigning any reason and without giving any notice.</li>
                                            
                                            <li>The subscriber shall be assigned password (s) by ACCOUNTSWALE.IN to enable the subscriber to post vacancies on the site in agreed upon section(s),but the sole responsibility of the safe custody of the password shall be that of the subscriber and ACCOUNTSWALE.IN shall not be responsible for data loss/theft of data/corruption of data or the wrong usage/misuse of the password and any damage or leak of information and its consequential usage by a third party. ACCOUNTSWALE.IN undertakes to take all reasonable precautions at its end to ensure that there is no leakage/misuse of the password granted to the subscriber.</li>
                                            
                                            <li>The User of these services does not claim any copyright or other Intellectual Property Right over the data uploaded by him/her on the website</li>
                                            
                                            <li>All disputes arising out of the transactions between a user and ACCOUNTSWALE.IN will be resolved in accordance with the laws of India as applicable.</li>
                                            
                                            <li>All Disputes arising out of the transactions between a user and ACCOUNTSWALE.IN will be subject to the jurisdiction of Courts situated in BANGALORE/Bangalore only.</li>
                                            
                                            <li>The subscription is neither re-saleable nor transferable by the subscriber to any other person, corporate body, firm or individual</li>
                                     </ul>
                                    
                                 </div>
                                 
                                 <div class="col-lg-12">
                                        <h3>Database Package services</h3>
                                    </div>
				 				<div class="col-lg-12">
                                     <ul>
                                            <li>The subscriber availing this service shall be deemed to have consented to be bound by all the applicable terms and conditions of this service.</li>

                                            <li>Decision of ACCOUNTSWALE.IN regarding all transactions under this service shall be final and binding and no correspondence shall be entertained in this regard.</li>

                                            <li>ACCOUNTSWALE.IN reserves the right to extend, cancel, discontinue, prematurely withdraw, change, alter or modify this service or any part thereof including charges, at its sole discretion at anytime as may be required in view of business exigencies and/or regulatory or statutory changes.</li>

                                            <li>The membership is for your personal use only. You cannot transfer, assign or authorize your membership to any other person outside your company.</li>

                                            <li>You will not post or transmit any content that is abusive, obscene, sexually oriented or against national interest to the candidates. ACCOUNTSWALE.IN reserves the right to suspend your profile if any prohibitive or objectionable content is found and may further initiate appropriate legal proceedings against you.</li>

                                            <li>ACCOUNTSWALE.IN reserves the right to modify/delete the profile contents at its own discretion without prior notice if the contents of profile are deemed unfit for broadcast.</li>

                                            <li>ACCOUNTSWALE.IN is not responsible for authenticity of the content arising thereto.</li>

                                            <li>The users specifically note and agree that the content and service or part thereof may be varied, added, withdrawn, withheld or suspended by ACCOUNTSWALE.IN at its sole discretion without prior notice to the users.</li>

                                            <li>ACCOUNTSWALE.IN shall not be liable for any costs, loss or damage (whether direct or indirect),or for loss of revenue, loss of profits or any consequential loss whatsoever as a result of the user using the Service.</li>

                                            <li>No reversal of deducted charges shall be allowed under any circumstances.</li>

                                            <li>You are bound by the terms and conditions as mentioned herein and as stated on the site.</li>

                                            <li>ACCOUNTSWALE.IN and/or its respective suppliers make no representations about the suitability, reliability, availability, timeliness, lack of viruses or other harmful components and accuracy of the information, software, products, services and related graphics contained within the, ACCOUNTSWALE.IN sites/services for any purpose. All such information, software, products, services and related graphics are provided "as is" without warranty of any kind. ACCOUNTSWALE.IN and/or its respective suppliers hereby disclaim all warranties and conditions with regard to this information, software, products, services and related graphics, including all implied warranties and conditions of merchantability, fitness for a particular purpose, workmanlike effort, title and non-infringement. ACCOUNTSWALE.IN shall not be responsible or liable for any consequential damages arising thereto.</li>

                                            <li>By agreeing to register at ACCOUNTSWALE.IN, a user allows ACCOUNTSWALE.IN to get in touch with him/her from time to time on events or offers regarding job posting or resume download packages. This can include exciting offers, information, as well as promotions.</li>

                                            <li>Registration presumes that the users have read, understood and accepted the terms and conditions.</li>

                                            <li>This service is live in India only.</li>

                                            <li>All information, data, text, video, messages or other materials, whether publicly or privately transmitted / posted, is the sole responsibility of the person from where such content originated (the Originator).</li>
                                     </ul>
                                    
                                 </div>
                                 
                                 <div class="col-lg-12">
                                        <h3>Charges                                   </h3>
                                    </div>
				 				<div class="col-lg-12">
                                     <p>This Site reserves the right to charge subscription and / or membership fees in respect of any part, aspect of this Site upon reasonable prior notice. ACCOUNTSWALE.IN reserves its right to terminate your account without any prior notice for any violation of the Terms of Use. All content posted by you becomes the property of ACCOUNTSWALE.IN and you agree to grant/assign the royalty free, perpetual right to use, reproduce, modify, adapt, publish, translate, create derivative works from, distribute, perform and display such content (in whole or part) worldwide and/or to incorporate it in other works in any form, media, or technology now known or later developed.</p>
                                    
                                 </div>
                                 
                                 <div class="col-lg-12">
                                        <h3>Disclaimer of Warranties and Liability                                   </h3>
                                    </div>
				 				<div class="col-lg-12">
                                     <p>All the contents of this Site are only for general information or use. They do not constitute advice and should not be relied upon in making (or refraining from making) any decision. Any specific advice or replies to queries in any part of the Site is/are the personal opinion of such experts/consultants/persons and are not subscribed to by this Site. The information from or through this site is provided on "AS IS" basis, and all warranties, expressed or implied of any kind, regarding any matter pertaining to any goods, service or channel, including without limitation, the implied warranties of merchantability, fitness for a particular purpose, and non-infringement are disclaimed and excluded. Certain links on the Site lead to resources located on servers maintained by third parties over whom ACCOUNTSWALE.IN has no control or connection, business or otherwise as these sites are external to ACCOUNTSWALE.IN you agree and understand that by visiting such sites you are beyond the ACCOUNTSWALE.IN website. ACCOUNTSWALE.IN therefore neither endorses nor offers any judgement or warranty and accepts no responsibility or liability for the authenticity/availability of any of the goods/services/or for any damage, loss or harm, direct or consequential or any violation of local or international laws that may be incurred by your visit and/or transaction/s on these sites.</p>
                                    
                                 </div>
                                 
                                 <div class="col-lg-12">
                                        <h3>Advertising Material                                   </h3>
                                    </div>
				 				<div class="col-lg-12">
                                     <p>Part of the Site contains advertising/other material submitted to ACCOUNTSWALE.IN by third parties. Responsibility for ensuring that material submitted for inclusion on the Site complies with applicable International and National law is exclusively on the advertisers and ACCOUNTSWALE.IN will not be responsible for any claim, error, omission or inaccuracy in advertising material. ACCOUNTSWALE.IN reserves the right to omit, suspend or change the position of any advertising material submitted for insertion. Acceptance of advertisements on the Site will be subject to ACCOUNTSWALE.IN terms and conditions which are available on request.</p>
                                    
                                 </div>
                                 
                                 <div class="col-lg-12">
                                        <h3>Manual Shortlisting                                   </h3>
                                    </div>
				 				<div class="col-lg-12">
                                     <ul>
                                           <li> ACCOUNTSWALE.IN agrees to provide the service to the subscriber only for the duration and the number of vacancies contracted for, to the best of its ability and ACCOUNTSWALE.IN shall in no way be held liable for any information received by the subscriber and it shall be the sole responsibility of the subscriber to check, authenticate and verify the information/response received at its own cost and expense.</li>

                                           <li>ACCOUNTSWALE.IN will make best efforts to use the parameters provided by the subscriber to short list, but takes no responsibility for the accuracy of the short listing based on the parameters for selection as specified by the subscriber</li>
                                            
                                           <li>ACCOUNTSWALE.IN would not be held liable for any loss of data technical or otherwise, information, particulars supplied by the customers due to the reasons beyond its control like corruption of data or delay or failure to perform as a result of any causes or conditions that are beyond ACCOUNTSWALE.IN's reasonable control including but not limited to strike, riots, civil unrest, Govt. policies, tampering of data by unauthorized persons like hackers, war and natural calamities</li>
                                            
                                           <li>ACCOUNTSWALE.IN will commence providing services only upon receipt of amount/charges upfront either from the subscriber or from a third party on behalf of the subscriber. The payment for service once subscribed to by the subscriber is not refundable and any amount paid shall stand appropriated.</li>
                                            
                                           <li>This subscription is neither re-saleable nor transferable by the subscriber to any other person, corporate body, firm or individual.</li>
                                            
                                           <li>The User of these services does not claim any copyright or other Intellectual Property Right over the data uploaded by him/her on the website.</li>
                                            
                                           <li>It is the responsibility of Job Seekers to verify the correctness and authenticity of the claims made by recruiters. We merely act as facilitators and do not as agents for any recruiter.</li>
                                            
                                           <li>Payments to recruiters are not advised and shall be at your own risk.</li>
                                            
                                           <li>All disputes arising out of the transactions between a user and ACCOUNTSWALE.IN will be resolved in accordance with the laws of India as applicable.</li>
                                            
                                           <li>All Disputes arising out of the transactions between a user and ACCOUNTSWALE.IN will be subject to the jurisdiction of Courts situated in Bangalore/BANGALORE alone.</li>
                                            
                                           <li>ACCOUNTSWALE.IN at a prorata basis to the subscriber at its discretion. 2A. The subscriber shall be entitled to 1 user name / password to access the database services alone and additional user names / passwords may be provided by ACCOUNTSWALE.IN on such terms and conditions as may be mutually agreed upon.</li>
                                            
                                           <li>ACCOUNTSWALE.IN offers no guarantee nor warranties that there would be a satisfactory response or any response at all subscriber for applications received using the database package.</li>
                                            
                                           <li>ACCOUNTSWALE.IN shall in no way be held liable for any information received by the subscriber and it shall be the sole responsibility of the subscriber to check, authenticate and verify the information/response received at its own cost and expense.</li>
                                            
                                           <li>Any actions taken by an employer/recruiter on the basis of the background check report or otherwise, is the employer's/recruiter's responsibility alone and ACCOUNTSWALE.IN will not be liable in any manner for the consequences of such action taken by the user.</li>
                                            
                                           <li>ACCOUNTSWALE.IN would not be held liable for any loss of data technical or otherwise, information, particulars supplied by the customers due to the reasons beyond its control like corruption of data or delay or failure to perform as a result of any causes or conditions that are beyond ACCOUNTSWALE.IN's reasonable control including but not limited to strike, riots, civil unrest, Govt. policies, tampering of data by unauthorized persons like hackers, war and natural calamities.</li>
                                            
                                           <li>ACCOUNTSWALE.IN will commence providing services only upon receipt of amount/charges upfront either from the subscriber or from a third party on behalf of the subscriber.</li>
                                     </ul>
                                    
                                 </div>
                                 
                                 <div class="col-lg-12">
                                        <h3>The subscriber/Recruiter                                   </h3>
                                    </div>
				 				<div class="col-lg-12">
                                    <ul>
                                            <li>The user shall give an undertaking to ACCOUNTSWALE.IN that the jobs sought to be filled through ACCOUNTSWALE.IN are in existence, genuine and the subscriber has the authority to recruit /advertise for such vacancies. Also the subscriber undertakes that the database will be used to contact candidates for jobs only.</li>

                                            <li> The user shall ensure compliance with all applicable laws for the protection of the personal details of the users whose profiles are accessed by them through ACCOUNTSWALE.IN including but not limited to compliance the Telecom Commercial Communications Customer Preference Regulations, 2010 as also rules, regulations, guidelines, bye laws and notifications made there under, while accessing or feeding any resume/ insertion or information/data into the computers, computer systems or computer networks of ACCOUNTSWALE.IN. Any unsolicited calls or emails sent to a candidate outside the use for Job openings will be taken as a direct reason for termination of services and no refund will be made under such conditions.</li>
                                            
                                            <li> ACCOUNTSWALE.IN reserves its right to change the look, feel, design, prominence, depiction, classification of any section of ACCOUNTSWALE.IN at any time without assigning any reason and without giving any notice.</li>
                                            
                                            <li> The subscriber/Recruiter must give an undertaking to ACCOUNTSWALE.IN that there will be no fee charged from any person who is contacted through our database package for processing of such person.</li>
                                            
                                            <li>This subscription is neither re-saleable nor transferable by the subscriber to any other person, corporate body, firm or individual concern</li>
                                            
                                            <li>The subscriber shall be assigned a password (s) by ACCOUNTSWALE.IN to enable the subscriber to access all the information received through the software, but the sole responsibility of the safe custody of the password shall be that of the subscriber and ACCOUNTSWALE.IN shall not be responsible for data loss/theft of data/corruption of data or the wrong usage/misuse of the password and any damage or leak of information and its consequential usage by a third party. ACCOUNTSWALE.IN undertakes to take all reasonable precautions at its end to ensure that there is no leakage/misuse of the password granted to the subscriber.</li>
                                            
                                            <li> The information on ACCOUNTSWALE.IN is for use by its subscribers alone and does not authorize the subscriber to download and use the data for commercial purposes. In case any one is found to be in violation of this then ACCOUNTSWALE.IN at its discretion may suspend its service/subscription and also may take such action as it may be advised. No refund will be processed under this case and the account will be terminated after due investigation.</li>
                                            
                                            <li> The subscriber shall not use / circulate / forward a person's resume hosted on the ACCOUNTSWALE.IN Network / Resumes to his / her current employer as mentioned by the person in his / her resume</li>
                                            
                                            <li>All disputes arising out of the transactions between a user and ACCOUNTSWALE.IN will be resolved in accordance with the laws of India as applicable.</li>
                                            
                                            <li> All Disputes arising out of the transactions between a user and ACCOUNTSWALE.IN will be subject to the jurisdiction of Courts situate in BANGALORE only.</li>
                                    </ul>
                                    
                                 </div>
                                 
                                 <div class="col-lg-12">
                                        <h3>ANTI SPAM POLICY                                   </h3>
                                    </div>
				 				<div class="col-lg-12">
                                     <p>The use and access to database package is subject to this policy. The services provided to you are aimed at providing recruitment solutions and should be restricted to contacting suitable candidates for genuine jobs in existence. Mailing practices such as transmitting marketing and promotional mailers/ Offensive messages/ messages with misleading subject lines in order to intentionally obfuscate the original message, are strictly prohibited. We reserve the right to terminate services, without prior notice, to the originator of Spam. No refund shall be admissible under such circumstances.</p>
                                    
                                     <ul>
                                         <li>Unsolicited Bulk Messages/Unsolicited Commercial Messages.</li>
                                         <li>Non Job related mails.</li>
                                         <li>Messages with misleading subject lines.</li>
                                         <li>Blank Messages.</li>
                                         <li>Extra ordinary High Number of mails.</li>
                                         <li>Mails soliciting payments.</li>
                                         <li>Misleading/Fraudulent mails.</li>
                                         <li>Calling candidates from ACCOUNTSWALE.IN Database soliciting payments.</li>
                                         <li>Charging candidates by giving fake Job offers.</li>
                                     </ul>


                                     <p>Users agree to indemnify and hold harmless ACCOUNTSWALE.IN from any damages or claims arising out of usage of their database package accounts for transmitting spam Users are advised to change their passwords frequently in order to reduce the possibility of misuse of their accounts.</p>

                                            <p>Note: The terms in this agreement may be changed by ACCOUNTSWALE.IN at any time. ACCOUNTSWALE.IN is free to offer its services to any client/prospective client without restriction.

                                                </p>
                                 </div>
                                 
                                 

				 			</div>
				 		</div>
				 	</div>
				 </div>
			</div>
		</div>
	</section>
@endsection