@extends('UI.base')

@section('Content')
<section>
    <div class="block no-padding">
        <div class="container fluid">
            <div class="row">
                <div class="col-lg-12">
                    <div class="main-featured-sec style2 mobile-height-sec">
                        <ul class="main-slider-sec style2 text-arrows home-img candidate-dashboard-banner-height mobile-height-slider">
                            <li class="slideHome"><img src="{{URL::asset('Demo/images/resource/mslider3.jpg')}}" alt="" /></li>
                        </ul>
                        
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section id="scroll-here" class="bg-grey">
    <div class="block">
        <div class="container">
            <div class="row candidate-row-mar-top">
                <div class="col-lg-4 col-md-6">
					<div class="reviews-sec candiates-card-profile-top">
						<div class="col-lg-12">
							<div class="reviews candidates-profile-card-border">
								<div class="col-md-3 pull-left">
										<div class="progress mx-auto" data-value='{{$CandidateProfile->profile_completion}}'>
											<span class="progress-left">
															<span class="progress-bar border-primary"></span>
											</span>
											<span class="progress-right">
															<span class="progress-bar border-primary"></span>
											</span>
											<div class="progress-value w-100 h-100 rounded-circle d-flex align-items-center justify-content-center">
												@if($CandidateProfile->profile_pic)
													<img class="candidate-profile-img" src="{{URL::asset('candidate_profile/')}}/{{$CandidateProfile->profile_pic}}" alt="" />
												@else
													<img class="candidate-profile-img" src="{{URL::asset('UI/images/resource/r1.jpg')}}" alt="" />
												@endif
											</div>
											<h5 class="profile-completion-percentage">{{$CandidateProfile->profile_completion}}%</h5>
											<p class="profile-completion-p">Profile Completion</p>
										</div>
				
									{{-- <div class="progress blue">
										<span class="progress-left">
											<span class="progress-bar"></span>
										</span>
										<span class="progress-right">
											<span class="progress-bar"></span>
										</span>
										<div class="progress-value">
										@if($CandidateProfile->profile_pic)
											<img class="candidate-profile-img" src="{{URL::asset('candidate_profile/')}}/{{$CandidateProfile->profile_pic}}" alt="" />
										@else
											<img class="candidate-profile-img" src="{{URL::asset('UI/images/resource/r1.jpg')}}" alt="" />
										@endif
										</div>
										<h5 class="profile-completion-percentage">90%</h5>
										<p class="profile-completion-p">Profile Completion</p>
									</div> --}}
								</div>
				
								<div class="col-md-9 pull-left mobile-profile-center">
									<h3 class="mobile-review-head">{{$Profile->name}} </h3>
									<span class="candidate-profile-position">
											@if($GetExperiencePosition)
											{{$GetExperiencePosition->name}}
											@else
											Position
											@endif
									</span>
									<br>
									<span class="candidate-profile-position">
											@if($GetExperiencePosition)
											{{$GetExperiencePosition->CompanyName}}
											@else
											Company name
											@endif    
									</span>  
									<br>   
									<a href="/Candidate/Profile" class="profile-complete-link">Complete Profile Now</a>
								</div>
							</div>
						</div>
					</div>
							
                </div>
                <div class="col-lg-8 col-md-6 recommended-jobs-list">
                    <div class="job-search-sec candidate-job-search-sec-width">
                        <div class="job-search style2">
                            <div class="search-job2 caniddate-dashboard-job-search">	
                                <form class="home-search-box">
                                    <div class="row no-gape tab-view-search-top">
                                        <div class="col-lg-6 col-md-6 col-sm-4 col-xs-6">
                                            <div class="job-field">
                                                <input type="text" class="candidate-search-input-border-radius" placeholder="Skill, Designation" id="SearchJobsTitleOrCompanyName"/>
                                            </div>
                                        </div>
                                        <div class="col-lg-3 col-md-3 col-sm-4">
                                            <div class="job-field">
                                                <select data-placeholder="Any category" class="chosen-city" id="city">
                                                    <option selected disabled>Location</option>
                                                    <option>Bangalore</option>
                                                    <option>Mumbai</option>
                                                    <option>Delhi</option>
                                                    <option>Pune</option>
                                                    <option>Hyderabad</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-lg-3  col-md-3 col-sm-4">
                                            <button type="button" id="SearchJobs" class="home-search-btn"><img src="images/custom/search.png" alt=""> SEARCH</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div class="filterbar candidate-profile-filter-pad">
                        <div class="sortby-sec candidate-profile-sortby">
                            <select data-placeholder="20 Per Page" class="chosen">
                                <option selected disabled>Sort by</option>
                                <option>40 Per Page</option>
                                <option>50 Per Page</option>
                                <option>60 Per Page</option>
                            </select>
                        </div>
                        <h5><span id="JobsCount">{{count($GetAddedFavouriteJobs)}}</span> favourites jobs</h5>
                    </div>

                    <div class="job-list-modern">
                            <div class="job-listings-sec no-border text-center" id="SearchJobsResults">
                                @foreach($GetAddedFavouriteJobs as $Jobs)
                                <img id="AjaxLoader" class="jobs-search-loader" src="{{URL::asset('UI/ajax_loader.gif')}}" alt="" style="display:none;">
                               <div class="job-listing wtabs recommented-jobs">
                                   <div class="job-title-sec recommended-jobs-pad-left">
                                        <h3><a href="/JobDetails/{{$Jobs->CompanySlug}}/{{$Jobs->id}}/{{$Jobs->slug}}" target="_blank" title="">{{$Jobs->title}}</a></h3>
                                       <p class="recommended-jobs-company-name">{{$Jobs->company_name}}</p>
                                       <div class="job-lctn job-attributes-recommended-jobs"><i class="fa fa-briefcase"></i>{{$Jobs->experience}} Years</div>
                                       <div class="job-lctn job-attributes-recommended-jobs"><i class="fa fa-user-circle"></i>
                                            @if($Jobs->job_type == 1)
                                                Part time
                                            @else
                                                Full time
                                            @endif
                                        </div>
                                       <div class="job-lctn job-attributes-recommended-jobs"><i class="fa fa-map-marker" ></i> {{$Jobs->city}}</div>
                                       {{-- <div class="job-lctn job-attributes-recommended-jobs"><i class="fa fa-industry"></i>{{$Jobs->name}}</div> --}}
                                    </div>
                                <div class="recommended-jobs-description">{!!implode(' ', array_slice(explode(' ', $Jobs->job_description), 0, 20))!!}</div>
                                    <p class="recommended-jobs-posted-on">Posted on: 
                                            @php 
                                            $now = new DateTime;
$full = false;	
$ago = new DateTime($Jobs->created_at);
$diff = $now->diff($ago);

$diff->w = floor($diff->d / 7);
$diff->d -= $diff->w * 7;

$string = array(
'y' => 'year',
'm' => 'month',
'w' => 'week',
'd' => 'day',
'h' => 'hour',
'i' => 'minute',
's' => 'second',
);
foreach ($string as $k => &$v) {
if ($diff->$k) {
$v = $diff->$k . ' ' . $v . ($diff->$k > 1 ? 's' : '');
} else {
unset($string[$k]);
}
}

if (!$full) $string = array_slice($string, 0, 1);
echo $string ? implode(', ', $string) . ' ago' : 'just now';
                                        @endphp
                                    </p>
                               </div>
                               @endforeach
                           </div>
                        </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
