@extends('UI.base')

@section('Content')
<section id="scroll-here" class="bg-grey">
    <div class="block">
        <div class="container">
            <div class="row candidate-row-mar-top">
                <div class="col-lg-12 col-md-6">
                    <div class="reviews-sec candiates-card-profile-top">
						<div class="row job-details-breadcrumb">
							<div class="col-lg-12 pad-top30">
								<p class="job-details-breadcrumb-p"><i class="fa fa-home clr-primary"></i> <i class="fa fa-caret-right"></i> Change password</p>
							</div>
						</div>


						<div class="reviews-sec applied-jobs-list">
							<div class="row">
									<div class="col-lg-12">
										<div class="reviews candidates-profile-card-border mar-top15 mobile-card-pad-right">
											<div class="row">
												<div class="col-md-12">
													<h5 class="text-center">Change Password</h5>
													<div class="col-md-12 change-password-column">
														<form class="mar-top15">
															{{-- <label class="pull-left remove-label">Name
															</label> --}}
															<div class="cfield">
																<input type="password" class="bg-white input-border" placeholder="Old Password" value="" id="old_password"/>
															</div>

															<div class="cfield">
																<input type="password" class="bg-white input-border" placeholder="New Password" value="" id="new_password"/>
															</div>

															<div class="cfield">
																<input type="password" class="bg-white input-border" placeholder="Confirm Password" value="" id="verify_password"/>
															</div>

															<button type="button" class="candidate-profile-save-btn col-md-6 col-xs-6 update-password-btn" id="UpdateEmployerPassword">Update
																</button>
														</form>
													</div>
												</div>
												
											</div>
										</div>
									</div>
							</div>
						</div>
                    </div>
                    
                </div>
            </div>
        </div>
    </div>
</section>

{{--  Post New Jobs  --}}
<div class="account-popup-area post-jobs-popup-box">
	<div class="account-popup post_new_jobs_popup">
		<span class="close-popup" id="register_close"><i class="la la-close"></i></span>
		<h3>Post a New Job</h3>
		<form class="mar-top15">
				<div class="row">
						<div class="dropdown-field col-md-12 pad-left0 pad-right0 mar-right10">
							<label class="pull-left remove-label">Job Title *
								</label>
							{{-- <div class="cfield">
								<input type="text" placeholder="UI Designer" value="" class="bg-white " id="title"/>
							</div> --}}
							<input type="hidden" name="_token" value="{{ csrf_token() }}">

							<select  data-placeholder="Please Select Functional Areas" class="chosen" id="title">
								<option selected>Select job title</option>
								@foreach($GetJobTitle as $Title)
							<option value="{{$Title->id}}">{{$Title->name}}
								</option>
								@endforeach
							</select>
						</div>
					</div>

			<div class="row">
				<div class="dropdown-field col-md-5 pad-left0 pad-right0 mar-right10">
					<label class="pull-left remove-label">Employee Type *
					</label>
					<select data-placeholder="Please Select Industries" class="chosen" id="employee_type" onchange="CheckEmployeeType()">
						<option selected>Select Type
							</option>
						<option value="1">Fresher
						</option>
						<option value="2">Experienced
							</option>
					</select>
				</div>

				<div class="dropdown-field col-md-5 pad-left0 pad-right0">
					<label class="pull-left remove-label">Job Type *
						</label>
					<select  data-placeholder="Please Select Functional Areas" class="chosen" id="job_type">
						<option selected>Select job type</option>
						<option value="1">Part time
						</option>
						<option value="2">Full time
							</option>
					</select>
				</div>
			</div>
			
			

			<div class="row">
				<div class="col-md-12 pad-left0 pad-right0 mar-right10">
					<label class="pull-left remove-label">Job Description
						</label>
					<div class="cfield">
						<textarea name="" id="description" class="bg-white"></textarea>
					</div>
				</div>
			</div>
			
			<div class="row">
				<div class="dropdown-field col-md-12 pad-left0 pad-right0 mar-right10">
					<label class="pull-left remove-label">Qualification
						</label>
					<div class="cfield">
						<input type="text" placeholder="Qualification" value="" class="bg-white" id="qualification"/>
					</div>

					{{--  <select  data-placeholder="Please Select Functional Areas" class="chosen" id="qualification">
						<option selected>Select qualification</option>
						@foreach($GetQualification as $Qualification)
					<option value="{{$Qualification->id}}">{{$Qualification->qualification}}
						</option>
						@endforeach
					</select>  --}}
				</div>
			</div>

			<h4 class="location-head">Location --------------------------------------------------</h4>
			<div id="GetAddress">
				
				<div class="row">
					<div class="col-md-4 pad-left0 pad-right0 mar-right5">
						<label class="pull-left remove-label">Zip Code
							</label>
						<div class="cfield">
							<input type="text" placeholder="Zipcode" value="" class="bg-white" onchange="CheckZipCode()" data-geocomplete="zip code" id="zipcode"/>
						</div>
					</div>
	
					<div class="col-md-7 pad-left0 pad-right0">
						<label class="pull-left remove-label">Country
							</label>
						<div class="cfield">
							<input type="text" placeholder="Country" value="" class="bg-white " data-geocomplete="county" id="country"/>
						</div>
					</div>
				</div>
	
				<div class="row">
					<div class="col-md-5 pad-left0 pad-right0 mar-right5">
						<label class="pull-left remove-label">State
							</label>
						<div class="cfield">
							<input type="text" data-geocomplete="state" placeholder="State" value="" class="bg-white " id="state"/>
						</div>
					</div>
	
					<div class="col-md-5 pad-left0 pad-right0">
						<label class="pull-left remove-label">City
							</label>
						<div class="cfield">
							<input type="text" placeholder="City" value="" class="bg-white" data-geocomplete="city" id="city"/>
						</div>
					</div>
				</div>
			</div>
			
			<div class="row">
				<div class="col-md-12 pad-left0">
					<label class="pull-left remove-label">Area
						</label>
					<div class="cfield">
						<input type="text" data-geocomplete="street address"  placeholder="Type Area" value="" class="bg-white " id="address"/>
					</div>
				</div>
				{{--  <div id="myForm">
					<input id="txtAddress1" data-geocomplete="street address" />
				</div>  --}}
			</div>
				
			<h4 class="location-head">Salary and Experience -----------------------</h4>

			<div class="row">
					<div class="col-md-4 pad-left0 pad-right0 salary-column-width">
							<label class="pull-left remove-label">Salary Type *
								</label>
							<select  data-placeholder="Please Select Functional Areas" class="chosen" id="salary_type" onchange="SalaryType()">
								<option value="1">Monthly
								</option>
								<option value="2">Yearly
								</option>
							</select>
						</div>
				<div class="col-md-4 pad-left0 pad-right0 salary-column-width">
					<label class="pull-left remove-label">Salary Min
						</label>
					<div class="cfield">
						<input type="text" placeholder="Salary Min" value="" class="bg-white " id="salary_min"/>
					</div>
				</div>

				<div class="col-md-4 pad-left0 pad-right0 salary-column-width">
					<label class="pull-left remove-label">Salary Max
						</label>
					<div class="cfield">
						<input type="text" placeholder="Salary Max" value="" class="bg-white" id="salary_max"/>
					</div>
				</div>
				
			</div>

			<div class="row">
				<div class="col-md-7 pad-left0 pad-right0 mar-right5" id="CheckExperienced">
					<label class="pull-left remove-label">Experience (Years)
						</label>
					<div class="cfield">
						<input type="text" placeholder="Experience" value="" class="bg-white " id="experience"/>
					</div>
				</div>

				<div class="col-md-4 pad-left0 pad-right0">
					<label class="pull-left remove-label">No Of Position
						</label>
					<div class="cfield">
						<input type="text" placeholder="Position" value="" class="bg-white " id="positions"/>
					</div>
				</div>
			</div>

			<div class="row">
				<div class="col-md-7">
					<button type="button" class="btn btn-default candidate-profile-cancel-btn text-right close-popup" data-dismiss="modal">Close
					</button>
				</div>
				<div class="col-md-5">
					<button type="button" id="PostJobs" class="btn btn-default candidate-profile-save-btn">Save
						</button>
				</div>

			</div>
		  </form>
	</div>
</div>
<!-- Post New Jobs POPUP -->

{{--  Post Trainings  --}}
<div class="account-popup-area post-trainings-popup-box">
    <div class="account-popup post_new_jobs_popup">
        <span class="close-popup" id="register_close"><i class="la la-close"></i></span>
        <h3>Post Internships</h3>
        <form class="mar-top15">
            
            <div class="row">
                <div class="dropdown-field col-md-12 pad-left0 pad-right0 mar-right10">
                    <label class="pull-left remove-label">Training Name *
                        </label>
                    {{-- <div class="cfield">
                        <input type="text" placeholder="Training Name" value="" class="bg-white " id="training_title"/>
                    </div> --}}
                    <select  data-placeholder="Please Select Functional Areas" class="chosen" id="training_title">
                        <option selected>Select training title
                        </option>
                        @foreach($GetTrainingTitle as $Title)
                            <option value="{{$Title->id}}">{{$Title->title}}
                            </option>
                        @endforeach
                    </select>
                </div>
            </div>

            <div class="row">
                <div class="col-md-5 pad-left0 pad-right0 mar-right5">
                    <label class="pull-left remove-label">Start Date * 
                        </label>
                    <div class="cfield">
                        <input type="date" placeholder="Start Date" value="" class="bg-white" name="start_date" id="start_date"/>
                    </div>
                </div>

                <div class="col-md-3 pad-left0 pad-right0 training-duration-marright">
                    <label class="pull-left remove-label">Duration *
                    </label>
                    <select  data-placeholder="Please select days" class="dropdown-custom-clr-pad duration_days" id="duration_days">
                        {{-- <option value="1">30
                        </option>
                        <option value="2">60
                        </option> --}}
                    </select>
                </div>

                <div class="col-md-3 pad-left0 pad-right0">
                    <label class="pull-left remove-label">
                    </label>
                    <select  data-placeholder="Please Select Functional Areas" class="chosen" id="duration">
                        <option value="Days">Days
                        </option>
                        <option value="Months">Months
                        </option>
                    </select>
                </div>
            </div>

            <div class="row">
                <div class="col-md-5 pad-left0 pad-right0 mar-right5">
                    <label class="pull-left remove-label">Monthly Stipend * 
                        </label>
                     <div class="cfield">
                        <input type="text" placeholder="Monthly stipend" value="" class="bg-white " id="stipend" maxlength="6"/>
                    </div> 
                </div>

                <div class="col-md-5 pad-left0 pad-right0">
                    <label class="pull-left remove-label">Last Date of application *
                        </label>
                    <div class="cfield">
                        <input type="date" placeholder="Last date of application" value="" class="bg-white"  id="last_date"/>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12 pad-left0 pad-right0 mar-right10">
                    <label class="pull-left remove-label">Internship Description
                        </label>
                    <div class="cfield">
                        <textarea name="" id="training_description" class="bg-white"></textarea>
                    </div>
                </div>
            </div>

           
            <div class="row">
                <div class="col-md-12 pad-left0 pad-right0 mar-right5">
                    <label class="pull-left remove-label">What trainee will get after Internship completion *
                    </label>
                    <select  data-placeholder="Please Select Functional Areas" class="chosen" id="completion">
                        <option selected>Select type
                        </option>
                        <option value="1">Certification
                        </option>
                        <option value="2">Certification + Job Offers
                        </option>
                    </select>
                </div>
            </div>

            <div class="row">
                <div class="col-md-7">
                    <button type="button" class="btn btn-default candidate-profile-cancel-btn text-right close-popup" data-dismiss="modal">Close
                        </button>
                </div>
                <div class="col-md-5">
                    <button type="button" id="PostTrainings" class="btn btn-default candidate-profile-save-btn">Save
                        </button>
                </div>

            </div>
        </form>
    </div>
</div>
<!-- Post New Trainings POPUP -->



    {{--  Post CA Articleships  --}}
<div class="account-popup-area post-ca-jobs-popup-box">
		<div class="account-popup post_new_jobs_popup">
			<span class="close-popup" id="register_close"><i class="la la-close"></i></span>
			<h3>Post CA Articleships</h3>
			<form class="mar-top15">
				
                <div class="row">
                        <div class="col-md-12 pad-left0 pad-right0 mar-right10">
                            <label class="pull-left remove-label">Description
                                </label>
                            <div class="cfield">
                                <textarea name="" id="Ca_description" class="bg-white"></textarea>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="dropdown-field col-md-12 pad-left0 pad-right0 mar-right10">
                            <label class="pull-left remove-label">Qualification
                                </label>
                            <div class="cfield">
                                <input type="text" placeholder="Qualification" value="" class="bg-white" id="Ca_qualification"/>
                            </div>
    
                            {{--  <select  data-placeholder="Please Select Functional Areas" class="chosen" id="Ca_qualification">
                                <option selected>Select qualification</option>
                                @foreach($GetQualification as $Qualification)
                            <option value="{{$Qualification->id}}">{{$Qualification->qualification}}
                                </option>
                                @endforeach
                            </select>  --}}
                        </div>
                    </div>
                    
				<div class="row">
					<div class="col-md-5 pad-left0 pad-right0 mar-right5">
						<label class="pull-left remove-label">Monthly Stipend * 
							</label>
						<select  data-placeholder="Please Select Functional Areas" class="chosen" id="stipend_type" onchange="CheckStipend()">
                            <option value="1">ICAI norms</option>
                            <option value="2">Others</option>
                        </select>
					</div>
	
					{{-- <div class="col-md-5 pad-left0 pad-right0 mar-right5" id="ShowStipend">
                        <label class="pull-left remove-label">Monthly Stipend * 
                            </label>
                        
                        <div class="cfield">
                            <input type="text" placeholder="Monthly stipend" value="" class="bg-white " id="Ca_stipend"/>
                        </div> 
                    </div> --}}
                </div>
                
                <div id="SalaryShow" style="display:none;">
                    <h4 class="location-head">Salary and Experience -----------------------</h4>

                    <div class="row">
							<div class="col-md-4 pad-left0 pad-right0 salary-column-width">
									<label class="pull-left remove-label">Salary Type *
										</label>
									<select  data-placeholder="Please Select Functional Areas" class="chosen" id="Ca_salary_type">
										<option value="1">Monthly
										</option>
										<option value="2">Yearly
										</option>
									</select>
								</div>
                        <div class="col-md-4 pad-left0 pad-right0 salary-column-width">
                            <label class="pull-left remove-label">Salary Min
                                </label>
                            <div class="cfield">
                                <input type="text" placeholder="Salary Min" value="" class="bg-white " id="Ca_salary_min" maxlength="6"/>
                            </div>
                        </div>
        
                        <div class="col-md-4 pad-left0 pad-right0 salary-column-width">
                            <label class="pull-left remove-label">Salary Max
                                </label>
                            <div class="cfield">
                                <input type="text" placeholder="Salary Max" value="" class="bg-white " id="Ca_salary_max" maxlength="6"/>
                            </div>
                        </div>
                        
                    </div>
                </div>
                
                <h4 class="location-head">Location --------------------------------------------------</h4>

                    <div id="GetCAProfileAddress">
                        

                        <div class="row">
                            <div class="col-md-4 pad-left0 pad-right0 mar-right5">
                                <label class="pull-left remove-label">Zip Code
                                    </label>
                                <div class="cfield">
                                    <input type="text" data-geocomplete="zip code" placeholder="Zipcode" value="" onchange="CheckArticleshipZipCode()" class="bg-white " id="Ca_Edit_zipcode"/>
                                </div>
                            </div>
            
                            <div class="col-md-7 pad-left0 pad-right0">
                                <label class="pull-left remove-label">Country
                                    </label>
                                <div class="cfield">
                                    <input type="text" data-geocomplete="county" placeholder="Country" value="" class="bg-white " id="Ca_Edit_country"/>
                                </div>
                            </div>
                        </div>
            
                        <div class="row">
                            <div class="col-md-5 pad-left0 pad-right0 mar-right5">
                                <label class="pull-left remove-label">State
                                    </label>
                                <div class="cfield">
                                    <input type="text" data-geocomplete="state" placeholder="State" value="" class="bg-white " id="Ca_Edit_state"/>
                                </div>
                            </div>
            
                            <div class="col-md-5 pad-left0 pad-right0">
                                <label class="pull-left remove-label">City
                                    </label>
                                <div class="cfield">
                                    <input type="text" data-geocomplete="city" placeholder="City" value="" class="bg-white " id="Ca_Edit_city"/>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12 pad-left0">
                                <label class="pull-left remove-label">Area
                                    </label>
                                <div class="cfield">
                                    <input type="text" data-geocomplete="street address"  placeholder="Type Area" value="" class="bg-white " id="Ca_Edit_address"/>
                                </div>
                            </div>
                            {{--  <div id="myForm">
                                <input id="txtAddress1" data-geocomplete="street address" />
                            </div>  --}}
                        </div>
                    </div>

				
	
			   
				<div class="row">
					<div class="col-md-6 pad-left0 pad-right0">
						<label class="pull-left remove-label">No Of Openings
							</label>
						<div class="cfield">
							<input type="text" placeholder="Position" value="" class="bg-white " id="Ca_positions"/>
						</div>
					</div>
				</div>
	
				<div class="row">
					<div class="col-md-7">
						<button type="button" class="btn btn-default candidate-profile-cancel-btn text-right close-popup" data-dismiss="modal">Close
                        </button>
					</div>
					<div class="col-md-5">
						<button type="button" id="PostCAJobs" class="btn btn-default candidate-profile-save-btn">Save
							</button>
					</div>
	
				</div>
			</form>
		</div>
	</div>
	<!-- Post New Trainings POPUP -->
@endsection
