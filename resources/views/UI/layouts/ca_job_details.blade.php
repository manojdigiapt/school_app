@extends('UI.base')


@section('Content')
@foreach($GetJobsBySlug as $Jobs)

<section id="scroll-here" class="bg-grey">
    <div class="block">
        <div class="container">
            <div class="row candidate-row-mar-top">
                <div class="col-lg-12 col-md-6">
                    <div class="reviews-sec candiates-card-profile-top">
						<div class="row job-details-breadcrumb">
							<div class="col-lg-12 pad-top30">
								<p class="job-details-breadcrumb-p"><i class="fa fa-home clr-primary"></i> <i class="fa fa-caret-right"></i> <a href="#" class="clr-primary">All jobs</a> <i class="fa fa-caret-right"></i> Ca Article</p>
							</div>
						</div>

						<div class="row">
							<div class="col-lg-12">
								<div class="reviews candidates-profile-card-border mar-top15 mobile-card-pad-right">
									<div class="row">
										<div class="col-md-1 col-xs-12 pull-left pad-left30">
											<img class="jobs-details-img" src="{{URL::asset('employer_profile')}}/{{$Jobs->profile_pic}}" alt="" />
										</div>
										
										<div class="col-md-7 pull-left pad-left30 mobile-job-details-align-center">
											<h3 class="mobile-review-head">
												Ca Article
											</h3>
											<span class="candidate-profile-position">
													{{$Jobs->company_name}}
											</span> 
											<br>
											@if($Jobs->experience)
											<p class="job-details-attributes"><i class="fa fa-briefcase"> 
												{{$Jobs->experience}} Years
											</i></p>
											@endif
		
											@if($Jobs->job_type)
											<p class="job-details-attributes"><i class="fa fa-briefcase"> 
												@if($Jobs->job_type == 1)
													Part time
												@else
													Full time
												@endif
											</i></p>
											@endif
		
											<p class="job-details-attributes width50"><i class="fa fa-map-marker"> {{$Jobs->city}}</i></p>
										</div>
		
										<div class="col-md-4 pull-left mobile-profile-center">
											@if(!Auth::guard('employer')->check())
											{{-- @if(!Auth::guard('employer')->check()) --}}
											<img id="ApplyShortlistBtnLoader" class="apply-jobs-loader" src="{{URL::asset('UI/otp_verify_loader.gif')}}" alt="" style="display:none;">
											
											

											<input type="hidden" value="{{$Jobs->id}}" id="JobsId">
											
											<p class="applied-successfully" id="AppliedSuccessfully" style="display:none;">Applied successfully</p>

											<img id="ApplyBtnLoader" class="apply-jobs-loader" src="{{URL::asset('UI/otp_verify_loader.gif')}}" alt="" style="display:none;">

											@if($CheckJobsApplied)
												<p class="applied-successfully pad-top10percentage" id="AppliedSuccessfully">Applied successfully</p>
											@else
												@if($CheckPercentage)
												<a href="javascript:void(0);" title="" id="ApplyJobs" class="for-employers-btn post-a-job-home col-md-8 text-center pull-right">APPLY</a>
												@else
												<a href="javascript:void(0);" title="" id="CheckPercentage" class="for-employers-btn post-a-job-home col-md-8 text-center pull-right">APPLY</a>
												@endif
											@endif

											@endif
											<p class="apply-linked-in">Apply with <a href="#" class="clr-primary">Linkedin</a></p>
										{{-- @endif --}}
										</div>
									</div>
									<div class="row job-attributes-row-bg">
										{{-- Job Attributes --}}
									<div class="col-md-9 pull-left">
											<p class="job-details-attributes job-attributes-width">@if($Jobs->stipend_type == 1)
													{{--  @php    
													   $num = $Jobs->stipend;
													   $units = ['', 'K', 'M'];
													   for($i = 0; $num>=1000;$i++){
														   $num /= 1000;
													   }
													   echo round($num, 1).$units[$i];
												   @endphp 
													   
													   / Month  --}}
		   
											   @else
											   <i class="fa fa-money"></i>  {{$Jobs->salary_min}} - {{$Jobs->salary_max}} / Month
											   @endif</p>
		
											<p class="job-details-attributes">Openings: {{$Jobs->total_positions}}</p>
		
											<p class="job-details-attributes job-attributes-width30">Posted on: 
													@php 
													$now = new DateTime;
$full = false;	
$ago = new DateTime($Jobs->created_at);
$diff = $now->diff($ago);

$diff->w = floor($diff->d / 7);
$diff->d -= $diff->w * 7;

$string = array(
'y' => 'year',
'm' => 'month',
'w' => 'week',
'd' => 'day',
'h' => 'hour',
'i' => 'minute',
's' => 'second',
);
foreach ($string as $k => &$v) {
if ($diff->$k) {
$v = $diff->$k . ' ' . $v . ($diff->$k > 1 ? 's' : '');
} else {
unset($string[$k]);
}
}

if (!$full) $string = array_slice($string, 0, 1);
echo $string ? implode(', ', $string) . ' ago' : 'just now';
												@endphp
											</p>

										<p class="job-details-attributes">Job Views: {{count($GetTotalViewsCount)}}</p>

										<p class="job-details-attributes">Job Applicant: 
											@if($CheckApplicantsCount)
												{{count($CheckApplicantsCount)}}
											@else
												0
											@endif
										</p>
										</div>
		
										<div class="col-md-3 pull-left mobile-profile-center apply-jobs-column">
											<p class="apply-jobs-share-p">Share:</p>
											<ul class="social candidate-profile-card-social-padtop apply-jobs-social-ul">
												<li class="social-fb apply-jobs-social-icon-width"> 
													<a href="#"> 
													<i class=" fa fa-facebook apply-jobs-social-i">   
													</i> 
													</a> 
												</li>
												<li class="social-twitter apply-jobs-social-icon-width"> 
													<a href="#"> 
													<i class="fa fa-twitter apply-jobs-social-i">   
													</i> 
													</a> 
												</li>
												<li class="social-instagram apply-jobs-social-icon-width"> 
													<a href="#"> 
													<i class="fa fa-instagram apply-jobs-social-i">   
													</i> 
													</a> 
												</li>
											</ul>
										</div>
									</div>
									
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-lg-12">
									<div class="reviews candidates-profile-card-border jobs-description-padding job-description-mar-top30">
										<div class="row job-description-p">
											<h3 class="job-description-h3">Job Description</h3>
											{!!$Jobs->job_description!!}
										</div>
		
										<div class="recent-jobs">
											<h3>Similar Jobs</h3>
											<div class="job-list-modern">
												@if($GetSimilarJobs)
												@foreach($GetSimilarJobs as $SimilarJobs)
												<div class="job-listings-sec no-border">
													<div class="job-listing wtabs">
														<div class="job-title-sec">
															<div class="c-logo"> <img src="{{URL::asset('employer_profile')}}/{{$SimilarJobs->profile_pic}}" class="mobile-recent-jobs-img" alt="" /> </div>
														<h3 class="recent-jobs-list pad-bottom10"><a href="/JobDetails/{{$SimilarJobs->CompanySlug}}/{{$SimilarJobs->slug}}" title="">{{$SimilarJobs->title}}</a></h3>
															<span class="pad-bottom10">{{$SimilarJobs->company_name}}</span>
															<div class="job-lctn"><i class="fa fa-briefcase"></i> {{$SimilarJobs->experience}} Years</div>
															
															<div class="job-lctn"><i class="fa fa-industry"></i> 
																@if($SimilarJobs->job_type == 1)
																	Part time
																@else
																	Full time
																@endif
															</div>
															<div class="job-lctn"><i class="fa fa-map-marker"></i> {{$SimilarJobs->city}}</div>
														</div>
														<div class="job-style-bx pad-top0">
															<a href="#">
																<p class="job-details-attributes width100 shortlist-jobs text-right pad-top0 pad-bottom10">
																	<span class="shortlist-icon">
																		<i class="fa fa-heart-o"> </i>
																	</span>
																	<span class="shortlist-bold">Shortlist</span>
																</p>
															</a>
															
		
															<i>Posted on: 
																@php 
																	$now = new DateTime;
	$full = false;	
    $ago = new DateTime($SimilarJobs->created_at);
    $diff = $now->diff($ago);

    $diff->w = floor($diff->d / 7);
    $diff->d -= $diff->w * 7;

    $string = array(
        'y' => 'year',
        'm' => 'month',
        'w' => 'week',
        'd' => 'day',
        'h' => 'hour',
        'i' => 'minute',
        's' => 'second',
    );
    foreach ($string as $k => &$v) {
        if ($diff->$k) {
            $v = $diff->$k . ' ' . $v . ($diff->$k > 1 ? 's' : '');
        } else {
            unset($string[$k]);
        }
    }

    if (!$full) $string = array_slice($string, 0, 1);
    echo $string ? implode(', ', $string) . ' ago' : 'just now';
																@endphp
															</i>
														</div>
													</div>
													
												</div>
												@endforeach
												@else
													<div class="job-listings-sec no-border">
														<h3>No Jobs Found.</h3>
													</div>
												@endif
											</div>
										</div>
									</div>
								</div>
		
								{{-- <div class="col-md-4">
									<div class="reviews candidates-profile-card-border jobs-description-padding job-description-mar-top30 pad-right10">
										<div class="row job-description-p quick-form-job">
											<h3 class="job-description-h3">Contact</h3>
											<form class="apply-jobs-contact">
												<label for="" class="remove-label apply-jobs-label">Name *</label>
												<input type="text" placeholder="" />
												<label for="" class="remove-label apply-jobs-label">Email Address *</label>
												<input type="text" placeholder="" />
												<label for="" class="remove-label apply-jobs-label">Phone Number *</label>
												<input type="text" placeholder="" />
												<label for="" class="remove-label apply-jobs-label">Message ( Should be more than 50 characters)</label>
												<textarea placeholder=""></textarea>
												<span><input type="checkbox" name="spealism" id="terms"><label for="terms" class="apply-jobs">You accepts our <a href="#" title="">Terms and Conditions</a></span>
												<br>
												<a href="#" title="" class="for-employers-btn post-a-job-home col-md-12 text-center">SEND MAIL</a>
												
											</form>
										</div>
									</div>
								</div> --}}
						</div>
                    </div>
                    
                </div>
            </div>
        </div>
    </div>
</section>



{{--  Post New Jobs  --}}
<div class="account-popup-area post-jobs-popup-box">
		<div class="account-popup post_new_jobs_popup">
			<span class="close-popup" id="register_close"><i class="la la-close"></i></span>
			<h3>Post a New Job</h3>
			<form class="mar-top15">
					<div class="row">
							<div class="dropdown-field col-md-12 pad-left0 pad-right0 mar-right10">
								<label class="pull-left remove-label">Job Title *
									</label>
								{{-- <div class="cfield">
									<input type="text" placeholder="UI Designer" value="" class="bg-white " id="title"/>
								</div> --}}
								<input type="hidden" name="_token" value="{{ csrf_token() }}">
	
								<select  data-placeholder="Please Select Functional Areas" class="chosen" id="title">
									<option selected>Select job title</option>
									@foreach($GetJobTitle as $Title)
								<option value="{{$Title->id}}">{{$Title->name}}
									</option>
									@endforeach
								</select>
							</div>
						</div>
	
				<div class="row">
					<div class="dropdown-field col-md-5 pad-left0 pad-right0 mar-right10">
						<label class="pull-left remove-label">Employee Type *
						</label>
						<select data-placeholder="Please Select Industries" class="chosen" id="employee_type" onchange="CheckEmployeeType()">
							<option selected>Select Type
								</option>
							<option value="1">Fresher
							</option>
							<option value="2">Experienced
								</option>
						</select>
					</div>
	
					<div class="dropdown-field col-md-5 pad-left0 pad-right0">
						<label class="pull-left remove-label">Job Type *
							</label>
						<select  data-placeholder="Please Select Functional Areas" class="chosen" id="job_type">
							<option selected>Select job type</option>
							<option value="1">Part time
							</option>
							<option value="2">Full time
								</option>
						</select>
					</div>
				</div>
				
				
	
				<div class="row">
					<div class="col-md-12 pad-left0 pad-right0 mar-right10">
						<label class="pull-left remove-label">Job Description
							</label>
						<div class="cfield">
							<textarea name="" id="description" class="bg-white"></textarea>
						</div>
					</div>
				</div>
				
				<div class="row">
					<div class="dropdown-field col-md-12 pad-left0 pad-right0 mar-right10">
						<label class="pull-left remove-label">Qualification
							</label>
						<div class="cfield">
							<input type="text" placeholder="Qualification" value="" class="bg-white" id="qualification"/>
						</div>
	
						{{--  <select  data-placeholder="Please Select Functional Areas" class="chosen" id="qualification">
							<option selected>Select qualification</option>
							@foreach($GetQualification as $Qualification)
						<option value="{{$Qualification->id}}">{{$Qualification->qualification}}
							</option>
							@endforeach
						</select>  --}}
					</div>
				</div>
	
				<h4 class="location-head">Location --------------------------------------------------</h4>
				<div id="GetAddress">
					
					<div class="row">
						<div class="col-md-4 pad-left0 pad-right0 mar-right5">
							<label class="pull-left remove-label">Zip Code
								</label>
							<div class="cfield">
								<input type="text" placeholder="Zipcode" value="" class="bg-white" onchange="CheckZipCode()" data-geocomplete="zip code" id="zipcode"/>
							</div>
						</div>
		
						<div class="col-md-7 pad-left0 pad-right0">
							<label class="pull-left remove-label">Country
								</label>
							<div class="cfield">
								<input type="text" placeholder="Country" value="" class="bg-white " data-geocomplete="county" id="country"/>
							</div>
						</div>
					</div>
		
					<div class="row">
						<div class="col-md-5 pad-left0 pad-right0 mar-right5">
							<label class="pull-left remove-label">State
								</label>
							<div class="cfield">
								<input type="text" data-geocomplete="state" placeholder="State" value="" class="bg-white " id="state"/>
							</div>
						</div>
		
						<div class="col-md-5 pad-left0 pad-right0">
							<label class="pull-left remove-label">City
								</label>
							<div class="cfield">
								<input type="text" placeholder="City" value="" class="bg-white" data-geocomplete="city" id="city"/>
							</div>
						</div>
					</div>
				</div>
				
				<div class="row">
					<div class="col-md-12 pad-left0">
						<label class="pull-left remove-label">Area
							</label>
						<div class="cfield">
							<input type="text" data-geocomplete="street address"  placeholder="Type Area" value="" class="bg-white " id="address"/>
						</div>
					</div>
					{{--  <div id="myForm">
						<input id="txtAddress1" data-geocomplete="street address" />
					</div>  --}}
				</div>
					
				<h4 class="location-head">Salary and Experience -----------------------</h4>
	
				<div class="row">
						<div class="col-md-4 pad-left0 pad-right0 salary-column-width">
								<label class="pull-left remove-label">Salary Type *
									</label>
								<select  data-placeholder="Please Select Functional Areas" class="chosen" id="salary_type" onchange="SalaryType()">
									<option value="1">Monthly
									</option>
									<option value="2">Yearly
									</option>
								</select>
							</div>
							
					<div class="col-md-4 pad-left0 pad-right0 salary-column-width">
						<label class="pull-left remove-label">Salary Min
							</label>
						<div class="cfield">
							<input type="text" placeholder="Salary Min" value="" class="bg-white " id="salary_min"/>
						</div>
					</div>
	
					<div class="col-md-4 pad-left0 pad-right0 salary-column-width">
						<label class="pull-left remove-label">Salary Max
							</label>
						<div class="cfield">
							<input type="text" placeholder="Salary Max" value="" class="bg-white" id="salary_max"/>
						</div>
					</div>
					
				</div>
	
				<div class="row">
					<div class="col-md-7 pad-left0 pad-right0 mar-right5" id="CheckExperienced">
						<label class="pull-left remove-label">Experience (Years)
							</label>
						<div class="cfield">
							<input type="text" placeholder="Experience" value="" class="bg-white " id="experience"/>
						</div>
					</div>
	
					<div class="col-md-4 pad-left0 pad-right0">
						<label class="pull-left remove-label">No Of Position
							</label>
						<div class="cfield">
							<input type="text" placeholder="Position" value="" class="bg-white " id="positions"/>
						</div>
					</div>
				</div>
	
				<div class="row">
					<div class="col-md-7">
						<button type="button" class="btn btn-default candidate-profile-cancel-btn text-right close-popup" data-dismiss="modal">Close
						</button>
					</div>
					<div class="col-md-5">
						<button type="button" id="PostJobs" class="btn btn-default candidate-profile-save-btn">Save
							</button>
					</div>
	
				</div>
			  </form>
		</div>
	</div>
	<!-- Post New Jobs POPUP -->
	
	{{--  Post Trainings  --}}
	<div class="account-popup-area post-trainings-popup-box">
		<div class="account-popup post_new_jobs_popup">
			<span class="close-popup" id="register_close"><i class="la la-close"></i></span>
			<h3>Post Internships</h3>
			<form class="mar-top15">
				
				<div class="row">
					<div class="dropdown-field col-md-12 pad-left0 pad-right0 mar-right10">
						<label class="pull-left remove-label">Training Name *
							</label>
						{{-- <div class="cfield">
							<input type="text" placeholder="Training Name" value="" class="bg-white " id="training_title"/>
						</div> --}}
						<select  data-placeholder="Please Select Functional Areas" class="chosen" id="training_title">
							<option selected>Select training title
							</option>
							@foreach($GetTrainingTitle as $Title)
								<option value="{{$Title->id}}">{{$Title->title}}
								</option>
							@endforeach
						</select>
					</div>
				</div>
	
				<div class="row">
					<div class="col-md-5 pad-left0 pad-right0 mar-right5">
						<label class="pull-left remove-label">Start Date * 
							</label>
						<div class="cfield">
							<input type="date" placeholder="Start Date" value="" class="bg-white" name="start_date" id="start_date"/>
						</div>
					</div>
	
					<div class="col-md-3 pad-left0 pad-right0 training-duration-marright">
						<label class="pull-left remove-label">Duration *
						</label>
						<select  data-placeholder="Please select days" class="dropdown-custom-clr-pad duration_days" id="duration_days">
							{{-- <option value="1">30
							</option>
							<option value="2">60
							</option> --}}
						</select>
					</div>
	
					<div class="col-md-3 pad-left0 pad-right0">
						<label class="pull-left remove-label">
						</label>
						<select  data-placeholder="Please Select Functional Areas" class="chosen" id="duration">
							<option value="Days">Days
							</option>
							<option value="Months">Months
							</option>
						</select>
					</div>
				</div>
	
				<div class="row">
					<div class="col-md-5 pad-left0 pad-right0 mar-right5">
						<label class="pull-left remove-label">Monthly Stipend * 
							</label>
						 <div class="cfield">
							<input type="text" placeholder="Monthly stipend" value="" class="bg-white " id="stipend" maxlength="6"/>
						</div> 
					</div>
	
					<div class="col-md-5 pad-left0 pad-right0">
						<label class="pull-left remove-label">Last Date of application *
							</label>
						<div class="cfield">
							<input type="date" placeholder="Last date of application" value="" class="bg-white" onchange="CheckLastDate()" id="last_date"/>
						</div>
					</div>
				</div>
	
				<div class="row">
					<div class="col-md-12 pad-left0 pad-right0 mar-right10">
						<label class="pull-left remove-label">Internship Description
							</label>
						<div class="cfield">
							<textarea name="" id="training_description" class="bg-white"></textarea>
						</div>
					</div>
				</div>
	
			   
				<div class="row">
					<div class="col-md-12 pad-left0 pad-right0 mar-right5">
						<label class="pull-left remove-label">What trainee will get after Internship completion *
						</label>
						<select  data-placeholder="Please Select Functional Areas" class="chosen" id="completion">
							<option selected>Select type
							</option>
							<option value="1">Certification
							</option>
							<option value="2">Certification + Job Offers
							</option>
						</select>
					</div>
				</div>
	
				<div class="row">
					<div class="col-md-7">
						<button type="button" class="btn btn-default candidate-profile-cancel-btn text-right close-popup" data-dismiss="modal">Close
							</button>
					</div>
					<div class="col-md-5">
						<button type="button" id="PostTrainings" class="btn btn-default candidate-profile-save-btn">Save
							</button>
					</div>
	
				</div>
			</form>
		</div>
	</div>
	<!-- Post New Trainings POPUP -->
	
	
	
		{{--  Post CA Articleships  --}}
	<div class="account-popup-area post-ca-jobs-popup-box">
			<div class="account-popup post_new_jobs_popup">
				<span class="close-popup" id="register_close"><i class="la la-close"></i></span>
				<h3>Post CA Articleships</h3>
				<form class="mar-top15">
					
					<div class="row">
							<div class="col-md-12 pad-left0 pad-right0 mar-right10">
								<label class="pull-left remove-label">Description
									</label>
								<div class="cfield">
									<textarea name="" id="Ca_description" class="bg-white"></textarea>
								</div>
							</div>
						</div>
	
						<div class="row">
							<div class="dropdown-field col-md-12 pad-left0 pad-right0 mar-right10">
								<label class="pull-left remove-label">Qualification
									</label>
								<div class="cfield">
									<input type="text" placeholder="Qualification" value="" class="bg-white" id="Ca_qualification"/>
								</div>
		
								{{--  <select  data-placeholder="Please Select Functional Areas" class="chosen" id="Ca_qualification">
									<option selected>Select qualification</option>
									@foreach($GetQualification as $Qualification)
								<option value="{{$Qualification->id}}">{{$Qualification->qualification}}
									</option>
									@endforeach
								</select>  --}}
							</div>
						</div>
						
					<div class="row">
						<div class="col-md-5 pad-left0 pad-right0 mar-right5">
							<label class="pull-left remove-label">Monthly Stipend * 
								</label>
							<select  data-placeholder="Please Select Functional Areas" class="chosen" id="stipend_type" onchange="CheckStipend()">
								<option value="1">ICAI norms</option>
								<option value="2">Others</option>
							</select>
						</div>
		
						{{-- <div class="col-md-5 pad-left0 pad-right0 mar-right5" id="ShowStipend">
							<label class="pull-left remove-label">Monthly Stipend * 
								</label>
							
							<div class="cfield">
								<input type="text" placeholder="Monthly stipend" value="" class="bg-white " id="Ca_stipend"/>
							</div> 
						</div> --}}
					</div>
					
					<div id="SalaryShow" style="display:none;">
						<h4 class="location-head">Salary and Experience -----------------------</h4>
	
						<div class="row">
							<div class="col-md-4 pad-left0 pad-right0 salary-column-width">
								<label class="pull-left remove-label">Salary Type *
									</label>
								<select  data-placeholder="Please Select Functional Areas" class="chosen" id="Ca_salary_type">
									<option value="1">Monthly
									</option>
									<option value="2">Yearly
									</option>
								</select>
							</div>
							<div class="col-md-4 pad-left0 pad-right0 salary-column-width">
								<label class="pull-left remove-label">Salary Min
									</label>
								<div class="cfield">
									<input type="text" placeholder="Salary Min" value="" class="bg-white " id="Ca_salary_min" maxlength="6"/>
								</div>
							</div>
			
							<div class="col-md-4 pad-left0 pad-right0 salary-column-width">
								<label class="pull-left remove-label">Salary Max
									</label>
								<div class="cfield">
									<input type="text" placeholder="Salary Max" value="" class="bg-white " id="Ca_salary_max" maxlength="6"/>
								</div>
							</div>
							
						</div>
					</div>
					
					<h4 class="location-head">Location --------------------------------------------------</h4>
	
						<div id="GetCAProfileAddress">
							
	
							<div class="row">
								<div class="col-md-4 pad-left0 pad-right0 mar-right5">
									<label class="pull-left remove-label">Zip Code
										</label>
									<div class="cfield">
										<input type="text" data-geocomplete="zip code" placeholder="Zipcode" value="" onchange="CheckArticleshipZipCode()" class="bg-white " id="Ca_Edit_zipcode"/>
									</div>
								</div>
				
								<div class="col-md-7 pad-left0 pad-right0">
									<label class="pull-left remove-label">Country
										</label>
									<div class="cfield">
										<input type="text" data-geocomplete="county" placeholder="Country" value="" class="bg-white " id="Ca_Edit_country"/>
									</div>
								</div>
							</div>
				
							<div class="row">
								<div class="col-md-5 pad-left0 pad-right0 mar-right5">
									<label class="pull-left remove-label">State
										</label>
									<div class="cfield">
										<input type="text" data-geocomplete="state" placeholder="State" value="" class="bg-white " id="Ca_Edit_state"/>
									</div>
								</div>
				
								<div class="col-md-5 pad-left0 pad-right0">
									<label class="pull-left remove-label">City
										</label>
									<div class="cfield">
										<input type="text" data-geocomplete="city" placeholder="City" value="" class="bg-white " id="Ca_Edit_city"/>
									</div>
								</div>
							</div>
	
							<div class="row">
								<div class="col-md-12 pad-left0">
									<label class="pull-left remove-label">Area
										</label>
									<div class="cfield">
										<input type="text" data-geocomplete="street address"  placeholder="Type Area" value="" class="bg-white " id="Ca_Edit_address"/>
									</div>
								</div>
								{{--  <div id="myForm">
									<input id="txtAddress1" data-geocomplete="street address" />
								</div>  --}}
							</div>
						</div>
	
					
		
				   
					<div class="row">
						<div class="col-md-6 pad-left0 pad-right0">
							<label class="pull-left remove-label">No Of Openings
								</label>
							<div class="cfield">
								<input type="text" placeholder="Position" value="" class="bg-white " id="Ca_positions"/>
							</div>
						</div>
					</div>
		
					<div class="row">
						<div class="col-md-7">
							<button type="button" class="btn btn-default candidate-profile-cancel-btn text-right close-popup" data-dismiss="modal">Close
							</button>
						</div>
						<div class="col-md-5">
							<button type="button" id="PostCAJobs" class="btn btn-default candidate-profile-save-btn">Save
								</button>
						</div>
		
					</div>
				</form>
			</div>
		</div>
		<!-- Post New Trainings POPUP -->


	<!-- Profile Edit -->
<div class="account-popup-area percentage-notify-profile-popup-box">
		<div class="account-popup candidate-profile-popup">
		  <span class="close-popup">
			<i class="la la-close">
			</i>
		  </span>
		  <h3 class="candidate-profile-details-popup">Profile Details
		  </h3>
		  <form class="mar-top15">
			<p>You should complete your profile atleast 90%, Then only you can able to apply this job.</p>
			<button type="button" onclick="location.href='/Candidate/Profile'" class="width100 candidate-profile-save-btn col-md-12 col-xs-12" id="">Go to your profile
			</button>
		  </form>
		</div>
	  </div>
@endforeach
@endsection



@section('JSScript')
	<script>
		$(document).ready(function () {
			var $select = $(".duration_days");
			for (i=1;i<=60;i++){
				$select.append($('<option value='+i+'></option>').val(i).html(i))
			}
		});
	</script>
@endsection