@extends('UI.base')

@section('Content')
<section id="scroll-here" class="bg-grey">
    <div class="block">
        <div class="container">
            <div class="row candidate-row-mar-top">
                <div class="col-lg-12 col-md-6">
                    <div class="reviews-sec candiates-card-profile-top">
						<div class="row job-details-breadcrumb">
							<div class="col-lg-12 pad-top30">
								@foreach($GetJobsById as $Jobs)
									<p class="job-details-breadcrumb-p"><i class="fa fa-home clr-primary"></i> <i class="fa fa-caret-right"></i> <a href="#" class="clr-primary">All jobs</a> <i class="fa fa-caret-right"></i> {{$Jobs->title}}</p>
								@endforeach
							</div>
						</div>

						<div class="row">
							<div class="col-lg-12">
								<div class="reviews candidates-profile-card-border mar-top15 mobile-card-pad-right applied-jobs-card-width-pad">
									@foreach($GetJobsById as $Jobs)
									<div class="row">
										<div class="col-md-8 pull-left pad-left30 mobile-job-details-align-center">
											<input type="hidden" id="JobId" value="{{$Jobs->id}}">

											<h3 class="mobile-review-head"><span>ID: {{$Jobs->jobs_id}}</span> {{$Jobs->title}}</h3>
											<br>
											<p class="job-details-attributes"><i class="fa fa-briefcase"> {{$Jobs->experience}} Years</i></p>
		
											<p class="job-details-attributes"><i class="fa fa-briefcase"> 
												@if($Jobs->job_type == 1)
													Part time
												@else
													Full time
												@endif
											</i></p>
		
											<p class="job-details-attributes"><i class="fa fa-map-marker"> {{$Jobs->city}}</i></p>

											<p class="job-details-attributes applied-candidates-salary-width"><i class="fa fa-map-marker"> 
												@php    
													$num = $Jobs->salary_min;
													$units = ['', 'K', 'M'];
													for($i = 0; $num>=1000;$i++){
														$num /= 1000;
													}
													echo round($num, 1).$units[$i];
												@endphp
												 - 
												 @php    
													$num = $Jobs->salary_max;
													$units = ['', 'K', 'M'];
													for($i = 0; $num>=1000;$i++){
														$num /= 1000;
													}
													echo round($num, 1).$units[$i];
												@endphp
												 / Month</i></p>

											{{-- <p class="job-details-attributes applied-candidates-salary-width"><i class="fa fa-map-marker"> Bachelor's Degree</i></p> --}}

											<div class="col-md-12 pull-left applied-candidates-jobs-card applied-jobs-attributes-column">
			
												<p class="job-details-attributes applied-jobs-details-attributes-width">Openings: {{$Jobs->total_positions}}</p>
			
												<p class="job-details-attributes job-attributes-width30 applied-candidates-job-attributes">Posted on: 
													{{date('d M Y', strtotime($Jobs->created_at))}}
												</p>
	
												<p class="job-details-attributes applied-jobs-details-attributes-width">Job Views: {{count($GetTotalViewsCount)}}</p>
	
												<p class="job-details-attributes applied-jobs-details-attributes-width">Job Applicant: 
													@if($CheckApplicantsCount)
														{{count($CheckApplicantsCount)}}
													@else
														0
													@endif
												</p>
											</div>
										</div>
		
										<div class="col-md-4 mobile-profile-center job-listing applied-jobs-listing">
												
												<button type="button" onclick="javascript:window.open('/JobDetails/{{$Jobs->CompanySlug}}/{{$Jobs->id}}/{{$Jobs->slug}}', '_blank');" formtarget="_blank"  class="view-details-jobs pull-right"><i class="fa fa-eye"></i> Preview as candidate</button>
												
												<ul class="list-group list-group-flush toggle-btn-mar-top">
													<li class="list-group-item text-center">
														Job Status
														<label class="switch JobClose">
															<input type="checkbox" name="ChangeJobStatus" class="primary ChangeJobStatus"
															@if($Jobs->status == 1)
															checked
															@else

															@endif
															>
															<span class="slider round"></span>
														</label>
													</li>
												</ul>
										</div>
									</div>
									{{-- <div class="row applied-candidates-jobs-card">
										
		
									</div> --}}
									@endforeach
								</div>
							</div>
						</div>
						<div class="row ">
							<div class="col-lg-12">
								<div class="reviews candidates-profile-card-border jobs-description-padding job-description-mar-top30 applied-jobs-card-width-pad">
									<div class="row applied-candidates-row-height">
										<div class="col-md-6">
											<h3 class="job-description-h3 applied-candidate-applied-jobs-head">Applied Candidates ({{count($GetCandidates)}})</h3>
										</div>
										<div class="col-md-4">
											{{-- <h3 class="job-description-h3 text-right">
												<p><input type="checkbox" name="CheckAppliedShortlisted" class="CheckAppliedShortlisted" id="CheckAppliedShortlisted" value="1"><label for="CheckAppliedShortlisted" class="CheckBoxFilter">View Only Shortlisted (10)</label></p> 
											</h3> --}}
										</div>
										<div class="col-md-2">
											<div class="sortby-sec pad-top15">
												{{-- <select data-placeholder="20 Per Page" onchange="SortByJobs()" id="sortby" class="chosen">
													<option selected disabled>Sort by</option>
													<option value="1">30 Per Page</option>
													<option value="2">40 Per Page</option>
													<option value="3">50 Per Page</option>
													<option value="4">60 Per Page</option>
												</select> --}}
												</div>
										</div>
									</div>
									<div class="text-center">
										<img id="AjaxLoader" class="appliedcandidates-loader" src="{{URL::asset('UI/ajax_loader.gif')}}" alt="" style="display:none;">
									</div>
									

									<div class="job-list-modern" id="AppliedCandidates">
										
										<table class="table">
											<thead>
											  <tr>
												<th>Name</th>
												<th>Degree</th>
												<th>Designation</th>
												<th>Year of passed</th>
												<th>Experience</th>
												<th>Area</th>
												<th></th>
											  </tr>
											</thead>
											<tbody>
											@foreach($GetCandidates as $Candidates)
											  <tr>
												<td><a href="/AppliedCandidateDetails/{{$Jobs->id}}/{{$Candidates->id}}/{{$Candidates->slug}}" target="_blank">{{$Candidates->name}}</a></td>
												<td>{{$Candidates->qualification}}</td>
												<td>{{$GetProfessionalDetails->JobTitle}}</td>
												<td>{{$Candidates->from_year}}</td>

												<td>@php
														$TotalExperience = $Candidates->experience;
														$GetTotalExperience = explode(',', $TotalExperience);
									
														echo $GetTotalExperience[0].' Years '.$GetTotalExperience[1].' Months';
													@endphp</td>

												<td>{{$Candidates->desired_location}}</td>

												<td>
														{{-- <img id="ApplyBtnLoader" class="candidate-shortlisted-loader" src="{{URL::asset('UI/otp_verify_loader.gif')}}" alt="" style="display:none;">
	
														<p class="shortlisted-success text-right" id="ShortlistedSuccess" style="display:none;"><i class="fa fa-check"></i> Shortlisted</p>
														
														@if($CheckShortlisted)
															<p class="applied-candiates-shortlisted-success" id="ShortlistedSuccess"><i class="fa fa-check"></i> Shortlisted</p>
														@else
															<a href="javascript:void(0);" id="AddShortlisted" class="text-center" onclick="AddShortlisted({{$Candidates->id}})">
																<p class="width100 shortlist-jobs shortlist-pad-mar shortlist-applied-candidates">
																	<span class="shortlist-icon">
																		<i class="fa fa-heart-o"> </i>
																	</span>
																<span class="shortlist-bold"> Shortlist</span></p>
															</a>
													@endif --}}
													
													<a href="/AppliedCandidateDetails/{{$Jobs->id}}/{{$Candidates->id}}/{{$Candidates->slug}}" target="_blank" class="for-employers-btn post-a-job-home text-center download-cv-mar pull-right mar-bottom15">View Candidate Details</a>
													
													

													<p id="DownloadCVPath" style="display:none;">/candidate_cv/{{$GetCV->cv}}</p>

													<a onclick="DownloadCV({{$Candidates->id}}, {{$Jobs->id}})" title="" class="for-employers-btn post-a-job-home text-center download-cv-mar pull-right" download>DOWNLOAD CV</a>
												</td>
											  </tr>
											  @endforeach
											</tbody>
										  </table>

										{{-- <div class="job-listings-sec no-border applied-candidates-width">
											<div class="job-listing wtabs pad-top0 candidate-list-mar-top pad-bottom25">
												<div class="width100 applied-candidates-list">
													<div class="col-md-1 pull-left">
														<img class="candidate-list-profile-pic" src="{{URL::asset('candidate_profile/')}}/{{$Candidates->profile_pic}}" alt="" />
													</div>
													<div class="col-md-7 pull-left">
													<a href="/AppliedCandidateDetails/{{$Jobs->id}}/{{$Candidates->id}}/{{$Candidates->slug}}" target="_blank">
														<h3 class="candidate-list-name">{{$Candidates->name}}</h3>
													</a>
														@if($GetProfessionalDetails)
														<div class="job-lctn"> 
															{{$GetProfessionalDetails->department}} at {{$GetProfessionalDetails->name}}
														</div>
														@endif
														<br>
														<div class="job-lctn"><i class="fa fa-map-marker"></i> {{$Candidates->desired_location}}</div>
	
														<div class="job-lctn"><i class="fa fa-briefcase"></i>  
														@php
															$TotalExperience = $Candidates->experience;
															$GetTotalExperience = explode(',', $TotalExperience);
										
															echo $GetTotalExperience[0].' Years '.$GetTotalExperience[1].' Months';
														@endphp
														</div>
	
														<div class="job-lctn"><i class="fa fa-briefcase"></i> 
														@php
															$TotalSalary = $Candidates->gross_salary;
															$GetTotalSalary = explode(',', $TotalSalary);
										
															echo $GetTotalSalary[0].' Lacs '.$GetTotalSalary[1].' Thousand';
														@endphp
														</div>
	
													<div>
												
													</div>
												</div>
												<div class="col-md-4 pull-left">
													<img id="ApplyBtnLoader" class="candidate-shortlisted-loader" src="{{URL::asset('UI/otp_verify_loader.gif')}}" alt="" style="display:none;">
	
													<p class="shortlisted-success text-right" id="ShortlistedSuccess" style="display:none;"><i class="fa fa-check"></i> Shortlisted</p>
													
													@if($CheckShortlisted)
														<p class="applied-candiates-shortlisted-success" id="ShortlistedSuccess"><i class="fa fa-check"></i> Shortlisted</p>
													@else
														<a href="javascript:void(0);" id="AddShortlisted" class="text-center" onclick="AddShortlisted({{$Candidates->id}})">
															<p class="width100 shortlist-jobs shortlist-pad-mar shortlist-applied-candidates">
																<span class="shortlist-icon">
																	<i class="fa fa-heart-o"> </i>
																</span>
															<span class="shortlist-bold"> Shortlist</span></p>
														</a>
												@endif
													<p id="DownloadCVPath" style="display:none;">/candidate_cv/{{$GetCV->cv}}</p>
													<a onclick="DownloadCV({{$Candidates->id}}, {{$Jobs->id}})" title="" class="for-employers-btn post-a-job-home text-center download-cv-mar pull-right" download>DOWNLOAD CV</a>
												</div>
												
											</div>
										</div>
									</div>
									@endforeach --}}
								</div>

								
							</div>
		
						</div>
                    </div>
                    
                </div>
            </div>
        </div>
    </div>
</section>



{{--  Post New Jobs  --}}
<div class="account-popup-area post-jobs-popup-box">
		<div class="account-popup post_new_jobs_popup">
			<span class="close-popup" id="register_close"><i class="la la-close"></i></span>
			<h3>Post a New Job</h3>
			<form class="mar-top15">
					<div class="row">
							<div class="dropdown-field col-md-12 pad-left0 pad-right0 mar-right10">
								<label class="pull-left remove-label">Job Title *
									</label>
								{{-- <div class="cfield">
									<input type="text" placeholder="UI Designer" value="" class="bg-white " id="title"/>
								</div> --}}
								<input type="hidden" name="_token" value="{{ csrf_token() }}">
	
								<select  data-placeholder="Please Select Functional Areas" class="chosen" id="title">
									<option selected>Select job title</option>
									@foreach($GetJobTitle as $Title)
								<option value="{{$Title->id}}">{{$Title->name}}
									</option>
									@endforeach
								</select>
							</div>
						</div>
	
				<div class="row">
					<div class="dropdown-field col-md-5 pad-left0 pad-right0 mar-right10">
						<label class="pull-left remove-label">Employee Type *
						</label>
						<select data-placeholder="Please Select Industries" class="chosen" id="employee_type" onchange="CheckEmployeeType()">
							<option selected>Select Type
								</option>
							<option value="1">Fresher
							</option>
							<option value="2">Experienced
								</option>
						</select>
					</div>
	
					<div class="dropdown-field col-md-5 pad-left0 pad-right0">
						<label class="pull-left remove-label">Job Type *
							</label>
						<select  data-placeholder="Please Select Functional Areas" class="chosen" id="job_type">
							<option selected>Select job type</option>
							<option value="1">Part time
							</option>
							<option value="2">Full time
								</option>
						</select>
					</div>
				</div>
				
				
	
				<div class="row">
					<div class="col-md-12 pad-left0 pad-right0 mar-right10">
						<label class="pull-left remove-label">Job Description
							</label>
						<div class="cfield">
							<textarea name="" id="description" class="bg-white"></textarea>
						</div>
					</div>
				</div>
				
				<div class="row">
					<div class="dropdown-field col-md-12 pad-left0 pad-right0 mar-right10">
						<label class="pull-left remove-label">Qualification
							</label>
						<div class="cfield">
							<input type="text" placeholder="Qualification" value="" class="bg-white" id="qualification"/>
						</div>
	
						{{--  <select  data-placeholder="Please Select Functional Areas" class="chosen" id="qualification">
							<option selected>Select qualification</option>
							@foreach($GetQualification as $Qualification)
						<option value="{{$Qualification->id}}">{{$Qualification->qualification}}
							</option>
							@endforeach
						</select>  --}}
					</div>
				</div>
	
				<h4 class="location-head">Location --------------------------------------------------</h4>
				<div id="GetAddress">
					
					<div class="row">
						<div class="col-md-4 pad-left0 pad-right0 mar-right5">
							<label class="pull-left remove-label">Zip Code
								</label>
							<div class="cfield">
								<input type="text" placeholder="Zipcode" value="" class="bg-white" onchange="CheckZipCode()" data-geocomplete="zip code" id="zipcode"/>
							</div>
						</div>
		
						<div class="col-md-7 pad-left0 pad-right0">
							<label class="pull-left remove-label">Country
								</label>
							<div class="cfield">
								<input type="text" placeholder="Country" value="" class="bg-white " data-geocomplete="county" id="country"/>
							</div>
						</div>
					</div>
		
					<div class="row">
						<div class="col-md-5 pad-left0 pad-right0 mar-right5">
							<label class="pull-left remove-label">State
								</label>
							<div class="cfield">
								<input type="text" data-geocomplete="state" placeholder="State" value="" class="bg-white " id="state"/>
							</div>
						</div>
		
						<div class="col-md-5 pad-left0 pad-right0">
							<label class="pull-left remove-label">City
								</label>
							<div class="cfield">
								<input type="text" placeholder="City" value="" class="bg-white" data-geocomplete="city" id="city"/>
							</div>
						</div>
					</div>
				</div>
				
				<div class="row">
					<div class="col-md-12 pad-left0">
						<label class="pull-left remove-label">Area
							</label>
						<div class="cfield">
							<input type="text" data-geocomplete="street address"  placeholder="Type Area" value="" class="bg-white " id="address"/>
						</div>
					</div>
					{{--  <div id="myForm">
						<input id="txtAddress1" data-geocomplete="street address" />
					</div>  --}}
				</div>
					
				<h4 class="location-head">Salary and Experience -----------------------</h4>
	
				<div class="row">
						<div class="col-md-4 pad-left0 pad-right0 salary-column-width">
								<label class="pull-left remove-label">Salary Type *
									</label>
								<select  data-placeholder="Please Select Functional Areas" class="chosen" id="salary_type" onchange="SalaryType()">
									<option value="1">Monthly
									</option>
									<option value="2">Yearly
									</option>
								</select>
							</div>
							
					<div class="col-md-4 pad-left0 pad-right0 salary-column-width">
						<label class="pull-left remove-label">Salary Min
							</label>
						<div class="cfield">
							<input type="text" placeholder="Salary Min" value="" class="bg-white " id="salary_min"/>
						</div>
					</div>
	
					<div class="col-md-4 pad-left0 pad-right0 salary-column-width">
						<label class="pull-left remove-label">Salary Max
							</label>
						<div class="cfield">
							<input type="text" placeholder="Salary Max" value="" class="bg-white" id="salary_max"/>
						</div>
					</div>
					
				</div>
	
				<div class="row">
					<div class="col-md-7 pad-left0 pad-right0 mar-right5" id="CheckExperienced">
						<label class="pull-left remove-label">Experience (Years)
							</label>
						<div class="cfield">
							<input type="text" placeholder="Experience" value="" class="bg-white " id="experience"/>
						</div>
					</div>
	
					<div class="col-md-4 pad-left0 pad-right0">
						<label class="pull-left remove-label">No Of Position
							</label>
						<div class="cfield">
							<input type="text" placeholder="Position" value="" class="bg-white " id="positions"/>
						</div>
					</div>
				</div>
	
				<div class="row">
					<div class="col-md-7">
						<button type="button" class="btn btn-default candidate-profile-cancel-btn text-right close-popup" data-dismiss="modal">Close
						</button>
					</div>
					<div class="col-md-5">
						<button type="button" id="PostJobs" class="btn btn-default candidate-profile-save-btn">Save
							</button>
					</div>
	
				</div>
			  </form>
		</div>
	</div>
	<!-- Post New Jobs POPUP -->
	
	{{--  Post Trainings  --}}
	<div class="account-popup-area post-trainings-popup-box">
		<div class="account-popup post_new_jobs_popup">
			<span class="close-popup" id="register_close"><i class="la la-close"></i></span>
			<h3>Post Internships</h3>
			<form class="mar-top15">
				
				<div class="row">
					<div class="dropdown-field col-md-12 pad-left0 pad-right0 mar-right10">
						<label class="pull-left remove-label">Training Name *
							</label>
						{{-- <div class="cfield">
							<input type="text" placeholder="Training Name" value="" class="bg-white " id="training_title"/>
						</div> --}}
						<select  data-placeholder="Please Select Functional Areas" class="chosen" id="training_title">
							<option selected>Select training title
							</option>
							@foreach($GetTrainingTitle as $Title)
								<option value="{{$Title->id}}">{{$Title->title}}
								</option>
							@endforeach
						</select>
					</div>
				</div>
	
				<div class="row">
					<div class="col-md-5 pad-left0 pad-right0 mar-right5">
						<label class="pull-left remove-label">Start Date * 
							</label>
						<div class="cfield">
							<input type="date" placeholder="Start Date" value="" class="bg-white" name="start_date" id="start_date"/>
						</div>
					</div>
	
					<div class="col-md-3 pad-left0 pad-right0 training-duration-marright">
						<label class="pull-left remove-label">Duration *
						</label>
						<select  data-placeholder="Please select days" class="dropdown-custom-clr-pad duration_days" id="duration_days">
							{{-- <option value="1">30
							</option>
							<option value="2">60
							</option> --}}
						</select>
					</div>
	
					<div class="col-md-3 pad-left0 pad-right0">
						<label class="pull-left remove-label">
						</label>
						<select  data-placeholder="Please Select Functional Areas" class="chosen" id="duration">
							<option value="Days">Days
							</option>
							<option value="Months">Months
							</option>
						</select>
					</div>
				</div>
	
				<div class="row">
					<div class="col-md-5 pad-left0 pad-right0 mar-right5">
						<label class="pull-left remove-label">Monthly Stipend * 
							</label>
						 <div class="cfield">
							<input type="text" placeholder="Monthly stipend" value="" class="bg-white " id="stipend" maxlength="6"/>
						</div> 
					</div>
	
					<div class="col-md-5 pad-left0 pad-right0">
						<label class="pull-left remove-label">Last Date of application *
							</label>
						<div class="cfield">
							<input type="date" placeholder="Last date of application" value="" class="bg-white"  id="last_date"/>
						</div>
					</div>
				</div>
	
				<div class="row">
					<div class="col-md-12 pad-left0 pad-right0 mar-right10">
						<label class="pull-left remove-label">Internship Description
							</label>
						<div class="cfield">
							<textarea name="" id="training_description" class="bg-white"></textarea>
						</div>
					</div>
				</div>
	
			   
				<div class="row">
					<div class="col-md-12 pad-left0 pad-right0 mar-right5">
						<label class="pull-left remove-label">What trainee will get after Internship completion *
						</label>
						<select  data-placeholder="Please Select Functional Areas" class="chosen" id="completion">
							<option selected>Select type
							</option>
							<option value="1">Certification
							</option>
							<option value="2">Certification + Job Offers
							</option>
						</select>
					</div>
				</div>
	
				<div class="row">
					<div class="col-md-7">
						<button type="button" class="btn btn-default candidate-profile-cancel-btn text-right close-popup" data-dismiss="modal">Close
							</button>
					</div>
					<div class="col-md-5">
						<button type="button" id="PostTrainings" class="btn btn-default candidate-profile-save-btn">Save
							</button>
					</div>
	
				</div>
			</form>
		</div>
	</div>
	<!-- Post New Trainings POPUP -->
	
	
	
		{{--  Post CA Articleships  --}}
	<div class="account-popup-area post-ca-jobs-popup-box">
			<div class="account-popup post_new_jobs_popup">
				<span class="close-popup" id="register_close"><i class="la la-close"></i></span>
				<h3>Post CA Articleships</h3>
				<form class="mar-top15">
					
					<div class="row">
							<div class="col-md-12 pad-left0 pad-right0 mar-right10">
								<label class="pull-left remove-label">Description
									</label>
								<div class="cfield">
									<textarea name="" id="Ca_description" class="bg-white"></textarea>
								</div>
							</div>
						</div>
	
						<div class="row">
							<div class="dropdown-field col-md-12 pad-left0 pad-right0 mar-right10">
								<label class="pull-left remove-label">Qualification
									</label>
								<div class="cfield">
									<input type="text" placeholder="Qualification" value="" class="bg-white" id="Ca_qualification"/>
								</div>
		
								{{--  <select  data-placeholder="Please Select Functional Areas" class="chosen" id="Ca_qualification">
									<option selected>Select qualification</option>
									@foreach($GetQualification as $Qualification)
								<option value="{{$Qualification->id}}">{{$Qualification->qualification}}
									</option>
									@endforeach
								</select>  --}}
							</div>
						</div>
						
					<div class="row">
						<div class="col-md-5 pad-left0 pad-right0 mar-right5">
							<label class="pull-left remove-label">Monthly Stipend * 
								</label>
							<select  data-placeholder="Please Select Functional Areas" class="chosen" id="stipend_type" onchange="CheckStipend()">
								<option value="1">ICAI norms</option>
								<option value="2">Others</option>
							</select>
						</div>
		
						{{-- <div class="col-md-5 pad-left0 pad-right0 mar-right5" id="ShowStipend">
							<label class="pull-left remove-label">Monthly Stipend * 
								</label>
							
							<div class="cfield">
								<input type="text" placeholder="Monthly stipend" value="" class="bg-white " id="Ca_stipend"/>
							</div> 
						</div> --}}
					</div>
					
					<div id="SalaryShow" style="display:none;">
						<h4 class="location-head">Salary and Experience -----------------------</h4>
	
						<div class="row">
							<div class="col-md-4 pad-left0 pad-right0 salary-column-width">
								<label class="pull-left remove-label">Salary Type *
									</label>
								<select  data-placeholder="Please Select Functional Areas" class="chosen" id="Ca_salary_type">
									<option value="1">Monthly
									</option>
									<option value="2">Yearly
									</option>
								</select>
							</div>
							<div class="col-md-4 pad-left0 pad-right0 salary-column-width">
								<label class="pull-left remove-label">Salary Min
									</label>
								<div class="cfield">
									<input type="text" placeholder="Salary Min" value="" class="bg-white " id="Ca_salary_min" maxlength="6"/>
								</div>
							</div>
			
							<div class="col-md-4 pad-left0 pad-right0 salary-column-width">
								<label class="pull-left remove-label">Salary Max
									</label>
								<div class="cfield">
									<input type="text" placeholder="Salary Max" value="" class="bg-white " id="Ca_salary_max" maxlength="6"/>
								</div>
							</div>
							
						</div>
					</div>
					
					<h4 class="location-head">Location --------------------------------------------------</h4>
	
						<div id="GetCAProfileAddress">
							
	
							<div class="row">
								<div class="col-md-4 pad-left0 pad-right0 mar-right5">
									<label class="pull-left remove-label">Zip Code
										</label>
									<div class="cfield">
										<input type="text" data-geocomplete="zip code" placeholder="Zipcode" value="" onchange="CheckArticleshipZipCode()" class="bg-white " id="Ca_Edit_zipcode"/>
									</div>
								</div>
				
								<div class="col-md-7 pad-left0 pad-right0">
									<label class="pull-left remove-label">Country
										</label>
									<div class="cfield">
										<input type="text" data-geocomplete="county" placeholder="Country" value="" class="bg-white " id="Ca_Edit_country"/>
									</div>
								</div>
							</div>
				
							<div class="row">
								<div class="col-md-5 pad-left0 pad-right0 mar-right5">
									<label class="pull-left remove-label">State
										</label>
									<div class="cfield">
										<input type="text" data-geocomplete="state" placeholder="State" value="" class="bg-white " id="Ca_Edit_state"/>
									</div>
								</div>
				
								<div class="col-md-5 pad-left0 pad-right0">
									<label class="pull-left remove-label">City
										</label>
									<div class="cfield">
										<input type="text" data-geocomplete="city" placeholder="City" value="" class="bg-white " id="Ca_Edit_city"/>
									</div>
								</div>
							</div>
	
							<div class="row">
								<div class="col-md-12 pad-left0">
									<label class="pull-left remove-label">Area
										</label>
									<div class="cfield">
										<input type="text" data-geocomplete="street address"  placeholder="Type Area" value="" class="bg-white " id="Ca_Edit_address"/>
									</div>
								</div>
								{{--  <div id="myForm">
									<input id="txtAddress1" data-geocomplete="street address" />
								</div>  --}}
							</div>
						</div>
	
					
		
				   
					<div class="row">
						<div class="col-md-6 pad-left0 pad-right0">
							<label class="pull-left remove-label">No Of Openings
								</label>
							<div class="cfield">
								<input type="text" placeholder="Position" value="" class="bg-white " id="Ca_positions"/>
							</div>
						</div>
					</div>
		
					<div class="row">
						<div class="col-md-7">
							<button type="button" class="btn btn-default candidate-profile-cancel-btn text-right close-popup" data-dismiss="modal">Close
							</button>
						</div>
						<div class="col-md-5">
							<button type="button" id="PostCAJobs" class="btn btn-default candidate-profile-save-btn">Save
								</button>
						</div>
		
					</div>
				</form>
			</div>
		</div>
		<!-- Post New Trainings POPUP -->
@endsection


@section('JSScript')
	<script>
		$(document).ready(function () {
			var $select = $(".duration_days");
			for (i=1;i<=60;i++){
				$select.append($('<option value='+i+'></option>').val(i).html(i))
			}
		});
	</script>
@endsection