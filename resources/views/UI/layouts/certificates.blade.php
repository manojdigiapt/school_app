@foreach($GetStudentDataByCertificates as $Certificate)
<div style="width:800px;height: 700px;padding: 40px;text-align:center;margin: 0 auto;background-image: url('UI/certificate/border.svg');background-repeat: no-repeat;">
        <div style="width:750px; height:650px; padding:20px; text-align:center;">
          <p style=" width: 20%;float: left;">
          @if($Certificate->profile_pic)
          <img src="/employer_profile/{{$Certificate->profile_pic}}" alt="">
          @else
          Company logo <br> (Institute Logo)
          @endif
          </p>
          <img src="UI/certificate/design.svg" alt="" style="
          width: 25%;
          float: left;
          padding-left: 17%;
      ">
          <img src="UI/Logo.svg" alt="" style="width: 20%;float: right;padding-bottom: 45px;padding-top: 20px;">
          <br>
          <br>
               <span style="font-size:70px; font-weight:200">Certificate </span>
               <br/>
               <img src="UI/certificate/design1.svg" alt="">
               <span style="font-size:30px;">of Completion</span>
               <img src="UI/certificate/design1.svg" alt="">
               <br><br>
                <span style="font-size:25px"><i>This certificate is proudly presented to</i></span>
               
               <br>
        <span style="font-size:60px" ><b>{{$Certificate->name}}</b></span><br/>
        <hr style="width: 40%;">
        <span style="font-size:25px"><i>has successfully completed the training program of</i></span> <br/>
        <span style="font-size:30px">{{$Certificate->title}}</span> <br/><br/> 
        <span style="font-size:20px;margin-left: -100%;"><i>On dated <b>{{$Certificate->training_end_date}}
         {{--  </b>with score of <b>4</b>/5</i></span> <br/><br/>  --}}
        <div style="width: 50%;float: left;margin-top: 5%;">      
          @if($Certificate->company_name) 
        
        <hr style="
        width: 50%;
        margin-left: 3%;
        margin-top: 10%;
    ">
    <span style="font-size:20px;margin-left: -110%;"><i>{{$Certificate->company_name}}</i></span>
      {{--  <p style="
      float: left;
      padding-left: 15%;
      margin-top: 0px;
  ">INSTITUTE</p>  --}}
  @endif

              <span style="font-size:30px;margin-left: -62%;"></span>
            </div>
            <div style="width: 50%;float: left;margin-top: 5%;">     
              <hr style="
              width: 50%;
              margin-top: 32px;
              margin-left: 50%;
              margin-bottom: -10px;
          "><br>
              <span style="font-size:25px;margin-left: 50%;"><i>Signature</i></span><br><br><br><br>

              <span style="font-size:30px;margin-left: -62%;"></span>
            </div>
        </div>
        </div>
    @endforeach