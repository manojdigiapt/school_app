@extends('UI.base')

@section('Content')
<section id="scroll-here" class="bg-grey">
    <div class="block">
        <div class="container">
            <div class="row candidate-row-mar-top">
                <div class="col-lg-12 col-md-6">
                    <div class="reviews-sec candiates-card-profile-top">
						<div class="row job-details-breadcrumb">
							<div class="col-lg-12 pad-top30">
								<p class="job-details-breadcrumb-p"><i class="fa fa-home clr-primary"></i> <i class="fa fa-caret-right"></i> Shortlisted jobs</p>
							</div>
						</div>


						<div class="filterbar applied-jobs-head-sortby mar-bottom0">
							{{-- <div class="sortby-sec candidate-profile-sortby">
								<select data-placeholder="20 Per Page" class="chosen">
									<option selected disabled>Sort by</option>
									<option>40 Per Page</option>
									<option>50 Per Page</option>
									<option>60 Per Page</option>
								</select>
							</div> --}}
							<h5>Shortlisted Jobs list</h5>
						</div>

						<div class="reviews-sec applied-jobs-list">
							<div class="row">
								@foreach($GetShortlistedCandidates as $Jobs)
									<div class="col-lg-12">
										<div class="reviews candidates-profile-card-border mar-top15 mobile-card-pad-right">
											<div class="row">
												
												<div class="col-md-12 pull-left pad-left30 mobile-job-details-align-center">
													<h3 class="mobile-review-head">{{$Jobs->company_name}}</h3>
													<br>
				
													{{-- <p class="job-details-attributes applied-jobs-attributes-width"><i class="fa fa-briefcase"> 
														@if($Jobs->job_type == 1)
															Part time
														@else
															Full time
														@endif
													</i></p> --}}
				
													<p class="job-details-attributes applied-jobs-attributes-width"><i class="fa fa-map-marker"> {{$Jobs->city}}</i></p>

													{{-- <p class="job-details-attributes applied-jobs-attributes-width"><i class="fa fa-industry"> {{$Jobs->name}}</i></p> --}}
												</div>
												
												<div class="row pull-left pad-left30 mobile-job-details-align-center">
                                                        <div class="col-md-6 applied-jobs-recruiter-status-pad shortlist-border-line-right">
                                                                <span class="applied-jobs-recruiter-dot">.</span> <span class="applied-jobs-border">Resume Downloaded</span>
        
                                                                <p class="applied-jobs-date">{{date('d M Y', strtotime($Jobs->created_at))}}</p>
                                                            </div>
        
        
                                                            <div class="col-md-2 applied-jobs-recruiter-status-pad applied-jobs-border-line shortlisted-jobs-border">
                                                                    <p class="applied-jobs-border-width"></p>
                                                                </div>
        
                                                            <div class="col-md-4 applied-jobs-recruiter-status-pad">
                                                                <span class="applied-jobs-recruiter-dot">.</span> <span class="applied-jobs-border">Shortlisted</span>
        
                                                                <p class="applied-jobs-date">{{date('d M Y', strtotime($Jobs->created_at))}}</p>
                                                            </div>
												</div>
											</div>
											
											
										</div>
									</div>
								@endforeach
							</div>
						</div>
                    </div>
                    
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
