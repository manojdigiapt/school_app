@extends('UI.base')

@section('Content')

<section id="scroll-here" class="bg-grey">
    <div class="block">
        <div class="container">
            <div class="row job-details-breadcrumb">
							<div class="col-lg-12 pad-top30">
								<p class="job-details-breadcrumb-p manage-jobs-breadcrumb-p"><i class="fa fa-home clr-primary"></i> <i class="fa fa-caret-right"></i> <a href="#" class="clr-primary">Jobs</a> <i class="fa fa-caret-right"></i> All Jobs</p>
							</div>
						</div>
            <div class="row candidate-row-mar-top manage-jobs-filter-mar-top">
                {{-- <div class="row job-details-breadcrumb">
                    <div class="col-lg-12 pad-top30">
                        <p class="job-details-breadcrumb-p"><i class="fa fa-home clr-primary"></i> > <a href="#" class="clr-primary">All jobs</a> > Job name</p>
                    </div>
                </div> --}}

                <div class="col-lg-4 col-md-6">
                    
                    <div class="widget border filter-card-candidate-profile filter-jobs-mar-bottom">
                        <h3 class="sb-title open Filters col-md-4 text-left pad-left0 mobile-filter-float-left">Filters (<span class="filter-count-font" id="AppliedCount">1</span>)</h3>
                        {{--  <h3 class="sb-title open Filters filter-applied col-md-4 mobile-filter-float-left" >Applied(<span id="AppliedCount">1</span>)</h3>  --}}
                        <a href="javascript:void(0);" class="pull-right open Filters col-md-4 filters-clearall mobile-filters-clear-all" id="ClearAllClosedTrainings">Clear All</a>
                        
                        <div class="posted_widget pad-bottom10">
                            <input type="text" class="search-input-bg" id="Closetrainings" placeholder="Search Keyword">
                            <a href="javascript:void(0);" id="SearchClosetrainings"><i class="fa fa-search search-icon-position"></i></a>
                        </div>

                        <h4 class="sb-title open">Date Posted</h4>
                        <div class="posted_widget pad-bottom10 managejobs-border-line">
                            <input type="radio" value="1" name="choose" id="232" class="CheckDatePostedCloseTrainingFilters" checked><label for="232">Last Hour</label><br />
                            <input type="radio" value="2" name="choose" class="CheckDatePostedCloseTrainingFilters" id="wwqe"><label for="wwqe">Last 24 hours</label><br />
                            <input type="radio" value="2" name="choose" class="CheckDatePostedCloseTrainingFilters" id="erewr"><label for="erewr">Last 7 days</label><br />
                            <input type="radio" value="4" name="choose" class="CheckDatePostedCloseTrainingFilters" id="all" checked><label for="all">All</label><br />
                        </div>

                        <div>
                            <h3 class="sb-title open">Location</h3>
                            {{--  <div class="type_widget pad-bottom10 managejobs-border-line">
                                <p><input type="checkbox" class="CheckCloseTrainingFilters" class="" value="Bangalore" name="Location" id="blr"><label for="blr" class="CheckBoxFilter">Bangalore</label></p>
							</div>  --}}
							<input type="text" class="search-input-bg" id="SearchCloseTrainingLocationByKeypress" placeholder="Search Location">

                            <div class="type_widget pad-bottom10 managejobs-border-line" id="AppendLocation">
                                {{--  <p><input type="checkbox" class="CheckCloseTrainingFilters" class="" value="Bangalore" name="Location" id="blr"><label for="blr" class="CheckBoxFilter">Bangalore</label></p>  --}}
                            </div>
                        </div>

                        <div>
							<h3 class="sb-title open">Duration</h3>
							<input type="text" class="search-input-bg" id="SearchDurationByCloseTraining" placeholder="Search Duration">
                            <div class="type_widget pad-bottom10 managejobs-border-line" id="AppendDuration">
                                {{--  <p><input type="checkbox" class="CheckCloseTrainingFilters" value="2" name="Duration" id="pt"><label for="pt" class="CheckBoxFilter">60 Days</label></p>  --}}
                            </div>
                        </div>

                    </div>
                    {{-- <div class="widget border filter-card-candidate-profile ">
                        
                    </div> --}}
                    {{-- <div class="widget border filter-card-candidate-profile filter-jobs-mar-top border-top0">
                        
                    </div> --}}
                    
                    {{-- <div class="widget border filter-card-candidate-profile filter-jobs-mar-top">
                        
                    </div> --}}

                    {{-- <div class="widget border filter-card-candidate-profile filter-jobs-mar-top">
                            
                    </div> --}}
                    {{-- <div class="widget border filter-card-candidate-profile filter-jobs-mar-top">
                            
                        </div> --}}
                </div>
                <div class="col-lg-8 col-md-6 recommended-jobs-list manage-jobs-pad-top">

                    <div class="job-list-modern manage-jobs-pad pad-top0">
                        <ul class="nav nav-tabs manage-jobs-tabs-border" role="tablist">
                                <li class="manage-jobs-active-left-mar"><a href="/Employer/ManageTrainings">Active</a></li>
                                <li class="active pull-left"><a href="/Employer/CloseTrainings">Close </a></li>
                            </ul>

                            <!-- Tab panes -->
                            <div class="tab-content">
                                <div role="tabpanel" class="tab-pane active pad-top0 text-center" id="home">
                                        <img id="AjaxLoader" class="jobs-search-loader" src="{{URL::asset('UI/ajax_loader.gif')}}" alt="" style="display:none;">
                                        <div class="job-list-modern" id="ManageTrainings">
                                            @if($GetTrainingByEmployerId)
                                            @foreach($GetTrainingByEmployerId as $Training)
                                            <div class="job-listings-sec no-border">
                                                <div class="job-listing wtabs pad-top0">
                                                    <div class="job-title-sec width100">
                                                    <h3 class="manage-jobs-list pad-bottom10"><a href="#" title="">{{$Training->title}}</a></h3>
                                                    <div class="job-lctn"><i class="fa fa-calendar"></i> {{date('d M Y', strtotime($Training->start_date))}}</div>
                                                        
                                                        <div class="job-lctn"><i class="fa fa-clock-o"></i> 
                                                        {{$Training->duration}}
                                                        </div>
                                                        <div class="job-lctn"><i class="fa fa-briefcase"></i> 
                                                        @php    
                                                            $num = $Training->stipend;
                                                            $units = ['', 'K', 'M'];
                                                            for($i = 0; $num>=1000;$i++){
                                                                $num /= 1000;
                                                            }
                                                            echo round($num, 1).$units[$i];
                                                        @endphp
                                                        
                                                         / Month</div>

                                                        <div class="job-lctn"><i class="fa fa-certificate"></i> 
                                                        @if($Training->training_completion == 1)
                                                            Certification
                                                        @else
                                                            Full Time Job
                                                        @endif
                                                        </div>

                                                        
                                                        <br>
                                                        <div class="row pad-top20">
                                                            <div class="col-md-4">
                                                            <button type="button" onclick="javascript:window.open('/ViewAllAppliedTrainees/{{$Training->id}}/{{$Training->slug}}', '_blank');" formtarget="_blank"  class="view-details-jobs"> <span class="view-details-dot">...</span> View Details</button>
                                                            </div>
                                                            
                                                            <div class="col-md-3">
                                                                <button type="button" onclick="EditTraining({{$Training->id}})" class="view-details-jobs"> <i class="fa fa-pencil"></i> Edit</button>
                                                            </div>
                                                            <div class="col-md-2">
                                                                <h3 class="text-left manage-jobs-status clr-red">Close</h3>
                                                            </div>
                                                            
                                                            {{-- <div class="col-md-3">
                                                                <p class="last-date-taining">Last Date: <span class="last-date-mar-pad-left">{{date('d M Y', strtotime($Training->last_date_of_application))}}</span></p>
                                                            </div> --}}
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            @endforeach
                                            @else
                                            <h5>No results found...</h5>
                                            @endif
                                        </div>
                                </div>

                                <div role="tabpanel" class="tab-pane pad-top0" id="profile">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</div>
                                
                            </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>



{{--  Post New Jobs  --}}
<div class="account-popup-area post-jobs-popup-box">
		<div class="account-popup post_new_jobs_popup">
			<span class="close-popup" id="register_close"><i class="la la-close"></i></span>
			<h3>Post a New Job</h3>
			<form class="mar-top15">
                    <div class="row">
                            <div class="dropdown-field col-md-12 pad-left0 pad-right0 mar-right10">
                                <label class="pull-left remove-label">Job Title *
                                    </label>
                                {{-- <div class="cfield">
                                    <input type="text" placeholder="UI Designer" value="" class="bg-white " id="title"/>
                                </div> --}}
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">

                                <select  data-placeholder="Please Select Functional Areas" class="chosen" id="title">
                                    <option selected>Select job title</option>
                                    @foreach($GetJobTitle as $Title)
                                <option value="{{$Title->id}}">{{$Title->name}}
                                    </option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

				<div class="row">
					<div class="dropdown-field col-md-5 pad-left0 pad-right0 mar-right10">
						<label class="pull-left remove-label">Employee Type *
						</label>
						<select data-placeholder="Please Select Industries" class="chosen" id="employee_type" onchange="CheckEmployeeType()">
							<option selected>Select Type
								</option>
							<option value="1">Fresher
                            </option>
                            <option value="2">Experienced
                                </option>
						</select>
					</div>
	
					<div class="dropdown-field col-md-5 pad-left0 pad-right0">
						<label class="pull-left remove-label">Job Type *
							</label>
						<select  data-placeholder="Please Select Functional Areas" class="chosen" id="job_type">
							<option selected>Select job type</option>
							<option value="1">Part time
							</option>
							<option value="2">Full time
								</option>
						</select>
					</div>
				</div>
				
				
	
				<div class="row">
					<div class="col-md-12 pad-left0 pad-right0 mar-right10">
						<label class="pull-left remove-label">Job Description
							</label>
						<div class="cfield">
							<textarea name="" id="description" class="bg-white"></textarea>
						</div>
					</div>
				</div>
				
				<div class="row">
					<div class="dropdown-field col-md-12 pad-left0 pad-right0 mar-right10">
						<label class="pull-left remove-label">Qualification
							</label>
						<div class="cfield">
							<input type="text" placeholder="Qualification" value="" class="bg-white" id="qualification"/>
						</div>

						{{--  <select  data-placeholder="Please Select Functional Areas" class="chosen" id="qualification">
							<option selected>Select qualification</option>
							@foreach($GetQualification as $Qualification)
						<option value="{{$Qualification->id}}">{{$Qualification->qualification}}
							</option>
							@endforeach
						</select>  --}}
					</div>
				</div>
	
                <h4 class="location-head">Location --------------------------------------------------</h4>
                <div id="GetAddress">
                    <div class="row">
                        <div class="col-md-12 pad-left0">
                            <label class="pull-left remove-label">Area
                                </label>
                            <div class="cfield">
                                <input type="text" data-geocomplete="street address"  placeholder="Type Area" value="" class="bg-white " id="address"/>
                            </div>
                        </div>
                        {{--  <div id="myForm">
                            <input id="txtAddress1" data-geocomplete="street address" />
                        </div>  --}}
                    </div>

                    <div class="row">
                        <div class="col-md-4 pad-left0 pad-right0 mar-right5">
                            <label class="pull-left remove-label">Zip Code
                                </label>
                            <div class="cfield">
                                <input type="text" placeholder="Zipcode" value="" class="bg-white" data-geocomplete="zip code" id="zipcode"/>
                            </div>
                        </div>
        
                        <div class="col-md-7 pad-left0 pad-right0">
                            <label class="pull-left remove-label">Country
                                </label>
                            <div class="cfield">
                                <input type="text" placeholder="Country" value="" class="bg-white " data-geocomplete="county" id="country"/>
                            </div>
                        </div>
                    </div>
        
                    <div class="row">
                        <div class="col-md-5 pad-left0 pad-right0 mar-right5">
                            <label class="pull-left remove-label">State
                                </label>
                            <div class="cfield">
                                <input type="text" data-geocomplete="state" placeholder="State" value="" class="bg-white " id="state"/>
                            </div>
                        </div>
        
                        <div class="col-md-5 pad-left0 pad-right0">
                            <label class="pull-left remove-label">City
                                </label>
                            <div class="cfield">
                                <input type="text" placeholder="City" value="" class="bg-white" data-geocomplete="city" id="city"/>
                            </div>
                        </div>
                    </div>
                </div>
                
                    
				<h4 class="location-head">Salary and Experience -----------------------</h4>
	
				<div class="row">
                        <div class="col-md-4 pad-left0 pad-right0 salary-column-width">
                                <label class="pull-left remove-label">Salary Type *
                                    </label>
                                <select  data-placeholder="Please Select Functional Areas" class="chosen" id="salary_type" onchange="SalaryType()">
                                    <option value="1">Monthly
                                    </option>
                                    <option value="2">Yearly
                                    </option>
                                </select>
                            </div>
					<div class="col-md-4 pad-left0 pad-right0 salary-column-width">
						<label class="pull-left remove-label">Salary Min
							</label>
						<div class="cfield">
							<input type="text" placeholder="Salary Min" value="" class="bg-white " id="salary_min"/>
						</div>
					</div>
	
					<div class="col-md-4 pad-left0 pad-right0 salary-column-width">
						<label class="pull-left remove-label">Salary Max
							</label>
						<div class="cfield">
							<input type="text" placeholder="Salary Max" value="" class="bg-white " id="salary_max"/>
						</div>
					</div>
					
				</div>
	
				<div class="row">
					<div class="col-md-7 pad-left0 pad-right0 mar-right5" id="CheckExperienced">
						<label class="pull-left remove-label">Experience (Years)
							</label>
						<div class="cfield">
							<input type="text" placeholder="Experience" value="" class="bg-white " id="experience"/>
						</div>
					</div>
	
					<div class="col-md-4 pad-left0 pad-right0">
						<label class="pull-left remove-label">No Of Position
							</label>
						<div class="cfield">
							<input type="text" placeholder="Position" value="" class="bg-white " id="positions"/>
						</div>
					</div>
				</div>
	
				<div class="row">
					<div class="col-md-7">
						<button type="button" class="btn btn-default candidate-profile-cancel-btn text-right close-popup" data-dismiss="modal">Close
                        </button>
					</div>
					<div class="col-md-5">
						<button type="button" id="PostJobs" class="btn btn-default candidate-profile-save-btn">Save
							</button>
					</div>
	
				</div>
			  </form>
		</div>
	</div>
	<!-- Post New Jobs POPUP -->

{{--  Post Trainings  --}}
<div class="account-popup-area post-trainings-popup-box">
		<div class="account-popup post_new_jobs_popup">
			<span class="close-popup" id="register_close"><i class="la la-close"></i></span>
			<h3>Post Internships</h3>
			<form class="mar-top15">
				
				<div class="row">
					<div class="dropdown-field col-md-12 pad-left0 pad-right0 mar-right10">
						<label class="pull-left remove-label">Training Name *
							</label>
						{{-- <div class="cfield">
							<input type="text" placeholder="Training Name" value="" class="bg-white " id="training_title"/>
						</div> --}}
						<select  data-placeholder="Please Select Functional Areas" class="chosen" id="training_title">
							<option selected>Select training title
							</option>
							@foreach($GetTrainingTitle as $Title)
								<option value="{{$Title->id}}">{{$Title->title}}
								</option>
							@endforeach
						</select>
					</div>
				</div>
	
				<div class="row">
					<div class="col-md-5 pad-left0 pad-right0 mar-right5">
						<label class="pull-left remove-label">Start Date * 
							</label>
						<div class="cfield">
							<input type="date" placeholder="Start Date" value="" class="bg-white" name="start_date" id="start_date"/>
						</div>
					</div>
	
					<div class="col-md-3 pad-left0 pad-right0 training-duration-marright">
						<label class="pull-left remove-label">Duration *
						</label>
						<select  data-placeholder="Please select days" class="dropdown-custom-clr-pad duration_days" id="duration_days">
							{{-- <option value="1">30
							</option>
							<option value="2">60
							</option> --}}
						</select>
					</div>
	
					<div class="col-md-3 pad-left0 pad-right0">
						<label class="pull-left remove-label">
						</label>
						<select  data-placeholder="Please Select Functional Areas" class="chosen" id="duration">
							<option value="Days">Days
							</option>
							<option value="Months">Months
							</option>
						</select>
					</div>
				</div>
	
				<div class="row">
					<div class="col-md-5 pad-left0 pad-right0 mar-right5">
						<label class="pull-left remove-label">Monthly Stipend * 
							</label>
						 <div class="cfield">
							<input type="text" placeholder="Monthly stipend" value="" class="bg-white " id="stipend" maxlength="6"/>
						</div> 
					</div>
	
					<div class="col-md-5 pad-left0 pad-right0">
						<label class="pull-left remove-label">Last Date of application *
							</label>
						<div class="cfield">
							<input type="date" placeholder="Last date of application" value="" class="bg-white"  id="last_date"/>
						</div>
					</div>
				</div>
	
				<div class="row">
					<div class="col-md-12 pad-left0 pad-right0 mar-right10">
						<label class="pull-left remove-label">Internship Description
							</label>
						<div class="cfield">
							<textarea name="" id="training_description" class="bg-white"></textarea>
						</div>
					</div>
				</div>
	
			   
				<div class="row">
					<div class="col-md-12 pad-left0 pad-right0 mar-right5">
						<label class="pull-left remove-label">What trainee will get after Internship completion *
						</label>
						<select  data-placeholder="Please Select Functional Areas" class="chosen" id="completion">
							<option selected>Select type
							</option>
							<option value="1">Certification
							</option>
							<option value="2">Certification + Job Offers
							</option>
						</select>
					</div>
				</div>
	
				<div class="row">
					<div class="col-md-7">
						<button type="button" class="btn btn-default candidate-profile-cancel-btn text-right close-popup" data-dismiss="modal">Close
							</button>
					</div>
					<div class="col-md-5">
						<button type="button" id="PostTrainings" class="btn btn-default candidate-profile-save-btn">Save
							</button>
					</div>
	
				</div>
			</form>
		</div>
	</div>
	<!-- Post New Trainings POPUP -->


{{--  Edit Trainings  --}}
<div class="account-popup-area edit-trainings-popup-box">
        <div class="account-popup post_new_jobs_popup">
            <span class="close-popup" id="register_close"><i class="la la-close"></i></span>
            <h3>Edit Trainings</h3>
            <form class="mar-top15">
            
                <div class="row">
                    <div class="dropdown-field col-md-12 pad-left0 pad-right0 mar-right10">
                        <label class="pull-left remove-label">Training Name *
                            </label>
                        <div class="cfield">
                                <input type="hidden" name="" id="TrainingId">
							{{-- <input type="text" placeholder="Training Name" value="" class="bg-white " id="Edit_training_title"/> --}}
							<select  data-placeholder="Please Select Functional Areas" class="dropdown-custom-clr-pad" id="Edit_training_title">
								<option selected>Select training title
								</option>
								@foreach($GetTrainingTitle as $Title)
									<option value="{{$Title->id}}">{{$Title->title}}
									</option>
								@endforeach
							</select>
                        </div>
                    </div>
                </div>
    
                <div class="row">
                    <div class="col-md-5 pad-left0 pad-right0 mar-right5">
                        <label class="pull-left remove-label">Start Date * 
                            </label>
                        <div class="cfield">
                            <input type="date" placeholder="Start Date" value="" class="bg-white " id="Edit_start_date"/>
                        </div>
                    </div>
    
                    {{-- <div class="col-md-5 pad-left0 pad-right0">
                        <label class="pull-left remove-label">Duration *
                        </label>
                        <select  data-placeholder="Please Select Functional Areas" class="dropdown-custom-clr-pad" id="Edit_duration">
                            <option value="1">30 Days
                            </option>
                            <option value="2">60 Days
                            </option>
                        </select>
                    </div> --}}

                    <div class="col-md-3 pad-left0 pad-right0 training-duration-marright">
                        <label class="pull-left remove-label">Duration *
                        </label>
                        <select  data-placeholder="Please select days" class="dropdown-custom-clr-pad duration_days" id="Edit_duration_days">
                            {{-- <option value="1">30
                            </option>
                            <option value="2">60
                            </option> --}}
                        </select>
                    </div>

                    <div class="col-md-3 pad-left0 pad-right0">
                        <label class="pull-left remove-label">
                        </label>
                        <select  data-placeholder="Please Select Functional Areas" class="dropdown-custom-clr-pad" id="Edit_duration">
                            <option value="Days">Days
                            </option>
                            <option value="Months">Months
                            </option>
                        </select>
                    </div>
                </div>
    
                <div class="row">
                    <div class="col-md-5 pad-left0 pad-right0 mar-right5">
                        <label class="pull-left remove-label">Monthly Stipend * 
                            </label>
                        {{-- <select  data-placeholder="Please Select Functional Areas" class="dropdown-custom-clr-pad" id="Edit_stipend">
                            @foreach($GetStipend as $Stipend)
                                <option value="{{$Stipend->id}}">{{$Stipend->training_completion}}
                                </option>
                            @endforeach
                        </select> --}}
                         <div class="cfield">
                            <input type="text" placeholder="Monthly stipend" value="" class="bg-white " id="Edit_stipend" maxlength="6"/>
                        </div> 
                    </div>
    
                    <div class="col-md-5 pad-left0 pad-right0">
                        <label class="pull-left remove-label">Last Date of application *
                            </label>
                        <div class="cfield">
                            <input type="date" placeholder="Last date of application" value="" class="bg-white " id="Edit_last_date"/>
                        </div>
                    </div>
                </div>
    
                <div class="row">
                    <div class="col-md-12 pad-left0 pad-right0 mar-right10">
                        <label class="pull-left remove-label">Job Description
                            </label>
                        <div class="cfield">
                            <textarea name="" id="Edit_description" class="bg-white"></textarea>
                        </div>
                    </div>
                </div>
    
                
                <div class="row">
                    <div class="col-md-12 pad-left0 pad-right0 mar-right5">
                        <label class="pull-left remove-label">What trainee will get after training completion *
                        </label>
                        <select  data-placeholder="Please Select Functional Areas" class="dropdown-custom-clr-pad" id="Edit_completion">
                            <option selected>Select type
                            </option>
                            <option value="1">Certification
                            </option>
                            <option value="2">Certification + Job Offer
                            </option>
                        </select>
                    </div>
                </div>
    
                <div class="row">
                    <div class="col-md-7">
                        <button type="button" class="btn btn-default candidate-profile-cancel-btn text-right close-popup" data-dismiss="modal">Close
                        </button>
                    </div>
                    <div class="col-md-5">
                        <button type="button" id="UpdateTrainings" class="btn btn-default candidate-profile-save-btn">Save
                            </button>
                    </div>
    
                </div>
            </form>
        </div>
    </div>
    <!-- Edit Trainings POPUP -->

	 {{--  Post CA Articleships  --}}
<div class="account-popup-area post-ca-jobs-popup-box">
		<div class="account-popup post_new_jobs_popup">
			<span class="close-popup" id="register_close"><i class="la la-close"></i></span>
			<h3>Post CA Articleships</h3>
			<form class="mar-top15">
				
                <div class="row">
                        <div class="col-md-12 pad-left0 pad-right0 mar-right10">
                            <label class="pull-left remove-label">Description
                                </label>
                            <div class="cfield">
                                <textarea name="" id="Ca_description" class="bg-white"></textarea>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="dropdown-field col-md-12 pad-left0 pad-right0 mar-right10">
                            <label class="pull-left remove-label">Qualification
                                </label>
                            <div class="cfield">
                                <input type="text" placeholder="Qualification" value="" class="bg-white" id="Ca_qualification"/>
                            </div>
    
                            {{--  <select  data-placeholder="Please Select Functional Areas" class="chosen" id="Ca_qualification">
                                <option selected>Select qualification</option>
                                @foreach($GetQualification as $Qualification)
                            <option value="{{$Qualification->id}}">{{$Qualification->qualification}}
                                </option>
                                @endforeach
                            </select>  --}}
                        </div>
                    </div>
                    
				<div class="row">
					<div class="col-md-5 pad-left0 pad-right0 mar-right5">
						<label class="pull-left remove-label">Monthly Stipend * 
							</label>
						<select  data-placeholder="Please Select Functional Areas" class="chosen" id="stipend_type" onchange="CheckStipend()">
                            <option value="1">ICAI norms</option>
                            <option value="2">Others</option>
                        </select>
					</div>
	
					{{-- <div class="col-md-5 pad-left0 pad-right0 mar-right5" id="ShowStipend">
                        <label class="pull-left remove-label">Monthly Stipend * 
                            </label>
                        
                        <div class="cfield">
                            <input type="text" placeholder="Monthly stipend" value="" class="bg-white " id="Ca_stipend"/>
                        </div> 
                    </div> --}}
                </div>
                
                <div id="SalaryShow" style="display:none;">
                    <h4 class="location-head">Salary and Experience -----------------------</h4>

                    <div class="row">
                            <div class="col-md-4 pad-left0 pad-right0 salary-column-width">
                                    <label class="pull-left remove-label">Salary Type *
                                        </label>
                                    <select  data-placeholder="Please Select Functional Areas" class="chosen" id="Ca_salary_type">
                                        <option value="1">Monthly
                                        </option>
                                        <option value="2">Yearly
                                        </option>
                                    </select>
                                </div>
                        <div class="col-md-4 pad-left0 pad-right0 salary-column-width">
                            <label class="pull-left remove-label">Salary Min
                                </label>
                            <div class="cfield">
                                <input type="text" placeholder="Salary Min" value="" class="bg-white " id="Ca_salary_min" maxlength="6"/>
                            </div>
                        </div>
        
                        <div class="col-md-4 pad-left0 pad-right0 salary-column-width">
                            <label class="pull-left remove-label">Salary Max
                                </label>
                            <div class="cfield">
                                <input type="text" placeholder="Salary Max" value="" class="bg-white " id="Ca_salary_max" maxlength="6"/>
                            </div>
                        </div>
                        
                    </div>
                </div>
                
                <h4 class="location-head">Location --------------------------------------------------</h4>

                    <div id="GetCAProfileAddress">
                        <div class="row">
                            <div class="col-md-12 pad-left0">
                                <label class="pull-left remove-label">Area
                                    </label>
                                <div class="cfield">
                                    <input type="text" data-geocomplete="street address"  placeholder="Type Area" value="" class="bg-white " id="Ca_Edit_address"/>
                                </div>
                            </div>
                            {{--  <div id="myForm">
                                <input id="txtAddress1" data-geocomplete="street address" />
                            </div>  --}}
                        </div>

                        <div class="row">
                            <div class="col-md-4 pad-left0 pad-right0 mar-right5">
                                <label class="pull-left remove-label">Zip Code
                                    </label>
                                <div class="cfield">
                                    <input type="text" data-geocomplete="zip code" placeholder="Zipcode" value="" class="bg-white " id="Ca_Edit_zipcode"/>
                                </div>
                            </div>
            
                            <div class="col-md-7 pad-left0 pad-right0">
                                <label class="pull-left remove-label">Country
                                    </label>
                                <div class="cfield">
                                    <input type="text" data-geocomplete="county" placeholder="Country" value="" class="bg-white " id="Ca_Edit_country"/>
                                </div>
                            </div>
                        </div>
            
                        <div class="row">
                            <div class="col-md-5 pad-left0 pad-right0 mar-right5">
                                <label class="pull-left remove-label">State
                                    </label>
                                <div class="cfield">
                                    <input type="text" data-geocomplete="state" placeholder="State" value="" class="bg-white " id="Ca_Edit_state"/>
                                </div>
                            </div>
            
                            <div class="col-md-5 pad-left0 pad-right0">
                                <label class="pull-left remove-label">City
                                    </label>
                                <div class="cfield">
                                    <input type="text" data-geocomplete="city" placeholder="City" value="" class="bg-white " id="Ca_Edit_city"/>
                                </div>
                            </div>
                        </div>
                    </div>

				
	
			   
				<div class="row">
					<div class="col-md-6 pad-left0 pad-right0">
						<label class="pull-left remove-label">No Of Openings
							</label>
						<div class="cfield">
							<input type="text" placeholder="Position" value="" class="bg-white " id="Ca_positions"/>
						</div>
					</div>
				</div>
	
				<div class="row">
					<div class="col-md-7">
						<button type="button" class="btn btn-default candidate-profile-cancel-btn text-right close-popup" data-dismiss="modal">Close
                        </button>
					</div>
					<div class="col-md-5">
						<button type="button" id="PostCAJobs" class="btn btn-default candidate-profile-save-btn">Save
							</button>
					</div>
	
				</div>
			</form>
		</div>
	</div>
	<!-- Post New Trainings POPUP -->
    
@endsection


@section('JSScript')
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js">
</script>
<script>
  $('#start_date').datepicker({
    onSelect: function(value, ui) {
      var today = new Date(),
          dob = new Date(value),
          age = new Date(today - dob).getFullYear() - 1970;
      $('#age').val(age);
    }
    ,
    maxDate: '+0d',
    yearRange: '1960:2019',
    changeMonth: true,
    changeYear: true
  }
            );
            
            $('#Edit_dob').datepicker({
    onSelect: function(value, ui) {
      var today = new Date(),
          dob = new Date(value),
          age = new Date(today - dob).getFullYear() - 1970;
      $('#age').val(age);
    }
    ,
    maxDate: '+0d',
    yearRange: '1960:2019',
    changeMonth: true,
    changeYear: true
  }
					  );
</script>

<script>
		$(document).ready(function () {
			var $select = $(".duration_days");
			for (i=1;i<=60;i++){
				$select.append($('<option value='+i+'></option>').val(i).html(i))
			}
		});
	</script>
@endsection 
