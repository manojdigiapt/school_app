@extends('UI.base')

@section('Content')
<section class="overlape">
    <div class="block no-padding">
        <div data-velocity="-.1" style="background: url({{URL::asset('UI/images/resource/mslider1.jpg')}} repeat scroll 50% 422.28px transparent;" class="parallax scrolly-invisible no-parallax"></div><!-- PARALLAX BACKGROUND IMAGE -->
        <div class="container fluid">
            <div class="row">
                <div class="col-lg-12">
                    <div class="inner-header">
                        <h3>Welcome {{Session::get('TraineeName')}}</h3>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section>
    <div class="block no-padding">
        <div class="container">
             <div class="row no-gape">
                {{-- @include('UI.common.trainee_sidebar') --}}
                 <div class="col-lg-12 column">
                     <div class="padding-left">
                        <div class="contact-edit">
                            <form>
                                <div class="col-lg-4">
                                    <button type="button" onclick="window.location.href='/Trainee/Dashboard'" style="float:left;">Back to dashboard</button>
                                </div>
                                <div class="col-lg-8">
                                </div>
                            </form>
                        </div>
                         <div class="manage-jobs-sec">
                             <h3>Selected Trainings</h3>
                             <table>
                                 <thead>
                                     <tr>
                                         <td>Title</td>
                                         <td>Created & Expired</td>
                                         <td>Training Status</td>
                                         <td>Ratings</td>
                                         <td>Download Certificate</td>
                                     </tr>
                                 </thead>
                                 <tbody>
                                     @foreach($GetCandidates as $Trainings)
                                     <tr>
                                         <td>
                                             <div class="table-list-title">
                                             <h3><a href="#" title="">{{$Trainings->title}}</a></h3>
                                                 <span><i class="la la-clock-o"></i>{{$Trainings->duration}} days</span>
                                             </div>
                                         </td>
                                         <td>
                                             {{--  <span>October 27, 2017</span><br />  --}}
                                         <span>{{date('M d, Y', strtotime($Trainings->training_start_date))}}</span><br>
                                             <span>{{date('M d, Y', strtotime($Trainings->training_end_date))}}</span>
                                         </td>
                                         <td>
                                         <span class="status active">
                                            @if($Trainings->training_status == 1)
                                            <span class="badge badge-success badge-font">Completed</span>
                                            @else
                                            <span class="badge badge-danger badge-font">Not Completed</span>
                                            @endif
                                        </span>
                                         </td>
                                         <td>
                                            @if($Trainings->training_status == 0)
                                            <span class="status active">
                                                <span class="badge badge-danger badge-font">No Ratings</span>
                                            </span>
                                            @else
                                            <div class="selected-rating-star">
                                                @foreach($GetRatings as $Ratings)
                                                @if($Ratings->review_stars == 1)
                                                <span data-id="001" data-val="1" class="clicked"></span>
                                                <span data-id="002" data-val="2"></span>
                                                <span data-id="003" data-val="3"></span>
                                                <span data-id="004" data-val="4"></span>
                                                <span data-id="005" data-val="5"></span>
                                                @elseif($Ratings->review_stars == 2)
                                                <span data-id="001" data-val="1" class="clicked"></span>
                                                <span data-id="002" data-val="2" class="clicked"></span>
                                                <span data-id="003" data-val="3"></span>
                                                <span data-id="004" data-val="4"></span>
                                                <span data-id="005" data-val="5"></span>
                                                @elseif($Ratings->review_stars == 3)
                                                <span data-id="001" data-val="1" class="clicked"></span>
                                                <span data-id="002" data-val="2" class="clicked"></span>
                                                <span data-id="003" data-val="3" class="clicked"></span>
                                                <span data-id="004" data-val="4"></span>
                                                <span data-id="005" data-val="5"></span>
                                                @elseif($Ratings->review_stars == 4)
                                                <span data-id="001" data-val="1" class="clicked"></span>
                                                <span data-id="002" data-val="2" class="clicked"></span>
                                                <span data-id="003" data-val="3" class="clicked"></span>
                                                <span data-id="004" data-val="4" class="clicked"></span>
                                                <span data-id="005" data-val="5"></span>
                                                @elseif($Ratings->review_stars == 5)
                                                <span data-id="001" data-val="1" class="clicked"></span>
                                                <span data-id="002" data-val="2" class="clicked"></span>
                                                <span data-id="003" data-val="3" class="clicked"></span>
                                                <span data-id="004" data-val="4" class="clicked"></span>
                                                <span data-id="005" data-val="5" class="clicked"></span>
                                                @endif
                                                @endforeach
                                            </div>
                                            @endif
                                        </td>
                                         <td>
                                             <ul class="action_job">
                                             <li><a href="/GetStudentDataByCertificates" target="_blank">Download now</a></li>
                                    
                                            {{-- <li><span>Delete</span><a href="#" title=""><i class="la la-trash-o"></i></a></li> --}}
                                             </ul>
                                         </td>
                                     </tr>
                                     @endforeach
                                 </tbody>
                             </table>
                         </div>
                     </div>
                </div>
             </div>
        </div>
    </div>
</section>
@endsection