@extends('UI.base')

@section('Content')
<section class="overlape">
		<div class="block no-padding">
			<div data-velocity="-.1" style="background: url({{URL::asset('UI/images/resource/mslider1.jpg')}}" repeat scroll 50% 422.28px transparent;" class="parallax scrolly-invisible no-parallax"></div><!-- PARALLAX BACKGROUND IMAGE -->
			<div class="container fluid">
				<div class="row">
					<div class="col-lg-12">
						<div class="inner-header wform">
							<div class="job-search-sec">
								<div class="job-search">
									<h4>Explore Thousand Of Jobs With Just Simple Search...</h4>
									<form>
										<div class="row">
											<div class="col-lg-5">
												<div class="job-field">
													<input type="text" name="SearchJobsTitleOrCompanyName" id="SearchJobsTitleOrCompanyName" placeholder="Jobs title, keywords or company name" />
													<i class="la la-keyboard-o"></i>
												</div>
											</div>
											<div class="col-lg-3">
												<div class="job-field">
													<select data-placeholder="Country, province or region" class="chosen-city" id="state">
														<option selected disabled>Please select state</option>
														<option value="India">India</option>
														<option value="NY">New York</option>
														<option value="London">London</option>
														<option value="demo">Demo</option>
													</select>
													<i class="la la-map-marker"></i>
												</div>
											</div>
											<div class="col-lg-3">
												<div class="job-field">
													<select data-placeholder="City, province or region" class="chosen-city" id="city">
														<option selected disabled>Please select city</option>
														<option value="Bangalore">Bangalore</option>
														<option value="CN">Chennai</option>
														<option value="test">Test</option>
													</select>
													<i class="la la-map-marker"></i>
												</div>
											</div>
											<div class="col-lg-1">
												<button type="button" id="SearchJobs"><i class="la la-search"></i></button>
											</div>
										</div>
									</form>
									{{--  <div class="tags-bar">
								 		<span>Full Time<i class="close-tag">x</i></span>
								 		<span>UX/UI Design<i class="close-tag">x</i></span>
								 		<span>Istanbul<i class="close-tag">x</i></span>
								 		<div class="action-tags">
								 			<a href="#" title=""><i class="la la-cloud-download"></i> Save</a>
								 			<a href="#" title=""><i class="la la-trash-o"></i> Clean</a>
								 		</div>
								 	</div><!-- Tags Bar -->  --}}
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

	<section>
		<div class="block remove-top">
			<div class="container">
				 <div class="row no-gape">
				 	<aside class="col-lg-3 column">
				 		<div class="widget border">
				 			<h3 class="sb-title open">Date Posted</h3>
				 			<div class="posted_widget">
								<input type="radio" name="choose" value="1" class="CheckFilterJobs" id="232"><label for="232">Last Hour</label><br />
								<input type="radio" value="2" class="CheckFilterJobs" name="choose" id="wwqe"><label for="wwqe">Last 24 hours</label><br />
								<input type="radio" value="3" class="CheckFilterJobs" name="choose" id="erewr"><label for="erewr">Last 7 days</label><br />
								<input type="radio" value="4" class="CheckFilterJobs" name="choose" id="qwe"><label for="qwe">Last 14 days</label><br />
								<input type="radio" value="5" class="CheckFilterJobs" name="choose" id="wqe"><label for="wqe">Last 30 days</label><br />
								<input type="radio" value="6" class="CheckFilterJobs" name="choose" id="qweqw"><label class="nm" for="qweqw">All</label><br />
				 			</div>
						 </div>
						 {{-- <div class="widget border">
							<h3 class="sb-title closed">Salary Range</h3>
							<div class="specialism_widget">
								<div id="slider-container"></div>
									<p>
									<h5 id="amount"></h5>
									</p>
									<div id="slider-range"></div>
							</div>
						</div> --}}

				 		<div class="widget border">
				 			<h3 class="sb-title open">Job Type</h3>
				 			<div class="type_widget">
								<p class="flchek"><input type="checkbox" name="CheckJobType[]" id="33r" value="1" class="CheckJobType"><label for="33r">Freelance (9)</label></p>
								<p class="ftchek"><input type="checkbox" name="CheckJobType[]" id="dsf" value="2" class="CheckJobType"><label for="dsf">Full Time (8)</label></p>
								<p class="ischek"><input type="checkbox" class="CheckJobType" value="3" name="CheckJobType[]" id="sdd"><label for="sdd">Internship (8)</label></p>
								<p class="ptchek"><input type="checkbox" class="CheckJobType" value="4" name="CheckJobType[]" id="sadd"><label for="sadd">Part Time (5)</label></p>
				 			</div>
				 		</div>
				 		<div class="widget border">
				 			<h3 class="sb-title open">Industry</h3>
				 			<div class="specialism_widget">
								{{-- <div class="field_w_search">
				 					<input type="text" placeholder="Search Spaecialisms" />
				 				</div><!-- Search Widget --> --}}
				 				<div class="simple-checkbox scrollbar">
									<p><input type="checkbox" class="CheckIndustry" name="CheckIndustry[]" id="as" value="Software Design"><label for="as">Software Design (2)</label></p>
									<p><input type="checkbox" class="CheckIndustry" name="CheckIndustry[]" id="asd"><label for="asd">Banking (2)</label></p>
									<p><input type="checkbox" class="CheckIndustry" name="CheckIndustry[]" id="errwe"><label for="errwe">Charity & Voluntary (3)</label></p>
									<p><input type="checkbox" class="CheckIndustry" name="CheckIndustry[]" value="Software Developement" id="fdg"><label for="fdg">Software Developement (4)</label></p>
									<p><input type="checkbox" class="CheckIndustry" name="CheckIndustry[]" id="sc"><label for="sc">Estate Agency (3)</label></p>
									<p><input type="checkbox" class="CheckIndustry" name="CheckIndustry[]" id="aw"><label for="aw">Graduate (2)</label></p>
									
				 				</div>
				 			</div>
						 </div>
						 
						 <div class="widget border">
							<h3 class="sb-title open">Desired Location</h3>
							<div class="specialism_widget">
								{{-- <div class="field_w_search">
									<input type="text" placeholder="Search City" />
								</div><!-- Search Widget --> --}}
								<div class="simple-checkbox scrollbar">
									<p><input type="checkbox" class="CheckCity" name="CheckCity[]" value="Bangalore" id="blr"><label for="blr">Bangalore</label></p>
									<p><input type="checkbox" class="CheckCity" name="CheckCity[]" id="delhi" value="Delhi"><label for="delhi">Delhi</label></p>
									<p><input type="checkbox" class="CheckCity" name="CheckCity[]" id="chennai" value="Chennai"><label for="chennai">Chennai</label></p>									
								</div>
							</div>
						</div>
				 		{{-- <div class="widget border">
				 			<h3 class="sb-title closed">Offerd Salary</h3>
				 			<div class="specialism_widget">
				 				<div class="simple-checkbox">
									<p><input type="checkbox" name="smplechk" id="1"><label for="1">10k - 20k</label></p>
									<p><input type="checkbox" name="smplechk" id="2"><label for="2">20k - 30k</label></p>
									<p><input type="checkbox" name="smplechk" id="3"><label for="3">30k - 40k</label></p>
									<p><input type="checkbox" name="smplechk" id="4"><label for="4">40k - 50k</label></p>
				 				</div>
				 			</div>
						 </div> --}}
						 

				 		{{-- <div class="widget border">
				 			<h3 class="sb-title closed">Eexperience Type</h3>
				 			<div class="specialism_widget">
				 				<div class="simple-checkbox">
									<p><input type="checkbox" name="smplechk" id="16"><label for="16">Meezan Job</label></p>
									<p><input type="checkbox" name="smplechk" id="17"><label for="17">Speicalize Jobs</label></p>
									<p><input type="checkbox" name="smplechk" id="18"><label for="18">Business Jobs</label></p>
				 				</div>
				 			</div>
				 		</div> --}}
				 		{{-- <div class="widget border">
				 			<h3 class="sb-title closed">Qualification</h3>
				 			<div class="specialism_widget">
				 				<div class="simple-checkbox">
									<p><input type="checkbox" name="smplechk" id="19"><label for="19">Matriculation</label></p>
									<p><input type="checkbox" name="smplechk" id="20"><label for="20">Intermidiate</label></p>
									<p><input type="checkbox" name="smplechk" id="21"><label for="21">Gradute</label></p>
				 				</div>
				 			</div>
				 		</div> --}}
			 			<div class="banner_widget">
			 				<a href="#" title=""><img src="{{URL::asset('UI/images/resource/banner.png')}}" alt="" /></a>
						</div>
				 	</aside>
				 	<div class="col-lg-9 column">
				 		<div class="modrn-joblist np">
					 		<div class="filterbar">
					 			{{-- <span class="emlthis"><a href="https://grandetest.com/cdn-cgi/l/email-protection#dbbea3bab6abb7bef5b8b4b6" title=""><i class="la la-envelope-o"></i> Email me Jobs Like These</a></span> --}}
					 			<div class="sortby-sec">
									<select data-placeholder="20 Per Page" onchange="SortByJobs()" id="sortby" class="chosen">
										<option selected disabled>Sort by</option>
										<option value="1">30 Per Page</option>
										<option value="2">40 Per Page</option>
										<option value="3">50 Per Page</option>
										<option value="4">60 Per Page</option>
									</select>
					 			</div>
					 			{{-- <h5>{{count($GetJobs)}} Jobs & Vacancies</h5> --}}
					 		</div>
						 </div><!-- MOdern Job LIst -->
						 <div class="job-list-modern">
						 	<div class="job-listings-sec no-border" id="SearchJobsResults">
								<div class="text-center" id="AjaxLoader" style="display: none;">
									<img src="{{URL::asset('UI/loader.gif')}}" alt="">
								</div>
                                 @foreach($GetJobs as $Jobs)
								<div class="job-listing wtabs">
									<div class="job-title-sec">
										<div class="c-logo"> <img src="{{URL::asset('employer_profile')}}/{{$Jobs->profile_pic}}" alt="" /> </div>
									<h3><a href="/JobDetails/{{$Jobs->CompanySlug}}/{{$Jobs->id}}/{{$Jobs->slug}}" target="_blank" title="">{{$Jobs->title}}</a></h3>
										<span>{{$Jobs->company_name}}</span>
										<div class="job-lctn"><i class="la la-map-marker"></i>{{$Jobs->location}}</div>
									</div>
									<div class="job-style-bx">
										@if($Jobs->job_type == 1)
										<span class="job-is ft">
											Full time
										</span>
										@else
										<span class="job-is pt">
											Part time
										</span>
                                        @endif
                                    
										{{-- <span class="fav-job"><i class="la la-heart-o"></i></span> --}}
										
										<i>5 months ago</i>
									</div>
                                </div>
								@endforeach
								<div class="col-lg-12" id="remove-row">
									<div class="browse-all-cat">
										<div class="text-center" id="ApplyBtnLoader" style="display: none;">
											<img class="applyJobsLoader" src="{{URL::asset('UI/apply_btn_loader.gif')}}" alt="">
										</div>
										@if(isset($Jobs))
										<a href="javascript:void(0);" data-id="{{ $Jobs->id }}" id="LoadmoreJobs" title="" class="style2">Load more jobs</a>
										@endif
									</div>
								</div>
							</div>
							
						 </div>
					 </div>
				 </div>
			</div>
		</div>
	</section>
@endsection

@section('JSScript')
{{-- <script src="https://code.jquery.com/jquery-1.12.4.js"></script> --}}
<script src="http://code.jquery.com/ui/1.10.2/jquery-ui.js"></script>

<script src="{{URL::asset('UI/js/custom/autocomplete_search.js')}}"></script>

<script>
// Salary Slider Range
$(function() {
    $('#slider-container').slider({
        range: true,
        min: 0,
        max: 100000,
        values: [0, 100000],
        slide: function(event, ui) {
            $( "#amount" ).text( "₹" + ui.values[ 0 ] + " - ₹" + ui.values[ 1 ] );
            var mi = ui.values[ 0 ];
            var mx = ui.values[ 1 ];
            // filterSystem(mi, mx);
            console.log(mx);
        }
    });
    
  $( "#amount" ).text( "₹" + $( "#slider-range" ).slider( "values", 0 ) +
  " - ₹" + $( "#slider-range" ).slider( "values", 1 ) );
  
});
</script>
@endsection
