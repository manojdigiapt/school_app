@extends('UI.base')

@section('Content')
<section class="home-section-height" 

@if((request()->is('Contactus')) ? 'active' : '')
style="height: 100px;"
@endif
>
        <div class="block no-padding overlape">
            <div class="container fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="main-featured-sec style2">
                            <ul class="main-slider-sec style2 text-arrows home-img aboutus_height">
                                <li class="slideHome"><img src="{{URL::asset('UI/images/resource/mslider3.jpg')}}" alt="" /></li>
                            </ul>
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section>
            <div class="block">
                <div class="container">
                     <div class="row pad-top8percentage aboutus-pad-left-right">
                         <div class="col-lg-6 column">
                             <div class="contact-form">
                                 <h3>Keep In Touch</h3>
                                 <form>
                                     <div class="row">
                                         <div class="col-lg-12">
                                             <span class="pf-title">Full Name</span>
                                             <div class="pf-field">
                                                 <input type="text" id="Name" placeholder="Name" />
                                             </div>
                                         </div>
                                         <div class="col-lg-12">
                                             <span class="pf-title">Email</span>
                                             <div class="pf-field">
                                                 <input type="text" id="Email" placeholder="Email" />
                                             </div>
                                         </div>
                                         <div class="col-lg-12">
                                             <span class="pf-title">Subject</span>
                                             <div class="pf-field">
                                                 <input type="text" id="Subject" placeholder="Subject" />
                                             </div>
                                         </div>
                                         <div class="col-lg-12">
                                             <span class="pf-title" >Message</span>
                                             <div class="pf-field">
                                                 <textarea id="Message"></textarea>
                                             </div>
                                         </div>
                                         <div class="col-lg-12">
                                             {{-- <button type="submit">Send</button> --}}
                                             <a href="javascript:void(0);" id="SendMail" title="" class=" for-employers-btn">Send</a>
                                         </div>
                                     </div>
                                 </form>
                             </div>
                         </div>
                         <div class="col-lg-6 column">
                             <div class="contact-textinfo">
                                 <h3>Accounts wale</h3>
                                 <ul>
                                     <li><i class="la la-map-marker"></i><span>435, 3rd Floor, 27th Main, Sector 1, HSR Layout, Bengaluru, Karnataka 560102.</span></li>
                                     <li><i class="la la-phone"></i><span>Call Us : +91 86184 14801</span></li>
                                     {{-- <li><i class="la la-fax"></i><span>Fax : 0934 343 343</span></li> --}}
                                     <li><i class="la la-envelope-o"></i><span>Email : <a href="mailto:services@accountswale.in" >services@accountswale.in</a></span></li>
                                 </ul>
                                 {{-- <a class="fill" href="#" title="">See on Map</a><a href="#" title="">Directions</a> --}}
                             </div>
                        </div>
                     </div>
                </div>
            </div>
        </section>
@endsection