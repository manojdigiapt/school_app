@extends('UI.base')

@section('Content')
<section class="overlape">
		<div class="block no-padding">
			<div data-velocity="-.1" style="background: url({{URL::asset('UI/images/resource/mslider1.jpg')}} repeat scroll 50% 422.28px transparent;" class="parallax scrolly-invisible no-parallax"></div><!-- PARALLAX BACKGROUND IMAGE -->
			<div class="container fluid">
				<div class="row">
					<div class="col-lg-12">
						<div class="inner-header">
							<h3>Welcome {{Session::get('EmployerName')}}</h3>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

	<section>
		<div class="block no-padding">
			<div class="container">
				 <div class="row no-gape">
                    {{-- @include('UI.common.employer_sidebar') --}}
				 	<div class="col-lg-12 column">
				 		<div class="padding-left">
							<div class="contact-edit">
								<form>
									<div class="col-lg-4">
										<button type="button" onclick="window.location.href='/Employer/Dashboard'" style="float:left;">Back to dashboard</button>
									</div>
									<div class="col-lg-8">
									</div>
								</form>
							</div>
					 		<div class="profile-title">
					 			<h3>Post a Training</h3>
					 		</div>
					 		<div class="profile-form-edit">
					 			<form>
					 				<div class="row">
					 					<div class="col-lg-12">
					 						<span class="pf-title">Training Title</span>
					 						<div class="pf-field">
                                                <select data-placeholder="Please Select Title" id="training_title" onchange="CheckTrainings()" class="chosen title">
													<option selected>Select training</option>
													<option value="Title1">Title 1</option>
                                                    <option value="Title2">Title 2</option>
                                                    <option value="Title3">Title 3</option>
                                                    <option value="Title4">Title 4</option>
                                                    <option value="others">Others</option>               
												</select>
											 </div>

											 <div class="pf-field OthersTitle-Padtop" id="otherTitleShow" style="display:none;">
												<input type="text" class="other_title" id="other_title" placeholder="Please type title name"/>
					 						</div>
                                         </div>
                                         
                                         <div class="col-lg-6">
                                            <span class="pf-title">Training Start Date</span>
                                            <div class="pf-field">
                                                <input type="date" placeholder="01.11.207"  class="form-control datepicker" id="start_date"/>
                                            </div>
                                        </div>

                                        <div class="col-lg-6">
                                            <span class="pf-title">Training Duration</span>
                                            <div class="pf-field">
                                                <input type="text" id="duration" placeholder="Please type duration"/>
                                            </div>
                                        </div>
                                        <div class="col-lg-6">
                                            <span class="pf-title">Monthly Stipend</span>
                                            <div class="pf-field">
                                                <input type="text" id="stipend" placeholder="Eg: $5000"/>
                                            </div>
                                        </div>
                                        <div class="col-lg-6">
                                            <span class="pf-title">Last date of application</span>
                                            <div class="pf-field">
                                                <input type="date" placeholder="01.11.207"  class="form-control datepicker" id="last_date"/>
                                            </div>
                                        </div>
                                        <div class="col-lg-6">
                                            <span class="pf-title">What trainee will get after training completion</span>
                                            <div class="pf-field">
                                                <select data-placeholder="Please Select Industry" id="completion" class="chosen">
													<option selected>Select type</option>
													<option value="1">Certificate</option>
                                                    <option value="2">Eligibility for fulltime job</option>
                                                </select>
                                            </div>
                                        </div>
                                        
					 					<div class="col-lg-12">
					 						<span class="pf-title">Training Highlights</span>
					 						<div class="pf-field">
					 							<textarea id="description">Spent several years working on sheep on Wall Street. Had moderate success investing in Yugos on Wall Street. Managed a small team buying and selling pogo sticks for farmers. Spent several years licensing licorice in West Palm Beach, FL. Developed severalnew methods for working with banjos in the aftermarket. Spent a weekend importing banjos in West Palm Beach, FL.In this position, the Software Engineer ollaborates with Evention's Development team to continuously enhance our current software solutions as well as create new solutions to eliminate the back-office operations and management challenges present</textarea>
					 						</div>
					 					</div>
					 					

					 				</div>
					 			</form>
					 		</div>
					 		<div class="contact-edit">
					 			<form>
					 				<div class="row">
					 					<div class="col-lg-12">
					 						<button type="button" id="PostTrainings">Post a training</button>
					 					</div>
					 				</div>
					 			</form>
					 		</div>
					 	</div>
					</div>
				 </div>
			</div>
		</div>
	</section>
@endsection