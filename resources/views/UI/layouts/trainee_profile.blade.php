@extends('UI.base')
@section('Content')
<section id="scroll-here">
  <div class="block caniddate-profile-pad-lef-right-top">
    <div class="container">
      <div class="row">
        <div class="col-lg-4 col-md-6 candidate-profile-card">
          <div class="col-md-3 pull-left pad-left5">
            @if($TraineeProfile['profile_pic'])
            <img id="TraineeProfilePic" src="{{URL::asset('trainee_profile/')}}/{{$TraineeProfile->profile_pic}}" class="candidate-profile-pic" alt="" />
            @else
            <img id="TraineeProfilePic" src="{{URL::asset('UI/images/resource/r1.jpg')}}" class="candidate-profile-pic" alt="" />
            @endif
            <input type="file" name="trainee_file" id="trainee_file" value="" style="display:none;">
            <p class="camera-candidate-profile-bg">
              <a href="javascript:void(0);" onclick="BrowseTraineePic('trainee_file');">
                <i class="fa fa-camera candidate-profile-camera-icon" aria-hidden="true">
                </i>
              </a>
            </p>
          </div>
          <div class="col-md-9 candidate-profile-card-pad-left">
            <h5 class="candidate-name">{{$Profile->name}}
            </h5>
            @if($TraineeProfile->id)
              <input type="hidden" id="pro_id" value="{{$TraineeProfile->id}}">
            <a href="javascript:void(0);" class="clr-white candidate-profile-card-pencil EditTraineeProfile">
              <i class="fa fa-pencil">
              </i>
            </a>
            @else
            <a href="javascript:void(0);" class="clr-white candidate-profile-card-pencil TraineeProfile">
                <i class="fa fa-pencil">
                </i>
              </a>
            @endif
            <p class="candidate-profile-card-position">
              Fresher
             
            </p>
            <p class="candidate-profile-completion-p">Profile Completion
            </p>
            <p class="candiate-profile-percentage">
              @if($TraineeProfile->profile_completion)
                {{$TraineeProfile->profile_completion}}%
              @else
                0%
              @endif
            </p>
            <div id="progressbar">
              <div style="width: @if($TraineeProfile->profile_completion)
                  {{$TraineeProfile->profile_completion}}%
                @else
                  0%
                @endif;">
                </div>
              
            </div>
            <br>
            <p class="clr-white candidate-profile-card-details">
              <i class="fa fa-map-marker pad-right5" aria-hidden="true">
              </i>
              @if($TraineeProfile->desired_location)
              {{$TraineeProfile->desired_location}}
              @else
              Location
              @endif
            </p>
            
            <br>
            <p class="clr-white candidate-profile-card-details">
              <i class="fa fa-phone pad-right5" aria-hidden="true">
              </i> {{$Profile->mobile}}
            </p>
            <p class="clr-white candidate-profile-card-details">
              <i class="fa fa-envelope pad-right5" aria-hidden="true">
              </i> {{$Profile->email}}
            </p>
            <ul class="social candidate-profile-card-social-padtop">
              <li class="social-fb"> 
                <a href="#"> 
                  <i class=" fa fa-facebook">   
                  </i> 
                </a> 
              </li>
              <li class="social-twitter"> 
                <a href="#"> 
                  <i class="fa fa-twitter">   
                  </i> 
                </a> 
              </li>
              <li class="social-instagram"> 
                <a href="#"> 
                  <i class="fa fa-instagram">   
                  </i> 
                </a> 
              </li>
            </ul>
          </div>
        </div>
        <div class="col-lg-8 col-md-6">
          <div class="job-list-modern">
            <div class="caniddate-profile-resume">
              <h3 class="candidate-profile-head">My Resume
              </h3>
              @if($GetCV)
              <a href="/trainee_cv/{{$GetCV->cv}}" target="_blank" class="pull-right preview-cv-link">
                <i class="fa fa-eye">
                </i> Preview CV
              </a>
              @endif
              {{-- <p class="candidate-profile-p">Resume is the most important document recruiters look for. Recruiters generally do not look at profiles without resumes.
              </p> --}}
              <div class="uploadbox">
                <a href="javascript:void(0);" onclick="BrowseTraineeCV('trainee_cv')">
                  <label for="file-upload" class="custom-file-upload">
                    <i class="la la-cloud-upload">
                    </i> 
                    @if($GetCV)
                    <span>Edit Resume
                    </span>
                    @else
                    <span>Upload Resume
                      </span>
                    @endif
                  </label>
                </a>
                @if($GetCV)
                <input id="cv_id" type="text" value="{{$GetCV->id}}" style="display: none;">
                @endif
                <input id="trainee_cv" type="file" style="display: none;">
              </div>
              {{-- <h3 class="candidate-profile-sub-head">Resume Headline
              </h3>
              @if($GetCV["id"])
              <a href="javascript:void(0);" onclick="EditResumeHeadline({{$GetCV["id"]}})">
                <i class="fa fa-pencil candidate-profile-pencil">
                </i>
              </a>
              @else
              <a href="javascript:void(0);">
                  <i class="fa fa-pencil candidate-profile-pencil">
                  </i>
                </a>
              @endif
              @if($GetCV["id"])
              <div class="candidate-profile-p">{!!$GetCV->title!!}
              </div>
              @else
              <p class="candidate-profile-p">Resume is the most important document recruiters look for. Recruiters generally do not look at profiles without resumes.
              </p>
              @endif --}}
            </div>
			
			<br>
            <div class="caniddate-profile-resume">
              <h3 class="candidate-profile-head">Education
              </h3>
              <a href="javascript:void(0);" class="pull-right preview-cv-link"  data-toggle="modal" data-target="#AddEducationModal">
                <i class="fa fa-plus">
                </i> Add Education
              </a>
              
              
              <div class="pad-top20">
                  <table class="table table-hover">
                      <thead>
                        <tr>
                          <th>Degree</th>
                          <th>University / Board</th>
                          <th>Year of passed</th>
                          <th>Percentage</th>
                          <th>Action</th>
                        </tr>
                      </thead>
                      <tbody>
                          @foreach($GetEducation as $Education)
                        <tr>
                          <td>{{$Education->qualification}}</td>
                          <td>{{$Education->name}}</td>
                          <td>{{$Education->from_year}}</td>
                          <td>{{$Education->percentage}}</td>
                          <td><a href="javascript:void(0);" onclick="EditTraineeEducation({{$Education->id}})">
                              <i class="fa fa-pencil">
                              </i>
                            </a>
            
                            <a href="javascript:void(0);" onclick="DeleteEducation({{$Education->id}})">
                                <i class="fa fa-trash">
                                </i>
                              </a></td>
                        </tr>
                        @endforeach
                      </tbody>
                    </table>

      </div>
    </div>
			
			<br>
            <div class="caniddate-profile-resume">
              <h3 class="candidate-profile-head">Personal Details
        </h3>
        @if($TraineeProfile["id"])
        <a href="javascript:void(0);" onclick="EditPersonalDetailsModal({{$TraineeProfile["id"]}})">
            <i class="fa fa-pencil candidate-profile-head-pencil candidate-profile-work-experience-pencil candidate-profile-personal-details-pencil">
            </i>
          </a>
          @else
			  <a href="javascript:void(0);" data-toggle="modal" data-target="#AddPersonalDetailsModal">
					<i class="fa fa-pencil candidate-profile-head-pencil candidate-profile-work-experience-pencil candidate-profile-personal-details-pencil">
					</i>
				</a>
        @endif

			<div class="row">
				<div class="col-md-3">
					<h4 class="candidate-work-experience-head">Date of birth
            </h4>
            @if($TraineeProfile["date_of_birth"])
					<p class="candidate-work-experience-p">{{date('d M Y', strtotime($TraineeProfile->date_of_birth))}}
          </p>
          @endif
				</div>
				<div class="col-md-3">
					<h4 class="candidate-work-experience-head">Gender
            </h4>
            @if($TraineeProfile["gender"])
					<p class="candidate-work-experience-p">
            @if($TraineeProfile->gender == 1)
              Male
            @else
              Female
            @endif
          </p>
          @endif
				</div>
				<div class="col-md-3">
					<h4 class="candidate-work-experience-head">Marital Status
            </h4>
            @if($TraineeProfile["marital_status"])
					<p class="candidate-work-experience-p">
              @if($TraineeProfile->marital_status == 1)
              Married
            @else
              Unmarried
            @endif
          </p>
          @endif
				</div>
				<div class="col-md-3">
					<h4 class="candidate-work-experience-head">Father's Name
            </h4>
            @if($TraineeProfile["father_name"])
					<p class="candidate-work-experience-p">{{$TraineeProfile->father_name}}
          </p>
          @endif
				</div>
			</div>
			<br>
			<div class="row">
				<div class="col-md-6">
					<h4 class="candidate-work-experience-head">Address
          </h4>
          @if($TraineeProfile["address"])
					<p class="candidate-work-experience-p">{{$TraineeProfile->address}}
          </p>
          @endif
				</div>
      </div>
      
      <br>
      <div class="row">
        <div class="col-md-4">
          <h4 class="candidate-work-experience-head">State
          </h4>
          @if($TraineeProfile["state"])
          <p class="candidate-work-experience-p">{{$TraineeProfile->state}}
          </p>
          @endif
        </div>
        <div class="col-md-4">
            <h4 class="candidate-work-experience-head">City
            </h4>
            @if($TraineeProfile["city"])
            <p class="candidate-work-experience-p">{{$TraineeProfile->city}}
            </p>
            @endif
          </div>
          <div class="col-md-4">
              <h4 class="candidate-work-experience-head">Zip code
              </h4>
              @if($TraineeProfile["zip_code"])
              <p class="candidate-work-experience-p">{{$TraineeProfile->zip_code}}
              </p>
              @endif
            </div>
      </div>
			  
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<!-- Profile Edit -->
<div class="account-popup-area candidate-profile-popup-box">
  <div class="account-popup candidate-profile-popup">
    <span class="close-popup">
      <i class="la la-close">
      </i>
    </span>
    <h3 class="candidate-profile-details-popup">Profile Details
    </h3>
    <form class="mar-top15">
      <label class="pull-left remove-label">Name
      </label>
      <div class="cfield">
        <input type="text" placeholder="Username" value="{{$Profile->name}}" id=""/>
      </div>
      <label class="pull-left remove-label">Total Experience *
      </label>
      <div class="dropdown-field col-md-8 pad-left0 width50">
        <select data-placeholder="Please Select Candidate Type" class="chosen" id="experience">
          <!-- <option selected disabled>Please Select Candidate Type</option> -->
          <option  value="1">2-6 years
          </option>
          <option  value="2">6-12 years
          </option>
        </select>
      </div>
      <label class="pull-left remove-label">Current Salary *
      </label>
      <div class="dropdown-field col-md-8 pad-left0 width50">
        <select data-placeholder="Please Select Candidate Type" class="chosen" id="current_salary">
          <!-- <option selected disabled>Please Select Candidate Type</option> -->
          <option  value="1">50k-60k
          </option>
          <option  value="2">60k-70k
          </option>
        </select>
      </div>
      <label class="pull-left remove-label">Current Location
      </label>
      <div class="cfield">
        <input type="text" placeholder="Bangalore" value="" id="desired_location"/>
        {{-- 
        <a href="javascript:void(0);" id="Currentlocation">
          <i class="fa fa-arrows">
          </i>
        </a> --}}
      </div>
      <label class="pull-left remove-label">Mobile Number *
      </label>
      <div class="cfield">
        <input type="text" placeholder="9876543210" value="{{$Profile->mobile}}" id=""/>
      </div>
      <label class="pull-left remove-label">Email Address
      </label>
      <div class="cfield">
        <input type="text" placeholder="demo@gmail.com" value="{{$Profile->email}}" id=""/>
      </div>
      <button type="button" class="candidate-profile-cancel-btn col-md-6 col-xs-6 text-right width50">Cancel
      </button>
      <button type="button" class="candidate-profile-save-btn col-md-6 col-xs-6" id="AddTraineeProfile">Save
      </button>
    </form>
    <img id="CandidateOtpLoader" class="signup-otp-width" src="{{URL::asset('UI/otp_verify_loader.gif')}}" alt="" style="display:none;">
  </div>
</div>

{{-- Edit Profile Details --}}
<div class="account-popup-area edit_trainee-profile-popup-box">
    <div class="account-popup candidate-profile-popup">
      <span class="close-popup">
        <i class="la la-close">
        </i>
      </span>
      <h3 class="candidate-profile-details-popup">Profile Details
      </h3>
      <form class="mar-top15">
        <label class="pull-left remove-label">Name
        </label>
        <div class="cfield">
          <input type="text" placeholder="Username" value="{{$Profile->name}}" id=""/>
        </div>
        
        <label class="pull-left remove-label">Current Location
        </label>
        <div class="cfield">
          <input type="text" placeholder="Bangalore" value="" id="Edit_desired_location"/>
          {{-- 
          <a href="javascript:void(0);" id="Currentlocation">
            <i class="fa fa-arrows">
            </i>
          </a> --}}
        </div>
        <label class="pull-left remove-label">Mobile Number *
        </label>
        <div class="cfield">
          <input type="text" placeholder="9876543210" value="{{$Profile->mobile}}" id=""/>
        </div>
        <label class="pull-left remove-label">Email Address
        </label>
        <div class="cfield">
          <input type="text" placeholder="demo@gmail.com" value="{{$Profile->email}}" id=""/>
        </div>
        <button type="button" class="candidate-profile-cancel-btn col-md-6 col-xs-6 text-right width50">Cancel
        </button>
        <button type="button" class="candidate-profile-save-btn col-md-6 col-xs-6" id="UpdateTraineeProfile">Save
        </button>
      </form>
      <img id="CandidateOtpLoader" class="signup-otp-width" src="{{URL::asset('UI/otp_verify_loader.gif')}}" alt="" style="display:none;">
    </div>
  </div>

{{-- Resume headline edit --}}
<div id="ResumeHeadline" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header pad-bottom0">
		  <h5>Add Resume Headline</h5>
        <button type="button" class="close" data-dismiss="modal">&times;
        </button>
      </div>
      <div class="modal-body pad-top0">
        <label class="pull-left remove-label">Resume Headline
		</label>
		<p class="resume-headline-p">Resume is most important document recruiter look for. Recruiters generally do not look at profiles without resumes.</p>
        <div class="cfield">
          <textarea id="description">
          </textarea>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default candidate-profile-cancel-btn" data-dismiss="modal">Close
        </button>
        <button type="button" onclick="InsertOrUpdateTraineeCV()" class="btn btn-default candidate-profile-save-btn">Save
        </button>
      </div>
    </div>
  </div>
</div>
{{-- Education --}}
<div id="AddEducationModal" class="modal fade" role="dialog">
		<div class="modal-dialog">
		  <!-- Modal content-->
		  <div class="modal-content">
			<div class="modal-header pad-bottom0">
				<h5>Add Education Details</h5>
			  <button type="button" class="close" data-dismiss="modal">&times;
			  </button>
			</div>
			<div class="modal-body pad-top0">
			  <form class="mar-top15">

				<label class="pull-left remove-label candidate-profile-label">Degree *
				</label>
				<div class="cfield">
				  {{-- <select data-placeholder="Please Select Industries" class="chosen board" id="board" onchange="CheckBoard()">
					  <option selected disabled>Select Board</option>
            <option value="1">10th
            </option>
            <option value="2">12th
					  </option>
            <option value="3">Graduate
					  </option>
          </select> --}}
          <select data-placeholder="Please Select Industries" class="chosen" id="board">
              <option selected disabled>Select Degree</option>
                @foreach($GetQualification as $Qualification)
            <option value="{{$Qualification->id}}">{{$Qualification->qualification}}
                </option>
                @endforeach
            </select>
				</div>
        
        <div id="CourseNameRow" style="display:none;">
          <label class="pull-left remove-label candidate-profile-label">Course Name *
          </label>
          <div class="cfield">
            <input type="text" placeholder="Course name" value="" id="course_name" class="bg-white input-border input-height44 mar-bottom25 course_name" id=""/>
          </div>
        </div>

				<label class="mar-top15 pull-left remove-label candidate-profile-label">University / Board *
				</label>
				<div class="cfield">
				  <input type="text" placeholder="School name" value="" id="school_clg" class="bg-white input-border input-height44 mar-bottom25" id=""/>
				</div>
				
				<label class="pull-left remove-label candidate-profile-label">Passing Out Year *
				  </label>
				  <div class="cfield dropdown-year-month-width">
					<select class="dropdown-custom-clr-pad" id="passed_out_year">
						<option selected disabled>Select year</option>
						{{--  <option value="2014">2014
						</option>
						<option value="2018">2018</option>  --}}
					</select>
				  </div>

				  {{-- <div class="cfield dropdown-year-month-width">
					<select class="chosen" id="course_type">
						<option selected disabled>Select type</option>
						<option value="1">Part time
						</option>
						<option value="2">Full time</option>
					</select>
          </div> --}}
          <div class="cfield width50 pull-left">
              <input type="text" placeholder="Percentage" value="" id="percentage" class="bg-white input-border input-height44 mar-bottom25" id=""/>
            </div>
			  </form>
			</div>
			<div class="modal-footer">
			  <button type="button" class="btn btn-default candidate-profile-cancel-btn" data-dismiss="modal">Close
			  </button>
			  <button type="button" id="AddTraineeEducation" class="btn btn-default candidate-profile-save-btn">Save
			  </button>
			</div>
		  </div>
		</div>
	  </div>


{{-- Edit Education --}}
<div id="EditEducationModal" class="modal fade" role="dialog">
		<div class="modal-dialog">
		  <!-- Modal content-->
		  <div class="modal-content">
			<div class="modal-header pad-bottom0">
				<h5>Edit Education Details</h5>
			  <button type="button" class="close" data-dismiss="modal">&times;
			  </button>
			</div>
			<div class="modal-body pad-top0">
			  <form class="mar-top15">

				<label class="pull-left remove-label candidate-profile-label">Degree *
				</label>
				<div class="cfield">
            <input type="hidden" placeholder="Course name" value="" id="edu_id" class="bg-white input-border input-height44 mar-bottom25 course_name"/>

				  {{-- <select data-placeholder="Please Select Industries" class="board dropdown-custom-clr-pad" id="Edit_board" onchange="Edit_CheckBoard()">
					  <option selected disabled>Select Board</option>
            <option value="1">10th
            </option>
            <option value="2">12th
					  </option>
            <option value="3">Graduate
					  </option>
          </select> --}}

          <select data-placeholder="Please Select Industries" class="dropdown-custom-clr-pad" id="Edit_board">
              <option selected disabled>Select Degree</option>
                @foreach($GetQualification as $Qualification)
            <option value="{{$Qualification->id}}">{{$Qualification->qualification}}
                </option>
                @endforeach
            </select>
				</div>
        
        {{--  <div id="Edit_CourseNameRow" style="display:none;">
          <label class="pull-left remove-label candidate-profile-label">Course Name *
          </label>
          <div class="cfield">
            <input type="text" placeholder="Course name" value="" id="edit_course_name" class="bg-white input-border input-height44 mar-bottom25 course_name" id=""/>
          </div>
        </div>  --}}

				<label class="mar-top15 pull-left remove-label candidate-profile-label">University / Board *
				</label>
				<div class="cfield">
				  <input type="text" placeholder="School name" value="" id="Edit_school_clg" class="bg-white input-border input-height44 mar-bottom25" id=""/>
				</div>
				
				<label class="pull-left remove-label candidate-profile-label">Passing Out Year *
				  </label>
				  <div class="cfield dropdown-year-month-width">
					<select class="dropdown-custom-clr-pad" id="Edit_passed_out_year">
						<option selected disabled>Select year</option>
						<option value="2014">2014
						</option>
						<option value="2018">2018</option>
					</select>
				  </div>

				  {{-- <div class="cfield dropdown-year-month-width">
					<select class="dropdown-custom-clr-pad" id="Edit_course_type">
						<option selected disabled>Select type</option>
						<option value="1">Part time
						</option>
						<option value="2">Full time</option>
					</select>
          </div> --}}
          
          <div class="cfield width50 pull-left">
              <input type="text" placeholder="Percentage" value="" id="Edit_percentage" class="bg-white input-border input-height44 mar-bottom25" id=""/>
            </div>
			  </form>
			</div>
			<div class="modal-footer">
			  <button type="button" class="btn btn-default candidate-profile-cancel-btn" data-dismiss="modal">Close
			  </button>
			  <button type="button" id="UpdateTraineeEducation" class="btn btn-default candidate-profile-save-btn">Save
			  </button>
			</div>
		  </div>
		</div>
	  </div>




{{-- Personal Details --}}
<div id="AddPersonalDetailsModal" class="modal fade" role="dialog">
		<div class="modal-dialog">
		  <!-- Modal content-->
		  <div class="modal-content">
			<div class="modal-header pad-bottom0">
				<h5>Add Personal Details</h5>
			  <button type="button" class="close" data-dismiss="modal">&times;
			  </button>
			</div>
			<div class="modal-body pad-top0">
			  <form class="mar-top15">

					<label class="pull-left remove-label candidate-profile-label">Date of Birth 
						</label>
						<div class="cfield width50">
						  <input type="text" placeholder="Date of birth" id="dob" value="" class="bg-white input-border input-height44 mar-bottom25" id=""/>
						</div>
				
				<label class="pull-left remove-label candidate-profile-label">Gender 
					</label>
					<div class="cfield dropdown-year-month-width">
					  <select class="dropdown-custom-clr-pad" id="gender">
						  <option selected disabled>Select gender</option>
						  <option value="1">Male
						  </option>
						  <option value="2">Female</option>
					  </select>
					</div>
  
					<div class="cfield dropdown-year-month-width">
					  <select class="dropdown-custom-clr-pad" id="marital_status">
						  <option selected disabled>Marital Status</option>
						  <option value="1">Married
						  </option>
						  <option value="2">Un married</option>
					  </select>
					  </div>

				<label class="pull-left remove-label candidate-profile-label">Father's Name 
				</label>
				<div class="cfield">
				  <input type="text" placeholder="Father name" value="" class="bg-white input-border input-height44 mar-bottom25" id="father_name"/>
				</div>
				
				<label class="pull-left remove-label candidate-profile-label">Address 
				</label>
				<div class="cfield">
        <input type="text" placeholder="Address" value="" id="address" class="bg-white input-border input-height44 mar-bottom25" id=""/>
				</div>
			  </form>
			</div>
			<div class="modal-footer">
			  <button type="button" class="btn btn-default candidate-profile-cancel-btn" data-dismiss="modal">Close
			  </button>
			  <button type="button" id="AddCandidatePersonalDetails" class="btn btn-default candidate-profile-save-btn">Save
			  </button>
			</div>
		  </div>
		</div>
    </div>	  
    
    {{-- Edit Personal Details --}}
<div id="EditPersonalDetailsModal" class="modal fade" role="dialog">
		<div class="modal-dialog">
		  <!-- Modal content-->
		  <div class="modal-content">
			<div class="modal-header pad-bottom0">
				<h5>Add Personal Details</h5>
			  <button type="button" class="close" data-dismiss="modal">&times;
			  </button>
			</div>
			<div class="modal-body pad-top0">
			  <form class="mar-top15">

					<label class="pull-left remove-label candidate-profile-label">Date of Birth 
						</label>
						<div class="cfield width50">

						  <input type="text" placeholder="Date of birth" id="Edit_dob" value="" class="bg-white input-border input-height44 mar-bottom25" id=""/>
						</div>
				
				<label class="pull-left remove-label candidate-profile-label">Gender 
					</label>
					<div class="cfield dropdown-year-month-width">
					  <select class="dropdown-custom-clr-pad" id="Edit_gender">
						  <option selected disabled>Select gender</option>
						  <option value="1">Male
						  </option>
						  <option value="2">Female</option>
					  </select>
					</div>
  
					<div class="cfield dropdown-year-month-width">
					  <select class="dropdown-custom-clr-pad" id="Edit_marital_status">
						  <option selected disabled>Marital Status</option>
						  <option value="1">Married
						  </option>
						  <option value="2">Un married</option>
					  </select>
					  </div>

				<label class="pull-left remove-label candidate-profile-label">Father's Name 
				</label>
				<div class="cfield">
				  <input type="text" placeholder="Father name" value="" class="bg-white input-border input-height44 mar-bottom25" id="Edit_father_name"/>
				</div>
        
        <div id="GetProfileAddress">
          <label class="pull-left remove-label candidate-profile-label">Address 
          </label>
          <div class="cfield">
          <input type="text" placeholder="Address" data-geocomplete="street address" value="" id="Edit_address" class="bg-white input-border input-height44 mar-bottom25" id=""/>
          </div>
          
          <label class="pull-left remove-label candidate-profile-label">State 
            </label>
            <div class="cfield">
            <input type="text" placeholder="State" value="" data-geocomplete="state" id="Edit_state" class="bg-white input-border input-height44 mar-bottom25" id=""/>
            </div>

            <label class="pull-left remove-label candidate-profile-label">City 
              </label>
              <div class="cfield">
              <input type="text" placeholder="City" value="" data-geocomplete="city" id="Edit_city" class="bg-white input-border input-height44 mar-bottom25" id=""/>
              </div>

            <label class="pull-left remove-label candidate-profile-label">Zipcode 
              </label>
              <div class="cfield">
              <input type="text" placeholder="Zip code" value="" data-geocomplete="zip code" id="Edit_zipcode" class="bg-white input-border input-height44 mar-bottom25" id=""/>
              </div>
          </div>
			  </form>
			</div>
			<div class="modal-footer">
			  <button type="button" class="btn btn-default candidate-profile-cancel-btn" data-dismiss="modal">Close
			  </button>
			  <button type="button" id="UpdateTrineePersonalDetails" class="btn btn-default candidate-profile-save-btn">Save
			  </button>
			</div>
		  </div>
		</div>
	  </div>	  
@endsection
@section('JSScript')
{{-- 
<script src="https://code.jquery.com/jquery-1.12.4.js">
</script> --}}
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js">
</script>
<script>
  $('#dob').datepicker({
    onSelect: function(value, ui) {
      var today = new Date(),
          dob = new Date(value),
          age = new Date(today - dob).getFullYear() - 1970;
      $('#age').val(age);
    }
    ,
    maxDate: '+0d',
    yearRange: '1960:2019',
    changeMonth: true,
    changeYear: true
  }
            );
            
            $('#Edit_dob').datepicker({
    onSelect: function(value, ui) {
      var today = new Date(),
          dob = new Date(value),
          age = new Date(today - dob).getFullYear() - 1970;
      $('#age').val(age);
    }
    ,
    maxDate: '+0d',
    yearRange: '1960:2019',
    changeMonth: true,
    changeYear: true
  }
            );
            

    $(document).ready(function () {
      for (i = new Date().getFullYear(); i > 1990; i--)
      {
          $('#passed_out_year').append($('<option />').val(i).html(i));
      }
    });

    $(document).ready(function () {
      for (i = new Date().getFullYear(); i > 1990; i--)
      {
          $('#Edit_passed_out_year').append($('<option />').val(i).html(i));
      }
    });
</script>
@endsection 
