@extends('UI.base')

@section('Content')
<section class="home-section-height">
	<div class="block no-padding overlape">
		<div class="container fluid">
			<div class="row">
				<div class="col-lg-12">
					<div class="main-featured-sec style2">
						<ul class="main-slider-sec style2 text-arrows home-img">
							<li class="slideHome"><img src="{{URL::asset('UI/images/resource/mslider3.jpg')}}" alt="" /></li>
						</ul>
						<div class="job-search-sec">
							<div class="job-search style2">
								<h3>The Easiest Way to Get Your New Job</h3>
								<span>Find Jobs, Employment & Career Opportunities</span>
								<div class="search-job2">	
									<form class="home-search-box" action="/SearchJobResults">
										<div class="row no-gape">
											<div class="col-lg-6 col-md-6 col-sm-4 col-xs-6">
												<div class="job-field">
													<input type="text" id="JobTitle" name="JobTitle" placeholder="Skill, Designation" />
												</div>
											</div>
											<div class="col-lg-3 col-md-3 col-sm-4">
												<div class="job-field">
													<select data-placeholder="Any category" name="City" class="chosen-city" id="Locations">
														<option selected>Location</option>
														@foreach($GetCityByJobPosted as $City)
													<option value="{{$City->city}}">{{$City->city}}</option>
														@endforeach
													</select>
												</div>
											</div>
											<div class="col-lg-3  col-md-3 col-sm-4">
												<button type="submit" id="SearchHomeJobs" class="home-search-btn"><img src="{{URL::asset('UI/images/custom/search.png')}}" alt=""> SEARCH</button>
											</div>
										</div>
									</form>
								</div><!-- Job Search 2 -->
								{{--  <div id="recipeCarousel" class="carousel slide w-100" data-ride="carousel">
									<div class="carousel-inner w-100" role="listbox">
										<div class="carousel-item row no-gutters active quick-select-sec quick-select-pad">
											<div class="row">
												<div class="col-lg-3 col-md-3 col-sm-3">
													<div class="quick-select border-right-quick-select">
														<a href="#" title="">
															<p class="quick-select1"><img src="{{URL::asset('UI/images/custom/bank-building.svg')}}" alt=""></p>
															<span>Accounting / Finance</span>
															<p>(22 open positions)</p>
														</a>
													</div><!-- Quick Select -->
												</div>
												<div class="col-lg-3 col-md-3 col-sm-3">
													<div class="quick-select border-right-quick-select">
														<a href="#" title="">
															<p class="quick-select2"><img src="{{URL::asset('UI/images/custom/teamwork.svg')}}" alt=""></p>
															<span>Human Resources</span>
															<p>(06 open positions)</p>
														</a>
													</div><!-- Quick Select -->
												</div>
												<div class="col-lg-3 col-md-3 col-sm-3">
													<div class="quick-select border-right-quick-select">
														<a href="#" title="">
															<p class="quick-select3"><img src="{{URL::asset('UI/images/custom/college-graduation.svg')}}" alt=""></p>
															<span>Education Training</span>
															<p>(03 open positions)</p>
														</a>
													</div><!-- Quick Select -->
												</div>
												<div class="col-lg-3 col-md-3 col-sm-3">
													<div class="quick-select">
														<a href="#" title="">
															<p class="quick-select4"><img src="{{URL::asset('UI/images/custom/pencil.svg')}}" alt=""></p>
															<span>Design/Art & Multimedia</span>
															<p>(03 open positions)</p>
														</a>
													</div><!-- Quick Select -->
												</div>
											</div>
										</div>
										<div class="carousel-item row no-gutters quick-select-sec quick-select-pad">
											<div class="row">
												<div class="col-lg-3 col-md-3 col-sm-3">
													<div class="quick-select border-right-quick-select">
														<a href="#" title="">
															<p class="quick-select1"><img src="{{URL::asset('UI/images/custom/bank-building.svg')}}" alt=""></p>
															<span>Accounting / Finance</span>
															<p>(22 open positions)</p>
														</a>
													</div><!-- Quick Select -->
												</div>
												<div class="col-lg-3 col-md-3 col-sm-3">
													<div class="quick-select border-right-quick-select">
														<a href="#" title="">
															<p class="quick-select2"><img src="{{URL::asset('UI/images/custom/teamwork.svg')}}" alt=""></p>
															<span>Human Resources</span>
															<p>(06 open positions)</p>
														</a>
													</div><!-- Quick Select -->
												</div>
												<div class="col-lg-3 col-md-3 col-sm-3">
													<div class="quick-select border-right-quick-select">
														<a href="#" title="">
															<p class="quick-select3"><img src="{{URL::asset('UI/images/custom/college-graduation.svg')}}" alt=""></p>
															<span>Education Training</span>
															<p>(03 open positions)</p>
														</a>
													</div><!-- Quick Select -->
												</div>
												<div class="col-lg-3 col-md-3 col-sm-3">
													<div class="quick-select">
														<a href="#" title="">
															<p class="quick-select4"><img src="{{URL::asset('UI/images/custom/pencil.svg')}}" alt=""></p>
															<span>Design/Art & Multimedia</span>
															<p>(03 open positions)</p>
														</a>
													</div><!-- Quick Select -->
												</div>
											</div>
										</div>
									</div>
									<a class="carousel-control-prev" href="#recipeCarousel" role="button" data-slide="prev">
										<span class="carousel-control-prev-icon" aria-hidden="true"></span>
										<span class="sr-only">Previous</span>
									</a>
									<a class="carousel-control-next" href="#recipeCarousel" role="button" data-slide="next">
										<span class="carousel-control-next-icon" aria-hidden="true"></span>
										<span class="sr-only">Next</span>
									</a>
								</div>  --}}

								{{-- <div class="quick-select-sec quick-select-pad">
									<div class="row">
										<div class="col-lg-3 col-md-3 col-sm-3">
											<div class="quick-select border-right-quick-select">
												<a href="#" title="">
													<p class="quick-select1"><img src="{{URL::asset('UI/images/custom/bank-building.svg')}}" alt=""></p>
													<span>Accounting / Finance</span>
													<p>(22 open positions)</p>
												</a>
											</div><!-- Quick Select -->
										</div>
										<div class="col-lg-3 col-md-3 col-sm-3">
											<div class="quick-select border-right-quick-select">
												<a href="#" title="">
													<p class="quick-select2"><img src="{{URL::asset('UI/images/custom/teamwork.svg')}}" alt=""></p>
													<span>Human Resources</span>
													<p>(06 open positions)</p>
												</a>
											</div><!-- Quick Select -->
										</div>
										<div class="col-lg-3 col-md-3 col-sm-3">
											<div class="quick-select border-right-quick-select">
												<a href="#" title="">
													<p class="quick-select3"><img src="{{URL::asset('UI/images/custom/college-graduation.svg')}}" alt=""></p>
													<span>Education Training</span>
													<p>(03 open positions)</p>
												</a>
											</div><!-- Quick Select -->
										</div>
										<div class="col-lg-3 col-md-3 col-sm-3">
											<div class="quick-select">
												<a href="#" title="">
													<p class="quick-select4"><img src="{{URL::asset('UI/images/custom/pencil.svg')}}" alt=""></p>
													<span>Design/Art & Multimedia</span>
													<p>(03 open positions)</p>
												</a>
											</div><!-- Quick Select -->
										</div>
									</div>
								</div> --}}
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>


{{-- <section>
	<div class="block gray home-place-to-work-sec-height best-place-section-padtop">
		<div class="container">
			<div class="row">
				<div class="offset-lg-1 col-lg-5">
					<div class="heading mobile-tab-center">
						<h2>Best place to work</h2>
						<span>For Candidate</span>
					</div><!-- Heading -->
					<div class="job-grid-sec job-grid-work">
						<div class="row">
							<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 pad-left-right0 border-bottom-left-radius-place-to-work">
								<div class="job-grid">
									<div class="job-title-sec">
										<div class="c-logo"> <img src="{{URL::asset('UI/images/resource/jg1.png')}}" alt="" /> </div>
										<h3><a href="#" title="">Web Designer / Developer</a></h3>
									</div>
									<span class="job-lctn">Sacramento, California</span>
									<a  href="#" title="">APPLY NOW</a>
								</div><!-- JOB Grid -->
							</div>
							<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 pad-left-right0 border-bottom-right-radius-place-to-work">
								<div class="job-grid">
									<div class="job-title-sec">
										<div class="c-logo"> <img src="{{URL::asset('UI/images/resource/jg4.png')}}" alt="" /> </div>
										<h3><a href="#" title="">Web Designer / Developer</a></h3>
									</div>
									<span class="job-lctn">Sacramento, California</span>
									<a  href="#" title="">APPLY NOW</a>
								</div><!-- JOB Grid -->
							</div>
							<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 pad-left-right0">
								<div class="job-grid">
									<div class="job-title-sec">
										<div class="c-logo"> <img src="{{URL::asset('UI/images/resource/jg5.png')}}" alt="" /> </div>
										<h3><a href="#" title="">Web Designer / Developer</a></h3>
									</div>
									<span class="job-lctn">Sacramento, California</span>
									<a  href="#" title="">APPLY NOW</a>
								</div><!-- JOB Grid -->
							</div>
							<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 pad-left-right0">
								<div class="job-grid">
									<div class="job-title-sec">
										<div class="c-logo"> <img src="{{URL::asset('UI/images/resource/jg6.png')}}" alt="" /> </div>
										<h3><a href="#" title="">Web Designer / Developer</a></h3>
									</div>
									<span class="job-lctn">Sacramento, California</span>
									<a href="#" title="">APPLY NOW</a>
								</div><!-- JOB Grid -->
							</div>
						</div>
						<p class="home-more-btn more-btn-font-weight"><a>more ></p></a>
					</div>
				</div>
				<div class="offset-lg-right-1 col-lg-5">
						<div class="heading mobile-tab-center">
							<h2>Best place to Learn</h2>
							<span>For Trainee</span>
						</div><!-- Heading -->
						<div class="job-grid-sec job-grid-learn">
							<div class="row">
									<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 pad-left-right0 border-bottom-left-radius-place-to-work">
										<div class="job-grid">
											<div class="job-title-sec">
												<div class="c-logo"> <img src="{{URL::asset('UI/images/resource/jg1.png')}}" alt="" /> </div>
												<h3><a href="#" title="">Web Designer / Developer</a></h3>
											</div>
											<span class="job-lctn">Sacramento, California</span>
											<a  href="#" title="">APPLY NOW</a>
										</div><!-- JOB Grid -->
									</div>
									<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 pad-left-right0 border-bottom-right-radius-place-to-work">
										<div class="job-grid">
											<div class="job-title-sec">
												<div class="c-logo"> <img src="{{URL::asset('UI/images/resource/jg4.png')}}" alt="" /> </div>
												<h3><a href="#" title="">Web Designer / Developer</a></h3>
											</div>
											<span class="job-lctn">Sacramento, California</span>
											<a  href="#" title="">APPLY NOW</a>
										</div><!-- JOB Grid -->
									</div>
									<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 pad-left-right0">
										<div class="job-grid">
											<div class="job-title-sec">
												<div class="c-logo"> <img src="{{URL::asset('UI/images/resource/jg5.png')}}" alt="" /> </div>
												<h3><a href="#" title="">Web Designer / Developer</a></h3>
											</div>
											<span class="job-lctn">Sacramento, California</span>
											<a  href="#" title="">APPLY NOW</a>
										</div><!-- JOB Grid -->
									</div>
									<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 pad-left-right0">
										<div class="job-grid">
											<div class="job-title-sec">
												<div class="c-logo"> <img src="{{URL::asset('UI/images/resource/jg6.png')}}" alt="" /> </div>
												<h3><a href="#" title="">Web Designer / Developer</a></h3>
											</div>
											<span class="job-lctn">Sacramento, California</span>
											<a href="#" title="">APPLY NOW</a>
										</div><!-- JOB Grid -->
									</div>
							</div>
							<p class="home-more-btn more-btn-font-weight"><a>more ></p></a>
						</div>
					</div>
			</div>
		</div>
	</div>
</section> --}}

{{-- <section class="home-employers-height">
		<div class="block ">
			<div class="container">
				<div class="row">
					<div class="col-lg-12">
						<div>
						<div class="heading text-center">
							<h2>Top Employers</h2>
						</div><!-- Heading -->
						<div class="comp-sec">
							<div class="company-img company-img-border">
								<a href="#" title=""><img src="{{URL::asset('UI/images/resource/jg1.png')}}" alt="" /></a>
							</div><!-- Client  -->
							<div class="company-img company-img-border">
								<a href="#" title=""><img src="{{URL::asset('UI/images/resource/jg2.png')}}" alt="" /></a>
							</div><!-- Client  -->
							<div class="company-img company-img-border">
								<a href="#" title=""><img src="{{URL::asset('UI/images/resource/jg3.png')}}" alt="" /></a>
							</div><!-- Client  -->
							<div class="company-img company-img-border">
								<a href="#" title=""><img src="{{URL::asset('UI/images/resource/jg4.png')}}" alt="" /></a>
							</div><!-- Client  -->
							<div class="company-img">
								<a href="#" title=""><img src="{{URL::asset('UI/images/resource/jg5.png')}}" alt="" /></a>
							</div><!-- Client  -->
						</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section> --}}

{{-- <section>
	<div class="block pad-top0">
		<div class="container">
			<div class="row">
				<div class="col-lg-12 home-how-it-work-pad">
					<div class="heading text-center">
						<h2>How It Works</h2>
						{{-- <span>Each month, more than 7 million Jobhunt turn to website in their search for work, making over <br />160,000 applications every day.
						</span> 
					</div>
					<div class="how-to-sec">
						<div class="how-to">
							<span class="how-icon"><i class="la la-user"></i></span>
							<h3>Register an account</h3>
						</div>
						<div class="how-to">
							<span class="how-icon"><i class="la la-file-archive-o"></i></span>
							<h3>Specify & search your job</h3>
							{{-- <p>Browse profiles, reviews, and proposals then interview top candidates. </p> 
						</div>
						<div class="how-to">
							<span class="how-icon"><i class="la la-list"></i></span>
							<h3>Apply for job</h3>
							{{-- <p>Use the Upwork platform to chat, share files, and collaborate from your desktop or on the go.</p
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section> --}}

{{-- <section>
	<div class="block reviews-candidates-pad-top">
		<div data-velocity="-.1" class="parallax scrolly-invisible layer color light"></div><!-- PARALLAX BACKGROUND IMAGE -->
		<div class="container">
			<div class="row">
				<div class="col-lg-12 reviews-pad-left-right">
					<div class="heading reviews-heading-mar-bottom text-center">
						<h2>Kind Words From Happy Candidates</h2>
						<span>What other people thought about the service provided by JobHunt</span>
					</div><!-- Heading -->
					<div class="reviews-sec reviews-height" id="reviews-carousel">
						<div class="col-lg-6">
							<div class="reviews">
								<!-- <img src="images/custom/left-quote.svg" class="review-quotes" alt="" /> -->
								<p class="testimonial-quotes"><i class="fa fa-quote-left" aria-hidden="true"></i></p>
								<p>Without JobHunt i’d be homeless, they found me a job and got me sorted out quickly with everything!  Can’t quite believe the service</p>
									<div class="col-md-2 col-xs-2 pull-left">
										<img src="{{URL::asset('UI/images/resource/r1.jpg')}}" alt="" />
									</div>
									<div class="col-md-8 col-xs-8  pull-left">
										<h3>Augusta Silva</h3>
										<span class="reviews-designation-span">Web designer</span>
									</div>
							</div>
						</div>
						<div class="col-lg-6">
							<div class="reviews">
								<p class="testimonial-quotes"><i class="fa fa-quote-left" aria-hidden="true"></i></p>
								<p>Without JobHunt i’d be homeless, they found me a job and got me sorted out quickly with everything!  Can’t quite believe the service</p>
									<div class="col-md-2 col-xs-2 pull-left">
										<img src="{{URL::asset('UI/images/resource/r1.jpg')}}" alt="" />
									</div>
									<div class="col-md-8 col-xs-8 pull-left">
										<h3>Augusta Silva</h3>
										<span class="reviews-designation-span">Web designer</span>
									</div>
							</div>
						</div>
						<div class="col-lg-6">
							<div class="reviews">
								<p class="testimonial-quotes"><i class="fa fa-quote-left" aria-hidden="true"></i></p>
								<p>Without JobHunt i’d be homeless, they found me a job and got me sorted out quickly with everything!  Can’t quite believe the service</p>
									<div class="col-md-2 col-xs-2 pull-left">
										<img src="{{URL::asset('UI/images/resource/r1.jpg')}}" alt="" />
									</div>
									<div class="col-md-8 col-xs-8 pull-left">
										<h3>Augusta Silva</h3>
										<span class="reviews-designation-span">Web designer</span>
									</div>
							</div>
						</div>
						<div class="col-lg-6">
							<div class="reviews">
								<p class="testimonial-quotes"><i class="fa fa-quote-left" aria-hidden="true"></i></p>
								<p>Without JobHunt i’d be homeless, they found me a job and got me sorted out quickly with everything!  Can’t quite believe the service</p>
									<div class="col-md-2 col-xs-2 pull-left">
										<img src="{{URL::asset('UI/images/resource/r1.jpg')}}" alt="" />
									</div>
									<div class="col-md-8 col-xs-8 pull-left">
										<h3>Augusta Silva</h3>
										<span class="reviews-designation-span">Web designer</span>
									</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>	
	</div>
</section> --}}

<section>
	<div class="block pad-top0 career-tips-pad-left-right">
		<div data-velocity="-.1" class="parallax scrolly-invisible no-parallax bg-grey"></div><!-- PARALLAX BACKGROUND IMAGE -->
		<div class="container">
			<div class="row">
				<div class="col-lg-12 pad-top15percentage">
					<div class="heading text-center">
						<h2>News And Updates</h2>
						{{-- <span>Found by employers communicate directly with hiring managers and recruiters.</span> --}}
					</div><!-- Heading -->
					<div class="blog-sec">
						<div class="row">
							@foreach($GetNews as $News)
							<div class="col-lg-4 col-md-6">
								<div class="my-blog">
									<div class="blog-thumb">
										<a href="#" title=""><img src="{{$News['image']}}" alt="" /></a>
									</div>
									<div class="blog-details blog-pad-left-right">
									<h3 class="text-left"><a href="#" title="">{{$News['title']}}</a></h3>
										<p class="text-left">{{$News['description']}} </p>
										<a class="text-left more-btn-font-weight" href="#" title="">more ></a>
									</div>
								</div>
							</div>
							@endforeach
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

{{-- <section>
	<div class="block social-border-bottom">
		<div class="container">
			<div class="row">
				<div class="col-lg-12">
					<div class="social-links">
						<a href="#" title="" class="fb-color"><i class="fa fa-facebook"></i> Facebook</a>
						<a href="#" title="" class="tw-color"><i class="fa fa-twitter"></i> Twitter</a>
						<a href="#" title="" class="in-color"><i class="fa fa-instagram"></i> Instagram</a>
						<a href="#" title="" class="pt-color"><i class="fa fa-pinterest"></i> Pinterest</a>
						<a href="#" title="" class="gl-color"><i class="la la-google"></i> Google</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</section> --}}

{{-- <section>
	<div class="block career-tips-pad-left-right">
		<div data-velocity="-.1" class="parallax scrolly-invisible no-parallax"></div><!-- PARALLAX BACKGROUND IMAGE -->
		<div class="container">
			<div class="row">
				<div class="col-lg-12">
					<div class="heading text-center">
						<h2>For Employers</h2>
					</div><!-- Heading -->
					<div class="blog-sec">
						<div class="row">
							<div class="col-lg-8">
								<div class="my-blog employer-box-shadow employer-pad pad-top0 pad-bottom0">
									<div class="col-md-8 pull-left">
										<div class="heading hiring-pad-top-bottom">
											<h2 class="text-left">Are You Hiring?</h2>
										</div>
										<p class="home-employer-p">Find everything you need to post a job and receive the best candidates by visiting our employer website. We offer small business and enterprise options.</p>

										<a href="#" title="" class="for-employers-btn post-a-job-home">POST A JOB TODAY</a>
									</div>
									<div class="col-md-4 pull-right">
										<img src="{{URL::asset('UI/images/custom/woman_employer_home.png')}}" alt="">
									</div>
									
								</div>
							</div>
							<div class="col-lg-4 need-help-pad-left-right">
								<div class="heading hiring-pad-top-bottom need-help-home-head-title-padtop">
									<h2 class="text-left">Still Need Help?</h2>
								</div>
								<p class="home-employer-p">Let Us about your issue and  a Professional will reach you out.</p>
								<div class="subscription-sec">
									<form>
										<input type="text" placeholder="Enter Valid Email Address" />
										<button type="submit"><i class="fa fa-paper-plane" aria-hidden="true"></i></button>
									</form>
								</div>
								
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section> --}}

@endsection