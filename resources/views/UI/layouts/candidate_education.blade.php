@extends('UI.base')

@section('Content')
<section class="overlape">
		<div class="block no-padding">
			<div data-velocity="-.1" style="background: url({{URL::asset('UI/images/resource/mslider1.jpg')}}) repeat scroll 50% 422.28px transparent;" class="parallax scrolly-invisible no-parallax"></div><!-- PARALLAX BACKGROUND IMAGE -->
			<div class="container fluid">
				<div class="row">
					<div class="col-lg-12">
						<div class="inner-header">
							<h3>Welcome {{Session::get('CandidateName')}}</h3>
						</div>
					</div>
				</div>
			</div>
		</div>
    </section>



<section>
		<div class="block remove-top">
			<div class="container">
				 <div class="row no-gape">
				 	{{-- @include('UI.common.candidate_sidebar') --}}
				 	<div class="col-lg-12 column">
				 		<div class="padding-left">
					 		<div class="manage-jobs-sec">
                                <div class="contact-edit">
                                    <form>
                                        <div class="col-lg-4">
                                            <button type="button" onclick="window.location.href='/Candidate/Dashboard'" style="float:left;">Back to dashboard</button>
                                        </div>
                                        <div class="col-lg-8">
                                        </div>
                                    </form>
                                </div>
                                 <div class="border-title"><h3>My Education</h3></div>
                                 
                                 <div class="resumeadd-form">
                                        <div class="row">
                                            <div class="col-lg-6">
                                                <span class="pf-title">University</span>
                                                <div class="pf-field">
                                                    <input id="university"  type="text">
                                                </div>
                                            </div>
                                            <div class="col-lg-6">
                                                <span class="pf-title">School / College Name</span>
                                                <div class="pf-field">
                                                    <input type="text" id="school_clg">
                                                </div>
                                            </div>
                                            <div class="col-lg-4">
                                                <span class="pf-title">Department</span>
                                                <div class="pf-field">
                                                    <input type="text" id="department">
                                                </div>
                                            </div>
                                            <div class="col-lg-4">
                                                <span class="pf-title">From Date</span>
                                                <div class="pf-field">
                                                    <input placeholder="06.11.2007" type="text" class="form-control datepicker" id="from_date" autocomplete="off">
                                                </div>
                                            </div>
                                            <div class="col-lg-4">
                                                <span class="pf-title">To Date</span>
                                                <div class="pf-field">
                                                    <input placeholder="06.11.2012" type="text" class="form-control datepicker" id="to_date" autocomplete="off">
                                                </div>
                                            </div>
                                            {{-- <div class="col-lg-12">
                                                <span class="pf-title">Description</span>
                                                <div class="pf-field">
                                                    <textarea></textarea>
                                                </div>
                                            </div> --}}
                                            <div class="col-lg-12">
                                                 <button type="button" id="AddEducation">Save</button>
                                            </div>
                                        </div>
                                    </div>

						 		<div class="edu-history-sec">
                                     @foreach($GetEducation as $Education)
		 							{{-- <div class="edu-history">
		 								<i class="la la-graduation-cap"></i>
		 								<div class="edu-hisinfo">
                                         <h3>{{$Education->title}}</h3>
		 									<i>{{date('Y', strtotime($Education->from_date))}} - {{date('Y', strtotime($Education->to_date))}}</i>
		 									<span>{{$Education->name}} <i>{{$Education->department}}</i></span>
		 								</div>
		 								<ul class="action_job">
				 							<li><span>Edit</span><a href="javascript:void(0);" onclick="EditEducation({{$Education->id}})" title=""><i class="la la-pencil"></i></a></li>
				 							<li><span>Delete</span><a href="javascript:void(0);" onclick="DeleteEducation({{$Education->id}})" title=""><i class="la la-trash-o"></i></a></li>
				 						</ul>
                                     </div> --}}
                                     <div class="manage-jobs-sec">
                                     <table>
                                            <thead>
                                                <tr>
                                                    <td>University</td>
                                                    <td>School / College Name</td>
                                                    <td>Department</td>
                                                    <td>From Date</td>
                                                    <td>To Date</td>
                                                    {{-- <td>Action</td> --}}
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td>{{$Education->title}}</td>
                                                    <td>{{$Education->name}}</td>
                                                    <td>{{$Education->department}}</td>
                                                    <td>{{$Education->from_date}}</td>
                                                    <td>{{$Education->to_date}}</td>
                                                    {{-- <td>
                                                         <ul class="action_job">
                                                            <li><span>Edit</span><a href="/Employer/EditJobs/" title=""><i class="la la-pencil"></i></a></li>
                                                            <li><span>Delete</span><a href="#" title=""><i class="la la-trash-o"></i></a></li>
                                                        </ul> 
                                                    </td> --}}
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                     @endforeach
		 						</div>
		 						
                             
                            </div>
					 	</div>
					</div>
				 </div>
			</div>
		</div>
    </section>
    



    {{-- Modal --}}

    <div id="add_education" class="modal fade" role="dialog">
            <div class="modal-dialog modal-lg">
          
              <!-- Modal content-->
              <div class="modal-content">
                <div class="modal-header">
                    {{-- <button type="button" class="close" data-dismiss="modal">&times;</button> --}}
                    <h4 class="modal-title text-left">Add Education</h4>
                </div>
                <div class="modal-body">
                        <div class="resumeadd-form">
                            <div class="row">
                                <div class="col-lg-12">
                                    <span class="pf-title">University</span>
                                    <div class="pf-field">
                                        <input id="university"  type="text">
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <span class="pf-title">School / College Name</span>
                                    <div class="pf-field">
                                        <input type="text" id="school_clg">
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <span class="pf-title">Department</span>
                                    <div class="pf-field">
                                        <input type="text" id="department">
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <span class="pf-title">From Date</span>
                                    <div class="pf-field">
                                        <input placeholder="06.11.2007" type="text" class="form-control datepicker" id="from_date">
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <span class="pf-title">To Date</span>
                                    <div class="pf-field">
                                        <input placeholder="06.11.2012" type="text" class="form-control datepicker" id="to_date">
                                    </div>
                                </div>
                                {{-- <div class="col-lg-12">
                                    <span class="pf-title">Description</span>
                                    <div class="pf-field">
                                        <textarea></textarea>
                                    </div>
                                </div> --}}
                                <div class="col-lg-12">
                                     <button type="button" id="AddEducation">Save</button>
                                </div>
                            </div>
                        </div>
                </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
              </div>
          
            </div>
          </div>

@endsection


@section('JSScript')
	{{-- <script src="https://code.jquery.com/jquery-1.12.4.js"></script> --}}
	<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

	<script>
		$('#from_date').datepicker({
	onSelect: function(value, ui) {
		var today = new Date(),
			dob = new Date(value),
			age = new Date(today - dob).getFullYear() - 1970;
	},
	maxDate: '+0d',
	yearRange: '1970:2019',
	changeMonth: true,
	changeYear: true
    });
    
    $('#to_date').datepicker({
	onSelect: function(value, ui) {
		var today = new Date(),
			dob = new Date(value),
			age = new Date(today - dob).getFullYear() - 1970;
	},
	maxDate: '+0d',
	yearRange: '1970:2019',
	changeMonth: true,
	changeYear: true
    });
    
    $('#exp_from_date').datepicker({
	onSelect: function(value, ui) {
		var today = new Date(),
			dob = new Date(value),
			age = new Date(today - dob).getFullYear() - 1970;
	},
	maxDate: '+0d',
	yearRange: '1970:2019',
	changeMonth: true,
	changeYear: true
    });
    
    $('#exp_to_date').datepicker({
	onSelect: function(value, ui) {
		var today = new Date(),
			dob = new Date(value),
			age = new Date(today - dob).getFullYear() - 1970;
	},
	maxDate: '+0d',
	yearRange: '1970:2019',
	changeMonth: true,
	changeYear: true
	});
	</script>
@endsection 