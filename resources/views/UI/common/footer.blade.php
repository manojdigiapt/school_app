<footer>
	<div class="block footer-pad-left-right">
		<div class="container">
			<div class="row">
				<div class="col-lg-3 col-md-3 column mobile-footer-pad-top">
					<div class="widget">
						<div class="about_widget">
							<h3 class="footer-title">Accountswale</h3>
							<span>435, 3rd Floor, 27th Main, Sector 1, HSR Layout, Bengaluru, Karnataka 560102.</span>
							<span><img src="{{URL::asset('Demo/images/custom/phone.svg')}}" class="footer-pad-right" alt="">+91 86184 14801</span>
							<span class="footer-email-pad-bottom"><img src="{{URL::asset('Demo/images/custom/envelope.svg')}}" class="footer-pad-right" alt=""><a href="mailto:demo@gmail.com" class="__cf_email__">services@accountswale.in</a></span>
							<br>
							{{-- <h5>Follow Us</h5>
							<ul class="social">
								<li class="social-fb"> <a href="#"> <i class=" fa fa-facebook">   </i> </a> </li>
								<li class="social-twitter"> <a href="#"> <i class="fa fa-twitter">   </i> </a> </li>
								<li class="social-instagram"> <a href="#"> <i class="fa fa-instagram">   </i> </a> </li>
							</ul> --}}
						</div><!-- About Widget -->
					</div>
				</div>
				<div class="col-lg-2 col-md-3 column mobile-footer-pad-top">
					<div class="widget">
						<h3 class="footer-title">Information</h3>
						<div class="link_widgets">
							<div class="row">
								<a href="/AboutUs" title="">About Us </a>
								<a href="/Contactus" title="">Contact Us</a>
								<a href="/PrivacyPolicy" title="">Privacy Policy </a>
								<a href="/TermsOfUse" title="">Terms Of Use </a>
							</div>
						</div>
					</div>
				</div>
				<div class="col-lg-2 col-md-3 column mobile-footer-pad-top">
					<div class="widget">
						<h3 class="footer-title">Browse Jobs</h3>
						<div class="link_widgets">
							<div class="row">
								<div class="col-lg-12">
									<a href="#" title="">Audit Assistant</a>	
									<a href="#" title="">Audit Executive</a>	
									<a href="#" title="">Auditor</a>	
									<a href="#" title="">Accounts Assistant</a>	
									<a href="#" title="">Accounts Executive</a>	
									<a href="#" title="">Accounts Manager</a>	
									<a href="#" title="">CFO</a>
									<a href="#" title="">CA Fresher</a>	
								</div>
							</div>
						</div>
					</div>
				</div>
				@if(!Auth::guard('employer')->check())
				@if(!Auth::guard('candidate')->check())
				@if(!Auth::guard('trainee')->check())
				<div class="col-lg-2 col-md-3 column mobile-footer-pad-top">
					<div class="widget">
						<h3 class="footer-title">For Employers</h3>
						<div class="link_widgets">
							<div class="row">
								<div class="col-lg-12">
									<a href="#" title="">Post Jobs</a>	
									<a href="#" title="">Report Problem</a>	
								</div>
							</div>
						</div>
					</div>
				</div>
				@endif
				@endif
				@endif
				{{-- <div class="col-lg-3 col-md-4 column">
					<div class="widget">
						<div class="download_widget">
							<a href="#" title=""><img src="{{URL::asset('UI/images/resource/dw3.png')}}" alt=""></a>
							<a href="#" title=""><img src="{{URL::asset('UI/images/resource/dw4.png')}}" alt=""></a>
						</div>
					</div>
				</div> --}}
			</div>
		</div>
	</div>
	<div class="bottom-line">
		<span>© 2019 Techtalents Software Technologies Pvt Ltd. All rights reserved.</span>
		<a href="#scrollup" class="scrollup" title=""><i class="la la-arrow-up"></i></a>
	</div>
</footer>