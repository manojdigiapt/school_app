<div class="responsive-header">
	<div class="responsive-menubar">
		<div class="res-logo"><a href="index.html" title=""><img src="{{URL::asset('Demo/images/resource/logo2.png')}}" alt="" /></a></div>
		<div class="menu-resaction">
			<div class="res-openmenu">
				<img src="{{URL::asset('Demo/images/icon.png')}}" alt="" /> Menu
			</div>
			<div class="res-closemenu">
				<img src="{{URL::asset('Demo/images/icon2.png')}}" alt="" /> Close
			</div>
		</div>
	</div>
	<div class="responsive-opensec">
		<div class="btn-extars">
			<ul class="account-btns">
				<li class="signin-candidate"><a title=""><i class="la la-key"></i> Sign In / Register </a></li>
				<li class="signin-employer"><a title=""><i class="la la-external-link-square"></i> For CA Forms</a></li>
			</ul>
		</div><!-- Btn Extras -->
		<!-- <form class="res-search">
			<input type="text" placeholder="Job title, keywords or company name" />
			<button type="submit"><i class="la la-search"></i></button>
		</form> -->
		<div class="responsivemenu">
			<ul>
					<li class="">
						<a href="/" title="">Home</a>
					</li>
					<li class="">
						<a href="#" title="">About Us</a>
					</li>
					<li class="">
						<a href="#" title="">Blog</a>
					</li>
					<li class="">
						<a href="#" title="">Contact Us</a>
					</li>
				</ul>
		</div>
	</div>
</div>

@if(Auth::guard('candidate')->check())
<header class="stick-top forsticky new-header">
	<div class="menu-sec">
		<div class="container">
			<div class="logo">
				<a href="/" title=""><img class="hidesticky" src="{{URL::asset('UI/Logo_w.svg')}}" alt="" /><img class="showsticky" src="{{URL::asset('UI/Logo.svg')}}" alt="" /></a>
			</div><!-- Logo -->
			@if(!(request()->is('Candidate/CandidateFirstRegister')) ? 'active' : '')
			<div class="my-profiles-sec">
					<span>
						@if($CandidateProfile->profile_pic)
							<img src="{{URL::asset('candidate_profile')}}/{{$CandidateProfile->profile_pic}}" class="candidate-profile-pic" alt="" />
						@else
							<img src="{{URL::asset('UI/images/resource/es1.jpg')}}" alt="" />
						@endif
						 <i class="la la-bars"></i>
					</span>
				</div>
				<div class="wishlist-dropsec notification-mar-right">
					<span><a href="/Candidate/CandidateFavouriteJobs"><i class="fa fa-heart-o"></i><strong class="heart-bg-clr">3</strong></a></span>
				</div>

				<div class="wishlist-dropsec notification-mar-right">
					<span><a href="/Candidate/Dashboard"><i class="fa fa-bell-o"></i><strong class="notification-bg-clr">3</strong></a></span>
				</div>
				<div class="wishlist-dropsec">
				{{-- <span><i class="fa fa-heart-o"></i><strong>{{count($GetFavouriteJobs)}}</strong></span> --}}
					<div class="wishlist-dropdown">
							
						{{-- @if($GetFavouriteJobs)
						@foreach($GetFavouriteJobs as $Jobs)
						<ul class="scrollbar">
							<li>
								<div class="job-listing">
									<div class="job-title-sec">
										<div class="c-logo"> <img src="images/resource/l1.png" alt="" /> </div>
									<h3><a href="#" title="">{{$Jobs->title}}</a></h3>
									<span>{{$Jobs->company_name}}</span>
									</div>
								</div><!-- Job -->
							</li>
						</ul>
						@endforeach
						@else
							<p>No jobs found</p>
						@endif --}}
							
						</div>
				</div>
			<nav class="candidates-dashboard-nav-center">
				<ul>
					<li class="{{ (request()->is('Candidate/Dashboard')) ? 'active' : '' }}">
						<a href="/Candidate/Dashboard" title="">Dashboard</a>
					</li>
					<li class="{{ (request()->is('GetAppliedJobs')) ? 'active' : '' }}">
						<a href="/GetAppliedJobs" title="">Applied Jobs</a>
					</li>

					<li class="{{ (request()->is('GetShortlistedJobs')) ? 'active' : '' }}">
						<a href="/GetShortlistedJobs" title="">Shorlisted by employer</a>
					</li>
				</ul>
			</nav><!-- Menus -->
			@endif
		</div>
	</div>
</header>
@elseif(Auth::guard('employer')->check())
<header class="stick-top forsticky new-header">
	<div class="menu-sec">
		<div class="container">
			<div class="logo">
				<a href="/" title=""><img class="hidesticky" src="{{URL::asset('UI/Logo_w.svg')}}" alt="" /><img class="showsticky" src="{{URL::asset('UI/Logo.svg')}}" alt="" /></a>
			</div><!-- Logo -->
			<div class="my-profiles-sec">
					
				<span>
						@if($EmployerProfile->profile_pic)
						<img src="{{URL::asset('employer_profile')}}/{{$EmployerProfile->profile_pic}}" class="candidate-profile-pic" alt="" />
					@else
						<img src="{{URL::asset('UI/images/resource/es1.jpg')}}" alt="" />
					@endif
					<i class="la la-bars"></i></span>
			</div>
			

			<div class="wishlist-dropsec notification-mar-right">
				<span><a href="/Employer/Dashboard"><i class="fa fa-bell-o"></i><strong class="notification-bg-clr">3</strong></a></span>
			</div>
			<div class="wishlist-dropsec post-new-dropsec notification-mar-right">
				<a href="javascript:void(0);" class="for-employers-btn post-a-job-home post-new-bg"><i class="fa fa-plus"></i> POST NEW</a>
			</div>
			<div class="wishlist-dropsec">
				{{-- <span><i class="fa fa-heart-o"></i><strong>{{count($GetFavouriteJobs)}}</strong></span> --}}
					<div class="wishlist-dropdown post-new-dropdown post-new-dropdown">
						<ul class="scrollbar">
							<li>
								<div class="job-listing post-new-dropdown-menu ">
									<div class="job-title-sec">
										<a href="javascript:void(0);" title="" class="post-new-link-font Post-New-Jobs"><i class="fa fa-briefcase"> </i><span class="post-link-span">Post New Job</span></a>
									</div>
								</div><!-- Job -->
							</li>
							<li>
								<div class="job-listing post-new-dropdown-menu">
									<div class="job-title-sec">
										<a href="javascript:void(0);" title="" class="post-new-link-font Post-New-Training"><i class="fa fa-graduation-cap"></i><span class="post-link-span pad-left5"> Post New Internship</span></a>
									</div>
								</div><!-- Job -->
							</li>

							<li>
								<div class="job-listing post-new-dropdown-menu">
									<div class="job-title-sec">
										<a href="javascript:void(0);" title="" class="post-new-link-font Post-New-CAJobs"><i class="fa fa-graduation-cap"></i><span class="post-link-span pad-left5"> Post CA Articleship</span></a>
									</div>
								</div><!-- Job -->
							</li>
						</ul>
							
				</div>
			</div>

			<nav class="candidates-dashboard-nav-center menu-sec-employer-mar-left">
				<ul>
					<li class="{{ (request()->is('Employer/ManageJobs')) ? 'active' : '' }}">
						<a href="/Employer/ManageJobs" class="menu-employer-font" title="">Manage Jobs</a>
					</li>
					<li class="{{ (request()->is('Employer/ManageTrainings')) ? 'active' : '' }}">
						<a href="/Employer/ManageTrainings" class="menu-employer-font" title="">Manage Internship</a>
					</li>
					<li class="{{ (request()->is('Employer/ManageCAArticleshipJobs')) ? 'active' : '' }}">
						<a href="/Employer/ManageCAArticleshipJobs" class="menu-employer-font" title="">Manage CA Articleship</a>
					</li>
					<li class="{{ (request()->is('Candidates')) ? 'active' : '' }}">
						<a href="/Candidates" title="" class="menu-employer-font">Search Candidates</a>
					</li>
				</ul>
			</nav><!-- Menus -->
		</div>
	</div>
</header>
@elseif(Auth::guard('trainee')->check())
<header class="stick-top forsticky new-header">
	<div class="menu-sec">
		<div class="container">
			<div class="logo">
				<a href="/" title=""><img class="hidesticky" src="{{URL::asset('UI/Logo_w.svg')}}" alt="" /><img class="showsticky" src="{{URL::asset('UI/Logo.svg')}}" alt="" /></a>
			</div><!-- Logo -->
			@if(!(request()->is('Trainee/TraineeFirstRegister')) ? 'active' : '')
			<div class="my-profiles-sec">
					<span>
						@if($TraineeProfile->profile_pic)
							<img src="{{URL::asset('trainee_profile')}}/{{$TraineeProfile->profile_pic}}" class="candidate-profile-pic" alt="" />
						@else
							<img src="{{URL::asset('UI/images/resource/es1.jpg')}}" alt="" />
						@endif
						 <i class="la la-bars"></i>
					</span>
				</div>
				<div class="wishlist-dropsec notification-mar-right">
					<span><a href="/Trainee/Dashboard"><i class="fa fa-bell-o"></i></a><strong class="notification-bg-clr">3</strong></span>
				</div>
				<div class="wishlist-dropsec">
				{{-- <span><i class="fa fa-heart-o"></i><strong>{{count($GetFavouriteJobs)}}</strong></span> --}}
					<div class="wishlist-dropdown">
							
						{{-- @if($GetFavouriteJobs)
						@foreach($GetFavouriteJobs as $Jobs)
						<ul class="scrollbar">
							<li>
								<div class="job-listing">
									<div class="job-title-sec">
										<div class="c-logo"> <img src="images/resource/l1.png" alt="" /> </div>
									<h3><a href="#" title="">{{$Jobs->title}}</a></h3>
									<span>{{$Jobs->company_name}}</span>
									</div>
								</div><!-- Job -->
							</li>
						</ul>
						@endforeach
						@else
							<p>No jobs found</p>
						@endif --}}
							
						</div>
				</div>
			<nav class="candidates-dashboard-nav-center">
				<ul>
					<li class="{{ (request()->is('Trainee/Dashboard')) ? 'active' : '' }}">
						<a href="/Trainee/Dashboard" title="">Dashboard</a>
					</li>
					<li class="{{ (request()->is('GetAppliedTrainings')) ? 'active' : '' }}">
						<a href="/GetAppliedTrainings" title="">Training Status</a>
					</li>
					<li class="{{ (request()->is('FresherJobs')) ? 'active' : '' }}">
						<a href="/FresherJobs" title="">Fresher Jobs</a>
					</li>
				</ul>
			</nav><!-- Menus -->
			@endif
		</div>
	</div>
</header>
@elseif(Auth::guard('ca_article')->check())
<header class="stick-top forsticky new-header">
	<div class="menu-sec">
		<div class="container">
			<div class="logo">
				<a href="/" title=""><img class="hidesticky" src="{{URL::asset('UI/Logo_w.svg')}}" alt="" /><img class="showsticky" src="{{URL::asset('UI/Logo.svg')}}" alt="" /></a>
			</div><!-- Logo -->
			@if(!(request()->is('Articleship/ArticleshipFirstRegister')) ? 'active' : '')
			<div class="my-profiles-sec">
					<span>
						@if($ArticleProfile->profile_pic)
							<img src="{{URL::asset('trainee_profile')}}/{{$ArticleProfile->profile_pic}}" class="candidate-profile-pic" alt="" />
						@else
							<img src="{{URL::asset('UI/images/resource/es1.jpg')}}" alt="" />
						@endif
						 <i class="la la-bars"></i>
					</span>
				</div>
				<div class="wishlist-dropsec notification-mar-right">
					<span><a href="/Trainee/Dashboard"><i class="fa fa-bell-o"></i></a><strong class="notification-bg-clr">3</strong></span>
				</div>
				<div class="wishlist-dropsec">
				{{-- <span><i class="fa fa-heart-o"></i><strong>{{count($GetFavouriteJobs)}}</strong></span> --}}
					<div class="wishlist-dropdown">
							
						{{-- @if($GetFavouriteJobs)
						@foreach($GetFavouriteJobs as $Jobs)
						<ul class="scrollbar">
							<li>
								<div class="job-listing">
									<div class="job-title-sec">
										<div class="c-logo"> <img src="images/resource/l1.png" alt="" /> </div>
									<h3><a href="#" title="">{{$Jobs->title}}</a></h3>
									<span>{{$Jobs->company_name}}</span>
									</div>
								</div><!-- Job -->
							</li>
						</ul>
						@endforeach
						@else
							<p>No jobs found</p>
						@endif --}}
							
						</div>
				</div>
			<nav class="candidates-dashboard-nav-center">
				<ul>
					<li class="{{ (request()->is('Article/Dashboard')) ? 'active' : '' }}">
						<a href="/Article/Dashboard" title="">Dashboard</a>
					</li>
					<li class="{{ (request()->is('GetAppliedCAJobs')) ? 'active' : '' }}">
						<a href="/GetAppliedCAJobs" title="">CA Job Status</a>
					</li>
					<li class="{{ (request()->is('Article/FresherJobs')) ? 'active' : '' }}">
						<a href="/Article/FresherJobs" title="">Fresher Jobs</a>
					</li>
				</ul>
			</nav><!-- Menus -->
			@endif
		</div>
	</div>
</header>
@else

<header class="stick-top forsticky style2">
	<div class="menu-sec">
		<div class="container fluid">
			<div class="logo">
				@if((request()->is('Candidate/CandidateFirstRegister')) ? 'active' : '')
					<a href="/" title=""><img class="hidesticky" src="{{URL::asset('UI/Logo.svg')}}" alt="" /><img class="showsticky" src="{{URL::asset('UI/Logo.svg')}}" alt="" /></a>
				@else
					<a href="/" title=""><img class="hidesticky" src="{{URL::asset('UI/Logo_w.svg')}}" alt="" /><img class="showsticky" src="{{URL::asset('UI/Logo.svg')}}" alt="" /></a>
				@endif
			</div><!-- Logo -->
			@if(!(request()->is('Candidate/CandidateFirstRegister')) ? 'active' : '')
			<div class="btn-extars">
				 <ul class="account-btns pull-left header-sign-in-pad-right">
					<li class="signin-candidate"><a title="" href="javascript:void(0);"><img src="{{URL::asset('Demo/images/custom/login.png')}}" alt=""> <span class="signin-pad-left">Sign In / Register</span></a></li>
				</ul>
				<a href="javascript:void(0);" title="" class="signin-employer for-employers-btn">For CA Firm</a>
			</div><!-- Btn Extras -->
			<nav>
				<ul>
					<li class="#">
						<a href="/" title="">Home</a>
					</li>
					<li class="#">
						<a href="/AboutUs" title="">About Us</a>
					</li>
					{{-- <li class="">
						<a href="#" title="">Blog</a>
					</li> --}}
					<li class="">
						<a href="/Contactus" title="">Contact Us</a>
					</li>
				</ul>
			</nav><!-- Menus -->
			@endif
		</div>
	</div>
</header>
@endif
